package futuremedium.client.ract.secure.actions.join;

import java.math.BigDecimal;
import java.util.HashMap;

import com.opensymphony.xwork2.ActionContext;
import com.ract.client.ClientVO;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.web.membership.WebMembershipClient;
import com.ract.web.membership.WebMembershipTransactionContainer;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Final step of the Join process.
 * @author gnewton
 */
public class ConfirmForm extends RactActionSupport {

	private String productName;
	private String termLength;
	
  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
	setUp();
	 
    this.setActionName(ActionContext.getContext().getName());
    
    setProductList();
    setProductFrequency();
    
    try {
	    WebMembershipClient customer = (WebMembershipClient) getSession().get(CUSTOMER_TOKEN);
	    
	    //MX - make sure unspecified is still displayed after db write as female
	    if (customer.getTitle().equals("MX")) {
	    	customer.setGender(ClientVO.GENDER_UNSPECIFIED);
	    }
	    
	    WebMembershipTransactionContainer wmtc = (WebMembershipTransactionContainer) getSession().get("container");
	    
	    if (customer == null) {
	      Config.logger.warn(Config.key + ": An error occurred: customer is null");
	    }
	
	    if (wmtc == null) {
	      Config.logger.warn(Config.key + ": An error occurred: WMTC is null");
	    }
	    
	    if(productName != null && termLength != null && (productName != getSession().get("productName") || termLength != getSession().get("termLength")) ) {
	    	
	    	
	  	    getSession().put("productName", productName);
	        getSession().put("termLength", termLength);
	        getSession().put("selectedFirstInstallment", getSelectedFirstInstallment(productName, termLength));
	        getSession().put("selectedSubsequentInstallment", getSelectedSubsequentInstallment(productName, termLength));
	        
	        
	        wmtc = roadsideMgr.updateWebMembershipJoinTransactionContainer(
	            wmtc.getWebMembershipTransactionHeader().getTransactionHeaderId(),
	            new DateTime((String) getSession().get("startDate")),
	            customer,
	            (String) getSession().get("productName"),
	            (String) getSession().get("termLength"),
	            null,
	            true);
	        
	       getSession().put("container", wmtc);
	    }
	    
	    String interval = (String) getSession().get("termLength");
	    Interval i = new Interval(interval);
	    int months = i.getMonths();
	    String installmentAmount = null;
	    if(months == 1) {
	    	installmentAmount = wmtc.getWebMembershipTransactionHeader()
					   .getAmountPayable()
					   .divide(new BigDecimal(12), BigDecimal.ROUND_HALF_EVEN)
					   .toString();
	    } else {
	    	installmentAmount = wmtc.getWebMembershipTransactionHeader()
					   .getAmountPayable()
					   .toString();
	    }
	    
	    getSession().put("installmentAmount", installmentAmount);
	    
    } catch (NullPointerException e) {
        this.setActionMessage("An Error occurred: Your session has expired");
        Config.logger.warn(Config.key + ": An error occurred: session has expired: ", e);
        return "reset";
    }
    
    return SUCCESS;
  }
  
  private String getSelectedSubsequentInstallment(String product, String term) {
		String t;
		if(term.equals("1:0:0:0:0:0:0")) {
			t = "yearly"; 
		} else {
			t = "monthly";
		}
		
		String key = t + product + "Price";
		return (String) getSession().get(key);
	}
	
	private String getSelectedFirstInstallment(String product, String term) {
		String t;
		if(term.equals("1:0:0:0:0:0:0")) {
			t = "yearly"; 
		} else {
			t = "monthly";
		}
		
		String key = t + product + "FirstPrice";
		return (String) getSession().get(key);
	}
  
  private void setProductList() {
	  String productType = (String) getSession().get("productName");
	  
	  HashMap<String, String> ps = new HashMap<String, String>();
	  
	  if(productType.equals("Lifestyle")) {
		  ps.put("Lifestyle", "Lifestyle");
	  } else {
		  ps.put("Ultimate", "Roadside Ultimate");
		  ps.put("Advantage", "Roadside Advantage");
	  }
	  
	  getSession().put("products", ps);
  }
  
  private void setProductFrequency() {
	  String productType = (String) getSession().get("productName");
	  HashMap<String, String> ps = new HashMap<String, String>();
	  
	  if(!productType.equals("Lifestyle")) {
		  ps.put("0:12:0:0:0:0:0", "Monthly");
	  }
	  
	  ps.put("1:0:0:0:0:0:0", "Yearly");
	  getSession().put("frequencies", ps);
  }
  
  public String getProductName() {
	 return productName;
  }
  
  public void setProductName(String name) {
	  this.productName = name;
  }
  
  
  public String getTermLength() {
	 return termLength;
  }
  
  public void setTermLength(String termLength) {
	  this.termLength = termLength;
  }
}
