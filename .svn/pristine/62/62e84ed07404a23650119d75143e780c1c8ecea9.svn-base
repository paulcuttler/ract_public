package futuremedium.client.ract.secure.actions.insurance.home;

import com.ract.web.insurance.*;

/**
 *
 * @author gnewton
 */
public class AddressDetailsForm extends AddressDetailsBase implements PolicyAware {

  private String residentialPostcode;
  private String residentialSuburb;
  private String residentialStreet;
  private String residentialStreetNumber;
  
  private String poBoxPostcode;
  private String poBoxSuburb;
  private String poBoxNumber;

  private String email;

  private Boolean differntPostalAddress;

  /**
   * Pre-populate postcode from primary client if stored (and residentialStreet is not blank) or from WhereYouLive data.
   * @return the residentailPostcode
   */
  public String getResidentialPostcode() {
    if (this.residentialPostcode == null) {
      if (this.getPrimaryClient() == null || this.getPrimaryClient().getResiPostcode() == null) {
        this.residentialPostcode = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_POSTCODE);
      } else if (this.getResidentialStreet() != null && !this.getResidentialStreet().isEmpty()) {
        this.residentialPostcode = this.getPrimaryClient().getResiPostcode();
      }
    }
    return residentialPostcode;
  }

  /**
   * @param postalPostcode the residentailPostcode to set
   */
  public void setResidentailPostcode(String residentialPostcode) {
    this.residentialPostcode = residentialPostcode;
  }

  /**
   * Pre-populate suburb from primary client if stored (and residentialStreet is not blank) or from WhereYouLive data.
   * @return the residentialSuburb
   */
  public String getResidentialSuburb() {
    if (this.residentialSuburb == null) {
      if (this.getPrimaryClient() == null || this.getPrimaryClient().getResiSuburb() == null) {
        this.residentialSuburb = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_SUBURB);        
      } else if (this.getResidentialStreet() != null && !this.getResidentialStreet().isEmpty()) {
        this.residentialSuburb = this.getPrimaryClient().getResiSuburb();        
      }
    }
    return residentialSuburb;
  }

  /**
   * @param postalSuburb the postalSuburb to set
   */
  public void setResidentialSuburb(String residentialSuburb) {
    this.residentialSuburb = residentialSuburb;
  }

  /**
   * Pre-populate street from primary client if stored or from WhereYouLive data.
   * @return the residentialStreet
   */
  public String getResidentialStreet() {
    if (this.residentialStreet == null) {
      if (this.getPrimaryClient() == null || this.getPrimaryClient().getResiStreet() == null) {
        this.residentialStreet = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_STREET);        
      } else {
        this.residentialStreet = this.getPrimaryClient().getResiStreet();        
      }
    }
    return residentialStreet;
  }

  /**
   * @param residentialStreet the residentailStreet to set
   */
  public void setResidentailStreet(String residentialStreet) {
    this.residentialStreet = residentialStreet;
  }

  /**
   * Pre-populate streetNumber from primary client if stored (and postalStreet is not blank) or from WhereYouLive data.
   * @return the postalStreetNumber
   */
  public String getResidentialStreetNumber() {
    if (this.residentialStreetNumber == null) {
      if (this.getPrimaryClient() == null || this.getPrimaryClient().getPostStreetChar() == null) {
        this.residentialStreetNumber = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_NO);        
      } else if (this.getResidentialStreet() != null && !this.getResidentialStreet().isEmpty()) {
        this.residentialStreetNumber = this.getPrimaryClient().getResiStreetChar();        
      }
    }
    return residentialStreetNumber;
  }

  /**
   * @param ResidentialStreetNumber the ResidentialStreetNumber to set
   */
  public void setResidentialStreetNumber(String residentialStreetNumber) {
    this.residentialStreetNumber = residentialStreetNumber;
  }

  /**
   * Pre-populate poBox postcode from the client postPostcode if postalStreet is blank (i.e. PO Box filled in).
   * @return the poBoxPostcode
   */
  public String getPoBoxPostcode() {
    if (this.poBoxPostcode == null) {
      if (this.getPrimaryClient() != null && this.isDifferntPostalAddress()) {
        this.poBoxPostcode = this.getPrimaryClient().getPostPostcode();
      }
    }
    return poBoxPostcode;
  }

  /**
   * @param poBoxPostcode the poBoxPostcode to set
   */
  public void setPoBoxPostcode(String poBoxPostcode) {
    this.poBoxPostcode = poBoxPostcode;
  }

  /**
   * Pre-populate poBox suburb from the client postSuburb if postalStreet is blank (i.e. PO Box filled in).
   * @return the poBoxSuburb
   */
  public String getPoBoxSuburb() {
    if (this.poBoxSuburb == null) {
      if (this.getPrimaryClient() != null && this.isDifferntPostalAddress()) {
        this.poBoxSuburb = this.getPrimaryClient().getPostSuburb();
      }
    }
    return poBoxSuburb;
  }

  /**
   * @param poBoxSuburb the poBoxSuburb to set
   */
  public void setPoBoxSuburb(String poBoxSuburb) {
    this.poBoxSuburb = poBoxSuburb;
  }

  /**
   * Pre-populate poBox number from the client postProperty.
   * @return the poBoxNumber
   */
  public String getPoBoxNumber() {
    if (this.poBoxNumber == null) {
      if (this.getPrimaryClient() != null && this.isDifferntPostalAddress()) {
        this.poBoxNumber = this.getPrimaryClient().getPostProperty();
      }
    }
    return poBoxNumber;
  }

  /**
   * @param poBoxNumber the poBoxNumber to set
   */
  public void setPoBoxNumber(String poBoxNumber) {
    this.poBoxNumber = poBoxNumber;
  }

  /**
   * @return the differntPostalAddress
   */
  public Boolean isDifferntPostalAddress() {
    if (this.differntPostalAddress == null) {
      this.differntPostalAddress = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.DIFFERENT_POSTAL_ADDRESS));
    }
    return differntPostalAddress;
  }

  /**
   * @param differntPostalAddress the differntPostalAddress to set
   */
  public void setDifferntPostalAddress(Boolean differntPostalAddress) {
    this.differntPostalAddress = differntPostalAddress;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    if(this.email == null) {
      this.email = this.getPrimaryClient().getEmailAddress();
    }
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

}