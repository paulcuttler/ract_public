package futuremedium.client.ract.secure.actions.user;

import java.util.*;
import java.util.regex.*;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.validator.annotations.*;
import com.ract.client.ClientVO;

import com.ract.common.GenericException;
import com.ract.membership.MembershipCardVO;
import com.ract.web.campaign.CampaignEntry;
import com.ract.web.campaign.CampaignMgrRemote;
import com.ract.web.client.ActivationRequest;
import com.ract.web.security.*;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.*;

/**
 * Create web user object.
 * @author gnewton
 */
public class Create extends CreateUpdateActionBase {

  @Override
  public String execute() throws Exception {

    setUp();
    this.setActionName(ActionContext.getContext().getName());

    String extraMsg = "";
    User u = null;
    String tmpUserName = null;
    String tmpAflTeam = null;

    try {
      // check user doesn't already have account
      u = userMgr.getUserByCardNumber(getCardNumber(), false);
      if (u != null) {
        if (u.getActivationDate() == null) {
          if (u.getUserName() != null && !u.getUserName().isEmpty() && u.getFootyTipping()) {
            tmpUserName = u.getUserName();
            tmpAflTeam = u.getAflTeam();
          }
          userMgr.disableUserAccount(u.getUserId()); // disable non-activated account
        } else {
          throw new GenericException("You already have a member account.");
        }
      }

      u = new MemberUser();
      u.setFootyTipping(false); // init footy tipping

      // check for existing user by email
      @SuppressWarnings("unchecked")
      List<User> usersByEmail = (List<User>) userMgr.getUsersByEmailAddress(getEmail());
      Collections.reverse(usersByEmail);

      Config.logger.debug(Config.key + ": looking for users by email = " + getEmail());

      if (usersByEmail != null && !usersByEmail.isEmpty()) {
        for (User tmpUser : usersByEmail) {
          Config.logger.debug(Config.key + ": found user by email, username = " + tmpUser.getUserName());
          if (tmpUser.getUserName() != null && !tmpUser.getUserName().isEmpty()) {
            this.setShowTipping(false); // don't create new tipping account
            u.setFootyTipping(true);
            u.setUserName(tmpUser.getUserName());
            u.setAflTeam(tmpUser.getAflTeam());
            break;
          }
        }
      } else {
        if (tmpUserName != null && tmpAflTeam != null) {
          u.setFootyTipping(true);
          u.setUserName(tmpUserName);
          u.setAflTeam(tmpAflTeam);
        }
        Config.logger.debug(Config.key + ": found 0 users by email");
      }

      ((MemberUser) u).setMemberCardNumber(getCardNumber());
      u.setEmailAddress(getEmail());
      u.setPassword(SecurityHelper.encryptPassword(getPassword()));
      u.setResiPostcode(getResidentialPostcode());
      u.setSurname(getLastName());
      u.setBirthDate(getDobDate());

      u = userMgr.createUserAccount(u);

      String storeEmail = null;
      if (isCampaignActive() && isUpdateDetails()) {
        // Record campaign entry
        ClientVO client = this.getCustomerMgr().getClientByMembershipCardNumber(getCardNumber());
        if (client != null) {
          CampaignEntry entry = new CampaignEntry(this.getMembershipCompetition(), client.getClientNumber());

          try {
            CampaignMgrRemote campaignMgr = (CampaignMgrRemote) this.getContext().lookup("CampaignMgr/remote");
            campaignMgr.createEntry(entry);
          }
          catch (Exception ex) {
            Config.logger.error(Config.key + ": can not create campaignMgr: ", ex);
          }

          storeEmail = getEmail();
        }
      }
      else if (isUpdateDetails()) {
        // Record email to update on activation request
        storeEmail = getEmail();
      }

      // request activation
      ActivationRequest activationRequest = userMgr.requestMemberAccountActivation(
              u.getUserId(),
              getCardNumber(),
              getLastName(),
              getResidentialPostcode(),
              RactActionSupport.stripNonDigits(getHomePhoneAreaCode() + getHomePhone()),
              RactActionSupport.stripNonDigits(getMobilePhone()),
              getDobDate(),
              storeEmail,
              Config.configManager.getProperty("ract.secure.activation.url"));

      if (activationRequest.getMessage() != null) {
        throw new GenericException(activationRequest.getMessage());
      }

      String msg = Config.configManager.getProperty("member.signup.success");
      if (msg.contains("%EMAIL%")) {
        msg = Pattern.compile("%EMAIL%").matcher(msg).replaceAll(getEmail());
      }
      if (msg.contains("\n\n")) {
        msg = Pattern.compile("\n\n").matcher(msg).replaceAll("</p><p>");
      }


      // activate tipping
      if (isShowTipping()) {

        // retrieve user from DB after activation request sent
        u = userMgr.getUser(u.getUserId());

        if (useStub || userMgr.userNameAvailable(getUsername())) {
          if (tippingEmailAvailable(null, getEmail())) {
            // set fields on User object to pass to tipping
            u.setUserName(getUsername());
            u.setAflTeam(getTeam());

            try {
              createTippingAccount(u);
              extraMsg += Config.configManager.getProperty("member.tipping.create.success");
            } catch (GenericException e) {
              extraMsg += e.getMessage();
            }
          } else {
            extraMsg += Config.configManager.getProperty("member.tipping.email.notice");
          }
        } else {
          extraMsg += Config.configManager.getProperty("member.tipping.username.error");
        }
      }

      // disable duplicate accounts by email
      for (User tmpUser : usersByEmail) {
        userMgr.disableUserAccount(tmpUser.getUserId());
      }
      
      this.setActionMessage(msg + extraMsg);
      this.setPageTitle("Member Only Access Registration Successful.");

      return SUCCESS;

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + extraMsg + e.getMessage() + Config.configManager.getProperty("member.error.suffix"));
      Config.logger.error(Config.key + ": An error occurred creating a MemberUser: ", e);

      return ERROR;
    }
  }
}
