package futuremedium.client.ract.secure.actions.insurance.home;

import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.util.DateTime;

import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.WebInsQuoteDetail;
import futuremedium.client.ract.secure.Config;

/**
 *
 * @author gnewton
 */
public class AboutYourHomePolicy extends AboutYourHomePolicyBase implements PolicyAware {

  private String businessUse;
  private Boolean goodOrder;
  private Boolean currentlyUnoccupied;
  private Boolean within90Days;
  private Boolean workUndertaken;
  private Boolean exceed50K;
  private Boolean unrelatedPersons;
  private Boolean unrelatedExceed2;
  private Boolean underFinance;
  private String financier;

  @Override
  public String execute() throws Exception {

    // retrieve data objects from submitted value
    InRfDet selectedBusinessUseOption = this.getBusinessUseLookup().get(this.getBusinessUse());
    InRfDet selectedFinancierOption = this.getFinancierLookup().get(this.getFinancier());

    // save details

    boolean ok = this.getHomeService().storeCoverStartDate(this);
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.BUSINESS_USE, this.getBusinessUse(), selectedBusinessUseOption);
    ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_GOOD_ORDER, this.getGoodOrder().toString());
    
    ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_CURRENTLY_UNOCCUPIED, this.getCurrentlyUnoccupied().toString());
    if (this.getCurrentlyUnoccupied() && this.getWithin90Days() != null) {
      ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_WITHIN_90_DAYS, this.getWithin90Days().toString());
    }

    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.WORK_UNDERTAKEN, this.getWorkUndertaken().toString());
    if (this.getWorkUndertaken() && this.getExceed50K() != null) {
      ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.EXCEED_50K, this.getExceed50K().toString());
    }

    if (this.getUnrelatedPersons() != null) {
      ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_UNRELATED_PERSONS, this.getUnrelatedPersons().toString());
    }

    if (this.getUnrelatedPersons() != null && this.getUnrelatedPersons() && this.getUnrelatedExceed2() != null) {
      ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_UNRELATED_EXCEED_2, this.getUnrelatedExceed2().toString());
    }

    if(!this.isContentsOnlyQuote()) {
      ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_UNDER_FINANCE, this.getUnderFinance().toString());
      if (this.getUnderFinance() && this.getFinancier() != null) {
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.FINANCIER, this.getFinancier(), selectedFinancierOption);
      }
    }

    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    // exclusions
    if (!selectedBusinessUseOption.isAcceptable()) {
      return this.getHomeService().abortQuote(this, "Business Use", this.getBusinessUse(), Config.configManager.getProperty("insurance.home.exclusion.businessUse.message"));
    }
    if (!this.getGoodOrder()) {
      return this.getHomeService().abortQuote(this, "Good Order", this.getGoodOrder().toString(), Config.configManager.getProperty("insurance.home.exclusion.goodOrder.message"));
    }  
    if (this.getCurrentlyUnoccupied() && this.getWithin90Days() != null && !this.getWithin90Days()) {
      return this.getHomeService().abortQuote(this, "Not occupied within 90 days", this.getWithin90Days().toString(), Config.configManager.getProperty("insurance.home.exclusion.within90Days.message"));
    }
    if (this.getWorkUndertaken() && this.getExceed50K() != null && this.getExceed50K()) {
      return this.getHomeService().abortQuote(this, "Work exceeds $50K", this.getExceed50K().toString(), Config.configManager.getProperty("insurance.home.exclusion.exceed50K.message"));
    }
    if (this.getUnrelatedPersons() != null &&this.getUnrelatedPersons() && this.getUnrelatedExceed2() != null && this.getUnrelatedExceed2()) {
      return this.getHomeService().abortQuote(this, "More than 2 unrelated persons", this.getUnrelatedExceed2().toString(), Config.configManager.getProperty("insurance.home.exclusion.unrelatedExceed2.message"));
    }

    return SUCCESS;
  }

  @Override
  public void validate() {

    if (this.getUnderFinance() == null && !this.isContentsOnlyQuote()) {
      this.addFieldError("underFinance", "Please select a financier");
    }

  }
  
  /**
   * @return the businessUse
   */
  public String getBusinessUse() {
    return businessUse;
  }

  /**
   * @param businessUse the businessUse to set
   */
  @RequiredStringValidator(message = "Please select a business use")
  public void setBusinessUse(String businessUse) {
    this.businessUse = businessUse;
  }


  /**
   * @param coverStartDate the coverStartDate to set
   */

  @Override
  @RequiredFieldValidator(message = "Please select a cover start date")
  public void setCoverStartDate(DateTime coverStartDate) {
    super.setCoverStartDate(coverStartDate);
  }

  /**
   * @return the goodOrder
   */
  public Boolean getGoodOrder() {
    return goodOrder;
  }

  /**
   * @param goodOrder the goodOrder to set
   */
  @RequiredFieldValidator(message = "Please answer this question", type = ValidatorType.SIMPLE)
  public void setGoodOrder(Boolean goodOrder) {
    this.goodOrder = goodOrder;
  }

  /**
   * @return the currentlyUnoccupied
   */
  public Boolean getCurrentlyUnoccupied() {
    return currentlyUnoccupied;
  }

  /**
   * @param currentlyUnoccupied the currentlyUnoccupied to set
   */
  @RequiredFieldValidator(message = "Please answer this question", type = ValidatorType.SIMPLE)
  public void setCurrentlyUnoccupied(Boolean currentlyUnoccupied) {
    this.currentlyUnoccupied = currentlyUnoccupied;
  }

  /**
   * @return the within90Days
   */
  public Boolean getWithin90Days() {
    return within90Days;
  }

  /**
   * @param within90Days the within90Days to set
   */
  @FieldExpressionValidator(expression = "currentlyUnoccupied in {null, false} or (currentlyUnoccupied in {true} and within90Days not in {null})",
  message = "Please select if the home will be occupied within 90 days of the start of the policy")
  public void setWithin90Days(Boolean within90Days) {
    this.within90Days = within90Days;
  }

  /**
   * @return the workUndertaken
   */
  public Boolean getWorkUndertaken() {
    return workUndertaken;
  }

  /**
   * @param workUndertaken the workUndertaken to set
   */
  @RequiredFieldValidator(message = "Please answer this question", type = ValidatorType.SIMPLE)
  public void setWorkUndertaken(Boolean workUndertaken) {
    this.workUndertaken = workUndertaken;
  }

  /**
   * @return the exceed50K
   */
  public Boolean getExceed50K() {
    return exceed50K;
  }

  /**
   * @param exceed50K the exceed50K to set
   */
  @FieldExpressionValidator(expression = "workUndertaken in {null, false} or (workUndertaken in {true} and exceed50K not in {null})",
  message = "Please select if the cost of the building work exceeds $50,000")
  public void setExceed50K(Boolean exceed50K) {
    this.exceed50K = exceed50K;
  }

  /**
   * @return the unrelatedPersons
   */
  public Boolean getUnrelatedPersons() {
    return unrelatedPersons;
  }

  /**
   * @param unrelatedPersons the unrelatedPersons to set
   */
  
  @FieldExpressionValidator(expression = "(investorQuote in (true) or (investorQuote in (null, false) and unrelatedPersons not in {null}))",
  message = "Please answer this question")
  public void setUnrelatedPersons(Boolean unrelatedPersons) {
    this.unrelatedPersons = unrelatedPersons;
  }

  /**
   * @return the unrelatedExceed2
   */
  public Boolean getUnrelatedExceed2() {
    return unrelatedExceed2;
  }

  /**
   * @param unrelatedExceed2 the unrelatedExceed2 to set
   */
  @FieldExpressionValidator(expression = "unrelatedPersons in {null, false} or (unrelatedPersons in {true} and unrelatedExceed2 not in {null})",
  message = "Please select how many unrelated persons live at this address")
  public void setUnrelatedExceed2(Boolean unrelatedExceed2) {
    this.unrelatedExceed2 = unrelatedExceed2;
  }

  /**
   * @return the underFinance
   */
  public Boolean getUnderFinance() {
    return underFinance;
  }

  /**
   * @param underFinance the underFinance to set
   */
  
  public void setUnderFinance(Boolean underFinance) {
    this.underFinance = underFinance;
  }

  /**
   * @return the financier
   */
  public String getFinancier() {
    return financier;
  }

  /**
   * @param financier the financier to set
   */
  @FieldExpressionValidator(expression = "underFinance in {null, false} or (underFinance in {true} and financier not in {null, ''})",
  message = "Please select a financier")
  public void setFinancier(String financier) {
    this.financier = financier;
  }
}
