package futuremedium.client.ract.secure.entities.insurance;

import org.springframework.stereotype.Service;

import com.ract.web.agent.WebAgentStatus;
import freemarker.template.utility.StringUtil;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.insurance.AgentPortal;
import futuremedium.client.ract.secure.actions.insurance.AgentPortalLogout;
import futuremedium.client.ract.secure.entities.BaseService;

/**
 * Services relating to Agent Portal.
 * @author gnewton
 */
@Service
public class AgentService extends BaseService {

  /**
   * Authenticate an Agent against RACT API.
   * @param action
   * @return
   */
  public boolean authenticateAgent(AgentPortal action) {

    boolean success = false;
    WebAgentStatus status = null;

    try {
      status = this.getInsuranceMgr().validateAgent(action.getAgentCode(), action.getPassword());

      if (status.getAgentStatusCode() == 0) { // ok
        action.setPersistedAgentCode(action.getAgentCode());
        action.setPersistedAgentUser(action.getUsername());
        success = true;
      } else {
        action.setActionMessage("An error has occurred: " + StringUtil.capitalize(status.getAgentStatusMessage()));
        // fall through to return false
      }

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to validate agent: " + action.getAgentCode(), e);
      action.setActionMessage("Unable to determine agent.");
    }

    return success;
  }
  
  public void logout(AgentPortalLogout action) {
  	action.setPersistedAgentCode(null);
  	action.setPersistedAgentUser(null);
  }

}