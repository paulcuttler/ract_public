# ------------------- Standard ConfigManager properties --------------------------
configManager.key=RACT_SECURE
configManager.name=RACT Secure Config Manager

configManager.server.development=false

configManager.logger.default=ract_secure

# ----------------------- Logging properties ----------------------------
log4j.logger.ract_secure=info, file

log4j.appender.file=org.apache.log4j.RollingFileAppender
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=%5p [%d{dd-MM-yy HH:mm:ss}] (%F:%L) - %m%n
log4j.appender.file.File=/data4/webprd/jboss-4.2.2/server/default/log/ract_secure.log
log4j.appender.file.bufferedIO=false
log4j.appender.file.Append=true
log4j.appender.file.MaxFileSize=100kb
log4j.appender.file.MaxBackupIndex=5

# ----------------------- Application specific properties -----------------------

# base URLs
ract.public.baseUrl=http://www.ract.com.au/
ract.secure.baseUrl=https://secure.ract.com.au/

# shell URLs
ract.secure.user.shell.url=${ract.public.baseUrl}member_area
ract.secure.membership.shell.url=${ract.public.baseUrl}membership/become_a_member
ract.secure.insurance.shell.url=${ract.public.baseUrl}insurance/car
ract.secure.tipping.shell.url=${ract.public.baseUrl}footy_tipping/prizes

# metadata values
ract.secure.metadata.User.title=User
ract.secure.metadata.User.keywords=User
ract.secure.metadata.User.description=User

ract.secure.metadata.Insurance.title=Insurance
ract.secure.metadata.Insurance.keywords=Insurance
ract.secure.metadata.Insurance.description=Insurance

ract.secure.metadata.Membership.title=Membership
ract.secure.metadata.Membership.keywords=Membership
ract.secure.metadata.Membership.description=Membership

ract.secure.metadata.Tipping.title=Footy Tipping
ract.secure.metadata.Tipping.keywords=Footy Tipping
ract.secure.metadata.Tipping.description=Footy Tipping

# use stub or remote EJB calls? (stub for testing without VPN access)
ract.secure.stub=false
ract.secure.password.length=8
ract.secure.membership.expiryDays=14

# URL to Autopublish-driven banner service
ract.public.banner.service.url=${ract.public.baseUrl}banner

# public site links prefix
ract.public.prefix=${ract.public.baseUrl}

# ips permitted to access the site remotely
ract.public.permit.ips=116.213.4.244,203.45.11.136,127.0.0.1,203.20.20.13,203.20.21.3,116.213.4.249

# transform occurences of http to https?
ract.secure.transformHttp=true

# is tipping currently running?
ract.public.tipping.enabled=true

ract.public.tipping.terms=${ract.public.baseUrl}footy_tipping/terms_and_conditions
ract.public.member.terms=${ract.public.baseUrl}roadside/terms_and_conditions

# SSO target URLs
futuremedium.sso.targetUrl=${ract.public.baseUrl}Controller
futuremedium.sso.server.tomcat_a.setupUrl=${ract.public.baseUrl}servlets/a/Controller
futuremedium.sso.server.tomcat_b.setupUrl=${ract.public.baseUrl}servlets/b/Controller
futuremedium.sso.loginlogoutUrl=${ract.public.baseUrl}Controller

# URLs to redirect to after login
ract.secure.welcome.redirectUrl=${ract.secure.baseUrl}UserWelcome.action
ract.secure.tipping.redirectUrl=${ract.public.baseUrl}footy_tipping/tipping_home

ract.secure.activation.url=${ract.secure.baseUrl}UserActivate.action?activationRequestNo=

# Fake namespace path
ract.secure.fakeNamespace=/

# URL re-writing values
ract.secure.urlRewriteFrom=/
#ract.secure.urlRewriteTo=/
ract.secure.urlRewriteTo=https://www.ract.com.au/

java.naming.factory.initial=org.jnp.interfaces.NamingContextFactory
java.naming.factory.url.pkgs=org.jboss.naming:org.jnp.interfaces
java.naming.provider.url=jnp://203.20.20.13:1099

# RACT Member Access properties
member.signup.success=Your member account has been successfully created.\n\nYou will need to activate your member account by clicking on the link in the email we sent to %EMAIL%.\n\nThe 'MEMBER ONLY AREA' cannot be used until you activate the account.
member.error.suffix=</p><p>Please email <a href="mailto:memberaccess@ract.com.au">memberaccess@ract.com.au</a> if you require further assistance.
member.tipping.create.success=</p><p>Your tipping account has been created. You will receive a confirmation email shortly
member.tipping.email.notice=</p><p class="sys-msg msg-error">Note: The email address you selected was unavailable on the Tipping Platform, please try another email address after logging in.
member.tipping.username.error=</p><p class="sys-msg msg-error">The footy tipping username entered is already in use by another user. Please try another username after logging into the member only area.

# upper lower bounds of agreed value expressed as float
insurance.vehicle.agreedValue.lower=0.1
insurance.vehicle.agreedValue.upper=0.1

# RACT insurance validation properties
insurance.validation.string.empty=<span class='highlight'>fieldname</span> cannot be empty
insurance.validation.string.name=<span class='highlight'>fieldname</span> can only contain letters and -,'
insurance.validation.integer.empty=<span class='highlight'>fieldname</span> is a number and it cannot be empty
insurance.validation.integer.numberonly=<span class='highlight'>fieldname</span> is number only
insurance.validation.integer.fourdigit=<span class='highlight'>fieldname</span> is a number and it must be 4 digits
insurance.validation.date.invalidate=<span class='highlight'>fieldname</span> must be in format of dd/mm/yyyy
insurance.validation.radio.empty=You must indicate <span class='highlight'>fieldname</span>
insurance.validation.select.empty=You must select <span class='highlight'>fieldname</span>
insurance.validation.captcha=You must enter the correct characters as shown in the picture before proceeding.
insurance.validation.commenceddriving=Driver License Commencement Year Invalid.
insurance.validation.dob=Driver age must be greater than 16 and less than 100.
insurance.validation.telephonenumber.wrong=<span class='highlight'>fieldname</span> must be 10 digits.
insurance.validation.email.wrong=You must enter a valid <span class='highlight'>fieldname</span>.

# RACT insurance message properties
insurance.message.carusage.uninsurancable=We need to ask you some more questions about the usage of your vehicle. Please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.
insurnace.message.carusage.commercial=RACT Insurance is unable to provide cover for business use vehicles. Please contact GIO Commercial Insurance.
insurance.message.carvalue=We need to ask you some more questions about the value of your vehicle, please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>. Alternately you may wish to re evaluate the value of your vehicle.
insurance.message.highperformancecar=You indicated that you are under age 25 and driving a high performance car. We need to ask you some more questions please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.
insurance.message.twoaccidents=You have had two accidents in the past three years. We need to ask you some more questions please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.
insurance.message.accidentsmorethantwo=You have had more than 2 accidents in the past five years. We need to ask you some more questions please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.
insurance.message.extradamage=You indicated that your vehicle has existing damage. We need to ask you some more questions please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.
insurance.message.nonstandardaccessories=We need to ask you some more questions about your non-standard accessories or modifications. Please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.
insurance.message.additionalcriminal=We need to ask you some more questions please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.
insurance.message.fiftyplus=You have indicated that all drivers and policy holders in this quote are over fifty. However, at least one driver or policy holder's date of birth does not indicate this.
insurance.message.suspension=You indicated that your license has been suspended before. We need to ask you some more questions please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.
insurance.message.cancelledinsurance=You indicated that your insurance has been refused or cancelled before. We need to ask you some more questions please <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.
insurance.message.noprimarycontact=No primary contact has been saved.
insurance.message.tp.amountcovered=Third Party Property Damage up to $20 million
insurance.message.tp.fireTheft.amountcovered=Fire and theft cover up to $5,000.
insurance.message.comp.amountcovered=<value> Comprehensive Insurance
insurance.message.unacceptablevehicle=The selected vehicle is not acceptable
insurance.message.zeropremium=An error has occurred. Please try again later or <a href='${ract.public.baseUrl}contact_us' rel='external'>contact us</a> on <span class='highlight'>13 27 22</span>.