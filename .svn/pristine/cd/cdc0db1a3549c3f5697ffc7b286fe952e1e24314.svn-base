package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.actions.insurance.InsuranceBase;

/**
 * Action class save and validation page 1 questions.
 * @author xmfu
 */
public class QuotePageSave1 extends QuotePageSave {
    private static final long serialVersionUID = 7163614027666970138L;
  private String conditions;
  private String existingQuoteNumber;
  private String savedQuoteEmail;

  @Override
  public String execute() throws Exception {

    this.setUp();
        
    // Clean session for a new quote.
    this.cleanSession();
    this.setPageId(1L);

    // Check whether it is a existing quote.
    if (this.getExistingQuoteNumber() != null && !this.getExistingQuoteNumber().isEmpty()) {
      if (!this.verifyCaptcha()) {
      	this.setStartPage("saved");
        return validationFailed("", VALIDATE_CAPTCHA, false);
      }

      Integer quoteNumber = null;

      try {
        quoteNumber = WebInsQuote.validateQuoteNo(this.getExistingQuoteNumber());
      } catch (Exception e) {}

      if (quoteNumber != null && quoteNumber != 0) {
        WebInsQuote currentQuote = this.insuranceMgr.getQuote(quoteNumber);

        // check quote status
        List<String> permittedStatuses = new ArrayList<String>();
        permittedStatuses.add(WebInsQuote.NEW);
        permittedStatuses.add(WebInsQuote.QUOTE);
        permittedStatuses.add(WebInsQuote.COMPLETE);
        permittedStatuses.add(WebInsQuote.COVER_ISSUED);

        if (!permittedStatuses.contains(currentQuote.getQuoteStatus()) || !currentQuote.getQuoteType().equals(WebInsQuote.TYPE_MOTOR)) {
          Config.logger.info(Config.key + ": Vehicle Quote #" + quoteNumber + " is not in a retrievable state. (" + currentQuote.getQuoteStatus() + " | " + currentQuote.getQuoteType() + ").");
        	this.setStartPage("saved");
          return validationFailed("Quote Number was not found.", VALIDATE_CUSTOMIZED, true);
        }

        // test quote date
        Calendar validDate = Calendar.getInstance();
        validDate.add(Calendar.DATE, -Integer.parseInt(Config.configManager.getProperty("insurance.vehicle.maxQuoteAgeDays", "14")));

        Calendar quoteDate = Calendar.getInstance();
        quoteDate.setTime(currentQuote.getQuoteDate());

        if (quoteDate.before(validDate)) {
          Config.logger.info(Config.key + ": Vehicle Quote #" + quoteNumber + " is not in a retrievable state.");
        	this.setStartPage("saved");
          return validationFailed("Quote Number is not in a retrievable state (expired).", VALIDATE_CUSTOMIZED, true);
        }

        // test quote type
        String webQuoteType = this.insuranceMgr.getQuoteDetail(quoteNumber, WebInsQuoteDetail.WEB_QUOTE_TYPE);
        if (!WebInsQuoteDetail.WEB_QUOTE_TYPE_MOTOR.equalsIgnoreCase(webQuoteType)) {
            Config.logger.info(Config.key + ": Quote #" + quoteNumber + " is not a valid vehicle quote.");
            this.setStartPage("saved");
            return validationFailed("We were unable to locate a valid vehicle quote for the specified quote number.", VALIDATE_CUSTOMIZED, true);
        }
        
        // make sure that the email addresses are aligned
        String savedQuoteEmailAddress = this.insuranceMgr.getQuoteDetail(quoteNumber, VehicleSaveQuote.SAVED_QUOTE_EMAIL);
        if(null == savedQuoteEmailAddress || !savedQuoteEmailAddress.equals(savedQuoteEmail)) {
            Config.logger.info(Config.key + ": Quote #" + quoteNumber + " is not associated with email address " + savedQuoteEmailAddress + ".");
            this.setStartPage("saved");
            return validationFailed("We were unable to locate a valid vehicle quote for the specified quote number and email address.", VALIDATE_CUSTOMIZED, true);
        }
        
        this.setQuoteNo(quoteNumber);
        this.setSecureQuoteNumber(this.insuranceMgr.getQuote(quoteNumber).getSecuredQuoteNo());

        // Redirect to previous visited page for an existing quote.
        String currentPageId = this.insuranceMgr.getQuoteDetail(this.getQuoteNo(), QuotePageSave.FIELD_CURRENT_PAGE_ID);

        if (currentPageId != null && !currentPageId.isEmpty()) {
          this.setPageId(Long.valueOf(currentPageId));
        }
        
        // record user's session
        String sessionId = recordSession(null, RactActionSupport.CONTEXT_VEHICLE_QUOTE);
        this.getInsuranceMgr().setQuoteDetail(quoteNumber, InsuranceBase.FIELD_SESSION_ID, sessionId);

      } else {
      	this.setStartPage("saved");
        return validationFailed("Quote Number was not found.", VALIDATE_CUSTOMIZED, true);
      }

      this.setNewQuote(false);
      this.setNextAction();

      return SUCCESS;

    } else if (this.getConditions() != null && this.getConditions().equals("checked")) { // New quote.

      this.setNewQuote(true);
      this.setNextAction();

      return SUCCESS;
    } else {
      return validationFailed("You must accept the terms and conditions.", VALIDATE_CUSTOMIZED, true);
    }
  }

  @Override
  public void validate() {
    if (this.getExistingQuoteNumber() != null && !this.getExistingQuoteNumber().isEmpty()) {
      if (this.getCaptchaResponse() == null || this.getCaptchaResponse().isEmpty()) {
        this.addFieldError("captchaResponse", "Please enter correct the security phrase.");
        this.setStartPage("saved");
      }
      
      if(!validate(this.getSavedQuoteEmail(), VALIDATE_EMAIL)) {
          this.addFieldError("savedQuoteEmail", "Please enter a valid email address.");
          this.setStartPage("saved");
      }
      
    } else if (this.getConditions() == null || !this.getConditions().equals("checked")) {
      this.addFieldError("conditions", "You must accept the terms and conditions.");
    }
    
  }

  /**
   * @return the conditions
   */
  public String getConditions() {
    return conditions;
  }

  /**
   * @param conditions the conditions to set
   */
  public void setConditions(String conditions) {
    this.conditions = conditions;
  }

  /**
   * @return the existingQuoteNumber
   */
  public String getExistingQuoteNumber() {
    return existingQuoteNumber.trim();
  }

  /**
   * @param existingQuoteNumber the existingQuoteNumber to set
   */
  public void setExistingQuoteNumber(String existingQuoteNumber) {
    this.existingQuoteNumber = existingQuoteNumber;
  }
  
  /**
   * @return the savedQuoteEmail
   */
  public String getSavedQuoteEmail() {
      return savedQuoteEmail;
  }

  /**
   * @param savedQuoteEmail
   *            the savedQuoteEmail to set
   */
  public void setSavedQuoteEmail(String savedQuoteEmail) {
      this.savedQuoteEmail = savedQuoteEmail;
  }
}