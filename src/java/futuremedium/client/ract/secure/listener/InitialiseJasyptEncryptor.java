package futuremedium.client.ract.secure.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate3.encryptor.HibernatePBEEncryptorRegistry;

import futuremedium.client.ract.secure.InvalidKeyStore;
import futuremedium.client.ract.secure.SecretsManager;


/**
 * This context listener initialises the strong string encryptor provided by Jasypt
 * and registers it in Hibernate's type registry
 * 
 * @see ract_public web.xml for listener configuration
 * @see ractPublicEJB class com.ract.web.payment.WebPayment for annotated implementation
 * 
 * @author sharpg
 *
 */
public class InitialiseJasyptEncryptor implements ServletContextListener {
	
	static final String JASYPT_HEXADECIMAL_OUTPUT = "hexadecimal"; 

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("Jasypt listener destroyed"); // make sure this is logged
	}

	@Override
	public void contextInitialized(ServletContextEvent context) {
		String password = loadPassword();
		System.out.println("Jasypt listener started"); // make sure this is logged
		StandardPBEStringEncryptor ractStringEncryptor = new StandardPBEStringEncryptor();
		ractStringEncryptor.setStringOutputType("hexadecimal");
		ractStringEncryptor.setAlgorithm("PBEWithMD5AndDES"); // ensure STRONG
		
		// this is REQUIRED
		String passwordENC = String.format("ENC(%s)", password);
		ractStringEncryptor.setPassword(passwordENC);
		
		HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
		registry.registerPBEStringEncryptor("ractStringEncryptor", ractStringEncryptor);
	}
	
	private String loadPassword() {
		String password = null;
		SecretsManager sm;
		
		try {
			sm = SecretsManager.getInstance();
			password = sm.getSecret("jasypt_password");
		} catch (InvalidKeyStore e) {
			e.printStackTrace();
		}
				
		return password;
	}
}