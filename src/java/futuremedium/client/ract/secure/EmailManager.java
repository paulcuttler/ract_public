package futuremedium.client.ract.secure;

import java.util.*;

import com.opensymphony.xwork2.ActionContext;
import com.ract.common.CommonEJBHelper;
import com.ract.common.MailMgr;
import com.ract.common.mail.MailMessage;

import futuremedium.client.ract.secure.actions.campaign.Campaign;
import futuremedium.common2.EmailBuilder;

import futuremedium.client.ract.secure.actions.insurance.home.SaveQuote;

import futuremedium.client.ract.secure.actions.insurance.car.VehicleSaveQuote;
import futuremedium.client.ract.secure.interceptors.JSONfeed;
import org.json.JSONObject;
/**
 * Centralised handling of email messages.
 * 
 * @author gnewton
 */
public class EmailManager {

  /**
   * Email a HTML saved quote email.
   * @param action action containing parameters to set.
   */
  static public boolean sendCampaignEmail(Campaign action) {
    boolean success = false;

    EmailBuilder builder = new EmailBuilder(Config.configManager);

    builder.setHtmlShellUrl(Config.configManager.getProperty("email.campaign.url"));
    builder.setFrom(Config.configManager.getProperty("email.default.fromEmail"), Config.configManager.getProperty("email.default.fromName") );

    builder.addTo(action.getEmail(), action.getEmail());

    builder.setSubject("RACT Campaign Email");

    Map application = ActionContext.getContext().getApplication();
    if(application != null) {
      JSONObject json = (JSONObject) application.get(JSONfeed.CAMPAIGN_JSON);
      if(json != null) {
        try {
          builder.setBody(json.get("email").toString());
        } catch(Exception e) {
          Config.logger.error(Config.key + ": cannot get email from json: " + e);
        }
      }
    }

    try {
      builder.sendHtmlEmail();
      success = true;
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to send vehicle campaign email to: " + action.getEmail(), e);
    }

    return success;
  }

  /**
   * Email a HTML saved quote email.
   * @param action action containing parameters to set.
   */
  static public boolean sendVehicleSavedQuoteEmail(VehicleSaveQuote action) {
    boolean success = false;

    EmailBuilder builder = EmailManager.buildBasicEmail("vehicleSavedQuoteEmail");

    builder.clearTo();
    builder.addTo(action.getEmail(), action.getEmail());
    builder.setSubstitution("%QUOTE_NO%",  action.getSecureQuoteNumber());

    try {
    	MailMessage message= new MailMessage();
    	message.setSubject(builder.getSubstitutedSubject());
  		message.setRecipient(builder.getFirstToEmail());
  		message.setMessage(builder.getHtmlMessage());
  		message.setFrom(builder.getFromEmail());
  		message.setHtml(true);
    	
    	MailMgr mailMgr = CommonEJBHelper.getMailMgr();
  		mailMgr.sendMail(message);
      //builder.sendHtmlEmail();

      success = true;

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to send vehicle savedQuoteEmail to: " + action.getEmail(), e);
    }

    return success;
  }
  /**
   * Email a HTML saved quote email.
   * @param action action containing parameters to set.
   */
  static public boolean sendSavedQuoteEmail(SaveQuote action) {
    boolean success = false;
    EmailBuilder builder = EmailManager.buildBasicEmail("savedQuoteEmail");
    builder.clearTo();
    builder.addTo(action.getEmail(), action.getEmail());

    builder.setSubstitution("%QUOTE_NO%", action.getSecuredQuoteNo());
    
    try {
    	MailMessage message= new MailMessage();
    	message.setSubject(builder.getSubstitutedSubject());
  		message.setRecipient(builder.getFirstToEmail());
  		message.setMessage(builder.getHtmlMessage());
  		message.setFrom(builder.getFromEmail());
  		message.setHtml(true);
    	
    	MailMgr mailMgr = CommonEJBHelper.getMailMgr();
  		mailMgr.sendMail(message);
      //builder.sendHtmlEmail();

      success = true;

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to send home savedQuoteEmail to: " + action.getEmail(), e);
    }

    return success;
  }
  /**
   * Send an email determined by the key passed.
   *
   * The key looks up an entry from the configuration file in the form: email.EMAIL_KEY.subject and email.EMAIL_KEY.body
   * A User object is passed in to determine the user's name and email address to send to.  Pass null in if you wish to set this later or use the buildbasicEmail(emailKey) method.
   * @param emailKey key to look up subject and body from configuration file.
   * @return an EmailBuilder object mostly ready to go.
   */
  static public EmailBuilder buildBasicEmail(String emailKey) {
    EmailBuilder builder = new EmailBuilder(Config.configManager);

    builder.setHtmlShellUrl(Config.configManager.getProperty("email.url"));
    builder.setFrom(Config.configManager.getProperty("email.default.fromEmail"), Config.configManager.getProperty("email.default.fromName") );
    
    if (builder.getToList().isEmpty()) {
      builder.addTo(Config.configManager.getProperty("email.default.toEmail"), Config.configManager.getProperty("email.default.toName") );
    }

    builder.setSubject(Config.configManager.getProperty("email." + emailKey + ".subject"));
    builder.setBody(Config.configManager.getProperty("email." + emailKey + ".body"));
      
    return builder;
  }

}
