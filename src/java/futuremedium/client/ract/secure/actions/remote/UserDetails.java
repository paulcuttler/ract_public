package futuremedium.client.ract.secure.actions.remote;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.Validation;
import com.ract.web.client.ClientTransaction;

import com.ract.web.security.*;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Retrieve user details by username.
 * Used by Footy Tipping.
 * @author gnewton
 */
@Validation
public class UserDetails extends RactActionSupport {

  private String username;

  @Override
  public String execute() throws Exception {
    setUp();

    try {
      User user = userMgr.getUserAccount(getUsername());
      ClientTransaction customer = null;

      if (user instanceof MemberUser) {
        customer = getCustomer(Integer.parseInt(((MemberUser) user).getClientNumber()));
      }

      StringBuilder payload = new StringBuilder();
      payload.append("<user>");
      payload.append("<name>");
      payload.append(customer != null ? (customer.getTitle() != null ? customer.getTitle() : "") : (user.getTitle() != null ? user.getTitle() : ""));
      payload.append(" ");
      payload.append(customer != null ? (customer.getGivenNames() != null ? customer.getGivenNames() : "") : (user.getGivenNames() != null ? user.getGivenNames() : ""));
      payload.append(" ");
      payload.append(customer != null ? (customer.getSurname() != null ? customer.getSurname() : "") : (user.getSurname() != null ? user.getSurname() : ""));
      payload.append("</name>");
      payload.append("<memberNo>");
      payload.append(customer != null ? ((MemberUser) user).getClientNumber() : "n/a");
      payload.append("</memberNo>");
      payload.append("<cardNo>");
      payload.append(customer != null ? ((MemberUser) user).getMemberCardNumber() : "n/a");
      payload.append("</cardNo>");
      payload.append("<gender>");
      payload.append(customer != null ? (customer.getGender() != null ? customer.getGender() : "") : (user.getGender() != null ? user.getGender() : ""));
      payload.append("</gender>");
      payload.append("<phone>");
      payload.append("<h>");
      payload.append(customer != null ? (customer.getHomePhone() != null ? customer.getHomePhone() : "") : (user.getHomePhone() != null ? user.getHomePhone() : ""));
      payload.append("</h>");
      payload.append("<m>");
      payload.append(customer != null ? (customer.getMobilePhone() != null ? customer.getMobilePhone() : "") : (user.getMobilePhone() != null ? user.getMobilePhone() : ""));
      payload.append("</m>");
      payload.append("<w>");
      payload.append(customer != null ? (customer.getWorkPhone() != null ? customer.getWorkPhone() : "") : (user.getWorkPhone() != null ? user.getWorkPhone() : ""));
      payload.append("</w>");
      payload.append("</phone>");
      payload.append("<address>");
      payload.append("<r>");
      payload.append(customer != null ? (customer.getResiProperty() != null ? customer.getResiProperty() : "") : (user.getResiProperty() != null ? user.getResiProperty() : ""));
      payload.append(" ");
      payload.append(customer != null ? (customer.getResiStreetChar() != null ? customer.getResiStreetChar() : "") : (user.getResiStreetChar() != null ? user.getResiStreetChar() : ""));
      payload.append(" ");
      payload.append(customer != null ? (customer.getResiStreet() != null ? customer.getResiStreet() : "") : (user.getResiStreet() != null ? user.getResiStreet() : ""));
      payload.append(", ");
      payload.append(customer != null ? (customer.getResiSuburb() != null ? customer.getResiSuburb() : "") : (user.getResiSuburb() != null ? user.getResiSuburb() : ""));
      payload.append(", ");
      payload.append(customer != null ? (customer.getResiPostcode() != null ? customer.getResiPostcode() : "") : (user.getResiPostcode() != null ? user.getResiPostcode() : ""));
      payload.append(", ");
      payload.append(customer != null ? (customer.getResiState() != null ? customer.getResiState() : "") : (user.getResiState() != null ? user.getResiState() : ""));
      payload.append("</r>");
      payload.append("<p>");
      payload.append("</p>");
      payload.append("</address>");
      payload.append("</user>");

      this.setActionMessage(payload.toString());

      return SUCCESS;
    } catch (Exception e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred checking availability of username via remote call from public to secure.", e);
      return ERROR;
    }
  }

  /**
   * @return the username
   */
  @RequiredStringValidator(message = "Please supply a username")
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }
}
