package futuremedium.client.ract.secure.actions.remote;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.ract.common.GenericException;
import com.ract.web.security.User;
import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Change user username remotely.
 * Used by Footy Tipping.
 *
 * @author gnewton
 */
public class ChangeUsername extends RactActionSupport {
  private String oldUsername;
  private String newUsername;

  @Override
  public String execute() throws Exception {
    setUp();

    try {
      User user = userMgr.getUserAccount(getOldUsername());
      user.setUserName(getNewUsername());
      userMgr.updateUserAccount(user);

      this.setActionMessage("Username has been changed.");

      return SUCCESS;
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred changing User username via remote call from public to secure.", e);
      return ERROR;
    }
  }

  /**
   * @return the oldUsername
   */
  @RequiredStringValidator(message = "Supply old username")
  public String getOldUsername() {
    return oldUsername;
  }

  /**
   * @param oldUsername the oldUsername to set
   */
  public void setOldUsername(String oldUsername) {
    this.oldUsername = oldUsername;
  }

  /**
   * @return the newUsername
   */
  @RequiredStringValidator(message = "Supply new username")
  public String getNewUsername() {
    return newUsername;
  }

  /**
   * @param newUsername the newUsername to set
   */
  public void setNewUsername(String newUsername) {
    this.newUsername = newUsername;
  }


}
