package futuremedium.client.ract.secure.actions.remote;

import com.ract.common.GenericException;
import com.ract.web.security.User;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Unsubscribe a user from Tipping remotely.
 * @author gnewton
 */
public class UnsubscribeTipping extends RactActionSupport {
  private String username;

  @Override
  public String execute() throws Exception {
    setUp();

    try {
      User user = userMgr.getUserAccount(getUsername());
      user.setFootyTipping(false);
      user = userMgr.updateUserAccount(user);

      this.setActionMessage("Tipping unsubscribed.");

      return SUCCESS;
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred unsubscribing User from Tipping via remote call from public to secure.", e);
      return ERROR;
    }
  }

  /**
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }

}
