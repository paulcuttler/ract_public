package futuremedium.client.ract.secure.actions.remote;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Check if username is available.
 * Used by Footy Tipping.
 * @author gnewton
 */
public class UsernameAvailable extends RactActionSupport  {
  private String username;

  @Override
  public String execute() throws Exception {
    setUp();

    try {
      userMgr.userNameAvailable(getUsername());

      this.setActionMessage("Username is available");

      return SUCCESS;
    } catch (Exception e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred checking availability of username via remote call from public to secure.", e);
      return ERROR;
    }
  }

  /**
   * @return the username
   */
  @RequiredStringValidator(message = "Please supply a username")
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }
}
