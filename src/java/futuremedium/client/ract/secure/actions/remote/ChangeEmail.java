package futuremedium.client.ract.secure.actions.remote;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

import com.ract.common.GenericException;
import com.ract.web.security.User;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Change user email remotely.
 * Used by Footy Tipping.
 * @author gnewton
 */
public class ChangeEmail extends RactActionSupport {
  private String username;
  private String email;

  @Override
  public String execute() throws Exception {
    setUp();

    try {
      User user = userMgr.getUserAccount(getUsername());

      user.setEmailAddress(getEmail());
      user = userMgr.updateUserAccount(user);
      this.setActionMessage("Email has been changed.");

      return SUCCESS;
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred changing User email via remote call from public to secure.", e);
      return ERROR;
    }
  }

  /**
   * @return the username
   */
  @RequiredStringValidator(message = "Supply username")
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * @return the email
   */
  @RequiredStringValidator(message = "Supply email")
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

}
