package futuremedium.client.ract.secure.actions;

import java.io.*;
import java.util.*;

import java.awt.Color;
import java.awt.image.BufferedImage;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.multitype.GenericManageableCaptchaService;
import futuremedium.client.ract.secure.Config;

import futuremedium.client.ract.secure.actions.insurance.CaptchaServiceSingleton;

/**
 * CAPTCHA actions.
 * @author gnewton
 */
public class CaptchaSupport extends RactActionSupport {
    private static final long serialVersionUID = 4290627146897247488L;
  // For file download.
  private String contentType;
  private String contentDisposition;
  private InputStream inputStream;
  private String captchaResponse;

  @Override
  public String execute() throws Exception {
    return captchaImage();
  }

  public Long getCaptchaTriedTimes() {
    Map<String, Object> session = getSession();
    return (Long) session.get("captchaTriedTimes");
  }

  public void setCaptchaTriedTimes(Long captchaTriedTimes) {
    Map<String, Object> session = getSession();
    session.put("captchaTriedTimes", captchaTriedTimes);
  }

  public boolean verifyCaptcha() {
    boolean isCaptchaCorrect = false;

    // allow a bypass code for automated testing
    String captchaBypass = Config.configManager.getProperty("insurance.captcha.bypass", "");
    if (!captchaBypass.isEmpty() && this.getCaptchaResponse().equals(captchaBypass)) {
      Config.logger.warn(Config.key + ": CAPTCHA bypass accepted.");
      return true;
    }
    
    String captchaId = this.getHttpRequest().getSession().getId();

    try {
      isCaptchaCorrect = CaptchaServiceSingleton.getInstance().validateResponseForID(captchaId, this.getCaptchaResponse());
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to validate Captcha: ", e);
    }

    return isCaptchaCorrect;
  }

  // Generate captcha image.
  public String captchaImage() throws IOException {
    byte[] captchaChallengeAsJpeg = null;
    ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();

    try {
      String captchaId = getHttpRequest().getSession().getId();
      // set characters used in generating the captcha image.
      CaptchaServiceSingleton.setCaptchaConfiguration("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 18, 18, 130, 30, 6, new Color(0, 0, 0));
      BufferedImage challenge = ((GenericManageableCaptchaService) CaptchaServiceSingleton.getInstance()).getImageChallengeForID(captchaId, Locale.UK);

      // @todo add ability to create PNG or GIF
      JPEGImageEncoder jpegEncoder = JPEGCodec.createJPEGEncoder(jpegOutputStream);
      jpegEncoder.encode(challenge);

    } catch (IllegalArgumentException e) {
    } catch (CaptchaServiceException e) {
    }

    captchaChallengeAsJpeg = jpegOutputStream.toByteArray();

    this.setContentType("image/jpeg");
    this.setContentDisposition("attachment;filename=captcha.jpg");
    this.setInputStream(new ByteArrayInputStream(captchaChallengeAsJpeg));

    return SUCCESS;
  }
  /**
   * @return the contentType
   */
  public String getContentType() {
    return contentType;
  }

  /**
   * @param contentType the contentType to set
   */
  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  /**
   * @return the contentDisposition
   */
  public String getContentDisposition() {
    return contentDisposition;
  }

  /**
   * @param contentDisposition the contentDisposition to set
   */
  public void setContentDisposition(String contentDisposition) {
    this.contentDisposition = contentDisposition;
  }

  /**
   * @return the inputStream
   */
  public InputStream getInputStream() {
    return inputStream;
  }

  /**
   * @param inputStream the inputStream to set
   */
  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  /**
   * @return the captchaResponse
   */
  public String getCaptchaResponse() {
    return captchaResponse;
  }

  /**
   * @param captchaResponse the captchaResponse to set
   */
  public void setCaptchaResponse(String captchaResponse) {
    this.captchaResponse = captchaResponse;
  }
}
