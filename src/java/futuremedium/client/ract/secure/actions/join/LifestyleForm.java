package futuremedium.client.ract.secure.actions.join;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.opensymphony.xwork2.ActionContext;

import com.ract.common.GenericException;
import com.ract.membership.ProductVO;
import com.ract.util.DateTime;
import com.ract.web.membership.*;

import com.ract.web.security.MemberUser;
import com.ract.web.security.User;
import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Step 1 of the Lifestyle Join process. Prepares page for Join Lifestyle Form.
 * 
 * @author gnewton
 */
public class LifestyleForm extends RactActionSupport {

	@Override
	public String execute() throws Exception {
		setUp();

		this.setActionName(ActionContext.getContext().getName());

		User user = (User) getSession().get(USER_TOKEN);
		// deny if logged in
		if (user != null && user instanceof MemberUser) {
			this.setActionMessage("You are already a member of the RACT.");
			return ERROR;
		}

		init();

		return SUCCESS;
	}

	/**
	 * Initialises a commencement date from now to 32 days in the future.
	 * 
	 * @throws RemoteException
	 * @throws ParseException 
	 */
	private void init() throws RemoteException, ParseException {
		Calendar start = Calendar.getInstance();
		// start.add(Calendar.DATE, 2);

		Calendar end = Calendar.getInstance();
		end.add(Calendar.DATE, 32);

		List<String> dates = new ArrayList<String>();
		Calendar date = Calendar.getInstance();
		date.setTime(start.getTime());

		while (date.before(end)) {
			dates.add(df.format(date.getTime()));
			date.add(Calendar.DATE, 1);
		}

		getSession().put("commenceDates", dates);

		start.add(Calendar.YEAR, 1);
		getSession().put("endDate", RactActionSupport.df.format(start.getTime()));

		getSession().put("PRODUCT_LIFESTYLE", ProductVO.PRODUCT_LIFESTYLE);
		getSession().put("firstPage", true);

		getSession().put("MEMBERSHIP_TYPE", "Lifestyle");
		getSession().put("MEMBERSHIP_DESC_BASE", "");

		getSession().put("LINK_TYPE", "Lifestyle");

		// membership costs
		try {

			// Don't create a real transaction
			WebMembershipTransactionContainer lifestyle = roadsideMgr.createWebMembershipJoinTransaction(new DateTime(), new WebMembershipClient(), ProductVO.PRODUCT_LIFESTYLE, "1:0:0:0:0:0:0", null, false); 

			// Check for current date being in range for any Lifestyle Zero Cost promotions
//			lifestyle.setLifeStyleZero(checkForLifestyleZeroState());
			lifestyle.getPrimeAddressWebMembershipTransaction().getAmountPayable();
			
			
//			if (lifestyle.isLifeStyleZero()) {
				getSession().put("LIFESTYLE_PRICE", lifestyle.getPrimeAddressWebMembershipTransaction().getAmountPayable());
//			} else {
//				getSession().put("LIFESTYLE_PRICE", 39);
//			}

			getSession().put("lifestyleTxn", lifestyle);

		} catch (GenericException e) {
			Config.logger.error(Config.key + ": unable to acquire join fees: ", e);
		}
	}

	/**
	 * Returns true if the current date is within a zero cost promotion date range for Lifestyle
	 * 
	 * @return boolean value - true if in range and false if not
	 * @throws GenericException
	 * @throws RemoteException
	 * @throws ParseException 
	 */
//	private boolean checkForLifestyleZeroState() throws RemoteException, GenericException, ParseException {
//		// Check for lifestyle for zero date range
//		HashMap<String, String> lifestyleZeroDates = roadsideMgr.getLifestyleZeroDates();
//
//		boolean isLifestyleZero = false;
//
//		// Must match the common regex pattern for dates in the format dd{/|:|.}MM{/|:|.}yyyy
//		String dateRegex = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
//		if (lifestyleZeroDates.get("start").matches(dateRegex) && lifestyleZeroDates.get("end").matches(dateRegex)) {
//			// Compare today to start and end to determine if we are in the zero payment period
//			Date today = new Date();
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//			Date start = sdf.parse(lifestyleZeroDates.get("start"));
//			Date end = sdf.parse(lifestyleZeroDates.get("end"));
//			if ((today.compareTo(start) < 0 || today.compareTo(start) == 0) && (today.compareTo(end) > 0 || today.compareTo(end) == 0)) {
//				isLifestyleZero = true;
//			}
//		}
//
//		// Return the state
//		return isLifestyleZero;
//	}

	public String getAction() {
		return "JoinLifestyleForm";
	}
}
