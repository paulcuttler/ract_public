package futuremedium.client.ract.secure.actions.join;

import java.util.*;
import java.util.regex.Pattern;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.common.GenericException;
import com.ract.web.payment.WebPayment;
import com.ract.web.client.ClientHelper;
import com.ract.web.membership.*;

import com.ract.web.payment.WebMembershipPayment;
import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

import java.util.Random;

/**
 * 
 * @author gnewton
 */
public class Payment extends RactActionSupport {

	private static final long serialVersionUID = -1599175874064399091L;
	
	public static final String CREDIT_CARD = "CreditCard";
	public static final String BANK_ACCOUNT = "BankAccount";
	public static final String NO_CHARGE = "NoCharge";

	public static final String PAY_NOW = "PayNow";
	public static final String DIRECT_DEBIT = "DirectDebit";

	// payment details
	private String cardName;
	private String cardNumber1;
	private String cardNumber2;
	private String cardNumber3;
	private String cardNumber4;
	private String cardType;
	private String cvv;
	private String expiryDate1;
	private String expiryDate2;
	private String pdfUrl;
	private String gotcha;
	private String paymentType;
	private String paymentMethod;
	private String bsb;
	private String accountName;
	private String accountNumber;

	@Override
	public String execute() throws Exception {
		setUp();

		this.setActionName(ActionContext.getContext().getName());

		getSession().put("paymentMethod", getPaymentMethod());

		try {
			WebMembershipTransactionContainer container = (WebMembershipTransactionContainer) getSession().get("container");
			WebPayment payment = null;
			if (getPaymentMethod().equals(CREDIT_CARD)) {
				// attempt to store payment info (credit card)
				payment = new WebMembershipPayment(container.getWebMembershipTransactionHeader().getTransactionHeaderId().toString(), getCardName(), getCardType(), getCardNumber(), getCvv(), getExpiryDate(), getPaymentType());

			} else if (getPaymentMethod().equals(BANK_ACCOUNT)) {
				// attempt to store payment info (Direct Dedit)
				payment = new WebMembershipPayment(container.getWebMembershipTransactionHeader().getTransactionHeaderId().toString(), getBsb(), getAccountName(), getAccountNumber(), getPaymentType());
			}

			container.setWebPayment(payment);

			container = roadsideMgr.completeWebMembershipTransactions(container);

//			WebMembershipClient customer = (WebMembershipClient) getSession().get(CUSTOMER_TOKEN);

			// email membership advice - discontinued 14/9/2020 - APPS-296 GS
			// roadsideMgr.emailJoinMembershipAdvice(container.getWebMembershipTransactionHeader().getTransactionHeaderId(), customer.getEmailAddress());
			
			// Get the transaction interim membership number and put into session for the complete page
			WebMembershipTransaction wmt = container.getPrimeAddressWebMembershipTransaction();
			getSession().put("interimMembershipNumber", wmt.getInterimMembershipNo());

			// Put the transaction header id in the session
			getSession().put("txnHeaderId", container.getWebMembershipTransactionHeader().getTransactionHeaderId());

			// clear customer details
			getSession().remove(CUSTOMER_TOKEN);
			getSession().remove("showPostal");
			getSession().remove("container");

			Random rand = new Random();
			int randomNum = rand.nextInt((5000 - 0) + 1) + 0;

			getSession().put("nonce", Integer.toString(randomNum));

			return SUCCESS;

			// } catch (GenericException e) {
			// this.setActionMessage("An Error occurred: " + e.getMessage());
			// Config.logger.error(Config.key + "An error occurred: ", e);
			// return ERROR;
		} catch (NullPointerException e) {
			this.setActionMessage("An Error occurred: Your session has expired");
			Config.logger.warn(Config.key + ": An error occurred: session has expired: ", e);
			return "reset";
		}
	}

	@Override
	public void validate() {

		if (getPaymentMethod().equals(CREDIT_CARD)) {
			if (getCardNumber1().length() != 4 || getCardNumber2().length() != 4 || getCardNumber3().length() != 4 || getCardNumber4().length() != 4) {
				System.out.println("incorrect length");
				addFieldError("cardNumber1", "Please enter a valid credit card number.");
			} else if (!RactActionSupport.REGEXP_DIGITS.matcher(getCardNumber()).matches()) {
				System.out.println("not digits");
				addFieldError("cardNumber1", "Please enter a valid credit card number.");
			} else if (!validateCreditCardNumber(getCardNumber())) {
				System.out.println("invalid");
				addFieldError("cardNumber1", "Please enter a valid credit card number.");
			}

			if (getCardName().equals("") || getCardName() == null) {
				addFieldError("cardName", "Please supply a Card Holder's Name");
			}

			if (getCardType() == null || getCardType().equals("")) {
				addFieldError("cardType", "Please supply a Card Type");
			}

			if (getCardType().equals("MASTERCARD") && !Pattern.matches("^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$", getCardNumber())) {
				addFieldError("cardType", "It looks like you have not entered a mastercard card number? Perhaps it was a Visa card?");
			}

			if (getCardType().equals("VISA") && !Pattern.matches("^4[0-9]{6,}$", getCardNumber())) {
				addFieldError("cardType", "It looks like you have not entered a visa card number? Perhaps it was a  card?");
			}

			if (getCvv() == null || getCvv().equals("")) {
				addFieldError("cvv", "Please supply a CVV");
			} else if (getCvv().length() != 3) {
				addFieldError("cvv", "Please supply a 3 number CVV");
			}

			if (getExpiryDate1().equals("") || getExpiryDate1() == null || getExpiryDate1().equals("") || getExpiryDate1() == null) {
				addFieldError("expiryDate1", "Please supply an expiry date");
			}

			if (!RactActionSupport.REGEXP_DIGITS.matcher(getExpiryDate1()).matches() || !RactActionSupport.REGEXP_DIGITS.matcher(getExpiryDate2()).matches()) {
				addFieldError("expiryDate1", "Please enter a valid expiry date.");
			} else if (!getExpiryDate1().isEmpty() && !getExpiryDate2().isEmpty()) {
				Calendar expiryDate = Calendar.getInstance();
				expiryDate.set(Calendar.YEAR, 2000 + Integer.parseInt(getExpiryDate2()));
				expiryDate.set(Calendar.MONTH, Integer.parseInt(getExpiryDate1()) - 1); // subtract 1 from month value
				expiryDate.set(Calendar.DATE, expiryDate.getMaximum(Calendar.DAY_OF_MONTH));

				// Config.logger.debug(Config.key + ": Expiry date = " + expiryDate.getTime());

				if (expiryDate.getTime().before(new Date())) { // expiry date in the past
					addFieldError("expiryDate1", "Please enter a current expiry date.");
				}
			}
		} else if (getPaymentMethod().equals(BANK_ACCOUNT)) {
			if (getBsb() == null || getBsb().equals("")) {
				addFieldError("bsb", "Please supply a BSB number");
			} else if (getBsb().length() != 6) {
				addFieldError("bsb", "Please supply a 6 number bsb");
			}

			if (getAccountName().equals("") || getAccountName() == null) {
				addFieldError("accountName", "Please supply an Account Name");
			}

			if (getAccountNumber().equals("") || getAccountNumber() == null) {
				addFieldError("accountNumber", "Please supply a Account Number");
			}
		} else if (getPaymentMethod() != null) {
			addFieldError("paymentType", "Please select a valid payment type");
		}

		// fall through to annotation validation
	}

	/**
	 * @return the cardName
	 */
	public String getCardName() {
		return cardName;
	}

	/**
	 * @param cardName the cardName to set
	 */
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	/**
	 * @return the cardNumber1
	 */
	public String getCardNumber1() {
		return cardNumber1;
	}

	/**
	 * @param cardNumber1 the cardNumber1 to set
	 */
	public void setCardNumber1(String cardNumber1) {
		this.cardNumber1 = cardNumber1;
	}

	/**
	 * @return the cardNumber2
	 */
	public String getCardNumber2() {
		return cardNumber2;
	}

	/**
	 * @param cardNumber2 the cardNumber2 to set
	 */
	public void setCardNumber2(String cardNumber2) {
		this.cardNumber2 = cardNumber2;
	}

	/**
	 * @return the cardNumber3
	 */
	public String getCardNumber3() {
		return cardNumber3;
	}

	/**
	 * @param cardNumber3 the cardNumber3 to set
	 */
	public void setCardNumber3(String cardNumber3) {
		this.cardNumber3 = cardNumber3;
	}

	/**
	 * @return the cardNumber4
	 */
	public String getCardNumber4() {
		return cardNumber4;
	}

	/**
	 * @param cardNumber4 the cardNumber4 to set
	 */
	public void setCardNumber4(String cardNumber4) {
		this.cardNumber4 = cardNumber4;
	}

	/**
	 * Returns an aggregation of 4 card number parts.
	 * 
	 * @return
	 */
	public String getCardNumber() {
		return getCardNumber1() + getCardNumber2() + getCardNumber3() + getCardNumber4();
	}

	/**
	 * @return the expiryDate1
	 */
	public String getExpiryDate1() {
		return expiryDate1;
	}

	/**
	 * @param expiryDate the expiryDate1 to set
	 */
	public void setExpiryDate1(String expiryDate) {
		this.expiryDate1 = expiryDate;
	}

	/**
	 * @return the expiryDate2
	 */
	public String getExpiryDate2() {
		return expiryDate2;
	}

	/**
	 * @param expiryDate the expiryDate1 to set
	 */
	public void setExpiryDate2(String expiryDate) {
		this.expiryDate2 = expiryDate;
	}

	public String getExpiryDate() {
		return getExpiryDate1() + getExpiryDate2();
	}

	/**
	 * @return the cardType
	 */

	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @param bsb
	 */
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	/**
	 * BSB - the bank identifier
	 * 
	 * @return
	 */
	public String getCvv() {
		return cvv;
	}

	/**
	 * @return the pdfUrl
	 */
	public String getPdfUrl() {
		return pdfUrl;
	}

	/**
	 * @param pdfUrl the pdfUrl to set
	 */
	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}

	/**
	 * Honey pot - this field should remain blank, invalid otherwise.
	 * 
	 * @return the gotcha
	 */
	@StringLengthFieldValidator(message = "An error occurred", minLength = "0", maxLength = "0")
	public String getGotcha() {
		return gotcha;
	}

	/**
	 * @param gotcha the gotcha to set
	 */
	public void setGotcha(String gotcha) {
		this.gotcha = gotcha;
	}

	/**
	 * @param paymentType
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * Payment type - the kind of payment details provided.
	 * 
	 * @return
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 */
	@RequiredStringValidator(message = "Please supply a Payment Method")
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * Payment type - the kind of payment details provided.
	 * 
	 * @return
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param bsb
	 */
	public void setBsb(String bsb) {
		this.bsb = bsb;
	}

	/**
	 * BSB - the bank identifier
	 * 
	 * @return
	 */
	public String getBsb() {
		return bsb;
	}

	/**
	 * @param accountName
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * Account Name
	 * 
	 * @return
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * 
	 * @param accountNumber
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * 
	 * @return
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
}
