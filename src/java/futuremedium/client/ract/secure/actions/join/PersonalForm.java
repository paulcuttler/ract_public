package futuremedium.client.ract.secure.actions.join;

import com.opensymphony.xwork2.ActionContext;

import com.ract.common.GenericException;
import com.ract.web.membership.WebMembershipClient;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Step 2 of the Join process.
 * Prepares page for Personal details Form.
 * @author gnewton
 */
public class PersonalForm extends RactActionSupport {

  private String homePhoneAreaCode;
  private String homePhone;
  private String workPhoneAreaCode;
  private String workPhone;
  
@Override
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    try {
      preparePhoneNumbers();
      prepareTitles();
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + "An error occurred: ", e);
      return ERROR;
    }

    return SUCCESS;
  }

  /**
   * Utility method to split raw phone number into area code and number.
   * @throws GenericException
   */
  private void preparePhoneNumbers() throws GenericException {

    WebMembershipClient customer = (WebMembershipClient) getSession().get(CUSTOMER_TOKEN);

    if (customer != null) {      
      String rawHomePhone = customer.getHomePhone();
      if (rawHomePhone != null && !rawHomePhone.isEmpty() && rawHomePhone.length() >= 2) {
        this.setHomePhoneAreaCode(rawHomePhone.substring(0, 2));
        if (rawHomePhone.length() == 10) {
          this.setHomePhone(rawHomePhone.substring(2, 10));
        } else {
          this.setHomePhone(rawHomePhone.substring(2));
        }
      }

      String rawWorkPhone = customer.getWorkPhone();
      if (rawWorkPhone != null && !rawWorkPhone.isEmpty() && rawWorkPhone.length() >= 2) {
        this.setWorkPhoneAreaCode(rawWorkPhone.substring(0, 2));
        if (rawWorkPhone.length() == 10) {
          this.setWorkPhone(rawWorkPhone.substring(2, 10));
        } else {
          this.setWorkPhone(rawWorkPhone.substring(2));
        }
      }
    }
  }

  @SuppressWarnings("unchecked")
  private void prepareTitles() throws GenericException {
    getSession().put("titles", customerMgr.getClientTitles(true,false));
  }

  /**
   * @return the homePhoneAreaCode
   */
  public String getHomePhoneAreaCode() {
    return homePhoneAreaCode;
  }

  /**
   * @param homePhoneAreaCode the homePhoneAreaCode to set
   */
  public void setHomePhoneAreaCode(String homePhoneAreaCode) {
    this.homePhoneAreaCode = homePhoneAreaCode;
  }

  /**
   * @return the homePhone
   */
  public String getHomePhone() {
    return homePhone;
  }

  /**
   * @param homePhone the homePhone to set
   */
  public void setHomePhone(String homePhone) {
    this.homePhone = homePhone;
  }

  /**
   * @return the workPhoneAreaCode
   */
  public String getWorkPhoneAreaCode() {
    return workPhoneAreaCode;
  }

  /**
   * @param workPhoneAreaCode the workPhoneAreaCode to set
   */
  public void setWorkPhoneAreaCode(String workPhoneAreaCode) {
    this.workPhoneAreaCode = workPhoneAreaCode;
  }

  /**
   * @return the workPhone
   */
  public String getWorkPhone() {
    return workPhone;
  }

  /**
   * @param workPhone the workPhone to set
   */
  public void setWorkPhone(String workPhone) {
    this.workPhone = workPhone;
  }
  
  public String getBackAction() {
	  return "Join" + getSession().get("LINK_TYPE") + "Form"; 
  }
}
