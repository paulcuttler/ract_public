package futuremedium.client.ract.secure.actions.join;

import java.util.*;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.common.GenericException;
import com.ract.common.StreetSuburbVO;
import com.ract.web.membership.WebMembershipClient;
import com.ract.web.membership.WebMembershipTransactionContainer;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.CreateUpdateActionBase;

/**
 * @author gnewton
 */
public class Personal extends CreateUpdateActionBase {

  private boolean existing = false;

  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    try {
    	
      Object[] ks = getSession().values().toArray();
    	
      // attempt to retrieve customer from session
      WebMembershipClient customer = (WebMembershipClient) getSession().get(CUSTOMER_TOKEN);
      if (customer == null) {
        customer = new WebMembershipClient();
        existing = false;
      }
      customer.setTitle(getTitle());
      customer.setGivenNames(getFirstName().toUpperCase());
      customer.setSurname(getLastName().toUpperCase());
      customer.setEmailAddress(getEmail());
      customer.setHomePhone(getHomePhoneAreaCode() + getHomePhone());
      customer.setMobilePhone(getMobilePhone());
      customer.setWorkPhone(getWorkPhoneAreaCode() + getWorkPhone());
   	  customer.setGender(getGender());
      customer.setBirthDate(getDobDate());

      customer.setResiPostcode(getResidentialPostcode());
      customer.setResiStreet(getResidentialStreet().toUpperCase());
      customer.setResiSuburb(getResidentialSuburb().toUpperCase());
      customer.setResiProperty(getResidentialPropertyName().toUpperCase());
      customer.setResiStreetChar(getResidentialPropertyQualifier().toUpperCase());
      customer.setResiState(getResidentialState());

      // clear residential streetSuburbId
      customer.setResiStsubid(null);

      // lookup streetSuburbID and store
      StreetSuburbVO ssvo = addressMgr.getUniqueStreetSuburb(getResidentialStreet(), getResidentialSuburb(), getResidentialPostcode());
      if (ssvo != null) {
      	customer.setResiStsubid(ssvo.getStreetSuburbID());
      }

      if (!isShowPostal()) {
        setPostalPostcode(getResidentialPostcode());
        setPostalStreet(getResidentialStreet());
        setPostalSuburb(getResidentialSuburb());
        setPostalPropertyName(getResidentialPropertyName());
        setPostalPropertyQualifier(getResidentialPropertyQualifier());
        setPostalState(getResidentialState());
      }

      customer.setPostPostcode(getPostalPostcode());
      customer.setPostStreet(getPostalStreet().toUpperCase());
      customer.setPostSuburb(getPostalSuburb().toUpperCase());
      customer.setPostProperty(getPostalPropertyName().toUpperCase());
      customer.setPostStreetChar(getPostalPropertyQualifier().toUpperCase());
      customer.setPostState(getPostalState());

      // clear postal streetSuburbId
      customer.setPostStsubid(null);

      // lookup streetSuburbID and store
      ssvo = addressMgr.getUniqueStreetSuburb(getPostalStreet(), getPostalSuburb(), getPostalPostcode());
      if (ssvo != null) {
      	customer.setPostStsubid(ssvo.getStreetSuburbID());
      }

      if (existing) {
        customer = roadsideMgr.updateMembershipClient(customer);
      } else {
        customer = roadsideMgr.createMembershipClient(customer);
      }
  
      
      WebMembershipTransactionContainer wmtc = (WebMembershipTransactionContainer) getSession().get("container");
      
      
      if (!useStub) {
        roadsideMgr.associateWebClient(wmtc, customer);
      }

      // store txn in session
      getSession().put("container", wmtc);
     
      
      // store membership client in session
      getSession().put(CUSTOMER_TOKEN, customer);

      getSession().put("showPostal", isShowPostal());

      return SUCCESS;

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + "An error occurred: ", e);
      return ERROR;

    } catch (NullPointerException e) {
      this.setActionMessage("An Error occurred: Your session has expired");
      Config.logger.warn(Config.key + "An error occurred: session has expired: ", e);
      return "reset";
    }
  }

}
