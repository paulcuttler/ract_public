package futuremedium.client.ract.secure.actions.join;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.sql.DataSource;

import com.opensymphony.xwork2.ActionContext;

import com.ract.client.Client;
import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.common.SourceSystem;
import com.ract.membership.ProductVO;
import com.ract.security.SecurityHelper;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.web.membership.*;

import com.ract.web.security.MemberUser;
import com.ract.web.security.User;
import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.entities.ProductService;

/**
 * Step 1 of the Join process. Prepares page for Join Membership Form.
 * 
 * @author gnewton
 */
public class MembershipForm extends RactActionSupport {

	private static final long serialVersionUID = -1006643338320017133L;

	@Resource(mappedName = "java:/ClientDS")
	private DataSource dataSource;

	public String product;

	@Override
	public String execute() throws Exception {
		setUp();

		this.setActionName(ActionContext.getContext().getName());

		User user = (User) getSession().get(USER_TOKEN);
		// deny if logged in
		if (user != null && user instanceof MemberUser) {
			this.setActionMessage("You are already a member of the RACT.");
			return ERROR;
		}

		init();

		return SUCCESS;
	}

	/**
	 * Initialises a commencement date from now to 32 days in the future.
	 */
	@SuppressWarnings("unchecked")
	private void init() {
		System.out.println(product);

		Calendar start = Calendar.getInstance();
		// start.add(Calendar.DATE, 2);

		Calendar end = Calendar.getInstance();
		end.add(Calendar.DATE, 32);

		List<String> dates = new ArrayList<String>();
		Calendar date = Calendar.getInstance();
		date.setTime(start.getTime());

		while (date.before(end)) {
			dates.add(df.format(date.getTime()));
			date.add(Calendar.DATE, 1);
		}

		getSession().put("commenceDates", dates);

		start.add(Calendar.YEAR, 1);
		getSession().put("endDate", RactActionSupport.df.format(start.getTime()));

		getSession().put("PRODUCT_ULTIMATE", ProductVO.PRODUCT_ULTIMATE);
		getSession().put("PRODUCT_ADVANTAGE", ProductVO.PRODUCT_ADVANTAGE);
		getSession().put("firstPage", true);

		getSession().put("MEMBERSHIP_TYPE", "RACT Roadside");
		getSession().put("MEMBERSHIP_DESC_BASE", "RACT Roadside");
		getSession().put("LINK_TYPE", "Membership");

		//
		// Changed the way that pricing is changed. As there is going to be no joining fee,
		// we will now lookup the price from a config file
		//

		if (product != null) {
			getSession().put("PRODUCT_SERVICE", new ProductService(product));
		} else {
			getSession().put("PRODUCT_SERVICE", new ProductService("Ultimate", "Yearly"));
		}

		// membership costs
		try {
			WebMembershipTransactionContainer advantage = roadsideMgr.createWebMembershipJoinTransaction(new DateTime(), new WebMembershipClient(), ProductVO.PRODUCT_ADVANTAGE, ((ProductService) getSession().get("PRODUCT_SERVICE")).getTermLength(), null, false); // don't create a real transaction

			// Never subject to a zero cost state (and not Lifestyle)
			// advantage.setLifeStyleZero(false);

			getSession().put("advantageTxn", advantage);

			WebMembershipTransactionContainer ultimate = roadsideMgr.createWebMembershipJoinTransaction(new DateTime(), new WebMembershipClient(), ProductVO.PRODUCT_ULTIMATE, ((ProductService) getSession().get("PRODUCT_SERVICE")).getTermLength(), null, false); // don't create a real transaction

			// Never subject to a zero cost state (and not Lifestyle)
			// ultimate.setLifeStyleZero(false);

			getSession().put("ultimateTxn", ultimate);

			/* Get monthly pricing for Ultimate from db and yearly from session */
			DecimalFormat df = new DecimalFormat("#.00"); 
			
			getSession().put("monthlyUltimatePrice", df.format(getMonthlyPrice("Ultimate", "monthly").doubleValue()));
			getSession().put("monthlyUltimateFirstPrice", df.format(getMonthlyPrice("Ultimate", "monthlyfp").doubleValue()));
			getSession().put("yearlyUltimatePrice", getYearlyPrice(ultimate));

			/* Get monthly pricing for Advantage from db and yearly from session */
			getSession().put("monthlyAdvantagePrice", df.format(getMonthlyPrice("Advantage", "monthly").doubleValue()));
			getSession().put("monthlyAdvantageFirstPrice", df.format(getMonthlyPrice("Advantage", "monthlyfp").doubleValue()));
			getSession().put("yearlyAdvantagePrice", getYearlyPrice(advantage));

		} catch (GenericException e) {
			Config.logger.error(Config.key + ": unable to acquire join fees: ", e);
		}
	}

	// private String getMonthlyFirstPrice(WebMembershipTransactionContainer wmtc) {
	// BigDecimal yd = wmtc.getWebMembershipTransactionHeader().getAmountPayable();
	// BigDecimal wbd = yd.divide(new BigDecimal(12), BigDecimal.ROUND_HALF_UP);
	//
	// MathContext mc;
	// if(wbd.compareTo(new BigDecimal(10)) < 0) {
	// mc = new MathContext(2);
	// } else {
	// mc = new MathContext(3);
	// }
	//
	// wbd = wbd.setScale(2, RoundingMode.FLOOR);
	// wbd = wbd.round(mc);
	// BigDecimal ybd = wbd.multiply(new BigDecimal(12));
	// BigDecimal diff = yd.subtract(ybd);
	// String r = diff.add(wbd).toString();
	// return r;
	// }

	// public String getMonthlyPrice(WebMembershipTransactionContainer wmtc) {
	// BigDecimal yd = wmtc.getWebMembershipTransactionHeader().getAmountPayable();
	// BigDecimal wbd = yd.divide(new BigDecimal(12), BigDecimal.ROUND_HALF_UP);
	//
	// MathContext mc;
	// if(wbd.compareTo(new BigDecimal(10)) < 0) {
	// mc = new MathContext(2);
	// } else {
	// mc = new MathContext(3);
	// }
	// wbd = wbd.setScale(2, RoundingMode.FLOOR);
	// wbd = wbd.round(mc);
	// wbd = wbd.setScale(2, RoundingMode.FLOOR);
	// return wbd.toString();
	// }

	public String getAction() {
		return "JoinMembershipForm";
	}

	public String getYearlyPrice(WebMembershipTransactionContainer wmtc) {
		String s = wmtc.getWebMembershipTransactionHeader().getAmountPayable().toString();
		return s;
	}

	/**
	 * getMonthlyPrice
	 * <hr/>
	 * <p>
	 * Obtain the indicated price the RoadsidePrice table
	 * </p>
	 * 
	 * @param product {String} the product
	 * @param type {String} the price type
	 * @return
	 */
	private BigDecimal getMonthlyPrice(String product, String type) {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		BigDecimal price = new BigDecimal(0);
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement("select Price from RoadsidePrice where Product = '" + product + "' and PriceType = '" + type + "' and ValidFrom <= GETDATE() and ExpiryDate > GETDATE()");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				price = new BigDecimal(resultSet.getDouble(1));
			}
			resultSet.close();

		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return price;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String p) {
		product = p;
	}
}
