package futuremedium.client.ract.secure.actions.join;

import java.util.*;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.web.membership.*;
import futuremedium.client.ract.secure.Config;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Process the Membership Form.
 * @author gnewton
 */
public class Membership extends RactActionSupport {

  private String startDate;
  private String endDate;
  private String product;
  private String gotcha;

  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    WebMembershipClient customer = (WebMembershipClient) getSession().get(CUSTOMER_TOKEN);
    if (customer == null) {
      customer = new WebMembershipClient();
    }

    try {
      String[] pandt = product.split("-");
      
      getSession().put("productName", pandt[0]);
      getSession().put("termLength", getTermIntervalString(pandt[1]));
      getSession().put("selectedFirstInstallment", getSelectedFirstInstallment(pandt[0], pandt[1]));
      getSession().put("selectedSubsequentInstallment", getSelectedSubsequentInstallment(pandt[0], pandt[1]));
      
      List<String> dates = (List<String>) getSession().get("commenceDates");
      setStartDate(dates.get(0));
      getSession().put("startDate", startDate);
      
      // determine end date in one year's time
      Calendar now = Calendar.getInstance();
      now.setTime(RactActionSupport.df.parse(startDate));
      now.add(Calendar.YEAR, 1);

      getSession().put("endDate", RactActionSupport.df.format(now.getTime()));
      getSession().put("firstPage", false);
      
      WebMembershipTransactionContainer wmtc = (WebMembershipTransactionContainer) getSession().get("container");
      if (wmtc != null) {
        wmtc = roadsideMgr.updateWebMembershipJoinTransactionContainer(
              wmtc.getWebMembershipTransactionHeader().getTransactionHeaderId(),
              new DateTime((String) getSession().get("startDate")),
              customer,
              (String) getSession().get("productName"),
              (String) getSession().get("termLength"),
              null,
              true);
      } else {
        wmtc = roadsideMgr.createWebMembershipJoinTransaction(
              new DateTime((String) getSession().get("startDate")),
              customer,
              (String) getSession().get("productName"),
              (String) getSession().get("termLength"),
              null,
              true);
      }

      roadsideMgr.associateWebClient(wmtc, customer);

      // store txn in session
      getSession().put("container", wmtc);

      // record user session
      recordSession(null, RactActionSupport.CONTEXT_JOIN);
      
      return SUCCESS;

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + "An error occurred: ", e);
      return ERROR;
    }

  }
  
  	private String getSelectedSubsequentInstallment(String product, String term) {
  		String t = term.toLowerCase(); 
  		String key = t + product + "Price";
  		return (String) getSession().get(key);
	}
	
	private String getSelectedFirstInstallment(String product, String term) {
		String t = term.toLowerCase(); 
  		String key = t + product + "FirstPrice";
  		return (String) getSession().get(key);
	}

public String getTermIntervalString(String term) {
	  if(term.equals("Monthly")) {
		  return "0:12:0:0:0:0:0";
	  } else {
		  return "1:0:0:0:0:0:0";
	  }
  }

  /**
   * @return the startDate
   */
  //@RequiredStringValidator(message = "Please supply a Start Date")
  public String getStartDate() {
    return startDate;
  }

  /**
   * @param startDate the startDate to set
   */
  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  /**
   * @return the product
   */
  @RequiredStringValidator(message = "Please select a Membership Type")
  public String getProduct() {
    return product;
  }

  /**
   * @param product the product to set
   */
  public void setProduct(String product) {
    this.product = product;
  }

  /**
   * Honey pot - this field should remain blank, invalid otherwise.
   * @return the gotcha
   */
  @StringLengthFieldValidator(message = "An error occurred", minLength = "0", maxLength = "0")
  public String getGotcha() {
    return gotcha;
  }

  /**
   * @param gotcha the gotcha to set
   */
  public void setGotcha(String gotcha) {
    this.gotcha = gotcha;
  }

  /**
   * @return the endDate
   */
  public String getEndDate() {
    return endDate;
  }

  /**
   * @param endDate the endDate to set
   */
  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }
}
