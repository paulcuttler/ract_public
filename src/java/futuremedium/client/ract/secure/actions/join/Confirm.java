/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package futuremedium.client.ract.secure.actions.join;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Random;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.ract.web.membership.WebMembershipClient;
import com.ract.web.membership.WebMembershipTransaction;
import com.ract.web.membership.WebMembershipTransactionContainer;
import com.ract.web.payment.WebMembershipPayment;
import com.ract.web.payment.WebPayment;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * 
 * @author gnewton
 */
public class Confirm extends RactActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5624112063915746093L;
	private boolean terms;
	private static String SUCCESS_LIFESTYLE_ZERO = "successLifestyleZero";

	@SuppressWarnings("deprecation")
	@Override
	public String execute() throws Exception {
		setUp();

		this.setActionName(ActionContext.getContext().getName());

		try {
//			WebMembershipClient customer = (WebMembershipClient) getSession().get(CUSTOMER_TOKEN);

			WebMembershipTransactionContainer wmtc = (WebMembershipTransactionContainer) getSession().get("container");
			BigDecimal amountPayable = wmtc.getPrimeAddressWebMembershipTransaction().getAmountPayable();
			
//			if (customer == null) {
//				Config.logger.warn(Config.key + ": An error occurred: customer is null");
//			}
//
//			if (wmtc == null) {
//				Config.logger.warn(Config.key + ": An error occurred: WMTC is null");
//			}
			
			// Check if this is a Lifestyle Zero transaction - (Lifestyle for Zero promotion)
//			boolean isLifestyleZero = wmtc.isLifeStyleZero();
			
			if (amountPayable.equals(new BigDecimal(0))) {
				// Is is a LifestyleZero promo - set up a web payment transaction with no charge
				WebPayment payment = new WebMembershipPayment(wmtc.getWebMembershipTransactionHeader().getTransactionHeaderId().toString(), Payment.NO_CHARGE);
				
				// Complete the transaction
				wmtc.setWebPayment(payment);
				wmtc = roadsideMgr.completeWebMembershipTransactions(wmtc);

				// email membership advice - discontinued 14/9/2020 - APPS-296 GS
				// roadsideMgr.emailJoinMembershipAdvice(wmtc.getWebMembershipTransactionHeader().getTransactionHeaderId(), customer.getEmailAddress());
				
				// Get the transaction interim membership number and put into session for the complete page
				WebMembershipTransaction wmt = wmtc.getPrimeAddressWebMembershipTransaction();
				getSession().put("interimMembershipNumber", wmt.getInterimMembershipNo());

				getSession().put("txnHeaderId", wmtc.getWebMembershipTransactionHeader().getTransactionHeaderId());

				// clear customer details
				getSession().remove(CUSTOMER_TOKEN);
				getSession().remove("showPostal");
				getSession().remove("container");

				Random rand = new Random();
				int randomNum = rand.nextInt((5000 - 0) + 1) + 0;

				getSession().put("nonce", Integer.toString(randomNum));
				getSession().put("free", "true");
				
				// Skip the payment page altogether
				return SUCCESS_LIFESTYLE_ZERO;
			} else {
				// Its not so go to the payment method page as usual
				getSession().put("container", wmtc);

				this.setActionName(ActionContext.getContext().getName());
				getSession().put("free", "false");

				return SUCCESS;
			}
			/*
			 * } catch (GenericException e) { this.setActionMessage("An Error occurred: " + e.getMessage()); return ERROR;
			 */

		} catch (NullPointerException e) {
			this.setActionMessage("An Error occurred: Your session has expired");
			Config.logger.warn(Config.key + ": An error occurred: session has expired: ", e);
			return "reset";
		}
	}

	/**
	 * @return the terms
	 */
	@FieldExpressionValidator(message = "You must agree to the terms and conditions", expression = "terms==true", fieldName = "terms")
	public boolean getTerms() {
		return terms;
	}

	/**
	 * @param terms the terms to set
	 */
	public void setTerms(boolean terms) {
		this.terms = terms;
	}
}
