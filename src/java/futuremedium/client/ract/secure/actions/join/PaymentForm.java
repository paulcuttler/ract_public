package futuremedium.client.ract.secure.actions.join;

import java.util.*;

import com.opensymphony.xwork2.ActionContext;

import com.ract.common.GenericException;
import com.ract.web.membership.WebMembershipTransaction;
import com.ract.web.membership.WebMembershipTransactionContainer;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Step 3 of the Join process.
 * Prepares page for Join Payment Form.
 * @author gnewton
 */
public class PaymentForm extends RactActionSupport {


  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();
    setupPaymentMethods();
    
    getSession().put("cardTypeList", getCardTypeList());
    //getSession().put("membershipType", getMembershipType());
    
    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }

  /**
   * @return the cardTypeList
   */
  private Collection<String> getCardTypeList() {
    try {
      return roadsideMgr.getMembershipPaymentTypes();
    } catch (GenericException ex) {
      Config.logger.error(Config.key + ": could not retrieve card type list: ", ex);
      return null;
    }
  }

  /**
   * Determine the membership type from recorded transaction.
   * e.g. Personal Ultimate, Lifestyle and so on
   * @return the membershipType
   */
  private String getMembershipType() {
    WebMembershipTransactionContainer container = (WebMembershipTransactionContainer) getSession().get("container");
    Iterator i = container.getTransactionList().iterator();
    while (i.hasNext()) {
      WebMembershipTransaction t = (WebMembershipTransaction) i.next();
      return t.getMembershipType() + " " + t.getProductCode();
    }
    return "";
  }
  
  public void setupPaymentMethods() {
	  String productType = (String) getSession().get("productName");
	  
	  HashMap<String, String> list = new HashMap<String, String>();
	  
      list.put(Payment.CREDIT_CARD, "Credit Card");
	  list.put(Payment.BANK_ACCOUNT, "Bank Account");

	  getSession().put("paymentMethodList", list);
  }
 

}