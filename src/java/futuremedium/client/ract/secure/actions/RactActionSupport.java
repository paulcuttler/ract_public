package futuremedium.client.ract.secure.actions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsStatics;
import org.apache.struts2.interceptor.SessionAware;
import org.json.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.web.address.AddressMgrRemote;
import com.ract.web.address.test.AddressMgrStub;
import com.ract.web.campaign.CampaignEntry;
import com.ract.web.campaign.CampaignMgrRemote;
import com.ract.web.client.ClientHelper;
import com.ract.web.client.ClientTransaction;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.client.test.CustomerMgrStub;
import com.ract.web.insurance.WebInsGlMgrRemote;
import com.ract.web.insurance.WebInsMgrRemote;
import com.ract.web.insurance.test.WebInsGlMgrStub;
import com.ract.web.insurance.test.WebInsMgrStub;
import com.ract.web.membership.WebMembershipMgrRemote;
import com.ract.web.membership.test.WebMembershipMgrStub;
import com.ract.web.security.MemberUser;
import com.ract.web.security.User;
import com.ract.web.security.UserSecurityMgrRemote;
import com.ract.web.security.test.UserSecurityMgrStub;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.user.ChangeDetail;
import futuremedium.client.ract.secure.actions.user.TippingCreate;
import futuremedium.client.ract.secure.entities.AddressService;
import futuremedium.client.ract.secure.entities.QasService;

// TODO: Auto-generated Javadoc
/**
 * Base action common to all RACT actions.
 * @author gnewton
 */
public class RactActionSupport extends ActionSupport implements SessionAware {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -887136872265078061L;
  
  /** The Constant DOC_MEMBERSHIP. */
  static public final String DOC_MEMBERSHIP = "mem";
  
  /** The Constant DOC_INSURANCE. */
  static public final String DOC_INSURANCE = "ins";
	
  /** The Constant TOKEN_DOC_QUOTE_NO. */
  static public final String TOKEN_DOC_QUOTE_NO = "docQuoteNo";
  
  /** The user token. */
  // session tokens
  static public String USER_TOKEN = "USER_TOKEN";
  
  /** The customer token. */
  static public String CUSTOMER_TOKEN = "CUSTOMER_TOKEN";
  
  /** The customer tmp token. */
  static public String CUSTOMER_TMP_TOKEN = "CUSTOMER_TMP_TOKEN";
  
  /** The member token. */
  static public String MEMBER_TOKEN = "MEMBER_TOKEN";
  
  /** The banner token. */
  static public String BANNER_TOKEN = "BANNER_TOKEN";
  
  /** The Constant NOTICE. */
  static public final String NOTICE = "notice";
  
  /** The Constant JSON. */
  static public final String JSON = "json";
  
  /** The Constant ACTION_REDIRECT. */
  static public final String ACTION_REDIRECT = "actionRedirect";

  /** The Constant STATE_TAS. */
  static protected final String STATE_TAS = "TAS";
  
  /** The Constant ACTION_MESSAGE_TOKEN. */
  static public final String ACTION_MESSAGE_TOKEN = "ACTION_MESSAGE";
  
  /** The Constant ACTION_RESULT_TOKEN. */
  static private final String ACTION_RESULT_TOKEN = "ACTION_RESULT";
  
  /** The Constant TOKEN_START_TIME. */
  static public final String TOKEN_START_TIME = "START_TIME";
  
  /** The context tipping. */
  static public String CONTEXT_TIPPING = "tipping";
  
  /** The context member. */
  static public String CONTEXT_MEMBER = "member";
  
  /** The context join. */
  static public String CONTEXT_JOIN = "join";
  
  /** The context vehicle quote. */
  static public String CONTEXT_VEHICLE_QUOTE = "motor";
  
  /** The context home quote. */
  static public String CONTEXT_HOME_QUOTE = "homeContents";

  /** The Constant REGEXP_DIGITS. */
  static final public Pattern REGEXP_DIGITS = Pattern.compile("^\\d*$");
  
  /** The Constant REGEXP_TAS_POSTCODE. */
  static final public Pattern REGEXP_TAS_POSTCODE = Pattern.compile("^7\\d{3}$");
  
  /** The Constant REGEXP_MEMBER_CARD. */
  static final public Pattern REGEXP_MEMBER_CARD = Pattern.compile("^308407\\d{10}$");
  
  /** The Constant REGEXP_EMAIL. */
  static final public Pattern REGEXP_EMAIL = Pattern.compile("^[A-Za-z0-9_\\-\\.\\&\\']+@[A-Za-z0-9_-]+\\.[A-Za-z0-9_-]+.*$");
  
  /** The Constant REGEXP_NAME. */
  static final public Pattern REGEXP_NAME = Pattern.compile("^[A-Za-z \\-\\']*$");
  
  /** The Constant REGEXP_USERNAME. */
  static final public Pattern REGEXP_USERNAME = Pattern.compile("^[A-Za-z0-9_]*$");

  /** The df. */
  static public DateFormat df = new SimpleDateFormat("d/M/yyyy");
  
  /** The expanded df. */
  static public DateFormat expandedDf = new SimpleDateFormat("dd/MM/yyyy");
  
  /** The dtf. */
  static public DateFormat dtf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
  
  /** The page title. */
  private String pageTitle;
  
  /** The action name. */
  private String actionName;
  
  /** The base url. */
  private String baseUrl = Config.configManager.getProperty("ract.secure.baseUrl");
  
  /** The use stub. */
  protected boolean useStub = Boolean.parseBoolean(Config.configManager.getProperty("ract.secure.stub", "false"));
  
  /** The user mgr. */
  // EJBs from which API calls are made
  protected UserSecurityMgrRemote userMgr;
  
  /** The address mgr. */
  protected AddressMgrRemote addressMgr;
  
  /** The customer mgr. */
  protected CustomerMgrRemote customerMgr;
  
  /** The roadside mgr. */
  protected WebMembershipMgrRemote roadsideMgr;
  
  /** The insurance mgr. */
  protected WebInsMgrRemote insuranceMgr;
  
  /** The glasses mgr. */
  protected WebInsGlMgrRemote glassesMgr;
  
  /** The namespace. */
  private String namespace = Config.configManager.getProperty("ract.secure.fakeNamespace");
  
  /** The tipping enabled. */
  private boolean tippingEnabled = Boolean.parseBoolean(Config.configManager.getProperty("ract.public.tipping.enabled", "false"));
  
  /** The development server. */
  private boolean developmentServer = Boolean.parseBoolean(Config.configManager.getProperty("configManager.server.development", "false"));
  
  /** The tipping redirect url. */
  private String tippingRedirectUrl = Config.configManager.getProperty("ract.secure.tipping.redirectAbsUrl");
  
  /** The sso login logout url. */
  private String ssoLoginLogoutUrl = Config.configManager.getProperty("futuremedium.sso.loginlogoutUrl");
  
  /** The tipping terms url. */
  private String tippingTermsUrl = Config.configManager.getProperty("ract.public.tipping.terms");
  
  /** The fsg url. */
  private String fsgUrl = Config.configManager.getProperty("insurance.fsg.url");
  
  /** The pds url. */
  private String pdsUrl = Config.configManager.getProperty("insurance.pds.url");

  /** The pds investor. */
  private String pdsInvestor = Config.configManager.getProperty("insurance.home.pds.investor");
  
  /** The pds home contents. */
  private String pdsHomeContents = Config.configManager.getProperty("insurance.home.pds.homeContents");
  
  /** The pds contents calculator. */
  private String pdsContentsCalculator = Config.configManager.getProperty("insurance.home.pds.contentsCalculator");

  /** The membership competition. */
  private String membershipCompetition = Config.configManager.getProperty("ract.public.membershipcompetition");
  
  /** The discount msg. */
  private String discountMsg = null;
  
  /** The discount range. */
  private String discountRange = Config.configManager.getProperty("insurance.car.discount.range");
  
  /** The bcp mode. */
  private boolean bcpMode = Boolean.parseBoolean(Config.configManager.getProperty("insurance.bcpMode", "false"));
    
  /** The campaign active. */
  private Boolean campaignActive = null;
  
  /** The campaign friendly name. */
  private String campaignFriendlyName = "";
  
  /** The start time. */
  private Long startTime;

  /** The ajax context. */
  private boolean ajaxContext;
  
  /** The json context. */
  private boolean jsonContext;
  
  /** The session. */
  protected Map<String, Object> session;

  /** The ctx. */
  protected InitialContext ctx;
  
  /** The address service. */
  private AddressService addressService;
  
  /** The qas service. */
  private QasService qasService;

  /** The ip address lookup service url. */
  private String ipAddressLookupServiceUrl = Config.configManager.getProperty("insurance.ip.address.lookup.service.url");
  
  /** The ip address lookup service username. */
  private String ipAddressLookupServiceUsername = Config.configManager.getProperty("insurance.ip.address.lookup.service.username");
  
  /** The ip address lookup service password. */
  private String ipAddressLookupServicePassword = Config.configManager.getProperty("insurance.ip.address.lookup.service.password");
  
  /**
     * Instantiates a new ract action support.
     */
  public RactActionSupport() {
  }

  /**
     * Sets the up.
     * 
     * @throws Exception
     *             the exception
     * @deprecated setup EJBs. Use @BaseService methods instead.
     */
  public void setUp() throws Exception {

    if (!useStub) {
      userMgr = (UserSecurityMgrRemote) this.getContext().lookup("UserSecurityMgr/remote");
      insuranceMgr = (WebInsMgrRemote) this.getContext().lookup("WebInsMgr/remote");
      glassesMgr = (WebInsGlMgrRemote) this.getContext().lookup("WebInsGlMgr/remote");
      addressMgr = (AddressMgrRemote) this.getContext().lookup("AddressMgr/remote");
      customerMgr = (CustomerMgrRemote) this.getContext().lookup("CustomerMgr/remote");
      roadsideMgr = (WebMembershipMgrRemote) this.getContext().lookup("WebMembershipMgr/remote");
    } else {
      userMgr = new UserSecurityMgrStub();
      insuranceMgr = new WebInsMgrStub();
      glassesMgr = new WebInsGlMgrStub();
      addressMgr = new AddressMgrStub();
      customerMgr = new CustomerMgrStub();
      roadsideMgr = new WebMembershipMgrStub();
    }
    
    
  }

  /**
     * Gets the context.
     * 
     * @return the context
     * @throws Exception
     *             the exception
     */
  protected InitialContext getContext() throws Exception {
    if(this.ctx == null) {
      this.ctx = new InitialContext(Config.configManager.getConfiguration().getProperties("java.naming"));
    }
    return this.ctx;
  }

  /**
     * Gets the customer mgr.
     * 
     * @return the customer mgr
     */
  protected CustomerMgrRemote getCustomerMgr() {
      if(this.customerMgr == null) {
          try {
              this.setUp();
          } catch(Exception e) {
              Config.logger.fatal(Config.key + ": Unable to init roadsideMgr: ", e);
          }
      }
      return this.customerMgr;
  }

  /**
     * Gets the roadside mgr.
     * 
     * @return the roadside mgr
     */
  protected WebMembershipMgrRemote getRoadsideMgr() {
      if(this.roadsideMgr == null) {
          try {
              this.setUp();
          } catch(Exception e) {
              Config.logger.fatal(Config.key + ": Unable to init roadsideMgr: ", e);
          }
      }
      return this.roadsideMgr;
  }
  
  /**
     * Gets the insurance mgr.
     * 
     * @return the insurance mgr
     */
  protected WebInsMgrRemote getInsuranceMgr() {
      if(this.insuranceMgr == null) {
          try {
              this.setUp();
          } catch(Exception e) {
              Config.logger.fatal(Config.key + ": Unable to init insuranceMgr: ", e);
          }
      }
      return this.insuranceMgr;
  }
  
  /**
     * Gets the address mgr.
     * 
     * @return the address mgr
     */
  protected AddressMgrRemote getAddressMgr() {
      if(this.addressMgr == null) {
          try {
              this.setUp();
          } catch(Exception e) {
              Config.logger.fatal(Config.key + ": Unable to init addressMgr: ", e);
          }
          
      }
      return this.addressMgr;
  } 
  
  /**
     * Gets the glasses mgr.
     * 
     * @return the glasses mgr
     */
  protected WebInsGlMgrRemote getGlassesMgr() {
      if(this.glassesMgr == null) {
          try {
              this.setUp();
          } catch(Exception e) {
              Config.logger.fatal(Config.key + ": Unable to init glassesMgr: ", e);
          }
          
      }
      return this.glassesMgr;
  } 

  /* (non-Javadoc)
   * @see com.opensymphony.xwork2.ActionSupport#execute()
   */
  @Override
  public String execute() throws Exception {

    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }

  /**
     * Returns a ClientTransaction based on clientNumber.
     * 
     * @param clientNumber
     *            the client number
     * @return ClientTransaction the most recently recorded customer update, or
     *         the RACT client.
     * @throws GenericException
     *             the generic exception
     */
  public ClientTransaction getCustomer(int clientNumber) throws GenericException {
    ClientVO customer;

    if (useStub) {
      return (ClientTransaction) getSession().get(CUSTOMER_TOKEN);
    }

    ClientTransaction trans = customerMgr.getLastClientTransaction(clientNumber, ClientTransaction.TRANSACTION_TYPE_UPDATE);
    if (trans != null) {
      customer = ClientHelper.convertClientTransaction(trans);
    } else {
      customer = customerMgr.getClient(clientNumber);
    }

    customer = cleanValues(customer);

    return ClientHelper.convertClient(customer);
  }

  /**
     * Remove helper values from phone numbers and email address. i.e. convert
     * "Not applicable", "To follow", "Refused" etc. to empty string.
     * 
     * @param customer
     *            the customer
     * @return the client vo
     */
  private ClientVO cleanValues(ClientVO customer) {    
    String invalidValuesString = Config.configManager.getProperty("member.invalid.values", ",");

    List<String> invalidValues = new ArrayList<String>();
    invalidValues.addAll(Arrays.asList(invalidValuesString.split(",")));

    for (String invalid : invalidValues) {
      if (customer.getHomePhone() != null && customer.getHomePhone().trim().equalsIgnoreCase(invalid)) {
        try {
          customer.setHomePhone("");
        } catch (Exception e) {}
      }
      if (customer.getWorkPhone() != null && customer.getWorkPhone().trim().equalsIgnoreCase(invalid)) {
        try {
          customer.setWorkPhone("");
        } catch (Exception e) {}
      }
      if (customer.getMobilePhone() != null && customer.getMobilePhone().trim().equalsIgnoreCase(invalid)) {
        try {
          customer.setMobilePhone("");
        } catch (Exception e) {}
      }
      if (customer.getEmailAddress() != null && customer.getEmailAddress().trim().equalsIgnoreCase(invalid)) {
        try {
          customer.setEmailAddress("");
        } catch (Exception e) {}
      }
    }

    return customer;  	
  }

  /**
     * Remote call to create an account on the Tipping platform.
     * 
     * @param u
     *            User to associate tipping with if successful.
     * @return String containing any error messages.
     * @throws GenericException
     *             the generic exception
     */
  protected String createTippingAccount(User u) throws GenericException {
    String extraMsg = "";
    String responseString = retrieveHtml(Config.configManager.getProperty("futuremedium.sso.targetUrl"),
            "entity=user&command=requestLogin"
            + "&username=" + u.getUserName()
            + "&email=" + u.getEmailAddress()
            + "&dob=" + u.getBirthDate()
            + "&surname=" + u.getSurname()
            + "&team=" + u.getAflTeam());

    if (responseString != null && responseString.contains(";")) {
      String msgs[] = responseString.split(";");
      if (!msgs[0].contains("true")) {
        throw new GenericException("Unable associate your account with the Tipping platform: " + (msgs.length == 2 ? msgs[1] : "Unable to communicate with server."));
      } else {
        // set footyTipping=true now that we know account has been created
        u.setFootyTipping(true);
        userMgr.updateUserAccount(u);
      }
    } else {
      throw new GenericException("Unable associate your account with the Tipping platform: No response from server");
    }

    return extraMsg;
  }

  /**
     * Remote call to disable an account on the Tipping platform.
     * 
     * @param u
     *            User to disassociate tipping with if successful.
     * @return String containing any error messages.
     * @throws GenericException
     *             the generic exception
     */
  protected String disableTippingAccount(User u) throws GenericException {
    String extraMsg = "";
    String responseString = retrieveHtml(Config.configManager.getProperty("futuremedium.sso.targetUrl"),
            "entity=user&command=disable" + "&username=" + u.getUserName());

    if (responseString != null && responseString.contains(";")) {
      String msgs[] = responseString.split(";");
      if (!msgs[0].contains("true")) {
        throw new GenericException("Unable remove your account from the Tipping platform: " + (msgs.length == 2 ? msgs[1] : "Unable to communicate with server."));
      } else {
        // set footyTipping=false now that we know account has been created
        u.setFootyTipping(false);
        userMgr.updateUserAccount(u);
      }
    } else {
      throw new GenericException("Unable remove your account from the Tipping platform: Unable to communicate with server");
    }

    return extraMsg;
  }

  /**
     * Remote call to check that the supplied username and email are available
     * on the tipping server.
     * 
     * @param username
     *            username to check for.
     * @param email
     *            email to check for.
     * @return false if email is used or cannot communicate with server, true
     *         otherwise.
     * @throws GenericException
     *             the generic exception
     */
  protected boolean tippingUsernameEmailAvailable(String username, String email) throws GenericException {
    boolean success = false;
    String responseString = retrieveHtml(Config.configManager.getProperty("futuremedium.sso.targetUrl"),
            "entity=user&command=checkUsernameEmail" + "&username=" + username + "&email=" + email);

    if (responseString != null && responseString.contains(";")) {
      String msgs[] = responseString.split(";");
      if (msgs[0].contains("true")) {
        success = true;
      } else {
        throw new GenericException(msgs.length == 2 ? msgs[1] : "Unable to communicate with server.");
      }
    }

    return success;
  }

  /**
     * Remote call to check that the supplied email is available on the tipping
     * server.
     * 
     * @param username
     *            the username
     * @param email
     *            the email
     * @return false if email is used or cannot communicate with server, true
     *         otherwise
     */
  protected boolean tippingEmailAvailable(String username, String email) {
    boolean success = false;
    String responseString = retrieveHtml(Config.configManager.getProperty("futuremedium.sso.targetUrl"),
            "entity=user&command=checkEmail"
            + "&username=" + username
            + "&email=" + email);

    if (responseString != null && responseString.contains(";")) {
      String msgs[] = responseString.split(";");
      if (msgs[0].contains("true")) {
        success = true;
      }
    }

    return success;
  }

  /* (non-Javadoc)
   * @see org.apache.struts2.interceptor.SessionAware#setSession(java.util.Map)
   */
  @Override
  public void setSession(Map<String, Object> session) {
    this.session = session;
  }

  /**
     * Gets the session.
     * 
     * @return the session
     */
  public Map<String, Object> getSession() {
    return session;
  }

  /**
     * Return the name of the currently executing Action.
     * 
     * @return the current action name
     */
  public String getCurrentActionName() {
    return ActionContext.getContext().getName();
  }

  /**
   * Return the title of the page based on the name of the Action.
   * @return Localised name of the page.
   */
  public String getPageTitle() {
    return pageTitle;
  }

  /**
   * Return the name of the Welcome action for this particular user.
   *
   * Depending on the type of the user, send them to an appropriate Welcome action.
   *
   * @return The name of the Action to redirect to.
   */
  public String getWelcomeAction() {

    return null;
  }

  /**
     * Gets the action name.
     * 
     * @return the actionName
     */
  public String getActionName() {
    return actionName;
  }

  /**
     * Sets the action name.
     * 
     * @param actionName
     *            the actionName to set
     */
  public void setActionName(String actionName) {
    this.actionName = actionName;
  }

  /**
   * Retrieve a message for an Action invocation, not necessarily the current one.
   *
   * This message is retrieved from the Applicant's session in case we have redirected from another page (i.e. GET after POST)
   * @return the actionMessage
   */
  public String getActionMessage() {
    String actionMessage = null;
    if (ActionContext.getContext() != null) {
      Map<String, Object> sessionMap = ActionContext.getContext().getSession();
      if (sessionMap != null) {
        actionMessage = (String) sessionMap.get(RactActionSupport.ACTION_MESSAGE_TOKEN);
        sessionMap.remove(RactActionSupport.ACTION_MESSAGE_TOKEN);
      }
    }
    return actionMessage;
  }

  /**
   * Set a message for this Action invocation.
   *
   * This message is stored in the Applicant's session in case we redirect to another page (i.e. GET after POST)
   * @param actionMessage the actionMessage to set
   */
  public void setActionMessage(String actionMessage) {
    if (ActionContext.getContext() != null) {
      Map<String, Object> sessionMap = ActionContext.getContext().getSession();
      if (sessionMap != null) {
        sessionMap.put(RactActionSupport.ACTION_MESSAGE_TOKEN, actionMessage);
      }
    }
  }

  /**
   * Retrieve the result for an Action invocation, not necessarily the current one.
   *
   * This result is retrieved from the Applicant's session in case we have redirected from another page (i.e. GET after POST)
   * @return the actionResult
   */
  public String getActionResult() {
    String actionResult = null;
    Map<String, Object> sessionMap = ActionContext.getContext().getSession();
    if (sessionMap != null) {
      actionResult = (String) sessionMap.get(RactActionSupport.ACTION_RESULT_TOKEN);
      sessionMap.remove(RactActionSupport.ACTION_RESULT_TOKEN);
    }
    return actionResult;
  }

  /**
   * Set a result for this Action invocation.
   *
   * This result is stored in the Applicant's session in case we redirect to another page (i.e. GET after POST)
   * @param actionResult the actionResult to set
   * @return actionResult the actionResult that has been set (for convenience)
   */
  public String setActionResult(String actionResult) {
    Map<String, Object> sessionMap = ActionContext.getContext().getSession();
    if (sessionMap != null) {
      sessionMap.put(RactActionSupport.ACTION_RESULT_TOKEN, actionResult);
    }

    return actionResult;
  }

  /**
     * Gets the original action result.
     * 
     * @return the original action result
     */
  public String getOriginalActionResult() {
    return ActionContext.getContext().getActionInvocation().getResultCode();
  }

  /**
     * Manually set the name of the page corresponding to this Action.
     * 
     * A value set here will bypass the process of looking up the title from a
     * ResourceBundle, and will be returned verbatim.
     * 
     * @param pageTitle
     *            the new page title
     */
  public void setPageTitle(String pageTitle) {
    this.pageTitle = pageTitle;
  }

  /**
     * Gets the base url.
     * 
     * @return the base url
     */
  public String getBaseUrl() {
    return baseUrl;
  }

  /**
     * Sets the base url.
     * 
     * @param baseUrl
     *            the new base url
     */
  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  /**
     * Retrieve HTML page content as a String.
     * 
     * @param path
     *            the path
     * @param queryString
     *            the query string
     * @return HTML page content as a String.
     */
  static public String retrieveHtml(String path, String queryString) {
    String html = null;
    try {
      URI uri = new URI(path, true);

      HttpMethod method = new PostMethod();
      method.setURI(uri);
      method.setQueryString(queryString);

      HttpClient client = new HttpClient();
      
      String httpTimeout = Config.configManager.getProperty("external.http.timeout", "4000"); // 4 secs
      HttpClientParams params = new HttpClientParams();
      params.setSoTimeout(Integer.parseInt(httpTimeout));
      
      client.setParams(params);
      
      client.executeMethod(method);
      if (method.getStatusCode() == 200) {
        html = method.getResponseBodyAsString();
        Config.logger.debug(Config.key + ": Retrieved HTML from: " + path + "?" + queryString);
      } else {
        Config.logger.error(Config.key + ": Got error code when attempting to retrieve HTML: " + method.getStatusCode() + " (" + path + "?" + queryString + ")");
      }
      method.releaseConnection();
    } catch (URIException e) {
      Config.logger.error(Config.key + ": Could not retrieve HTML: " + e.getMessage());
    } catch (IOException e) {
      Config.logger.error(Config.key + ": Could not retrieve HTML: " + e.getMessage());
    }

    return html;
  }

  /**
   * Verify that credit card number is valid.
   * @param s card number to validate.
   * @return true if number is valid, false otherwise.
   */
  static public boolean validateCreditCardNumber(String s) {
    if (s == null) return false;

    double i, j;
    // validate number
    j = s.length() / 2;
    if (j < 6.5 || j > 8 || j == 7) {
      return false;
    }

    int k = (int) Math.floor(j);
    int m = (int) (Math.ceil(j) - k);
    int c = 0;
    for (i = 0; i < k; i++) {
      int a = Integer.parseInt(String.valueOf(s.charAt((int) i * 2 + m))) * 2;
      c += a > 9 ? Math.floor(a / 10 + a % 10) : a;
    }
    for (i = 0; i < k + m; i++) {
      c += Integer.parseInt(String.valueOf(s.charAt((int) i * 2 + 1 - m))) * 1;
    }
    return (c % 10 == 0);
  }

  /**
   * Strips non-digits from the provided String.
   * @param input String to strip.
   * @return String without non-digits.
   */
  static public String stripNonDigits(String input) {
    if (input != null) {
      input = Pattern.compile("[^\\d]").matcher(input).replaceAll("");
    }

    return input;
  }

  /**
     * Create session for RACT records if not already created.
     * 
     * @param userId
     *            the user id
     * @param initialContext
     *            the initial context
     * @return the string
     */
  public String recordSession(Integer userId, String initialContext) {
  	HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(StrutsStatics.HTTP_REQUEST);
  	String sessionId = request.getSession().getId();
  	  	
    try {
      if (!getSession().containsKey("hasRactSession")) {
        if (sessionId != null) {
          if (this.getUserMgr().getUserSession(sessionId) == null) {          	
          	this.getUserMgr().createUserSession(sessionId, extractOriginatingIp(), userId, initialContext);
          }
          getSession().put("hasRactSession", true);
          getSession().put("ractSession", sessionId);
        } else {
          Config.logger.debug(Config.key + ": could not create RACT User Session: no session available.");
        }
      } else {
      	Config.logger.debug(Config.key + ": could not create RACT User Session: already present.");
      }
    } catch (Exception e) {
      Config.logger.error(Config.key + ": could not create RACT User Session: ", e);
    }
    
    return sessionId;
  }
  
  /**
     * Lazy initialise userMgr.
     * 
     * @return the user mgr
     */
  public UserSecurityMgrRemote getUserMgr() {
  	if (this.userMgr == null) {
  		try {
  			this.setUp();
  		} catch (Exception e) {
        Config.logger.error(Config.key + ": could not aquire userMgr: ", e);  			
  		}
  	}
  	return this.userMgr;
  }
  
  /**
     * Attempt to extract originating IP address from "X-Forwarded-For" header.
     * Return REMOTE_ADDR if "X-Forwarded-For" header is null. "X-Forwarded-For"
     * header may contain multiple IPs, the first one of which is the real
     * client IP, the rest are the client?s upstream proxies.
     * 
     * @return the string
     */
  protected String extractOriginatingIp() {  	
  	String header = ServletActionContext.getRequest().getHeader("X-Forwarded-For");
  	String ip = null;
  	
  	if (header == null) {
  		ip = ServletActionContext.getRequest().getRemoteAddr();
  	} else if (header.contains(",")) {
  		ip = header.split(",")[0].trim();
  	} else if (header.contains(";")) {
  		ip = header.split(";")[0].trim();  		
  	} else if (header.trim().contains(" ")) {
  		ip = header.split(" ")[0].trim();
    } else {
    	ip = header;
    }
  	
  	if (ip != null && ip.length() > 50) {
  		ip = ip.substring(0, 50);
  	}
  	
  	return ip;
  }

    /**
     * Retrieve a user's country based on IP.
     * @param ipAddress to look up.
     * 
     * @return country related to IP address, from external lookup
     */
    protected String extractOriginatingCountry(String ipAddress) {
        long start = Calendar.getInstance().getTimeInMillis();

        String html = RactActionSupport.retrieveHtmlWithGetMethodAndBasicAuthentication(ipAddressLookupServiceUrl + ipAddress, "", ipAddressLookupServiceUsername, ipAddressLookupServicePassword);

        long end = Calendar.getInstance().getTimeInMillis();

        Config.logger.info("Retrieved IP country in: " + (end - start) + " millis");

        if (html != null) {
            try {
                JSONObject json = new JSONObject(html);

                if (json.has("error")) {
                    // error response from web service
                    if (json.has("code")) {
                        String code = json.getString("code");
                        if (code.equalsIgnoreCase("IP_ADDRESS_RESERVED")) {
                            // This is a reserved IP Address range - private, multicast etc
                            return code;
                        }
                    }
                }

                if (json.has("country")) {
                    JSONObject country = json.getJSONObject("country");
                    if(country.has("names")) {
                        JSONObject names = country.getJSONObject("names");
                        // Return the English representation of the country name
                        return names.getString("en");
                    }
                }

                // we couldn't get country - next best is the registered_country
                if (json.has("registered_country")) {
                    JSONObject country = json.getJSONObject("registered_country");
                    if(country.has("names")) {
                        JSONObject names = country.getJSONObject("names");
                        // Return the English representation of the country name
                        // TODO: should we prefix the country with something to indicate that we are using registered_country instead?
                        return names.getString("en");
                    }
                }
            } catch (Exception e) {
                Config.logger.debug(Config.key + ": can not determine user country by IP: " + e);
            }
        }

        return null;
    }

    static public String retrieveHtmlWithGetMethodAndBasicAuthentication(String path, String queryString, String username, String password) {
        String html = null;
        try {
            URI uri = new URI(path, true);

            HttpMethod method = new GetMethod();
            method.setURI(uri);
            method.setQueryString(queryString);

            HttpClient client = new HttpClient();

            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
                       
            client.getState().setCredentials(AuthScope.ANY, credentials);

            String httpTimeout = Config.configManager.getProperty("external.http.timeout", "4000"); // 4 seconds
            HttpClientParams params = new HttpClientParams();
            params.setSoTimeout(Integer.parseInt(httpTimeout));

            client.setParams(params);

            client.executeMethod(method);
            if (method.getStatusCode() == 200) {
                html = method.getResponseBodyAsString();
                Config.logger.debug(Config.key + ": Retrieved HTML from: " + path + "?" + queryString);
            } else {
                Config.logger.error(Config.key + ": Got error code when attempting to retrieve HTML: " + method.getStatusCode() + ":" + method.getStatusText() + " (" + path + "?" + queryString + ")");
                Config.logger.error(Config.key + ": ResponseBody: " + method.getResponseBodyAsString());
                html = method.getResponseBodyAsString();
            }
            method.releaseConnection();
        } catch (URIException e) {
            Config.logger.error(Config.key + ": Could not retrieve HTML: " + e.getMessage());
        } catch (IOException e) {
            Config.logger.error(Config.key + ": Could not retrieve HTML: " + e.getMessage());
        }

        return html;
    }
  
  /**
   * Split a CamelCase string into a space separated string.
   * @param inputString The string you wish to split.
   * @return A string where each upper case letter is separated by a single space
   */
  static public String unCamelise(String inputString) {
    Pattern p = Pattern.compile("\\p{Lu}\\p{Lu}*");
    Matcher m = p.matcher(inputString);
    StringBuffer sb = new StringBuffer();
    while (m.find()) {
      if (m.group().length() > 1) {
        StringBuilder caps = new StringBuilder(m.group());
        m.appendReplacement(sb, caps.insert(m.group().length() - 1, ' ').toString());
      } else {
        m.appendReplacement(sb, ' ' + m.group());
      }
    }
    m.appendTail(sb);
    return sb.toString().trim();
  }

  /**
     * Validate dob.
     * 
     * @param dobString
     *            the dob string
     * @param minAge
     *            the min age
     * @param maxAge
     *            the max age
     * @param fieldName
     *            the field name
     * @param fieldLabel
     *            the field label
     */
  protected void validateDob(String dobString, int minAge, int maxAge, String fieldName, String fieldLabel) {
    try {
    	
    	// add 1 year to max age, 
    	// if max allowable age is 100, then we allow ages NOT >= 101
    	maxAge += 1;
    	
      Calendar testDate = Calendar.getInstance();
      testDate.set(Calendar.HOUR_OF_DAY, 0);
      testDate.set(Calendar.MINUTE, 0);
      testDate.set(Calendar.SECOND, 0);
      testDate.set(Calendar.MILLISECOND, 0);
      
      Calendar dobDate = Calendar.getInstance();      
      DateTime dob = new DateTime(dobString);
      dobDate.setTime(dob);
      dobDate.set(Calendar.HOUR_OF_DAY, 0);
      dobDate.set(Calendar.MINUTE, 0);
      dobDate.set(Calendar.SECOND, 0);
      dobDate.set(Calendar.MILLISECOND, 0);
            
      if (dob.after(testDate.getTime())) { // dob is in the future
        throw new Exception("Date of birth cannot be in the future");
      }

      testDate = Calendar.getInstance();
      testDate.set(Calendar.HOUR_OF_DAY, 0);
      testDate.set(Calendar.MINUTE, 0);
      testDate.set(Calendar.SECOND, 0);
      testDate.set(Calendar.MILLISECOND, 0);      
      testDate.add(Calendar.YEAR, -maxAge);
      
      if (dobDate.before(testDate) || dobDate.equals(testDate)) { // dob is more than maxAge years ago
        throw new Exception("Date of birth cannot be more than " + maxAge + " years ago");
      }

      if (!(this instanceof TippingCreate) && !(this instanceof ChangeDetail)) {
        testDate = Calendar.getInstance();
        testDate.set(Calendar.HOUR_OF_DAY, 0);
        testDate.set(Calendar.MINUTE, 0);
        testDate.set(Calendar.SECOND, 0);
        testDate.set(Calendar.MILLISECOND, 0);        
        testDate.add(Calendar.YEAR, -minAge);
        
        if (dobDate.after(testDate)) { // dob is less than minAge years ago
          throw new Exception("Date of birth cannot be less than " + minAge + " years ago");
        }
      }

    } catch (Exception e) {
      addFieldError(fieldName, "Please provide a valid " + fieldLabel + ".");
    }
 
  }

  /**
     * Remove all field errors matching provided fieldName.
     * 
     * @param fieldName
     *            the field name
     */
  protected void removeFieldError(String fieldName) {
    Map<String, List<String>> fieldErrors = this.getFieldErrors();
    fieldErrors.remove(fieldName);
    this.setFieldErrors(fieldErrors);
  }

  /**
     * Gets the namespace.
     * 
     * @return the namespace
     */
  public String getNamespace() {
    return namespace;
  }

  /**
     * Sets the namespace.
     * 
     * @param namespace
     *            the namespace to set
     */
  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  /**
     * Gets the http request.
     * 
     * @return the http request
     */
  public HttpServletRequest getHttpRequest() {
    HttpServletRequest request = ServletActionContext.getRequest();
    return request;
  }

  /**
     * Checks if is tipping enabled.
     * 
     * @return the tippingEnabled
     */
  public boolean isTippingEnabled() {
    return tippingEnabled;
  }

  /**
     * Sets the tipping enabled.
     * 
     * @param tippingEnabled
     *            the tippingEnabled to set
     */
  public void setTippingEnabled(boolean tippingEnabled) {
    this.tippingEnabled = tippingEnabled;
  }

  /**
     * Gets the tipping redirect url.
     * 
     * @return the tippingRedirectUrl
     */
  public String getTippingRedirectUrl() {
    return tippingRedirectUrl;
  }

  /**
     * Sets the tipping redirect url.
     * 
     * @param tippingRedirectUrl
     *            the tippingRedirectUrl to set
     */
  public void setTippingRedirectUrl(String tippingRedirectUrl) {
    this.tippingRedirectUrl = tippingRedirectUrl;
  }

  /**
     * Gets the sso login logout url.
     * 
     * @return the ssoLoginLogoutUrl
     */
  public String getSsoLoginLogoutUrl() {
    return ssoLoginLogoutUrl;
  }

  /**
     * Sets the sso login logout url.
     * 
     * @param ssoLoginLogoutUrl
     *            the ssoLoginLogoutUrl to set
     */
  public void setSsoLoginLogoutUrl(String ssoLoginLogoutUrl) {
    this.ssoLoginLogoutUrl = ssoLoginLogoutUrl;
  }

  /**
     * Gets the tipping terms url.
     * 
     * @return the tippingTermsUrl
     */
  public String getTippingTermsUrl() {
    return tippingTermsUrl;
  }

  /**
     * Sets the tipping terms url.
     * 
     * @param tippingTermsUrl
     *            the tippingTermsUrl to set
     */
  public void setTippingTermsUrl(String tippingTermsUrl) {
    this.tippingTermsUrl = tippingTermsUrl;
  }

  /**
     * Checks if is older than.
     * 
     * @param dobTest
     *            the dob test
     * @param age
     *            the age
     * @return true, if is older than
     */
  public boolean isOlderThan(DateTime dobTest, int age) {
    Calendar cutoff = Calendar.getInstance();
    cutoff.add(Calendar.YEAR, -age);

    Calendar dob = Calendar.getInstance();
    dob.setTime(dobTest);
    if (dob.after(cutoff)) {
      return false;
    } else {
      return true;
    }
  }

  /**
     * Checks if is ajax context.
     * 
     * @return the ajaxContext
     */
  public boolean isAjaxContext() {
    return ajaxContext;
  }

  /**
     * Sets the ajax context.
     * 
     * @param ajaxContext
     *            the ajaxContext to set
     */
  public void setAjaxContext(boolean ajaxContext) {
    this.ajaxContext = ajaxContext;
  }

  /**
     * Checks if is json context.
     * 
     * @return the jsonContext
     */
  public boolean isJsonContext() {
    return jsonContext;
  }

  /**
     * Sets the json context.
     * 
     * @param jsonContext
     *            the jsonContext to set
     */
  public void setJsonContext(boolean jsonContext) {
    this.jsonContext = jsonContext;
  }

  /**
     * Checks if is development server.
     * 
     * @return the developmentServer
     */
  public boolean isDevelopmentServer() {
    return developmentServer;
  }

  /**
     * Sets the development server.
     * 
     * @param developmentServer
     *            the developmentServer to set
     */
  public void setDevelopmentServer(boolean developmentServer) {
    this.developmentServer = developmentServer;
  }

  /**
     * Gets the fsg url.
     * 
     * @return the fsgUrl
     */
  public String getFsgUrl() {
    return fsgUrl;
  }

  /**
     * Sets the fsg url.
     * 
     * @param fsgUrl
     *            the fsgUrl to set
     */
  public void setFsgUrl(String fsgUrl) {
    this.fsgUrl = fsgUrl;
  }

  /**
     * Gets the pds url.
     * 
     * @return the pdsUrl
     */
  public String getPdsUrl() {
    return pdsUrl;
  }

  /**
     * Sets the pds url.
     * 
     * @param pdsUrl
     *            the pdsUrl to set
     */
  public void setPdsUrl(String pdsUrl) {
    this.pdsUrl = pdsUrl;
  }

  /**
     * Retrieve a cookie by name.
     * 
     * @param cookieName
     *            the cookie name
     * @return the cookie
     */
  public javax.servlet.http.Cookie getCookie(String cookieName) {
    javax.servlet.http.Cookie[] cookieList = ServletActionContext.getRequest().getCookies();
    if(cookieList != null ){
      List<javax.servlet.http.Cookie> cookies = Arrays.asList(cookieList);

      for (javax.servlet.http.Cookie c : cookies) {
        if (c.getName().equals(cookieName)) {
          return c;
        }
      }
    }
    return null;
  }

  /**
     * Gets the pds investor.
     * 
     * @return the pdsInvestor
     */
  public String getPdsInvestor() {
    return pdsInvestor;
  }

  /**
     * Sets the pds investor.
     * 
     * @param pdsInvestor
     *            the pdsInvestor to set
     */
  public void setPdsInvestor(String pdsInvestor) {
    this.pdsInvestor = pdsInvestor;
  }

  /**
     * Gets the pds home contents.
     * 
     * @return the pdsHomeContents
     */
  public String getPdsHomeContents() {
    return pdsHomeContents;
  }

  /**
     * Sets the pds home contents.
     * 
     * @param pdsHomeContents
     *            the pdsHomeContents to set
     */
  public void setPdsHomeContents(String pdsHomeContents) {
    this.pdsHomeContents = pdsHomeContents;
  }

  /**
     * Gets the pds contents calculator.
     * 
     * @return the pdsContentsCalculator
     */
  public String getPdsContentsCalculator() {
    return pdsContentsCalculator;
  }

  /**
     * Sets the pds contents calculator.
     * 
     * @param pdsContentsCalculator
     *            the pdsContentsCalculator to set
     */
  public void setPdsContentsCalculator(String pdsContentsCalculator) {
    this.pdsContentsCalculator = pdsContentsCalculator;
  }

  /**
     * Checks if is campaign active.
     * 
     * @return the campaignActive
     */
  public boolean isCampaignActive() {
    if (campaignActive == null) {
      try {
        CampaignMgrRemote campaignMgr = (CampaignMgrRemote) this.getContext().lookup("CampaignMgr/remote");
        com.ract.web.campaign.Campaign campaign = campaignMgr.retrieveCampaign(this.getMembershipCompetition());
        
        Config.logger.info("Campaign = " + campaign);
        
        if (campaign == null || !campaign.isActive()) {
          return false;
        }
        return true;
      }
      catch (Exception ex) {
        Config.logger.error(Config.key + ": can not create campaignMgr: ", ex);
        return false;
      }
    }
    else {
      return campaignActive;
    }
  }

  /**
     * Gets the campaign friendly name.
     * 
     * @return the campaign friendly name
     */
  public String getCampaignFriendlyName() {
    if (campaignFriendlyName.isEmpty()) {
      try {
        CampaignMgrRemote campaignMgr = (CampaignMgrRemote) this.getContext().lookup("CampaignMgr/remote");
        com.ract.web.campaign.Campaign campaign = campaignMgr.retrieveCampaign(this.getMembershipCompetition());
        if (campaign == null || !campaign.isActive()) {
          return "";
        }
        return campaign.getFriendlyName();
      }
      catch (Exception ex) {
        Config.logger.error(Config.key + ": can not retrieve Campaign's friendly name: ", ex);
        return "";
      }
    }
    else {
      return campaignFriendlyName;
    }
  }

  /**
     * Checks if is client in campaign.
     * 
     * @return true, if is client in campaign
     */
  public boolean isClientInCampaign() {
    if (isCampaignActive()) {
      try {
        CampaignMgrRemote campaignMgr = (CampaignMgrRemote) this.getContext().lookup("CampaignMgr/remote");
        List<CampaignEntry> entries = campaignMgr.getEntriesByReference(this.getMembershipCompetition());
        User user = (User) getSession().get(USER_TOKEN);
        if (user == null) {
          return false;
        }
        String clientNumber = ((MemberUser) user).getClientNumber();
        for (CampaignEntry e : entries) {
          if (e.getClientNumber() == Integer.parseInt(clientNumber)) {
            return true;
          }
        }
      } catch (Exception ex) {
        Config.logger.error(Config.key + ": can not check if Client is in Campaign: " + ex);
      }
    }
    return false;
  }

	/**
     * Gets the membership competition.
     * 
     * @return the membership competition
     */
	public String getMembershipCompetition() {
		return membershipCompetition;
	}

	/**
     * Sets the membership competition.
     * 
     * @param membershipCompetition
     *            the new membership competition
     */
	public void setMembershipCompetition(String membershipCompetition) {
		this.membershipCompetition = membershipCompetition;
	}

	/**
     * Gets the discount msg.
     * 
     * @return the discount msg
     */
	public String getDiscountMsg() {
		if (discountRange != null && discountRange.contains("-")) {
			String parts[] = discountRange.split("-");
			try {
				DateTime start = new DateTime(dtf.parse(parts[0]));
				DateTime end = new DateTime(dtf.parse(parts[1]));
				DateTime now = new DateTime();
				
				if (start.before(now) && end.after(now)) {
					discountMsg = Config.configManager.getProperty("insurance.car.discount.msg");
				}
				
			} catch (ParseException e) {
        Config.logger.warn(Config.key + ": could not parse discountRange: " + discountRange + ", " + e);				
			}
		}
		return discountMsg;
	}

	/**
     * Sets the discount msg.
     * 
     * @param discountMsg
     *            the new discount msg
     */
	public void setDiscountMsg(String discountMsg) {
		this.discountMsg = discountMsg;
	}

	/**
     * Gets the address service.
     * 
     * @return the address service
     */
	public AddressService getAddressService() {
		return addressService;
	}

	/**
     * Sets the address service.
     * 
     * @param addressService
     *            the new address service
     */
	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}

	/**
     * Gets the qas service.
     * 
     * @return the qas service
     */
	public QasService getQasService() {
		return qasService;
	}

	/**
     * Sets the qas service.
     * 
     * @param qasService
     *            the new qas service
     */
	public void setQasService(QasService qasService) {
		this.qasService = qasService;
	}

	/**
     * Checks if is bcp mode.
     * 
     * @return true, if is bcp mode
     */
	public boolean isBcpMode() {
		return bcpMode;
	}

	/**
     * Sets the bcp mode.
     * 
     * @param bcpMode
     *            the new bcp mode
     */
	public void setBcpMode(boolean bcpMode) {
		this.bcpMode = bcpMode;
	}
	
   public Long getStartTime() {
        if (this.startTime == null) {
            this.startTime = (Long) this.getSession().get(TOKEN_START_TIME);
            this.getSession().remove(TOKEN_START_TIME);
        }
        
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
        this.getSession().put(TOKEN_START_TIME, this.startTime);
    }
}
