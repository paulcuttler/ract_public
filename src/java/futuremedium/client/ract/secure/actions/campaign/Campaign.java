/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package futuremedium.client.ract.secure.actions.campaign;

import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;
import com.ract.web.client.ClientTransaction;
import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.membership.MembershipCardVO;
import com.ract.util.DateTime;
import com.ract.web.campaign.CampaignMgrRemote;
import com.ract.web.campaign.CampaignVehicleEntry;
import com.ract.web.client.ClientTransactionPK;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.EmailManager;
import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.actions.data.GlassesGuide;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author pcock
 */
public class Campaign extends CampaignForm {

  private final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

  private Boolean member;
  private String membershipCard1;
  private String membershipCard2;
  private String membershipCard3;
  private String membershipCard4;
  private String memberDobDay;
  private String memberDobMonth;
  private String memberDobYear;
  private String title;
  private String firstname;
  private String lastname;
  private String gender;
  private String dobDay;
  private String dobMonth;
  private String dobYear;
  private String postcode;
  private String suburb;
  private String street;
  private String street_no;
  private String phone;
  private String email;
  private Boolean updateEmail;
  private String year;
  private String make;
  private String model;
  private String bodyType;
  private String transmission;
  private String cylinders;
  private String nvic;
  private String renewalMonth;
  private String code;
  private Boolean tnc;



  @Override
  public String execute() {

    try {

      CampaignVehicleEntry c = new CampaignVehicleEntry();

      if(this.isMember()){

        MembershipCardVO card = this.getRoadsideMgr().getMembershipCard(this.getMembershipCard1() + this.getMembershipCard2() + this.getMembershipCard3() + this.getMembershipCard4());
        ClientVO client = this.getCustomerMgr().getClientByMembershipCardNumber(card.getCardNumber());

        if (client == null) {
          this.setActionMessage("Unable to retrieve your membership account via card number.");
          Config.logger.error(Config.key + ": Unable to retrieve membership by card number: " + card.getCardNumber());
        	return ERROR;
        }
        
        c.setClientNumber(client.getClientNumber());

        if (this.getUpdateEmail() != null && this.getUpdateEmail()) {

          // retrieve/create client transaction
          ClientTransaction trans = this.getCustomer(client.getClientNumber());

          // set email address
          trans.setEmailAddress(this.getEmail());

          // create new primary key for transaction
          ClientTransactionPK pk = trans.getClientTransactionPK();
          pk.setTransactionDate(new DateTime());
          pk.setTransactionType(ClientTransaction.TRANSACTION_TYPE_UPDATE);
          trans.setClientTransactionPK(pk);

          // persist transaction
          this.getCustomerMgr().createClientTransaction(trans);
        }
        
      } else {

        Date dobDate = dateFormat.parse(this.getDobDay() + '/' + this.getDobMonth() + '/' + this.getDobYear());
        DateTime dobDateTime = new DateTime(dobDate);

        c.setClientNumber(null);
        c.setTitle(this.getTitle());
        c.setFirstName(this.getFirstname());
        c.setLastName(this.getLastname());

        if(this.getGender().equals("Male")) {
          c.setGender(true); // male
        } else {
          c.setGender(false); // female
        }

        c.setBirthDate(dobDateTime);
        c.setStreetNumber(this.getStreet_no());

        try {
          c.setStreetSuburbId(Integer.parseInt(this.getStreet()));
        } catch(Exception e) {
          Config.logger.error(Config.key + ": street not a number: ", e);
        }
      }

      c.setPhone(this.getPhone());
      c.setEmail(this.getEmail());
      c.setNvic(this.getNvic());
      
      try {
        c.setRenewalMonth(Integer.parseInt(this.getRenewalMonth()));
      } catch(Exception e) {
        Config.logger.error(Config.key + ": renewal month not a number: ", e);
      }

      c.setCampaignRef(this.getCode());
      c.setCreateDate(new DateTime());
      

      // persist to DB – CampaignVehicle in ract_web_dev
      try {
        CampaignMgrRemote campaignMgr = (CampaignMgrRemote) this.getContext().lookup("CampaignMgr/remote");
        com.ract.web.campaign.Campaign campaign = campaignMgr.retrieveCampaign(this.getCode());

        if(campaign == null || !campaign.isActive()) {
          Config.logger.info(Config.key + ":Campaign: User has entered an invalid code: " + this.getCode());
          this.setActionMessage("noCampaign");
          return SUCCESS;
        } else {
          campaignMgr.createEntry(c);
        }

      } catch(Exception e) {
        Config.logger.error(Config.key + ": can not create campaignMgr: ", e);
      }


    } catch(Exception e) {
      Config.logger.error(Config.key + ": invalid date or  cannot get client via remote managers: ", e);
    }

    
    if (!EmailManager.sendCampaignEmail(this)) {
      this.setActionMessage("An error occurred sending an email to your email address");
      return ERROR;
    }
    
    this.setActionMessage("sent");
    return SUCCESS;

  }

  @Override
  public void validate() {

    if (this.isMember() != null) {

      if (this.isMember()) {

        if (this.getMembershipCard1() == null || this.getMembershipCard1().trim().length() != 4
                && this.getMembershipCard2() == null || this.getMembershipCard2().trim().length() != 4
                && this.getMembershipCard3() == null || this.getMembershipCard3().trim().length() != 4
                && this.getMembershipCard4() == null || this.getMembershipCard4().trim().length() != 4) {
          this.addFieldError("membershipCard1", "Please enter a 16 digit number");
        }

        if (this.getMemberDobDay() == null || this.getMemberDobDay().isEmpty()
                || this.getMemberDobMonth() == null || this.getMemberDobMonth().isEmpty()
                || this.getMemberDobYear() == null || this.getMemberDobYear().isEmpty()) {
          this.addFieldError("memberDobDay", "Please enter a date of birth");
        }

      } else {

        if (this.getTitle() == null || this.getTitle().isEmpty()) {
          this.addFieldError("title", "Please answer this question");
        }

        if (this.getFirstname() == null || this.getFirstname().isEmpty()) {
          this.addFieldError("firstname", "Please answer this question");
        }

        if (this.getLastname() == null || this.getLastname().isEmpty()) {
          this.addFieldError("lastname", "Please answer this question");
        }

        if (this.getGender() == null || this.getGender().isEmpty()) {
          this.addFieldError("gender", "Please select an option");
        }

        if (this.getDobDay() == null || this.getDobDay().isEmpty()
                || this.getDobMonth() == null || this.getDobMonth().isEmpty()
                || this.getDobYear() == null || this.getDobYear().isEmpty()) {
          this.addFieldError("dobDay", "Please enter a date of birth");
        }

        if (this.getPostcode() == null || this.getPostcode().isEmpty()) {
          this.addFieldError("postcode", "Please answser this question");
        }


        if (this.getSuburb() == null || this.getSuburb().isEmpty()) {
          this.addFieldError("suburb", "Please select an option");
        }

        if (this.getStreet() == null || this.getStreet().isEmpty()) {
          this.addFieldError("street", "Please select an option");
        }

        if (this.getStreet_no() == null || this.getStreet_no().isEmpty()) {
          this.addFieldError("street_no", "Please answser this question");
        }

      }

    } else {
      this.addFieldError("member", "Please select an option");
    }


    if (this.getEmail() == null || this.getEmail().isEmpty()) {
      this.addFieldError("email", "Please answser this question");
    }

    if (this.getYear() == null || this.getYear().isEmpty()) {
      this.addFieldError("year", "Please answser this question");
    }

    if (this.getMake() == null || this.getMake().isEmpty()) {
      this.addFieldError("make", "Please select an option");
    }

    if (this.getModel() == null || this.getModel().isEmpty()) {
      this.addFieldError("model", "Please select an option");
    }

    if (this.getBodyType() == null || this.getBodyType().isEmpty()) {
      this.addFieldError("bodyType", "Please select an option");
    }

    if (this.getTransmission() == null || this.getTransmission().isEmpty()) {
      this.addFieldError("transmission", "Please select an option");
    }

    if (this.getCylinders() == null || this.getCylinders().isEmpty()) {
      this.addFieldError("cylinders", "Please select an option");
    }

    if (this.getNvic() == null || this.getNvic().isEmpty()) {
      this.addFieldError("nvic", "Please select an option");
    }

    if (this.getRenewalMonth() == null || this.getRenewalMonth().isEmpty()) {
      this.addFieldError("renewalMonth", "Please select an option");
    }

    if (this.getTnc() == null || !this.getTnc()) {
      this.addFieldError("tnc", "You must accept the terms and conditions");
    }

    if (this.hasErrors()) {
      Config.logger.info(Config.key + " validate Campaign: running VehicleList");
      populateVehicleLists();
    }

  }

  public void populateVehicleLists() {

    if (this.getYear() != null) {

      GlassesGuide gd = new GlassesGuide();
      try {
        int iLookup = 1;

        gd.setYear(Integer.parseInt(getYear()));
        gd.setLookup(iLookup);
        gd.execute();
        makeList = gd.getMakeList();
        gd.setYear(Integer.parseInt(this.getYear()));
        if (this.getMake() != null && !this.getMake().isEmpty()) {
          gd.setMake(this.getMake());
          iLookup++;

          gd.setLookup(iLookup);
          gd.execute();
          modelList = gd.getModelList();
        }
        if (this.getModel() != null && !this.getModel().isEmpty()) {
          gd.setModel(this.getModel());
          iLookup++;

          gd.setLookup(iLookup);
          gd.execute();
          bodyTypeList = gd.getBodyTypeList();
        }
        
        if (this.getBodyType() != null && !this.getBodyType().isEmpty()) {
          gd.setBodyType(this.getBodyType());
          iLookup++;

          gd.setLookup(iLookup);
          gd.execute();
          transmissionList = gd.getTransmissionList();
        }
        if (this.getTransmission() != null && !this.getTransmission().isEmpty()) {
          gd.setTransmission(this.getTransmission());
          iLookup++;

          gd.setLookup(iLookup);
          gd.execute();
          cylinderList = gd.getCylinderList();
        }
        if (this.getCylinders() != null && !this.getCylinders().isEmpty()) {
          gd.setCylinders(this.getCylinders());
          iLookup++;

          gd.setLookup(iLookup);
          gd.execute();
          nvicList = gd.getVehicleList();
        }
        
      } catch(Exception e) {
        Config.logger.info(Config.key + ": CampaignFrom getMakeList: " + e);
      }

    }
  }

  /**
   * @return the member
   */
  public Boolean isMember() {
    return member;
  }

  /**
   * @param member the member to set
   */
  public void setMember(Boolean member) {
    this.member = member;
  }

  /**
   * @return the membershipCard1
   */
  public String getMembershipCard1() {
    return membershipCard1;
  }

  /**
   * @param membershipCard1 the membershipCard1 to set
   */
  public void setMembershipCard1(String membershipCard1) {
    this.membershipCard1 = membershipCard1;
  }

  /**
   * @return the membershipCard2
   */
  public String getMembershipCard2() {
    return membershipCard2;
  }

  /**
   * @param membershipCard2 the membershipCard2 to set
   */
  public void setMembershipCard2(String membershipCard2) {
    this.membershipCard2 = membershipCard2;
  }

  /**
   * @return the membershipCard3
   */
  public String getMembershipCard3() {
    return membershipCard3;
  }

  /**
   * @param membershipCard3 the membershipCard3 to set
   */
  public void setMembershipCard3(String membershipCard3) {
    this.membershipCard3 = membershipCard3;
  }

  /**
   * @return the membershipCard4
   */
  public String getMembershipCard4() {
    return membershipCard4;
  }

  /**
   * @param membershipCard4 the membershipCard4 to set
   */
  public void setMembershipCard4(String membershipCard4) {
    this.membershipCard4 = membershipCard4;
  }

  /**
   * @return the memberDobDay
   */
  public String getMemberDobDay() {
    return memberDobDay;
  }

  /**
   * @param memberDobDay the memberDobDay to set
   */
  public void setMemberDobDay(String memberDobDay) {
    this.memberDobDay = memberDobDay;
  }

  /**
   * @return the memberDobMonth
   */
  public String getMemberDobMonth() {
    return memberDobMonth;
  }

  /**
   * @param memberDobMonth the memberDobMonth to set
   */
  public void setMemberDobMonth(String memberDobMonth) {
    this.memberDobMonth = memberDobMonth;
  }

  /**
   * @return the memberDobYear
   */
  public String getMemberDobYear() {
    return memberDobYear;
  }

  /**
   * @param memberDobYear the memberDobYear to set
   */
  public void setMemberDobYear(String memberDobYear) {
    this.memberDobYear = memberDobYear;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the firstname
   */
  public String getFirstname() {
    return firstname;
  }

  /**
   * @param firstname the firstname to set
   */
  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  /**
   * @return the lastname
   */
  public String getLastname() {
    return lastname;
  }

  /**
   * @param lastname the lastname to set
   */
  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  /**
   * @return the gender
   */
  public String getGender() {
    return gender;
  }

  /**
   * @param gender the gender to set
   */
  public void setGender(String gender) {
    this.gender = gender;
  }

  /**
   * @return the dobDay
   */
  public String getDobDay() {
    return dobDay;
  }

  /**
   * @param dobDay the dobDay to set
   */
  public void setDobDay(String dobDay) {
    this.dobDay = dobDay;
  }

  /**
   * @return the dobMonth
   */
  public String getDobMonth() {
    return dobMonth;
  }

  /**
   * @param dobMonth the dobMonth to set
   */
  public void setDobMonth(String dobMonth) {
    this.dobMonth = dobMonth;
  }

  /**
   * @return the dobYear
   */
  public String getDobYear() {
    return dobYear;
  }

  /**
   * @param dobYear the dobYear to set
   */
  public void setDobYear(String dobYear) {
    this.dobYear = dobYear;
  }

  /**
   * @return the postcode
   */
  public String getPostcode() {
    return postcode;
  }

  /**
   * @param postcode the postcode to set
   */
  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  /**
   * @return the suburb
   */
  public String getSuburb() {
    return suburb;
  }

  /**
   * @param suburb the suburb to set
   */
  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  /**
   * @return the street
   */
  public String getStreet() {
    return street;
  }

  /**
   * @param street the street to set
   */
  public void setStreet(String street) {
    this.street = street;
  }

  /**
   * @return the street_no
   */
  public String getStreet_no() {
    return street_no;
  }

  /**
   * @param street_no the street_no to set
   */
  public void setStreet_no(String street_no) {
    this.street_no = street_no;
  }

  /**
   * @return the phone
   */
  public String getPhone() {
    return phone;
  }

  /**
   * @param phone the phone to set
   */
  @StringLengthFieldValidator(message = "Please enter a valid phone number", minLength = "8", maxLength = "10")
  public void setPhone(String phone) {
    this.phone = phone;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  @EmailValidator(message = "Please enter a valid email")
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the year
   */
  public String getYear() {
    return year;
  }

  /**
   * @param year the year to set
   */
  public void setYear(String year) {
    this.year = year;
  }

  /**
   * @return the make
   */
  public String getMake() {
    return make;
  }

  /**
   * @param make the make to set
   */
  public void setMake(String make) {
    this.make = make;
  }

  /**
   * @return the model
   */
  public String getModel() {
    return model;
  }

  /**
   * @param model the model to set
   */
  public void setModel(String model) {
    this.model = model;
  }

  /**
   * @return the bodyType
   */
  public String getBodyType() {
    return bodyType;
  }

  /**
   * @param bodyType the bodyType to set
   */
  public void setBodyType(String bodyType) {
    this.bodyType = bodyType;
  }

  /**
   * @return the transmission
   */
  public String getTransmission() {
    return transmission;
  }

  /**
   * @param transmission the transmission to set
   */
  public void setTransmission(String transmission) {
    this.transmission = transmission;
  }

  /**
   * @return the cylinders
   */
  public String getCylinders() {
    return cylinders;
  }

  /**
   * @param cylinders the cylinders to set
   */
  public void setCylinders(String cylinders) {
    this.cylinders = cylinders;
  }

  /**
   * @return the nvic
   */
  public String getNvic() {
    return nvic;
  }

  /**
   * @param nvic the nvic to set
   */
  public void setNvic(String nvic) {
    this.nvic = nvic;
  }

  /**
   * @return the renewalMonth
   */
  public String getRenewalMonth() {
    return renewalMonth;
  }

  /**
   * @param renewalMonth the renewalMonth to set
   */
  public void setRenewalMonth(String renewalMonth) {
    this.renewalMonth = renewalMonth;
  }

  /**
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @return the updateEmail
   */
  public Boolean getUpdateEmail() {
    return updateEmail;
  }

  /**
   * @param updateEmail the updateEmail to set
   */
  public void setUpdateEmail(Boolean updateEmail) {
    this.updateEmail = updateEmail;
  }

  /**
   * @return the tnc
   */
  public Boolean getTnc() {
    return tnc;
  }

  /**
   * @param tnc the tnc to set
   */
  public void setTnc(Boolean tnc) {
    this.tnc = tnc;
  }
}
