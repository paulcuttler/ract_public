/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package futuremedium.client.ract.secure.actions.campaign;

import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.common.StreetSuburbVO;
import com.ract.membership.MembershipCardVO;
import com.ract.util.DateTime;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.entities.insurance.car.AjaxReturn;
import futuremedium.client.ract.secure.entities.insurance.car.GaragedAddress;
import futuremedium.client.ract.secure.entities.insurance.car.StreetComparator;
import futuremedium.client.ract.secure.entities.insurance.car.SuburbComparator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author pcock
 */
public class CampaignAjax extends RactActionSupport {
  private final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

  private AjaxReturn ajaxReturn;
  private String postCode;
  private String suburb;
  private String dob;
  private String memberCardNo;
  
  public String suburbList() {
    
    AjaxReturn newAjaxReturnMessage = new AjaxReturn();
    this.setAjaxReturn(newAjaxReturnMessage);
    try {
      
      Iterator iterator = this.getAddressMgr().getStreetSuburbsByPostcode(this.getPostCode()).iterator();
      List<GaragedAddress> garagedAddresses = new ArrayList<GaragedAddress>();
      List<String> list = new ArrayList<String>();
      List<String> keys = new ArrayList<String>();
      while (iterator.hasNext()) {
        StreetSuburbVO streetSuburbVO = (StreetSuburbVO) iterator.next();
        if (!list.contains(streetSuburbVO.getSuburb())) {
          list.add(streetSuburbVO.getSuburb());
        }
      }
      for (String item : list) {
        GaragedAddress garagedAddress = new GaragedAddress();
        garagedAddress.setSuburb(item);
        garagedAddresses.add(garagedAddress);
      }
      Collections.sort(garagedAddresses, new SuburbComparator());
      list.clear();
      for (GaragedAddress garagedAddress : garagedAddresses) {
        list.add(garagedAddress.getSuburb());
        keys.add(garagedAddress.getSuburb());
      }
      // Construct Ajax return object.
      if (list.size() > 0) {
        newAjaxReturnMessage.setValidation(true);
        newAjaxReturnMessage.setTexts(list);
        newAjaxReturnMessage.setValues(keys);
        newAjaxReturnMessage.setMessage("");
      } else {
        newAjaxReturnMessage.setValidation(false);
        newAjaxReturnMessage.setMessage("No suburbs were found for the supplied postcode");
      }
      return SUCCESS;
    } catch (GenericException ex) {
      Config.logger.error(Config.key + ": An Error occurred retrieving suburb list: ", ex);
      newAjaxReturnMessage.setMessage("No suburbs were found for the supplied postcode");
      return SUCCESS;
    }
  }

  public String streetList() {
    AjaxReturn newAjaxReturnMessage = new AjaxReturn();

    if (this.getPostCode() == null || this.getPostCode().isEmpty()) {
      newAjaxReturnMessage.setValidation(false);
    } else {
      try {
        Integer.parseInt(this.getPostCode());
        
        Iterator iterator = this.getAddressMgr().getStreetSuburbsBySuburb(this.getSuburb(), this.getPostCode()).iterator();
        List<GaragedAddress> garagedAddresses = new ArrayList<GaragedAddress>();
        while (iterator.hasNext()) {
          StreetSuburbVO streetSuburbVO = (StreetSuburbVO) iterator.next();
          if (!streetSuburbVO.getStreet().equals("")) {
            GaragedAddress garagedAddress = new GaragedAddress();
            garagedAddress.setStreet(streetSuburbVO.getStreet());
            garagedAddress.setStreetSuburbId(String.valueOf(streetSuburbVO.getStreetSuburbID()));
            garagedAddresses.add(garagedAddress);
          }
        }
        Collections.sort(garagedAddresses, new StreetComparator());
        List<String> list = new ArrayList<String>();
        List<String> keys = new ArrayList<String>();
        for (GaragedAddress garagedAddress : garagedAddresses) {
          list.add(garagedAddress.getStreet());
          keys.add(garagedAddress.getStreetSuburbId());
        }
        if (list.size() > 0) {
          newAjaxReturnMessage.setValidation(true);
          newAjaxReturnMessage.setTexts(list);
          newAjaxReturnMessage.setValues(keys);
        } else {
          newAjaxReturnMessage.setValidation(false);
          newAjaxReturnMessage.setMessage("No streets were found for the supplied suburb");
        }
      } catch (GenericException ex) {
        Config.logger.error(Config.key + ": An Error occurred retrieving suburb list: ", ex);
        newAjaxReturnMessage.setMessage("No suburbs were found for the supplied postcode");
        return SUCCESS;
      }
    }

    this.setAjaxReturn(newAjaxReturnMessage);
    return SUCCESS;
  }

  public String validateMemberCard() {
    AjaxReturn newAjaxReturnMessage = new AjaxReturn();
    try {

      Date dobDate = dateFormat.parse(this.getDob());
      DateTime dobDateTime = new DateTime(dobDate);
      String[] memberCards = this.getMemberCardNo().split(",");

      Config.logger.debug(Config.key + ": member card no: " + this.getMemberCardNo());

      MembershipCardVO card = this.getRoadsideMgr().getMembershipCard(memberCards[0] + memberCards[1] + memberCards[2] + memberCards[3]);

      Config.logger.debug(Config.key + ": member card: " + card);

      if(card != null ){
        
        ClientVO client = this.getCustomerMgr().getClientByMembershipCardNumber(card.getCardNumber());

        Config.logger.debug(Config.key + ": client: " + client);
        Config.logger.debug(Config.key + ": client dob equal: " + client.getBirthDate().equals(dobDateTime));

        if (client != null && client.getBirthDate().equals(dobDateTime)) {
          newAjaxReturnMessage.setValidation(true);
          newAjaxReturnMessage.setMessage("you are a valid member of RACT.");
        } else {
          newAjaxReturnMessage.setValidation(false);
          newAjaxReturnMessage.setMessage("Not valid");
        }

      } else {
        newAjaxReturnMessage.setValidation(false);
        newAjaxReturnMessage.setMessage("Not valid");
      }

      this.setAjaxReturn(newAjaxReturnMessage);  
      return SUCCESS;

    } catch (Exception ex) {
      Config.logger.error(Config.key + ": Error validating member card: ", ex);
      newAjaxReturnMessage.setMessage("Not valid");
      return SUCCESS;
    }
  }

  /**
   * @return the postCode
   */
  public String getPostCode() {
    return postCode;
  }

  /**
   * @param postCode the postCode to set
   */
  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  /**
   * @return the ajaxReturn
   */
  public AjaxReturn getAjaxReturn() {
    return ajaxReturn;
  }

  /**
   * @param ajaxReturn the ajaxReturn to set
   */
  public void setAjaxReturn(AjaxReturn ajaxReturn) {
    this.ajaxReturn = ajaxReturn;
  }

  /**
   * @return the suburb
   */
  public String getSuburb() {
    return suburb;
  }

  /**
   * @param suburb the suburb to set
   */
  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  /**
   * @return the dob
   */
  public String getDob() {
    return dob;
  }

  /**
   * @param dob the dob to set
   */
  public void setDob(String dob) {
    this.dob = dob;
  }

  /**
   * @return the memberCardNo
   */
  public String getMemberCardNo() {
    return memberCardNo;
  }

  /**
   * @param memberCardNo the memberCardNo to set
   */
  public void setMemberCardNo(String memberCardNo) {
    this.memberCardNo = memberCardNo;
  }
}
