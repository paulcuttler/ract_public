/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package futuremedium.client.ract.secure.actions.campaign;

import java.util.*;

import com.ract.client.ClientTitleVO;

import com.ract.common.GenericException;
import com.ract.common.StreetSuburbVO;
import com.ract.web.campaign.CampaignMgrRemote;
import com.ract.web.insurance.GlVehicle;
import com.ract.web.insurance.ListPair;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.actions.data.GlassesGuide;
import futuremedium.client.ract.secure.entities.insurance.car.GaragedAddress;
import futuremedium.client.ract.secure.entities.insurance.car.StreetComparator;
import futuremedium.client.ract.secure.entities.insurance.car.SuburbComparator;


/**
 *
 * @author pcock
 */
public class CampaignForm extends RactActionSupport {
  
  private Collection<ClientTitleVO> titleOptions;

  private String code;
  private String postcode;
  private String suburb;

  private String year;
  private String make;
  private String model;
  private String bodyType;
  private String transmission;
  private String cylinders;
  private String nvic;
  private boolean activeCampaign = true;

  protected Collection<GlVehicle> nvicList;
  protected Collection<ListPair> makeList;
  protected Collection<String> modelList;
  protected Collection<ListPair> bodyTypeList;
  protected Collection<ListPair> transmissionList;
  protected Collection<String> cylinderList;

  private GlVehicle vehicle;

  @Override
  public String execute() {

  if(this.getCode() != null && !this.getCode().isEmpty()) {

    // persist to DB – CampaignVehicle in ract_web_dev
      try {
        CampaignMgrRemote campaignMgr = (CampaignMgrRemote) this.getContext().lookup("CampaignMgr/remote");
        com.ract.web.campaign.Campaign campaign = campaignMgr.retrieveCampaign(this.getCode());

        if(campaign == null || !campaign.isActive()) {
          Config.logger.info(Config.key + ":Campaign: User has entered an invalid code: " + this.getCode());
          this.setActionMessage("noCampaign");
          return SUCCESS;
        } 

      } catch(Exception e) {
        this.setActionMessage("noCampaign");
        Config.logger.error(Config.key + ":CampaignForm: noCampaign: ", e);
      }
    }

    return SUCCESS;
  }


  public List<String> getSuburbList() {

    List<String> list = new ArrayList<String>();

    if(this.getPostcode() != null && !this.getPostcode().isEmpty()) {
      try {
   
        Iterator iterator = this.getAddressMgr().getStreetSuburbsByPostcode(this.getPostcode()).iterator();
        List<GaragedAddress> garagedAddresses = new ArrayList<GaragedAddress>();
        List<String> keys = new ArrayList<String>();
        while (iterator.hasNext()) {
          StreetSuburbVO streetSuburbVO = (StreetSuburbVO) iterator.next();
          if (!list.contains(streetSuburbVO.getSuburb())) {
            list.add(streetSuburbVO.getSuburb());
          }
        }
        for (String item : list) {
          GaragedAddress garagedAddress = new GaragedAddress();
          garagedAddress.setSuburb(item);
          garagedAddresses.add(garagedAddress);
        }
        Collections.sort(garagedAddresses, new SuburbComparator());
        list.clear();
        for (GaragedAddress garagedAddress : garagedAddresses) {
          list.add(garagedAddress.getSuburb());
          keys.add(garagedAddress.getSuburb());
        }
    

      } catch (GenericException ex) {
        Config.logger.error(Config.key + ":CampaignForm:  An Error occurred retrieving suburb list: ", ex);
      }
    }
    return list;
  }

  public Map<String,String> getStreetList() {

    Map<String,String> list = new HashMap<String,String>();

    if (this.getPostcode() != null && !this.getPostcode().isEmpty()) {
      
      try {

        Integer.parseInt(this.getPostcode());
        Iterator iterator = this.getAddressMgr().getStreetSuburbsBySuburb(this.getSuburb(), this.getPostcode()).iterator();
        List<GaragedAddress> garagedAddresses = new ArrayList<GaragedAddress>();

        while (iterator.hasNext()) {
          StreetSuburbVO streetSuburbVO = (StreetSuburbVO) iterator.next();
          if (!streetSuburbVO.getStreet().equals("")) {
            GaragedAddress garagedAddress = new GaragedAddress();
            garagedAddress.setStreet(streetSuburbVO.getStreet());
            garagedAddress.setStreetSuburbId(String.valueOf(streetSuburbVO.getStreetSuburbID()));
            garagedAddresses.add(garagedAddress);
          }
        }
        Collections.sort(garagedAddresses, new StreetComparator());

        for (GaragedAddress garagedAddress : garagedAddresses) {
          list.put(garagedAddress.getStreetSuburbId(),garagedAddress.getStreet());
        }


      } catch (GenericException ex) {
        Config.logger.error(Config.key + ": An Error occurred retrieving suburb list: ", ex);
      }
    }

    return list;
  }



  @SuppressWarnings("unchecked")
  public Collection<ClientTitleVO> getTitleOptions() {

    this.titleOptions = new ArrayList<ClientTitleVO>();

    try {
      this.titleOptions = this.getCustomerMgr().getClientTitles(true, false);
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to populate title options: ", e);
    }
    return titleOptions;
  }

  /**
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @return the postCode
   */
  public String getPostcode() {
    return postcode;
  }

  /**
   * @param postCode the postCode to set
   */
  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  /**
   * @return the suburb
   */
  public String getSuburb() {
    return suburb;
  }

  /**
   * @param suburb the suburb to set
   */
  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  /**
   * @param titleOptions the titleOptions to set
   */
  public void setTitleOptions(Collection<ClientTitleVO> titleOptions) {
    this.titleOptions = titleOptions;
  }

  /**
   * @return the year
   */
  public String getYear() {
    return year;
  }

  /**
   * @param year the year to set
   */
  public void setYear(String year) {
    this.year = year;
  }

  /**
   * @return the make
   */
  public String getMake() {
    return make;
  }

  /**
   * @param make the make to set
   */
  public void setMake(String make) {
    this.make = make;
  }

  /**
   * @return the model
   */
  public String getModel() {
    return model;
  }

  /**
   * @param model the model to set
   */
  public void setModel(String model) {
    this.model = model;
  }

  /**
   * @return the bodyType
   */
  public String getBodyType() {
    return bodyType;
  }

  /**
   * @param bodyType the bodyType to set
   */
  public void setBodyType(String bodyType) {
    this.bodyType = bodyType;
  }

  /**
   * @return the transmission
   */
  public String getTransmission() {
    return transmission;
  }

  /**
   * @param transmission the transmission to set
   */
  public void setTransmission(String transmission) {
    this.transmission = transmission;
  }

  /**
   * @return the cylinders
   */
  public String getCylinders() {
    return cylinders;
  }

  /**
   * @param cylinders the cylinders to set
   */
  public void setCylinders(String cylinders) {
    this.cylinders = cylinders;
  }

  /**
   * @return the nvic
   */
  public String getNvic() {
    return nvic;
  }

  /**
   * @param nvic the nvic to set
   */
  public void setNvic(String nvic) {
    this.nvic = nvic;
  }

  /**
    * @return the vehicleList
   */
  public Collection<GlVehicle> getNvicList() {
    if (nvicList == null) {
      return new ArrayList<GlVehicle>();
    }
    else {
      return nvicList;
    }
  }

  /**
   * @param vehicleList the vehicleList to set
   */
  public void setNvicList(Collection<GlVehicle> nvicList) {
    this.nvicList = nvicList;
  }

  /**
   * @return the makeList
   */
  public Collection<ListPair> getMakeList() {
    if (makeList == null) {
      return new ArrayList<ListPair>();
    }
    else {
      return makeList;
    }
  }

  /**
   * @param makeList the makeList to set
   */
  public void setMakeList(Collection<ListPair> makeList) {
    this.makeList = makeList;
  }

  /**
   * @return the modelList
   */
  public Collection<String> getModelList() {
    if (modelList == null) {
      return new ArrayList<String>();
    }
    else {
      return modelList;
    }
  }

  /**
   * @param modelList the modelList to set
   */
  public void setModelList(Collection<String> modelList) {
    this.modelList = modelList;
  }

  /**
   * @return the bodyTypeList
   */
  public Collection<ListPair> getBodyTypeList() {
    if (bodyTypeList == null) {
      return new ArrayList<ListPair>();
    }
    else {
      return bodyTypeList;
    }
  }

  /**
   * @param bodyTypeList the bodyTypeList to set
   */
  public void setBodyTypeList(Collection<ListPair> bodyTypeList) {
    this.bodyTypeList = bodyTypeList;
  }

  /**
   * @return the transmissionList
   */
  public Collection<ListPair> getTransmissionList() {
    if (transmissionList == null) {
      return new ArrayList<ListPair>();
    }
    else {
      return transmissionList;
    }
  }

  /**
   * @param transmissionList the transmissionList to set
   */
  public void setTransmissionList(Collection<ListPair> transmissionList) {
    this.transmissionList = transmissionList;
  }

  /**
   * @return the cylinderList
   */
  public Collection<String> getCylinderList() {
    if (cylinderList == null) {
      return new ArrayList<String>();
    }
    else {
      return cylinderList;
    }
  }

  /**
   * @param cylinderList the cylinderList to set
   */
  public void setCylinderList(Collection<String> cylinderList) {
    this.cylinderList = cylinderList;
  }

  /**
   * @return the activeCampaign
   */
  public boolean isActiveCampaign() {

  try {

      if(this.getCode() != null && this.getCode().isEmpty()){
        CampaignMgrRemote campaignMgr = (CampaignMgrRemote) this.getContext().lookup("CampaignMgr/remote");
        com.ract.web.campaign.Campaign campaign = campaignMgr.retrieveCampaign(this.getCode());

        if(campaign == null || !campaign.isActive()) {
          this.activeCampaign = false;
        }
      }

    } catch(Exception e) {
      Config.logger.error(Config.key + ": can not create campaignMgr: ", e);
    }

    return activeCampaign;
  }

  /**
   * @param activeCampaign the activeCampaign to set
   */
  public void setActiveCampaign(boolean activeCampaign) {
    this.activeCampaign = activeCampaign;
  }
}
