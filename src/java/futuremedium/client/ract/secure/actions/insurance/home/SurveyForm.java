package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * Optional Customer Survey form.
 * @author gnewton
 */
public class SurveyForm extends SurveyBase {

  @Override
  public String getPageTitle() {
    return "Rate the process";
  }
}
