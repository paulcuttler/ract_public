package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * Display the final page of the quote.
 * @author gnewton
 */
public class FinalisePolicyForm extends PolicySupport implements PolicyAware, PolicyFinalisedAware {
	
	private Integer docQuoteNo;
	
	@Override
	public String execute() throws Exception {
		
		this.setDocQuoteNo(this.getQuoteNo());
		
		return SUCCESS;
	}
	
	/**
   * Retrieve quoteNo from session.
   * @return the quoteNo
   */
  public int getDocQuoteNo() {
    if (this.session != null && this.session.containsKey(TOKEN_DOC_QUOTE_NO)) {
      this.docQuoteNo = (Integer) this.session.get(TOKEN_DOC_QUOTE_NO);
    }
    return this.docQuoteNo;
  }

  /**
   * Store quoteNo in session.
   * @param quoteNo the quoteNo to set
   */
  public void setDocQuoteNo(int docQuoteNo) {
    this.docQuoteNo = docQuoteNo;
    if (this.session != null) {
      this.session.put(TOKEN_DOC_QUOTE_NO, docQuoteNo);
    }
  }
}
