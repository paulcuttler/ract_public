package futuremedium.client.ract.secure.actions.insurance.home;

/**
 *
 * @author gnewton
 */
public class SpecifiedContentsForm extends SpecifiedContentsBase implements PolicyAware {
    private static final long serialVersionUID = -4859509676524521996L;
  private Boolean specify;

  /**
   * @return the specify
   */
  public Boolean getSpecify() {
    if (this.specify == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_SPECIFY);
      if (value == null) {
        this.specify = null; // neither option selected
      } else {
        this.specify = Boolean.parseBoolean(value);
      }
    }
    return specify;
  }

  /**
   * @param specify the specify to set
   */
  public void setSpecify(Boolean specify) {
    this.specify = specify;
  }

}
