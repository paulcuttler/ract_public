package futuremedium.client.ract.secure.actions.insurance.home;

import com.ract.web.insurance.WebInsQuoteDetail;

/**
 * Pre-populate Sum Insured Form.
 * @author gnewton
 */
public class SumInsuredForm extends QuoteSubTypeSupport implements QuoteAware {

  private String contentsSumInsured;

  /**
   * @return the buildingSumInsured
   */
  @Override
  public Integer getBuildingSumInsured() {
    this.buildingSumInsured = super.getBuildingSumInsured();
    if (this.buildingSumInsured == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.BLDG_SUM_INSURED);
      if (value != null) {
        this.buildingSumInsured = Integer.parseInt(value);
      }
    }
    return buildingSumInsured;
  }

  /**
   * @return the investorSumInsured
   */
  @Override
  public Integer getInvestorSumInsured() {
    this.investorSumInsured = super.getInvestorSumInsured();
    if (this.investorSumInsured == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.INV_SUM_INSURED);
      if (value != null) {
        this.investorSumInsured = Integer.parseInt(value);
      }
    }
    return investorSumInsured;
  }

  /**
   * @return the contentsSumInsured
   */
  public String getContentsSumInsured() {
    if (this.contentsSumInsured == null) {
      this.contentsSumInsured = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.CNTS_SUM_INSURED);
    }
    return contentsSumInsured;
  }

  /**
   * @param contentsSumInsured the contentsSumInsured to set
   */
  public void setContentsSumInsured(String contentsSumInsured) {
    this.contentsSumInsured = contentsSumInsured;
  }

}
