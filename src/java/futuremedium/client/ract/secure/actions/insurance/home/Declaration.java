package futuremedium.client.ract.secure.actions.insurance.home;

import com.opensymphony.xwork2.validator.annotations.*;
import com.ract.web.insurance.WebInsQuoteDetail;

/**
 *
 * @author gnewton
 */
public class Declaration extends PolicySupport implements PolicyAware {
    private Boolean notWithheld;
    private Boolean policyApplies;
    private boolean agree;

    @Override
    public String execute() throws Exception {
        boolean ok = this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.TERMS_AND_CONDITIONS_ACCEPTED, Boolean.TRUE.toString());
        if (ok) {
            // this.setActionMessage(this.getSuccessMessage());
        } else {
            this.setActionMessage(this.getErrorMessage());
            return ERROR;
        }

        return SUCCESS;
    }

  /**
   * @return the notWithheld
   */
  public Boolean getNotWithheld() {
    return notWithheld;
  }

  /**
   * @param notWithheld the notWithheld to set
   */
  @FieldExpressionValidator(message="You must accept this condition", expression="notWithheld == true")
  public void setNotWithheld(Boolean notWithheld) {
    this.notWithheld = notWithheld;
  }

  /**
   * @return the policyApplies
   */
  public Boolean getPolicyApplies() {
    return policyApplies;
  }

  /**
   * @param policyApplies the policyApplies to set
   */
  @FieldExpressionValidator(message="You must accept this condition", expression="policyApplies == true")
  public void setPolicyApplies(boolean policyApplies) {
    this.policyApplies = policyApplies;
  }

  /**
   * @return the agree
   */
  public boolean isAgree() {
    return agree;
  }

  /**
   * @param agree the agree to set
   */
  @FieldExpressionValidator(message="You must agree to the terms and conditions", expression="agree == true")
  public void setAgree(boolean agree) {
    this.agree = agree;
  }

}
