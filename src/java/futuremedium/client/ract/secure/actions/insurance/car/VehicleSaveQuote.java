package futuremedium.client.ract.secure.actions.insurance.car;

import com.opensymphony.xwork2.validator.annotations.*;

import futuremedium.client.ract.secure.EmailManager;

/**
 * Record user's email address and email quote number.
 * @author gnewton
 */
public class VehicleSaveQuote extends QuotePageSave {
    private static final long serialVersionUID = 7631254073165675129L;
  private String email;
  public static final String SAVED_QUOTE_EMAIL = "savedQuoteEmail";
  private boolean complete = false;

  @Override
  public String execute() throws Exception {
    boolean ok;
    this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), SAVED_QUOTE_EMAIL, this.getEmail());
    ok = EmailManager.sendVehicleSavedQuoteEmail(this);

    if (ok) {
      complete = true;
      //this.setActionMessage("Your quote number has been sent to your email address.");
    } else {
      this.setActionMessage("An error occurred sending an email to your email address");
      return ERROR;
    }
    
    return SUCCESS;
  }


  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  @RequiredStringValidator(message = "Please enter your email address", shortCircuit=true)
  @EmailValidator(message = "Please enter a valid email address")
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the complete
   */
  public boolean isComplete() {
    return complete;
  }

  /**
   * @param complete the complete to set
   */
  public void setComplete(boolean complete) {
    this.complete = complete;
  }
  
  
  
}
