package futuremedium.client.ract.secure.actions.insurance.home;

import futuremedium.client.ract.secure.Config;

/**
 * Store sum insured value in session.
 * @author gnewton
 */
public class Calculator extends QuoteSubTypeSupport implements PersistanceAware {

  private Integer sumInsured;

  @Override
  public String execute() throws Exception {
    if (this.isBuildingOptionsRequired()) {
      this.setBuildingSumInsured(this.getSumInsured());
    } else if (this.isInvestorOptionsRequired()) {
      this.setInvestorSumInsured(this.getSumInsured());
    }

    this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_USED_CALCULATOR, "true", null, false); //dont update last action.

    return SUCCESS;
  }

  /**
   * Return user to Sum Insured Page if they have gone beyond the page before it, or Checklist page.
   */
  @Override
  public String getNextAction() {
    String nextAction;
    String sumInsuredFormAction = ActionBase.FORM_ACTION_PREFIX + SumInsuredForm.class.getSimpleName();
    
    if (this.getFlow().indexOf(this.getLastFormActionName()) >= this.getFlow().indexOf(sumInsuredFormAction) - 1) {
      nextAction = sumInsuredFormAction;
    } else {
      nextAction = ActionBase.FORM_ACTION_PREFIX + ChecklistForm.class.getSimpleName();
    }

    Config.logger.info(Config.key + ": Calculator forcing next action as " + nextAction);

    return nextAction;
  }

  /**
   * @return the sumInsured
   */
  public Integer getSumInsured() {
    return sumInsured;
  }

  /**
   * @param sumInsured the sumInsured to set
   */
  public void setSumInsured(Integer sumInsured) {
    this.sumInsured = sumInsured;
  }
}
