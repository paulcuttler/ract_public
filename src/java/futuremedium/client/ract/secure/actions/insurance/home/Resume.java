package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * Return a user to where they should be in the quote.
 * @author gnewton
 */
public class Resume extends ActionBase {

  @Override
  public String execute() throws Exception {
    this.setLastActionName(this.getHomeService().retrieveLastAction(this.getQuoteNo()));
    
    this.setNextAction(this.getHomeService().calculateNextAction(this));

    return SUCCESS;
  }
}
