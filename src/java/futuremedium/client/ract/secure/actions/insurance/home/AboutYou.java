package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.*;

import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.util.DateTime;
import com.ract.web.insurance.*;

import futuremedium.client.ract.secure.Config;

/**
 *
 * @author gnewton
 */
public class AboutYou extends ActionBase implements QuoteAware {
    private static final long serialVersionUID = -4160189955863043401L;
    private Boolean applyPip;
  private Boolean fiftyPlus;
  private Boolean member;
  private String dobDay;
  private String dobMonth;
  private String dobYear;
  private String memberDobDay;
  private String memberDobMonth;
  private String memberDobYear;
  private String membershipCard1;
  private String membershipCard2;
  private String membershipCard3;
  private String membershipCard4;

  @Override
  public String execute() throws Exception {

    boolean ok = this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_IS_MEMBER, (this.isMember() != null ? this.isMember().toString() : ""));
    ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_MEMBER_CARD, this.getMembershipCard());
    ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_MEMBER_DOB, this.getMemberDob());
    ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_OLDEST_DOB, this.getDob());
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.PIP_DISCOUNT, (this.isApplyPip() != null ? this.isApplyPip().toString() : ""));
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.FIFTY_PLUS, (this.isFiftyPlus() != null ? this.isFiftyPlus().toString() : ""));

    // check card number attempts
    if (this.getMemberCardAttempts() > Integer.parseInt(Config.configManager.getProperty("insurance.home.maxMemberNumberAttempts", "1"))) {
      this.resetMemberCardAttempts();      
      return this.getHomeService().abortQuote(this,
              "multiple incorrect member card number/dob combo",
              this.getMembershipCard() + " / " + this.getMemberDob(),
              Config.configManager.getProperty("insurance.home.exclusion.memberCard.message"));
    }

    // check oldest dob attempts
    if (this.getOldestDobAttempts() > Integer.parseInt(Config.configManager.getProperty("insurance.home.maxOldestDobAttempts", "1"))) {
      this.resetOldestDobAttempts();
      return this.getHomeService().abortQuote(this,
              "oldest dob outside of range",
              this.getDob(),
              Config.configManager.getProperty("insurance.home.exclusion.dob.message"));
    }

    // reset attempts
    this.resetMemberCardAttempts();
    this.resetOldestDobAttempts();

    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    return SUCCESS;
  }

  @Override
  public void validate() {

    // nominated as member
    if (this.isMember() != null && this.isMember()) {

      // check card number
      if (this.getMembershipCard() != null
              && REGEXP_MEMBER_CARD.matcher(this.getMembershipCard()).matches()) {

        // check member dob
        if (this.getMemberDob() != null) {
          this.validateDob(this.getMemberDob(), 16, 110, "memberDobDay", "date of birth");

          // check valid member
          WebInsClient client = this.getHomeService().attachMember(this);
          if (client == null) {
            this.addFieldError("membershipCard1", "The card number or date of birth you have entered is incorrect, please try again.");
            this.addFieldError("memberDobDay", "The card number or date of birth you have entered is incorrect, please try again.");
            this.incrementMemberCardAttempts();
          } else {
            this.resetMemberCardAttempts();
          }

        } else {
          this.addFieldError("memberDobDay", "Please enter your date of birth.");
        }

      } else {
        this.addFieldError("membershipCard1", "Please enter a valid card number.");

        if (this.getMemberDob() != null) {
          this.validateDob(this.getMemberDob(), 16, 110, "memberDobDay", "date of birth");
        } else {
          this.addFieldError("memberDobDay", "Please enter your date of birth.");
        }
      }

    } else {
      this.resetMemberCardAttempts();
    }

    // check dob
    if (this.getDob() == null) {
      this.addFieldError("dobDay", "Please enter the date of birth of the Oldest Owner.");
    } else {
      this.validateDob(this.getDob(), 16, 110, "dobDay", "date of birth of the Oldest Owner");
      if (this.getFieldErrors().containsKey("dobDay")) {
        // change input message if dob is valid but out of range (age of -1 means dob could not be parsed)
        if (this.getAge() >= 0) {
          this.incrementOldestDobAttempts();
          this.removeFieldError("dobDay");
          this.addFieldError("dobDay", "The date of birth indicates you are " + this.getAge() + " - please adjust if this is incorrect");
        }
      } else {
        // dob ok, check 50+
        this.resetOldestDobAttempts();
        if (this.getAge() >= 50) {
          if (this.isFiftyPlus() == null) {
            this.addFieldError("fiftyPlus", "Please answer this question");
          }
        } else {
          this.setFiftyPlus(false);
        }
      }
    }
    
    // lastly, remove dob and membership card errors if those errors are all that remain and max attempts have been exceeded

    // remove field error, allow execute to generate NOTICE
    if (this.getOldestDobAttempts() > 1 && this.getFieldErrors().size() == 1
            && this.getFieldErrors().containsKey("dobDay")) {
      this.removeFieldError("dobDay");
    }

    // remove field errors, allow execute to generate NOTICE
    if (this.isMember() != null && this.isMember()) {
      if (this.getMemberCardAttempts() > 1 && this.getFieldErrors().size() == 2
              && this.getFieldErrors().containsKey("membershipCard1")
              && this.getFieldErrors().containsKey("memberDobDay")
              ) {
        this.removeFieldError("membershipCard1");
        this.removeFieldError("memberDobDay");
      }
    }

    // remove field errors, allow execute to generate NOTICE
    if (this.isMember() != null && this.isMember()) {
      if (this.getMemberCardAttempts() > 1 && this.getOldestDobAttempts() > 1
      				&& this.getFieldErrors().size() == 3
              && this.getFieldErrors().containsKey("membershipCard1")
              && this.getFieldErrors().containsKey("memberDobDay")
              && this.getFieldErrors().containsKey("dobDay")) {
        this.removeFieldError("dobDay");
        this.removeFieldError("membershipCard1");
        this.removeFieldError("memberDobDay");
      }
    }
  }
  
  /**
   * @return the applyPip
   */
  public Boolean isApplyPip() {
      return applyPip;
  }
  
  /**
   * @param applyPip the applyPip to set
   */
  public void setApplyPip(Boolean applyPip) {
      this.applyPip = applyPip;
  }

  /**
   * @return the fiftyPlus
   */
  public Boolean isFiftyPlus() {
    return fiftyPlus;
  }

  /**
   * @param fiftyPlus the fiftyPlus to set
   */
  public void setFiftyPlus(Boolean fiftyPlus) {
    this.fiftyPlus = fiftyPlus;
  }

  /**
   * @return the member
   */
  public Boolean isMember() {
    return member;
  }

  /**
   * @param member the member to set
   */
  @RequiredFieldValidator(message="Please answer this question", type=ValidatorType.SIMPLE)
  public void setMember(Boolean member) {
    this.member = member;
  }

  /**
   * @return the dobDay
   */
  public String getDobDay() {
    return dobDay;
  }

  /**
   * @param dobDay the dobDay to set
   */
  public void setDobDay(String dobDay) {
    this.dobDay = dobDay;
  }

  /**
   * @return the dobMonth
   */
  public String getDobMonth() {
    return dobMonth;
  }

  /**
   * @param dobMonth the dobMonth to set
   */
  public void setDobMonth(String dobMonth) {
    this.dobMonth = dobMonth;
  }

  /**
   * @return the dobYear
   */
  public String getDobYear() {
    return dobYear;
  }

  /**
   * @param dobYear the dobYear to set
   */
  public void setDobYear(String dobYear) {
    this.dobYear = dobYear;
  }

  private String getDob() {
    String dob = null;
    if (this.getDobDay() != null && this.getDobMonth() != null && this.getDobYear() != null) {
      dob = this.getDobDay() + "/" + this.getDobMonth() + "/" + this.getDobYear();
    }
    if (dob != null && dob.equals("//")) {
      dob = null;
    }

    return dob;
  }

  private int getAge() {
    int age = -1;
    try {
      DateTime dobDateTime = new DateTime(this.getDob());
      Calendar today = Calendar.getInstance();
      Calendar dob = Calendar.getInstance();
      dob.setTimeInMillis(dobDateTime.getTime());

      age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
      if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
        age--;
      }
      // Mop up silly dates like 13/04/2011 when today's date is 01/03/2011, which would give age as -1 which is treated as a parse error and handled differently
      if (age < 0) {
        age = 0;
      }
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to parse oldest dob: " + this.getDob(), e);
    }

    return age;
  }

  /**
   * @return the memberDobDay
   */
  public String getMemberDobDay() {
    return memberDobDay;
  }

  /**
   * @param memberDobDay the memberDobDay to set
   */
  public void setMemberDobDay(String memberDobDay) {
    this.memberDobDay = memberDobDay;
  }

  /**
   * @return the memberDobMonth
   */
  public String getMemberDobMonth() {
    return memberDobMonth;
  }

  /**
   * @param memberDobMonth the memberDobMonth to set
   */
  public void setMemberDobMonth(String memberDobMonth) {
    this.memberDobMonth = memberDobMonth;
  }

  /**
   * @return the memberDobYear
   */
  public String getMemberDobYear() {
    return memberDobYear;
  }

  /**
   * @param memberDobYear the memberDobYear to set
   */
  public void setMemberDobYear(String memberDobYear) {
    this.memberDobYear = memberDobYear;
  }

  private String getMemberDob() {
    String memberDob = null;
    if (this.getMemberDobDay() != null && this.getMemberDobMonth() != null && this.getMemberDobYear() != null) {
      memberDob = this.getMemberDobDay() + "/" + this.getMemberDobMonth() + "/" + this.getMemberDobYear();
    }
    if (memberDob != null && memberDob.equals("//")) {
      memberDob = null;
    }

    return memberDob;
  }

  public DateTime getMemberDobDateTime() {
    DateTime date =  null;
    try {
      date = new DateTime(getMemberDob());
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to parse member dob: " + this.getMemberDob(), e);
    }

    return date;
  }

  /**
   * @return the membershipCard1
   */
  public String getMembershipCard1() {
    return membershipCard1;
  }

  /**
   * @param membershipCard1 the membershipCard1 to set
   */
  public void setMembershipCard1(String membershipCard1) {
    this.membershipCard1 = membershipCard1;
  }

  /**
   * @return the membershipCard2
   */
  public String getMembershipCard2() {
    return membershipCard2;
  }

  /**
   * @param membershipCard2 the membershipCard2 to set
   */
  public void setMembershipCard2(String membershipCard2) {
    this.membershipCard2 = membershipCard2;
  }

  /**
   * @return the membershipCard3
   */
  public String getMembershipCard3() {
    return membershipCard3;
  }

  /**
   * @param membershipCard3 the membershipCard3 to set
   */
  public void setMembershipCard3(String membershipCard3) {
    this.membershipCard3 = membershipCard3;
  }

  /**
   * @return the membershipCard4
   */
  public String getMembershipCard4() {
    return membershipCard4;
  }

  /**
   * Get complete membership card number
   */
  public String getMembershipCard() {
    return this.getMembershipCard1() + this.getMembershipCard2() + this.getMembershipCard3() + this.getMembershipCard4();
  }

  /**
   * @param membershipCard4 the membershipCard4 to set
   */
  public void setMembershipCard4(String membershipCard4) {
    this.membershipCard4 = membershipCard4;
  }

}
