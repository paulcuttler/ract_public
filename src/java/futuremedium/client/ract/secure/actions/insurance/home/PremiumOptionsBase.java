package futuremedium.client.ract.secure.actions.insurance.home;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.Premium;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;

/**
 * Common fields for Premium/Options form and processing.
 * @author gnewton
 */
public class PremiumOptionsBase extends QuoteSubTypeSupport {
    private static final long serialVersionUID = -3274088546022075367L;
    
  static private final String TOKEN_BLDG_EXCESS_OPTIONS = ActionBase.VALUE_QUOTE_TYPE_BLDG_ONLY.toUpperCase() + "_EXCESS_OPTIONS";
  static private final String TOKEN_CNTS_EXCESS_OPTIONS = ActionBase.VALUE_QUOTE_TYPE_CNTS_ONLY.toUpperCase() + "_EXCESS_OPTIONS";
  static private final String TOKEN_INV_EXCESS_OPTIONS = ActionBase.VALUE_QUOTE_TYPE_INV.toUpperCase() + "_EXCESS_OPTIONS";
  
  static public final String VALUE_BLDG_DWC = ActionBase.VALUE_QUOTE_TYPE_BLDG_ONLY.toUpperCase() + "_DWC";
  static public final String VALUE_BLDG_ACCIDENTAL_DAMAGE = "BLDGAD";
  static public final String VALUE_BLDG_ELECTRIC_MOTORS = "BLDGFUSION";
  static public final String VALUE_BLDG_STORM_DAMAGE = "BLDGSTORM";

  static public final String VALUE_CNTS_PERSONAL_EFFECTS = ActionBase.VALUE_QUOTE_TYPE_CNTS_ONLY.toUpperCase() + "_PersonalEffects";
  static public final String VALUE_CNTS_DWC = "CNTSDWC";
  static public final String VALUE_CNTS_ACCIDENTAL_DAMAGE = "CNTSAD";
  static public final String VALUE_CNTS_ELECTRIC_MOTORS = "CNTSFUSION";
  
  static public final String VALUE_INV_STORM_DAMAGE = "PINVSTORM";
  static public final String VALUE_INV_ELECTRIC_MOTORS = "PINVBLDFSN";
  static public final String VALUE_INV_CONTENTS_ELECTRIC_MOTORS = "PINVCNTFSN";
  static public final String VALUE_INV_CONTENTS_COVER_10 = ActionBase.VALUE_QUOTE_TYPE_INV.toUpperCase() + "_ContentsCover10";
  static public final String VALUE_INV_CONTENTS_COVER_30 = ActionBase.VALUE_QUOTE_TYPE_INV.toUpperCase() + "_ContentsCover30";
  static public final String VALUE_INV_CONTENTS_COVER_50 = ActionBase.VALUE_QUOTE_TYPE_INV.toUpperCase() + "_ContentsCover50";

  private List<InRfDet> buildingInsuranceOptions;
  private List<InRfDet> contentsInsuranceOptions;
  private List<InRfDet> investorInsuranceOptions;
  private List<InRfDet> investorContentsInsuranceOptions;
  private List<InRfDet> investorContentsSumInsuredOptions;

  protected BigDecimal totalAnnualPremium;
  protected BigDecimal totalMonthlyPremium;

  private String buildingAccidentalDamagePercentage;
  private String contentsAccidentalDamagePercentage;
  
  private BigDecimal buildingMotorDamageCost;
  private BigDecimal contentsMotorDamageCost;
  private BigDecimal investorMotorDamageCost;
  private BigDecimal investorContentsMotorDamageCost;

  private BigDecimal buildingStormDamageCost;
  private BigDecimal investorStormDamageCost;
  
  private BigDecimal workersCompensationCost;
  private Boolean allowUnspecifiedPersonalEffects;
  private BigDecimal investorCover30Cost;
  private BigDecimal investorCover50Cost;

  private String buildingDefaultExcess;
  private List<InRfDet> buildingExcessOptions;
  private String contentsDefaultExcess;
  private List<InRfDet> contentsExcessOptions;
  private String investorDefaultExcess;
  private List<InRfDet> investorExcessOptions;

  /**
   * Retrieve or populate buildingExcessOptions from session.
   * @return the excessOptions
   */
  @SuppressWarnings("unchecked")
  public List<InRfDet> getBuildingExcessOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_BLDG_EXCESS_OPTIONS)) { // retrieve from session
        this.buildingExcessOptions = (List<InRfDet>) this.session.get(TOKEN_BLDG_EXCESS_OPTIONS);
      } else { // initialise in session
        this.buildingExcessOptions = this.getOptionService().getExcessOptions(this, WebInsQuote.COVER_BLDG);
        this.session.put(TOKEN_BLDG_EXCESS_OPTIONS, this.buildingExcessOptions);
      }
    } else {
      this.buildingExcessOptions = this.getOptionService().getExcessOptions(this, WebInsQuote.COVER_BLDG);
    }
    return buildingExcessOptions;
  }

  /**
   * @param buildingExcessOptions the buildingExcessOptions to set
   */
  public void setBuildingExcessOptions(List<InRfDet> buildingExcessOptions) {
    this.buildingExcessOptions = buildingExcessOptions;
    if (this.session != null) {
      this.session.put(TOKEN_BLDG_EXCESS_OPTIONS, this.buildingExcessOptions);
    }
  }

  /**
   * @return the buildingDefaultExcess
   */
  public String getBuildingDefaultExcess() throws Exception {
    if (this.buildingDefaultExcess == null) {
      for (InRfDet option : this.getBuildingExcessOptions()) {
        if (option.isAcceptable()) {
          buildingDefaultExcess = option.getrClass();
          break;
        }
      }
    }
    // @todo Home: remove temporary building excess fallback
    if (this.buildingDefaultExcess == null) {
      this.buildingDefaultExcess = Config.configManager.getProperty("insurance.home.excess.fallback");
      Config.logger.warn(Config.key + ": unable to lookup default Building Excess, falling back to $" + this.buildingDefaultExcess);
    }

    return buildingDefaultExcess;
  }

  /**
   * @param buildingDefaultExcess the buildingDefaultExcess to set
   */
  public void setBuildingDefaultExcess(String buildingDefaultExcess) {
    this.buildingDefaultExcess = buildingDefaultExcess;
  }

  /**
   * Retrieve or populate contentsExcessOptions from session.
   * @return the excessOptions
   */
  @SuppressWarnings("unchecked")
  public List<InRfDet> getContentsExcessOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_CNTS_EXCESS_OPTIONS)) { // retrieve from session
        this.contentsExcessOptions = (List<InRfDet>) this.session.get(TOKEN_CNTS_EXCESS_OPTIONS);
      } else { // initialise in session
        this.contentsExcessOptions = this.getOptionService().getExcessOptions(this, WebInsQuote.COVER_CNTS);
        this.session.put(TOKEN_CNTS_EXCESS_OPTIONS, this.contentsExcessOptions);
      }
    } else {
      this.contentsExcessOptions = this.getOptionService().getExcessOptions(this, WebInsQuote.COVER_CNTS);
    }
    return contentsExcessOptions;
  }

  /**
   * @param contentsExcessOptions the contentsExcessOptions to set
   */
  public void setContentsExcessOptions(List<InRfDet> contentsExcessOptions) {
    this.contentsExcessOptions = contentsExcessOptions;
    if (this.session != null) {
      this.session.put(TOKEN_CNTS_EXCESS_OPTIONS, this.contentsExcessOptions);
    }
  }

  /**
   * @return the contentsDefaultExcess
   */
  public String getContentsDefaultExcess() throws Exception {
    if (this.contentsDefaultExcess == null) {
      for (InRfDet option : this.getContentsExcessOptions()) {
        if (option.isAcceptable()) {
          contentsDefaultExcess = option.getrClass();
          break;
        }
      }
    }
    // @todo Home: remove temporary contents excess fallback
    if (this.contentsDefaultExcess == null) {
      this.contentsDefaultExcess = Config.configManager.getProperty("insurance.home.excess.fallback");
      Config.logger.warn(Config.key + ": unable to lookup default Contents Excess, falling back to $" + this.contentsDefaultExcess);
    }
    return contentsDefaultExcess;
  }

  /**
   * @param contentsDefaultExcess the contentsDefaultExcess to set
   */
  public void setContentsDefaultExcess(String contentsDefaultExcess) {
    this.contentsDefaultExcess = contentsDefaultExcess;
  }

  /**
   * Retrieve or populate investorExcessOptions from session.
   * @return the excessOptions
   */
  @SuppressWarnings("unchecked")
  public List<InRfDet> getInvestorExcessOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_INV_EXCESS_OPTIONS)) { // retrieve from session
        this.investorExcessOptions = (List<InRfDet>) this.session.get(TOKEN_INV_EXCESS_OPTIONS);
      } else { // initialise in session
        this.investorExcessOptions = this.getOptionService().getExcessOptions(this, WebInsQuote.COVER_INV);
        this.session.put(TOKEN_INV_EXCESS_OPTIONS, this.investorExcessOptions);
      }
    } else {
      this.investorExcessOptions = this.getOptionService().getExcessOptions(this, WebInsQuote.COVER_INV);
    }
    return investorExcessOptions;
  }

  /**
   * @param investorExcessOptions the investorExcessOptions to set
   */
  public void setInvestorExcessOptions(List<InRfDet> investorExcessOptions) {
    this.investorExcessOptions = investorExcessOptions;
    if (this.session != null) {
      this.session.put(TOKEN_INV_EXCESS_OPTIONS, this.investorExcessOptions);
    }
  }

  /**
   * @return the investorDefaultExcess
   */
  public String getInvestorDefaultExcess() throws Exception {
    if (this.investorDefaultExcess == null) {
      for (InRfDet option : this.getInvestorExcessOptions()) {
        if (option.isAcceptable()) {
          investorDefaultExcess = option.getrClass();
          break;
        }
      }
    }
    // @todo Home: remove temporary investor excess fallback
    if (this.investorDefaultExcess == null) {
      this.investorDefaultExcess = Config.configManager.getProperty("insurance.home.excess.fallback");
      Config.logger.warn(Config.key + ": unable to lookup default Investor Excess, falling back to $" + this.investorDefaultExcess);
    }
    return investorDefaultExcess;
  }

  /**
   * @param investorDefaultExcess the investorDefaultExcess to set
   */
  public void setInvestorDefaultExcess(String investorDefaultExcess) {
    this.investorDefaultExcess = investorDefaultExcess;
  }

  /**
   * @return the buildingStormDamageCost
   */
  public BigDecimal getBuildingStormDamageCost() throws Exception {
    if (this.buildingStormDamageCost == null) {
      this.buildingStormDamageCost = this.getBuildingInsuranceLookup().get(VALUE_BLDG_STORM_DAMAGE).getrValue();
    }
    return buildingStormDamageCost;
  }

  /**
   * @param buildingStormDamageCost the buildingStormDamageCost to set
   */
  public void setBuildingStormDamageCost(BigDecimal buildingStormDamageCost) {
    this.buildingStormDamageCost = buildingStormDamageCost;
  }

  /**
   * @return the buildingMotorDamageCost
   */
  public BigDecimal getBuildingMotorDamageCost() throws Exception {
    if (this.buildingMotorDamageCost == null) {
      this.buildingMotorDamageCost = this.getBuildingInsuranceLookup().get(VALUE_BLDG_ELECTRIC_MOTORS).getrValue();
    }
    return buildingMotorDamageCost;
  }

  /**
   * @param buildingMotorDamageCost the buildingMotorDamageCost to set
   */
  public void setBuildingMotorDamageCost(BigDecimal buildingMotorDamageCost) {
    this.buildingMotorDamageCost = buildingMotorDamageCost;
  }

  /**
   * @return the workersCompensationCost
   */
  public BigDecimal getWorkersCompensationCost() throws Exception {
    if (this.workersCompensationCost == null) {
      this.workersCompensationCost = this.getContentsInsuranceLookup().get(VALUE_CNTS_DWC).getrValue();
    }
    return workersCompensationCost;
  }

  /**
   * @param workersCompensationCost the workersCompensationCost to set
   */
  public void setWorkersCompensationCost(BigDecimal workersCompensationCost) {
    this.workersCompensationCost = workersCompensationCost;
  }

  /**
   * @return the contentsMotorDamageCost
   */
  public BigDecimal getContentsMotorDamageCost() throws Exception {
    if (this.contentsMotorDamageCost == null) {
      this.contentsMotorDamageCost = this.getContentsInsuranceLookup().get(VALUE_CNTS_ELECTRIC_MOTORS).getrValue();
    }
    return contentsMotorDamageCost;
  }

  /**
   * @param contentsMotorDamageCost the contentsMotorDamageCost to set
   */
  public void setContentsMotorDamageCost(BigDecimal contentsMotorDamageCost) {
    this.contentsMotorDamageCost = contentsMotorDamageCost;
  }

  /**
   * @return the investorStormDamageCost
   */
  public BigDecimal getInvestorStormDamageCost() throws Exception {
    if (this.investorStormDamageCost == null) {
      this.investorStormDamageCost = this.getInvestorInsuranceLookup().get(VALUE_INV_STORM_DAMAGE).getrValue();
    }
    return investorStormDamageCost;
  }

  /**
   * @param investorStormDamageCost the investorStormDamageCost to set
   */
  public void setInvestorStormDamageCost(BigDecimal investorStormDamageCost) {
    this.investorStormDamageCost = investorStormDamageCost;
  }

  /**
   * @return the investorMotorDamageCost
   */
  public BigDecimal getInvestorMotorDamageCost() throws Exception {
    if (this.investorMotorDamageCost == null) {
      this.investorMotorDamageCost = this.getInvestorInsuranceLookup().get(VALUE_INV_ELECTRIC_MOTORS).getrValue();
    }
    return investorMotorDamageCost;
  }

  /**
   * @param investorMotorDamageCost the investorMotorDamageCost to set
   */
  public void setInvestorMotorDamageCost(BigDecimal investorMotorDamageCost) {
    this.investorMotorDamageCost = investorMotorDamageCost;
  }
  
  /**
   * @return the investorContentsMotorDamageCost
   */
  public BigDecimal getInvestorContentsMotorDamageCost() throws Exception {
      if (this.investorContentsMotorDamageCost == null) {
          this.investorContentsMotorDamageCost = this.getInvestorContentsInsuranceLookup().get(VALUE_INV_CONTENTS_ELECTRIC_MOTORS).getrValue();
      }
      return investorContentsMotorDamageCost;
  }
  
  /**
   * @param investorContentsMotorDamageCost the investorContentsMotorDamageCost to set
   */
  public void setInvestorContentsMotorDamageCost(BigDecimal investorContentsMotorDamageCost) {
      this.investorContentsMotorDamageCost = investorContentsMotorDamageCost;
  }

  /**
   * @return the investorCover30Cost
   */
  public BigDecimal getInvestorCover30Cost() throws Exception {
    if (this.investorCover30Cost == null) {
      this.investorCover30Cost = this.getInvestorContentsSumInsuredLookup().get(VALUE_INV_CONTENTS_COVER_30).getrValue();
    }
    return investorCover30Cost;
  }

  /**
   * @param investorCover30Cost the investorCover30Cost to set
   */
  public void setInvestorCover30Cost(BigDecimal investorCover30Cost) {
    this.investorCover30Cost = investorCover30Cost;
  }

  /**
   * @return the investorCover50Cost
   */
  public BigDecimal getInvestorCover50Cost() throws Exception {
    if (this.investorCover50Cost == null) {
      this.investorCover50Cost = this.getInvestorContentsSumInsuredLookup().get(VALUE_INV_CONTENTS_COVER_50).getrValue();
    }
    return investorCover50Cost;
  }

  /**
   * @param investorCover50Cost the investorCover50Cost to set
   */
  public void setInvestorCover50Cost(BigDecimal investorCover50Cost) {
    this.investorCover50Cost = investorCover50Cost;
  }

  /**
   * @return the buildingInsuranceOptions
   */
  public List<InRfDet> getBuildingInsuranceOptions() throws Exception {
    if (this.buildingInsuranceOptions == null) {
      this.buildingInsuranceOptions = this.getOptionService().getBuildingInsuranceOptions(this);
    }
    return buildingInsuranceOptions;
  }

  /**
   * Return a lookup of Building Insurance objects by key.
   * @return
   */
  public Map<String, InRfDet> getBuildingInsuranceLookup() throws Exception {
    return this.getLookup(this.getBuildingInsuranceOptions());
  }

  /**
   * @param buildingInsuranceOptions the buildingInsuranceOptions to set
   */
  public void setBuildingInsuranceOptions(List<InRfDet> buildingInsuranceOptions) {
    this.buildingInsuranceOptions = buildingInsuranceOptions;
  }

  /**
   * @return the contentsInsuranceOptions
   */
  public List<InRfDet> getContentsInsuranceOptions() throws Exception {
    if (this.contentsInsuranceOptions == null) {
      this.contentsInsuranceOptions = this.getOptionService().getContentsInsuranceOptions(this);
    }
    return contentsInsuranceOptions;
  }

  /**
   * Return a lookup of Contents Insurance objects by key.
   * @return
   */
  public Map<String, InRfDet> getContentsInsuranceLookup() throws Exception {
    return this.getLookup(this.getContentsInsuranceOptions());
  }

  /**
   * @param contentsInsuranceOptions the contentsInsuranceOptions to set
   */
  public void setContentsInsuranceOptions(List<InRfDet> contentsInsuranceOptions) {
    this.contentsInsuranceOptions = contentsInsuranceOptions;
  }

  /**
   * @return the totalAnnualPremium
   */
  public BigDecimal getTotalAnnualPremium() {
    if (totalAnnualPremium == null) {
        Premium annualPremium = this.getHomeService().retrievePremium(this, WebInsQuoteDetail.PAY_ANNUALLY, WebInsQuote.COVER_BLDG);
        if(null == annualPremium) {
            totalAnnualPremium = BigDecimal.ZERO;
        } else {
            totalAnnualPremium = annualPremium.getAnnualPremium();
        }
    }
    return totalAnnualPremium;
  }

  /**
   * @param totalAnnualPremium the totalAnnualPremium to set
   */
  public void setTotalAnnualPremium(BigDecimal totalAnnualPremium) {
    this.totalAnnualPremium = totalAnnualPremium;
  }

  /**
   * @return the totalMonthlyPremium
   */
  public BigDecimal getTotalMonthlyPremium() {
    if (totalMonthlyPremium == null) {
        Premium monthlyPremium = this.getHomeService().retrievePremium(this, WebInsQuoteDetail.PAY_MONTHLY, WebInsQuote.COVER_BLDG);
        if(null == monthlyPremium) {
            totalMonthlyPremium = BigDecimal.ZERO;
        } else {
            totalMonthlyPremium = monthlyPremium.getAnnualPremium();
        }
    }
    return totalMonthlyPremium;
  }

  /**
   * @param totalMonthlyPremium the totalMonthlyPremium to set
   */
  public void setTotalMonthlyPremium(BigDecimal totalMonthlyPremium) {
    this.totalMonthlyPremium = totalMonthlyPremium;
  }

  /**
   * @return the investorInsuranceOptions
   */
  public List<InRfDet> getInvestorInsuranceOptions() throws Exception {
    if (this.investorInsuranceOptions == null) {
      this.investorInsuranceOptions = this.getOptionService().getInvestorInsuranceOptions(this);
    }
    return investorInsuranceOptions;
  }

  /**
   * @return the investorInsuranceOptions
   */
  public List<InRfDet> getInvestorContentsInsuranceOptions() throws Exception {
      if (this.investorContentsInsuranceOptions == null) {
          this.investorContentsInsuranceOptions = this.getOptionService().getInvestorContentsInsuranceOptions(this);
      }
      return investorContentsInsuranceOptions;
  }
  
  /**
   * @param investorInsuranceOptions the investorInsuranceOptions to set
   */
  public void setInvestorInsuranceOptions(List<InRfDet> investorInsuranceOptions) {
    this.investorInsuranceOptions = investorInsuranceOptions;
  }
  
  /**
   * @return the investorContentsSumInsuredOptions
   */
  public List<InRfDet> getInvestorContentsSumInsuredOptions() throws Exception {
      if (this.investorContentsSumInsuredOptions == null) {
          this.investorContentsSumInsuredOptions = this.getOptionService().getInvestorContentsSumInsuredOptions(this);
      }
      return investorContentsSumInsuredOptions;
  }
  
  /**
   * @param investorContentsSumInsuredOptions the investorContentsSumInsuredOptions to set
   */
  public void setInvestorContentsSumInsuredOptions(List<InRfDet> investorContentsSumInsuredOptions) {
      this.investorContentsSumInsuredOptions = investorContentsSumInsuredOptions;
  }

  /**
   * Return a lookup of Investor Insurance objects by key.
   * @return
   */
  public Map<String, InRfDet> getInvestorInsuranceLookup() throws Exception {
    return this.getLookup(this.getInvestorInsuranceOptions());
  }
  
  /**
   * Return a lookup of Investor Contents Insurance objects by key.
   * @return
   */
  public Map<String, InRfDet> getInvestorContentsInsuranceLookup() throws Exception {
      return this.getLookup(this.getInvestorContentsInsuranceOptions());
  }
  
  /**
   * Return a lookup of Investor Contents Sum Insured objects by key.
   * @return
   */
  public Map<String, InRfDet> getInvestorContentsSumInsuredLookup() throws Exception {
      return this.getLookup(this.getInvestorContentsSumInsuredOptions());
  }

  /**
   * @return the buildingAccidentalDamagePercentage
   */
  public String getBuildingAccidentalDamagePercentage() throws Exception {
    if (this.buildingAccidentalDamagePercentage == null) {
      this.buildingAccidentalDamagePercentage = this.getBuildingInsuranceLookup().get(VALUE_BLDG_ACCIDENTAL_DAMAGE).getrValue().toString() + "%";
    }
    return buildingAccidentalDamagePercentage;
  }

  /**
   * @param buildingAccidentalDamagePercentage the buildingAccidentalDamagePercentage to set
   */
  public void setBuildingAccidentalDamagePercentage(String buildingAccidentalDamagePercentage) {
    this.buildingAccidentalDamagePercentage = buildingAccidentalDamagePercentage;
  }

  /**
   * @return the contentsAccidentalDamagePercentage
   */
  public String getContentsAccidentalDamagePercentage() throws Exception {
    if (this.contentsAccidentalDamagePercentage == null) {
      this.contentsAccidentalDamagePercentage = this.getContentsInsuranceLookup().get(VALUE_CNTS_ACCIDENTAL_DAMAGE).getrValue().toString() + "%";
    }
    return contentsAccidentalDamagePercentage;
  }

  /**
   * @param contentsAccidentalDamagePercentage the contentsAccidentalDamagePercentage to set
   */
  public void setContentsAccidentalDamagePercentage(String contentsAccidentalDamagePercentage) {
    this.contentsAccidentalDamagePercentage = contentsAccidentalDamagePercentage;
  }

  @Override
  public String getPageTitle() {
    return "Premium / Options";
  }

  /**
   * Allow unspecified personal effects to be selected if Occupancy is not Holiday Home.
   * @return the allowUnspecifiedPersonalEffects
   */
  public Boolean getAllowUnspecifiedPersonalEffects() {
    if (this.allowUnspecifiedPersonalEffects == null) {
      String occupancy = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.OCCUPANCY);
      this.allowUnspecifiedPersonalEffects = (occupancy != null && !occupancy.equals(ActionBase.VALUE_OCCUPANCY_HOLIDAY_HOME));
    }
    return allowUnspecifiedPersonalEffects;
  }

  /**
   * @param allowUnspecifiedPersonalEffects the allowUnspecifiedPersonalEffects to set
   */
  public void setAllowUnspecifiedPersonalEffects(Boolean allowUnspecifiedPersonalEffects) {
    this.allowUnspecifiedPersonalEffects = allowUnspecifiedPersonalEffects;
  }
}

