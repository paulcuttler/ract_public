package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.Calendar;

import com.ract.web.insurance.GlVehicle;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;
import futuremedium.client.ract.secure.Config;


/**
 * Load page 5 for garage address.
 * @author xmfu
 */
public class QuotePageSave5 extends QuotePageSave {
    private static final long serialVersionUID = -2605383879752736457L;
  private String agreedValue;

  @Override
  public String execute() throws Exception {
    this.setUp();
    int quoteNo = this.getQuoteNo();

    // If user has reached page 9, access from page 1 to page 8 is not allowed.
    if (isReachedPage9()) {
      this.setNextAction();
      return SUCCESS;
    }

    // Go back to previous page.
    if (this.getSubmit().equals(BUTTON_BACK)) {
      this.setNextAction();
      return SUCCESS;
    }

    // Save agreed Value.
    // Check whether agreed value is outside the Glasses acceptable range.
    boolean outside = false;
    WebInsQuote webInsQuote = insuranceMgr.getQuote(quoteNo);

    String nvic = insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.NVIC);
    GlVehicle vehicle = this.getGlassesMgr().getVehicle(nvic, webInsQuote.getQuoteDate());

    int suggestedAgreedValue = vehicle.getValue();
    int changedAgreedValue = Integer.valueOf(this.getAgreedValue());
    
    Calendar cal = Calendar.getInstance();
    cal.setTime(webInsQuote.getQuoteDate());
    int vehicleAge = cal.get(Calendar.YEAR) - vehicle.getVehYear();
    
    int roundFactor = getRoundFactor(suggestedAgreedValue);    
    int lowerAgreedValue = getLowerAgreedValueBound(roundFactor, suggestedAgreedValue, vehicleAge);
    int upperAgreedValue = getUpperAgreedValueBound(roundFactor, suggestedAgreedValue, vehicleAge);
    
  	Config.logger.debug(Config.key + ": lower: " + lowerAgreedValue);
  	Config.logger.debug(Config.key + ": selected: " + changedAgreedValue);
  	Config.logger.debug(Config.key + ": upper: " + upperAgreedValue);
    
    if ((changedAgreedValue < lowerAgreedValue) || changedAgreedValue > upperAgreedValue) {    	
    	Config.logger.warn(Config.key + ": Quote # " + quoteNo + " agreed value outside Glasses range, aborting");    	
      outside = true;
    }
        
    // Check whether agreed value is outside the RACTI acceptable range.    
    int absMin = 0;
    int absMax = 0;
    String absMinValue = this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.MIN_SUM_INSURED, WebInsQuote.COVER_COMP, webInsQuote.getQuoteDate());
    String absMaxValue = this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.MAX_SUM_INSURED, WebInsQuote.COVER_COMP, webInsQuote.getQuoteDate());
    absMin = Integer.parseInt(absMinValue);
    absMax = Integer.parseInt(absMaxValue);
    
    if ((changedAgreedValue < absMin) || changedAgreedValue > absMax) {    	
    	Config.logger.warn(Config.key + ": Quote # " + quoteNo + " agreed value outside RACTI range, aborting");    	
      outside = true;
    }

    insuranceMgr.setQuoteDetail(quoteNo, "vehicleValue", this.getAgreedValue());
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.AGREED_VALUE, this.getAgreedValue());
    
    // agreed value is outside the acceptable range.
    if (outside) {
      return this.messagePage(MESSAGE_CARVALUE);
    }

    this.setNextAction();
    return SUCCESS;
  }

  /**
   * @return the agreedValue
   */
  public String getAgreedValue() {
    return agreedValue;
  }

  /**
   * @param agreedValue the agreedValue to set
   */
  public void setAgreedValue(String agreedValue) {
    this.agreedValue = agreedValue;
  }
}
