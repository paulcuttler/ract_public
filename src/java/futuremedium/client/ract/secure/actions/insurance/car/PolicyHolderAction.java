package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.List;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.entities.insurance.car.QuoteQuestion;

/**
 * Create a new policy holder.
 * @author xmfu
 */
public class PolicyHolderAction extends QuotePageSave {

  private String policyHolderCreation;
  
  public String create() throws Exception {
    this.setUp();

    this.loadCreatePolicyHolderPage();

    return SUCCESS;
  }

  /**
   * Load the policy holder page.
   */
  public void loadCreatePolicyHolderPage() throws Exception {
    this.setQuotePage(this.quoteQuestionManager.getPage(13));
    this.setPageId(13L);

    List<QuoteQuestion> duplicatedSubQuestions = duplicatePolicyHolders(this.getDrivers().size() + 1);

    // Pre populate some questions.
    this.loadQuestion(duplicatedSubQuestions.get(4), "no");
    this.loadTitles(duplicatedSubQuestions.get(7), null);
    this.loadQuestion(duplicatedSubQuestions.get(11), "yes");
    duplicatedSubQuestions.get(11).setDisplay(false);
    this.loadQuestion(duplicatedSubQuestions.get(17), "yes");
    duplicatedSubQuestions.get(17).setDisplay(false);

    this.getQuestionById("q23").getSubQuestions().clear();
    this.getQuestionById("q23").getSubQuestions().addAll(duplicatedSubQuestions);

    this.setPolicyHolderCreation("true");
    this.setShowNewPolicyHolderLink("false");
  }

  /**
   * Save the newly created policy holder.
   */
  @SuppressWarnings("unchecked")
  public String save() throws Exception {
    this.setUp();

    int existingDriverSize = 0;

    if (this.getSubmit().equals("next")) {

      if (getSession().get("existingDriverSize") != null) {
        existingDriverSize = (Integer) getSession().get("existingDriverSize");
      } else {
        existingDriverSize = this.getDrivers().size() + 1;
        getSession().put("existingDriverSize", existingDriverSize);
      }
      
      Config.logger.debug(Config.key + ": existingDriverSize = " + existingDriverSize);

      String returnMessage = this.savePolicyHolder(existingDriverSize, "true", this.getQuoteNo());

      // Validation.
      if (returnMessage.equals(String.valueOf(MESSAGE_SUSPENSION))) {
        return this.messagePage(MESSAGE_SUSPENSION);
      } else if (returnMessage.equals(String.valueOf(MESSAGE_CANCELLEDINSURANCE))) {
        return this.messagePage(MESSAGE_CANCELLEDINSURANCE);
      } else if (returnMessage.equals(String.valueOf(MESSAGE_FIFTYPLUS))) {
        return this.messagePage(MESSAGE_FIFTYPLUS);
      } else if (!returnMessage.equals(PASSED)) {
        if (!returnMessage.equals(SYSTEM_MESSAGE)) {
          this.setPolicyHolderCreation("true");
          this.setShowNewPolicyHolderLink("false");
          this.setQuotePage(this.quoteQuestionManager.getPage(13));
          this.setPageId(13L);

          List<QuoteQuestion> subQuestions = this.loadPolicyHolderData(this.getDrivers().get(this.getDrivers().size() - 1), duplicatePolicyHolders(existingDriverSize), existingDriverSize, "true");
          this.getQuestionById("q23").getSubQuestions().clear();
          this.getQuestionById("q23").getSubQuestions().addAll(subQuestions);
          return this.validationFailed(returnMessage, VALIDATE_CUSTOMIZED, false);
        }
      }
    }
    
    // Go back to page 13 as it is the page to launch this action.
    this.setQuotePage(this.quoteQuestionManager.getPage(13));
    this.setPageId(13L);

    next();

    // Load all policy holders as it is going to page 13 to display all policy holders in one page.
    this.loadPolicyHolders("true", "-1", true);
    this.setPolicyHolderCreation("false");
    // Still display the create new policy holder link f user wants to add more policy holder.
    this.setShowNewPolicyHolderLink("true");
    getSession().put("existingDriverSize", null);

    return SUCCESS;
  }

  /**
   * @return the policyHolderCreation
   */
  public String getPolicyHolderCreation() {
    return policyHolderCreation;
  }

  /**
   * @param policyHolderCreation the policyHolderCreation to set
   */
  public void setPolicyHolderCreation(String policyHolderCreation) {
    this.policyHolderCreation = policyHolderCreation;
  }
}