package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * Support action to determine whether the user needs to interact with
 * Building-type options or Contents-type options (or both).
 * @author gnewton
 */
public class QuoteSubTypeSupport extends ActionBase {

  private Boolean buildingOptionsRequired;
  private Boolean contentsOptionsRequired;
  private Boolean investorOptionsRequired;

  /**
   * Is this a building only or building & contents quote.
   * @return
   */
  public boolean isBuildingOptionsRequired() {
    if (this.buildingOptionsRequired == null) {
      this.buildingOptionsRequired = (this.getQuoteType().equals(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS)
            || this.getQuoteType().equals(ActionBase.VALUE_QUOTE_TYPE_BLDG_ONLY));
    }
    return this.buildingOptionsRequired;
  }

  /**
   * Is this a contents only or building & contents quote.
   * @return
   */
  public boolean isContentsOptionsRequired() {
    if (this.contentsOptionsRequired == null) {
      this.contentsOptionsRequired = (this.getQuoteType().equals(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS)
            || this.getQuoteType().equals(ActionBase.VALUE_QUOTE_TYPE_CNTS_ONLY));
    }
    return this.contentsOptionsRequired;
  }

  /**
   * Is this an investor quote.
   * @return the investorOptionsRequired
   */
  public boolean isInvestorOptionsRequired() {
    if (this.investorOptionsRequired == null) {
      this.investorOptionsRequired = this.getQuoteType().equals(ActionBase.VALUE_QUOTE_TYPE_INV);
    }
    return investorOptionsRequired;
  }

  /**
   * @param buildingOptionsRequired the buildingOptionsRequired to set
   */
  public void setBuildingOptionsRequired(Boolean buildingOptionsRequired) {
    this.buildingOptionsRequired = buildingOptionsRequired;
  }

  /**
   * @param contentsOptionsRequired the contentsOptionsRequired to set
   */
  public void setContentsOptionsRequired(Boolean contentsOptionsRequired) {
    this.contentsOptionsRequired = contentsOptionsRequired;
  }

  /**
   * @param investorOptionsRequired the investorOptionsRequired to set
   */
  public void setInvestorOptionsRequired(Boolean investorOptionsRequired) {
    this.investorOptionsRequired = investorOptionsRequired;
  }

}
