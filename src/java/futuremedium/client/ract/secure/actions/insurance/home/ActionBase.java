package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ract.util.DateTime;
import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.Premium;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.actions.insurance.InsuranceBase;
import futuremedium.client.ract.secure.entities.insurance.home.HomeService;
import futuremedium.client.ract.secure.entities.insurance.home.OptionService;
import futuremedium.client.ract.secure.entities.insurance.home.Page;
import futuremedium.client.ract.secure.entities.insurance.home.SiteInfo;
import futuremedium.common2.Constants;

/**
 * Base Action for Home Insurance Quoting.
 * @author gnewton
 */
public class ActionBase extends InsuranceBase {
    private static final long serialVersionUID = 7220765326345107097L;
  // global session references
  static public final String TOKEN_HOME_QUOTE_NO = "HOME_QUOTE_NO";
  static public final String TOKEN_HOME_QUOTE_TYPE = "HOME_QUOTE_TYPE";
  static public final String TOKEN_HOME_QUOTE_SECURED_NO = "HOME_QUOTE_SECURED_NO";
  static public final String TOKEN_HOME_QUOTE_DATE = "HOME_QUOTE_DATE";
  static public final String TOKEN_HOME_QUOTE_ABORTED = "HOME_QUOTE_ABORTED";
  static public final String TOKEN_HOME_BUILDING_SUM_INSURED = "HOME_BUILDING_SUM_INSURED";
  static public final String TOKEN_HOME_INVESTOR_SUM_INSURED = "HOME_INVESTOR_SUM_INSURED";
  static public final String TOKEN_HOME_QUOTE_MEMBER_CARD_ATTEMPTS = "HOME_QUOTE_MEMBER_CARD_ATTEMPTS";
  static public final String TOKEN_HOME_QUOTE_OLDEST_DOB_ATTEMPTS = "HOME_QUOTE_OLDEST_DOB_ATTEMPTS";

  static public final String TOKEN_COVER_START_DATE = "HOME_QUOTE_DATE";

  static public final String BUTTON_NEXT = "Next";
  static public final String BUTTON_PREV = "Prev";

  static public final String FORM_ACTION_PREFIX = "Home";
  static public final String FORM_ACTION_SUFFIX = "Form";

  // custom field type lookups
  static public final String FIELD_FINALISED_QUOTE = "hasFinalisedQuote";
  static public final String FIELD_FINALISED_POLICY = "hasFinalisedPolicy";
  static public final String FIELD_DISPLAY_QUOTE_NUMBER = "displayQuoteNumber";
  static public final String FIELD_QUOTE_SUB_TYPE = "quoteSubType";
  static public final String FIELD_LAST_ACTION = "lastAction";
  static public final String FIELD_IS_MEMBER = "isMember";
  static public final String FIELD_MEMBER_CARD = "memberCardNo";
  static public final String FIELD_MEMBER_DOB = "dateOfBirth";
  static public final String FIELD_OLDEST_DOB = "oldestDateOfBirth";
  static public final String FIELD_PRODUCT_MODIFIER_PIP = "PIP";
  static public final String FIELD_PRODUCT_MODIFIER_FIFTY_PLUS = "50+";
  static public final String FIELD_SAVED_QUOTE_EMAIL = "savedQuoteEmail";
  static public final String FIELD_GOOD_ORDER = "goodOrder";
  static public final String FIELD_CURRENTLY_UNOCCUPIED = "currentlyUnoccupied";
  static public final String FIELD_WITHIN_90_DAYS = "within90Days";
  static public final String FIELD_UNRELATED_PERSONS = "unrelatedPersons";
  static public final String FIELD_UNRELATED_EXCEED_2 = "unrelatedExceed2";
  static public final String FIELD_UNDER_FINANCE = "underFinance";
  static public final String FIELD_INSURANCE_REFUSED = "insuranceRefused";
  static public final String FIELD_SECURITY_OTHER = "securityOther";
  static public final String FIELD_MANAGER_OTHER = "managerOther";
  static public final String FIELD_BUILDING_TYPE_CONJOINED = "conjoined";
  static public final String FIELD_SPECIFY = "specify";

  static public final String FIELD_USED_CALCULATOR = "usedCalculator";
  static public final String FIELD_VIEWED_PREMIUM = "viewedPremium";

  static public final String FIELD_BUILDING_MONTHLY_PREMIUM = WebInsQuote.COVER_BLDG.toUpperCase() + "_monthlyPremium";
  static public final String FIELD_BUILDING_ANNUAL_PREMIUM = WebInsQuote.COVER_BLDG.toUpperCase() + "_annualPremium";
  static public final String FIELD_CONTENTS_MONTHLY_PREMIUM = WebInsQuote.COVER_CNTS.toUpperCase() + "_monthlyPremium";
  static public final String FIELD_CONTENTS_ANNUAL_PREMIUM = WebInsQuote.COVER_CNTS.toUpperCase() + "_annualPremium";
  static public final String FIELD_INVESTOR_MONTHLY_PREMIUM = WebInsQuote.COVER_INV.toUpperCase() + "_monthlyPremium";
  static public final String FIELD_INVESTOR_ANNUAL_PREMIUM = WebInsQuote.COVER_INV.toUpperCase() + "_annualPremium";

  static public final String DIFFERENT_POSTAL_ADDRESS = "differentPostalAddress";

  // custom or existing field values stored or looked up
  static public final String VALUE_QUOTE_TYPE_BLDG_CNTS = WebInsQuote.COVER_BLDG + "_" + WebInsQuote.COVER_CNTS;
  static public final String VALUE_QUOTE_TYPE_BLDG_ONLY = WebInsQuote.COVER_BLDG;
  static public final String VALUE_QUOTE_TYPE_CNTS_ONLY = WebInsQuote.COVER_CNTS;
  static public final String VALUE_QUOTE_TYPE_INV = WebInsQuote.COVER_INV;
  static public final String VALUE_SECURITY_MONITORED = "M";
  static public final String VALUE_SECURITY_LOCAL = "L";
  static public final String VALUE_SECURITY_OTHER = "O";
  static public final String VALUE_MANAGER_OTHER = "4";
  static public final String VALUE_BUILDING_TYPE_CONJOINED = "C";
  static public final String VALUE_OCCUPANCY_HOLIDAY_HOME = "HHOO";

  private String nextAction;
  private String lastActionName;

  private String submitButton;
  private int quoteNo;
  protected String quoteType;
  private String quoteTypeDescription;
  private String securedQuoteNo;
  private DateTime quoteEffectiveDate;
  private DateTime coverStartDate;
  private boolean aborted;
  protected Integer buildingSumInsured;
  protected Integer investorSumInsured;
  private List<String> discountList;
  private String benefitCssClasses;
  private int memberCardAttempts;
  private int oldestDobAttempts;

  private HomeService homeService;
  private OptionService optionService;
  private SiteInfo siteInfo;

  @Override
  public String execute() throws Exception {

    return SUCCESS;
  }

  /**
   * Determine the Next Action to perform.
   * @return the nextAction
   */
  public String getNextAction() {
    if (this.nextAction == null) {

      String currentFormActionName = ActionBase.FORM_ACTION_PREFIX + ActionBase.formify(this.getClass().getSimpleName());

      int idx = this.getHomeService().getFlow().indexOf(currentFormActionName);
      idx++;

      if (this.getQuoteType() != null && this.getHomeService().getPageFlow().get(idx).getExcludedFromFlow().contains(this.getQuoteType())) {
        idx++;
    	Config.logger.debug(Config.key + ": action skipped, setting nextAction to: " + this.getHomeService().getFlow().get(idx));        
      }

      if (idx < this.getHomeService().getFlow().size()) {
        this.nextAction = this.getHomeService().getFlow().get(idx);
      }
    }

    Config.logger.debug(Config.key + ": " + this.getClass().getSimpleName() + ", nextAction = " + this.nextAction);

    return this.nextAction;
  }

  /**
   * @param nextAction the nextAction to set
   */
  public void setNextAction(String nextAction) {
    this.nextAction = nextAction;
  }

  /**
   * Return the last recorded Action.
   * @return
   */
  public String getLastActionName() {
    if (this.lastActionName == null) {
      this.lastActionName = this.getHomeService().retrieveLastAction(this.getQuoteNo());
    }
    return this.lastActionName;
  }

  public void setLastActionName(String lastActionName) {
    this.lastActionName = lastActionName;
  }

  public String getLastFormActionName() {
    return ActionBase.formify(this.getLastActionName());
  }

  public String getCurrentFormActionName() {
    return ActionBase.formify(this.getCurrentActionName());
  }

  /**
   * Retrieve quoteNo from session.
   * @return the quoteNo
   */
  public int getQuoteNo() {
    if (this.session != null && this.session.containsKey(TOKEN_HOME_QUOTE_NO)) {
      this.quoteNo = (Integer) this.session.get(TOKEN_HOME_QUOTE_NO);
    }
    return this.quoteNo;
  }

  /**
   * Store quoteNo in session.
   * @param quoteNo the quoteNo to set
   */
  public void setQuoteNo(int quoteNo) {
    this.quoteNo = quoteNo;
    if (this.session != null) {
      this.session.put(TOKEN_HOME_QUOTE_NO, quoteNo);
    }
  }

  /**
   * Retrieve quoteEffectiveDate from session.
   * @return the quoteEffectiveDate
   */
  public DateTime getQuoteEffectiveDate() {
    if (this.session != null && this.session.containsKey(TOKEN_HOME_QUOTE_DATE)) {
      this.quoteEffectiveDate = (DateTime) this.session.get(TOKEN_HOME_QUOTE_DATE);
    }
    if (this.quoteEffectiveDate == null) {
      this.quoteEffectiveDate = new DateTime();
    }
    return quoteEffectiveDate;
  }

  /**
   * Store quoteEffectiveDate in session.
   * @param quoteEffectiveDate the quoteEffectiveDate to set
   */
  public void setQuoteEffectiveDate(DateTime quoteEffectiveDate) {
    this.quoteEffectiveDate = quoteEffectiveDate;
    if (this.session != null) {
      this.session.put(TOKEN_HOME_QUOTE_DATE, quoteEffectiveDate);
    }
  }

    /**
   * Retrieve coverStartDate from session.
   * @return the coverStartDate
   */
  public DateTime getCoverStartDate() {
    if (this.session != null && this.session.containsKey(TOKEN_COVER_START_DATE)) {
      this.coverStartDate = (DateTime) this.session.get(TOKEN_COVER_START_DATE);
    }
    if (this.coverStartDate == null) {
      DateTime coverStartDateTime = this.getHomeService().getCoverStartDate(this.getQuoteNo());
      if (coverStartDateTime == null) {
        coverStartDateTime = this.getQuoteEffectiveDate(); // Fall back to date supplied in WhereYouLive action
      }
      this.coverStartDate = coverStartDateTime;
    }
    return coverStartDate;
  }

  /**
   * Store coverStartDate in session.
   * @param coverStartDate the coverStartDate to set
   */
  public void setCoverStartDate(DateTime coverStartDate) {
    this.coverStartDate = coverStartDate;
    if (this.session != null) {
      this.session.put(TOKEN_COVER_START_DATE, coverStartDate);
    }
  }
  


  /**
   * Retrieve aborted from session. Has the user's quote been aborted by the system?
   * @return the aborted
   */
  public boolean isAborted() {
    if (this.session != null && this.session.containsKey(TOKEN_HOME_QUOTE_ABORTED)) {
      this.aborted = (Boolean) this.session.get(TOKEN_HOME_QUOTE_ABORTED);
    }
    return aborted;
  }

  /**
   * Store aborted in session.
   * @param aborted the aborted to set
   */
  public void setAborted(boolean aborted) {
    this.aborted = aborted;
    if (this.session != null) {
      this.session.put(TOKEN_HOME_QUOTE_ABORTED, aborted);
    }
  }

  /**
   * @return the homeService
   */
  public HomeService getHomeService() {
    return homeService;
  }

  /**
   * @param homeService the homeService to set
   */
  public void setHomeService(HomeService homeService) {
    this.homeService = homeService;
  }

  /**
   * @return the optionService
   */
  public OptionService getOptionService() {
    return optionService;
  }

  /**
   * @param optionService the optionService to set
   */
  public void setOptionService(OptionService optionService) {
    this.optionService = optionService;
  }

  protected String getSuccessMessage() {
    return "Your " + this.getPageTitle() + " has been saved.";
  }

  protected String getErrorMessage() {
    return "An error occurred saving your " + this.getPageTitle() + ", please try again later.";
  }

  @Override
  public String getPageTitle() {
    return RactActionSupport.unCamelise(this.getClass().getSimpleName().replace("Form", ""));
  }

  /**
   * Retrieve or populate securedQuoteNo from session.
   * @return the securedQuoteNo
   */
  public String getSecuredQuoteNo() {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_HOME_QUOTE_SECURED_NO)) { // retrieve from session
        this.securedQuoteNo = (String) this.session.get(TOKEN_HOME_QUOTE_SECURED_NO);
      } else { // initialise in session
        this.securedQuoteNo = this.getHomeService().getSecuredQuoteNumber(this.getQuoteNo());
        this.session.put(TOKEN_HOME_QUOTE_SECURED_NO, this.securedQuoteNo);
      }
    }
    return securedQuoteNo;
  }

  /**
   * Store securedQuoteNo in session
   * @param securedQuoteNo the securedQuoteNo to set
   */
  public void setSecuredQuoteNo(String secureQuoteNo) {
    this.securedQuoteNo = secureQuoteNo;
    if (this.session != null) {
      this.session.put(TOKEN_HOME_QUOTE_SECURED_NO, this.securedQuoteNo);
    }
  }

  /**
   * @return the submitButton
   */
  public String getSubmitButton() {
    return submitButton;
  }

  /**
   * @param submitButton the submitButton to set
   */
  public void setSubmitButton(String submitButton) {
    this.submitButton = submitButton;
  }

  /**
   * @return the BUTTON_NEXT
   */
  public String getBUTTON_NEXT() {
    return BUTTON_NEXT;
  }

  /**
   * @return the BUTTON_PREV
   */
  public String getBUTTON_PREV() {
    return BUTTON_PREV;
  }
  
  public List<Map<String,String>> getQuoteTypeList() {
    return this.getOptionService().getQuoteTypeList();
  }

  public List<Map<String,String>> getQuoteTypeTopList() {
    List<Map<String,String>> list = new ArrayList<Map<String,String>>();

    for (Map<String,String> qt : this.getQuoteTypeList()) {
      if (qt.get("list").equals("top")) {
        list.add(qt);
      }
    }

    return list;
  }

  public List<Map<String,String>> getQuoteTypeBottomList() {
    List<Map<String,String>> list = new ArrayList<Map<String,String>>();

    for (Map<String,String> qt : this.getQuoteTypeList()) {
      if (qt.get("list").equals("bottom")) {
        list.add(qt);
      }
    }

    return list;
  }


  /**
   * Retrieve quoteType from session.
   * @return the quoteType
   */
  public String getQuoteType() {
    if (this.session != null && this.session.containsKey(TOKEN_HOME_QUOTE_TYPE)) {
      this.quoteType = (String) this.session.get(TOKEN_HOME_QUOTE_TYPE);
    }
    return this.quoteType;
  }

  /**
   * Store quoteNo in session.
   * @param quoteNo the quoteNo to set
   */
  public void setQuoteType(String quoteType) {
    this.quoteType = quoteType;
    if (this.session != null) {
      this.session.put(TOKEN_HOME_QUOTE_TYPE, quoteType);
    }
  }

  /**
   * @return the quoteTypeDescription
   */
  public String getQuoteTypeDescription() {
    if (this.quoteTypeDescription == null) {
      for (Map<String,String> qt : this.getOptionService().getQuoteTypeList()) {
        if (qt.get("key").equals(this.getQuoteType())) {
          this.quoteTypeDescription = qt.get("value");
        }
      }
    }
    return quoteTypeDescription;
  }

  /**
   * @param quoteTypeDescription the quoteTypeDescription to set
   */
  public void setQuoteTypeDescription(String quoteTypeDescription) {
    this.quoteTypeDescription = quoteTypeDescription;
  }

  public List<String> getFlow() {
    return this.getHomeService().getFlow();
  }

  public List<Page> getPageFlow() {
    return this.getHomeService().getPageFlow();
  }

  public SiteInfo getSiteInfo() {
    if (this.siteInfo == null) {
      this.siteInfo = SiteInfo.getInstance();
    }

    return this.siteInfo;
  }

  /**
   * This is a method relying on convention - always return an action name ending in "Form".
   * This is used for the purposes of navigation and showing current state.
   * e.g. HomeAboutYouForm and HomeAboutYou display the same content.
   * @return
   */
  static public String formify(String actionName) {
    if (actionName != null) {
      if (!actionName.endsWith(ActionBase.FORM_ACTION_SUFFIX)) {
        actionName = actionName + ActionBase.FORM_ACTION_SUFFIX;
      }
    }
    return actionName;
  }

  /**
   * Provide a lookup for a list of @InRfDet options based on rClass.
   */
  protected Map<String, InRfDet> getLookup(List<InRfDet> list) {
    Map<String, InRfDet> lookup = new HashMap<String, InRfDet>();
    if (list != null) {
      for (InRfDet r : list) {
        lookup.put(r.getrClass(), r);
      }
    }

    return lookup;
  }

  public boolean isQuoteOnly() {
    return (this instanceof QuoteAware);
  }

  public boolean isPolicyOnly() {
    return (this instanceof PolicyAware);
  }

  public Page getPage() {
    return this.getHomeService().getPage(this);
  }

  /**
   * Retrieve buildingSumInsured from session.
   * @return the buildingSumInsured
   */
  public Integer getBuildingSumInsured() {
    if (this.session != null && this.session.containsKey(TOKEN_HOME_BUILDING_SUM_INSURED)) {
      this.buildingSumInsured = (Integer) this.session.get(TOKEN_HOME_BUILDING_SUM_INSURED);
    }
    return buildingSumInsured;
  }

  /**
   * Store buildingSumInsured in session.
   * @param buildingSumInsured the buildingSumInsured to set
   */
  public void setBuildingSumInsured(Integer buildingSumInsured) {
    this.buildingSumInsured = buildingSumInsured;
    if (this.session != null) {
      this.session.put(TOKEN_HOME_BUILDING_SUM_INSURED, buildingSumInsured);
    }
  }

  /**
   * Retrieve investorSumInsured from session.
   * @return the investorSumInsured
   */
  public Integer getInvestorSumInsured() {
    if (this.session != null && this.session.containsKey(TOKEN_HOME_INVESTOR_SUM_INSURED)) {
      this.investorSumInsured = (Integer) this.session.get(TOKEN_HOME_INVESTOR_SUM_INSURED);
    }
    return investorSumInsured;
  }

  /**
   * Store investorSumInsured in session.
   * @param investorSumInsured the investorSumInsured to set
   */
  public void setInvestorSumInsured(Integer investorSumInsured) {
    this.investorSumInsured = investorSumInsured;
    if (this.session != null) {
      this.session.put(TOKEN_HOME_INVESTOR_SUM_INSURED, investorSumInsured);
    }
  }
  
  /**
   * Utility method to get a Base Premium for the quote.
   * Use this premium to determine what discounts have been applied.
   */
  public Premium getBasePremium() {
    return this.getHomeService().retrievePremium(this, WebInsQuoteDetail.PAY_ANNUALLY, this.getTranslatedCoverType());
  }

  /**
   * @return the discountList
   */
  public List<String> getDiscountList() {
    if (this.discountList == null) {
      discountList = this.getHomeService().retrieveDiscounts(this);
    }
    return discountList;
  }

  /**
   * @param discountList the discountList to set
   */
  public void setDiscountList(List<String> discountList) {
    this.discountList = discountList;
  }

  /**
   * @return the benefitCssClasses
   */
  public String getBenefitCssClasses() {
    if (this.benefitCssClasses == null) {
      benefitCssClasses = Constants.join(this.getHomeService().retrieveBenefits(this), " ");
    }
    return benefitCssClasses;
  }

  /**
   * @param benefitCssClasses the benefitCssClasses to set
   */
  public void setBenefitCssClasses(String benefits) {
    this.benefitCssClasses = benefits;
  }

  /**
   * Translate Quote Type to Cover Type.
   * @param action
   * @return
   */
  public String getTranslatedCoverType() {
    String coverType = this.getQuoteType();

    // for "Building and Contents" return "Building".
    if (this.getQuoteType().equals(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS)) {
      coverType = WebInsQuote.COVER_BLDG;
    }

    return coverType;
  }

  /**
   * Expose VALUE_SECURITY_OTHER to JSP.
   */
  public String getVALUE_SECURITY_OTHER() {
    return ActionBase.VALUE_SECURITY_OTHER;
  }

  /**
   * Expose VALUE_MANAGER_OTHER to JSP.
   */
  public String getVALUE_MANAGER_OTHER() {
    return ActionBase.VALUE_MANAGER_OTHER;
  }

  /**
   * Expose VALUE_BUILDING_TYPE_CONJOINED to JSP.
   */
  public String getVALUE_BUILDING_TYPE_CONJOINED() {
    return ActionBase.VALUE_BUILDING_TYPE_CONJOINED;
  }

  /**
   * @return the memberCardAttempts
   */
  public int getMemberCardAttempts() {
    if (this.session != null && this.session.containsKey(TOKEN_HOME_QUOTE_MEMBER_CARD_ATTEMPTS)) {
      this.memberCardAttempts = (Integer) this.session.get(TOKEN_HOME_QUOTE_MEMBER_CARD_ATTEMPTS);
    }
    return memberCardAttempts;
  }

  /**
   * @param memberCardAttempts the memberCardAttempts to set
   */
  public void setMemberCardAttempts(int memberCardAttempts) {
    this.memberCardAttempts = memberCardAttempts;
    if (this.session != null) {
      this.session.put(TOKEN_HOME_QUOTE_MEMBER_CARD_ATTEMPTS, this.memberCardAttempts);
    }
  }

  /**
   * Increment the number of membership card attempts.
   */
  public void incrementMemberCardAttempts() {
    this.setMemberCardAttempts(this.getMemberCardAttempts() + 1);
  }

  /**
   * Reset the number of membership card attempts.
   */
  public void resetMemberCardAttempts() {
    this.setMemberCardAttempts(0);
  }

  /**
   * @return the oldestDobAttempts
   */
  public int getOldestDobAttempts() {
    if (this.session != null && this.session.containsKey(TOKEN_HOME_QUOTE_OLDEST_DOB_ATTEMPTS)) {
      this.oldestDobAttempts = (Integer) this.session.get(TOKEN_HOME_QUOTE_OLDEST_DOB_ATTEMPTS);
    }
    return oldestDobAttempts;
  }

  /**
   * @param oldestDobAttempts the oldestDobAttempts to set
   */
  public void setOldestDobAttempts(int oldestDobAttempts) {
    this.oldestDobAttempts = oldestDobAttempts;
    if (this.session != null) {
      this.session.put(TOKEN_HOME_QUOTE_OLDEST_DOB_ATTEMPTS, this.oldestDobAttempts);
    }
  }

  /**
   * Increment the number of oldest dob attempts.
   */
  public void incrementOldestDobAttempts() {
    this.setOldestDobAttempts(this.getOldestDobAttempts() + 1);
  }

  /**
   * Reset oldest dob attempts.
   */
  public void resetOldestDobAttempts() {
    this.setOldestDobAttempts(0);
  }

  /**
   * Is this a Building Only Quote (and therefore upgradable)?
   * @return
   */
  public boolean isBuildingOnlyQuote() {
    return this.getQuoteType().equals(ActionBase.VALUE_QUOTE_TYPE_BLDG_ONLY);
  }

  /**
   * Is this a Contents Only Quote (and therefore upgradable)?
   * @return
   */
  public boolean isContentsOnlyQuote() {
    return this.getQuoteType().equals(ActionBase.VALUE_QUOTE_TYPE_CNTS_ONLY);
  }

  /**
   * Is this a Investor Quote
   * @return
   */
  public boolean isInvestorQuote() {
    return this.getQuoteType().equals(ActionBase.VALUE_QUOTE_TYPE_INV);
  }

}
