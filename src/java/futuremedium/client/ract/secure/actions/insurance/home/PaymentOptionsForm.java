package futuremedium.client.ract.secure.actions.insurance.home;

import com.ract.web.insurance.WebInsQuoteDetail;

/**
 *
 * @author gnewton
 */
public class PaymentOptionsForm extends PaymentOptionsBase implements PolicyAware {
    private static final long serialVersionUID = -600550982331566629L;
  private String paymentOption;
  private String frequency; 
  private String directDebitMethod;
  private String cardType;
  
  private String creditCardName;
  private String expiryDate;
  private String expiryDateMonth;
  private String expiryDateYear;
  private String accountName;
  
  private String bankingConditions;

  /**
   * @return the paymentOption
   */
  public String getPaymentOption() {
    if (this.paymentOption == null) {
      this.paymentOption = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.PAY_METHOD);
    }

    if (this.paymentOption == null || this.paymentOption.isEmpty()) {
    	this.paymentOption = WebInsQuoteDetail.PAY_ANNUALLY;
    }

    return paymentOption;
  }

  /**
   * @param paymentOption the paymentOption to set
   */
  public void setPaymentOption(String paymentOption) {
    this.paymentOption = paymentOption;
  }

  /**
   * @return the directDebitMethod
   */
  public String getDirectDebitMethod() {
    if (this.directDebitMethod == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.DD_TYPE);
      if (value != null) {
        this.directDebitMethod = (value.equals(WebInsQuoteDetail.ACC_TYPE_DEBIT) ? ActionBase.FIELD_DD_BANK_ACCOUNT : ActionBase.FIELD_DD_CREDIT_CARD);
      }
    }

    if (this.directDebitMethod == null || this.directDebitMethod.isEmpty()) {
    	this.directDebitMethod = ActionBase.FIELD_DD_CREDIT_CARD;
    }

    return directDebitMethod;
  }

  /**
   * @param directDebitMethod the directDebitMethod to set
   */
  public void setDirectDebitMethod(String directDebitMethod) {
    this.directDebitMethod = directDebitMethod;
  }

  /**
   * @return the cardType
   */
  public String getCardType() {
    if (this.cardType == null) {
      this.cardType = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.DD_TYPE);
    }
    return cardType;
  }

  /**
   * @param cardType the cardType to set
   */
  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  /**
   * @return the expiryDateMonth
   */
  public String getExpiryDateMonth() {
    if (this.expiryDateMonth == null && this.getExpiryDate() != null && this.getExpiryDate().contains("/")) {
      this.expiryDateMonth = this.getExpiryDate().split("/")[0];
    }
    return expiryDateMonth;
  }

  /**
   * @param expiryDateMonth the expiryDateMonth to set
   */
  public void setExpiryDateMonth(String expiryDateMonth) {
    this.expiryDateMonth = expiryDateMonth;
  }

  /**
   * @return the expiryDateYear
   */
  public String getExpiryDateYear() {
    if (this.expiryDateYear == null && this.getExpiryDate() != null && this.getExpiryDate().contains("/")) {
      this.expiryDateYear = this.getExpiryDate().split("/")[1];
    }
    return expiryDateYear;
  }

  /**
   * @param expiryDateYear the expiryDateYear to set
   */
  public void setExpiryDateYear(String expiryDateYear) {
    this.expiryDateYear = expiryDateYear;
  }

  /**
   * @return the accountName
   */
  public String getAccountName() {
    if (this.accountName == null && this.getDirectDebitMethod() != null && this.getDirectDebitMethod().equals(ActionBase.FIELD_DD_BANK_ACCOUNT)) {
      this.accountName = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.DD_NAME);
    }
    return accountName;
  }

  /**
   * @param accountName the accountName to set
   */
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  /**
   * @return the expiryDate
   */
  public String getExpiryDate() {
    if (this.expiryDate == null) {
      this.expiryDate = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.DD_EXP);
    }
    return expiryDate;
  }

  /**
   * @param expiryDate the expiryDate to set
   */
  public void setExpiryDate(String expiryDate) {
    this.expiryDate = expiryDate;
  }

  /**
   * @return the creditCardName
   */
  public String getCreditCardName() {
    if (this.creditCardName == null && this.getDirectDebitMethod() != null && this.getDirectDebitMethod().equals(ActionBase.FIELD_DD_CREDIT_CARD)) {
      this.creditCardName = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.DD_NAME);
    }
    return creditCardName;
  }

  /**
   * @param creditCardName the creditCardName to set
   */
  public void setCreditCardName(String creditCardName) {
    this.creditCardName = creditCardName;
  }

	public String getFrequency() {		
		if (this.frequency == null) {
			this.frequency = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.PAY_FREQ);
		}
		
		if (this.frequency == null || this.frequency.isEmpty()) {
			this.frequency = WebInsQuoteDetail.PAY_ANNUALLY;
		}
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

    public String getBankingConditions() {
        return bankingConditions;
    }

    public void setBankingConditions(String bankingConditions) {
        this.bankingConditions = bankingConditions;
    }

}
