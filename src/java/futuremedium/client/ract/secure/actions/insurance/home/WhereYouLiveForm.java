package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.List;

import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuoteDetail;

/**
 * Populate Where You Live form.
 * @author gnewton
 */
public class WhereYouLiveForm extends WhereYouLiveBase implements QuoteAware {

	private String residentialAddressQuery;
  private String residentialPostcode;
  private String residentialSuburb;
  private String residentialStreet;
  private String residentialStreetNumber;
  private String residentialGnaf;
  private String residentialLatitude;
  private String residentialLongitude;
  
  private WebInsClient defaultClient;
  
  /**
   * Retrieve saved postcode if present, otherwise pre-fill client postcode.
   * @return the residentialPostcode
   */
  public String getResidentialPostcode() {
    if (this.residentialPostcode == null) {
      this.residentialPostcode = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_POSTCODE);
    }
    if (this.residentialPostcode == null) {    	
  		this.residentialPostcode = this.getDefaultClient().getResiPostcode();
    }
    
    return residentialPostcode;
  }

  /**
   * Retrieve saved suburb if present, otherwise pre-fill client suburb.
   * @return the residentialSuburb
   */
  public String getResidentialSuburb() {
    if (this.residentialSuburb == null) {
      this.residentialSuburb = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_SUBURB);
    }
    if (this.residentialSuburb == null) {
    	this.residentialSuburb = this.getDefaultClient().getResiSuburb();
    }
    return residentialSuburb;
  }

  /**
   * Retrieve saved street if present, otherwise pre-fill client street.
   * @return the residentialStreet
   */
  public String getResidentialStreet() {
    if (this.residentialStreet == null) {
      this.residentialStreet = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_STREET);
    }
    if (this.residentialStreet == null) {
    	this.residentialStreet = this.getDefaultClient().getResiStreet();
    }
    return residentialStreet;
  }

  /**
   * Retrieve saved streetNumber if present, otherwise pre-fill client streetNumber.
   * @return the streetNumber
   */
  public String getResidentialStreetNumber() {
    if (this.residentialStreetNumber == null) {
      this.residentialStreetNumber = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_NO);
    }
    if (this.residentialStreetNumber == null) {
    	this.residentialStreetNumber = this.getDefaultClient().getResiStreetChar();
    }
    return residentialStreetNumber;
  }

	public WebInsClient getDefaultClient() {
		if (defaultClient == null) {
			List<WebInsClient> clients = this.getHomeService().retrieveClients(this);
    	if (clients != null && !clients.isEmpty()) {
    		defaultClient = clients.get(0);
    	}
		}
		return defaultClient;
	}

	public void setDefaultClient(WebInsClient defaultClient) {
		this.defaultClient = defaultClient;
	}
	
	public String getResidentialDisplayAddress() {
		StringBuilder addressString = new StringBuilder();
		
		if (this.getResidentialStreetNumber() != null) {
			addressString.append(this.getResidentialStreetNumber());
		}
		if (this.getResidentialStreet() != null) {
			addressString.append(" " + this.getResidentialStreet());
		}
		if (this.getResidentialSuburb() != null) {
			addressString.append(" " + this.getResidentialSuburb());
		}
		if (this.getResidentialPostcode() != null) {
			addressString.append(" " + this.getResidentialPostcode());
		}
		
		return addressString.toString();
	}

	public String getResidentialAddressQuery() {
		// if we have an un-validated address from client, pre-fill query string
		if (this.getDefaultClient() != null 
				&& (this.getDefaultClient().getResiGnaf() == null || this.getDefaultClient().getResiGnaf().isEmpty())
			) {
			this.residentialAddressQuery = this.getResidentialDisplayAddress();
		}
		return residentialAddressQuery;		
	}

	public void setResidentialAddressQuery(String addressQuery) {
		this.residentialAddressQuery = addressQuery;
	}

	/**
	 * Retrieve saved GNAF if present, otherwise pre-fill client GNAF.
   * @return
	 */
	public String getResidentialGnaf() {
		if (this.residentialGnaf == null) {
      this.residentialGnaf = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_GNAF);
    }
    if (this.residentialGnaf == null) {
    	this.residentialGnaf = this.getDefaultClient().getResiGnaf();
    }
		return residentialGnaf;
	}

	public void setResidentialGnaf(String residentialGnaf) {
		this.residentialGnaf = residentialGnaf;
	}

	/**
	 * Retrieve saved Latitude if present, otherwise pre-fill client Latitude.
   * @return
	 */
	public String getResidentialLatitude() {
		if (this.residentialLatitude == null) {
      this.residentialLatitude = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_LATITUDE);
    }
    if (this.residentialLatitude == null) {
    	this.residentialLatitude = this.getDefaultClient().getResiLatitude();
    }
		return residentialLatitude;
	}

	public void setResidentialLatitude(String residentialLatitude) {
		this.residentialLatitude = residentialLatitude;
	}

	/**
	 * Retrieve saved Longitude if present, otherwise pre-fill client Longitude.
   * @return
	 */
	public String getResidentialLongitude() {
		if (this.residentialLongitude == null) {
      this.residentialLongitude = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_LONGITUDE);
    }
    if (this.residentialLongitude == null) {
    	this.residentialLongitude = this.getDefaultClient().getResiLongitude();
    }
		return residentialLongitude;
	}

	public void setResidentialLongitude(String residentialLongitude) {
		this.residentialLongitude = residentialLongitude;
	}

}
