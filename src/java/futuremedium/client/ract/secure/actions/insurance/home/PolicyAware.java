package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * This is a marker interface, which is used to indicate whether an action is
 * part of the "Fulfilment" area of the system.
 * @author gnewton
 */
public interface PolicyAware extends PersistanceAware {

}
