package futuremedium.client.ract.secure.actions.insurance.car;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.*;
import java.util.*;

import com.opensymphony.xwork2.ActionContext;

import com.ract.client.ClientTitleVO;
import com.ract.common.*;
import com.ract.web.insurance.*;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

import futuremedium.client.ract.secure.*;
import futuremedium.client.ract.secure.actions.*;
import futuremedium.client.ract.secure.entities.insurance.car.*;
import futuremedium.client.ract.secure.actions.insurance.InsuranceBase;
import futuremedium.client.ract.secure.actions.insurance.home.ActionBase;

/**
 * The parent class for all other page classes. It provides common methods which can be shared by all other page classes.
 * It also defines constants for looking up validation and message text that are displayed on the screen.
 * @author xmfu
 */
public class QuotePageSave extends InsuranceBase {
    private static final long serialVersionUID = 7678654725446867893L;
  // Constant for page flow.
  public final static String BUTTON_BACK = "back";
  public final static String BUTTON_NEXT = "next";
  // Constants to look up validation message in properties and as a flag in validate method.
  protected final static String VALIDATION_FAILED = "validationFailed";
  protected final static int VALIDATE_STRING = 1;
  protected final static int VALIDATE_INTEGER = 2;
  protected final static int VALIDATE_DATETIME_STRING = 3;
  protected final static int VALIDATE_INTEGER_STRING = 4;
  protected final static int VALIDATE_SELECT = 5;
  protected final static int VALIDATE_FOUR_INTEGER = 6;
  protected final static int VALIDATE_CUSTOMIZED = 7;
  protected final static int VALIDATE_RADIO = 8;
  protected final static int VALIDATE_CREDIT_CARD_DATE = 9;
  protected final static int VALIDATE_ACCIDENT_DATE = 10;
  protected final static int VALIDATE_DOB = 11;
  protected final static int VALIDATE_CAPTCHA = 12;
  protected final static int VALIDATE_COMMENCED_DRIVING = 13;
  protected final static int VALIDATE_INTEGER_ONLY = 14;
  protected final static int VALIDATE_NAME = 15;
  protected final static int VALIDATE_TELEPHONE = 16;
  protected final static int VALIDATE_EMAIL = 17;
  protected final static String SYSTEM_MESSAGE = "We need to ask you some more questions please Contact Us";
  protected final static String PASSED = "passed";
  protected final static String VALUE_UNACCEPTABLE_VEHICLE = "U";
  protected final static String VALUE_HIGH_PERFORMANCE_VEHICLE = "H";
  protected final static String BENEFITS_LIST = "benefitsList";
  // Flag indicates whether the next page is a message page, which is a sign for stop.
  // Some conditions turn the page flow to message page, such as people less than 25 years old is driving a high performance vehicle.
  public final static String MESSAGE_PAGE = "message";
  // Format a date.
  protected final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  protected final static SimpleDateFormat creditCardDateFormat = new SimpleDateFormat("MM/yyyy");
  protected final static SimpleDateFormat accidentDateFormat = new SimpleDateFormat("MM/yyyy");
  public final static String QUOTE_NUMBER = "quoteNO";
  public final static String NEW_QUOTE = "newQuote";
  public final static String SECURE_QUOTE_NUMBER = "secureQuoteNo";
  // Constants to look up messages from properties.
  protected final static int MESSAGE_CARUSAGE_UNINSURANCABLE = 1;
  protected final static int MESSAGE_CARUSAGE_COMMERCIAL = 2;
  protected final static int MESSAGE_CARVALUE = 3;
  protected final static int MESSAGE_HIGHPERFORMANCE = 4;
  protected final static int MESSAGE_TWOACCIDENTS = 5;
  protected final static int MESSAGE_ACCIDENTS_MORETHANTWO = 6;
  protected final static int MESSAGE_EXTRADAMAGE = 7;
  protected final static int MESSAGE_NONSTANDARDACCRESSORIES = 8;
  protected final static int MESSAGE_ADDITIONALCRIMINAL = 9;
  protected final static int MESSAGE_FIFTYPLUS = 10;
  protected final static int MESSAGE_SUSPENSION = 11;
  protected final static int MESSAGE_NOPRIMARYCONTACT = 12;
  protected final static int MESSAGE_CANCELLEDINSURANCE = 13;
  protected final static int MESSAGE_UNACCEPTABLE_VEHICLE = 14;
  protected final static int MESSAGE_ZERO_PREMIUM = 15;
  protected final static int MESSAGE_UNACCEPTABLE_SUBURB = 16;
  protected final static int MESSAGE_UNKNOWN_SUBURB = 17;
  protected final static int MESSAGE_SAVE_FAIL = 18;
  
  // Field constants for DB storage
  public final static String FIELD_CURRENT_PAGE_ID = "currentPageId";
  
  public final static String FIELD_COMP_MONTHLY_PREMIUM = WebInsQuote.COVER_COMP + "_monthlyPremium";
  public final static String FIELD_COMP_ANNUAL_PREMIUM = WebInsQuote.COVER_COMP + "_annualPremium";
  public final static String FIELD_TP_MONTHLY_PREMIUM = WebInsQuote.COVER_TP + "_monthlyPremium";
  public final static String FIELD_TP_ANNUAL_PREMIUM = WebInsQuote.COVER_TP + "_annualPremium";
  
  protected final static String FIELD_VIEWED_PREMIUM = "reachedPage9";
  protected final static String FIELD_LOOKUP_OPTION = "vehicleLookupOption";
  
  protected final static String LOOKUP_OPTION_REGO = "rego";
  protected final static String LOOKUP_OPTION_LOOKUP = "lookup";
  
  protected final static String VALUE_FIN_TYPE_NO = "N";
  
  // Common action attributes.
  private Long pageId;
  private Long lastPageId;
  private QuotePage quotePage;
  private int pageNumbers = 0;
  // To capture whether user clicked 'next' or 'previous' button.
  private String message;
  private String showBackButton;
  private String submitButton;
  private String hasDriver2;
  private String showNewPolicyHolderLink;
  private Long progress;
  private Long totalPages;
  // URL used for next action.
  private String redirectUrl;
  private String summary;
  private String doNextStep;
  // A flag to indicate whether the saved premium is $0.
  private boolean zeroPremium = false;
  private boolean showCAPTCHA = false;
  private boolean showBenefits = false;
  private Boolean pipDiscount = false;
  private Boolean ssDiscount = false;
  private Boolean immobiliserDiscount = false;
  private Boolean hireCarDiscount = false;
  private Boolean windscreenDiscount = false;
  private Boolean fireTheftDiscount = false;
  private Boolean compDiscounts = true;
  
  protected final static String PIP_DISCOUNT = "pipDiscount";
  protected final static String SS_DISCOUNT = "ssDiscount";
  protected final static String IMMOBILISER_DISCOUNT = "immobiliserDiscount";
  protected final static String WINDSCREEN_DISCOUNT = "windscreendDiscount";
  protected final static String HIRECAR_DISCOUNT = "hireCarDiscount";
  protected final static String FIRETHEFT_DISCOUNT = "fireTheftDiscount";
  
  protected final static String VEHICLE_OPTIONS = "vehicleOptions";
  
  protected final static String COMP_DISCOUNTS = "compDiscounts";
  
  protected final static String CAR_WINDSCREEN = "COMCWIND";
  protected final static String CAR_HIRECAR = "COMCHIRE";
  protected final static String TP_FIREANDTHEFT = "TPPDFIRE";
  
  
  // Xml Parser for Quote Questions.
  protected QuoteQuestionManager quoteQuestionManager = new QuoteQuestionManager();
  private List<QuotePage> flow;
  private String dt; // discount type
  private String startPage = "";
  private String existingQuoteNumber = "";

  @Override
  public String execute() throws Exception {
    this.setUp();
    
    if (this.getSubmit().equals(BUTTON_NEXT)) {
      // Next page, page number + 1;
      this.setPageId(this.getPageId() + 1);
    } else {
      // Previous page, page number - 1;
      this.setPageId(this.getPageId() - 1);
    }

    return next();
  }

  public String process() throws Exception {
    return this.execute();
  }

  public String goFirstPage() throws Exception {
    this.setPageId(1L);
    return next();
  }

  public String getParameter(String name) {
    Map<String, Object> parameters = ActionContext.getContext().getParameters();

    if (parameters.containsKey(name)) {
      String[] parameter = (String[]) parameters.get(name);

      if (parameter != null) {
        return parameter[0];

      } else {
        return "";
      }
    } else {
      return "";
    }
  }

  // A common method used to validate form input.
  public boolean validate(Object object, final int type) {
    if (type == VALIDATE_NAME) {
      String value = (String) object;

      if (value == null || value.equals("") || !RactActionSupport.REGEXP_NAME.matcher(value).matches()) {
        return false;
      } else {
        return true;
      }

    } else if (type == VALIDATE_STRING) {
      String value = (String) object;
      if (value == null || value.equals("")) {
        return false;
      } else {
        return true;
      }
    } else if (type == VALIDATE_EMAIL) {
      String value = (String) object;
      if (value == null || !RactActionSupport.REGEXP_EMAIL.matcher(value).matches()) {
        return false;
      } else {
        return true;
      }
    } else if (type == VALIDATE_INTEGER) {
      if (object == null) {
        return false;
      } else {
        try {
          Integer value = (Integer) object;
          return true;
        } catch (Exception e) {
          return false;
        }
      }
    } else if (type == VALIDATE_DATETIME_STRING) {
      String value = (String) object;
      if (value == null || value.equals("")) {
        return false;
      } else {
        try {
          dateFormat.parse(value);
          new DateTime(value);
          return true;
        } catch (Exception e) {
          return false;
        }
      }
    } else if (type == VALIDATE_INTEGER_STRING) {
      String value = (String) object;
      if (value == null || value.equals("")) {
        return false;
      } else {
        try {
          Long.valueOf(value);

          return true;
        } catch (Exception e) {
          return false;
        }
      }
    } else if (type == VALIDATE_SELECT) {
      String value = (String) object;
      if (value == null) {
        return false;
      } else if (value.equals("")) {
        return false;
      } else {
        return true;
      }
    } else if (type == VALIDATE_CREDIT_CARD_DATE) {
      String value = (String) object;
      if (value == null || value.equals("")) {
        return false;
      } else {
        try {
          creditCardDateFormat.parse(value);

          String parts[] = value.split("/");
          int test = Integer.parseInt(parts[0]);
          if (test < 1 || test > 12) {  // disallow e.g. "00/12"
            return false;
          }

          return true;
        } catch (Exception e) {
          return false;
        }
      }
    } else if (type == VALIDATE_ACCIDENT_DATE) {
      String value = (String) object;
      if (value == null || value.equals("")) {
        return false;
      } else {
        try {
          accidentDateFormat.parse(value);
          return true;
        } catch (Exception e) {
          return false;
        }
      }
    } else if (type == VALIDATE_DOB) {
      String value = (String) object;
      if (value == null || value.equals("")) {
        return false;
      } else {
        try {
          Date date = dateFormat.parse(value);
          Calendar dob = Calendar.getInstance();
          dob.setTime(date);

          // test that the value can be intepreted as a DateTime
          new DateTime(value);

          Calendar cutoff = Calendar.getInstance();
          cutoff.add(Calendar.YEAR, -16);

          if (dob.after(cutoff)) {
            return false;
          }

          cutoff = Calendar.getInstance();
          cutoff.add(Calendar.YEAR, -100);

          if (dob.before(cutoff)) {
            return false;
          }

          return true;
        } catch (Exception e) {
          return false;
        }
      }
    } else if (type == VALIDATE_TELEPHONE) {
      String value = (String) object;
      if (value.length() == 10) {
        return true;
      }
    }

    return false;
  }

  // Get sub question by Id from insurance_quote_question.xml.
  public QuoteQuestion getSubQuestionById(String questionId, String subPageId) {
    QuoteQuestion quoteQuestion = this.getQuestionById(questionId);

    for (QuoteQuestion subQuoteQuestion : quoteQuestion.getSubQuestions()) {
      if (subQuoteQuestion.getId().equals(subPageId)) {
        return subQuoteQuestion;
      }
    }

    return null;
  }

  // Get question by Id from insurance_quote_question.xml.
  public QuoteQuestion getQuestionById(String questionId) {
    if (this.getQuotePage() == null) {
      this.setQuotePage(this.quoteQuestionManager.getPage(this.getPageId()));
    }

    if (this.getQuotePage() != null && this.getQuotePage().getQuestions() != null && this.getQuotePage().getQuestions().size() > 0) {
      for (QuoteQuestion quoteQuestion : this.getQuotePage().getQuestions()) {
        if (quoteQuestion.getId() != null && quoteQuestion.getId().equals(questionId)) {
          return quoteQuestion;
        }
      }
    }

    return null;
  }

  // Look up validation message to display on the screen from properties.
  public String getValidationMessage(String fieldName, int validationType) {
    String validationMessage = "";

    // Load validation message from properties file.
    if (validationType == VALIDATE_NAME) {
      validationMessage = Config.configManager.getProperty("insurance.validation.string.name");
    }

    if (validationType == VALIDATE_STRING) {
      validationMessage = Config.configManager.getProperty("insurance.validation.string.empty");
    } else if (validationType == VALIDATE_INTEGER) {
      validationMessage = Config.configManager.getProperty("insurance.validation.integer.empty");
    } else if (validationType == VALIDATE_INTEGER_ONLY) {
      validationMessage = Config.configManager.getProperty("insurance.validation.integer.numberonly");
    } else if (validationType == VALIDATE_DATETIME_STRING) {
      validationMessage = Config.configManager.getProperty("insurance.validation.date.invalidate");
    } else if (validationType == VALIDATE_FOUR_INTEGER) {
      validationMessage = Config.configManager.getProperty("insurance.validation.integer.fourdigit");
    } else if (validationType == VALIDATE_CUSTOMIZED) {
      validationMessage = fieldName;
    } else if (validationType == VALIDATE_RADIO) {
      validationMessage = Config.configManager.getProperty("insurance.validation.radio.empty");
    } else if (validationType == VALIDATE_SELECT) {
      validationMessage = Config.configManager.getProperty("insurance.validation.select.empty");
    } else if (validationType == VALIDATE_CAPTCHA) {
      validationMessage = Config.configManager.getProperty("insurance.validation.captcha");
    } else if (validationType == VALIDATE_COMMENCED_DRIVING) {
      validationMessage = Config.configManager.getProperty("insurance.validation.commenceddriving");
    } else if (validationType == VALIDATE_DOB) {
      validationMessage = Config.configManager.getProperty("insurance.validation.dob");
    } else if (validationType == VALIDATE_TELEPHONE) {
      validationMessage = Config.configManager.getProperty("insurance.validation.telephonenumber.wrong");
    } else if (validationType == VALIDATE_EMAIL) {
      validationMessage = Config.configManager.getProperty("insurance.validation.email.wrong");
    }

    // Replace the field name.
    if (validationType != VALIDATE_CUSTOMIZED) {
      validationMessage = validationMessage.replaceAll("fieldname", fieldName);
    }

    return validationMessage;
  }

  // Validation failed. Go back to previous page.
  public String validationFailed(String fieldName, int validationType, boolean loadPage) throws Exception {
    this.setActionMessage(getValidationMessage(fieldName, validationType));

    if (loadPage) {
      this.back();
    }

    return VALIDATION_FAILED;
  }
  
  public static String lookupMessageTitle(int messageId) {
    
    switch (messageId) {
      case MESSAGE_ACCIDENTS_MORETHANTWO:
        return Config.configManager.getProperty("insurance.message.accidentsmorethantwo");
      case MESSAGE_ADDITIONALCRIMINAL:
        return Config.configManager.getProperty("insurance.message.additionalcriminal");
      case MESSAGE_CARUSAGE_COMMERCIAL:
        return Config.configManager.getProperty("insurnace.message.carusage.commercial");
      case MESSAGE_CARUSAGE_UNINSURANCABLE:
        return Config.configManager.getProperty("insurance.message.carusage.uninsurancable"); 
      case MESSAGE_CARVALUE:
        return Config.configManager.getProperty("insurance.message.carvalue");
      case MESSAGE_EXTRADAMAGE:
        return Config.configManager.getProperty("insurance.message.extradamage");
      case MESSAGE_FIFTYPLUS:
        return Config.configManager.getProperty("insurance.message.fiftyplus");
      case MESSAGE_HIGHPERFORMANCE:
        return Config.configManager.getProperty("insurance.message.highperformancecar");
      case MESSAGE_NONSTANDARDACCRESSORIES:
        return Config.configManager.getProperty("insurance.message.nonstandardaccessories");
      case MESSAGE_TWOACCIDENTS:
        return Config.configManager.getProperty("insurance.message.accidentsmorethantwo");
      case MESSAGE_SUSPENSION:
        return Config.configManager.getProperty("insurance.message.suspension");
      case MESSAGE_NOPRIMARYCONTACT:
        return Config.configManager.getProperty("insurnace.message.noprimarycontact");
      case MESSAGE_CANCELLEDINSURANCE:
        return Config.configManager.getProperty("insurance.message.cancelledinsurance");
      case MESSAGE_UNACCEPTABLE_VEHICLE:
        return Config.configManager.getProperty("insurance.message.unacceptablevehicle");
      case MESSAGE_ZERO_PREMIUM:
        return Config.configManager.getProperty("insurance.message.zeropremium");
      case MESSAGE_UNACCEPTABLE_SUBURB:
        return Config.configManager.getProperty("insurance.message.unacceptablesuburb");
      case MESSAGE_UNKNOWN_SUBURB:
          return Config.configManager.getProperty("insurance.message.unknownsuburb");
      case MESSAGE_SAVE_FAIL:
          return Config.configManager.getProperty("insurance.message.save.fail");
      default:
        return SYSTEM_MESSAGE;
    }
  }
    
  public static String lookupMessageCode(int messageId) {
    
    switch (messageId) {
      case MESSAGE_ACCIDENTS_MORETHANTWO:
        return "Accidents More Than Two";
      case MESSAGE_ADDITIONALCRIMINAL:
        return "Criminal Convictions";
      case MESSAGE_CARUSAGE_COMMERCIAL:
        return "Car Usage Commercial";
      case MESSAGE_CARUSAGE_UNINSURANCABLE:
        return "Car Usage Uninsurancable"; 
      case MESSAGE_CARVALUE:
        return "Car Value";
      case MESSAGE_EXTRADAMAGE:
        return "Extra Damage";
      case MESSAGE_FIFTYPLUS:
        return "invalid 50+ policyholder";
      case MESSAGE_HIGHPERFORMANCE:
        return "high performance car";
      case MESSAGE_NONSTANDARDACCRESSORIES:
        return "non standard accessories";
      case MESSAGE_TWOACCIDENTS:
        return "invalid 50+ policyholder";
      case MESSAGE_SUSPENSION:
        return "Suspension";
      case MESSAGE_NOPRIMARYCONTACT:
        return "noprimarycontact";
      case MESSAGE_CANCELLEDINSURANCE:
        return "cancelled insurance";
      case MESSAGE_UNACCEPTABLE_VEHICLE:
        return "unacceptable vehicle";
      case MESSAGE_ZERO_PREMIUM:
        return "zero premium";
      case MESSAGE_UNACCEPTABLE_SUBURB:
        return "unacceptable suburb";
      case MESSAGE_UNKNOWN_SUBURB:
          return "unknown suburb";
      case MESSAGE_SAVE_FAIL:
          return "save fail";
      default:
        return SYSTEM_MESSAGE;
    }
  }

  
  public String messagePage(int messageId) throws Exception {

    // Set message page has called.
    // If it is called, user cannot walk away from the message page as defined in business logic.
  	this.setMessage(lookupMessageTitle(messageId));
    
    this.setCalledMessagePage(messageId);
   
    return MESSAGE_PAGE;
  }
  
  
  

  // Stored what message page has been called and the Id can be used later to display relavent message page.
  public int getMessageId() {
    String messageId = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "messageId");
    return Integer.valueOf(messageId);
  }

  public void setMessageId(int messageId) {
    this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), "messageId", String.valueOf(messageId));
  }

  // Whether message page is called.
  public boolean isCalledMessagePage() {
    String calledMessagePage = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), MESSAGE_PAGE);

    if (calledMessagePage == null) {
      return false;
    } else {
      return true;
    }
  }

  // Set whether message page has been called.
  public void setCalledMessagePage(int messageId) {
      this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), MESSAGE_PAGE, Integer.toString(messageId));
      this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), FIELD_ABORTED, lookupMessageCode(messageId));
  }

  // Page 9 is the premium page, once a user has reached this page, the user cannot go back to from page 1 to page 8.
  public boolean isReachedPage9() throws Exception {
    this.setUp();

    if (this.getQuoteNo() != null) {
      String reachedPage9 = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "reachedPage9");

      if (reachedPage9 != null && reachedPage9.equals("true")) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  }

  /**
   * This method loads answers for next page if it is loading for existing quote. 
   * Otherwise, just load empty answers for new quote.
   */
  public String next() throws Exception {
    this.setUp();
    
    //clean session on StartQuote
    if(ActionContext.getContext().getName() != null && ActionContext.getContext().getName().equals("StartQuote")){
      this.cleanSession();
    }

    // Get total page numbers.
    this.pageNumbers = this.quoteQuestionManager.getPageNumbers();
    Config.logger.debug("Value of pageNumbers: " + pageNumbers);

    if (this.getPageId() == null) {
      // Get current stored page Id.
      // User is retrieving an existing quote.


      String currentPageId = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), QuotePageSave.FIELD_CURRENT_PAGE_ID);

      try {
        this.setPageId(Long.valueOf(currentPageId));
      } catch (Exception e) { // Default to first page
        this.setPageId(new Long(1));
      }
    }

    // Get questions from insurance_quote_question.xml.
    // Current page Id exceeds the total page number, go back to the first page.
    if ((this.getPageId() > this.pageNumbers)) {
      this.setPageId(1L);
      this.setQuotePage(this.quoteQuestionManager.getPage(this.getPageId()));
    } else {
      // Check if pageId is greater than or equal to 9, it should never go back to pages before page 9 if user has reached page 9.
      String reachedPage9 = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), QuotePageSave.FIELD_VIEWED_PREMIUM);

      if (reachedPage9 != null && reachedPage9.equals("true")) {
        if (this.getPageId() <= 9 && this.getPageId() >= 0) {
          this.setPageId(9l);
        }
      }

      this.setQuotePage(this.quoteQuestionManager.getPage(this.getPageId()));

      // Only proceed if current quote number is not null.
      if (this.getQuoteNo() != null) {
        int quoteNo = this.getQuoteNo();
        // Retrieve web insurance quote from RACT.
        WebInsQuote webInsQuote = getInsuranceMgr().getQuote(quoteNo);

        // Check to ensure that saved premium is not $0
        if (this.getPageId() > 9) {
          String premium = getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.PREMIUM);
          if (premium == null || Double.parseDouble(premium) <= 0) {
            this.zeroPremium = true;
          }
        }

        // Pre populate answers for the next page, if it is retrieving existing quote.
        switch (this.getPageId().intValue()) {
          // Page 2.
          case 2:
            if (webInsQuote != null) {
              String member = "";

              if (this.isMember()) {
                member = "yes";
              } else {
                member = "no";
              }

              this.loadPage2(this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.PIP_DISCOUNT), this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS), member, "", "");
            }

            break;
          // Page 3.
          case 3:
            if (webInsQuote != null) {
              GlVehicle glVehicle = this.getGlassesMgr().getVehicleCodes(this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.NVIC), webInsQuote.getQuoteDate());

              // load NVIC if we are using rego lookup
              String lookupOption = this.getInsuranceMgr().getQuoteDetail(quoteNo, FIELD_LOOKUP_OPTION);
              String nvicDirect = null;
              if (lookupOption != null && lookupOption.equals(LOOKUP_OPTION_REGO)) {
              	nvicDirect = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.NVIC);
              }
              
              //if (glVehicle != null && glVehicle.getNvic() != null) {
                this.loadPage3(glVehicle, 
                		this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.IMOBILISER), 
                		this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.USEAGE), 
                		this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.FINANCIER), 
                		this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.FIN_TYPE), 
                		this.getInsuranceMgr().getQuoteDetail(quoteNo, FIELD_LOOKUP_OPTION), 
                		this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.REG_NO),
                		nvicDirect);
              //}
            }
            break;
          // Page 4.
          case 4:

            // Load garage address, this address is not associated to any clients.
            GaragedAddress garagedAddress = this.populateGaragedAddress(null, "garagedAddress");
            // if not already saved, attempt to lookup attached client
            if (garagedAddress == null) {
            	List<WebInsClient> clients = this.getInsuranceMgr().getClients(quoteNo);
            	if (clients != null && !clients.isEmpty()) {
            		garagedAddress = this.populateGaragedAddress(clients.get(0), "residential");
            	}
            }
            this.loadPage4(garagedAddress);

            break;
          // Page 5.
          case 5:
            String savedValue = this.getInsuranceMgr().getQuoteDetail(quoteNo, "vehicleValue");
            Config.logger.debug(Config.key + ": getting glasses value: nvic=" + this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.NVIC));
            
            String nvic = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.NVIC);
            GlVehicle vehicle = this.getGlassesMgr().getVehicle(nvic, webInsQuote.getQuoteDate());
            Integer initialVehicleValue = vehicle.getValue();
            
            Calendar cal = Calendar.getInstance();
            cal.setTime(webInsQuote.getQuoteDate());
            int vehicleAge = cal.get(Calendar.YEAR) - vehicle.getVehYear();

            Integer vehicleValue = 0;
            int roundFactor = 0;

            // Load saved agreed value for existing quote.
            if (savedValue != null && !savedValue.isEmpty()) {
              //this.loadQuestion(this.getSubQuestionById("q10", "sub6"), "$" + savedValue);
              vehicleValue = Integer.parseInt(savedValue);
              Config.logger.debug(Config.key + ": loaded agreed value of " + vehicleValue);
            } else { // No saved agreed value, load the default one. If also means, this is a new quote.
              vehicleValue = initialVehicleValue;
              savedValue = String.valueOf(vehicleValue);
              //this.loadQuestion(this.getSubQuestionById("q10", "sub6"), "$" + savedValue);
              Config.logger.debug(Config.key + ": loaded initial value of " + vehicleValue);
              getInsuranceMgr().setQuoteDetail(quoteNo, "vehicleValue", savedValue);
            }

            // determine range for agreed value
          	roundFactor = getRoundFactor(vehicleValue);            
            int lower = getLowerAgreedValueBound(roundFactor, initialVehicleValue, vehicleAge);
            int upper = getUpperAgreedValueBound(roundFactor, initialVehicleValue, vehicleAge);
            
            Config.logger.debug(Config.key + ": loaded lower value of " + lower);
            Config.logger.debug(Config.key + ": loaded upper value of " + upper);

            this.getHttpRequest().setAttribute("agreedValueIncrement", roundFactor);
            this.getHttpRequest().setAttribute("agreedValueDefault", roundTo(vehicleValue, roundFactor)-roundFactor);      
            this.getHttpRequest().setAttribute("agreedValueLower", lower);
            this.getHttpRequest().setAttribute("agreedValueUpper", upper);
            
            this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.AGREED_VALUE_DEFAULT, String.valueOf(roundTo(vehicleValue, roundFactor)-roundFactor));
            this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.AGREED_VALUE_LOWER, String.valueOf(lower));
            this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.AGREED_VALUE_UPPER, String.valueOf(upper));

            break;
          // Page 6.
          case 6:
            //page 6 was merged into page 5
            this.loadPage6(this.getDrivers());

            break;
          // Page 7.
          case 7:
            WebInsClient ratingDriver = this.getInsuranceMgr().getRatingDriver(quoteNo);

            if (ratingDriver != null) {
              if (ratingDriver.getYearCommencedDriving() != null && ratingDriver.getNumberOfAccidents() != null) {
                this.loadPage7(String.valueOf(ratingDriver.getYearCommencedDriving()), String.valueOf(ratingDriver.getNumberOfAccidents()), "no", ratingDriver.getGivenNames(), ratingDriver.getSurname());
              }
            }

            // Create a title for the ratingDriver.
            String newDescription = this.getQuotePage().getDescription().get(0).replaceAll("name", ratingDriver.getGivenNames() + " " + ratingDriver.getSurname());

            this.getQuotePage().getDescription().clear();
            this.getQuotePage().getDescription().add(newDescription);

            this.setShowCAPTCHA(true);

            break;
          // Page 8.
          case 8:
            this.loadPage8();
            break;
          // Page 9.
          case 9:

            this.loadPremium();
            break;
          // Page 10.
          case 10:
            this.loadFinalSummaryPage();
            break;
          // Page 11.
          case 11:
            loadPage11(this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.COVER_START), getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.REG_NO), getInsuranceMgr().getQuoteDetail(quoteNo, "nonExistingDamage"), null, getInsuranceMgr().getQuoteDetail(quoteNo, "extras"));
            break;
          // Page 12.
          case 12:
            // Load the policy holder selection list.
            this.loadPolicyHolderOptions();
            // Initialise the list before entering the policy holder page.
            // As this list keeps a flag about whether a client in the quote is policy holder.
            this.initializeIsPolicyHolderArray();
            break;
          // Load policy holder edit page.
          case 13:
            if (this.isMember()) { // Load page 13 for RACT member.
              // Load page based on selected policy holder option.
              String policyHolderId = this.getInsuranceMgr().getQuoteDetail(quoteNo, "policyHolderOption");
              String policyHolderOption = "-1";
              if(!policyHolderId.equals("-1")) {
                  List<WebInsClient> clients = this.getInsuranceMgr().getClients(quoteNo);
                  for(WebInsClient client : clients) {
                      if(client.getWebClientNo().toString().equals(policyHolderId)) {
                          policyHolderOption = client.getSelectionDescription();
                          break;
                      }
                  }
              }
              
              // Create new policy holders.
              if (policyHolderOption.equals("-1")) {
                this.setNewPolicyInSession("true");
                this.initializeIsPolicyHolderArray();
                this.setShowNewPolicyHolderLink("true");
                this.loadPolicyHolders("true", policyHolderOption, true);
              } else { // Load existing policy holders.
                this.setNewPolicyInSession("false");
                this.loadPolicyHolders("false", policyHolderOption, false);
              }

            } else { // Load page 13 for non RACT member.
              this.initializeIsPolicyHolderArray();
              this.setNewPolicyInSession("true");
              this.setShowNewPolicyHolderLink("true");
              this.loadPolicyHolders("true", "-1", true);
            }

            break;
          // Page 14.
          case 14:
            // Load additional policy holder page.
            this.loadPage14(false);
            break;
          // Page 15.
          case 15:
            // Load primary contact policy holder page.
            this.loadFirstContactPolicyHolder();
            break;
            // Page 16.
          case 16:
              // Load terms and conditions page.
              // Need to ensure user has clicked and agreed the terms and conditions.
              this.setShowBenefits(true);
              String page16Checked = this.getInsuranceMgr().getQuoteDetail(quoteNo, "page16Checked");
              if (page16Checked != null && page16Checked.equals("true")) {
                this.loadQuestion(this.getQuestionById("q25"), "checked");
              }
              break;
          // Page 17.
          case 17:
            // Load payment page.
            // Existing quote.
            if (getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_METHOD) != null && !getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_METHOD).isEmpty()) {
              loadPage17();
            } else { // New quote.
              //this.loadDirectDebitOption();
            	loadPage17();
            }
            break;
          // Page 18.
          case 18:
          	// tmp placeholder to allow cover note to be downloaded
          	Integer oldQuoteNo = this.getQuoteNo();
          	
            // Load the last page in the quote process.
            // Clean the quote number after user has completed the quote.

            //This has been done in a way hard to follow: These get called and set the items values into the action
            //so when the session is clear and they are recalled via the jspx they can get the action value beacuse the session gets cleared.
            this.isPipDiscount();
            this.isSsDiscount();
            this.isImmobiliserDiscount();
            this.isHireCarDiscount();
            this.isWindscreenDiscount();
            this.isFireTheftDiscount();
            this.getCompDiscounts();
                        
            this.setQuoteNo(null);
            this.setNewQuote(null);
            this.setSecureQuoteNumber(null);
            cleanSession();
            
            // allow cover note to be accessed
            this.getSession().put(TOKEN_DOC_QUOTE_NO, oldQuoteNo);
            
            break;
          default:
        }

        // 0 premium is not allowed, display message page and stop the quote.
        if (this.zeroPremium) {
          Config.logger.warn(Config.key + ": Detected quote '" + quoteNo + "' with $0 premium, exiting.");
          return this.messagePage(MESSAGE_ZERO_PREMIUM);
        }
      }
    }

    // go to next page.
    return SUCCESS;
  }

  protected int getRoundFactor(Integer vehicleValue) {
  	int roundFactor;
  	
  	if (vehicleValue < 1000) {
      roundFactor = 10;
  	} else if (vehicleValue >= 1000 && vehicleValue <= 50000) {
      roundFactor = 100;
    } else { // > 50000
      roundFactor = 1000;
    }
  	
  	return roundFactor;
  }
  
    protected Integer getLowerAgreedValueBound(int roundFactor, Integer initialVehicleValue, int vehicleAge) throws RemoteException {
        DateTime quoteDate = this.getInsuranceMgr().getQuote(this.getQuoteNo()).getQuoteDate();
        float lowerFactor = this.getInsuranceMgr().getMinVehicleValuePercentage(quoteDate, vehicleAge);
        int lower = roundTo(Math.round(initialVehicleValue * lowerFactor), roundFactor);
        return lower;
    }

    protected Integer getUpperAgreedValueBound(int roundFactor, Integer initialVehicleValue, int vehicleAge) throws RemoteException {
        DateTime quoteDate = this.getInsuranceMgr().getQuote(this.getQuoteNo()).getQuoteDate();
        float upperFactor = this.getInsuranceMgr().getMaxVehicleValuePercentage(quoteDate, vehicleAge);
        int upper = roundTo(Math.round(initialVehicleValue * upperFactor), roundFactor) - roundFactor;
        return upper;
    }
  
  /**
   * Round a number to the nearest factor
   * @param x
   * @param factor
   * @return
   */
  protected int roundTo(int x, int factor) {
    int m = (x / factor) + 1;
    int rounded = m * factor;

    return rounded;
  }

  /**
   * Initialise an array list to hold the a flag which indicates whether a newly added or a existing client is a policy holder or not.
   *
   */
  public void initializeIsPolicyHolderArray() throws Exception {
    if (this.getQuoteTempMemoryObject() == null) {
      this.setQuoteTempMemoryObject();
    }

    if (this.getQuoteTempMemoryObject().getIsPolicyHolders().isEmpty()) {
      if (this.isMember()) {
        for (int i = 0; i < this.getDrivers().size(); i++) {
          this.getQuoteTempMemoryObject().getIsPolicyHolders().add(false);
        }
      } else {
        List<WebInsClient> clients = this.getInsuranceMgr().getClients(this.getQuoteNo());

        for (WebInsClient client : clients) {
          // If a client is owner and driver, then the client is a policy holder.
          if (client.isOwner() && client.isDriver()) {
            this.getQuoteTempMemoryObject().getIsPolicyHolders().add(true);
          } else {
            this.getQuoteTempMemoryObject().getIsPolicyHolders().add(false);
          }
        }
      }
    }
  }

  /**
   * Go back to previous page.
   */
  public String back() {
    // Get questions from Xml.
    if (this.getPageId() == null) {
      this.setPageId(new Long(1));
    }

    this.setQuotePage(this.quoteQuestionManager.getPage(this.getPageId()));

    return SUCCESS;
  }

  /**
   * @return the quotePage
   */
  public QuotePage getQuotePage() {
    if (this.quotePage == null) {
      quotePage = this.quoteQuestionManager.getPage(this.getPageId());
    }
    return quotePage;
  }

  /**
   * @param quotePage the quotePage to set
   */
  public void setQuotePage(QuotePage quotePage) {
    this.quotePage = quotePage;
  }

  /**
   * Set a flag to indicate whether it is a existing quote or new quote.
   */
  public void setNewQuote(Boolean newQuote) {
    Map<String, Object> session = getSession();
    session.put(NEW_QUOTE, newQuote);
  }

  public Boolean isNewQuote() {
    Map<String, Object> session = getSession();
    return (Boolean) session.get(NEW_QUOTE);
  }

  public void setNewPolicyInSession(String newPolicy) {
    this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), "newPolicy", newPolicy);
  }

  public String getNewPolicyInSession() {
    return this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "newPolicy");
  }

  /**
   * Set QuoteTempMemoryObject bean.
   */
  public void setQuoteTempMemoryObject() {
    Map<String, Object> session = getSession();
    session.put("quoteTempMemoryObject", new QuoteTempMemoryObject());
  }

  public QuoteTempMemoryObject getQuoteTempMemoryObject() {
    Map<String, Object> session = getSession();
    return (QuoteTempMemoryObject) session.get("quoteTempMemoryObject");
  }

  /**
   * Temporally save WebInsClient object.
   * @param client
   */
  public void setClient(WebInsClient client) {
    Map<String, Object> session = getSession();
    session.put("client", client);
  }

  public WebInsClient getClient() {
    Map<String, Object> session = getSession();
    WebInsClient client = (WebInsClient) session.get("client");
    return client;
  }

  /**
   * Temporally save Quote Number.
   * @param client
   */
  public void setQuoteNo(Integer quoteNo) {
    Map<String, Object> session = getSession();
    session.put(QUOTE_NUMBER, quoteNo);
  }

  public Integer getQuoteNo() {
    Map<String, Object> session = getSession();
    Integer quoteNo = (Integer) session.get(QUOTE_NUMBER);

    return quoteNo;
  }

  /**
   * Get vehicle from RACT storage.
   */
  public GlVehicle getVehicle() throws Exception {
    WebInsQuote webInsQuote = this.getInsuranceMgr().getQuote(this.getQuoteNo());
    return this.glassesMgr.getVehicleCodes(getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.NVIC), webInsQuote.getQuoteDate());
  }

  // Temporally save GarageAddress bean.
  public void setGaragedAddress(GaragedAddress garagedAddress) {
    Map<String, Object> session = getSession();
    session.put("garagedAddress", garagedAddress);
  }

  public GaragedAddress getGaragedAddress() {
    Map<String, Object> session = getSession();
    return (GaragedAddress) session.get("garagedAddress");
  }

  /**
   * @return the pageId
   */
  public Long getPageId() {
    return pageId;
  }

  /**
   * @param pageId the pageId to set
   */
  public void setPageId(Long pageId) {
    this.pageId = pageId;
  }

  /**
   * @return the submit
   */
  public String getSubmit() {
    return this.getSubmitButton();
  }

  /**
   * @param submit the submit to set
   */
  public void setSubmit(String submit) {
    this.setSubmitButton(submit);
  }

  public void setMember(Boolean yes) {
    if (yes) {
      this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), "isMember", "true");
    } else {
      this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), "isMember", "false");
    }
  }

  // Check whether it is a member.
  public Boolean isMember() {
    String yes = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "isMember");

    if (yes.equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @return the showBackButton
   */
  public String getShowBackButton() {
    return showBackButton;
  }

  /**
   * @param showBackButton the showBackButton to set
   */
  public void setShowBackButton() {
    this.showBackButton = "true";
  }

  /**
   * Save client address based on address type.
   */
  public void saveClientAddress(WebInsClient client, String type, GaragedAddress address) throws Exception {
    // Save residential address.
    if (type.equals("residential")) {
      client.setResiPostcode(address.getPostCode());
      client.setResiStreet(address.getStreet());
      client.setResiSuburb(address.getSuburb());
      client.setResiState(address.getState());
      client.setResiStsubid(Integer.valueOf(address.getStreetSuburbId()));
      client.setResiStreetChar(address.getStreetNumber());

      client = this.getInsuranceService().attemptQasRiskAddress(this.getQuoteNo(), client);
      this.getInsuranceMgr().setClient(client);
      
    } else if (type.equals("postal")) { // Save postal address.
      client.setPostPostcode(address.getPostCode());
      client.setPostStreet(address.getStreet());
      client.setPostSuburb(address.getSuburb());
      client.setPostState(address.getState());
      client.setPostStsubid(Integer.valueOf(address.getStreetSuburbId()));
      client.setPostStreetChar(address.getStreetNumber());
      client.setPostProperty(address.getPoBox());
      this.getInsuranceMgr().setClient(client);
    }
  }

  /**
   * Save policy holders who are attached to the quote.
   */
    // TODO: check implementation
    public String savePolicyHolder(int i, String newPolicy, int quoteNo) throws Exception {
        String firstName = this.getParameter("firstName" + "_" + i);
        String surName = this.getParameter("surName" + "_" + i);

        this.initializeIsPolicyHolderArray();

        // To prevent blank fields for first name and surname.
        if (firstName == null || firstName.isEmpty() || surName == null || surName.isEmpty()) {
            // Validation message if first name and surname are empty.
            return "First name and Surname cannot be empty.";
        }

        WebInsClient client = null;
        List<WebInsClient> clients = this.getDrivers();

        if (i <= clients.size()) {
            // Existing clients.
            client = clients.get(i - 1);
        } else { 
            // Create new Clients.
            client = this.getInsuranceMgr().createInsuranceClient(quoteNo);
        }

        client.setGivenNames(firstName);
        client.setSurname(surName);
        client.setDriver(true);
        this.getInsuranceMgr().setClient(client);

        // Save DOB
        try {
            DateTime dateOfBirth = new DateTime(this.getParameter("additionalDob_" + i));
            client.setBirthDate(dateOfBirth);
        } catch (Exception e) {
            return getValidationMessage("Date of Birth", VALIDATE_DOB);
        }

        // Save year commenced driving.
        String yearCommencedDriving = this.getParameter("yearCommencedDriving" + "_" + i);

        if (client.getYearCommencedDriving() == null) {
            if (!this.validate(yearCommencedDriving, VALIDATE_INTEGER_STRING)) {
                return getValidationMessage("Year Commenced Driving", VALIDATE_INTEGER);
            } else {
                client.setYearCommencedDriving(Integer.valueOf(yearCommencedDriving));
            }

            this.getInsuranceMgr().setClient(client);
        }

        // Save gender.
        String gender = this.getParameter("gender" + "_" + i);
        if (gender.equals("Male")) {
            client.setGender(WebInsClient.GENDER_MALE);
        } else {
            client.setGender(WebInsClient.GENDER_FEMALE);
        }

        this.getInsuranceMgr().setClient(client);

        String policyHolder = "";
        if (newPolicy.equals("true")) {
            policyHolder = this.getParameter("policyHolder" + "_" + i);

            if (policyHolder != null && policyHolder.equals("yes")) {
                // Add new policy holder.
                if (i > this.getQuoteTempMemoryObject().getIsPolicyHolders().size()) {
                    this.getQuoteTempMemoryObject().getIsPolicyHolders().add(true);
                } else { // Set existing client to be a policy holder.
                    this.getQuoteTempMemoryObject().getIsPolicyHolders().set(i - 1, true);
                }

                // Save preferred contact person.
                String preferContact = this.getParameter("preferContactPoint" + "_" + i);

                if (preferContact.equals("yes")) {
                    client.setPreferredAddress(true);
                } else {
                    client.setPreferredAddress(false);
                }

                this.getInsuranceMgr().setClient(client);

                // Save title.
                String title = this.getParameter("title" + "_" + i);

                if (!this.validate(title, VALIDATE_STRING)) {
                    return this.getValidationMessage("Title", VALIDATE_STRING);
                }

                client.setTitle(title);
                this.getInsuranceMgr().setClient(client);

                // Save contact numbers.
                String homePhoneNumber = this.getParameter("homePhoneNumber" + "_" + i);
                if (homePhoneNumber != null && !homePhoneNumber.equals("")) {
                    if (!this.validate(homePhoneNumber, VALIDATE_INTEGER_STRING)) {
                        return this.getValidationMessage("Home Phone Number", VALIDATE_INTEGER_ONLY);
                    }

                    if (!this.validate(homePhoneNumber, VALIDATE_TELEPHONE)) {
                        return this.getValidationMessage("Home Phone Number", VALIDATE_TELEPHONE);
                    }
                }

                client.setHomePhone(homePhoneNumber);
                this.getInsuranceMgr().setClient(client);

                String workPhoneNumber = this.getParameter("workPhoneNumber" + "_" + i);
                if (workPhoneNumber != null && !workPhoneNumber.equals("")) {
                    if (!this.validate(workPhoneNumber, VALIDATE_INTEGER_STRING)) {
                        return this.getValidationMessage("Work Phone Number", VALIDATE_INTEGER_ONLY);
                    }

                    if (!this.validate(workPhoneNumber, VALIDATE_TELEPHONE)) {
                        return this.getValidationMessage("Work Phone Number", VALIDATE_TELEPHONE);
                    }
                }

                client.setWorkPhone(workPhoneNumber);
                this.getInsuranceMgr().setClient(client);

                String mobilePhoneNumber = this.getParameter("mobilePhoneNumber" + "_" + i);
                if (mobilePhoneNumber != null && !mobilePhoneNumber.equals("")) {
                    if (!this.validate(mobilePhoneNumber, VALIDATE_INTEGER_STRING)) {
                        return this.getValidationMessage("Mobile Phone Number", VALIDATE_INTEGER_ONLY);
                    }

                    if (!this.validate(mobilePhoneNumber, VALIDATE_TELEPHONE)) {
                        return this.getValidationMessage("Mobile Phone Number", VALIDATE_TELEPHONE);
                    }
                }

                client.setMobilePhone(mobilePhoneNumber);
                this.getInsuranceMgr().setClient(client);

                GaragedAddress garagedAddress = new GaragedAddress();
                this.setGaragedAddress(garagedAddress);

                // Residential Address
                String sameAsGaragedAddressQ = this.getParameter("sameAsGaragedAddress" + "_" + i);

                if (sameAsGaragedAddressQ.equals("yes")) {
                    GaragedAddress garagedAddressTemp = this.populateGaragedAddress(null, "garagedAddress");
                    this.saveClientAddress(client, "residential", garagedAddressTemp);
                } else {
                    String postCode = this.getParameter("residentialPostcode" + "_" + i);
                    if (postCode == null || postCode.equals("")) {
                        return this.getValidationMessage("Residential Post Code", VALIDATE_STRING);
                    }

                    this.getGaragedAddress().setPostCode(postCode);
                    client.setResiPostcode(postCode);
                    this.getInsuranceMgr().setClient(client);

                    // Save suburb.
                    String suburb = this.getParameter("residentialSuburb" + "_" + i);
                    if (suburb == null || suburb.equals("")) {
                        return this.getValidationMessage("Residential Suburb", VALIDATE_STRING);
                    }

                    this.getGaragedAddress().setSuburb(suburb);
                    client.setResiSuburb(suburb);
                    this.getInsuranceMgr().setClient(client);

                    // Save street Id.
                    String streetSuburbId = this.getParameter("residentialStreetSuburbId" + "_" + i);
                    if (streetSuburbId == null || streetSuburbId.equals("")) {
                        return this.getValidationMessage("Residential Street", VALIDATE_STRING);
                    }

                    client.setResiStsubid(Integer.valueOf(streetSuburbId));
                    this.getGaragedAddress().setStreetSuburbId(streetSuburbId);
                    this.getInsuranceMgr().setClient(client);

                    Collection<StreetSuburbVO> ssList = addressMgr.getStreetSuburbsBySuburb(suburb, postCode);

                    for (StreetSuburbVO streetSuburbVO : ssList) {
                        if (!streetSuburbVO.getStreet().equals("") && null != streetSuburbVO.getStreetSuburbID() && streetSuburbVO.getStreetSuburbID().equals(Integer.valueOf(streetSuburbId))) {
                            client.setResiStreet(streetSuburbVO.getStreet());
                            client.setResiState(streetSuburbVO.getState());
                            this.getInsuranceMgr().setClient(client);
                            break;
                        }
                    }

                    // Save street number
                    String streetNumber = this.getParameter("residentialStreetNumber" + "_" + i);
                    this.getGaragedAddress().setStreetNumber(streetNumber);

                    if (streetNumber == null || streetNumber.equals("")) {
                        return this.getValidationMessage("Residential Street Number", VALIDATE_STRING);
                    } else {
                        client.setResiStreetChar(streetNumber);
                        this.getInsuranceMgr().setClient(client);
                    }
                }

                // Postal Address
                String sameAsAboveQ = this.getParameter("sameAsAbove" + "_" + i);

                if (sameAsAboveQ.equals("yes")) {
                    GaragedAddress garagedAddressTemp = this.populateGaragedAddress(null, "garagedAddress");
                    this.saveClientAddress(client, "postal", garagedAddressTemp);
                } else {
                    // Save post code.
                    String postCode = this.getParameter("postcode" + "_" + i);
                    if (postCode == null || postCode.equals("")) {
                        return this.getValidationMessage("Post Code", VALIDATE_STRING);
                    }

                    this.getGaragedAddress().setPostCode(postCode);
                    client.setPostPostcode(postCode);
                    this.getInsuranceMgr().setClient(client);

                    // Save suburb.
                    String suburb = this.getParameter("suburb" + "_" + i);
                    if (suburb == null || suburb.equals("")) {
                        return this.getValidationMessage("Suburb", VALIDATE_STRING);
                    }

                    this.getGaragedAddress().setSuburb(suburb);
                    client.setPostSuburb(suburb);
                    this.getInsuranceMgr().setClient(client);

                    // Save po box and street number
                    String poBox = this.getParameter("pobox" + "_" + i);
                    this.getGaragedAddress().setPoBox(poBox);

                    String streetNumber = this.getParameter("streetNumber" + "_" + i);
                    this.getGaragedAddress().setStreetNumber(streetNumber);

                    String streetSuburbId = this.getParameter("streetSuburbId" + "_" + i);

                    if (poBox == null || poBox.equals("")) { // Save for street number.
                        client.setPostProperty("");
                        this.getInsuranceMgr().setClient(client);

                        if (streetNumber == null || streetNumber.equals("")) {
                            return "You must fill in either Street Name and Number or Post Box.";
                        }

                        if (streetSuburbId == null || streetSuburbId.equals("")) {
                            return "You must fill in either Street Name and Number or Post Box.";
                        }

                        // Save street Id.
                        client.setPostStsubid(Integer.valueOf(streetSuburbId));
                        this.getGaragedAddress().setStreetSuburbId(streetSuburbId);
                        this.getInsuranceMgr().setClient(client);

                        Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsBySuburb(suburb, postCode);

                        for (StreetSuburbVO streetSuburbVO : ssList) {
                            if (!streetSuburbVO.getStreet().equals("") && null != streetSuburbVO.getStreetSuburbID() && streetSuburbVO.getStreetSuburbID().equals(Integer.valueOf(streetSuburbId))) {
                                client.setPostStreet(streetSuburbVO.getStreet());
                                client.setPostState(streetSuburbVO.getState());
                                client.setPostStsubid(streetSuburbVO.getStreetSuburbID());
                                this.getInsuranceMgr().setClient(client);
                                break;
                            }
                        }

                        client.setPostStreetChar(streetNumber);
                        this.getInsuranceMgr().setClient(client);

                    } else {
                        // Save for po box.
                        client.setPostStreetChar("");
                        this.getInsuranceMgr().setClient(client);

                        Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsByPostcode(postCode);

                        for(StreetSuburbVO streetSuburbVO : ssList) {
                            if (suburb.equals(streetSuburbVO.getSuburb())) {
                                client.setPostStsubid(streetSuburbVO.getStreetSuburbID());
                            }
                        }

                        client.setPostProperty(poBox);
                        this.getInsuranceMgr().setClient(client);
                    }
                }

                // Save email address.
                String emailAddress = this.getParameter("emailAddress" + "_" + i);

                if (emailAddress != null && !emailAddress.isEmpty()) {
                    if (!this.validate(emailAddress, VALIDATE_EMAIL)) {
                        return this.getValidationMessage("Email", VALIDATE_EMAIL);
                    }
                    client.setEmailAddress(emailAddress);
                }

                this.getInsuranceMgr().setQuoteDetail(quoteNo, "policyHolder_" + i, policyHolder);

                this.getInsuranceMgr().setClient(client);
            } else {
                if (i > this.getQuoteTempMemoryObject().getIsPolicyHolders().size()) {
                    // The newly added client is not a policy holder.
                    this.getQuoteTempMemoryObject().getIsPolicyHolders().add(false);
                } else {
                    // Update the existing client to be not a policy holder.
                    this.getQuoteTempMemoryObject().getIsPolicyHolders().set(i - 1, false);
                }
            }
        }

        this.getInsuranceMgr().setClient(client);

        // Check whether license is suspended.
        String suspended = this.getParameter("licenseSuspended" + "_" + i);

        if (suspended == null || suspended.equals("")) {
            String question = this.getSubQuestionById("q23", "sub12").getTitle();
            return this.getValidationMessage("You must answer \" " + question + "\".", VALIDATE_CUSTOMIZED);
        }

        // If license is suspended, show message page.
        if (suspended.equals("yes")) {
            return String.valueOf(MESSAGE_SUSPENSION);
        } else {
            this.getInsuranceMgr().setQuoteDetail(quoteNo, "licenseSuspended_" + i, "no");
        }

        WebInsClientDetail detail = null;

        // Save number of accidents.
        String accidents = this.getParameter("accidents" + "_" + i);

        if (accidents == null || accidents.isEmpty()) {
            String question = this.getSubQuestionById("q23", "showAccident").getTitle();
            return this.getValidationMessage("You must answer \" " + question + "\".", VALIDATE_CUSTOMIZED);
        }

        this.getInsuranceMgr().setQuoteDetail(quoteNo, "hasAccidents_" + i, accidents);

        List<WebInsClientDetail> existingDetails = this.getInsuranceMgr().getClientDetails(client.getWebClientNo());

        // Record at most 5 recent accidents.
        int numberOfAccident = 5;
        for (int j = 1; j <= numberOfAccident; j++) {
            String accidentDateString = this.getParameter("accidentDate" + j + "_" + i);
            String accidentType = this.getParameter("accidentType" + j + "_" + i);

            if (accidentType != null && !accidentType.equals("") && accidentDateString != null && !accidentDateString.equals("")) {
                if (!this.validate(accidentDateString, VALIDATE_ACCIDENT_DATE)) {
                    return "Accident Date must be in format of (mm/yyyy)";
                }

                String[] accidentDate = accidentDateString.split("/");

                if (existingDetails != null && existingDetails.size() > 0) {
                    if (j <= existingDetails.size()) {
                        detail = existingDetails.get(j - 1);
                    }
                } else {
                    detail = new WebInsClientDetail(client.getWebClientNo(), accidentType, new Integer(accidentDate[0]), new Integer(accidentDate[1]), "accident");
                }

                detail.setWebClientNo(client.getWebClientNo());
                this.getInsuranceMgr().setClientDetail(detail);
                this.getInsuranceMgr().setClient(client);
            }
        }

        // Check whether had a cancelled insurance before.
        String insuranceCancelled = this.getParameter("insuranceCancelled" + "_" + i);

        if (insuranceCancelled == null || insuranceCancelled.isEmpty()) {
            String question = this.getSubQuestionById("q23", "sub14").getTitle();
            return this.getValidationMessage("You must answer \"" + question + "\".", VALIDATE_CUSTOMIZED);
        }

        // If had a cancelled insurance before, stop quote and display message page.
        if (insuranceCancelled.equals("yes")) {
            return String.valueOf(MESSAGE_CANCELLEDINSURANCE);
        } else {
            this.getInsuranceMgr().setQuoteDetail(quoteNo, "insuranceRefused_" + i, "no");
        }

        // New policy.
        if (newPolicy.equals("true")) {
            if (policyHolder != null && policyHolder.equals("yes")) {
                client.setOwner(true);
            } else { // Saved policy.
                client.setOwner(false);
            }
        }

        this.getInsuranceMgr().setClient(client);

        if (this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS).equals(WebInsQuoteDetail.TRUE)) {
            // check driver is over 50 if user has answered 'yes' to 50+ question
            if (!isOlderThan(client.getBirthDate(), 50)) {
                return String.valueOf(MESSAGE_FIFTYPLUS);
            }
        }

        return PASSED;
    }

  // Set question answers based on question type.
  // The quoteQuestion bean is the question and answers container, which is passed into the view for display.
  public void loadQuestion(QuoteQuestion quoteQuestion, String value) {
    quoteQuestion.setDisplay(true);

    if (quoteQuestion.getType() == QuestionType.RADIO) {
      for (Text text : quoteQuestion.getTexts()) {
        if (text.getValue().equals(value)) {
          text.setAnswer(text.getValue());
        } else {
          text.setAnswer("");
        }
      }
    } else if (quoteQuestion.getType() == QuestionType.TEXT) {
      for (Text text : quoteQuestion.getTexts()) {
        if (value != null) {
          text.setAnswer(value.trim());
        } else {
          text.setAnswer("");
        }
      }
    } else if (quoteQuestion.getType() == QuestionType.SELECT) {
      for (Option option : quoteQuestion.getOptions()) {
        if (option.getValue().equals(value)) {
          option.setSelected(true);
        }
      }
    } else if (quoteQuestion.getType() == QuestionType.CHECK_BOX) {
      for (Text text : quoteQuestion.getTexts()) {
        if (text.getValue().equals(value)) {
          text.setAnswer("true");
        }
      }
    } else if (quoteQuestion.getType() == QuestionType.TEXT_SHOW) {
      for (Text text : quoteQuestion.getTexts()) {
        text.setAnswer(value);
      }
    }
  }

  /**
   * Get all associated clients for a policy holder.
   */
  public WebInsClient getClientsBySelectedPolicyHolder(String selectionDescription) throws Exception {
    LogUtil.warn(this.getClass(), "Asked to load client with selectionDescription: " + selectionDescription);
    List<WebInsClient> possibleClients = getPossibleClientAssociations(this.getQuoteNo());
    
    for(WebInsClient client : possibleClients) {
        if (client.getSelectionDescription().equals(selectionDescription)) {
          return client;
        }
    }
    
    LogUtil.warn(this.getClass(), "Failed to match selectionDescription in possible clients");
    return null;
  }

    /**
     * Check whether a selected client is already in the quote.
     */
    public boolean checkSelectedClient(WebInsClient selectedClient) throws Exception {
        List<WebInsClient> clients = this.getInsuranceMgr().getClients(this.getQuoteNo());
        for (WebInsClient c : clients) {
            if (selectedClient.getSelectionDescription().equals(c.getSelectionDescription())) {
                return true;
            }
        }
        return false;
    }

  /**
   * Duplicate quote questions for each policy holder.
   * All policy holders will be displayed in one page, and the insurance_quote_question only holds one copy of the questions.
   * In order to display the same list of questions with different answers for each policy holder, this method duplicate the
   * questions, but fills in different answers.
   * Index is the position number of each policy holder in the list. The first policy holder has index 1 and the second policy holder has index 2.
   */
  public List<QuoteQuestion> duplicatePolicyHolders(int index) throws Exception {
    QuoteQuestion quoteQuestion = this.quoteQuestionManager.getQuestionByPageAndId(13, "q23");
    List<QuoteQuestion> quoteQuestionCopy = new ArrayList<QuoteQuestion>();

    for (QuoteQuestion quoteSubQuestion : quoteQuestion.getSubQuestions()) {
      // Duplicate the prefer contact question.
      if (quoteSubQuestion.getId().equals("sub5")) {
        for (Text text : quoteSubQuestion.getTexts()) {
          for (Event event : text.getEvents()) {
            if (text.getValue().equals("yes")) {
              event.setFunction("showQuestions('preferContact" + "_" + index + ",sub6" + "_" + index + ",sub7" + "_" + index + ",sub8" + "_" + index + ",sub9" + "_" + index + ",sub11" + "_" + index + ",sameAsGaragedAddressQ_" + index + ",sameAsAboveQ_" + index + ",residentialAddress_" + index + ",postalAddress_" + index + "','show')");
            } else {
              event.setFunction("showQuestions('preferContact" + "_" + index + ",sub6" + "_" + index + ",sub7" + "_" + index + ",sub8" + "_" + index + ",sub9" + "_" + index + ",sub11" + "_" + index + ",sameAsGaragedAddressQ_" + index + ",sameAsAboveQ_" + index + ",residentialAddress_" + index + ",residentialSubstreetNumber_" + index + ",residentialSubStreet_" + index + ",residentialSubsuburb_" + index + ",residentialSubPostCode_" + index + ",sameAsGaragedAddressQ_" + index + ",substreetNumber_" + index + ",subpobox_" + index + ",subStreet_" + index + ",subsuburb_" + index + ",subPostCode_" + index + ",sameAsAboveQ_" + index + ",postalAddress_" + index + "','hidden')");
            }
          }
        }
      }

      // Duplicate same as garaged address question.
      if (quoteSubQuestion.getId().equals("sameAsGaragedAddressQ")) {
        for (Text text : quoteSubQuestion.getTexts()) {
          for (Event event : text.getEvents()) {
            if (text.getValue().equals("yes")) {
              event.setFunction("showQuestions('residentialSubPostCode" + "_" + index + ",residentialSubsuburb" + "_" + index + ",residentialSubStreet" + "_" + index + ",residentialSubstreetNumber" + "_" + index + "','hidden')");
            } else {
              event.setFunction("showQuestions('residentialSubPostCode" + "_" + index + ",residentialSubsuburb" + "_" + index + ",residentialSubStreet" + "_" + index + ",residentialSubstreetNumber" + "_" + index + "','show')");
            }
          }
        }
      }

      // Duplicate same as above question.
      if (quoteSubQuestion.getId().equals("sameAsAboveQ")) {
        for (Text text : quoteSubQuestion.getTexts()) {
          for (Event event : text.getEvents()) {
            if (text.getValue().equals("yes")) {
              event.setFunction("showQuestions('subPostCode" + "_" + index + ",subsuburb" + "_" + index + ",subStreet" + "_" + index + ",substreetNumber" + "_" + index + ",subpobox" + "_" + index + "','hidden')");
            } else {
              event.setFunction("showQuestions('subPostCode" + "_" + index + ",subsuburb" + "_" + index + ",subStreet" + "_" + index + ",substreetNumber" + "_" + index + ",subpobox" + "_" + index + "','show')");
            }
          }
        }
      }

      // Duplicate accidents question.
      if (quoteSubQuestion.getId().equals("showAccident")) {
        for (Text text : quoteSubQuestion.getTexts()) {
          for (Event event : text.getEvents()) {
            if (text.getValue().equals("yes")) {
              event.setFunction("showQuestions('sub13_" + index + ",accidentT1_" + index + ",accidentD1_" + index + ",accidentT2_" + index + ",accidentD2_" + index + ",accidentT3_" + index + ",accidentD3_" + index + ",accidentT4_" + index + ",accidentD4_" + index + ",accidentT5_" + index + ",accidentD5_" + index + "','show')");
            } else {
              event.setFunction("showQuestions('sub13_" + index + ",accidentT1_" + index + ",accidentD1_" + index + ",accidentT2_" + index + ",accidentD2_" + index + ",accidentT3_" + index + ",accidentD3_" + index + ",accidentT4_" + index + ",accidentD4_" + index + ",accidentT5_" + index + ",accidentD5_" + index + "','hidden')");
            }
          }
        }
      }

      // Duplicate Residential address.
      if (quoteSubQuestion.getId().equals("residentialSubsuburb")) {
        quoteSubQuestion.getEvent().setFunction("getStreetList('QuoteAjaxAction_streetList.action', 'residentialStreetSuburbId" + "_" + index + "', '13', 'q23', 'residentialSubStreet', 'residentialSuburb" + "_" + index + "', 'residentialPostcode" + "_" + index + "')");
        quoteSubQuestion.getLink().getEvents().get(0).setFunction("getSuburbList('QuoteAjaxAction_suburbList.action','residentialPostcode" + "_" + index + "', '13', 'q23', 'residentialSubPostCode', 'residentialSuburb" + "_" + index + "','no')");
        quoteSubQuestion.getLink().setId(quoteSubQuestion.getLink().getId() + "_" + index);
      }

      if (quoteSubQuestion.getId().equals("residentialSubStreet")) {
        quoteSubQuestion.getLink().getEvents().get(0).setFunction("getStreetList('QuoteAjaxAction_streetList.action', 'residentialStreetSuburbId" + "_" + index + "', '13', 'q23', 'residentialSubStreet', 'residentialSuburb" + "_" + index + "', 'residentialPostcode" + "_" + index + "')");
        quoteSubQuestion.getLink().setId(quoteSubQuestion.getLink().getId() + "_" + index);
      }

      if (quoteSubQuestion.getId().equals("residentialSubPostCode")) {
        for (Text text : quoteSubQuestion.getTexts()) {
          text.setName(text.getName() + "_" + index);
          for (Event event : text.getEvents()) {
            event.setFunction("getSuburbList('QuoteAjaxAction_suburbList.action','residentialPostcode" + "_" + index + "', '13', 'q23', 'residentialSubPostCode', 'residentialSuburb" + "_" + index + "','no')");
          }
        }
      } else {
        if (quoteSubQuestion.getTexts() != null) {
          for (Text text : quoteSubQuestion.getTexts()) {
            text.setName(text.getName() + "_" + index);
          }
        }
      }

      // Duplicate Post address.
      if (quoteSubQuestion.getId().equals("subsuburb")) {
        quoteSubQuestion.getEvent().setFunction("getStreetList('QuoteAjaxAction_streetList.action', 'streetSuburbId" + "_" + index + "', '13', 'q23', 'subStreet', 'suburb" + "_" + index + "', 'postcode" + "_" + index + "')");
        quoteSubQuestion.getLink().getEvents().get(0).setFunction("getSuburbList('QuoteAjaxAction_suburbList.action','postcode" + "_" + index + "', '13', 'q23', 'subPostCode', 'suburb" + "_" + index + "','no')");
        quoteSubQuestion.getLink().setId(quoteSubQuestion.getLink().getId() + "_" + index);
      }

      if (quoteSubQuestion.getId().equals("subStreet")) {
        quoteSubQuestion.getLink().getEvents().get(0).setFunction("getStreetList('QuoteAjaxAction_streetList.action', 'streetSuburbId" + "_" + index + "', '13', 'q23', 'subStreet', 'suburb" + "_" + index + "', 'postcode" + "_" + index + "')");
        quoteSubQuestion.getLink().setId(quoteSubQuestion.getLink().getId() + "_" + index);
      }

      if (quoteSubQuestion.getId().equals("subPostCode")) {
        for (Text text : quoteSubQuestion.getTexts()) {
          for (Event event : text.getEvents()) {
            event.setFunction("getSuburbList('QuoteAjaxAction_suburbList.action','postcode" + "_" + index + "', '13', 'q23', 'subPostCode', 'suburb" + "_" + index + "','no')");
          }
        }
      }

      quoteSubQuestion.setName(quoteSubQuestion.getName() + "_" + index);
      quoteSubQuestion.setId(quoteSubQuestion.getId() + "_" + index);
      quoteQuestionCopy.add(quoteSubQuestion);
    }

    return quoteQuestionCopy;
  }

  /**
   * Bind client residential, postal and garage address to GaragedAddress bean. 
   */
  public GaragedAddress populateGaragedAddress(WebInsClient client, String type) throws Exception {
    GaragedAddress garagedAddress = new GaragedAddress();

    if (type.equals("residential")) { // Assign residential address.
      garagedAddress.setPostCode(client.getResiPostcode());
      garagedAddress.setState(client.getResiState());

      garagedAddress.setStreet(client.getResiStreet());
      garagedAddress.setStreetNumber(client.getResiStreetChar());
      garagedAddress.setStreetSuburbId(String.valueOf(client.getResiStsubid()));
      garagedAddress.setSuburb(client.getResiSuburb());
      garagedAddress.setLatitude(client.getResiLatitude());
      garagedAddress.setLongitude(client.getResiLongitude());
      garagedAddress.setGnaf(client.getResiGnaf());

      return garagedAddress;

    } else if (type.equals("postal")) { //Assign postal address.
      garagedAddress.setPostCode(client.getPostPostcode());
      garagedAddress.setState(client.getPostState());
      garagedAddress.setPoBox(client.getPostProperty());

      garagedAddress.setStreet(client.getPostStreet());
      garagedAddress.setStreetNumber(client.getPostStreetChar());

      if (client.getPostProperty() == null || client.getPostProperty().isEmpty()) {
        garagedAddress.setStreetSuburbId(String.valueOf(client.getPostStsubid()));
      }

      garagedAddress.setSuburb(client.getPostSuburb());

      return garagedAddress;
    } else { // Assign garage address.
      String streetNumber = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_NO);
      String streetSuburbId = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_ID);
      String street = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_STREET);
      String suburb = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_SUBURB);
      String postcode = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_POSTCODE);
      String latitude = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_LATITUDE);
      String longitude = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_LONGITUDE);
      String gnaf = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_GNAF);

      if (streetNumber != null && !streetNumber.isEmpty() && streetSuburbId != null && !streetSuburbId.isEmpty()) {
        
        garagedAddress = new GaragedAddress();

        //garagedAddress.setState(streetSuburb.getState());
        garagedAddress.setPostCode(postcode);
        garagedAddress.setSuburb(suburb);
        garagedAddress.setStreet(street);
        garagedAddress.setLatitude(latitude);
        garagedAddress.setLongitude(longitude);
        garagedAddress.setGnaf(gnaf);
        garagedAddress.setStreetNumber(streetNumber);
        garagedAddress.setStreetSuburbId(streetSuburbId);

        return garagedAddress;
      }
    }

    return null;
  }

  /**
   * Load saved data for each policy holder in policy holder page.
   * @param client Policy holders associated with
   * @param duplicatedSubQuestions a set of question for the policy holder.
   * @param i  index number for the policy holder. policy holder 1 has index 1.
   * @param newPolicy whether it is a new policy or existing policy.
   * @return
   */
  public List<QuoteQuestion> loadPolicyHolderData(WebInsClient client, List<QuoteQuestion> duplicatedSubQuestions, int i, String newPolicy) throws Exception {
    this.initializeIsPolicyHolderArray();
    
    // Load first name and surname.
    this.loadQuestion(duplicatedSubQuestions.get(0), client.getGivenNames());
    this.loadQuestion(duplicatedSubQuestions.get(1), client.getSurname());
    this.loadQuestion(duplicatedSubQuestions.get(2), (client.getBirthDate() != null ? client.getBirthDate().toString() : ""));

    // Load year commenced driving.
    if (client.getYearCommencedDriving() != null) {
      duplicatedSubQuestions.get(3).setType(QuestionType.TEXT_SHOW);
      duplicatedSubQuestions.get(3).setTypeString("textShow");
      this.loadQuestion(duplicatedSubQuestions.get(3), "" + client.getYearCommencedDriving());
    } else {
      this.loadQuestion(duplicatedSubQuestions.get(3), "");
    }

    // Load gender.
    if (client.getGender() != null) {
      this.loadQuestion(duplicatedSubQuestions.get(4), client.getGender().trim().toString());
    } else {
      this.loadQuestion(duplicatedSubQuestions.get(4), "");
    }
    
    if (newPolicy.equals("false")) {
      duplicatedSubQuestions.get(5).setDisplay(false);
      this.loadTitles(duplicatedSubQuestions.get(7), null);
    } else {
      // Has policy holders to display.
      if (this.getQuoteTempMemoryObject().getIsPolicyHolders().size() > 0 && this.getQuoteTempMemoryObject().getIsPolicyHolders().get(i - 1)) {
        // Load questions for policy holders.
        this.loadQuestion(duplicatedSubQuestions.get(5), "yes");

        // Load prefer contact.
        if (client.isPreferredAddress()) {
          this.loadQuestion(duplicatedSubQuestions.get(6), "yes");
        } else {
          this.loadQuestion(duplicatedSubQuestions.get(6), "no");
        }

        // Load title, contact numbers.
        //this.loadQuestion(duplicatedSubQuestions.get(7), client.getTitle());
        this.loadTitles(duplicatedSubQuestions.get(7), client.getTitle());
        this.loadQuestion(duplicatedSubQuestions.get(8), client.getHomePhone());
        this.loadQuestion(duplicatedSubQuestions.get(9), client.getWorkPhone());
        this.loadQuestion(duplicatedSubQuestions.get(10), client.getMobilePhone());

        GaragedAddress garagedAddress = null;

        // Load residential address.
        duplicatedSubQuestions.get(11).setDisplay(true);
        if (client.getResiPostcode() != null && !client.getResiPostcode().isEmpty()) {
          garagedAddress = this.populateGaragedAddress(client, "residential");
          this.loadQuestion(duplicatedSubQuestions.get(12), "no");
        } else {
          garagedAddress = this.populateGaragedAddress(null, "garaged");
          this.loadQuestion(duplicatedSubQuestions.get(12), "yes");
        }

        this.loadStreetSuburb(garagedAddress, duplicatedSubQuestions.get(13), duplicatedSubQuestions.get(14), duplicatedSubQuestions.get(15), duplicatedSubQuestions.get(16), null);

        // Load post address.
        duplicatedSubQuestions.get(17).setDisplay(true);
        if (client.getPostPostcode() != null && !client.getPostPostcode().isEmpty()) {
          garagedAddress = this.populateGaragedAddress(client, "postal");
          this.loadQuestion(duplicatedSubQuestions.get(18), "no");
        } else { // Load default garaged address.
          garagedAddress = this.populateGaragedAddress(null, "garaged");
          this.loadQuestion(duplicatedSubQuestions.get(18), "yes");
        }

        this.loadStreetSuburb(garagedAddress, duplicatedSubQuestions.get(19), duplicatedSubQuestions.get(20), duplicatedSubQuestions.get(21), duplicatedSubQuestions.get(23), duplicatedSubQuestions.get(22));

        this.loadQuestion(duplicatedSubQuestions.get(24), client.getEmailAddress());

      } else {
        // No policy holders to display.

        this.loadQuestion(duplicatedSubQuestions.get(6), "no");
        this.loadTitles(duplicatedSubQuestions.get(7), null);
        this.loadQuestion(duplicatedSubQuestions.get(12), "yes");
        this.loadQuestion(duplicatedSubQuestions.get(18), "yes");
        duplicatedSubQuestions.get(12).setDisplay(false);
        duplicatedSubQuestions.get(18).setDisplay(false);
      }
    }
    
    this.loadQuestion(duplicatedSubQuestions.get(5), this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "policyHolder_" + i));
    
    this.loadQuestion(duplicatedSubQuestions.get(25), this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "licenseSuspended_" + i));
    this.loadQuestion(duplicatedSubQuestions.get(26), this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "hasAccidents_" + i));
    this.loadQuestion(duplicatedSubQuestions.get(38), this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "insuranceRefused_" + i));

    List<WebInsClientDetail> clientDetails = this.getInsuranceMgr().getClientDetails(client.getWebClientNo());

    // Load accident question.
    int detailsIndex = 1;
    for (WebInsClientDetail clientDetail : clientDetails) {
      if (clientDetail.getDetail().equals("accident")) {
        this.loadQuestion(duplicatedSubQuestions.get(26), "yes");
        switch (detailsIndex) {
          case 1:
            this.loadQuestion(duplicatedSubQuestions.get(28), clientDetail.getDetailType());
            this.loadQuestion(duplicatedSubQuestions.get(29), clientDetail.getDetMonth() + "/" + clientDetail.getDetYear());
            break;
          case 2:
            this.loadQuestion(duplicatedSubQuestions.get(30), clientDetail.getDetailType());
            this.loadQuestion(duplicatedSubQuestions.get(31), clientDetail.getDetMonth() + "/" + clientDetail.getDetYear());
            break;
          case 3:
            this.loadQuestion(duplicatedSubQuestions.get(32), clientDetail.getDetailType());
            this.loadQuestion(duplicatedSubQuestions.get(33), clientDetail.getDetMonth() + "/" + clientDetail.getDetYear());
            break;
          case 4:
            this.loadQuestion(duplicatedSubQuestions.get(34), clientDetail.getDetailType());
            this.loadQuestion(duplicatedSubQuestions.get(35), clientDetail.getDetMonth() + "/" + clientDetail.getDetYear());
            break;
          case 5:
            this.loadQuestion(duplicatedSubQuestions.get(36), clientDetail.getDetailType());
            this.loadQuestion(duplicatedSubQuestions.get(37), clientDetail.getDetMonth() + "/" + clientDetail.getDetYear());
            break;
        }

        detailsIndex++;
      }

      if (clientDetail.getDetailType().equals(WebInsClient.CLIENT_DETAIL_SUSPENSION)) {
        this.loadQuestion(duplicatedSubQuestions.get(25), clientDetail.getDetail());
      } else {
        this.loadQuestion(duplicatedSubQuestions.get(25), "no");
      }
    }

    return duplicatedSubQuestions;
  }

  /**
   * Load policy holder page based on user's selection.
   * The selection is indicated by parameter 'index'.
   *
   */
  public void loadPolicyHolders(String newPolicy, String index, boolean skip) throws Exception {
    int quoteNo = this.getQuoteNo();
    this.setShowBenefits(true);
    // Decided whether to skip creating a new client to hold the selected client.
    if (!skip) {
        // Clear Owner / Preferred Address for existing clients
        List<WebInsClient> clients = this.getInsuranceMgr().getClients(this.getQuoteNo());
        if (null != clients) {
            for (WebInsClient client : clients) {
                client.setOwner(false);
                client.setPreferredAddress(false);
                try {
                    this.getInsuranceMgr().setClient(client);
                } catch (Exception e) {
                    Config.logger.error(Config.key + ": unable to update WebInsClient details for Client #" + client.getWebClientNo(), e);
                }
            }
        }
        
      // Decide whether it is existing or new policy holders.
      if (newPolicy.equals("false")) {
        WebInsClient selectedClient = this.getClientsBySelectedPolicyHolder(index);
        
        // Create a new client to hold the selected client which is not in the quote yet.
        if (!checkSelectedClient(selectedClient)) {
          WebInsClient quoteClient = this.getInsuranceMgr().createInsuranceClient(quoteNo);
          selectedClient.createShallowCopy(quoteClient);
          quoteClient.setWebQuoteNo(this.getQuoteNo());
          quoteClient.setOwner(true);
          quoteClient.setPreferredAddress(true);

          this.getInsuranceMgr().setClient(quoteClient);

        } else { // The client is already in the quote, just set the client as the owner and prefer contact.
          selectedClient.setPreferredAddress(true);
          selectedClient.setOwner(true);
          if(null != selectedClient.getWebClientNo()) {
              this.getInsuranceMgr().setClient(selectedClient);
          }
        }
      }
    }

    // Get all saved drivers.
    List<WebInsClient> clients = this.getDrivers();
    List<QuoteQuestion> subQuestions = new ArrayList<QuoteQuestion>();

    int i = 1;
    // Duplicate questions for each driver.
    for (WebInsClient client : clients) {
      List<QuoteQuestion> duplicatedSubQuestions = duplicatePolicyHolders(i);
      subQuestions.addAll(this.loadPolicyHolderData(client, duplicatedSubQuestions, i, newPolicy));
      i++;
    }

    // Add these duplicated into an array list, which will be used in view page.
    this.getQuestionById("q23").getSubQuestions().clear();
    this.getQuestionById("q23").getSubQuestions().addAll(subQuestions);

    // No drivers are available to edit.
    if (subQuestions.isEmpty()) {
      String description = "No Drivers are available to edit.";
      List<String> descriptions = new ArrayList<String>();
      descriptions.add(description);
      this.getQuotePage().setDescription(descriptions);
    }
  }

    public WebInsClient getAdditionalPolicyHolder() throws Exception {
        int quoteNo = this.getQuoteNo();
        List<WebInsClient> policyHolders = this.getInsuranceMgr().getClients(quoteNo);

        for (WebInsClient policyHolder : policyHolders) {
            if (this.isMember()) {
                if (policyHolder.isOwner() && !policyHolder.isDriver()) {
                    if (this.getClient() != null && !this.getClient().getSelectionDescription().equals(policyHolder.getSelectionDescription())) {
                        return policyHolder;
                    }
                }
            } else {
                if (policyHolder.isOwner() && !policyHolder.isDriver()) {
                    return policyHolder;
                }
            }
        }
        return null;
    }

  /**
   * Get the policy holder who is the primary contact in the quote.
   */
  public WebInsClient getPrimaryContactPolciyHolder() throws Exception {
    int quoteNo = this.getQuoteNo();
    List<WebInsClient> policyHolders = this.getInsuranceMgr().getClients(quoteNo);

    for (WebInsClient policyHolder : policyHolders) {
      if (policyHolder.isOwner() && policyHolder.isPreferredAddress()) {
        return policyHolder;
      }
    }

    return null;
  }

  /**
   * Load page to display the primary contact policy holder in the quote.
   * If user has defined a primary contact policy holder, this policy holder will be loaded.
   * Otherwise, the first policy holder in the quote will be loaded as the primary contact.
   */
  public void loadFirstContactPolicyHolder() throws Exception {
    this.setShowBenefits(true);
    // Get the first policy holder.
    // The method will return the primary contact policy holder or the first policy holder.
    WebInsClient firstPolicyHolder = this.getFirstPolicyHolder();
    String emailAddress = firstPolicyHolder.getEmailAddress();

    GaragedAddress garagedAddress = null;

    // Get saved address.
    // If residential not null, load it. Otherwise, load the saved garage address as the default address.
    if (firstPolicyHolder.getResiPostcode() != null && !firstPolicyHolder.getResiPostcode().isEmpty()) {
      garagedAddress = this.populateGaragedAddress(firstPolicyHolder, "residential");
    } else {
      garagedAddress = this.populateGaragedAddress(null, "garaged");
    }

    // Load residential address.
    this.loadStreetSuburb(garagedAddress, this.getSubQuestionById("q24", "residentialSubPostCode"), this.getSubQuestionById("q24", "residentialSubsuburb"), this.getSubQuestionById("q24", "residentialSubStreet"), this.getSubQuestionById("q24", "residentialSubstreetNumber"), null);

    // Get post address.
    if (firstPolicyHolder.getPostPostcode() != null && !firstPolicyHolder.getPostPostcode().isEmpty()) {
      garagedAddress = this.populateGaragedAddress(firstPolicyHolder, "postal");
    } else { // Load default garaged address.
      garagedAddress = this.populateGaragedAddress(null, "garaged");
    }

    // Load postal address.
    this.loadStreetSuburb(garagedAddress, this.getSubQuestionById("q24", "subPostCode"), this.getSubQuestionById("q24", "subsuburb"), this.getSubQuestionById("q24", "subStreet"), this.getSubQuestionById("q24", "substreetNumber"), this.getSubQuestionById("q24", "subpobox"));

    String holderName = "";
    List<WebInsClient> policyHolders = this.getInsuranceMgr().getClients(this.getQuoteNo());

    int index = 0;

    // Append all policy holder names into a long string.
    for (WebInsClient policyHolder : policyHolders) {
      if (policyHolder.isOwner()) {
        if (index == 0) {
          holderName = policyHolder.getSelectionDescription();
        } else {
          holderName = holderName + " &amp; " + policyHolder.getSelectionDescription();
        }

        index++;
      }
    }

    if (holderName.isEmpty()) {
      holderName = policyHolders.get(0).getSelectionDescription();
    }

    // Set holder names and email address in view page.
    this.loadQuestion(this.getQuestionById("q24"), holderName);
    
    // email only mandatory if not Agent
    this.getSubQuestionById("q24", "sub2").setMandatory(this.getPersistedAgentUser() == null || this.getPersistedAgentUser().isEmpty());
    this.loadQuestion(this.getSubQuestionById("q24", "sub2"), emailAddress);
  }

  /**
   * Load page 8, which is a summary page before premium is calculated.
   */
  public void loadPage8() throws Exception {

    int quoteNo = this.getQuoteNo();
    VehicleInfo vehicleInfo = null;
    String startDate = "";
    WebInsClient driver1 = null;
    WebInsClient driver2 = null;
    WebInsClient ratingDriver;
    String noClaimDiscount = "";

    // Load vehicle.
    GlVehicle vehicle = this.getVehicle();
    vehicleInfo = new VehicleInfo();

    List<ListPair> makes = this.glassesMgr.getMakes(vehicle);

    for (ListPair tempMake : makes) {
      if (vehicle.getMake().equals(tempMake.getCol2())) {
        vehicleInfo.setMake(tempMake.getCol1());
        break;
      }
    }

    vehicleInfo.setYear(String.valueOf(vehicle.getVehYear()));
    vehicleInfo.setModel(vehicle.getModel());

    // Load body type.
    List<ListPair> bodyTypes = this.glassesMgr.getBodyTypes(vehicle);

    for (ListPair tempBodyType : bodyTypes) {
      if (vehicle.getBodyType().equals(tempBodyType.getCol2())) {
        vehicleInfo.setBodyType(tempBodyType.getCol1());
        break;
      }
    }

    // Load transmission.
    List<ListPair> transmissions = this.glassesMgr.getTransmissionTypes(vehicle);

    for (ListPair tempTransmission : transmissions) {
      if (vehicle.getTransmission().equals(tempTransmission.getCol2())) {
        vehicleInfo.setTransmission(tempTransmission.getCol1());
        break;
      }
    }

    // Load nvic
    List<GlVehicle> vehicles = glassesMgr.getList(vehicle, new DateTime("1/1/" + vehicle.getVehYear()));

    for (GlVehicle tempVehicle : vehicles) {
      if (tempVehicle.getNvic().equals(vehicle.getNvic())) {
        vehicleInfo.setSummary(tempVehicle.getSummary());
        vehicleInfo.setVariant(tempVehicle.getVariant());
        break;
      }
    }

    vehicleInfo.setUsage(getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.USEAGE));
    vehicleInfo.setGaragedAddress(getInsuranceMgr().getQuoteDetail(quoteNo, "garagedAddress"));
    noClaimDiscount = getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.NO_CLAIM_DISCOUNT);

    WebInsQuote webInsQuote = getInsuranceMgr().getQuote(quoteNo);
    startDate = webInsQuote.getQuoteDate().formatShortDate();

    // Set Rating driver.
    ratingDriver = getInsuranceMgr().getRatingDriver(quoteNo);

    // Get drivers.
    List<WebInsClient> drivers = this.getDrivers();
    driver1 = drivers.get(0);

    if (drivers.size() > 1) {
      driver2 = drivers.get(1);
      this.setHasDriver2("true");
    } else {
      this.setHasDriver2("false");
    }

    // Pass loaded info to view page.
    this.getHttpRequest().setAttribute("vehicleInfo", vehicleInfo);
    this.getHttpRequest().setAttribute("driver1", driver1);
    this.getHttpRequest().setAttribute("driver2", driver2);
    this.getHttpRequest().setAttribute("ratingDriver", ratingDriver);
    this.getHttpRequest().setAttribute("noClaimDiscount", noClaimDiscount);
    this.getHttpRequest().setAttribute("startDate", startDate);
  }

  /**
   * Load the primary contact policy holder. If null, return the first policy holder in the quote.
   */
  public WebInsClient getFirstPolicyHolder() throws Exception {
    int quoteNo = this.getQuoteNo();

    List<WebInsClient> policyHolders = this.getInsuranceMgr().getClients(quoteNo);
    WebInsClient firstPolicyHolder = null;

    WebInsClient primaryContact = this.getPrimaryContactPolciyHolder();

    if (primaryContact == null) {
      for (WebInsClient policyHolder : policyHolders) {
        if (policyHolder.isOwner()) {
          firstPolicyHolder = policyHolder;
          break;
        }
      }
      if (firstPolicyHolder == null) {
        firstPolicyHolder = policyHolders.get(0);
      }

    } else {
      firstPolicyHolder = primaryContact;
    }

    return firstPolicyHolder;
  }

  public String savePolicyHolderContact(String emailAddress) throws Exception {
    WebInsClient firstPolicyHolder = this.getFirstPolicyHolder();

    firstPolicyHolder.setPreferredAddress(true);
    firstPolicyHolder.setOwner(true);

    if (emailAddress != null && !emailAddress.isEmpty()) {
      firstPolicyHolder.setEmailAddress(emailAddress);
      this.getInsuranceMgr().setClient(firstPolicyHolder);
    }

    String postCode = this.getParameter("residentialPostcode");
    if (postCode != null && !postCode.isEmpty()) {
      firstPolicyHolder.setResiPostcode(postCode);
    }

    String suburb = this.getParameter("residentialSuburb");
    if (suburb != null && !suburb.isEmpty()) {
      firstPolicyHolder.setResiSuburb(suburb);
    }

    String streetSuburbId = this.getParameter("residentialStreetSuburbId");
    if (streetSuburbId != null && !streetSuburbId.isEmpty()) {
      firstPolicyHolder.setResiStsubid(Integer.valueOf(streetSuburbId));
    }

    if (suburb != null && !suburb.isEmpty() && postCode != null && !postCode.isEmpty()) {
    	Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsBySuburb(suburb, postCode);

      for (StreetSuburbVO streetSuburbVO : ssList) {        
        if (!streetSuburbVO.getStreet().equals("") && String.valueOf(streetSuburbVO.getStreetSuburbID()).equals(streetSuburbId)) {
          firstPolicyHolder.setResiStreet(streetSuburbVO.getStreet());
          firstPolicyHolder.setResiState(streetSuburbVO.getState());
          this.getInsuranceMgr().setClient(firstPolicyHolder);
          break;
        }
      }
    }

    String streetNumber = this.getParameter("residentialStreetNumber");

    if (streetNumber != null && !streetNumber.equals("")) {
      firstPolicyHolder.setResiStreetChar(streetNumber);
      this.getInsuranceMgr().setClient(firstPolicyHolder);
    }

    postCode = this.getParameter("postcode");
    if (postCode != null && !postCode.isEmpty()) {
      firstPolicyHolder.setPostPostcode(postCode);
    }

    suburb = this.getParameter("suburb");
    if (suburb != null && !suburb.isEmpty()) {
      firstPolicyHolder.setPostSuburb(suburb);
    }

    String poBox = this.getParameter("pobox");

    streetNumber = this.getParameter("streetNumber");

    if (poBox != null && !poBox.equals("")) {
      firstPolicyHolder.setPostProperty(poBox);

      Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsByPostcode(postCode);

      for (StreetSuburbVO streetSuburbVO : ssList) {
        if (suburb.equals(streetSuburbVO.getSuburb())) {
          firstPolicyHolder.setPostStsubid(streetSuburbVO.getStreetSuburbID());
          break;
        }
      }

      firstPolicyHolder.setPostStreetChar("");
      this.getInsuranceMgr().setClient(firstPolicyHolder);
      
      // Set DIFFERENT_POSTAL_ADDRESS, so we can handle the PO Box on submission to Pure
      this.getInsuranceMgr().setQuoteDetail(getQuoteNo(), ActionBase.DIFFERENT_POSTAL_ADDRESS, "true");

    } else {
      // Since this represents a normal well formed correspondence address (no PO BOX etc) we do not need to set DIFFERENT_POSTAL_ADDRESS
      firstPolicyHolder.setPostProperty("");
      streetSuburbId = this.getParameter("streetSuburbId");
      if (streetSuburbId != null && !streetSuburbId.isEmpty()) {
        firstPolicyHolder.setPostStsubid(Integer.valueOf(streetSuburbId));
      }

      if (suburb != null && !suburb.isEmpty() && postCode != null && !postCode.isEmpty()) {
      	Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsBySuburb(suburb, postCode);

        for (StreetSuburbVO streetSuburbVO : ssList) {
          if (!streetSuburbVO.getStreet().equals("") && String.valueOf(streetSuburbVO.getStreetSuburbID()).equals(streetSuburbId)) {
            firstPolicyHolder.setPostStreet(streetSuburbVO.getStreet());
            firstPolicyHolder.setPostState(streetSuburbVO.getState());
            firstPolicyHolder.setPostStsubid(streetSuburbVO.getStreetSuburbID());
            this.getInsuranceMgr().setClient(firstPolicyHolder);
            break;
          }
        }
      }

      firstPolicyHolder.setPostStreetChar(streetNumber);
      this.getInsuranceMgr().setClient(firstPolicyHolder);
    }

    firstPolicyHolder = this.getInsuranceService().attemptQasRiskAddress(this.getQuoteNo(), firstPolicyHolder);
    this.getInsuranceMgr().setClient(firstPolicyHolder);

    return "";
  }

  /**
   * Clear the session, but retain references to Agent Portal and promo code.
   */
  public void cleanSession() {
    Map<String, Object> session = getSession();

    String tmpPersistedAgentCode = this.getPersistedAgentCode();
    String tmpPersistedAgentUser = this.getPersistedAgentUser();
    String tmpPromoCode = this.getPromoCode();

    session.clear();

    this.setPersistedAgentCode(tmpPersistedAgentCode);
    this.setPersistedAgentUser(tmpPersistedAgentUser);
    this.setPromoCode(tmpPromoCode);
  }

  /**
   * Load calculated premium for Page 9, which is a premium page.
   */
    public void loadPremium() throws Exception {
        int quoteNo = this.getQuoteNo();
        // Load premium for page 9.
        // Lookup default premium excess.
        // Load various options.

        String coverType = null;
        WebInsQuote quote = this.getInsuranceMgr().getQuote(quoteNo);
        if (quote != null) {
            coverType = quote.getCoverType();
        }

        String premiumAmt = getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.PREMIUM);
        String freq = getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_FREQ);

        // determine defaults
        if (premiumAmt == null || premiumAmt.isEmpty()) {

            String prodType = "PIP"; // default
            if (getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS).equals(WebInsQuoteDetail.TRUE)) {
                prodType = WebInsQuoteDetail.FIFTY_PLUS;
            }

            String defaultExcess = "";
            List<InRfDet> options = getInsuranceMgr().getLookupData(prodType, WebInsQuote.COVER_COMP, WebInsQuoteDetail.EXCESS_OPTIONS, quote.getQuoteDate());
            for (InRfDet r : options) {
                if (r.isAcceptable()) { // default
                    defaultExcess = r.getrClass();
                    break;
                }
            }

            getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.EXCESS, defaultExcess);

            String windScreen = getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.WINDSCREEN);
            if (windScreen != null && windScreen.equals(WebInsQuoteDetail.TRUE)) {
                getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.WINDSCREEN, WebInsQuoteDetail.TRUE);
            } else {
                getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.WINDSCREEN, WebInsQuoteDetail.FALSE);
            }

            String hireCar = getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.HIRE_CAR);
            if (hireCar != null && hireCar.equals(WebInsQuoteDetail.TRUE)) {
                getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.HIRE_CAR, WebInsQuoteDetail.TRUE);
            } else {
                getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.HIRE_CAR, WebInsQuoteDetail.FALSE);
            }

            String fireTheft = getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.FIRE_THEFT);
            if (fireTheft != null && fireTheft.equals(WebInsQuoteDetail.TRUE)) {
                getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.FIRE_THEFT, WebInsQuoteDetail.TRUE);
            } else {
                getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.FIRE_THEFT, WebInsQuoteDetail.FALSE);
            }

            freq = WebInsQuoteDetail.PAY_ANNUALLY;
            getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_FREQ, freq);

            coverType = WebInsQuote.COVER_COMP;
            getInsuranceMgr().setCoverType(quoteNo, coverType);

            // Calculate default premium
            Premium annualPremium = retrievePremium(WebInsQuoteDetail.PAY_ANNUALLY, coverType);
            Premium monthlyPremium = retrievePremium(WebInsQuoteDetail.PAY_MONTHLY, coverType);

            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_BASE_ANNUAL_PERMIUM, annualPremium.getBaseAnnualPremium().toString());
            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_GST, annualPremium.getGst().toString());
            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_STAMP_DUTY, annualPremium.getStampDuty().toString());
            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString());
            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_INSTALMENT_AMOUNT, annualPremium.getPayment().toString());

            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_BASE_ANNUAL_PERMIUM, monthlyPremium.getBaseAnnualPremium().toString());
            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_GST, monthlyPremium.getGst().toString());
            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_STAMP_DUTY, monthlyPremium.getStampDuty().toString());
            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_TOTAL_ANNUAL_PREMIUM, monthlyPremium.getAnnualPremium().toString());
            this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT, monthlyPremium.getPayment().toString());

            // if we encounter a $0 premium, set flag to exit
            if (annualPremium.getAnnualPremium().equals(BigDecimal.ZERO) || monthlyPremium.getAnnualPremium().equals(BigDecimal.ZERO)) {
                this.zeroPremium = true;
            }

            // Save the default premium which is the comprehensive premium.
            getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.PREMIUM, annualPremium.getAnnualPremium().toPlainString());
            getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.PAYMENT, monthlyPremium.getPayment().toPlainString());
        } // done determining defaults

        this.setShowBenefits(true);

        // Load all answers for page 9.
        String annualPremium = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM);
        String monthlyPremium = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT);
        this.loadPage9(coverType, annualPremium, monthlyPremium);
    }

    /*
     * return a list of the clients it is possible to associate to a policy, based on client groups and existing client attachments to policy
     */
    public List<WebInsClient> getPossibleClientAssociations(int quoteNo) throws Exception {
        List<WebInsClient> finalClientList = new ArrayList<WebInsClient>();
        List<WebInsClient> associatedClients = this.getInsuranceMgr().getClients(quoteNo);
        List<WebInsClient> groupClients = this.getInsuranceMgr().getClientGroups(quoteNo);
        
        boolean fiftyPlus = getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS).equals(WebInsQuoteDetail.TRUE);

        
        if (associatedClients != null && !associatedClients.isEmpty()) {
            for (WebInsClient client : associatedClients) {
                if (fiftyPlus && client.getBirthDate() != null && !isOlderThan(client.getBirthDate(), 50)) {
                    Config.logger.info(Config.key + ": client " + client.getDisplayName() + " is not over 50 but the Home quote type is 50+, skipping");
                    continue;
                }
                if (client.getRactClientNo() == null) {
                    Config.logger.debug(Config.key + ": client " + client.getDisplayName() + " is a new, skipping");
                    continue;
                }
                finalClientList.add(client);
            }
        }

        if (groupClients != null && !groupClients.isEmpty()) {
            groupClients: for (WebInsClient groupClient : groupClients) {

                if (fiftyPlus && groupClient.getBirthDate() != null && !isOlderThan(groupClient.getBirthDate(), 50)) {
                    Config.logger.info(Config.key + ": client " + groupClient.getDisplayName() + " is not over 50 but the Home quote type is 50+, skipping");
                    continue groupClients;
                }

                for (WebInsClient existing : finalClientList) {
                    if (isSameClientNumber(existing.getRactClientNo(), groupClient.getRactClientNo()) && isSameGroupListString(existing.getGroupListString(), groupClient.getGroupListString())) {
                        // existing client has already been added
                        continue groupClients;
                    }
                }
                finalClientList.add(groupClient);
            }
        }

        return finalClientList;
    }

    /**
     * Load a list of options for user to click, before entering policy holder page.
     */
    public void loadPolicyHolderOptions() throws Exception {
        int quoteNo = this.getQuoteNo();

        List<WebInsClient> possibleClientAssociations = getPossibleClientAssociations(quoteNo);

        Option option = null;
        QuoteQuestion quoteQuestion22 = this.getQuestionById("q22");
        quoteQuestion22.getOptions().clear();

        for (WebInsClient client : possibleClientAssociations) {
            option = new Option();
            option.setType("text");
            option.setText(client.getSelectionDescription());
            option.setValue(client.getSelectionDescription());
            quoteQuestion22.getOptions().add(option);
        }

        option = new Option();
        option.setType("text");
        option.setText("I would like this policy to have new policy holders");
        option.setValue("-1");
        quoteQuestion22.getOptions().add(option);
    }

  private boolean isSameClientNumber(Integer first, Integer second) {
      return first != null && second != null && first.equals(second);
  }
  
  private boolean isSameGroupListString(String first, String second) {
      return first != null && second != null && first.equals(second);
  }
  
  /**
   * Load answers for page 2.
   *
   * @param startDate  Quote start date.
   * @param fiftyPlus  answer for fiftyPlus question.
   * @param member     whether is a member.
   * @param dateOfBirth    date of birth.
   * @param memberCardNumber   member card number.
   * @throws Exception
   */
  public void loadPage2(String applyPip, String fiftyPlus, String member, String dateOfBirth, String memberCardNumber) throws Exception {

    // Load quote start date question.
    //this.loadQuestion(this.getQuestionById("q2"), startDate);

      // Load apply pip question.
      if (applyPip != null) {
          if (applyPip.equals("yes")) {
              this.loadQuestion(this.getQuestionById("q4"), "yes");
          } else {
              this.loadQuestion(this.getQuestionById("q4"), "no");
          }
      }
      
    // Load fifty plus question.
    if (fiftyPlus != null) {
      if (fiftyPlus.equals("yes")) {
        this.loadQuestion(this.getQuestionById("q3"), "yes");
      } else {
        this.loadQuestion(this.getQuestionById("q3"), "no");
      }
    }

    // Member option not clicked.
    if (member != null) {
      if (member.equals("yes")) { // is a member.

        this.loadQuestion(this.getQuestionById("q5"), "yes");

        if (this.getQuoteNo() != null) {
          if (!dateOfBirth.isEmpty()) {
            this.loadQuestion(this.getSubQuestionById("q5", "sub2"), dateOfBirth);
          } else {
            this.loadQuestion(this.getSubQuestionById("q5", "sub2"), this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "dateOfBirth"));
          }
        } else {
          this.loadQuestion(this.getSubQuestionById("q5", "sub2"), dateOfBirth);
        }

        String[] memberCards = null;

        if (this.getQuoteNo() != null) { // Split member card number for existing quote.
          if (this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "memberCard") != null) {
            memberCards = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "memberCard").split(",");
          } else {// Split member card number from submitted form.
            memberCards = memberCardNumber.split(",");
          }
        } else { // Split member card number from submitted form.
          memberCards = memberCardNumber.split(",");
        }

        // Load anwers to member card number fields based on hom many fields have been filed in.
        switch (memberCards.length) {
          case 1:
            this.getHttpRequest().setAttribute("creditCard1", memberCards[0]);
            break;
          case 2:
            this.getHttpRequest().setAttribute("creditCard1", memberCards[0]);
            this.getHttpRequest().setAttribute("creditCard2", memberCards[1]);
            break;
          case 3:
            this.getHttpRequest().setAttribute("creditCard1", memberCards[0]);
            this.getHttpRequest().setAttribute("creditCard2", memberCards[1]);
            this.getHttpRequest().setAttribute("creditCard3", memberCards[2]);
            break;
          case 4:
            this.getHttpRequest().setAttribute("creditCard1", memberCards[0]);
            this.getHttpRequest().setAttribute("creditCard2", memberCards[1]);
            this.getHttpRequest().setAttribute("creditCard3", memberCards[2]);
            this.getHttpRequest().setAttribute("creditCard4", memberCards[3]);
            break;
        }

        this.getSubQuestionById("q5", "sub1").setDisplay(true);
      } else { // Not a member.
        this.loadQuestion(this.getQuestionById("q5"), "no");
      }
    }
  }

  /**
   * Populate street suburb address questions based on passed in GaragedAddress bean.
   *
   */
  public void loadStreetSuburb(GaragedAddress garagedAddress, QuoteQuestion postCodeQuestion, QuoteQuestion suburbQuestion, QuoteQuestion streetSuburbQuestion, QuoteQuestion streetNumberQuestion, QuoteQuestion poboxQuestion) throws Exception {

    // Postcode.
    if (garagedAddress.getPostCode() != null) {

      this.loadQuestion(postCodeQuestion, garagedAddress.getPostCode());
    } else {

      this.loadQuestion(postCodeQuestion, "");
    }

    // Suburb.
    Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsByPostcode(garagedAddress.getPostCode());

    List<String> suburbs = new ArrayList<String>();
    for (StreetSuburbVO streetSuburbVO  : ssList) {
      if (!suburbs.contains(streetSuburbVO.getSuburb())) {
        suburbs.add(streetSuburbVO.getSuburb());
      }
    }
    // Populate html select for suburb.
    if (garagedAddress.getPostCode() != null) {
      suburbQuestion.setDisplay(true);

      for (String suburb : suburbs) {
        Option option1 = new Option();

        if (garagedAddress.getSuburb().equals(suburb)) {
          option1.setSelected(true);
        }

        option1.setText(suburb);
        option1.setValue(suburb);
        option1.setType("text");
        suburbQuestion.getOptions().add(option1);
      }
    } else {
      this.loadQuestion(suburbQuestion, "");
    }
    // Populate html select for Street Name.
    if (garagedAddress.getSuburb() != null && !garagedAddress.getSuburb().equals("")) {
    	ssList = this.getAddressMgr().getStreetSuburbsBySuburb(garagedAddress.getSuburb(), garagedAddress.getPostCode());

      streetSuburbQuestion.setDisplay(true);

      for (StreetSuburbVO streetSuburbVO  : ssList) {
        Option option1 = new Option();

        String garageStreet = garagedAddress.getStreet();
        if (null != garageStreet && garageStreet.equals(streetSuburbVO.getStreet())) {
            option1.setSelected(true);
        }

        option1.setText(streetSuburbVO.getStreet());
        option1.setValue(String.valueOf(streetSuburbVO.getStreetSuburbID()));
        option1.setType("text");
        streetSuburbQuestion.getOptions().add(option1);
      }
    }
    if (garagedAddress.getStreetSuburbId() != null && !garagedAddress.getStreetSuburbId().equals("")) {
      this.loadQuestion(streetSuburbQuestion, garagedAddress.getStreetSuburbId());
    } else {
      this.loadQuestion(streetSuburbQuestion, "");
    }

    // Street Number.
    if (streetNumberQuestion != null) {
      if (garagedAddress.getStreetNumber() != null && !garagedAddress.getStreetNumber().equals("")) {
        this.loadQuestion(streetNumberQuestion, garagedAddress.getStreetNumber());
      } else {
        this.loadQuestion(streetNumberQuestion, "");
      }
    }

    // Street PO BOX.
    if (poboxQuestion != null) {
      if (garagedAddress.getPoBox() != null && !garagedAddress.getPoBox().equals("")) {
        this.loadQuestion(poboxQuestion, garagedAddress.getPoBox());
      } else {
        this.loadQuestion(poboxQuestion, "");
      }
    }
  }

  /**
   * Load page 3. Vehicle selection page.
   *
   */
  public void loadPage3(GlVehicle vehicle, String imobiliser, String usage, String financier, String underFinance, String lookupOptions, String rego, String nvicDirect) throws Exception {
    // Load vehicles.
    if (vehicle != null) {
      // Vehicle year.
      if (vehicle.getVehYear() == null || String.valueOf(vehicle.getVehYear()).equals("0")) {
        this.loadQuestion(this.getSubQuestionById("q6", "sub1"), "");
      } else {
        this.loadQuestion(this.getSubQuestionById("q6", "sub1"), String.valueOf(vehicle.getVehYear()));
      }

      // Load vehicle make.
      if (vehicle.getNvic() != null && !vehicle.getNvic().isEmpty()) {
        QuoteQuestion quoteQuestion6Sub2 = this.getSubQuestionById("q6", "sub2");
        quoteQuestion6Sub2.setDisplay(true);

        List<ListPair> makes = this.getGlassesMgr().getMakes(vehicle);

        for (ListPair tempMake : makes) {
          Option option = new Option();

          if (vehicle.getMake().equals(tempMake.getCol2().trim()) || vehicle.getMake().equals(tempMake.getCol1().trim())) {
            option.setSelected(true);
          }

          option.setText(tempMake.getCol1());
          option.setValue(tempMake.getCol2().trim());
          option.setType("text");
          quoteQuestion6Sub2.getOptions().add(option);
        }
      //}
	
	      // Load vehicle model. 
	      if (vehicle.getMake() != null && !vehicle.getMake().equals("") && !vehicle.getMake().equals("1")) {
	        QuoteQuestion quoteQuestion6Sub3 = this.getSubQuestionById("q6", "sub3");
	        quoteQuestion6Sub3.setDisplay(true);
	        List<String> modes = this.getGlassesMgr().getModels(vehicle);
	
	        for (String tempModes : modes) {
	          Option option = new Option();
	
	          if (tempModes.trim().equals(vehicle.getModel())) {
	            option.setSelected(true);
	          }
	
	          option.setText(tempModes);
	          option.setValue(tempModes.trim());
	          option.setType("text");
	          quoteQuestion6Sub3.getOptions().add(option);
	        }
	      }
	
	      // Load body type.  
	      if (vehicle.getModel() != null && !vehicle.getModel().equals("") && !vehicle.getModel().equals("1")) {
	        QuoteQuestion quoteQuestion6Sub4 = this.getSubQuestionById("q6", "bodyTypediv");
	        quoteQuestion6Sub4.setDisplay(true);
	        List<ListPair> bodyTypes = this.getGlassesMgr().getBodyTypes(vehicle);
	
	        for (ListPair tempBodyType : bodyTypes) {
	          Option option = new Option();
	
	          if (vehicle.getBodyType().equals(tempBodyType.getCol2().trim())) {
	            option.setSelected(true);
	          }
	
	          option.setText(tempBodyType.getCol1());
	          option.setValue(tempBodyType.getCol2().trim());
	          option.setType("text");
	          quoteQuestion6Sub4.getOptions().add(option);
	        }
	      }
	
	      // Load transmission.  
	      if (vehicle.getBodyType() != null && !vehicle.getBodyType().equals("") && !vehicle.getBodyType().equals("1")) {
	        QuoteQuestion quoteQuestion6Sub5 = this.getSubQuestionById("q6", "transmissiondiv");
	        quoteQuestion6Sub5.setDisplay(true);
	        List<ListPair> transmissions = this.getGlassesMgr().getTransmissionTypes(vehicle);
	
	        for (ListPair tempTransmission : transmissions) {
	          Option option = new Option();
	
	          if (vehicle.getTransmission().equals(tempTransmission.getCol2().trim())) {
	            option.setSelected(true);
	          }
	
	          option.setText(tempTransmission.getCol1());
	          option.setValue(tempTransmission.getCol2().trim());
	          option.setType("text");
	          quoteQuestion6Sub5.getOptions().add(option);
	        }
	      }
	
	      // Load cylinders.  
	      if (vehicle.getTransmission() != null && !vehicle.getTransmission().equals("") && !vehicle.getTransmission().equals("1")) {
	        QuoteQuestion quoteQuestion6Sub6 = this.getSubQuestionById("q6", "cylindersdiv");
	        quoteQuestion6Sub6.setDisplay(true);
	        List<String> cylinders = this.getGlassesMgr().getCylinders(vehicle);
	
	        for (String tempCylinder : cylinders) {
	          Option option = new Option();
	          if (tempCylinder.trim().equals(vehicle.getCylinders())) {
	            option.setSelected(true);
	          }
	
	          option.setText(tempCylinder);
	          option.setValue(tempCylinder.trim());
	          option.setType("text");
	          quoteQuestion6Sub6.getOptions().add(option);
	        }
	      }
	
	      // Load nvic
	      if (vehicle.getCylinders() != null && !vehicle.getCylinders().equals("") && !vehicle.getCylinders().equals("1")) {
	        QuoteQuestion quoteQuestion6Sub7 = this.getSubQuestionById("q6", "nvicdiv");
	        quoteQuestion6Sub7.setDisplay(true);
	        List<GlVehicle> vehicles = this.getGlassesMgr().getList(vehicle, new DateTime("1/1/" + vehicle.getVehYear()));
	
	        for (GlVehicle tempVehicle : vehicles) {
	          Option option = new Option();
	
	          if (tempVehicle.getNvic().trim().equals(vehicle.getNvic())) {
	            option.setSelected(true);
	          }
	
	          option.setText(tempVehicle.getSummary());
	          option.setValue(tempVehicle.getNvic().trim() + ":" + tempVehicle.getAcceptable());
	          option.setType("text");
	          quoteQuestion6Sub7.getOptions().add(option);
	        }
	      }
      }
    }

    // Load page4.
    // Load vehicle lookup method
    this.loadQuestion(this.getQuestionById("q6"), lookupOptions);
    
    // load car rego
    this.loadQuestion(this.getSubQuestionById("q6", "regoLookup"), rego);
    
    // load NVIC derived from car rego
    this.loadQuestion(this.getSubQuestionById("q6", "nvicDirectSub"), nvicDirect);
    
    // Load immobiliser
    this.loadQuestion(this.getQuestionById("q7"), imobiliser);

    // Load primary usage.
    //this.loadQuestion(this.getQuestionById("q8"), usage);
    this.loadUsage(usage);

    this.loadFinanceType(underFinance);
    this.loadFinancier(financier, false);
    //this.loadQuestion(this.getQuestionById("q21"), hasFinancier);
  }

  public void loadPage4(GaragedAddress garagedAddress) throws Exception {
    // Load garage address.
    if (garagedAddress != null) {
      //this.loadStreetSuburb(garagedAddress, this.getSubQuestionById("q9", "sub1"), this.getSubQuestionById("q9", "sub2"), this.getSubQuestionById("q9", "sub3"), this.getSubQuestionById("q9", "sub5"), null);
    	
    	Map<String,Object> context = new HashMap<String,Object>();
    	context.put("residentialSuburb", garagedAddress.getSuburb());
    	context.put("residentialStreet", garagedAddress.getStreet());      
    	context.put("residentialStreetNumber", garagedAddress.getStreetNumber());
    	context.put("residentialPostcode", garagedAddress.getPostCode());
    	context.put("residentialLatitude", garagedAddress.getLatitude());
    	context.put("residentialLongitude", garagedAddress.getLongitude());
    	context.put("residentialGnaf", garagedAddress.getGnaf());      
      context.put("residentialDisplayAddress", this.getDisplayAddress(garagedAddress));
      
      if (garagedAddress.getGnaf() == null || garagedAddress.getGnaf().isEmpty()) {
      	context.put("residentialAddressQuery", this.getDisplayAddress(garagedAddress));
  		}

      ActionContext.getContext().getValueStack().push(context);
    }
  }
  
  private String getDisplayAddress(GaragedAddress garagedAddress) {
		StringBuilder addressString = new StringBuilder();
		
		if (garagedAddress.getStreetNumber() != null) {
			addressString.append(garagedAddress.getStreetNumber());
		}
		if (garagedAddress.getStreet() != null) {
			addressString.append(" " + garagedAddress.getStreet());
		}
		if (garagedAddress.getSuburb() != null) {
			addressString.append(" " + garagedAddress.getSuburb());
		}
		if (garagedAddress.getPostCode() != null) {
			addressString.append(" " + garagedAddress.getPostCode());
		}
		
		return addressString.toString();
	}

  /**
   * Load page 6, which is client details page.
   * Currently only support 2 clients.
   */
  public void loadPage6(List<WebInsClient> clients) throws Exception {
    int index = 1;
    QuoteQuestion quoteQuestionSub1 = null;
    QuoteQuestion quoteQuestionSub2 = null;
    QuoteQuestion quoteQuestionSub3 = null;
    QuoteQuestion quoteQuestionSub4 = null;

    String driver1Frequency = "";
    String driver2Frequency = "";

    for (WebInsClient driver : clients) {
      if (driver.isDriver()) {
        if (index == 1) {
          quoteQuestionSub1 = this.getSubQuestionById("q11", "sub1");
          quoteQuestionSub2 = this.getSubQuestionById("q11", "sub2");
          quoteQuestionSub3 = this.getSubQuestionById("q11", "sub3");
          quoteQuestionSub4 = this.getSubQuestionById("q11", "sub4");
          driver1Frequency = driver.getDrivingFrequency();
        } else {
          this.getQuestionById("q12").setDisplay(true);
          this.getSubQuestionById("q11", "sub11").setDisplay(false);
          quoteQuestionSub1 = this.getSubQuestionById("q12", "sub6");
          quoteQuestionSub2 = this.getSubQuestionById("q12", "sub7");
          quoteQuestionSub3 = this.getSubQuestionById("q12", "sub8");
          quoteQuestionSub4 = this.getSubQuestionById("q12", "sub9");
          driver2Frequency = driver.getDrivingFrequency();
        }


        if (driver.getGivenNames() != null && !driver.getGivenNames().equals("")) {
          this.loadQuestion(quoteQuestionSub1, driver.getGivenNames());
        }

        if (driver.getSurname() != null && !driver.getSurname().equals("")) {
          this.loadQuestion(quoteQuestionSub2, driver.getSurname());
        }

        if (driver.getGender() != null && !driver.getGender().equals("")) {
          this.loadQuestion(quoteQuestionSub3, driver.getGender().trim().toString());
        }

        if (driver.getBirthDate() != null) {
          this.loadQuestion(quoteQuestionSub4, driver.getBirthDate().formatShortDate());
        }

        index++;
      }
    }
    this.loadFrequence1(driver1Frequency);
    this.loadFrequence2(driver2Frequency);
  }

  /**
   * Get a list of clients who are driver.
   */
  public List<WebInsClient> getDrivers() throws Exception {
    int quoteNo = this.getQuoteNo();
    List<WebInsClient> clients = this.getInsuranceMgr().getClients(quoteNo);
    List<WebInsClient> drivers = new ArrayList<WebInsClient>();

    if (clients != null) {
      for (WebInsClient driver : clients) {
        if (driver.isDriver()) {
          drivers.add(driver);
        }
      }
    }

    return drivers;
  }

    /**
     * Load page 14, which is an additional policy holder page.
     * 
     * @param validation
     *            indicate which skip validation or not, when load for an existing quote, validation is not necessary.
     */
    public void loadPage14(boolean validation) throws Exception {
        WebInsClient additionalPolicyHolder = this.getAdditionalPolicyHolder();
        this.setShowBenefits(true);

        this.loadTitles(this.getSubQuestionById("q29", "sub17"), null);

        if (additionalPolicyHolder != null) {
            Config.logger.debug("**** additionalPolicyHolder != null");
            this.loadQuestion(this.getSubQuestionById("q29", "sub16"), "yes");
            // this.loadQuestion(this.getSubQuestionById("q29", "sub17"), additionalPolicyHolder.getTitle());
            this.loadTitles(this.getSubQuestionById("q29", "sub17"), additionalPolicyHolder.getTitle());
            this.loadQuestion(this.getSubQuestionById("q29", "sub18"), additionalPolicyHolder.getGivenNames());
            this.loadQuestion(this.getSubQuestionById("q29", "sub19"), additionalPolicyHolder.getSurname());
            if (additionalPolicyHolder.getBirthDate() != null) {
                this.loadQuestion(this.getSubQuestionById("q29", "sub20"), additionalPolicyHolder.getBirthDate().formatShortDate());
            } else {
                this.loadQuestion(this.getSubQuestionById("q29", "sub20"), "");
            }

            if (additionalPolicyHolder.getGender() != null) {
                this.loadQuestion(this.getSubQuestionById("q29", "sub21"), additionalPolicyHolder.getGender().trim().toString());
            }

            GaragedAddress garagedAddress = null;

            // Load residential address.
            if (additionalPolicyHolder.getResiPostcode() != null && !additionalPolicyHolder.getResiPostcode().isEmpty()) {
                garagedAddress = this.populateGaragedAddress(additionalPolicyHolder, "residential");
            } else {
                garagedAddress = this.populateGaragedAddress(null, "garagedAddress");
            }

            this.loadStreetSuburb(garagedAddress, this.getSubQuestionById("q29", "residentialSubPostCode"), this.getSubQuestionById("q29", "residentialSubsuburb"), this.getSubQuestionById("q29", "residentialSubStreet"), this.getSubQuestionById("q29", "residentialSubstreetNumber"), null);

            // Load postal address.
            if (additionalPolicyHolder.getPostPostcode() != null && !additionalPolicyHolder.getPostPostcode().isEmpty()) {
                garagedAddress = this.populateGaragedAddress(additionalPolicyHolder, "postal");
            } else {
                garagedAddress = this.populateGaragedAddress(null, "garagedAddress");
            }

            this.loadStreetSuburb(garagedAddress, this.getSubQuestionById("q29", "additionalSubPostCode"), this.getSubQuestionById("q29", "additionalSubsuburb"), this.getSubQuestionById("q29", "additionalSubStreet"), this.getSubQuestionById("q29", "additionalSubstreetNumber"), this.getSubQuestionById("q29", "additionalSubpobox"));

            this.loadQuestion(this.getSubQuestionById("q29", "sub26"), "work");

            String phoneNumber = "";
            if (additionalPolicyHolder.getWorkPhone() != null && !additionalPolicyHolder.getWorkPhone().isEmpty()) {
                this.loadQuestion(this.getSubQuestionById("q29", "sub26"), "work");
                phoneNumber = additionalPolicyHolder.getWorkPhone();
            } else if (additionalPolicyHolder.getHomePhone() != null && !additionalPolicyHolder.getHomePhone().isEmpty()) {
                this.loadQuestion(this.getSubQuestionById("q29", "sub26"), "home");
                phoneNumber = additionalPolicyHolder.getHomePhone();
            } else if (additionalPolicyHolder.getMobilePhone() != null && !additionalPolicyHolder.getMobilePhone().isEmpty()) {
                this.loadQuestion(this.getSubQuestionById("q29", "sub26"), "mobile");
                phoneNumber = additionalPolicyHolder.getMobilePhone();
            }

            this.loadQuestion(this.getSubQuestionById("q29", "sub23"), phoneNumber);
            this.loadQuestion(this.getSubQuestionById("q29", "sub24"), additionalPolicyHolder.getEmailAddress());

            if (additionalPolicyHolder.isPreferredAddress()) {
                this.loadQuestion(this.getSubQuestionById("q29", "sub27"), "yes");
            } else {
                this.loadQuestion(this.getSubQuestionById("q29", "sub27"), "no");
            }

            this.loadQuestion(this.getSubQuestionById("q29", "sub15"), "no");

        } else {
            if (validation) {
                this.loadQuestion(this.getSubQuestionById("q29", "sub16"), "yes");

                this.getSubQuestionById("q29", "sub17").setDisplay(true);
                this.getSubQuestionById("q29", "sub18").setDisplay(true);
                this.getSubQuestionById("q29", "sub19").setDisplay(true);
                this.getSubQuestionById("q29", "sub20").setDisplay(true);
                this.getSubQuestionById("q29", "sub21").setDisplay(true);
                this.getSubQuestionById("q29", "additionalSubPostCode").setDisplay(true);
                this.getSubQuestionById("q29", "sub23").setDisplay(true);
                this.getSubQuestionById("q29", "sub24").setDisplay(true);
                this.getSubQuestionById("q29", "sub26").setDisplay(true);
                this.getSubQuestionById("q29", "sub27").setDisplay(true);
                this.getSubQuestionById("q29", "additionalSubsuburb").setDisplay(true);
                this.getSubQuestionById("q29", "additionalSubStreet").setDisplay(true);
                this.getSubQuestionById("q29", "additionalSubpobox").setDisplay(true);
                this.getSubQuestionById("q29", "additionalSubstreetNumber").setDisplay(true);

                this.getSubQuestionById("q29", "residentialAddress").setDisplay(true);
                this.getSubQuestionById("q29", "residentialSubPostCode").setDisplay(true);
                this.getSubQuestionById("q29", "residentialSubsuburb").setDisplay(true);
                this.getSubQuestionById("q29", "residentialSubStreet").setDisplay(true);
                this.getSubQuestionById("q29", "residentialSubstreetNumber").setDisplay(true);
                this.getSubQuestionById("q29", "postalAddress").setDisplay(true);

            } else {
                this.loadQuestion(this.getSubQuestionById("q29", "sub16"), "no");

                GaragedAddress garagedAddress = null;

                // Load residential address.
                garagedAddress = this.populateGaragedAddress(null, "garagedAddress");

                this.loadStreetSuburb(garagedAddress, this.getSubQuestionById("q29", "residentialSubPostCode"), this.getSubQuestionById("q29", "residentialSubsuburb"), this.getSubQuestionById("q29", "residentialSubStreet"), this.getSubQuestionById("q29", "residentialSubstreetNumber"), null);

                this.getSubQuestionById("q29", "residentialSubPostCode").setDisplay(false);
                this.getSubQuestionById("q29", "residentialSubsuburb").setDisplay(false);
                this.getSubQuestionById("q29", "residentialSubStreet").setDisplay(false);
                this.getSubQuestionById("q29", "residentialSubstreetNumber").setDisplay(false);

                // Load postal address.
                garagedAddress = this.populateGaragedAddress(null, "garagedAddress");

                this.loadStreetSuburb(garagedAddress, this.getSubQuestionById("q29", "additionalSubPostCode"), this.getSubQuestionById("q29", "additionalSubsuburb"), this.getSubQuestionById("q29", "additionalSubStreet"), this.getSubQuestionById("q29", "additionalSubstreetNumber"), this.getSubQuestionById("q29", "additionalSubpobox"));

                this.getSubQuestionById("q29", "additionalSubPostCode").setDisplay(false);
                this.getSubQuestionById("q29", "additionalSubsuburb").setDisplay(false);
                this.getSubQuestionById("q29", "additionalSubStreet").setDisplay(false);
                this.getSubQuestionById("q29", "additionalSubstreetNumber").setDisplay(false);
                this.getSubQuestionById("q29", "additionalSubpobox").setDisplay(false);
            }

            this.loadQuestion(this.getSubQuestionById("q29", "sub15"), this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "criminalConvicted"));
        }
    }

  /**
   * Load page 7, which is about number of accidents, primary driver, and when got full driving license.
   */
  public void loadPage7(String fullLicenseYear, String numberOfAccident, String bothAccidents, String givenname, String surname) throws Exception {

    if (fullLicenseYear != null) {
      this.loadQuestion(this.getSubQuestionById("q13", "q13sub1"), fullLicenseYear);
    }

    if (numberOfAccident != null) {
      this.loadQuestion(this.getSubQuestionById("q13", "q13sub2"), numberOfAccident);

      if (numberOfAccident.equals("2")) {
        this.loadQuestion(this.getSubQuestionById("q13", "q13sub4"), bothAccidents);
      }
    }

    String newDescription = this.getQuotePage().getDescription().get(0).replaceAll("name", givenname + " " + surname);
    List<String> descriptions = new ArrayList<String>();
    descriptions.add(newDescription);
    this.getQuotePage().setDescription(descriptions);

    this.setShowCAPTCHA(true);
  }

  /**
   * Load Car usage list based on selected usage.
   */
  public void loadUsage(String usage) throws Exception {
    WebInsQuote webInsQuote = this.getInsuranceMgr().getQuote(this.getQuoteNo());
    ArrayList<RefType> refTypes = this.getInsuranceMgr().getRefList(WebInsQuoteDetail.REF_TYPE_USAGE, webInsQuote.getQuoteDate());

    QuoteQuestion quoteQuestion8 = this.getQuestionById("q8");
    String name = quoteQuestion8.getOptions().get(0).getName();
    quoteQuestion8.getOptions().clear();

    for (RefType refType : refTypes) {
      Option option = new Option();
      option.setType("text");
      option.setValue(refType.getTypeCode());
      option.setName(name);
      option.setText(refType.getTDescription());

      if (refType.getTypeCode().equals(usage)) {
        option.setSelected(true);
      }

      quoteQuestion8.getOptions().add(option);
    }
  }
  
  /**
   * Load driving frequency list 1 based on selected frequency.
   */
  public void loadFrequence1(String frequency) throws Exception {
      WebInsQuote webInsQuote = this.getInsuranceMgr().getQuote(this.getQuoteNo());
      ArrayList<RefType> refTypes = this.getInsuranceMgr().getRefList(WebInsQuoteDetail.REF_TYPE_DRIVING_FREQUENCY, webInsQuote.getQuoteDate());
      
      QuoteQuestion frequence1 = this.getSubQuestionById("q11", "sub5");
      String name = frequence1.getOptions().get(0).getName();
      frequence1.getOptions().clear();
      
      for (RefType refType : refTypes) {
          Option option = new Option();
          option.setType("text");
          option.setValue(refType.getTypeCode());
          option.setName(name);
          option.setText(refType.getTDescription());
          
          if (refType.getTypeCode().equals(frequency)) {
              option.setSelected(true);
          }
          
          frequence1.getOptions().add(option);
      }
  }
  
  /**
   * Load driving frequency list 2 based on selected frequency.
   */
  public void loadFrequence2(String frequency) throws Exception {
      WebInsQuote webInsQuote = this.getInsuranceMgr().getQuote(this.getQuoteNo());
      ArrayList<RefType> refTypes = this.getInsuranceMgr().getRefList(WebInsQuoteDetail.REF_TYPE_DRIVING_FREQUENCY, webInsQuote.getQuoteDate());
      
      QuoteQuestion frequence2 = this.getSubQuestionById("q12", "sub10");
      String name = frequence2.getOptions().get(0).getName();
      frequence2.getOptions().clear();
      
      for (RefType refType : refTypes) {
          Option option = new Option();
          option.setType("text");
          option.setValue(refType.getTypeCode());
          option.setName(name);
          option.setText(refType.getTDescription());
          
          if (refType.getTypeCode().equals(frequency)) {
              option.setSelected(true);
          }
          
          frequence2.getOptions().add(option);
      }
  }

  /**
   * Load a list of financier and set the selected financier to be selected.
   */
  public void loadFinancier(String financier, boolean show) throws Exception {
    WebInsQuote webInsQuote = this.getInsuranceMgr().getQuote(this.getQuoteNo());
    QuoteQuestion quoteQuestion21Sub9 = this.getSubQuestionById("q21", "sub9");
    quoteQuestion21Sub9.setDisplay(true);
    String name = quoteQuestion21Sub9.getOptions().get(0).getName();
    
    List<InRfDet> refTypes = this.getInsuranceMgr().getLookupData("PIP", webInsQuote.getCoverType(), WebInsQuoteDetail.FINANCIER, webInsQuote.getQuoteDate());

    quoteQuestion21Sub9.getOptions().clear();
    
    for (InRfDet refType : refTypes) {
      Option option = new Option();
      option.setType("text");
      option.setValue(refType.getrClass());
      option.setName(name);
      option.setText(refType.getrDescription());

      if (refType.getrClass().equals(financier)) {
        option.setSelected(true);
      }

      quoteQuestion21Sub9.getOptions().add(option);
    }

    if (!show) {
      quoteQuestion21Sub9.setDisplay(false);
    }
  }

  /**
   * Load a list of finance types and set the selected finance type to be selected.
   */
  public void loadFinanceType(String financeType) throws Exception {
    WebInsQuote webInsQuote = this.getInsuranceMgr().getQuote(this.getQuoteNo());
    QuoteQuestion quoteQuestion21 = this.getQuestionById("q21");
    quoteQuestion21.setDisplay(true);
    String name = quoteQuestion21.getOptions().get(0).getName();
    
    List<InRfDet> refTypes = this.getInsuranceMgr().getLookupData("PIP", webInsQuote.getCoverType(), WebInsQuoteDetail.FIN_TYPE, webInsQuote.getQuoteDate());

    quoteQuestion21.getOptions().clear();
    
    for (InRfDet refType : refTypes) {
      Option option = new Option();
      option.setType("text");
      option.setValue(refType.getrClass());
      option.setName(name);
      option.setText(refType.getrDescription());

      if (refType.getrClass().equals(financeType)) {
        option.setSelected(true);
      }

      quoteQuestion21.getOptions().add(option);
    }
  }
  

  /**
   * Load a list of titles and set the selected title to be selected.
   */
  public void loadTitles(QuoteQuestion quoteQuestion, String title) throws Exception {
    String name = quoteQuestion.getOptions().get(0).getName();
    
    Collection<ClientTitleVO> refTypes = this.getCustomerMgr().getClientTitles(true, false);

    quoteQuestion.getOptions().clear();
    
    for (ClientTitleVO refType : refTypes) {
      Option option = new Option();
      option.setType("text");
      option.setValue(refType.getKey());
      option.setName(name);
      option.setText(refType.getTitleName());

      if (refType.getKey().equals(title)) {
        option.setSelected(true);
      }

      quoteQuestion.getOptions().add(option);
    }
  }
  
  /**
   * Load a list of quote start date options.
   * This is used in page 2.
   */
  public void loadCoverStartDate(String coverStart) throws Exception {
    WebInsQuote webInsQuote = this.getInsuranceMgr().getQuote(this.getQuoteNo());

    // Pre populate the cover start date.
    Calendar calendar = Calendar.getInstance();
    Date quoteDate = dateFormat.parse(webInsQuote.getQuoteDate().formatShortDate());

    calendar.setTime(quoteDate);

    QuoteQuestion quoteQuestion17 = this.getQuestionById("q17");
    String name = quoteQuestion17.getOptions().get(0).getName();
    quoteQuestion17.getOptions().clear();

    for (int i = 0; i <= 13; i++) {
      if (i != 0) {
        calendar.add(Calendar.HOUR, 24);
      }

      String coverStartDate = dateFormat.format(calendar.getTime());
      Option option = new Option();
      option.setType("text");
      option.setValue(coverStartDate);
      option.setName(name);
      option.setText(coverStartDate);

      if (coverStartDate.equals(coverStart)) {
        option.setSelected(true);
      }

      quoteQuestion17.getOptions().add(option);
    }
  }

  public void loadPage11(String coverStart, String registration, String noExistingDamage, List<String> extraModifications, String extra) throws Exception {
    loadCoverStartDate(coverStart);

    if (registration != null && !registration.equals("")) {
      this.loadQuestion(this.getQuestionById("q18"), registration);
      
      // hide rego field if already supplied for nvic lookup
      if (this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), FIELD_LOOKUP_OPTION).equals(LOOKUP_OPTION_REGO)) {
      	this.getQuestionById("q18").setCssClass("smooth_inv");
      }
    }

    if (noExistingDamage != null && !noExistingDamage.equals("")) {
      this.loadQuestion(this.getQuestionById("q19"), noExistingDamage);
    }

    if (extra != null && extra.equals("yes")) {
      this.loadQuestion(this.getQuestionById("q20"), "yes");
      if (extraModifications != null && extraModifications.size() > 0) {
        String[] subQuestionIds = {"sub1", "sub2", "sub3", "sub4", "sub5", "sub6", "sub7", "sub8"};

        for (int i = 0; i < extraModifications.size(); i++) {
          for (int j = 0; j < subQuestionIds.length; j++) {
            if (extraModifications.get(i) != null && !extraModifications.get(i).equals("")) {
              this.loadQuestion(this.getSubQuestionById("q20", subQuestionIds[j]), extraModifications.get(i));
            }
          }
        }
      }
    } else if (extra != null && extra.equals("no")) {
      this.loadQuestion(this.getQuestionById("q20"), "no");
    }

  }

  /**
   * @deprecated Pre-load payment page based on what type of premium user has chosen.
   */
  public void loadDirectDebitOption() {
    String premiumType = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), "premiumType");
    this.setShowBenefits(true);
    // Pre-load for monthly premium.
    if (premiumType.equals("compm") || premiumType.equals("tpm")) {
    	this.loadQuestion(this.getQuestionById("q26"), "monthly");
      this.getQuestionById("q26").setDisplay(false);
      this.getQuestionById("q27").setDisplay(false);

      this.getSubQuestionById("q27", "sub1").setDisplay(true);
      this.getSubQuestionById("q27", "sub2").setDisplay(true);
      this.getSubQuestionById("q27", "sub0").setDisplay(true);
      this.getSubQuestionById("q27", "sub3").setDisplay(true);
      this.getSubQuestionById("q27", "sub4").setDisplay(true);
      this.getSubQuestionById("q27", "sub6").setDisplay(true);
    } else { // Pre-load for yearly premium.
    	this.loadQuestion(this.getQuestionById("q26"), "yearly");
      this.getQuestionById("q27").setDisplay(true);
    }
  }

  /**
   * Load the premiums for the specified cover type.
   * @param coverType  type of cover.
   * @param annualPremium annual premium for specified cover type.
   * @param monthlyPremium monthly premium for specified cover type.
   */
    public void loadPremium(String coverType, String annualPremium, String monthlyPremium) {
        DecimalFormat premiumFormat = new DecimalFormat("###,##0.00");
        if (coverType == null || coverType.isEmpty() || coverType.equals(WebInsQuote.COVER_COMP)) {
            this.loadQuestion(this.getSubQuestionById("q15", "sub2"), "comprehensive");
            this.loadQuestion(this.getSubQuestionById("q15", "sub4"), "<dd class=\"amounts\"><div class=\"year\"><span class=\"amount\" id=\"annualPremium\">$" + premiumFormat.format(Double.valueOf(annualPremium)) + "</span> <span class=\"inc\">Per Year</span></div> <div class=\"month\"><span class=\"amount\" id=\"monthlyPremium\">$" + premiumFormat.format(Double.valueOf(monthlyPremium)) + "</span> <span class=\"inc\">Per Month</span></div><span class=\"or\">or</span></dd>");
        } else {
            // tp
            this.loadQuestion(this.getSubQuestionById("q15", "sub2"), "thirdPartyDamage");
            this.loadQuestion(this.getSubQuestionById("q15", "sub11"), "<dd class=\"amounts\"><div class=\"year\"><span class=\"amount\" id=\"annualPremium\">$" + premiumFormat.format(Double.valueOf(annualPremium)) + "</span> <span class=\"inc\">Per Year</span> </div> <div class=\"month\"> <span class=\"amount\" id=\"monthlyPremium\">$" + premiumFormat.format(Double.valueOf(monthlyPremium)) + "</span> <span class=\"inc\">Per Month</span></div><span class=\"or\">or</span></dd>");
        }
    }

  /**
   * Load saved data for premium page.
   * @param coverType  type of cover.
   * @param annualPremium annual premium for cover type.
   * @param monthlyPremium monthly premium for cover type.
   */
  public void loadPage9(String coverType, String annualPremium, String monthlyPremium) {
    this.loadPremium(coverType, annualPremium, monthlyPremium);
    
    WebInsQuote webInsQuote = this.getInsuranceMgr().getQuote(this.getQuoteNo());
    String prodType = "PIP"; //default
    if (this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS).equals(WebInsQuoteDetail.TRUE)) {
      prodType = WebInsQuoteDetail.FIFTY_PLUS;
    }
    
    // Load premium options.
    String windScreen = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.WINDSCREEN);
    String hireCar = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.HIRE_CAR);
    String fireTheft = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIRE_THEFT);

    if (hireCar != null && hireCar.equals(WebInsQuoteDetail.TRUE)) {
      this.setHireCarDiscount(true);
      this.loadQuestion(this.getSubQuestionById("q15", "sub6"), "hireCar");
    } else {
      this.setHireCarDiscount(false);
      this.loadQuestion(this.getSubQuestionById("q15", "sub6"), "");
    }
    
    if (windScreen != null && windScreen.equals(WebInsQuoteDetail.TRUE)) {
      this.setWindscreenDiscount(true);
      this.loadQuestion(this.getSubQuestionById("q15", "sub7"), "windScreen");
    } else {
      this.setWindscreenDiscount(false);
      this.loadQuestion(this.getSubQuestionById("q15", "sub7"), "");
    }

    if (fireTheft != null && fireTheft.equals(WebInsQuoteDetail.TRUE)) {
      this.setFireTheftDiscount(true);
      this.loadQuestion(this.getSubQuestionById("q15", "sub13"), "fireAndTheft");
    } else {
      this.setFireTheftDiscount(false);
      this.loadQuestion(this.getSubQuestionById("q15", "sub13"), "");
    }

    if (coverType.equals(WebInsQuote.COVER_COMP)) {
      this.getSubQuestionById("q15", "sub13").setDisplay(false);      
    } else {
      this.getSubQuestionById("q15", "sub6").setDisplay(false);
      this.getSubQuestionById("q15", "sub7").setDisplay(false);
      this.getSubQuestionById("q15", "sub8").setDisplay(false);
    }
    
    // inject optional excesses
    try {
    	List<InRfDet> options = this.getInsuranceMgr().getLookupData(prodType, WebInsQuote.COVER_COMP, WebInsQuoteDetail.EXCESS_OPTIONS, webInsQuote.getQuoteDate());
    	
    	List<Option> optionList = new ArrayList<Option>();      	
    	for (InRfDet r : options) {
    		Option e = new Option();
    		//e.setText("$" + r.getrClass());
    		e.setText("$" + r.getrDescription());
    		e.setValue(r.getrClass());
    		optionList.add(e);
    	}
    	
    	QuoteQuestion q = this.getSubQuestionById("q15", "sub8");
    	q.setOptions(optionList);
    	
    	this.loadQuestion(q, getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.EXCESS));
    	
    } catch (Exception e) {
    	Config.logger.error(Config.key + ": unable to lookup excess options", e);
    }
    
    try {
      Map<String, InRfDet> compOption = this.getVehicleOptionsLookup(prodType, WebInsQuote.COVER_COMP, webInsQuote.getQuoteDate());
      Map<String, InRfDet> tpOption = this.getVehicleOptionsLookup(prodType, WebInsQuote.COVER_TP, webInsQuote.getQuoteDate());
      
      if(compOption != null) {
        this.getSubQuestionById("q15", "sub6").setTitle(this.getSubQuestionById("q15", "sub6").getTitle() + " $" + Math.round(compOption.get(CAR_HIRECAR).getrValue().doubleValue()));
        this.getSubQuestionById("q15", "sub7").setTitle(this.getSubQuestionById("q15", "sub7").getTitle() + " $" + Math.round(compOption.get(CAR_WINDSCREEN).getrValue().doubleValue()));        
      }
      if(tpOption != null) {
        this.getSubQuestionById("q15", "sub13").setTitle(this.getSubQuestionById("q15", "sub13").getTitle() + " $" + Math.round(tpOption.get(TP_FIREANDTHEFT).getrValue().doubleValue()));
      }
    
    } catch (Exception e) {
      Config.logger.error(Config.key + ": Can not get lookup value in loadpage9: " + e);
    }

  }

  /**
   * Load page 17, which is payment page.
   */
  public void loadPage17() {
    int quoteNo = this.getQuoteNo();
    this.setShowBenefits(true);
        
    String paymentOptions = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_METHOD);
    paymentOptions = (paymentOptions == null || paymentOptions.isEmpty()) ? WebInsQuoteDetail.PAY_ANNUALLY : paymentOptions;
    this.loadQuestion(this.getQuestionById("paymentOptions"), paymentOptions);
    
    // Business Continuity Processing - means Invoice payment is only option
    if (this.isBcpMode()) {
	    QuoteQuestion q = this.getQuestionById("paymentOptions");
	    List<Text> optionList = q.getTexts();
	    //System.out.println("optionList size = " + optionList.size());
	    optionList.remove(0); // remove first option
	    optionList.remove(0); // remove second option
	    q.setTexts(optionList);
	    
	    q.setTitle("You have 1 option regarding payment");
    }
    
    // pay freq
    String payFreq = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_FREQ);
    payFreq = (payFreq == null || payFreq.isEmpty()) ? WebInsQuoteDetail.PAY_ANNUALLY : payFreq;
    this.loadQuestion(this.getQuestionById("q26"), payFreq);
    
    // DD type
    String ddType = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_TYPE);
    ddType = (ddType == null || ddType.isEmpty()) ? InsuranceBase.FIELD_DD_CREDIT_CARD : ddType;
    if (ddType.equals(WebInsQuoteDetail.ACC_TYPE_VISA) || ddType.equals(WebInsQuoteDetail.ACC_TYPE_MASTERCARD)) {
    	this.loadQuestion(this.getSubQuestionById("q27", "selectCardType"), ddType);
    	ddType = InsuranceBase.FIELD_DD_CREDIT_CARD;
    } else if (ddType.equals(WebInsQuoteDetail.ACC_TYPE_DEBIT)) { 
    	ddType = InsuranceBase.FIELD_DD_BANK_ACCOUNT;
    }
    this.loadQuestion(this.getSubQuestionById("q27", "sub2"), ddType);
    
		this.getHttpRequest().setAttribute("bankcard3", getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_NO));
    String bsb = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_BSB);		
    if (bsb != null && bsb.length() == 6) {    
	    this.getHttpRequest().setAttribute("bankcard1", bsb.substring(0, 3));
	    this.getHttpRequest().setAttribute("bankcard2", bsb.substring(3, 6));
    }
    
    this.loadQuestion(this.getSubQuestionById("q27", "sub1"), this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_NAME));  
    this.loadQuestion(this.getSubQuestionById("q27", "ccName"), this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_NAME));
    this.loadQuestion(this.getSubQuestionById("q27", "sub4"), this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_EXP));
    this.loadQuestion(this.getSubQuestionById("q27", "sub6"), "1");
  }
  
  public Map<String, InRfDet> getVehicleOptionsLookup(String prodType, String coverType, DateTime date) throws Exception {
    
    Map<String, InRfDet> optionMap = new HashMap<String, InRfDet>();
    List<InRfDet> options = this.getInsuranceMgr().getLookupData(prodType, coverType, WebInsQuoteDetail.OPTIONS, date);

    for(InRfDet option : options) {
      optionMap.put(option.getrClass(), option);
    }
          
    return optionMap;
  }
  
  public void setVehicleOptionsLookup(Map<String, InRfDet> optionMap) {
    if (this.session != null) {
      this.session.put(VEHICLE_OPTIONS, optionMap);
    }
  }

  /**
   * Load the final summary page, after premium has been calculated.
   */
  public void loadFinalSummaryPage() throws Exception {
    // Cover start date;
    WebInsQuote webInsQuote = this.getInsuranceMgr().getQuote(this.getQuoteNo());
    this.getHttpRequest().setAttribute("coverStartDate", webInsQuote.getQuoteDate().formatShortDate());

    // Policy Type, Amount Covered and Options.
    String coverType = webInsQuote.getCoverType();
    String policyType = "";
    String amountCovered = "";
    String options = "";

    // Fixed excesses
    String prodType = "PIP"; //default
    if (getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS).equals(WebInsQuoteDetail.TRUE)) {
      prodType = WebInsQuoteDetail.FIFTY_PLUS;
    }
    List<ListPair> fixedExcesses = this.getInsuranceMgr().getFixedExcesses(prodType, webInsQuote.getCoverType(), webInsQuote.getQuoteDate());

    // expose fixed excesses to request
    this.getHttpRequest().setAttribute("fixedExcesses", fixedExcesses);

    Map<String, InRfDet> optionMap = this.getVehicleOptionsLookup(prodType, webInsQuote.getCoverType(), webInsQuote.getQuoteDate());
    
    if(optionMap !=  null){
      
      if(optionMap.get(CAR_HIRECAR) != null ) {
        this.getHttpRequest().setAttribute(CAR_HIRECAR, optionMap.get(CAR_HIRECAR).getrValue()); 
      }
      
      if(optionMap.get(CAR_WINDSCREEN) != null ) {
        this.getHttpRequest().setAttribute(CAR_WINDSCREEN, optionMap.get(CAR_WINDSCREEN).getrValue()); 
      }
      
      if(optionMap.get(TP_FIREANDTHEFT) != null ) {
        this.getHttpRequest().setAttribute(TP_FIREANDTHEFT, optionMap.get(TP_FIREANDTHEFT).getrValue()); 
      }
      
    }
    
    // default excess heading
    this.getHttpRequest().setAttribute("defaultExcessHeading", "Excess/Optional Excess");

    // expose current excess to request;
    String excessCode = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.EXCESS);
    this.getHttpRequest().setAttribute("excess", excessCode);

    String excessDescription = excessCode;
    List<InRfDet> excessOptions = insuranceMgr.getLookupData(prodType, webInsQuote.getCoverType(), WebInsQuoteDetail.EXCESS_OPTIONS, webInsQuote.getQuoteDate());
    for (InRfDet r : excessOptions) {
        if (excessCode.equalsIgnoreCase(r.getrClass())) {
            excessDescription =  "$" + r.getrDescription();
        }
    }
    this.getHttpRequest().setAttribute("excessDescription", excessDescription);
        
    String usageCode = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.USEAGE);
    String usageDescription = usageCode;
    for(RefType type : this.getInsuranceMgr().getRefList(WebInsQuoteDetail.REF_TYPE_USAGE, webInsQuote.getQuoteDate()))
    {
        if(type.getTypeCode().equals(usageCode)) {
            usageDescription = type.getTDescription();
        }
    }
    this.getHttpRequest().setAttribute("usageDescription", usageDescription);

    String agreedValue = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.AGREED_VALUE);

    // Comprehensive premium.
    if (coverType.equals(WebInsQuote.COVER_COMP)) {
      
      this.getHttpRequest().setAttribute("compInsurance", true); 
      
      policyType = "Comprehensive";
      amountCovered = Config.configManager.getProperty("insurance.message.comp.amountcovered");
      amountCovered = amountCovered.replaceAll("<value>", agreedValue);

      if (this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.WINDSCREEN).equals(WebInsQuoteDetail.TRUE)) {
        //this will change in the future require RACT IT backend changes.
        //this.getHttpRequest().setAttribute("windscreenOption", optionMap.get(WebInsQuoteDetail.WINDSCREEN));
        
        if (options.isEmpty()) {
          options = options + "Windscreen";
        } else {
          options = options + ", Windscreen";
        }
      }

      if (this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.HIRE_CAR).equals(WebInsQuoteDetail.TRUE)) {
        if (options.isEmpty()) {
          options = options + "Hire Car";
        } else {
          options = options + ", Hire Car";
        }
      }

      amountCovered = "$" + amountCovered;

    } else { // Third party damage premium.
      policyType = "Third Party Damage";
      amountCovered = Config.configManager.getProperty("insurance.message.tp.amountcovered");

      if (this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIRE_THEFT).equals(WebInsQuoteDetail.TRUE)) {
        amountCovered = amountCovered + ". " + Config.configManager.getProperty("insurance.message.tp.fireTheft.amountcovered");
        if (options.isEmpty()) {
          options = options + "Fire &amp; Theft";
        } else {
          options = options + ", Fire &amp; Theft";
        }
      }
    }

    this.getHttpRequest().setAttribute("options", options);
    this.getHttpRequest().setAttribute("policyType", policyType);
    this.getHttpRequest().setAttribute("amountCovered", amountCovered);

    // expose Premium to request
    this.getHttpRequest().setAttribute("premium", getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.PREMIUM));

    this.setShowBenefits(true);

    String hpvStr = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.IS_HPV);
    this.getHttpRequest().setAttribute("hpv", hpvStr);
    
    this.loadPage8();
  }

  /**
   * @return the secureQuoteNumber
   */
  public String getSecureQuoteNumber() {
    Map<String, Object> session = getSession();
    String secureQuoteNumber = (String) session.get(SECURE_QUOTE_NUMBER);
    return secureQuoteNumber;
  }

  /**
   * @param secureQuoteNumber the secureQuoteNumber to set
   */
  public void setSecureQuoteNumber(String secureQuoteNumber) {
    Map<String, Object> session = getSession();
    session.put(SECURE_QUOTE_NUMBER, secureQuoteNumber);
  }

  /**
   * @return the submitButton
   */
  public String getSubmitButton() {
    return submitButton;
  }

  /**
   * @param submitButton the submitButton to set
   */
  public void setSubmitButton(String submitButton) {
    this.submitButton = submitButton;
  }

  /**
   * @return the hasDriver2
   */
  public String getHasDriver2() {
    return hasDriver2;
  }

  /**
   * @param hasDriver2 the hasDriver2 to set
   */
  public void setHasDriver2(String hasDriver2) {
    this.hasDriver2 = hasDriver2;
  }

  /**
   * @return the showNewPolicyHolderLink
   */
  public String getShowNewPolicyHolderLink() {
    return showNewPolicyHolderLink;
  }

  /**
   * @param showNewPolicyHolderLink the showNewPolicyHolderLink to set
   */
  public void setShowNewPolicyHolderLink(String showNewPolicyHolderLink) {
    this.showNewPolicyHolderLink = showNewPolicyHolderLink;
  }

  /**
   * @return the redirectUrl
   */
  public String getRedirectUrl() {
    return redirectUrl;
  }

  /**
   * @param redirectUrl the redirectUrl to set
   */
  public void setRedirectUrl(String redirectUrl) {
    this.redirectUrl = redirectUrl;
  }

  public void setNextAction() {
    this.setNextAction(0);
  }

  public void setNextAction(int increment) {
    long tmpPageId;

    Config.logger.debug(Config.key + ": Car: " + this.getCurrentActionName() + " processing pageId: " + this.getPageId());
    
    if (this.getSubmit().equals(BUTTON_NEXT)) {
      this.setPageId(this.getPageId() + increment);
      tmpPageId = this.getPageId() + 1;
    } else {
      this.setPageId(this.getPageId() - increment);
      tmpPageId = this.getPageId() - 1;
    }

    // Save current page Id, and will return it later if user is trying to retrieve an existing quote number.
    if (this.getQuoteNo() != null) {
      String currentPageID = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), QuotePageSave.FIELD_CURRENT_PAGE_ID);
      Long currentPage = 0l;
      try {
        currentPage = Long.parseLong(currentPageID);
      } catch (Exception e) {
      }

      // update current page id if larger than previous value and user is moving forwards
      if (this.getPageId() > currentPage && this.getSubmit().equals(BUTTON_NEXT)) {
      	Config.logger.debug(Config.key + ": Car: " + this.getCurrentActionName() + " saving currentPageId: " + this.getPageId());
        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), QuotePageSave.FIELD_CURRENT_PAGE_ID, String.valueOf(this.getPageId()));
      }
    }

    this.setSummary(this.quoteQuestionManager.getPage(tmpPageId).getSummary());

    String encSummary = "";
    try {
      encSummary = java.net.URLEncoder.encode(this.getSummary(), "UTF-8");
    } catch (Exception e) {
    }
   
    // This is to cope with https URL.
    this.setRedirectUrl(Config.configManager.getProperty("ract.secure.baseUrl")
    				+ this.getDoNextStep()
            + "?summary=" + encSummary
            + "&pageId=" + this.getPageId()
            + "&submit=" + this.getSubmit()
            + "&dt=" + this.getDt()
            + "&ajaxContext=" + this.isAjaxContext());
  }

  /**
   * @return the summary
   */
  public String getSummary() {
    return summary;
  }

  /**
   * @param summary the summary to set
   */
  public void setSummary(String summary) {
    this.summary = summary;
  }

  /**
   * @return the dt
   */
  public String getDt() {
    return dt;
  }

  /**
   * @param dt the dt to set
   */
  public void setDt(String dt) {
    this.dt = dt;
  }

  /**
   * Lazy initialise progress percentage.
   * @return the progress
   */
  public Long getProgress() {
    if (this.progress == null) {
      Long totalPage = this.getTotalPages();
      Double percentage = 0.0;

      if (totalPage != 0) {
        percentage = (double) pageId / totalPage;
      }
      percentage = percentage * 100;

      this.progress = percentage.longValue();
    }
    return progress;
  }

  /**
   * @param progress the progress to set
   */
  public void setProgress(Long progress) {
    this.progress = progress;
  }

  /**
   * Lazy initialise totalPages.
   * @return the totalPages
   */
  public Long getTotalPages() {
    if (this.totalPages == null) {
      this.totalPages = this.quoteQuestionManager.getNumberOfPage(this.getQuotePage().getPart());
    }
    return totalPages;
  }

  /**
   * @param totalPages the totalPages to set
   */
  public void setTotalPages(Long totalPages) {
    this.totalPages = totalPages;
  }

  /**
   * @return the flow
   */
  public List<QuotePage> getFlow() {
    if (this.flow == null) {
      this.flow = this.quoteQuestionManager.getPagesByPart(this.getQuotePage().getPart());
    }
    return flow;
  }

  /**
   * @param flow the flow to set
   */
  public void setFlow(List<QuotePage> flow) {
    this.flow = flow;
  }

  /**
   * @return the lastPageId
   */
  public Long getLastPageId() {
    if (this.lastPageId == null && this.getQuoteNo() != null) {
      this.lastPageId = Long.parseLong(this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), QuotePageSave.FIELD_CURRENT_PAGE_ID)) - 1;
    }
    return lastPageId;
  }

  /**
   * @param lastPageId the lastPageId to set
   */
  public void setLastPageId(Long lastPageId) {
    this.lastPageId = lastPageId;
  }

  /**
   * Lazy initialise InsuranceMgr bean.
   */
  public WebInsMgrRemote getInsuranceMgr() {
    if (this.insuranceMgr == null) {
      try {
        setUp();
      } catch (Exception e) {
        Config.logger.fatal(Config.key + ": unable to initialise RACT API Beans, ", e);
      }
    }

    return this.insuranceMgr;
  }

  /**
   * @return the page
   */
  public String getStartPage() {
    return startPage;
  }

  /**
   * @param page the page to set
   */
  public void setStartPage(String startPage) {
    this.startPage = startPage;
  }

  /**
   * @return the showCAPTCHA
   */
  public boolean isShowCAPTCHA() {
    return showCAPTCHA;
  }

  /**
   * @param showCAPTCHA the showCAPTCHA to set
   */
  public void setShowCAPTCHA(boolean showCAPTCHA) {
    this.showCAPTCHA = showCAPTCHA;
  }

  /**
   * @return the showBenifits
   */
  public boolean isShowBenefits() {
    if(this.getPageId() >= 9) {
      return true;
    }
    return showBenefits;
  }

  /**
   * @param showBenifits the showBenifits to set
   */
  public void setShowBenefits(boolean showBenefits) {

    this.showBenefits = showBenefits;
  }

  /**
   * @return the pipDiscount
   */
  public Boolean isPipDiscount() {
    if ( this.session != null && this.session.containsKey(PIP_DISCOUNT)) {
      pipDiscount = (Boolean) this.session.get(PIP_DISCOUNT);
    } else if( this.getQuoteNo() != null ) {
        pipDiscount = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.PIP_DISCOUNT).equals(WebInsQuoteDetail.TRUE);
    }
    return pipDiscount;
  }

  /**
   * @param pipDiscount the pipDiscount to set
   */
  public void setPipDiscount(boolean pipDiscount) {
    this.pipDiscount = pipDiscount;
    this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), PIP_DISCOUNT, (pipDiscount ? WebInsQuoteDetail.TRUE : WebInsQuoteDetail.FALSE));
    if (this.session != null) {
      this.session.put(PIP_DISCOUNT, pipDiscount);
    }
  }

  /**
   * @return the ssDiscount
   */
  public Boolean isSsDiscount() {

    if (this.session != null && this.session.containsKey(SS_DISCOUNT)) {
      ssDiscount = (Boolean) this.session.get(SS_DISCOUNT);
    } else if( this.getQuoteNo() != null ) {
        ssDiscount = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS).equals(WebInsQuoteDetail.TRUE);
    }
    return ssDiscount;
  }

  /**
   * @param ssDiscount the ssDiscount to set
   */
  public void setSsDiscount(boolean ssDiscount) {
    this.ssDiscount = ssDiscount;
    this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS, (ssDiscount ? WebInsQuoteDetail.TRUE : WebInsQuoteDetail.FALSE));
    if (this.session != null) {
      this.session.put(SS_DISCOUNT, ssDiscount);
    }
  }

  /**
   * @return the immobiliserDiscount
   */
  public Boolean isImmobiliserDiscount() {
    if (this.session != null && this.session.containsKey(IMMOBILISER_DISCOUNT)) {
      immobiliserDiscount = (Boolean) this.session.get(IMMOBILISER_DISCOUNT);
    } else if( this.getQuoteNo() != null ) {
      if(this.getCompDiscounts()) {
        immobiliserDiscount = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.IMOBILISER).equals(WebInsQuoteDetail.TRUE);
      } else {
        immobiliserDiscount = false;
      }
      this.setImmobiliserDiscount(immobiliserDiscount);
    }
    return immobiliserDiscount;
  }

  /**
   * @param immobiliserDiscount the immobiliserDiscount to set
   */
  public void setImmobiliserDiscount(boolean immobiliserDiscount) {
    this.immobiliserDiscount = immobiliserDiscount;
    if (this.session != null) {
      this.session.put(IMMOBILISER_DISCOUNT, immobiliserDiscount);
    }
  }

  /**
   * @return the hireCarDiscount
   */
  public Boolean isHireCarDiscount() {
    if (this.session != null && this.session.containsKey(HIRECAR_DISCOUNT)) {
      hireCarDiscount = (Boolean) this.session.get(HIRECAR_DISCOUNT);
    } else if( this.getQuoteNo() != null ) {
      hireCarDiscount = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.HIRE_CAR).equals(WebInsQuoteDetail.TRUE);
      this.setHireCarDiscount(hireCarDiscount);
    }
    return hireCarDiscount;
  }

  /**
   * @param hireCarDiscount the hireCarDiscount to set
   */
  public void setHireCarDiscount(boolean hireCarDiscount) {
    this.hireCarDiscount = hireCarDiscount;
    if (this.session != null) {
      this.session.put(HIRECAR_DISCOUNT, hireCarDiscount);
    }
  }

  /**
   * @return the windscreenDiscount
   */
  public Boolean isWindscreenDiscount() {
    if (this.session != null && this.session.containsKey(WINDSCREEN_DISCOUNT)) {
      windscreenDiscount = (Boolean) this.session.get(WINDSCREEN_DISCOUNT);
    } else if( this.getQuoteNo() != null ) {
      windscreenDiscount = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.WINDSCREEN).equals(WebInsQuoteDetail.TRUE);
      this.setWindscreenDiscount(windscreenDiscount);
    }
    return windscreenDiscount;
  }

  /**
   * @param windscreenDiscount the windscreenDiscount to set
   */
  public void setWindscreenDiscount(boolean windscreenDiscount) {
    this.windscreenDiscount = windscreenDiscount;
    if (this.session != null) {
      this.session.put(WINDSCREEN_DISCOUNT, windscreenDiscount);
    }
  }
  
  /**
   * @return the fireTheft
   */
  public Boolean isFireTheftDiscount() {
    if (this.session != null && this.session.containsKey(FIRETHEFT_DISCOUNT)) {
      fireTheftDiscount = (Boolean) this.session.get(FIRETHEFT_DISCOUNT);
    } else if( this.getQuoteNo() != null ) {
      fireTheftDiscount = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIRE_THEFT).equals(WebInsQuoteDetail.TRUE);
      this.setFireTheftDiscount(fireTheftDiscount);
    }
    return fireTheftDiscount;
  }

  /**
   * @param fireTheft the fireTheft to set
   */
  public void setFireTheftDiscount(boolean fireTheftDiscount) {
    this.fireTheftDiscount = fireTheftDiscount;
    if (this.session != null) {
      this.session.put(FIRETHEFT_DISCOUNT, fireTheftDiscount);
    }
  }
  
  
  /**
   * @return the compDiscounts
   */
  public Boolean getCompDiscounts() {
    if (this.session != null && this.session.containsKey(COMP_DISCOUNTS)) {
      compDiscounts = (Boolean) this.session.get(COMP_DISCOUNTS);
    } else if( this.getQuoteNo() != null ) {

      try {

        int quoteNo = this.getQuoteNo();
        String coverType = this.getInsuranceMgr().getQuote(quoteNo).getCoverType();

        if (coverType != null && !coverType.equals(WebInsQuote.COVER_COMP)) {
          compDiscounts = false;
        } else {
          compDiscounts = true;
        }
        
        this.setCompDiscounts(compDiscounts);

      } catch (Exception e) {
        Config.logger.info(Config.key + ":  failed get COMP discounts: ", e);
      }
    }
    return compDiscounts;
  }

  /**
   * @param ssDiscount the ssDiscount to set
   */
  public void setCompDiscounts(boolean compDiscounts) {
    this.compDiscounts = compDiscounts;
    if (this.session != null) {
      this.session.put(COMP_DISCOUNTS, compDiscounts);
    }
  }

  /**
   * @return the savedQuoteNo
   */
  public String getExistingQuoteNumber() {
    return existingQuoteNumber;
  }

  /**
   * @param savedQuoteNo the savedQuoteNo to set
   */
  public void setExistingQuoteNumber(String existingQuoteNumber) {
    this.existingQuoteNumber = existingQuoteNumber;
  }
  
  public String getTotalAnnualPremium() {
  	return this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.PREMIUM);
  }
  
  public String getTotalMonthlyPremium() {
  	return this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.PAYMENT);
  }
  
  /**
   * Lookup a @Premium based on information in action for given frequency and cover type.
   * @param action
   * @param frequency one of @WebInsQuoteDetail.PAY_ANNUALLY or @WebInsQuoteDetail.PAY_MONTHLY.
   * @param coverType one of @WebInsQuote.COVER_COMP or @WebInsQuote.COVER_TP.
   * @return
   */
  protected Premium retrievePremium(String frequency, String coverType) {

    Premium premium = null;

    try {
      premium = this.getInsuranceMgr().getPremium(this.getQuoteNo(), frequency, coverType);

      if (premium.getAnnualPremium().compareTo(BigDecimal.ZERO) != 1) { // not > 0
        throw new GenericException("calculated non-positive premium: " + premium.getAnnualPremium());
      }

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to calculate premium for Car Quote #:" + this.getQuoteNo(), e);
    }

    return premium;
  }
  
  public String getQuoteTypeDescription() {
  	String coverType = this.getInsuranceMgr().getQuote(this.getQuoteNo()).getCoverType();
  	String desc = (coverType.equals(WebInsQuote.COVER_COMP)) ? "Compre-hensive" : "Third Party Property Damage";
  	desc += " Car";
  	
  	return desc;  	
  }
  
  public DateTime getCoverStartDate() {
  	return this.getInsuranceMgr().getQuote(this.getQuoteNo()).getStartCover();  
  }

  public String getPaymentMethod() {
  	return this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.PAY_METHOD);
  }
  
  public String getFrequency() {
  	return this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.PAY_FREQ);
  }

  public Boolean isWebDiscount() {
    return (
    		(this.getPersistedAgentCode() == null || this.getPersistedAgentCode().isEmpty()) // not agent quote
    		&& this.getDiscountMsg() != null); // discount within date range
  }

  public String getHpvMessage() {
  	String msg = null;
  	
		try {
			msg = this.getInsuranceMgr().getMessageText("WEBINS", "QUOTE", "Wording", "", "HPV300", new DateTime());
			msg = msg.replaceAll("\"", "");
			msg = msg.replaceAll("\\n", "");
			
		} catch (Exception e) {
			Config.logger.error(Config.key + ": could not retrieve HPV message for Car Quote #:" + this.getQuoteNo(), e);
		}
  	
  	return msg;
  }
 
	public String getDoNextStep() {
		if (this.doNextStep == null || this.doNextStep.isEmpty()) {
			this.doNextStep = "QuoteProcess.action";
		}
		return doNextStep;
	}

	public void setDoNextStep(String nextAction) {
		this.doNextStep = nextAction;
	}
}
