package futuremedium.client.ract.secure.actions.insurance.car;

import com.ract.util.LogUtil;
import com.ract.web.insurance.WebInsClient;

/**
 * Load policy holder page.
 * @author xmfu
 */
public class QuotePageSave12 extends QuotePageSave {
    private static final long serialVersionUID = 885760649635074323L;
    private String policyHolderOption;

  @Override
  public String execute() throws Exception {
    this.setUp();
    // Option -1, create new policy holders.
    // Other options, load existing policy holders.
    // newPolicyInSession is a flag to indicate whether to load existing policy holders(false) or create a new policy holders(true). 
    LogUtil.warn(getClass(),"Execute - policy holder option: " + getPolicyHolderOption());
    if("-1".equals(getPolicyHolderOption())) {
        this.insuranceMgr.setQuoteDetail(this.getQuoteNo(), "policyHolderOption", this.getPolicyHolderOption());
    } else {
        WebInsClient client = this.getClientsBySelectedPolicyHolder(getPolicyHolderOption());
        if(client != null) {
            if(null == client.getWebClientNo()) {
                // This client has not yet been persisted - save it now, so we can get a non-null webClientNo to store into policyHolderOption
                WebInsClient quoteClient = this.getInsuranceMgr().createInsuranceClient(this.getQuoteNo());
                client.createShallowCopy(quoteClient);
                quoteClient.setWebQuoteNo(this.getQuoteNo());
                quoteClient.setOwner(true);
                quoteClient.setPreferredAddress(true);
                this.insuranceMgr.setClient(quoteClient);
                
                // Update the reference to the populated client detail 
                client = quoteClient;
            }
            
            this.insuranceMgr.setQuoteDetail(this.getQuoteNo(), "policyHolderOption", client.getWebClientNo().toString());
            
            // Store the newly selected client in the session - its the one we should be looking at from here on
            session.put("client", client);
        }
    }

    this.setNextAction();
    return SUCCESS;
  }

  /**
   * @return the policyHolder
   */
  public String getPolicyHolderOption() {
    return policyHolderOption;
  }

  /**
   * @param policyHolder the policyHolder to set
   */
  public void setPolicyHolderOption(String policyHolderOption) {
    this.policyHolderOption = policyHolderOption;
  }
}
