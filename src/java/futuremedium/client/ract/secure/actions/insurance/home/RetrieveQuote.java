package futuremedium.client.ract.secure.actions.insurance.home;

import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Retrieve a saved quote
 * 
 * @author gnewton
 */
public class RetrieveQuote extends ActionBase {
    private static final long serialVersionUID = 7003497664284182709L;
    private String savedQuoteNo;
    private String savedQuoteEmail;

    @Override
    public String execute() throws Exception {

        boolean ok = this.getHomeService().retrieveQuote(this);
        
        String savedQuoteEmail = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_SAVED_QUOTE_EMAIL);
        
        if(null != savedQuoteEmail && savedQuoteEmail.equals(getSavedQuoteEmail())) {
            // Looks like we have a valid quote number and email
        } else {
            this.setActionMessage("We were unable to locate a valid quote with the specified quote number and email address.");
            return INPUT;
        }

        if (ok) {// record user's session
            String sessionId = recordSession(null, RactActionSupport.CONTEXT_HOME_QUOTE);
            this.getHomeService().storeSessionId(this, sessionId);

            this.setActionMessage("Your quote has been retrieved.");
        } else {
            // this.setActionMessage("We were unable to locate a valid quote. " + (this.getExtraMessage() != null ? this.getExtraMessage() : ""));
            return INPUT;
        }

        return SUCCESS;
    }

    @Override
    public void validate() {
        if (this.getCaptchaResponse() == null || this.getCaptchaResponse().isEmpty()) {
            this.addFieldError("captchaResponse", "Please enter the security phrase");
        } else if (!this.verifyCaptcha()) {
            this.addFieldError("captchaResponse", "Please enter the correct security phrase");
        }
    }

    /**
     * @return the savedQuoteNo
     */
    public String getSavedQuoteNo() {
        return savedQuoteNo;
    }

    /**
     * @param savedQuoteNo
     *            the savedQuoteNo to set
     */
    @RequiredStringValidator(message = "Please provide your saved quote number")
    public void setSavedQuoteNo(String savedQuoteNo) {
        this.savedQuoteNo = savedQuoteNo;
    }

    /**
     * @return the savedQuoteEmail
     */
    public String getSavedQuoteEmail() {
        return savedQuoteEmail;
    }

    /**
     * @param savedQuoteEmail
     *            the savedQuoteEmail to set
     */
    @RequiredStringValidator(message = "Please enter your email address", shortCircuit=true)
    @EmailValidator(message = "Please enter a valid email address")
    public void setSavedQuoteEmail(String savedQuoteEmail) {
        this.savedQuoteEmail = savedQuoteEmail;
    }
}