package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.Map;

/**
 * Common elements for Survey form and processing.
 * @author gnewton
 */
public class SurveyBase extends ActionBase implements PersistanceAware {

  private Map<String,String> rateOptions;

  /**
   * @return the rateOptions
   */
  public Map<String,String> getRateOptions() {
    if (this.rateOptions == null) {
      this.rateOptions = this.getOptionService().getRateOptions();
    }
    return rateOptions;
  }

  /**
   * @param rateOptions the rateOptions to set
   */
  public void setRateOptions(Map<String,String> rateOptions) {
    this.rateOptions = rateOptions;
  }

}
