package futuremedium.client.ract.secure.actions.insurance.home;

import com.ract.web.insurance.WebInsClient;

/**
 *
 * @author gnewton
 */
public class AddressDetailsBase extends PolicySupport {

  private String policyholderNames;
  private WebInsClient primaryClient;
  private Boolean requireEmail;

  /**
   * @return the policyholderNames
   */
  public String getPolicyholderNames() {
    if (this.policyholderNames == null) {
      this.policyholderNames = this.getHomeService().retrievePolicyholderNames(this);
    }
    return policyholderNames;
  }

  /**
   * @param policyholderNames the policyholderNames to set
   */
  public void setPolicyholderNames(String policyholderNames) {
    this.policyholderNames = policyholderNames;
  }
  
  /**
   * @return the primaryClient
   */
  public WebInsClient getPrimaryClient() {
    if (this.primaryClient == null) {
      this.primaryClient = this.getHomeService().retrievePrimaryClient(this);
    }
    return primaryClient;
  }

  /**
   * @param primaryClient the primaryClient to set
   */
  public void setPrimaryClient(WebInsClient primaryClient) {
    this.primaryClient = primaryClient;
  }

  /**
   * @return the requireEmail
   */
  public boolean getRequireEmail() {
    if (this.requireEmail == null) {
      this.requireEmail = (this.getPrimaryClient().getEmailAddress() == null || this.getPrimaryClient().getEmailAddress().trim().isEmpty());
    }
    return requireEmail;
  }

  /**
   * @param requireEmail the requireEmail to set
   */
  public void setRequireEmail(Boolean requireEmail) {
    this.requireEmail = requireEmail;
  }



}
