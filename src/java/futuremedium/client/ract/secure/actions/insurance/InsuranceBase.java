package futuremedium.client.ract.secure.actions.insurance;

import javax.servlet.http.Cookie;

import org.apache.struts2.ServletActionContext;

import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.actions.CaptchaSupport;
import futuremedium.client.ract.secure.entities.insurance.InsuranceService;

/**
 * Base class for common insurance functionality.
 * @author gnewton
 */
public class InsuranceBase extends CaptchaSupport {

    private static final long serialVersionUID = -2762843292159859906L;

  static public final String FIELD_SESSION_ID = "sessionId";
  
  static public final String TOKEN_AGENT_CODE = "RACTI_AGENT_CODE";
  static public final String TOKEN_AGENT_USER = "RACTI_AGENT_USER";
  static public final String TOKEN_PROMO_CODE = "PROMO_CODE";
  static public final String TOKEN_TRAFFIC_SOURCE = "trfSrc";
  static public final String TOKEN_SITE_SOURCE = "siteSrc";
  static public final String TOKEN_SCREEN_WIDTH = "scrWth";
  
  static public final String FIELD_DD_CREDIT_CARD = "ddCreditcard";
  static public final String FIELD_DD_BANK_ACCOUNT = "ddBankaccount";
  static public final String FIELD_ABORTED = "aborted";
  static public final String FIELD_RISK_ADDRESS_QUERY = "riskAddressQuery";
  static public final String FIELD_PROMO_CODE = "promoCode";
  
  static public final String FIELD_USER_AGENT = "User_agent";
  static public final String FIELD_USER_IP = "User_IP";
  static public final String FIELD_USER_COUNTRY = "User_country";
  static public final String FIELD_USER_TRAFFIC_SRC = "Traffic_source";
  static public final String FIELD_USER_SITE_SRC = "Site_source";
  static public final String FIELD_USER_SCREEN_WIDTH = "User_screen";
  static public final String FIELD_USER_COOKIES = "User_cookies";
    
  private String persistedAgentCode;
  private String persistedAgentUser;
  private String promoCode;
  private String trafficSource;
  private String siteSource;
  private String screenWidth;
  
  private InsuranceService insuranceService;

  /**
   * @return the persistedAgentCode
   */
  public String getPersistedAgentCode() {
    if (this.persistedAgentCode == null) {
      Cookie c = this.getCookie(InsuranceBase.TOKEN_AGENT_CODE);
      if (c != null && c.getValue() != null && !c.getValue().isEmpty() && !c.getValue().equalsIgnoreCase("null")) {
        this.persistedAgentCode = c.getValue();
      }
    }
    return persistedAgentCode;
  }

  /**
   * @param persistedAgentCode the persistedAgentCode to set
   */
  public void setPersistedAgentCode(String persistedAgentCode) {
    this.persistedAgentCode = persistedAgentCode;

    Cookie userCookie = new Cookie(InsuranceBase.TOKEN_AGENT_CODE, this.persistedAgentCode);
    ServletActionContext.getResponse().addCookie(userCookie);
  }

  /**
   * @return the persistedAgentUser
   */
  public String getPersistedAgentUser() {
    if (this.persistedAgentUser == null) {
      Cookie c = this.getCookie(InsuranceBase.TOKEN_AGENT_USER);
      if (c != null && c.getValue() != null && !c.getValue().isEmpty() && !c.getValue().equalsIgnoreCase("null")) {
        this.persistedAgentUser = c.getValue();
      }
    }
    return this.persistedAgentUser;
  }

  /**
   * @param persistedAgentUser the persistedAgentUser to set
   */
  public void setPersistedAgentUser(String persistedAgentUser) {
    this.persistedAgentUser = persistedAgentUser;

    Cookie userCookie = new Cookie(InsuranceBase.TOKEN_AGENT_USER, this.persistedAgentUser);
    ServletActionContext.getResponse().addCookie(userCookie);
  }

  /**
   * Expose WebInsQuoteDetail.PAY_DD.
   */
  public String getPAY_DD() {
    return WebInsQuoteDetail.PAY_DD;
  }
  
  /**
   * Expose WebInsQuoteDetail.PAY_ANNUALLY.
   */
  public String getPAY_ANNUALLY() {
  	return WebInsQuoteDetail.PAY_ANNUALLY;
  }
  
  /**
   * Expose WebInsQuoteDetail.PAY_MONTHLY.
   */
  public String getPAY_MONTHLY() {
  	return WebInsQuoteDetail.PAY_MONTHLY;
  }

	public InsuranceService getInsuranceService() {
		return insuranceService;
	}

	public void setInsuranceService(InsuranceService insuranceService) {
		this.insuranceService = insuranceService;
	}
	
	/**
	 * Retrieve promoCode from session and flash.
	 * @return
	 */
	public String getPromoCode() {
		if (this.promoCode == null) {
			this.promoCode = (String) this.getSession().get(TOKEN_PROMO_CODE);
			this.getSession().remove(TOKEN_PROMO_CODE);
		}
		
		return this.promoCode;
	}
	
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
		this.getSession().put(TOKEN_PROMO_CODE, this.promoCode);
	}

	/**
	 * Retrieve traffic source referrer from cookie.
	 * @return
	 */
	public String getTrafficSource() {
		if (this.trafficSource == null) {
	      Cookie c = this.getCookie(InsuranceBase.TOKEN_TRAFFIC_SOURCE);
	      if (c != null && c.getValue() != null && !c.getValue().isEmpty() && !c.getValue().equalsIgnoreCase("null")) {
	        this.trafficSource = c.getValue();
	      }
	    }
		return trafficSource;
	}

	public void setTrafficSource(String trafficSource) {
		this.trafficSource = trafficSource;
		
		Cookie userCookie = new Cookie(InsuranceBase.TOKEN_TRAFFIC_SOURCE, this.trafficSource);
	    ServletActionContext.getResponse().addCookie(userCookie);
	}

	/**
	 * Retrieve site source referrer from cookie.
	 * @return
	 */
	public String getSiteSource() {
		if (this.siteSource == null) {
	      Cookie c = this.getCookie(InsuranceBase.TOKEN_SITE_SOURCE);
	      if (c != null && c.getValue() != null && !c.getValue().isEmpty() && !c.getValue().equalsIgnoreCase("null")) {
	        this.siteSource = c.getValue();
	      }
	    }
		return siteSource;
	}

	public void setSiteSource(String siteSource) {
		this.siteSource = siteSource;
		
		Cookie userCookie = new Cookie(InsuranceBase.TOKEN_SITE_SOURCE, this.siteSource);
	    ServletActionContext.getResponse().addCookie(userCookie);
	}

	/**
	 * Retrieve device screen width from cookie. 
	 * @return
	 */
	public String getScreenWidth() {
		if (this.screenWidth == null) {
	      Cookie c = this.getCookie(InsuranceBase.TOKEN_SCREEN_WIDTH);
	      if (c != null && c.getValue() != null && !c.getValue().isEmpty() && !c.getValue().equalsIgnoreCase("null")) {
	        this.screenWidth = c.getValue();
	      }
		}
		return screenWidth;
	}

	public void setScreenWidth(String screenWidth) {
		this.screenWidth = screenWidth;
		
		Cookie userCookie = new Cookie(InsuranceBase.TOKEN_SCREEN_WIDTH, this.screenWidth);
	    ServletActionContext.getResponse().addCookie(userCookie);
	}
}
