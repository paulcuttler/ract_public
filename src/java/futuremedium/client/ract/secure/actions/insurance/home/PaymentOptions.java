package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.Calendar;
import java.util.Date;

import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;

/**
 *
 * @author gnewton
 */
public class PaymentOptions extends PaymentOptionsBase implements PolicyAware {
    private static final long serialVersionUID = -1200614855182870679L;
  private String paymentOption;
  private String frequency;
  private String directDebitMethod;
  private String cardType;
  private String creditCard1;
  private String creditCard2;
  private String creditCard3;
  private String creditCard4;
  private String creditCardName;
  private String expiryDateMonth;
  private String expiryDateYear;
  private String bsb1;
  private String bsb2;
  private String accountNo;
  private String accountName;
  private String bankingConditions;

  @Override
  public String execute() throws Exception {
    boolean ok = this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.PAY_METHOD, this.getPaymentOption());
    ok = ok && this.getHomeService().commenceCover(this);
    
    if(ok) {
        // finalise the policy part of the process
        ok = ok && this.getHomeService().finalisePolicy(this);
    }
    
    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      //this.setActionMessage(this.getErrorMessage());
      //return ERROR;
      return this.getHomeService().abortQuote(this, "Failed to Persist to Pure", null, Config.configManager.getProperty("insurance.message.save.fail"));
    }

    return SUCCESS;
  }

  @Override
  public void validate() {
    if (this.getPaymentOption() != null && !this.getPaymentOption().isEmpty()) {

      if (this.getPaymentOption().equals(WebInsQuoteDetail.PAY_ANNUALLY)
              || ( this.getPaymentOption().equals(WebInsQuoteDetail.PAY_DD)
                && this.getDirectDebitMethod() != null
                && this.getDirectDebitMethod().equals(ActionBase.FIELD_DD_CREDIT_CARD) )) {
        if (this.getCardType() == null || this.getCardType().isEmpty()) {
          this.addFieldError("cardType", "Please select a card type");
        }
        if (this.getCreditCard() == null || !validateCreditCardNumber(this.getCreditCard())) {
          this.addFieldError("creditCard1", "Please enter a valid credit card number");
        }
        if (this.getCreditCardName() == null || this.getCreditCardName().isEmpty()) {
          this.addFieldError("creditCardName", "Please enter the name on credit card");
        }
        if (this.getExpiryDateMonth() == null || this.getExpiryDateMonth().isEmpty()
                || this.getExpiryDateYear() == null || this.getExpiryDateYear().isEmpty()) {
          this.addFieldError("expiryDateDay", "Please enter an expiry date");
        } else {
          Calendar expiryDate = Calendar.getInstance();
          expiryDate.set(Calendar.YEAR, Integer.parseInt(getExpiryDateYear()));
          expiryDate.set(Calendar.MONTH, Integer.parseInt(getExpiryDateMonth()) - 1); // subtract 1 from month value
          expiryDate.set(Calendar.DATE, expiryDate.getMaximum(Calendar.DAY_OF_MONTH));

          if (expiryDate.getTime().before(new Date())) { // expiry date in the past
            addFieldError("expiryDateDay", "Please enter a current expiry date.");
          }
        }
      }

      if (this.getPaymentOption().equals(WebInsQuoteDetail.PAY_DD)) {
      	
 				if (this.getFrequency() == null || this.getFrequency().isEmpty()) {
      		this.addFieldError("frequency", "Please select a frequency");
      	}

        if (this.getDirectDebitMethod() == null || this.getDirectDebitMethod().isEmpty()) {
          this.addFieldError("directDebitMethod", "Please select a direct debit method");
        } else if (this.getDirectDebitMethod().equals(ActionBase.FIELD_DD_BANK_ACCOUNT)) {
          if (this.getBsb() == null || this.getBsb().isEmpty()) {
            this.addFieldError("bsb", "Please enter your BSB");
          } else if (this.getBsb().length() != 6) {
            this.addFieldError("bsb", "Your BSB must consist of two 3 digit numbers.");
          }
          if (this.getAccountNo() == null || this.getAccountNo().isEmpty()) {
            this.addFieldError("accountNo", "Please enter your Account number");
          }
          if (this.getAccountName() == null || this.getAccountName().isEmpty()) {
            this.addFieldError("accountName", "Please enter your Account name");
          }
          if (this.getBankingConditions() == null || !this.getBankingConditions().equals("checked")) {
              this.addFieldError("bankingConditions", "You must agree to the terms of the Direct Debit Request Service Agreement.");
          }
        }
      }

    } else {
      this.addFieldError("paymentOption", "Please select a payment option");
    }
  }

  /**
   * @return the paymentOption
   */
  public String getPaymentOption() {
    return paymentOption;
  }

  /**
   * @param paymentOption the paymentOption to set
   */
  public void setPaymentOption(String paymentOption) {
    this.paymentOption = paymentOption;
  }

  /**
   * @return the directDebitMethod
   */
  public String getDirectDebitMethod() {
    return directDebitMethod;
  }

  /**
   * @param directDebitMethod the directDebitMethod to set
   */
  public void setDirectDebitMethod(String directDebitMethod) {
    this.directDebitMethod = directDebitMethod;
  }

  /**
   * @return the cardType
   */
  public String getCardType() {
    return cardType;
  }

  /**
   * @param cardType the cardType to set
   */
  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  /**
   * @return the creditCard1
   */
  public String getCreditCard1() {
    return creditCard1;
  }

  /**
   * @param creditCard1 the creditCard1 to set
   */
  public void setCreditCard1(String creditCard1) {
    this.creditCard1 = creditCard1;
  }

  /**
   * @return the creditCard2
   */
  public String getCreditCard2() {
    return creditCard2;
  }

  /**
   * @param creditCard2 the creditCard2 to set
   */
  public void setCreditCard2(String creditCard2) {
    this.creditCard2 = creditCard2;
  }

  /**
   * @return the creditCard3
   */
  public String getCreditCard3() {
    return creditCard3;
  }

  /**
   * @param creditCard3 the creditCard3 to set
   */
  public void setCreditCard3(String creditCard3) {
    this.creditCard3 = creditCard3;
  }

  /**
   * @return the creditCard4
   */
  public String getCreditCard4() {
    return creditCard4;
  }

  /**
   * @param creditCard4 the creditCard4 to set
   */
  public void setCreditCard4(String creditCard4) {
    this.creditCard4 = creditCard4;
  }

  /**
   * Get complete credit card number
   */
  public String getCreditCard() {
    if (this.getCreditCard1() != null && this.getCreditCard2() != null && this.getCreditCard3() != null && this.getCreditCard4() != null) {
      return this.getCreditCard1() + this.getCreditCard2() + this.getCreditCard3() + this.getCreditCard4();
    } else {
      return null;
    }
  }

  /**
   * @return the expiryDateMonth
   */
  public String getExpiryDateMonth() {
    return expiryDateMonth;
  }

  /**
   * @param expiryDateMonth the expiryDateMonth to set
   */
  public void setExpiryDateMonth(String expiryDateMonth) {
    this.expiryDateMonth = expiryDateMonth;
  }

  /**
   * @return the expiryDateYear
   */
  public String getExpiryDateYear() {
    return expiryDateYear;
  }

  /**
   * @param expiryDateYear the expiryDateYear to set
   */
  public void setExpiryDateYear(String expiryDateYear) {
    this.expiryDateYear = expiryDateYear;
  }

  public String getExpiryDate() {
    if (this.getExpiryDateMonth() != null && this.getExpiryDateYear() != null) {
      return this.getExpiryDateMonth() + "/" + this.getExpiryDateYear();
    } else {
      return null;
    }
  }

  /**
   * @return the bsb
   */
  public String getBsb1() {
    return bsb1;
  }

  /**
   * @param bsb the bsb to set
   */
  public void setBsb1(String bsb) {
    this.bsb1 = bsb;
  }

  /**
   * @return the bsb
   */
  public String getBsb2() {
    return bsb2;
  }

  /**
   * @param bsb the bsb to set
   */
  public void setBsb2(String bsb) {
    this.bsb2 = bsb;
  }

  public String getBsb() {
    if (this.getBsb1() != null &&  this.getBsb2() != null) {
      return this.getBsb1() + this.getBsb2();
    } else {
      return null;
    }
  }

  /**
   * @return the accountNo
   */
  public String getAccountNo() {
    return accountNo;
  }

  /**
   * @param accountNo the accountNo to set
   */
  public void setAccountNo(String accountNo) {
    this.accountNo = accountNo;
  }

  /**
   * @return the accountName
   */
  public String getAccountName() {
    return accountName;
  }

  /**
   * @param accountName the accountName to set
   */
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  /**
   * @return the creditCardName
   */
  public String getCreditCardName() {
    return creditCardName;
  }

  /**
   * @param creditCardName the creditCardName to set
   */
  public void setCreditCardName(String creditCardName) {
    this.creditCardName = creditCardName;
  }

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

    public String getBankingConditions() {
        return bankingConditions;
    }

    public void setBankingConditions(String bankingConditions) {
        this.bankingConditions = bankingConditions;
    }

}
