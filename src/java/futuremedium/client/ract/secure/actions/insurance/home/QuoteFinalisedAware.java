package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * This is a marker interface, which is used to indicate whether an action is
 * between QuoteAware and PolicyAware.
 * @author gnewton
 */
public interface QuoteFinalisedAware extends PersistanceAware {

}
