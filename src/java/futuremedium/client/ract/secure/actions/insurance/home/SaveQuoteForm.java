package futuremedium.client.ract.secure.actions.insurance.home;

/**
 *
 * @author gnewton
 */
public class SaveQuoteForm extends ActionBase implements PersistanceAware {

  @Override
  public String getPageTitle() {
    return "Email your quote number to yourself";
  }
  
}
