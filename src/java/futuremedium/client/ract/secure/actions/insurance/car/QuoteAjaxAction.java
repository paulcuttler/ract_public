package futuremedium.client.ract.secure.actions.insurance.car;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.ract.common.StreetSuburbVO;
import com.ract.util.DateTime;
import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.Premium;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.entities.insurance.car.AjaxReturn;
import futuremedium.client.ract.secure.entities.insurance.car.GaragedAddress;
import futuremedium.client.ract.secure.entities.insurance.car.QuoteQuestionManager;
import futuremedium.client.ract.secure.entities.insurance.car.StreetComparator;
import futuremedium.client.ract.secure.entities.insurance.car.SuburbComparator;

/**
 * Action processes ajax calls.
 * @author xmfu
 */
public class QuoteAjaxAction extends QuotePageSave {
    private static final long serialVersionUID = 1292886579865599163L;
  private AjaxReturn ajaxReturn;
  private String questionId;
  private String subQuestionId;
  private String postCode;
  private String suburb;
  private String memberCardNo;
  private String dob;
  private String compOption;
  private String tpOption;
  private String hireCar;
  private String windScreen;
  private String excess;
  private String premiumType;
  private String startDate;
  private String yearObtainedLicense;
  private String numberOfAccident;
  private String emailAddress;
  private String allowTasmaniaAddress;

  /**
   * Check whether a member number is valid.
   */
  public String memberCardCheck() throws Exception {
    // Get question.
    quoteQuestionManager = new QuoteQuestionManager();
    String message = quoteQuestionManager.getAjaxReturnMessage(this.getPageId(), this.getQuestionId(), this.getSubQuestionId(), false);

    AjaxReturn newAjaxReturnMessage = new AjaxReturn();

    this.setUp();
    int quoteNo = 0;

    // Create a new temp memory object for reloading pages.
    if (this.getQuoteTempMemoryObject() == null) {
      this.setQuoteTempMemoryObject();
    }

    if (this.getQuoteNo() == null) {
      this.cleanSession();
      Date sd = dateFormat.parse(this.getStartDate());
      DateTime sdTime = new DateTime(sd);
      quoteNo = insuranceMgr.startQuote(sdTime, WebInsQuote.TYPE_MOTOR);
      this.insuranceMgr.setQuoteStatus(quoteNo, WebInsQuote.NEW);
      setQuoteNo(quoteNo);
    } else {
      quoteNo = this.getQuoteNo();
    }

    setSecureQuoteNumber(this.insuranceMgr.getQuote(quoteNo).getFormattedQuoteNo());

    // DOB validation.
    if (!this.validate(this.getDob(), VALIDATE_DATETIME_STRING)) {
      newAjaxReturnMessage.setValidation(false);
      newAjaxReturnMessage.setMessage("The format of dob must be dd/mm/yyyy.");
    } else {
      Date dobDate = dateFormat.parse(this.getDob());
      DateTime dobDateTime = new DateTime(dobDate);

      String[] memberCards = this.getMemberCardNo().split(",");

      WebInsClient client = this.insuranceMgr.setFromRACTClient(quoteNo, memberCards[0] + memberCards[1] + memberCards[2] + memberCards[3], dobDateTime);

      if (client != null) {
        newAjaxReturnMessage.setValidation(true);
        newAjaxReturnMessage.setMessage("you are a valid member of RACT.");
        this.getQuoteTempMemoryObject().setMemberCard(this.getMemberCardNo());
        this.setClient(client);
        this.setMember(true);
      } else {
        newAjaxReturnMessage.setValidation(false);
        this.setMember(false);
        newAjaxReturnMessage.setMessage(message);
      }
    }

    this.setAjaxReturn(newAjaxReturnMessage);
    return SUCCESS;
  }

  /**
   *  Load street list.
   */
  public String streetList() throws Exception {
    setUp();
    AjaxReturn newAjaxReturnMessage = new AjaxReturn();

    if (this.getPostCode() == null || this.getPostCode().isEmpty()) {
      newAjaxReturnMessage.setValidation(false);
    } else {

      try {
        Integer.parseInt(this.getPostCode());
      } catch (Exception e) {
        newAjaxReturnMessage.setValidation(false);
        this.setAjaxReturn(newAjaxReturnMessage);
        return SUCCESS;
      }

      Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsBySuburb(this.getSuburb(), this.getPostCode());
      List<GaragedAddress> garagedAddresses = new ArrayList<GaragedAddress>();

      for(StreetSuburbVO streetSuburbVO : ssList) {
        if (!streetSuburbVO.getStreet().equals("")) {
          GaragedAddress garagedAddress = new GaragedAddress();
          garagedAddress.setStreet(streetSuburbVO.getStreet());
          garagedAddress.setStreetSuburbId(String.valueOf(streetSuburbVO.getStreetSuburbID()));
          garagedAddresses.add(garagedAddress);
        }
      }

      Collections.sort(garagedAddresses, new StreetComparator());

      List<String> list = new ArrayList<String>();
      List<String> keys = new ArrayList<String>();

      for (GaragedAddress garagedAddress : garagedAddresses) {
        list.add(garagedAddress.getStreet());
        keys.add(garagedAddress.getStreetSuburbId());
      }

      if (list.size() > 0) {
        newAjaxReturnMessage.setValidation(true);
        newAjaxReturnMessage.setTexts(list);
        newAjaxReturnMessage.setValues(keys);
      } else {
        newAjaxReturnMessage.setValidation(false);
        newAjaxReturnMessage.setMessage("No streets were found for the supplied suburb");
      }
    }

    this.setAjaxReturn(newAjaxReturnMessage);
    return SUCCESS;
  }

  /**
   * Send an email, currently it is not used.
   */
  public String sendEmail() throws Exception {
    AjaxReturn newAjaxReturnMessage = new AjaxReturn();
    if (this.getQuoteNo() != null) {
      if (this.getEmailAddress() != null && !this.getEmailAddress().isEmpty()) {
        this.insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.CONFIRMATION_EMAIL_ADDRESS, this.getEmailAddress());
        this.insuranceMgr.printCoverDoc(this.getQuoteNo());

        newAjaxReturnMessage.setValidation(true);
      } else {
        newAjaxReturnMessage.setValidation(false);
      }
    } else {
      newAjaxReturnMessage.setValidation(false);
    }

    this.setAjaxReturn(newAjaxReturnMessage);

    return SUCCESS;
  }

  /**
   * Load list of suburbs.
   */
  public String suburbList() throws Exception {

    setUp();

    this.quoteQuestionManager = new QuoteQuestionManager();
    String message = quoteQuestionManager.getAjaxReturnMessage(this.getPageId(), this.getQuestionId(), this.getSubQuestionId(), false);

    AjaxReturn newAjaxReturnMessage = new AjaxReturn();
    newAjaxReturnMessage.setMessage(message);
    newAjaxReturnMessage.setValidation(false);
    this.setAjaxReturn(newAjaxReturnMessage);

    if (this.getPostCode() != null && !this.getPostCode().isEmpty()) {
      try {
        int postCodeInteger = Integer.parseInt(this.getPostCode());

        // Validation for Tasmania postcode.
        if (this.getAllowTasmaniaAddress() != null && this.getAllowTasmaniaAddress().equals("yes")) {
          if (postCodeInteger < 7000 || postCodeInteger >= 8000) {
            return SUCCESS;
          }
        }

      } catch (Exception e) {
        return SUCCESS;
      }

      Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsByPostcode(this.getPostCode());
      List<GaragedAddress> garagedAddresses = new ArrayList<GaragedAddress>();
      List<String> list = new ArrayList<String>();
      List<String> keys = new ArrayList<String>();

      for(StreetSuburbVO streetSuburbVO : ssList) {
        if (!list.contains(streetSuburbVO.getSuburb())) {
          list.add(streetSuburbVO.getSuburb());
        }
      }

      for (String item : list) {
        GaragedAddress garagedAddress = new GaragedAddress();
        garagedAddress.setSuburb(item);
        garagedAddresses.add(garagedAddress);
      }

      Collections.sort(garagedAddresses, new SuburbComparator());

      list.clear();

      for (GaragedAddress garagedAddress : garagedAddresses) {
        list.add(garagedAddress.getSuburb());
        keys.add(garagedAddress.getSuburb());
      }

      // Construct Ajax return object.
      if (list.size() > 0) {
        newAjaxReturnMessage.setValidation(true);
        newAjaxReturnMessage.setTexts(list);
        newAjaxReturnMessage.setValues(keys);
        newAjaxReturnMessage.setMessage("");
      } else {
        newAjaxReturnMessage.setValidation(false);
        newAjaxReturnMessage.setMessage("No suburbs were found for the supplied postcode");
      }
    }

    return SUCCESS;
  }

  /**
   *  Return a update premium based on various selected options.
   */
  public String premium() throws Exception {
    this.setUp();
    int quoteNo = this.getQuoteNo();
    
    String prodType = "PIP"; //default
    if (insuranceMgr.getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS).equals(WebInsQuoteDetail.TRUE)) {
      prodType = WebInsQuoteDetail.FIFTY_PLUS;
    }
    
    // Set up options based on premium type.
    if (this.getPremiumType().equals("comp")) {

      if (this.getWindScreen() != null && this.getWindScreen().equals("yes")) {
        insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.WINDSCREEN, WebInsQuoteDetail.TRUE);
      } else {
        insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.WINDSCREEN, WebInsQuoteDetail.FALSE);
      }

      if (this.getHireCar() != null && this.getHireCar().equals("yes")) {
        insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.HIRE_CAR, WebInsQuoteDetail.TRUE);
      } else {
        insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.HIRE_CAR, WebInsQuoteDetail.FALSE);
      }

      if (this.getExcess() != null && !this.getExcess().equals("")) {
        insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.EXCESS, this.getExcess());
      } else {
        WebInsQuote q = insuranceMgr.getQuote(quoteNo);
        
        String defaultExcess = "";
        List<InRfDet> options = insuranceMgr.getLookupData(prodType, WebInsQuote.COVER_COMP, WebInsQuoteDetail.EXCESS_OPTIONS, q.getQuoteDate());
      	for (InRfDet r : options) {
      		if (r.isAcceptable()) { // default
      			defaultExcess = r.getrClass();
      			break;
      		}
      	}
        
        insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.EXCESS, defaultExcess);
      }

    }

    if (this.getPremiumType().equals("tp")) {
      if (this.getTpOption() != null && !this.getTpOption().equals("")) {
        if (this.getTpOption().equals("fireAndTheft")) {
          insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.FIRE_THEFT, WebInsQuoteDetail.TRUE);
        }
      } else {
        insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.FIRE_THEFT, WebInsQuoteDetail.FALSE);
      }
      WebInsQuote q = insuranceMgr.getQuote(quoteNo);
      
      String defaultExcess = "";
      List<InRfDet> options = insuranceMgr.getLookupData(prodType, WebInsQuote.COVER_TP, WebInsQuoteDetail.EXCESS_OPTIONS, q.getQuoteDate());
    	for (InRfDet r : options) {
    		if (r.isAcceptable()) { // default
    			defaultExcess = r.getrClass();
    			break;
    		}
    	}
      
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.EXCESS, defaultExcess);    
    }

    String coverType = WebInsQuote.COVER_COMP;
    if (this.getPremiumType().equals("comp")) {
        insuranceMgr.setCoverType(quoteNo, WebInsQuote.COVER_COMP);
        coverType = WebInsQuote.COVER_COMP;
    } else if (this.getPremiumType().equals("tp")) {
        insuranceMgr.setCoverType(quoteNo, WebInsQuote.COVER_TP);
        coverType = WebInsQuote.COVER_TP;
    }
    
    // Get premiums based on type and options.
    Premium annualPremium = retrievePremium(WebInsQuoteDetail.PAY_ANNUALLY, coverType);
    Premium monthlyPremium = retrievePremium(WebInsQuoteDetail.PAY_MONTHLY, coverType);
    
    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_BASE_ANNUAL_PERMIUM, annualPremium.getBaseAnnualPremium().toString());
    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_GST, annualPremium.getGst().toString());
    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_STAMP_DUTY, annualPremium.getStampDuty().toString());
    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString());
    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_INSTALMENT_AMOUNT, annualPremium.getPayment().toString());

    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_BASE_ANNUAL_PERMIUM, monthlyPremium.getBaseAnnualPremium().toString());
    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_GST, monthlyPremium.getGst().toString());
    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_STAMP_DUTY, monthlyPremium.getStampDuty().toString());
    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_TOTAL_ANNUAL_PREMIUM, monthlyPremium.getAnnualPremium().toString());
    insuranceMgr.setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT, monthlyPremium.getPayment().toString());
    
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.PREMIUM, annualPremium.getAnnualPremium().toPlainString());
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.PAYMENT, monthlyPremium.getPayment().toPlainString());

    DecimalFormat premiumFormat = new DecimalFormat("###,##0.00");
    String premium = premiumFormat.format(Double.valueOf(annualPremium.getAnnualPremium().toPlainString())) + "|" + premiumFormat.format(Double.valueOf(monthlyPremium.getPayment().toPlainString()));
    

    // Construct ajax return object.
    AjaxReturn newAjaxReturnMessage = new AjaxReturn();
    newAjaxReturnMessage.setValidation(true);
    if (this.getPremiumType().equals("0")) {
      newAjaxReturnMessage.setMessage("0");
    } else {
      newAjaxReturnMessage.setMessage(premium);
    }

    this.setAjaxReturn(newAjaxReturnMessage);

    return SUCCESS;
  }

  /**
   * Calculate the rating drver NCB.
   */
  public String calculateNCB() throws Exception {
    int ncb = -1;
    int year = 0;
    int number = 0;
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());

    AjaxReturn newAjaxReturnMessage = new AjaxReturn();

    try {
      year = Integer.valueOf(this.getYearObtainedLicense());
      number = Integer.valueOf(this.getNumberOfAccident());

      this.setUp();
      // Get rating driver.
      WebInsClient ratingDriver = this.insuranceMgr.getRatingDriver(this.getQuoteNo());

      // Set year commenced driving and number of accidents.
      ratingDriver.setYearCommencedDriving(year);
      ratingDriver.setNumberOfAccidents(number);

      // Calculate NCB.
      ncb = this.insuranceMgr.calculateNCD(ratingDriver);

      if ((year - ratingDriver.getBirthDate().getDateYear()) < 16 || year > calendar.get(Calendar.YEAR)) {
        newAjaxReturnMessage.setValidation(false);
        newAjaxReturnMessage.setMessage(this.getValidationMessage("", VALIDATE_COMMENCED_DRIVING));
        this.setAjaxReturn(newAjaxReturnMessage);
        return SUCCESS;
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

    // No ncb is available.
    if (number <= 2) {
      if (ncb < 60) {
        newAjaxReturnMessage.setValidation(false);
        newAjaxReturnMessage.setMessage("" + ncb);
      } else {
        newAjaxReturnMessage.setValidation(true);
        newAjaxReturnMessage.setMessage("" + ncb);
      }
    } else {
      newAjaxReturnMessage.setValidation(false);
      newAjaxReturnMessage.setMessage("");
    }

    this.setAjaxReturn(newAjaxReturnMessage);

    return SUCCESS;
  }

  /**
   * @return the ajaxReturn
   */
  public AjaxReturn getAjaxReturn() {
    return ajaxReturn;
  }

  /**
   * @param ajaxReturn the ajaxReturn to set
   */
  public void setAjaxReturn(AjaxReturn ajaxReturn) {
    this.ajaxReturn = ajaxReturn;
  }

  /**
   * @return the questionId
   */
  public String getQuestionId() {
    return questionId;
  }

  /**
   * @param questionId the questionId to set
   */
  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  /**
   * @return the subQuestionId
   */
  public String getSubQuestionId() {
    return subQuestionId;
  }

  /**
   * @param subQuestionId the subQuestionId to set
   */
  public void setSubQuestionId(String subQuestionId) {
    this.subQuestionId = subQuestionId;
  }

  /**
   * @return the postCode
   */
  public String getPostCode() {
    return postCode;
  }

  /**
   * @param postCode the postCode to set
   */
  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  /**
   * @return the memberCardNo
   */
  public String getMemberCardNo() {
    return memberCardNo;
  }

  /**
   * @param memberCardNo the memberCardNo to set
   */
  public void setMemberCardNo(String memberCardNo) {
    this.memberCardNo = memberCardNo;
  }

  /**
   * @return the dob
   */
  public String getDob() {
    return dob;
  }

  /**
   * @param dob the dob to set
   */
  public void setDob(String dob) {
    this.dob = dob;
  }

  /**
   * @return the excess
   */
  public String getExcess() {
    return excess;
  }

  /**
   * @param excess the excess to set
   */
  public void setExcess(String excess) {
    this.excess = excess;
  }

  /**
   * @return the premiumType
   */
  public String getPremiumType() {
    return premiumType;
  }

  /**
   * @param premiumType the premiumType to set
   */
  public void setPremiumType(String premiumType) {
    this.premiumType = premiumType;
  }

  /**
   * @return the compOption
   */
  public String getCompOption() {
    return compOption;
  }

  /**
   * @param compOption the compOption to set
   */
  public void setCompOption(String compOption) {
    this.compOption = compOption;
  }

  /**
   * @return the tpOption
   */
  public String getTpOption() {
    return tpOption;
  }

  /**
   * @param tpOption the tpOption to set
   */
  public void setTpOption(String tpOption) {
    this.tpOption = tpOption;
  }

  /**
   * @return the startDate
   */
  public String getStartDate() {
    return startDate;
  }

  /**
   * @param startDate the startDate to set
   */
  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  /**
   * @return the yearObtainedLicense
   */
  public String getYearObtainedLicense() {
    return yearObtainedLicense;
  }

  /**
   * @param yearObtainedLicense the yearObtainedLicense to set
   */
  public void setYearObtainedLicense(String yearObtainedLicense) {
    this.yearObtainedLicense = yearObtainedLicense;
  }

  /**
   * @return the numberOfAccident
   */
  public String getNumberOfAccident() {
    return numberOfAccident;
  }

  /**
   * @param numberOfAccident the numberOfAccident to set
   */
  public void setNumberOfAccident(String numberOfAccident) {
    this.numberOfAccident = numberOfAccident;
  }

  /**
   * @return the suburb
   */
  public String getSuburb() {
    return suburb;
  }

  /**
   * @param suburb the suburb to set
   */
  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  /**
   * @return the hireCar
   */
  public String getHireCar() {
    return hireCar;
  }

  /**
   * @param hireCar the hireCar to set
   */
  public void setHireCar(String hireCar) {
    this.hireCar = hireCar;
  }

  /**
   * @return the windScreen
   */
  public String getWindScreen() {
    return windScreen;
  }

  /**
   * @param windScreen the windScreen to set
   */
  public void setWindScreen(String windScreen) {
    this.windScreen = windScreen;
  }

  /**
   * @return the emailAddress
   */
  public String getEmailAddress() {
    return emailAddress;
  }

  /**
   * @param emailAddress the emailAddress to set
   */
  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  /**
   * @return the allowTasmaniaAddress
   */
  public String getAllowTasmaniaAddress() {
    return allowTasmaniaAddress;
  }

  /**
   * @param allowTasmaniaAddress the allowTasmaniaAddress to set
   */
  public void setAllowTasmaniaAddress(String allowTasmaniaAddress) {
    this.allowTasmaniaAddress = allowTasmaniaAddress;
  }
}
