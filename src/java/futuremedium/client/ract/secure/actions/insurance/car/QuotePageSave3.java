package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.regex.Pattern;

import com.ract.util.DateTime;
import com.ract.web.insurance.GlVehicle;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.entities.insurance.car.Option;
import futuremedium.client.ract.secure.entities.insurance.car.QuoteQuestion;

/**
 *
 * @author xmfu
 */
public class QuotePageSave3 extends QuotePageSave {

    private static final long serialVersionUID = 1941532107250022471L;
  private String year;
  private String lookupOptions;
  private String rego;
  private String nvicDirect;
  private String make;
  private String bodyType;
  private String model;
  private String transmission;
  private String cylinders;
  private String nvic;
  private String membershipCard1;
  private String membershipCard2;
  private String membershipCard3;
  private String membershipCard4;
  private String immobiliser;
  private String usage;
  private String financier;
  private String underFinance;

  @Override
  public String execute() throws Exception {
    this.setUp();

    int quoteNo = this.getQuoteNo();

    // If user has reached page 9, access from page 1 to page 8 is not allowed.
    if (isReachedPage9()) {
      this.setNextAction();
      return SUCCESS;
    }

    // Go back to previous page.
    if (this.getSubmit().equals(BUTTON_BACK)) {
      this.setNextAction();
      return SUCCESS;
    }

    // inject NVIC "direct" into NVIC
    if (this.getLookupOptions().equals(LOOKUP_OPTION_REGO)) {
    	this.setNvic(this.getNvicDirect());
    }
    
    // Construct a temp vehicle bean to hold submitted data. The only purpose is to reload page after validation failed.
    GlVehicle vehicle = new GlVehicle();
    try {
    	vehicle.setVehYear(Integer.parseInt(this.getYear()));
    } catch (Exception e) {}
    vehicle.setMake(this.getMake());
    vehicle.setModel(this.getModel());
    vehicle.setBodyType(this.getBodyType());
    vehicle.setTransmission(this.getTransmission());
    vehicle.setCylinders(this.getCylinders());
    vehicle.setNvic(this.getNvic());

    // Validation.

    GlVehicle glVehicle = this.getGlassesMgr().getVehicle(this.getNvic(), new DateTime());    
    String agreedValue = String.valueOf(glVehicle.getValue());

    insuranceMgr.setQuoteDetail(quoteNo, FIELD_LOOKUP_OPTION, this.getLookupOptions());
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.REG_NO, this.getRego());

    // Save default agreed value for reloading.
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.AGREED_VALUE, agreedValue);
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.NVIC, this.getNvic());
    
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.IS_HPV, Boolean.toString(glVehicle.getAcceptable().equalsIgnoreCase(VALUE_HIGH_PERFORMANCE_VEHICLE)));
    
    insuranceMgr.setQuoteDetail(quoteNo, "vehicleValue", "");

    if (this.getImmobiliser().equals("yes")) {
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.IMOBILISER, WebInsQuoteDetail.TRUE);
      this.setImmobiliserDiscount(true);
    } else {
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.IMOBILISER, WebInsQuoteDetail.FALSE);
      this.setImmobiliserDiscount(false);
    }

    // Store finance.
    this.insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.FINANCIER, this.getFinancier());
    this.insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.FIN_TYPE, this.getUnderFinance());

    // Store usage.
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.USEAGE, this.getUsage());
    
    // now hack it about to pull out the description for the below checks, though we probably want to start using acceptable flag
    String checkUsage = "";
    this.loadUsage(this.getUsage());
    QuoteQuestion quoteQuestion8 = this.getQuestionById("q8");
    for(Option option : quoteQuestion8.getOptions()) {
        if(this.getUsage().equals(option.getValue())) {
            checkUsage = option.getText();
            break;
        }
    }
    
    if (checkUsage.equals("Private") || checkUsage.equals("Tradesman (Owner Driver)") || checkUsage.equals("Farmer")) {
        // continue
    } else if (checkUsage.equals("Courier/Delivery") || checkUsage.equals("Driver Education") || checkUsage.equals("Taxi/Hire Car") || checkUsage.equals("Security Vehicle")) {
        return messagePage(MESSAGE_CARUSAGE_COMMERCIAL);
    } else {
        return messagePage(MESSAGE_CARUSAGE_UNINSURANCABLE);
    }
    
    // Check to ensure a vehicle is not unacceptable.
    if (glVehicle.getAcceptable().trim().equalsIgnoreCase(VALUE_UNACCEPTABLE_VEHICLE)) {
      // Display message page and stop quote.
      return this.messagePage(MESSAGE_UNACCEPTABLE_VEHICLE);
    }
    
    this.setNextAction();
    return SUCCESS;

  }

  @Override
  public void validate() {

    if (this.getSubmit().equals(BUTTON_BACK)) {
      return;
    }

    try {
    	GlVehicle vehicle = null;
      
    	if (this.getLookupOptions() == null || this.getLookupOptions().isEmpty()) {
    		this.addFieldError("lookupOptions", "Please answer this question");
    	} else {
    		
    		if (this.getLookupOptions().equals(LOOKUP_OPTION_REGO)) {
    			if (this.getRego() == null || this.getRego().isEmpty()) {
    				this.addFieldError("rego", "Please enter your car registration");
    			} else if (this.getNvicDirect() == null || this.getNvicDirect().isEmpty()) {
  	        //try to lookup
    				this.setNvicDirect(this.getGlassesMgr().getNvicByRegistration(this.getRego(), STATE_TAS));
    				GlVehicle glVehicle = this.getGlassesMgr().getVehicle(this.getNvicDirect(), new DateTime());
    		    
    		    if (glVehicle == null || glVehicle.getNvic() == null || glVehicle.getNvic().isEmpty()) {
    		    	this.addFieldError("rego", "Your car was not found, enter your car description");
    		    }    		    
  	      }
    		} else if (this.getLookupOptions().equals(LOOKUP_OPTION_LOOKUP)) { // lookup    	
    			// Construct a temp vehicle bean to hold submitted data. The only purpose is to reload page after validation failed.
    			vehicle = new GlVehicle();
    			try {
    				vehicle.setVehYear(Integer.parseInt(this.getYear()));
    			} catch (Exception e) {}
		      vehicle.setMake(this.getMake());
		      vehicle.setModel(this.getModel());
		      vehicle.setBodyType(this.getBodyType());
		      vehicle.setTransmission(this.getTransmission());
		      vehicle.setCylinders(this.getCylinders());
		      vehicle.setNvic(this.getNvic());
		
		      if (this.getYear() == null || this.getYear().isEmpty() || !Pattern.matches("\\d\\d\\d\\d", this.getYear())) {
		        this.addFieldError("year", getValidationMessage("Year", VALIDATE_FOUR_INTEGER));
		      }
		      if (!this.validate(this.getMake(), VALIDATE_SELECT)) {
		        this.addFieldError("make", getValidationMessage("Make", VALIDATE_SELECT));
		      }
		      if (!this.validate(this.getModel(), VALIDATE_SELECT)) {
		        this.addFieldError("model", getValidationMessage("Model", VALIDATE_SELECT));
		      }
		      if (!this.validate(this.getBodyType(), VALIDATE_SELECT)) {
		        this.addFieldError("bodyType", getValidationMessage("Body Type", VALIDATE_SELECT));
		      }
		      if (!this.validate(this.getTransmission(), VALIDATE_SELECT)) {
		        this.addFieldError("transmission", getValidationMessage("Transmission Type", VALIDATE_SELECT));
		      }
		      if (!this.validate(this.getNvic(), VALIDATE_SELECT)) {
		        this.addFieldError("nvic", getValidationMessage("Vehicle", VALIDATE_SELECT));

		        // only validate cylinders if NVIC fails
			      if (!this.validate(this.getCylinders(), VALIDATE_SELECT)) {
			        this.addFieldError("cylinders", getValidationMessage("Cylinders", VALIDATE_SELECT));
			      }
		      }
	    	}
    	}
  		
      if (!this.validate(this.getImmobiliser(), VALIDATE_STRING)) {
        this.addFieldError("immobiliser", "Please answer this question");
      }
      if (!this.validate(this.getUnderFinance(), VALIDATE_STRING)) {
        this.addFieldError("underFinance", "Please answer this question");
      } else if (!this.getUnderFinance().equals(QuotePageSave.VALUE_FIN_TYPE_NO)) { // Anything other than "No Finance" requires a Financier  
        if (!this.validate(this.getFinancier(), VALIDATE_STRING)) {
          this.addFieldError("financier", "Please select an option");
        }
      }
      if (!this.validate(this.getUsage(), VALIDATE_STRING)) {
        this.addFieldError("usage", "Please answer this question");
      }

      if (!this.getFieldErrors().isEmpty()) {      	
        this.loadPage3(vehicle, this.getImmobiliser(), this.getUsage(), this.getFinancier(), this.getUnderFinance(), this.getLookupOptions(), this.getRego(), this.getNvicDirect());
      }

    } catch (Exception e) {
      Config.logger.error(Config.key + ": loadpage3 failed on validation: ", e);
    }
  }

  /**
   * Strip NVIC from acceptability value.
   * @return the nvic
   */
  public String getNvic() {
  	if (nvic.contains(":")) {
  		return nvic.split(":")[0];
  	}
  	
    return nvic;
  }

  /**
   * @param nvic the nvic to set
   */
  public void setNvic(String nvic) {
    this.nvic = nvic;
  }

  /**
   * @return the year
   */
  public String getYear() {
    return year;
  }

  /**
   * @param year the year to set
   */
  public void setYear(String year) {
    this.year = year;
  }

  /**
   * @return the make
   */
  public String getMake() {
    return make;
  }

  /**
   * @param make the make to set
   */
  public void setMake(String make) {
    this.make = make;
  }

  /**
   * @return the bodyType
   */
  public String getBodyType() {
    return bodyType;
  }

  /**
   * @param bodyType the bodyType to set
   */
  public void setBodyType(String bodyType) {
    this.bodyType = bodyType;
  }

  /**
   * @return the model
   */
  public String getModel() {
    return model;
  }

  /**
   * @param model the model to set
   */
  public void setModel(String model) {
    this.model = model;
  }

  /**
   * @return the transmission
   */
  public String getTransmission() {
    return transmission;
  }

  /**
   * @param transmission the transmission to set
   */
  public void setTransmission(String transmission) {
    this.transmission = transmission;
  }

  /**
   * @return the cylinders
   */
  public String getCylinders() {
    return cylinders;
  }

  /**
   * @param cylinders the cylinders to set
   */
  public void setCylinders(String cylinders) {
    this.cylinders = cylinders;
  }

  /**
   * @return the membershipCard1
   */
  public String getMembershipCard1() {
    return membershipCard1;
  }

  /**
   * @param membershipCard1 the membershipCard1 to set
   */
  public void setMembershipCard1(String membershipCard1) {
    this.membershipCard1 = membershipCard1;
  }

  /**
   * @return the membershipCard2
   */
  public String getMembershipCard2() {
    return membershipCard2;
  }

  /**
   * @param membershipCard2 the membershipCard2 to set
   */
  public void setMembershipCard2(String membershipCard2) {
    this.membershipCard2 = membershipCard2;
  }

  /**
   * @return the membershipCard3
   */
  public String getMembershipCard3() {
    return membershipCard3;
  }

  /**
   * @param membershipCard3 the membershipCard3 to set
   */
  public void setMembershipCard3(String membershipCard3) {
    this.membershipCard3 = membershipCard3;
  }

  /**
   * @return the membershipCard4
   */
  public String getMembershipCard4() {
    return membershipCard4;
  }

  /**
   * @param membershipCard4 the membershipCard4 to set
   */
  public void setMembershipCard4(String membershipCard4) {
    this.membershipCard4 = membershipCard4;
  }

  /**
   * @return the immobiliser
   */
  public String getImmobiliser() {
    return immobiliser;
  }

  /**
   * @param immobiliser the immobiliser to set
   */
  public void setImmobiliser(String immobiliser) {
    this.immobiliser = immobiliser;
  }

  /**
   * @return the usage
   */
  public String getUsage() {
    return usage;
  }

  /**
   * @param usage the usage to set
   */
  public void setUsage(String usage) {
    this.usage = usage;
  }

  /**
   * @return the underFinance
   */
  public String getUnderFinance() {
    return underFinance;
  }

  /**
   * @param underFinance the underFinance to set
   */
  public void setUnderFinance(String underFinance) {
    this.underFinance = underFinance;
  }

  /**
   * @return the financier
   */
  public String getFinancier() {
    return financier;
  }

  /**
   * @param financier the financier to set
   */
  public void setFinancier(String financier) {
    this.financier = financier;
  }

	public String getLookupOptions() {
		return lookupOptions;
	}

	public void setLookupOptions(String lookupOptions) {
		this.lookupOptions = lookupOptions;
	}

	public String getRego() {
		return rego;
	}

	public void setRego(String rego) {
		this.rego = rego;
	}

	public String getNvicDirect() {
		return nvicDirect;
	}

	public void setNvicDirect(String nvicDirect) {
		this.nvicDirect = nvicDirect;
	}
}