package futuremedium.client.ract.secure.actions.insurance.home;

/**
 *
 * @author gnewton
 */
public class ReminderForm extends ActionBase implements PersistanceAware {

  @Override
  public String getPageTitle() {
    return "Your reminder information";
  }
}
