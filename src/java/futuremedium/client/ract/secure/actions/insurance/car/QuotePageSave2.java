package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.Date;

import com.ract.util.DateTime;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.actions.insurance.InsuranceBase;


/**
 * Page 2 action.
 * @author xmfu
 */
public class QuotePageSave2 extends QuotePageSave {
    private static final long serialVersionUID = 6319073591841117923L;
  private String applyPip; 
  private String fiftyPlus;
  private String member;
  private String dateOfBirth;
  private String membershipCard1;
  private String membershipCard2;
  private String membershipCard3;
  private String membershipCard4;

  @Override
  public String execute() throws Exception {
    // If user has reached page 9, access from page 1 to page 8 is not allowed.
    if (isReachedPage9()) {
      this.setNextAction();
      return SUCCESS;
    }
    
    // Go back to previous page.
    if (this.getSubmit().equals(BUTTON_BACK)) {
      this.setNextAction();
      return SUCCESS;
    }

    // Construct membership card.
    String memberShipCards = this.getMembershipCards();

    // Validation.
    if (!this.validate(this.getApplyPip(), VALIDATE_STRING)) {
        this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), this.getDateOfBirth(), memberShipCards);
        return this.validationFailed("whether all drivers to be covered by this policy are over 50 and at least 1 driver working less than 20 hours a week", VALIDATE_RADIO, false);
    } else if (!this.validate(this.getFiftyPlus(), VALIDATE_STRING)) {
      this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), this.getDateOfBirth(), memberShipCards);
      return this.validationFailed("whether all drivers to be covered by this policy are over 50 and at least 1 driver working less than 20 hours a week", VALIDATE_RADIO, false);
    } else if (!this.validate(this.getMember(), VALIDATE_STRING)) {
      this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), this.getDateOfBirth(), memberShipCards);
      return this.validationFailed("Member", VALIDATE_RADIO, false);
    } else {
      // Create a new quote.
      this.setUp();
      Integer quoteNo = this.getQuoteNo();
      
      // Create a new temp memory object for reloading pages.
      if (this.getQuoteTempMemoryObject() == null) {
        this.setQuoteTempMemoryObject();
      }

      if (this.getQuoteNo() == null) {       
        quoteNo = this.getInsuranceMgr().startQuote(new DateTime(), WebInsQuote.TYPE_MOTOR);
        this.getInsuranceMgr().setQuoteStatus(quoteNo, WebInsQuote.NEW);
        this.setQuoteNo(quoteNo);
                
        // store agent portal fields if present in session
        if (this.getPersistedAgentCode() != null) {
          this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.AGENT_CODE, this.getPersistedAgentCode());
        }
        if (this.getPersistedAgentUser() != null) {
          this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.AGENT_USER, this.getPersistedAgentUser());
        }
        
        // store promo code, if any
        String pCode = this.getPromoCode();
        if (pCode != null && !pCode.isEmpty()) {
        	this.getInsuranceMgr().setQuoteDetail(quoteNo, InsuranceBase.FIELD_PROMO_CODE, pCode);
        }
        
        this.getInsuranceMgr().setQuoteDetail(quoteNo, InsuranceBase.FIELD_USER_AGENT, this.getHttpRequest().getHeader("User-Agent"));
        String originatingIp = this.extractOriginatingIp();
        this.getInsuranceMgr().setQuoteDetail(quoteNo, InsuranceBase.FIELD_USER_IP, originatingIp);
        this.getInsuranceMgr().setQuoteDetail(quoteNo, InsuranceBase.FIELD_USER_COUNTRY, this.extractOriginatingCountry(originatingIp));
        this.getInsuranceMgr().setQuoteDetail(quoteNo, InsuranceBase.FIELD_USER_TRAFFIC_SRC, this.getTrafficSource());
        this.getInsuranceMgr().setQuoteDetail(quoteNo, InsuranceBase.FIELD_USER_SITE_SRC, this.getSiteSource()); // set via cookie
        this.getInsuranceMgr().setQuoteDetail(quoteNo, InsuranceBase.FIELD_USER_SCREEN_WIDTH, this.getScreenWidth()); // set via cookie
        this.getInsuranceMgr().setQuoteDetail(quoteNo, InsuranceBase.FIELD_USER_COOKIES, Boolean.toString((this.getScreenWidth() != null)));
      }
      
      // record user's session
      String sessionId = recordSession(null, RactActionSupport.CONTEXT_VEHICLE_QUOTE);
      this.getInsuranceMgr().setQuoteDetail(quoteNo, InsuranceBase.FIELD_SESSION_ID, sessionId);

      // store the quote type
      this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.WEB_QUOTE_TYPE, WebInsQuoteDetail.WEB_QUOTE_TYPE_MOTOR);
      
      // Set secure quote number. This is the number to be displayed for customers.
      setSecureQuoteNumber(this.getInsuranceMgr().getQuote(quoteNo).getSecuredQuoteNo());
      this.getInsuranceMgr().setQuoteDetail(quoteNo, "displayQuoteNumber", this.getSecureQuoteNumber());
      
      // Set applyPip
      if (this.getApplyPip().equals("yes")) {
          this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.PIP_DISCOUNT, WebInsQuoteDetail.TRUE);
      } else {
          this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.PIP_DISCOUNT, WebInsQuoteDetail.FALSE);
      }

      // Set fiftyPlus
      if (this.getFiftyPlus().equals("yes")) {
        this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS, WebInsQuoteDetail.TRUE);
      } else {
        this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS, WebInsQuoteDetail.FALSE);
      }

      // Set member number.
      if (member.equals("yes")) {
        try { // Validate for each filed of member card.
          if (!this.validate(this.getMembershipCard1(), VALIDATE_STRING)) {
            this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), this.getDateOfBirth(), memberShipCards);
            return this.validationFailed("Member card no. ", VALIDATE_STRING, false);
          } else if (!this.validate(this.getMembershipCard2(), VALIDATE_STRING)) {
            this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), this.getDateOfBirth(), memberShipCards);
            return this.validationFailed("Member card no.", VALIDATE_STRING, false);
          } else if (!this.validate(this.getMembershipCard3(), VALIDATE_STRING)) {
            this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), this.getDateOfBirth(), memberShipCards);
            return this.validationFailed("Member card no.", VALIDATE_STRING, false);
          } else if (!this.validate(this.getMembershipCard4(), VALIDATE_STRING)) {
            this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), this.getDateOfBirth(), memberShipCards);
            return this.validationFailed("Member card no.", VALIDATE_STRING, false);
          }

          Date dobDate = dateFormat.parse(this.getDateOfBirth());
          DateTime dob = new DateTime(dobDate);
          
          String cardNo = this.getMembershipCard1() + this.getMembershipCard2() + this.getMembershipCard3() + this.getMembershipCard4();

          // Save date of birth and member card for retrieving existing quote later.
          this.getInsuranceMgr().setQuoteDetail(quoteNo, "memberCard", memberShipCards);
          this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), "dateOfBirth", this.getDateOfBirth());

          WebInsClient client = this.getInsuranceMgr().setFromRACTClient(quoteNo, cardNo, dob);

          if (client != null) {
            this.getInsuranceMgr().setQuoteDetail(quoteNo, "memberCardNo", cardNo);
            this.setMember(true);
            this.setClient(client);     // Set client data in the session, so it can be used in later processing
          } else { // Cannot find client based on the provided member card.
            this.setMember(false);
            this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), this.getDateOfBirth(), memberShipCards);
            return this.validationFailed("We couldn't find your details. Please check that the card number and date of birth are correct. If you are still having problems please call us or proceed with your quote as a non-member if you require immediate cover outside business hours.", VALIDATE_CUSTOMIZED, false);
          }
        } catch (Exception e) {
          this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), "", memberShipCards);
          return this.validationFailed("Date of birth", VALIDATE_DATETIME_STRING, false);
        }
      } else { // Not a member.
        this.setMember(false);
        this.getInsuranceMgr().setQuoteDetail(quoteNo, "member", "no");
      }

      this.setNextAction();
      return SUCCESS;
    }
  }

  @Override
  public void validate() {
      
    if (this.getSubmit().equals(BUTTON_BACK)) {
      return;
    }  
      
    try {
            
        if (!this.validate(this.getApplyPip(), VALIDATE_STRING)) {
            this.addFieldError("applyPip", "Please answer this question");
        }
      if (!this.validate(this.getFiftyPlus(), VALIDATE_STRING)) {
        this.addFieldError("fiftyPlus", "Please answer this question");
      }
      if (!this.validate(this.getMember(), VALIDATE_STRING)) {
        this.addFieldError("member", "Please answer this question");
      } else if (this.getMember().equals("yes")) {
        if (!this.validate(this.getMembershipCard1(), VALIDATE_STRING)
                || !this.validate(this.getMembershipCard2(), VALIDATE_STRING)
                || !this.validate(this.getMembershipCard3(), VALIDATE_STRING)
                || !this.validate(this.getMembershipCard4(), VALIDATE_STRING)) {
          this.addFieldError("membershipCard1", "Please enter a valid card number");
        }

        try {
          Date dobDate = dateFormat.parse(this.getDateOfBirth());
          DateTime dob = new DateTime(dobDate);
        } catch (Exception e) {
          this.addFieldError("dateOfBirth", "Please enter a valid date of birth");
        }
      }

      if (!this.getFieldErrors().isEmpty()) {
        this.loadPage2(this.getApplyPip(), this.getFiftyPlus(), this.getMember(), this.getDateOfBirth(), this.getMembershipCards());
      }
    } catch (Exception e) {}
  }
  
  /**
   * @return the applyPip
   */
  public String getApplyPip() {
      return applyPip;
  }
  
  /**
   * @param applyPip the applyPip to set
   */
  public void setApplyPip(String applyPip) {
      this.applyPip = applyPip;
  }

  /**
   * @return the fiftyPlus
   */
  public String getFiftyPlus() {
    return fiftyPlus;
  }

  /**
   * @param fiftyPlus the fiftyPlus to set
   */
  public void setFiftyPlus(String fiftyPlus) {
    this.fiftyPlus = fiftyPlus;
  }

  /**
   * @return the member
   */
  public String getMember() {
    return member;
  }

  /**
   * @param member the member to set
   */
  public void setMember(String member) {
    this.member = member;
  }

  /**
   * @return the dateOfBirth
   */
  public String getDateOfBirth() {
    return dateOfBirth;
  }

  /**
   * @param dateOfBirth the dateOfBirth to set
   */
  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  /**
   * @return the membershipCard1
   */
  public String getMembershipCard1() {
    return membershipCard1;
  }

  /**
   * @param membershipCard1 the membershipCard1 to set
   */
  public void setMembershipCard1(String membershipCard1) {
    this.membershipCard1 = membershipCard1;
  }

  /**
   * @return the membershipCard2
   */
  public String getMembershipCard2() {
    return membershipCard2;
  }

  /**
   * @param membershipCard2 the membershipCard2 to set
   */
  public void setMembershipCard2(String membershipCard2) {
    this.membershipCard2 = membershipCard2;
  }

  /**
   * @return the membershipCard3
   */
  public String getMembershipCard3() {
    return membershipCard3;
  }

  /**
   * @param membershipCard3 the membershipCard3 to set
   */
  public void setMembershipCard3(String membershipCard3) {
    this.membershipCard3 = membershipCard3;
  }

  /**
   * @return the membershipCard4
   */
  public String getMembershipCard4() {
    return membershipCard4;
  }

  /**
   * @param membershipCard4 the membershipCar4 to set
   */
  public void setMembershipCard4(String membershipCard4) {
    this.membershipCard4 = membershipCard4;
  }

  public String getMembershipCards() {
    return this.getMembershipCard1() +","+ this.getMembershipCard2() +","+  this.getMembershipCard3() +","+ this.getMembershipCard4();
  }
}
