package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.Map;

/**
 *
 * @author gnewton
 */
public class ExitBase extends ActionBase implements PersistanceAware {

  private Map<String,String> reasonOptions;

  /**
   * @return the reasonOptions
   */
  public Map<String,String> getReasonOptions() {
    if (this.reasonOptions == null) {
      this.reasonOptions = this.getOptionService().getReasonOptions();
    }
    return reasonOptions;
  }

  /**
   * @param reasonOptions the reasonOptions to set
   */
  public void setReasonOptions(Map<String,String> reasonOptions) {
    this.reasonOptions = reasonOptions;
  }


}
