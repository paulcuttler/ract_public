package futuremedium.client.ract.secure.actions.insurance.home;


import java.util.*;


import com.ract.client.ClientTitleVO;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;


/**
 * Common elements for Policyholder Details form and processing.
 * @author gnewton
 */
public class PolicyholderDetailsBase extends PolicySupport {
    private static final long serialVersionUID = -2328156106811847702L;

static private final String TITLE_OPTIONS = "TITLE_OPTIONS";

  private Map<String,String> existingPolicyHolders;
  private List<WebInsClient> newClients;
  private Map<String,String> genderList;
  private List<String> areaCodeList;
  private Collection<ClientTitleVO> titleOptions;
  private boolean additionalPolicyHolderForm;
  private Boolean fiftyPlus;


  /**
   * @return the existingPolicyHolders
   */
  public Map<String,String> getExistingPolicyHolders() {
    if (this.existingPolicyHolders == null) {
      List<WebInsClient> clientList = this.getHomeService().retrieveClientOptions(this);
      existingPolicyHolders = new LinkedHashMap<String,String>();

      for (WebInsClient client : clientList) {
        existingPolicyHolders.put(client.getSelectionDescription(), client.getSelectionDescription());
      }



      // add additional option
      existingPolicyHolders.put("-1", "New policy holders.");
    }
    return existingPolicyHolders;
  }

    public String getSelectedPolicyHolder() {
        List<WebInsClient> clients = null;
        try {
            clients = this.getInsuranceMgr().getClients(this.getQuoteNo());
        } catch (Exception e) {
            Config.logger.error(Config.key + ": unable to retrieve clients for Home Quote #" + this.getQuoteNo(), e);
            return null;
        }

        if (null != clients) {
            for (WebInsClient client : clients) {
                if (!client.isOwner()) {
                    continue;
                }
                if (null == client.getRactClientNo()) {
                    return "-1";
                }
                return client.getDisplayName();
            }
        }

        return null;
    }
  
  /**
   * @param existingPolicyHolders the existingPolicyHolders to set
   */
  public void setExistingPolicyHolders(LinkedHashMap<String,String> existingPolicyHolders) {
      this.existingPolicyHolders = existingPolicyHolders;
  }

  /**
   * @return the genderList
   */
  public Map<String,String> getGenderList() {
    if (this.genderList == null) {
      this.genderList = this.getOptionService().getGenderOptions();
    }
    return genderList;
  }

  /**
   * @param genderList the genderList to set
   */
  public void setGenderList(Map<String,String> genderList) {
    this.genderList = genderList;
  }

  /**
   * @return the areaCodeList
   */
  public List<String> getAreaCodeList() {
    if (this.areaCodeList == null) {
      this.areaCodeList = this.getOptionService().getAreaCodeOptions();
    }
    return areaCodeList;
  }

  /**
   * @param areaCodeList the areaCodeList to set
   */
  public void setAreaCodeList(List<String> areaCodeList) {
    this.areaCodeList = areaCodeList;
  }

  /**
   * @return the newClients
   */
  public List<WebInsClient> getNewClients() {
    if (this.newClients == null) {
      this.newClients = this.getHomeService().retrieveNewClients(this);
    }
    return newClients;
  }

  /**
   * @param newClients the newClients to set
   */
  public void setNewClients(List<WebInsClient> newClients) {
    this.newClients = newClients;
  }

  /**
   * @return the additionalPolicyHolderForm
   */
  public boolean isAdditionalPolicyHolderForm() {
    return additionalPolicyHolderForm;
  }

  /**
   * @param additionalPolicyHolderForm the additionalPolicyHolderForm to set
   */
  public void setAdditionalPolicyHolderForm(boolean additionalPolicyHolderForm) {
    this.additionalPolicyHolderForm = additionalPolicyHolderForm;
  }

  /**
   * Returned configured max number of policy holders.
   */
  public int getMaxAdditionalPolicyHolders() {
    try {
      return Integer.parseInt(Config.configManager.getProperty("insurance.home.maxAdditionalPolicyHolders"));
    } catch (Exception e) {
      return 3;
    }
  }

  /**
   * @return the titleList
   */

  @SuppressWarnings("unchecked")
  public Collection<ClientTitleVO> getTitleOptions() {

    if (this.session != null) {
      if (this.session.containsKey(TITLE_OPTIONS)) { // retrieve from session
        this.titleOptions = (List<ClientTitleVO>) this.session.get(TITLE_OPTIONS);
      } else { // initialise in session
        this.titleOptions = this.getOptionService().getTitleOptions();
        this.session.put(TITLE_OPTIONS, this.titleOptions);
      }
    }
    return titleOptions;
  }


  /**
   * @param titleOptions the titleList to set
   */
  public void setTitleOptions(List<ClientTitleVO> titleOptions) {
    this.titleOptions = titleOptions;
  }


  /**
   * @return the fiftyPlus
   */
  public Boolean getFiftyPlus() {
    if (this.fiftyPlus == null) {
      String fifty = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS);
      if (fifty == null) {
        this.fiftyPlus = null;
      } else {
        this.fiftyPlus = Boolean.parseBoolean(fifty);
      }
    }
    return this.fiftyPlus;
  }
}
