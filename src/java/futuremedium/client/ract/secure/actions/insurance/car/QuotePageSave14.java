package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.Collection;

import com.ract.common.StreetSuburbVO;
import com.ract.util.DateTime;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;

/**
 * Load additional policy holder page. This page will only be loaded if user chooses to create new policy holders. For exisitng policy holder, this page will be no shown.
 * 
 * @author xmfu
 */
public class QuotePageSave14 extends QuotePageSave {
    private static final long serialVersionUID = -7323343523836739758L;

    @Override
    public String execute() throws Exception {
        this.setUp();

        // Go back to previous page.
        if (this.getSubmit().equals(BUTTON_BACK)) {
            this.setNewPolicyInSession("true");
            this.setNextAction();
            return SUCCESS;
        }

        // Set flag to indicate, user has chosen to create new policy holder.
        this.setNewPolicyInSession("true");

        // Validation
        String additionalPolicyHolder = this.getParameter("additionalPolicyHolder");
        WebInsClient client = null;
        if (additionalPolicyHolder != null && additionalPolicyHolder.equals("yes")) {
            client = this.getAdditionalPolicyHolder();

            if (client == null) {
                client = this.insuranceMgr.createInsuranceClient(this.getQuoteNo());
                client.setDriver(false);
                client.setOwner(true);
            }

            if (!this.validate(this.getParameter("additionalTitle"), VALIDATE_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("PolicyHolder Title", VALIDATE_STRING, false);
            } else {
                client.setTitle(this.getParameter("additionalTitle"));
                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("additionalFirstName"), VALIDATE_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("First Name", VALIDATE_STRING, false);
            } else {
                client.setGivenNames(this.getParameter("additionalFirstName"));
                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("additionalSurName"), VALIDATE_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("SurName", VALIDATE_STRING, false);
            } else {
                client.setSurname(this.getParameter("additionalSurName"));
                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("additionalDob"), VALIDATE_DATETIME_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("Date of birth", VALIDATE_DATETIME_STRING, false);
            } else {
                DateTime dateTime = new DateTime(this.getParameter("additionalDob"));
                client.setBirthDate(dateTime);
                this.insuranceMgr.setClient(client);
            }

            client.setGender(this.getParameter("additionalSex"));
            this.insuranceMgr.setClient(client);

            if (!this.validate(this.getParameter("residentialPostcode"), VALIDATE_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("Residential Postcode", VALIDATE_STRING, false);
            } else {
                client.setResiPostcode(this.getParameter("residentialPostcode"));
                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("residentialSuburb"), VALIDATE_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("Residential Suburb", VALIDATE_STRING, false);
            } else {
                client.setResiSuburb(this.getParameter("residentialSuburb"));
                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("residentialStreetSuburbId"), VALIDATE_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("Residential Street", VALIDATE_STRING, false);
            } else {
                client.setResiStsubid(Integer.valueOf(this.getParameter("residentialStreetSuburbId")));
                this.insuranceMgr.setClient(client);
            }

            Collection<StreetSuburbVO> ssList = addressMgr.getStreetSuburbsBySuburb(client.getResiSuburb(), client.getResiPostcode());
            for (StreetSuburbVO streetSuburbVO : ssList) {
                if (!streetSuburbVO.getStreet().equals("") && null != client.getResiStsubid() && client.getResiStsubid().equals(streetSuburbVO.getStreetSuburbID())) {
                    client.setResiStreet(streetSuburbVO.getStreet());
                    client.setResiState(streetSuburbVO.getState());
                    this.insuranceMgr.setClient(client);
                    break;
                }
            }

            String streetNumber = this.getParameter("residentialStreetNumber");

            if (streetNumber == null || streetNumber.equals("")) {
                this.loadPage14(true);
                return this.validationFailed("Residential Street number", VALIDATE_STRING, false);
            } else {
                client.setResiStreetChar(streetNumber);
                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("additionalPostcode"), VALIDATE_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("Postal Postcode", VALIDATE_STRING, false);
            } else {
                client.setPostPostcode(this.getParameter("additionalPostcode"));
                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("additionalSuburb"), VALIDATE_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("Postal Suburb", VALIDATE_STRING, false);
            } else {
                client.setPostSuburb(this.getParameter("additionalSuburb"));
                this.insuranceMgr.setClient(client);
            }

            // Save po box and street number
            String poBox = this.getParameter("additionalPobox");

            streetNumber = this.getParameter("additionalStreetNumber");

            if (poBox == null || poBox.equals("")) {
                client.setPostProperty("");
                this.insuranceMgr.setClient(client);

                if (streetNumber == null || streetNumber.equals("")) {
                    this.loadPage14(true);
                    return this.validationFailed("You must fill in either Street Name and Number or Post Box", VALIDATE_CUSTOMIZED, false);
                }

                if (!this.validate(this.getParameter("additionalStreetSuburbId"), VALIDATE_STRING)) {
                    this.loadPage14(true);
                    return this.validationFailed("You must fill in either Street Name and Number or Post Box", VALIDATE_CUSTOMIZED, false);
                } else {
                    client.setPostStsubid(Integer.valueOf(this.getParameter("additionalStreetSuburbId")));
                    this.insuranceMgr.setClient(client);
                }

                ssList = addressMgr.getStreetSuburbsBySuburb(client.getPostSuburb(), client.getPostPostcode());
                for (StreetSuburbVO streetSuburbVO : ssList) {
                    if (!streetSuburbVO.getStreet().equals("") && null != client.getPostStsubid() && client.getPostStsubid().equals(streetSuburbVO.getStreetSuburbID())) {
                        client.setPostStreet(streetSuburbVO.getStreet());
                        client.setPostState(streetSuburbVO.getState());
                        client.setPostStsubid(streetSuburbVO.getStreetSuburbID());
                        this.insuranceMgr.setClient(client);
                        break;
                    }
                }

                client.setPostStreetChar(streetNumber);
                this.insuranceMgr.setClient(client);
            } else {
                client.setPostStreetChar("");
                this.insuranceMgr.setClient(client);

                ssList = addressMgr.getStreetSuburbsByPostcode(client.getPostPostcode());
                for (StreetSuburbVO streetSuburbVO : ssList) {
                    if (client.getPostSuburb().equals(streetSuburbVO.getSuburb())) {
                        client.setPostStsubid(streetSuburbVO.getStreetSuburbID());
                    }
                }

                client.setPostProperty(poBox);
                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("additionalPhoneNumber"), VALIDATE_STRING)) {
            } else {
                String phoneType = this.getParameter("additionalPhoneType");

                if (phoneType.equals("home")) {
                    client.setHomePhone(this.getParameter("additionalPhoneNumber"));
                } else if (phoneType.equals("work")) {
                    client.setWorkPhone(this.getParameter("additionalPhoneNumber"));
                } else if (phoneType.equals("mobile")) {
                    client.setMobilePhone(this.getParameter("additionalPhoneNumber"));
                }

                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("additionalEmailAddress"), VALIDATE_EMAIL)) {
                this.loadPage14(true);
                return this.validationFailed("Email", VALIDATE_EMAIL, false);
            } else {
                client.setEmailAddress(this.getParameter("additionalEmailAddress"));
                this.insuranceMgr.setClient(client);
            }

            if (!this.validate(this.getParameter("additionalContact"), VALIDATE_STRING)) {
                this.loadPage14(true);
                return this.validationFailed("Preferred Contact", VALIDATE_STRING, false);
            } else {
                if (this.getParameter("additionalContact").equals("yes")) {
                    client.setPreferredAddress(true);
                } else {
                    client.setPreferredAddress(false);
                }
                this.insuranceMgr.setClient(client);
            }

            client = this.getInsuranceService().attemptQasRiskAddress(this.getQuoteNo(), client);
            this.insuranceMgr.setClient(client);
        }

        // Check for criminal conviction, if yes, stop the quote.
        String criminalConviction = this.getParameter("criminalConviction");

        if (criminalConviction == null || criminalConviction.isEmpty()) {
            this.loadPage14(false);
            String question = this.getSubQuestionById("q29", "sub15").getTitle();
            return this.validationFailed("You must answer \"" + question + "\"", VALIDATE_CUSTOMIZED, false);
        }

        this.insuranceMgr.setQuoteDetail(this.getQuoteNo(), "criminalConvicted", criminalConviction);

        if (criminalConviction.equals("yes")) {
            return this.messagePage(MESSAGE_ADDITIONALCRIMINAL);
        }

        if (client != null) {
            if (insuranceMgr.getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS).equals(WebInsQuoteDetail.TRUE)) {
                // check additionalPolicyHolder is over 50 if user has answered 'yes' to 50+ question
                if (!isOlderThan(client.getBirthDate(), 50)) {
                    return this.messagePage(MESSAGE_FIFTYPLUS);
                }
            }
        }

        this.setNextAction();
        return SUCCESS;
    }

    @Override
    public void validate() {

        if (this.getSubmit().equals(BUTTON_BACK)) {
            return;
        }

        try {

            this.setNewPolicyInSession("true");

            // Validation
            String additionalPolicyHolder = this.getParameter("additionalPolicyHolder");
            WebInsClient client = null;
            if (additionalPolicyHolder != null && additionalPolicyHolder.equals("yes")) {
                client = this.getAdditionalPolicyHolder();

                if (client == null) {
                    client = this.getInsuranceMgr().createInsuranceClient(this.getQuoteNo());
                    client.setDriver(false);
                    client.setOwner(true);
                }

                if (!this.validate(this.getParameter("additionalTitle"), VALIDATE_STRING)) {
                    this.addFieldError("additionalTitle", "Please enter a title");
                } else {
                    client.setTitle(this.getParameter("additionalTitle"));
                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("additionalFirstName"), VALIDATE_STRING)) {
                    this.addFieldError("additionalFirstName", "Please enter a first name");
                } else {
                    client.setGivenNames(this.getParameter("additionalFirstName"));
                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("additionalSurName"), VALIDATE_STRING)) {
                    this.addFieldError("additionalSurName", "Please enter a surname");
                } else {
                    client.setSurname(this.getParameter("additionalSurName"));
                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("additionalDob"), VALIDATE_DATETIME_STRING)) {
                    this.addFieldError("additionalDob", "Please enter a valid date of birth");
                } else {
                    DateTime dateTime = new DateTime(this.getParameter("additionalDob"));
                    client.setBirthDate(dateTime);
                    this.getInsuranceMgr().setClient(client);
                }

                if (this.getParameter("additionalSex") == null || this.getParameter("additionalSex").isEmpty()) {
                    this.addFieldError("additionalSex", "Please select an option");
                } else {
                    client.setGender(this.getParameter("additionalSex"));
                }

                this.getInsuranceMgr().setClient(client);

                if (!this.validate(this.getParameter("residentialPostcode"), VALIDATE_STRING)) {
                    this.addFieldError("residentialPostcode", "Please enter a valid postcode");
                } else {
                    client.setResiPostcode(this.getParameter("residentialPostcode"));
                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("residentialSuburb"), VALIDATE_STRING)) {
                    this.addFieldError("residentialSuburb", "Please enter a valid subrub");
                } else {
                    client.setResiSuburb(this.getParameter("residentialSuburb"));
                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("residentialStreetSuburbId"), VALIDATE_STRING)) {
                    this.loadPage14(true);
                    this.addFieldError("residentialStreetSuburbId", "Please enter a valid street");
                } else {
                    client.setResiStsubid(Integer.valueOf(this.getParameter("residentialStreetSuburbId")));
                    this.getInsuranceMgr().setClient(client);
                }

                Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsBySuburb(client.getResiSuburb(), client.getResiPostcode());

                for (StreetSuburbVO streetSuburbVO : ssList) {
                    if (!streetSuburbVO.getStreet().equals("") && null != client.getPostStsubid() && client.getPostStsubid().equals(streetSuburbVO.getStreetSuburbID())) {
                        client.setResiStreet(streetSuburbVO.getStreet());
                        client.setResiState(streetSuburbVO.getState());
                        this.getInsuranceMgr().setClient(client);
                        break;
                    }
                }

                String streetNumber = this.getParameter("residentialStreetNumber");

                if (streetNumber == null || streetNumber.equals("")) {
                    this.addFieldError("residentialStreetNumber", "Please enter a valid street number");
                } else {
                    client.setResiStreetChar(streetNumber);
                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("additionalPostcode"), VALIDATE_STRING)) {
                    this.addFieldError("additionalPostcode", "Please enter a valid postcode");
                } else {
                    client.setPostPostcode(this.getParameter("additionalPostcode"));
                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("additionalSuburb"), VALIDATE_STRING)) {
                    this.addFieldError("additionalPostcode", "Please enter a valid suburb");
                } else {
                    client.setPostSuburb(this.getParameter("additionalSuburb"));
                    this.getInsuranceMgr().setClient(client);
                }

                // Save po box and street number
                String poBox = this.getParameter("additionalPobox");

                streetNumber = this.getParameter("additionalStreetNumber");

                if (poBox == null || poBox.equals("")) {
                    client.setPostProperty("");
                    this.getInsuranceMgr().setClient(client);

                    if (streetNumber == null || streetNumber.equals("")) {
                        this.addFieldError("additionalStreetNumber", "Please enter in either Street Name and Number or Post Box");
                    }

                    if (!this.validate(this.getParameter("additionalStreetSuburbId"), VALIDATE_STRING)) {
                        this.addFieldError("additionalStreetSuburbId", "Please enter in either Street Name and Number or Post Box");
                    } else {
                        client.setPostStsubid(Integer.valueOf(this.getParameter("additionalStreetSuburbId")));
                        this.getInsuranceMgr().setClient(client);
                    }

                    ssList = this.getAddressMgr().getStreetSuburbsBySuburb(client.getPostSuburb(), client.getPostPostcode());

                    for (StreetSuburbVO streetSuburbVO : ssList) {
                        if (!streetSuburbVO.getStreet().equals("") && null != client.getPostStsubid() && client.getPostStsubid().equals(streetSuburbVO.getStreetSuburbID())) {
                            client.setPostStreet(streetSuburbVO.getStreet());
                            client.setPostState(streetSuburbVO.getState());
                            client.setPostStsubid(streetSuburbVO.getStreetSuburbID());
                            this.getInsuranceMgr().setClient(client);
                            break;
                        }
                    }

                    client.setPostStreetChar(streetNumber);
                    this.getInsuranceMgr().setClient(client);
                } else {
                    client.setPostStreetChar("");
                    this.getInsuranceMgr().setClient(client);

                    ssList = this.getAddressMgr().getStreetSuburbsByPostcode(client.getPostPostcode());

                    for (StreetSuburbVO streetSuburbVO : ssList) {
                        if (client.getPostSuburb().equals(streetSuburbVO.getSuburb())) {
                            client.setPostStsubid(streetSuburbVO.getStreetSuburbID());
                        }
                    }

                    client.setPostProperty(poBox);
                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("additionalPhoneNumber"), VALIDATE_STRING)) {
                } else {
                    String phoneType = this.getParameter("additionalPhoneType");

                    if (phoneType.equals("home")) {
                        client.setHomePhone(this.getParameter("additionalPhoneNumber"));
                    } else if (phoneType.equals("work")) {
                        client.setWorkPhone(this.getParameter("additionalPhoneNumber"));
                    } else if (phoneType.equals("mobile")) {
                        client.setMobilePhone(this.getParameter("additionalPhoneNumber"));
                    }

                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("additionalEmailAddress"), VALIDATE_EMAIL)) {
                    this.addFieldError("additionalEmailAddress", "Please enter a valid email");
                } else {
                    client.setEmailAddress(this.getParameter("additionalEmailAddress"));
                    this.getInsuranceMgr().setClient(client);
                }

                if (!this.validate(this.getParameter("additionalContact"), VALIDATE_STRING)) {
                    this.addFieldError("additionalContact", "Please enter select an option");
                } else {
                    if (this.getParameter("additionalContact").equals("yes")) {
                        client.setPreferredAddress(true);
                    } else {
                        client.setPreferredAddress(false);
                    }
                    this.getInsuranceMgr().setClient(client);
                }
            }

            String criminalConviction = this.getParameter("criminalConviction");
            if (criminalConviction == null || criminalConviction.isEmpty()) {
                this.addFieldError("criminalConviction", "Please enter select an option");
            }

            if (!this.getFieldErrors().isEmpty()) {
                if (additionalPolicyHolder != null && additionalPolicyHolder.equals("yes")) {
                    this.loadPage14(true);
                } else {
                    this.loadPage14(false);
                }

            }

        } catch (Exception e) {
            Config.logger.debug(Config.key + ": loadpage14 failed on validation: ", e);
        }
    }
}