package futuremedium.client.ract.secure.actions.insurance.home;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;

import com.ract.common.SystemException;
import com.ract.util.DateTime;
import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;

/**
 * The form on which the user views the Quote Summary.
 * @author gnewton
 */
public class QuoteForm extends PremiumOptionsForm implements QuoteFinalisedAware {
    private static final long serialVersionUID = -6573246380279584091L;
    
  private String yearConstructed;
  private String address;
  private String buildingType;
  private String wallConstruction;
  private String roofConstruction;
  private Boolean smokeAlarm;
  private String securityAlarm;
  private String securityLock;
  private String occupancy;
  private DateTime dob;
  private String contentsSumInsured;
  
  private BigDecimal buildingExcessCost;
  private BigDecimal contentsExcessCost;
  private BigDecimal investorExcessCost;

  @Override
  public String execute() throws Exception {
    // do nothing, but effectively cancel out the execute method from PremiumOptionsForm.
    return SUCCESS;
  }

  /**
   * Retrieve the totalAnnualPremium from the DB, rather than calculating again.
   */
  @Override
  public BigDecimal getTotalAnnualPremium() {
    if (this.totalAnnualPremium == null) {
      this.totalAnnualPremium = this.getHomeService().retrieveTotalAnnualPremium(this);
    }
    return totalAnnualPremium;
  }

  /**
   * Retrieve the totalMonthlyPremium from the DB, rather than calculating again.
   */
  @Override
  public BigDecimal getTotalMonthlyPremium() {
    if (this.totalMonthlyPremium == null) {
      this.totalMonthlyPremium = this.getHomeService().retrieveTotalMonthlyPremium(this);
    }
    return totalMonthlyPremium;
  }

  /**
   * @return the yearConstructed
   */
  public String getYearConstructed() {
    if (this.yearConstructed == null) {
      this.yearConstructed = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.YEAR_CONST);
    }
    return yearConstructed;
  }

  /**
   * @param yearConstructed the yearConstructed to set
   */
  public void setYearConstructed(String yearConstructed) {
    this.yearConstructed = yearConstructed;
  }

  /**
   * @return the address
   */
  public String getAddress() {
    if (this.address == null) {
      StringBuilder sb = new StringBuilder("");
      sb.append(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_NO));
      sb.append(" ");
      sb.append(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_STREET));
      sb.append(", ");
      sb.append(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_SUBURB));
      sb.append(", ");
      sb.append(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SITUATION_POSTCODE));
      
      this.address = sb.toString();
    }
    return address;
  }

  /**
   * @param address the address to set
   */
  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * @return the buildingType
   */
  public String getBuildingType() throws Exception {
    if (this.buildingType == null) {
      List<InRfDet> options = this.getOptionService().getBuildingTypeOptions(this);
      String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.BUILDING_TYPE);
      this.buildingType = this.getLookup(options).get(savedValue).getrDescription();
    }
    return buildingType;
  }

  /**
   * @param buildingType the buildingType to set
   */
  public void setBuildingType(String buildingType) {
    this.buildingType = buildingType;
  }

  /**
   * @return the wallConstruction
   */
  public String getWallConstruction() throws Exception {
    if (this.wallConstruction == null) {
      List<InRfDet> options = this.getOptionService().getWallConstructionOptions(this);
      String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.CONSTRUCTION);
      this.wallConstruction = this.getLookup(options).get(savedValue).getrDescription();
    }
    return wallConstruction;
  }

  /**
   * @param wallConstruction the wallConstruction to set
   */
  public void setWallConstruction(String wallConstruction) {
    this.wallConstruction = wallConstruction;
  }

  /**
   * @return the roofConstruction
   */
  public String getRoofConstruction() throws Exception {
    if (this.roofConstruction == null) {
      List<InRfDet> options = this.getOptionService().getRoofConstructionOptions(this);
      String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ROOF);
      this.roofConstruction = this.getLookup(options).get(savedValue).getrDescription();
    }
    return roofConstruction;
  }

  /**
   * @param roofConstruction the roofConstruction to set
   */
  public void setRoofConstruction(String roofConstruction) {
    this.roofConstruction = roofConstruction;
  }

  /**
   * @return the smokeAlarm
   */
  public Boolean isSmokeAlarm() {
    if (this.smokeAlarm == null) {
      this.smokeAlarm = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SMOKE_ALARM));
    }
    return smokeAlarm;
  }

  /**
   * @param smokeAlarm the smokeAlarm to set
   */
  public void setSmokeAlarm(Boolean smokeAlarm) {
    this.smokeAlarm = smokeAlarm;
  }

  /**
   * @return the occupancy
   */
  public String getOccupancy() throws Exception {
    if (this.occupancy == null) {
      List<InRfDet> options = this.getOptionService().getOccupancyOptions(this);
      String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.OCCUPANCY);
      this.occupancy = this.getLookup(options).get(savedValue).getrDescription();
    }
    return occupancy;
  }

  /**
   * @param occupancy the occupancy to set
   */
  public void setOccupancy(String occupancy) {
    this.occupancy = occupancy;
  }

  /**
   * @return the dob
   */
  public DateTime getDob() {
    if (this.dob == null) {
      String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_OLDEST_DOB);
      try {
        this.dob = new DateTime(savedValue);
      } catch (Exception e) {
        Config.logger.error(Config.key + ": unable to parse Oldest DOB: " + savedValue + " for Home Quote #" + this.getQuoteNo(), e);
      }
    }
    return dob;
  }

  /**
   * @param dob the dob to set
   */
  public void setDob(DateTime dob) {
    this.dob = dob;
  }
  
  /**
   * @return the contentsSumInsured
   */
  public String getContentsSumInsured() {
    if (this.contentsSumInsured == null) {
      this.contentsSumInsured = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.CNTS_SUM_INSURED);
    }
    return contentsSumInsured;
  }

  /**
   * @param contentsSumInsured the contentsSumInsured to set
   */
  public void setContentsSumInsured(String contentsSumInsured) {
    this.contentsSumInsured = contentsSumInsured;
  }

  /**
   * @return the buildingSumInsured
   */
  @Override
  public Integer getBuildingSumInsured() {
    if (this.buildingSumInsured == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.BLDG_SUM_INSURED);
      if (value != null) {
        this.buildingSumInsured = Integer.parseInt(value);
      }
    }
    return buildingSumInsured;
  }

  /**
   * @return the investorSumInsured
   */
  @Override
  public Integer getInvestorSumInsured() {
    if (this.investorSumInsured == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.INV_SUM_INSURED);
      if (value != null) {
        this.investorSumInsured = Integer.parseInt(value);
      }
    }
    return investorSumInsured;
  }

    public String getSecurityAlarm() throws Exception {
        if (this.securityAlarm == null) {
            List<InRfDet> options = this.getOptionService().getSecurityAlarmOptions(this);
            String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SECURITY_ALARM);
            this.securityAlarm = this.getLookup(options).get(savedValue).getrDescription();
        }
        return securityAlarm;
    }

    public void setSecurityAlarm(String securityAlarm) {
        this.securityAlarm = securityAlarm;
    }

    public String getSecurityLock() throws Exception {
        if (this.securityLock == null) {
            List<InRfDet> options = this.getOptionService().getSecurityLockOptions(this);
            String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SECURITY_LOCK);
            this.securityLock = this.getLookup(options).get(savedValue).getrDescription();

            String securityOther = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_SECURITY_OTHER);
            if (securityOther != null && !securityOther.isEmpty()) {
                this.securityLock += " (" + securityOther + ")";
            }
        }
        return securityLock;
    }

    public void setSecurityLock(String securityLocks) {
        this.securityLock = securityLocks;
    }
    
    public BigDecimal getBuildingExcessCost() throws Exception {
        if(null == buildingExcessCost) {
            String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.BLDG_EXCESS);
            InRfDet selectedBuildingExcess = this.getLookup(this.getBuildingExcessOptions()).get(savedValue);
            
            DecimalFormat format = new DecimalFormat();
            format.setParseBigDecimal(true);
            try {
                buildingExcessCost = (BigDecimal) format.parse(selectedBuildingExcess.getrDescription());
            } catch (ParseException e) {
                throw new SystemException("Failed to convert building excess to decimal", e);
            }
        }
        return buildingExcessCost;
    }
    
    public void setBuildingExcessCost(BigDecimal buildingExcessCost) {
        this.buildingExcessCost = buildingExcessCost;
    }
    
    public BigDecimal getContentsExcessCost() throws Exception {
        if(null == contentsExcessCost) {
            String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.CNTS_EXCESS);
            InRfDet selectedContentsExcess = this.getLookup(this.getContentsExcessOptions()).get(savedValue);
            
            DecimalFormat format = new DecimalFormat();
            format.setParseBigDecimal(true);
            try {
                contentsExcessCost = (BigDecimal) format.parse(selectedContentsExcess.getrDescription());
            } catch (ParseException e) {
                throw new SystemException("Failed to convert contents excess to decimal", e);
            }
        }
        return contentsExcessCost;
    }
    
    public void setContentsExcessCost(BigDecimal contentsExcessCost) {
        this.contentsExcessCost = contentsExcessCost;
    }
    
    public BigDecimal getInvestorExcessCost() throws Exception {
        if(null == investorExcessCost) {
            String savedValue = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.INV_EXCESS);
            InRfDet selectedInvestorExcess = this.getLookup(this.getInvestorExcessOptions()).get(savedValue);
            
            DecimalFormat format = new DecimalFormat();
            format.setParseBigDecimal(true);
            try {
                investorExcessCost = (BigDecimal) format.parse(selectedInvestorExcess.getrDescription());
            } catch (ParseException e) {
                throw new SystemException("Failed to convert investor excess to decimal", e);
            }
        }
        return investorExcessCost;
    }
    
    public void setInvestorExcessCost(BigDecimal investorExcessCost) {
        this.investorExcessCost = investorExcessCost;
    }
}
