package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * @author gnewton
 */
public class NewQuoteForm extends ActionBase {

  @Override
  public String getQuoteType() {
    // don't populate quote type selection
    return null;
  }
}