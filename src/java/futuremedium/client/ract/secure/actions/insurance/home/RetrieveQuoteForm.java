package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * Display form to retrieve a saved quote. Allow savedQuoteNo to be provided.
 * 
 * @author gnewton
 */
public class RetrieveQuoteForm extends ActionBase {
    private static final long serialVersionUID = -3357330705805118562L;
    private String savedQuoteNo;
    private String savedQuoteEmail;

    /**
     * @return the savedQuoteNo
     */
    public String getSavedQuoteNo() {
        return savedQuoteNo;
    }

    /**
     * @param savedQuoteNo
     *            the savedQuoteNo to set
     */
    public void setSavedQuoteNo(String savedQuoteNo) {
        this.savedQuoteNo = savedQuoteNo;
    }

    /**
     * @return the savedQuoteEmail
     */
    public String getSavedQuoteEmail() {
        return savedQuoteEmail;
    }

    /**
     * @param savedQuoteEmail
     *            the savedQuoteEmail to set
     */
    public void setSavedQuoteEmail(String savedQuoteEmail) {
        this.savedQuoteEmail = savedQuoteEmail;
    }
}
