package futuremedium.client.ract.secure.actions.insurance.home;

import futuremedium.client.ract.secure.entities.insurance.home.Page;

/**
 *
 * @author gnewton
 */
public class ReminderComplete extends ReminderForm {

  @Override
  public String getPageTitle() {
    return "Reminder setup!";
  }

  public boolean isComplete() {
    return true;
  }

  @Override
  public Page getPage() {
    return this.getHomeService().getPage("HomeReminderForm");
  }

}
