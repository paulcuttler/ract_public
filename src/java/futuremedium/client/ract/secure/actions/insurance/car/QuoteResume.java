package futuremedium.client.ract.secure.actions.insurance.car;

/**
 * Return a user to the correct place in the quote.
 * This is effectively achieved by calling next() when pageId is null.
 * @author gnewton
 */
public class QuoteResume extends QuotePageSave {

  @Override
  public String execute() throws Exception {
    
    this.setActionMessage("Please avoid using the \"Back\" and \"Refresh\" buttons on your browser whilst using this application.");

    return next();
  }
}
