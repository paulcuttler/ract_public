package futuremedium.client.ract.secure.actions.insurance.car;

import com.ract.web.insurance.Premium;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

/**
 * Quote page save class. Display premiums based on the information provided by client.
 * @author xmfu
 */
public class QuotePageSave9 extends QuotePageSave {
    private static final long serialVersionUID = 44445547715844052L;
  private String premiumType;
  private String coverType;

  @Override
    public String execute() throws Exception {
        int quoteNo = this.getQuoteNo();
        this.setUp();
        String coverType = WebInsQuote.COVER_COMP;

        if (this.getCoverType().equals("comprehensive")) {
            // Comprehensive premium.
            coverType = WebInsQuote.COVER_COMP;
            this.getInsuranceMgr().setCoverType(quoteNo, WebInsQuote.COVER_COMP);
            this.setCompDiscounts(true);
        } else if (this.getCoverType().equals("thirdPartyDamage")) {
            // Third party damage premium.
            coverType = WebInsQuote.COVER_TP;
            this.getInsuranceMgr().setCoverType(quoteNo, WebInsQuote.COVER_TP);
            this.setCompDiscounts(false);
        }

        String windScreen = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.WINDSCREEN);
        String hireCar = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.HIRE_CAR);
        String fireTheft = this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIRE_THEFT);

        if (hireCar != null && hireCar.equals(WebInsQuoteDetail.TRUE)) {
            this.setHireCarDiscount(true);
        } else {
            this.setHireCarDiscount(false);
        }

        if (windScreen != null && windScreen.equals(WebInsQuoteDetail.TRUE)) {
            this.setWindscreenDiscount(true);
        } else {
            this.setWindscreenDiscount(false);
        }

        if (fireTheft != null && fireTheft.equals(WebInsQuoteDetail.TRUE)) {
            this.setFireTheftDiscount(true);
        } else {
            this.setFireTheftDiscount(false);
        }

        Premium annualPremium = retrievePremium(WebInsQuoteDetail.PAY_ANNUALLY, coverType);
        Premium monthlyPremium = retrievePremium(WebInsQuoteDetail.PAY_MONTHLY, coverType);

        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_BASE_ANNUAL_PERMIUM, annualPremium.getBaseAnnualPremium().toString());
        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_GST, annualPremium.getGst().toString());
        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_STAMP_DUTY, annualPremium.getStampDuty().toString());
        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString());
        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_INSTALMENT_AMOUNT, annualPremium.getPayment().toString());

        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_BASE_ANNUAL_PERMIUM, monthlyPremium.getBaseAnnualPremium().toString());
        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_GST, monthlyPremium.getGst().toString());
        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_STAMP_DUTY, monthlyPremium.getStampDuty().toString());
        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_TOTAL_ANNUAL_PREMIUM, monthlyPremium.getAnnualPremium().toString());
        this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT, monthlyPremium.getPayment().toString());

        // Set legacy payments data
        this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.PREMIUM, annualPremium.getAnnualPremium().toPlainString());
        this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.PAYMENT, monthlyPremium.getPayment().toPlainString());

        this.setNextAction();
        return SUCCESS;
    }


  /**
   * @return the premiumType
   */
  public String getPremiumType() {
    return premiumType;
  }

  /**
   * @param premiumType the premiumType to set
   */
  public void setPremiumType(String premiumType) {
    this.premiumType = premiumType;
  }


	public String getCoverType() {
		return coverType;
	}


	public void setCoverType(String coverType) {
		this.coverType = coverType;
	}
}