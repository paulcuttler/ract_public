package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.Collection;
import java.util.List;

import com.ract.common.StreetSuburbVO;
import com.ract.util.DateTime;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsClientDetail;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.entities.insurance.car.GaragedAddress;

/**
 *
 * @author xmfu
 */
public class QuotePageSave13 extends QuotePageSave {
    private static final long serialVersionUID = 6942905930320423772L;

  @Override
  public String execute() throws Exception {
    this.setUp();
    int quoteNo = this.getQuoteNo();
    String newPolicy = this.getNewPolicyInSession();

    if (this.getSubmit().equals(BUTTON_BACK)) {
      if (this.isMember()) {
      } else {
        this.setPageId(this.getPageId() - 1);
      }

      this.setNextAction();
      return SUCCESS;
    }

    Integer driverSize = this.getDrivers().size();

    //Save policy holders.
    for (int i = 1; i <= driverSize; i++) {
      String returnMessage = this.savePolicyHolder(i, newPolicy, quoteNo);

      if (returnMessage.equals(String.valueOf(MESSAGE_SUSPENSION))) {
        return this.messagePage(MESSAGE_SUSPENSION);
      } else if (returnMessage.equals(String.valueOf(MESSAGE_CANCELLEDINSURANCE))) {
        return this.messagePage(MESSAGE_CANCELLEDINSURANCE);
      } else if (returnMessage.equals(String.valueOf(MESSAGE_FIFTYPLUS))) {
        return this.messagePage(MESSAGE_FIFTYPLUS);
      } else if (!returnMessage.equals(PASSED)) {
        if (!returnMessage.equals(SYSTEM_MESSAGE)) {
          this.loadPolicyHolders(this.getNewPolicyInSession(), "-1", true);
          return this.validationFailed(returnMessage, VALIDATE_CUSTOMIZED, false);
        }
      }
    }

    this.setNextAction();
    return SUCCESS;
  }

  @Override
  public void validate() {

    if (this.getSubmit().equals(BUTTON_BACK)) {
      return;
    }

    try {
      
      int quoteNo = this.getQuoteNo();
      String newPolicy = this.getNewPolicyInSession();
      Integer driverSize = this.getDrivers().size();
      int preferredAddressCount = 0;

      for (int i = 1; i <= driverSize; i++) {
                
	      String firstName = this.getParameter("firstName_" + i);
	      String surName = this.getParameter("surName_" + i);
	
	      this.initializeIsPolicyHolderArray();

        // To prevent blank fields for first name and surname.
        if (firstName == null || firstName.isEmpty()) {
          this.addFieldError("firstName_" + i, "Please enter a first name");
        } 

        if(surName == null || surName.isEmpty()) {
          this.addFieldError("surName_" + i, "Please enter a surname");
        }
        
      
        WebInsClient client = null;
        List<WebInsClient> clients = this.getDrivers();

        // Existing clients.
        if (i <= clients.size()) {
          client = clients.get(i - 1);
        } else { // Create new Clients.
          client = this.getInsuranceMgr().createInsuranceClient(quoteNo);
        }

        client.setGivenNames(firstName);
        client.setSurname(surName);
        client.setDriver(true);
        this.getInsuranceMgr().setClient(client);

        // Save DOB
        try {
          DateTime dateOfBirth = new DateTime(this.getParameter("additionalDob_" + i));
          client.setBirthDate(dateOfBirth);
        } catch (Exception e) {
          this.addFieldError("additionalDob_" + i, "Please enter a valid date of birth");
        }

        // Save year commenced driving.
        String yearCommencedDriving = this.getParameter("yearCommencedDriving_" + i);

        if (client.getYearCommencedDriving() == null) {
          if (!this.validate(yearCommencedDriving, VALIDATE_INTEGER_STRING)) {
            this.addFieldError("yearCommencedDriving_" + i, "Please enter the year you commenced driving");
          } else {
            client.setYearCommencedDriving(Integer.valueOf(yearCommencedDriving));
          }

          this.getInsuranceMgr().setClient(client);
        }

        // Save gender.
        String gender = this.getParameter("gender_" + i);
        if (gender.equals("Male")) {
          client.setGender(WebInsClient.GENDER_MALE);
        } else {
          client.setGender(WebInsClient.GENDER_FEMALE);
        }

        this.getInsuranceMgr().setClient(client);

        String policyHolder = "";
        if (newPolicy.equals("true")) {
          policyHolder = this.getParameter("policyHolder_" + i);
          
          
          if(this.getParameter("policyHolder_" + i) == null || this.getParameter("policyHolder_" + i).isEmpty()) {
            this.addFieldError("policyHolder_" + i, "Please select an option");
          } else {
            this.getInsuranceMgr().setQuoteDetail(quoteNo, "policyHolder_" + i, this.getParameter("policyHolder_" + i));
          }
          

          if (policyHolder != null && policyHolder.equals("yes")) {
            // Add new policy holder.
            if (i > this.getQuoteTempMemoryObject().getIsPolicyHolders().size()) {
              this.getQuoteTempMemoryObject().getIsPolicyHolders().add(true);
            } else { // Set existing client to be a policy holder.
              this.getQuoteTempMemoryObject().getIsPolicyHolders().set(i - 1, true);
            }

            // Save preferred contact person.
            String preferContact = this.getParameter("preferContactPoint_" + i);

            if (preferContact.equals("yes")) {
              client.setPreferredAddress(true);
              preferredAddressCount++;
              
              if (preferredAddressCount > 1) {
              	this.addFieldError("preferContactPoint_" + i, "Only one person can be set as the preferred contact");
              }
            } else {
              client.setPreferredAddress(false);
            }

            this.getInsuranceMgr().setClient(client);

            // Save title.
            String title = this.getParameter("title_" + i);

            if (!this.validate(title, VALIDATE_STRING)) {
              this.addFieldError("title_" + i, "Please enter a title");
            }

            client.setTitle(title);
            this.getInsuranceMgr().setClient(client);

            // Save contact numbers.
            String homePhoneNumber = this.getParameter("homePhoneNumber_" + i);
            if (homePhoneNumber != null && !homePhoneNumber.equals("")) {
              if (!this.validate(homePhoneNumber, VALIDATE_INTEGER_STRING)) {
                this.addFieldError("homePhoneNumber_" + i, "Please enter a valid home phone number");
              }

              if (!this.validate(homePhoneNumber, VALIDATE_TELEPHONE)) {
                this.addFieldError("homePhoneNumber_" + i, "Please enter a valid home phone number");
              }
            }

            client.setHomePhone(homePhoneNumber);
            this.getInsuranceMgr().setClient(client);

            String workPhoneNumber = this.getParameter("workPhoneNumber_" + i);
            if (workPhoneNumber != null && !workPhoneNumber.equals("")) {
              if (!this.validate(workPhoneNumber, VALIDATE_INTEGER_STRING)) {
                this.addFieldError("workPhoneNumber_" + i, "Please enter a valid work phone number");
              }

              if (!this.validate(workPhoneNumber, VALIDATE_TELEPHONE)) {
                this.addFieldError("workPhoneNumber_" + i, "Please enter a valid work phone number");
              }
            }

            client.setWorkPhone(workPhoneNumber);
            this.getInsuranceMgr().setClient(client);

            String mobilePhoneNumber = this.getParameter("mobilePhoneNumber_" + i);
            if (mobilePhoneNumber != null && !mobilePhoneNumber.equals("")) {
              if (!this.validate(mobilePhoneNumber, VALIDATE_INTEGER_STRING)) {
                this.addFieldError("mobilePhoneNumber_" + i, "Please enter a valid mobile phone number");
              }

              if (!this.validate(mobilePhoneNumber, VALIDATE_TELEPHONE)) {
                this.addFieldError("mobilePhoneNumber_" + i, "Please enter a mobile phone number");
              }
            }

            client.setMobilePhone(mobilePhoneNumber);
            this.getInsuranceMgr().setClient(client);

            GaragedAddress garagedAddress = new GaragedAddress();
            this.setGaragedAddress(garagedAddress);

            // Residential Address
            String sameAsGaragedAddressQ = this.getParameter("sameAsGaragedAddress_" + i);

            if (sameAsGaragedAddressQ.equals("yes")) {
              GaragedAddress garagedAddressTemp = this.populateGaragedAddress(null, "garagedAddress");
              this.saveClientAddress(client, "residential", garagedAddressTemp);
            } else {
              String postCode = this.getParameter("residentialPostcode_" + i);
              if (postCode == null || postCode.equals("")) {
                this.addFieldError("residentialPostcode_" + i, "Please enter a valid postcode");
              }

              this.getGaragedAddress().setPostCode(postCode);
              client.setResiPostcode(postCode);
              this.getInsuranceMgr().setClient(client);

              // Save suburb.
              String suburb = this.getParameter("residentialSuburb_" + i);
              if (suburb == null || suburb.equals("")) {
                this.addFieldError("residentialSuburb_" + i, "Please enter a valid suburb");
              }

              this.getGaragedAddress().setSuburb(suburb);
              client.setResiSuburb(suburb);
              this.getInsuranceMgr().setClient(client);

              // Save street Id.
              String streetSuburbId = this.getParameter("residentialStreetSuburbId_" + i);
              if (streetSuburbId == null || streetSuburbId.equals("")) {
                this.addFieldError("residentialStreetSuburbId_" + i, "Please enter a valid street");
              }

              client.setResiStsubid(Integer.valueOf(streetSuburbId));
              this.getGaragedAddress().setStreetSuburbId(streetSuburbId);
              this.getInsuranceMgr().setClient(client);

              Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsBySuburb(suburb, postCode);

              for(StreetSuburbVO streetSuburbVO : ssList) {
                if (!streetSuburbVO.getStreet().equals("") && null != streetSuburbVO.getStreetSuburbID() && streetSuburbVO.getStreetSuburbID().equals(Integer.valueOf(streetSuburbId))) {
                  client.setResiStreet(streetSuburbVO.getStreet());
                  client.setResiState(streetSuburbVO.getState());
                  this.getInsuranceMgr().setClient(client);
                }
              }

              // Save po box and street number
              String streetNumber = this.getParameter("residentialStreetNumber_" + i);
              this.getGaragedAddress().setStreetNumber(streetNumber);

              if (streetNumber == null || streetNumber.equals("")) {
                this.addFieldError("residentialStreetNumber_" + i, "Please enter a valid street number");
              } else {
                client.setResiStreetChar(streetNumber);
                this.getInsuranceMgr().setClient(client);
              }
            }

            // Postal Address
            // Save post code.
            String sameAsAboveQ = this.getParameter("sameAsAbove_" + i);

            if (sameAsAboveQ.equals("yes")) {
              GaragedAddress garagedAddressTemp = this.populateGaragedAddress(null, "garagedAddress");
              this.saveClientAddress(client, "postal", garagedAddressTemp);
            } else {
              String postCode = this.getParameter("postcode_" + i);
              if (postCode == null || postCode.equals("")) {
                this.addFieldError("postcode_" + i, "Please enter a valid postcode");
              }

              this.getGaragedAddress().setPostCode(postCode);
              client.setPostPostcode(postCode);
              this.getInsuranceMgr().setClient(client);

              // Save suburb.
              String suburb = this.getParameter("suburb_" + i);
              if (suburb == null || suburb.equals("")) {
                this.addFieldError("postcode_" + i, "Please enter a valid suburb");
              }

              this.getGaragedAddress().setSuburb(suburb);
              client.setPostSuburb(suburb);
              this.getInsuranceMgr().setClient(client);

              // Save po box and street number
              String poBox = this.getParameter("pobox_" + i);
              this.getGaragedAddress().setPoBox(poBox);

              String streetNumber = this.getParameter("streetNumber_" + i);
              this.getGaragedAddress().setStreetNumber(streetNumber);

              String streetSuburbId = this.getParameter("streetSuburbId_" + i);

              if (poBox == null || poBox.equals("")) { // Save for street number.
                client.setPostProperty("");
                this.getInsuranceMgr().setClient(client);

                if (streetNumber == null || streetNumber.equals("")) {
                  this.addFieldError("streetNumber_" + i, "Please enter either Street Name and Number or PO Box");
                }

                if (streetSuburbId == null || streetSuburbId.equals("")) {
                  this.addFieldError("streetSuburbId_" + i, "Please enter either Street Name and Number or PO Box");
                } else {                	
	                // Save street Id.
	                client.setPostStsubid(Integer.valueOf(streetSuburbId));
	                this.getGaragedAddress().setStreetSuburbId(streetSuburbId);
	                this.getInsuranceMgr().setClient(client);
	
	                Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsBySuburb(suburb, postCode);
	
	                for(StreetSuburbVO streetSuburbVO : ssList) {
	                  if (!streetSuburbVO.getStreet().equals("") && null != streetSuburbVO.getStreetSuburbID() && streetSuburbVO.getStreetSuburbID().equals(Integer.valueOf(streetSuburbId))) {
	                    client.setPostStreet(streetSuburbVO.getStreet());
	                    client.setPostState(streetSuburbVO.getState());
	                    client.setPostStsubid(streetSuburbVO.getStreetSuburbID());
	                    this.getInsuranceMgr().setClient(client);
	                  }
	                }
	
	                client.setPostStreetChar(streetNumber);
	                this.getInsuranceMgr().setClient(client);
                }
                
              } else {
                // Save for po box.
                client.setPostStreetChar("");
                this.getInsuranceMgr().setClient(client);

                Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsByPostcode(postCode);

                for(StreetSuburbVO streetSuburbVO : ssList) {
                  if (suburb.equals(streetSuburbVO.getSuburb())) {
                    client.setPostStsubid(streetSuburbVO.getStreetSuburbID());
                  }
                }

                client.setPostProperty(poBox);
                this.getInsuranceMgr().setClient(client);
              }
            }

            // Save email address.
            String emailAddress = this.getParameter("emailAddress_" + i);

            if (emailAddress != null && !emailAddress.isEmpty()) {
              if (!this.validate(emailAddress, VALIDATE_EMAIL)) {
                this.addFieldError("emailAddress_" + i, "Please enter a valid email");
              }
              client.setEmailAddress(emailAddress);
            }

            this.getInsuranceMgr().setClient(client);
          } else {
            if (i > this.getQuoteTempMemoryObject().getIsPolicyHolders().size()) {
              // The newly added client is not a policy holder.
              this.getQuoteTempMemoryObject().getIsPolicyHolders().add(false);
            } else {
              // Update the existing client to be not a policy holder.
              this.getQuoteTempMemoryObject().getIsPolicyHolders().set(i - 1, false);
            }
          }
        }

        this.getInsuranceMgr().setClient(client);

        // Check whether license is suspended.
        String suspended = this.getParameter("licenseSuspended_" + i);

        if (suspended == null || suspended.equals("")) {
          this.addFieldError("licenseSuspended_" + i, "Please select an option");
        }

        // If license is suspended, show message page.
        if (suspended.equals("yes")) {
          
        } else {
          this.getInsuranceMgr().setQuoteDetail(quoteNo, "licenseSuspended_" + i, "no");
        }

        WebInsClientDetail detail = null;

        // Save number of accidents.
        String accidents = this.getParameter("accidents_" + i);

        if (accidents == null || accidents.isEmpty()) {
          this.addFieldError("accidents_" + i, "Please select an option");
        }

        this.getInsuranceMgr().setQuoteDetail(quoteNo, "hasAccidents_" + i, accidents);

        List<WebInsClientDetail> existingDetails = this.getInsuranceMgr().getClientDetails(client.getWebClientNo());

        // Record at most 5 recent accidents.
        int numberOfAccident = 5;
        for (int j = 1; j <= numberOfAccident; j++) {
          String accidentDateString = this.getParameter("accidentDate" + j + "_" + i);
          String accidentType = this.getParameter("accidentType" + j + "_" + i);

          if (accidentType != null && !accidentType.equals("") && accidentDateString != null && !accidentDateString.equals("")) {
            if (!this.validate(accidentDateString, VALIDATE_ACCIDENT_DATE)) {
              this.addFieldError("accidentDate" + j + "_" + i, "Accident Date must be in format of (mm/yyyy)");
            }

            String[] accidentDate = accidentDateString.split("/");

            if (existingDetails != null && existingDetails.size() > 0) {
              if (j <= existingDetails.size()) {
                detail = existingDetails.get(j - 1);
              }
            } else {
              detail = new WebInsClientDetail(client.getWebClientNo(), accidentType, new Integer(accidentDate[0]), new Integer(accidentDate[1]), "accident");
            }

            detail.setWebClientNo(client.getWebClientNo());
            this.getInsuranceMgr().setClientDetail(detail);
            this.getInsuranceMgr().setClient(client);
          }
        }

        // Check whether had a cancelled insurance before.
        String insuranceCancelled = this.getParameter("insuranceCancelled_" + i);

        if (insuranceCancelled == null || insuranceCancelled.isEmpty()) {
          this.addFieldError("insuranceCancelled_" + i, "Please select an option");
        }

        // If had a cancelled insurance before, stop quote and display message page.
        if (insuranceCancelled.equals("yes")) {
          
        } else {
          this.getInsuranceMgr().setQuoteDetail(quoteNo, "insuranceRefused_" + i, "no");
        }

        // New policy.
        if (newPolicy.equals("true")) {
          if (policyHolder != null && policyHolder.equals("yes")) {
            client.setOwner(true);
          } else { // Saved policy.
            client.setOwner(false);
          }
        }
      }

      if (!this.getFieldErrors().isEmpty()) {
        this.loadPolicyHolders(this.getNewPolicyInSession(), "-1", true);
        this.setShowNewPolicyHolderLink("true");
      }

    } catch (Exception e) {
      Config.logger.debug(Config.key + ": loadpage13 failed on validation: ", e);
    }

  }
}
