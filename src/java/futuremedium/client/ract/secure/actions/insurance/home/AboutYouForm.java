package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.regex.*;

import com.ract.web.insurance.WebInsQuoteDetail;

/**
 *
 * @author gnewton
 */
public class AboutYouForm extends QuoteSubTypeSupport implements QuoteAware {

  private Boolean member;
  private Boolean applyPip;
  private Boolean fiftyPlus;
  private String dobDay;
  private String dobMonth;
  private String dobYear;
  private String memberDobDay;
  private String memberDobMonth;
  private String memberDobYear;
  private String membershipCard1;
  private String membershipCard2;
  private String membershipCard3;
  private String membershipCard4;

  @Override
  public String execute() throws Exception {

    initMemberCards();
    initDob();
    initMemberDob();

    return SUCCESS;
  }

  public void initMemberCards() {
    String membershipCard = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_MEMBER_CARD);

    try {
      Matcher m = Pattern.compile("(.{4})(.{4})(.{4})(.{4})").matcher(membershipCard);
      m.matches();

      this.membershipCard1 = m.group(1);
      this.membershipCard2 = m.group(2);
      this.membershipCard3 = m.group(3);
      this.membershipCard4 = m.group(4);
    } catch (Exception e) {
    }
  }

  public void initDob() {
    String dob = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_OLDEST_DOB);

    try {
      String[] parts = dob.split("/");
      this.dobDay = parts[0];
      this.dobMonth = parts[1];
      this.dobYear = parts[2];
    } catch (Exception e) {
    }

  }

  public void initMemberDob() {
    String dob = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_MEMBER_DOB);

    try {
      String[] parts = dob.split("/");
      this.memberDobDay = parts[0];
      this.memberDobMonth = parts[1];
      this.memberDobYear = parts[2];
    } catch (Exception e) {
    }

  }

  public Boolean isMember() {
    if (this.member == null) {
      String isMember = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_IS_MEMBER);
      if (isMember == null) {
        this.member = null;
      } else {
        this.member = Boolean.parseBoolean(isMember);
      }
    }
    return this.member;
  }
  
  /**
   * @return the applyPip
   */
  public Boolean getApplyPip() {
      if (this.applyPip == null) {
          String pip = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.PIP_DISCOUNT);
          if (pip == null) {
              this.applyPip = null;
          } else {
              this.applyPip = Boolean.parseBoolean(pip);
          }
      }
      return this.applyPip;
  }

  /**
   * @return the fiftyPlus
   */
  public Boolean getFiftyPlus() {
    if (this.fiftyPlus == null) {
      String fifty = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS);
      if (fifty == null) {
        this.fiftyPlus = null;
      } else {
        this.fiftyPlus = Boolean.parseBoolean(fifty);
      }
    }
    return this.fiftyPlus;
  }

  /**
   * @return the dobDay
   */
  public String getDobDay() {
    return dobDay;
  }

  /**
   * @return the dobMonth
   */
  public String getDobMonth() {
    return dobMonth;
  }

  /**
   * @return the dobYear
   */
  public String getDobYear() {
    return dobYear;
  }

  /**
   * @return the memberDobDay
   */
  public String getMemberDobDay() {
    return memberDobDay;
  }

  /**
   * @return the memberDobMonth
   */
  public String getMemberDobMonth() {
    return memberDobMonth;
  }

  /**
   * @return the memberDobYear
   */
  public String getMemberDobYear() {
    return memberDobYear;
  }

  /**
   * @return the membershipCard1
   */
  public String getMembershipCard1() {
    return membershipCard1;
  }

  /**
   * @return the membershipCard2
   */
  public String getMembershipCard2() {
    return membershipCard2;
  }

  /**
   * @return the membershipCard3
   */
  public String getMembershipCard3() {
    return membershipCard3;
  }

  /**
   * @return the membershipCard4
   */
  public String getMembershipCard4() {
    return membershipCard4;
  }
}
