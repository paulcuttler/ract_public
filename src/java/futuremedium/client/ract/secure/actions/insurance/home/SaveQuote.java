package futuremedium.client.ract.secure.actions.insurance.home;

import com.opensymphony.xwork2.validator.annotations.*;

import futuremedium.client.ract.secure.EmailManager;

/**
 * Record user's email address and email quote number.
 * @author gnewton
 */
public class SaveQuote extends ActionBase implements PersistanceAware {
    private static final long serialVersionUID = 1707387526483249089L;
  private String email;

  @Override
  public String execute() throws Exception {
    boolean ok = this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_SAVED_QUOTE_EMAIL, this.getEmail());
    ok = ok && EmailManager.sendSavedQuoteEmail(this);

    if (ok) {
      //this.setActionMessage("Your quote number has been sent to your email address.");
    } else {
      this.setActionMessage("An error occurred sending an email to your email address");
      return ERROR;
    }

    return SUCCESS;
  }

  @Override
  public String getNextAction() {
    return "HomeSaveQuoteComplete";
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  @RequiredStringValidator(message = "Please enter your email address", shortCircuit=true)
  @EmailValidator(message = "Please enter a valid email address")
  public void setEmail(String email) {
    this.email = email;
  }
}
