package futuremedium.client.ract.secure.actions.insurance.car;

import com.ract.web.insurance.*;
import futuremedium.client.ract.secure.Config;

/**
 * Summary Page Save class.
 * @author xmfu
 */
public class QuotePageSave8 extends QuotePageSave {
  @Override
  public String execute() throws Exception {
     // If user has reached page 9, access from page 1 to page 8 is not allowed.
    if (isReachedPage9()) {
      this.setNextAction();
      return SUCCESS;
    }

    // Set quote number to display in the quote.jsp page.
    if (this.getSubmit().equals(BUTTON_BACK)) {
      this.setNextAction();
      return SUCCESS;
    }

    

    this.setNextAction();
    return SUCCESS;
  }
  
  @Override
    public void validate() {
        
        if (this.getSubmit().equals(BUTTON_BACK)) {
            return;
        }

        try {
          
            // Check Captcha before proceed.
            if (!this.verifyCaptcha()) {
              if (this.getCaptchaTriedTimes() == null) {
                this.setCaptchaTriedTimes(1l);
              } else {
                this.setCaptchaTriedTimes(this.getCaptchaTriedTimes() + 1);
              }

              if (this.getCaptchaTriedTimes() > 3) {
                this.setPageId(0l);
                this.setQuoteNo(null);
                this.setCaptchaTriedTimes(null);
              } else {
                this.setUp();
                this.addFieldError("captchaResponse", "Please enter the correct security phrase");
                this.setPageId(8l);
                this.loadPage8();
              }
            } else {
              // Has reached page 9, user should never be able to go backwards.
              this.getInsuranceMgr().setQuoteStatus(this.getQuoteNo(), WebInsQuote.QUOTE);
              this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), QuotePageSave.FIELD_VIEWED_PREMIUM, "true");
            }


        } catch (Exception e) {
            Config.logger.debug(Config.key + ": loadpage8 failed on validation: ", e);
        }
  
    }
  
}
