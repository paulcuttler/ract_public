package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.*;

import com.ract.web.insurance.InRfDet;

/**
 * Common elements for About Your Home selections.
 * @author gnewton
 */
public class AboutYourHomeBase extends QuoteSubTypeSupport {

  static private final String TOKEN_YEAR_CONSTRUCTED_OPTIONS = "YEAR_CONSTRUCTED_OPTIONS";
  static private final String TOKEN_BUILDING_TYPE_OPTIONS = "BUILDING_TYPE_OPTIONS";
  static private final String TOKEN_WALL_CONSTRUCTION_OPTIONS = "WALL_CONSTRUCTION_OPTIONS";
  static private final String TOKEN_ROOF_CONSTRUCTION_OPTIONS = "ROOF_CONSTRUCTION_OPTIONS";
  static private final String TOKEN_OCCUPANCY_OPTIONS = "OCCUPANCY_OPTIONS";
  static private final String TOKEN_SECURITY_ALARM_OPTIONS = "SECURITY_ALARM_OPTIONS";
  static private final String TOKEN_SECURITY_LOCK_OPTIONS = "SECURITY_LOCK_OPTIONS";
  static private final String TOKEN_MANAGER_OPTIONS = "MANAGER_OPTIONS";



  private List<String> yearConstructedOptions;
  private List<InRfDet> buildingTypeOptions;
  private List<InRfDet> wallConstructionOptions;
  private List<InRfDet> roofConstructionOptions;
  private List<InRfDet> occupancyOptions;
  private List<InRfDet> securityAlarmOptions;
  private List<InRfDet> securityLockOptions;
  private List<InRfDet> managerOptions;


   /**
   * Retrieve or populate managerOptions from session.
   * @return the managerOptions
   */

  @SuppressWarnings("unchecked")
  public List<InRfDet> getManagerOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_MANAGER_OPTIONS)) { // retrieve from session
        this.managerOptions = (List<InRfDet>) this.session.get(TOKEN_MANAGER_OPTIONS);
      } else { // initialise in session
        this.managerOptions = this.getOptionService().getMangerOptions(this);
        this.session.put(TOKEN_MANAGER_OPTIONS, this.managerOptions);
      }
    }
    return managerOptions;
  }

  /**
   * @param managerOptions the managerOptions to set
   */
  public void setManagerOptions(List<InRfDet> managerOptions) {
    this.managerOptions = managerOptions;
    if (this.session != null) {
      this.session.put(TOKEN_MANAGER_OPTIONS, this.managerOptions);
    }
  }

  public Map<String, InRfDet> getManagerLookup() throws Exception {
    return this.getLookup(this.getManagerOptions());
  }



  /**
   * Retrieve or populate yearConstructedOptions from session.
   * @return the yearConstructedOptions
   */
  @SuppressWarnings("unchecked")
  public List<String> getYearConstructedOptions() {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_YEAR_CONSTRUCTED_OPTIONS)) { // retrieve from session
        this.yearConstructedOptions = (List<String>) this.session.get(TOKEN_YEAR_CONSTRUCTED_OPTIONS);
      } else { // initialise in session
        this.yearConstructedOptions = this.getOptionService().getYearConstructedOptions();
        this.session.put(TOKEN_YEAR_CONSTRUCTED_OPTIONS, this.yearConstructedOptions);
      }
    }
    return yearConstructedOptions;
  }

  /**
   * @param yearConstructedOptions the yearConstructedOptions to set
   */
  public void setYearConstructedOptions(List<String> yearConstructedOptions) {
    this.yearConstructedOptions = yearConstructedOptions;
    if (this.session != null) {
      this.session.put(TOKEN_YEAR_CONSTRUCTED_OPTIONS, this.yearConstructedOptions);
    }
  }

  /**
   * Retrieve or populate buildingTypeOptions from session.
   * @return the buildingTypeOptions
   */
  @SuppressWarnings("unchecked")
  public List<InRfDet> getBuildingTypeOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_BUILDING_TYPE_OPTIONS)) { // retrieve from session
        this.buildingTypeOptions = (List<InRfDet>) this.session.get(TOKEN_BUILDING_TYPE_OPTIONS);
      } else { // initialise in session
        this.buildingTypeOptions = this.getOptionService().getBuildingTypeOptions(this);
        this.session.put(TOKEN_BUILDING_TYPE_OPTIONS, this.buildingTypeOptions);
      }
    }
    return buildingTypeOptions;
  }

  /**
   * @param buildingTypeOptions the buildingTypeOptions to set
   */
  public void setBuildingTypeOptions(List<InRfDet> buildingTypeOptions) {
    this.buildingTypeOptions = buildingTypeOptions;
    if (this.session != null) {
      this.session.put(TOKEN_BUILDING_TYPE_OPTIONS, this.buildingTypeOptions);
    }
  }

  /**
   * Return a lookup of Building Type objects by key.
   * @return
   */
  public Map<String, InRfDet> getBuildingTypeLookup() throws Exception {
    return this.getLookup(this.getBuildingTypeOptions());
  }

  /**
   * Retrieve or populate wallConstructionOptions from session.
   * @return the wallConstructionOptions
   */
  @SuppressWarnings("unchecked")
  public List<InRfDet> getWallConstructionOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_WALL_CONSTRUCTION_OPTIONS)) { // retrieve from session
        this.wallConstructionOptions = (List<InRfDet>) this.session.get(TOKEN_WALL_CONSTRUCTION_OPTIONS);
      } else { // initialise in session
        this.wallConstructionOptions = this.getOptionService().getWallConstructionOptions(this);
        this.session.put(TOKEN_WALL_CONSTRUCTION_OPTIONS, this.wallConstructionOptions);
      }
    }
    return wallConstructionOptions;
  }

  /**
   * @param wallConstructionOptions the wallConstructionOptions to set
   */
  public void setWallConstructionOptions(List<InRfDet> wallConstructionOptions) {
    this.wallConstructionOptions = wallConstructionOptions;
    if (this.session != null) {
      this.session.put(TOKEN_WALL_CONSTRUCTION_OPTIONS, this.wallConstructionOptions);
    }
  }

  /**
   * Return a lookup of Wall Construction Options by key.
   * @return
   */
  public Map<String, InRfDet> getWallConstructionLookup() throws Exception {
    return this.getLookup(this.getWallConstructionOptions());
  }


  /**
   * Retrieve or populate roofConstructionOptions from session.
   * @return the roofConstructionOptions
   */
  @SuppressWarnings("unchecked")
  public List<InRfDet> getRoofConstructionOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_ROOF_CONSTRUCTION_OPTIONS)) { // retrieve from session
        this.roofConstructionOptions = (List<InRfDet>) this.session.get(TOKEN_ROOF_CONSTRUCTION_OPTIONS);
      } else { // initialise in session
        this.roofConstructionOptions = this.getOptionService().getRoofConstructionOptions(this);
        this.session.put(TOKEN_ROOF_CONSTRUCTION_OPTIONS, this.roofConstructionOptions);
      }
    }
    return roofConstructionOptions;
  }

  /**
   * @param roofConstructionOptions the roofConstructionOptions to set
   */
  public void setRoofConstructionOptions(List<InRfDet> roofConstructionOptions) {
    this.roofConstructionOptions = roofConstructionOptions;
    if (this.session != null) {
      this.session.put(TOKEN_ROOF_CONSTRUCTION_OPTIONS, this.roofConstructionOptions);
    }
  }

  /**
   * Return a lookup of Roof Construction Options by key.
   * @return
   */
  public Map<String, InRfDet> getRoofConstructionLookup() throws Exception {
    return this.getLookup(this.getRoofConstructionOptions());
  }

  /**
   * Retrieve or populate occupancyOptions from session.
   * @return the occupancyOptions
   */
  @SuppressWarnings("unchecked")
  public List<InRfDet> getOccupancyOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_OCCUPANCY_OPTIONS)) { // retrieve from session
        this.occupancyOptions = (List<InRfDet>) this.session.get(TOKEN_OCCUPANCY_OPTIONS);
      } else { // initialise in session
        this.occupancyOptions = this.getOptionService().getOccupancyOptions(this);
        this.session.put(TOKEN_OCCUPANCY_OPTIONS, this.occupancyOptions);
      }
    }
    return occupancyOptions;
  }

  /**
   * @param occupancyOptions the occupancyOptions to set
   */
  public void setOccupancyOptions(List<InRfDet> occupancyOptions) {
    this.occupancyOptions = occupancyOptions;
    if (this.session != null) {
      this.session.put(TOKEN_OCCUPANCY_OPTIONS, this.occupancyOptions);
    }
  }

  /**
   * Return a lookup of Occupancy Options by key.
   * @return
   */
  public Map<String, InRfDet> getOccupancyLookup() throws Exception {
    return this.getLookup(this.getOccupancyOptions());
  }

 
    @SuppressWarnings("unchecked")
    public List<InRfDet> getSecurityAlarmOptions() throws Exception {
        if (this.session != null) {
            if (this.session.containsKey(TOKEN_SECURITY_ALARM_OPTIONS)) { // retrieve from session
                this.securityAlarmOptions = (List<InRfDet>) this.session.get(TOKEN_SECURITY_ALARM_OPTIONS);
            } else { // initialise in session
                this.securityAlarmOptions = this.getOptionService().getSecurityAlarmOptions(this);
                this.session.put(TOKEN_SECURITY_ALARM_OPTIONS, this.securityAlarmOptions);
            }
        }
        return securityAlarmOptions;
    }

    public void setSecurityAlarmOptions(List<InRfDet> securityAlarmOptions) {
        this.securityAlarmOptions = securityAlarmOptions;
        if (this.session != null) {
            this.session.put(TOKEN_SECURITY_ALARM_OPTIONS, this.securityAlarmOptions);
        }
    }

    public Map<String, InRfDet> getSecurityAlarmLookup() throws Exception {
        return this.getLookup(this.getSecurityAlarmOptions());
    }

    @SuppressWarnings("unchecked")
    public List<InRfDet> getSecurityLockOptions() throws Exception {
        if (this.session != null) {
            if (this.session.containsKey(TOKEN_SECURITY_LOCK_OPTIONS)) { // retrieve from session
                this.securityLockOptions = (List<InRfDet>) this.session.get(TOKEN_SECURITY_LOCK_OPTIONS);
            } else { // initialise in session
                this.securityLockOptions = this.getOptionService().getSecurityLockOptions(this);
                this.session.put(TOKEN_SECURITY_LOCK_OPTIONS, this.securityLockOptions);
            }
        }
        return securityLockOptions;
    }

    public void setSecurityLockOptions(List<InRfDet> securityLockOptions) {
        this.securityLockOptions = securityLockOptions;
        if (this.session != null) {
            this.session.put(TOKEN_SECURITY_LOCK_OPTIONS, this.securityLockOptions);
        }
    }

    public Map<String, InRfDet> getSecurityLockLookup() throws Exception {
        return this.getLookup(this.getSecurityLockOptions());
    }
}
