package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.List;

import com.ract.web.insurance.*;

import futuremedium.client.ract.secure.Config;

/**
 * Common elements for Specified Contents form and processing.
 * @author gnewton
 */
public class SpecifiedContentsBase extends PolicySupport {
    private static final long serialVersionUID = -2042331684643192701L;
  private List<InRfDet> specifiedContentsOptions;
  private List<InRiskSi> specifiedContents;
  private Integer minSpecifiedContentsValue;
  private Integer getMaxSpecifiedContentsPercentage = null;

  /**
   * @return the specifiedContentsOptions
   */
  public List<InRfDet> getSpecifiedContentsOptions() throws Exception {
    if (this.specifiedContentsOptions == null) {
      this.specifiedContentsOptions = this.getOptionService().getSpecifiedContentsOptions(this);
    }
    return specifiedContentsOptions;
  }

  /**
   * @param specifiedContentsOptions the specifiedContentsOptions to set
   */
  public void setSpecifiedContentsOptions(List<InRfDet> specifiedContentsOptions) {
    this.specifiedContentsOptions = specifiedContentsOptions;
  }

  /**
   * @return the specifiedContents
   */
  public List<InRiskSi> getSpecifiedContents() {
    if (this.specifiedContents == null) {
      this.specifiedContents = this.getHomeService().retrieveSpecifiedContents(this);
    }
    return specifiedContents;
  }

  /**
   * @param specifiedContents the specifiedContents to set
   */
  public void setSpecifiedContents(List<InRiskSi> specifiedContents) {
    this.specifiedContents = specifiedContents;
  }

	public void setMinSpecifiedContentsValue(Integer minSpecifiedContentsValue) {
		this.minSpecifiedContentsValue = minSpecifiedContentsValue;
	}

	public Integer getMinSpecifiedContentsValue() {
		if (this.minSpecifiedContentsValue == null) {
			this.minSpecifiedContentsValue = Integer.parseInt(Config.configManager.getProperty("insurance.home.minSpecifiedContentsValue","1000"));
		}
		return minSpecifiedContentsValue;
	}

    public Integer getMaxSpecifiedContentsPercentage() {
        if (this.getMaxSpecifiedContentsPercentage == null) {
            this.getMaxSpecifiedContentsPercentage = Integer.parseInt(Config.configManager.getProperty("insurance.home.maxSpecifiedContentsPercentage", "25"));
        }
        return getMaxSpecifiedContentsPercentage;
    }
}
