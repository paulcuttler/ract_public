package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;
import com.ract.util.DateTime;
import com.ract.web.insurance.WebInsClient;

import futuremedium.client.ract.secure.Config;


/**
 * Either attach an existing policy holder or create a new client from a form.
 * If the user picks a client currently attached to the quote, do nothing.
 * @author gnewton
 */
public class PolicyholderDetails extends PolicyholderDetailsBase implements PolicyAware, ServletRequestAware {
    private static final long serialVersionUID = 6328498360550991868L;
  private String selectedClient;
  private int count;
  private HttpServletRequest servletRequest;

  private boolean sameAddress = true;


    @Override
    public String execute() throws Exception {
        boolean ok = true;

        if (this.getSelectedClient().equals("-1")) {
            // I would like this policy to have new policy holders.

            // detach any existing clients from the policy
            ok = ok && this.getHomeService().detachCurrentClients(this, true);

            ok = ok && this.getHomeService().storeClients(this);

            // confirm policy holders are 50+ if a 50+ quote
            if (this.getFiftyPlus() != null && this.getFiftyPlus()) {
                for (int i = 1; i <= this.getCount(); i++) {
                    String dobDay = this.getServletRequest().getParameter("dob" + i + "Day");
                    dobDay = (dobDay == null) ? null : dobDay.trim();
                    String dobMonth = this.getServletRequest().getParameter("dob" + i + "Month");
                    dobMonth = (dobMonth == null) ? null : dobMonth.trim();
                    String dobYear = this.getServletRequest().getParameter("dob" + i + "Year");
                    dobYear = (dobYear == null) ? null : dobYear.trim();

                    if ((dobDay == null || dobDay.isEmpty()) && (dobMonth == null || dobMonth.isEmpty()) && (dobYear == null || dobYear.isEmpty())) {
                        continue;
                    }

                    DateTime dobDateTime = new DateTime(dobDay + "/" + dobMonth + "/" + dobYear);
                    if (!this.isOlderThan(dobDateTime, 50)) {
                        return this.getHomeService().abortQuote(this, "invalid 50+ policyholder", dobDateTime.toString(), Config.configManager.getProperty("insurance.home.exclusion.invalid50+.message"));
                    }
                }
            }
            ok = ok && this.getHomeService().checkPreferredAddress(this);
        } else {
            // selected existing policy holder

            // make sure any selected clients are cleared
            ok = ok && this.getHomeService().detachCurrentClients(this, false);
            // this.setNewClients(null);

            List<WebInsClient> policyHolderOptionsList = this.getHomeService().retrieveClientOptions(this);

            for (WebInsClient clientOption : policyHolderOptionsList) {
                // if the selected client is an existing policy holder, add them
                if (clientOption.getSelectionDescription().equals(this.getSelectedClient())) {
                    // make the client an owner, and preferred address
                    clientOption.setOwner(true);
                    clientOption.setPreferredAddress(true);
                    ok = ok && this.getHomeService().attachClient(this.getQuoteNo(), clientOption);
                    break;
                }
            }
        }

        ok = ok && this.getHomeService().storeLastActionName(this);

        if (!this.isSameAddress()) {
            String value = String.valueOf(this.isSameAddress());
            return this.getHomeService().abortQuote(this, "same address", value, Config.configManager.getProperty("insurance.home.exclusion.sameAddress.message"));
        }

        if (ok) {
            // this.setActionMessage(this.getSuccessMessage());
        } else {
            this.setActionMessage(this.getErrorMessage());
            return ERROR;
        }

        return SUCCESS;
    }

  @Override
  public void validate() {
    // I would like this policy to have new policy holders.
    if (this.getSelectedClient() != null && this.getSelectedClient().equals("-1")) {
      for (int i = 1; i <= this.getCount(); i++) {
        boolean phone = false;

        String title = this.getServletRequest().getParameter("title" + i);
        title = (title == null) ? null : title.trim();
        String firstName = this.getServletRequest().getParameter("firstName" + i);
        firstName = (firstName == null) ? null : firstName.trim();
        String surname = this.getServletRequest().getParameter("surname" + i);
        surname = (surname == null) ? null : surname.trim();
        String dobDay = this.getServletRequest().getParameter("dob" + i + "Day");
        dobDay = (dobDay == null) ? null : dobDay.trim();
        String dobMonth = this.getServletRequest().getParameter("dob" + i + "Month");
        dobMonth = (dobMonth == null) ? null : dobMonth.trim();
        String dobYear = this.getServletRequest().getParameter("dob" + i + "Year");
        dobYear = (dobYear == null) ? null : dobYear.trim();
        String gender = this.getServletRequest().getParameter("gender" + i);
        gender = (gender == null) ? null : gender.trim();
        String workA = this.getServletRequest().getParameter("workA" + i);
        workA = (workA == null) ? null : workA.trim();
        String workB = this.getServletRequest().getParameter("workB" + i);
        workB = (workB == null) ? null : workB.trim();
        String homeA = this.getServletRequest().getParameter("homeA" + i);
        homeA = (homeA == null) ? null : homeA.trim();
        String homeB = this.getServletRequest().getParameter("homeB" + i);
        homeB = (homeB == null) ? null : homeB.trim();
        String mobileA = this.getServletRequest().getParameter("mobileA" + i);
        mobileA = (mobileA == null) ? null : mobileA.trim();
        String mobileB = this.getServletRequest().getParameter("mobileB" + i);
        mobileB = (mobileB == null) ? null : mobileB.trim();
        String email = this.getServletRequest().getParameter("email" + i);
        email = (email == null) ? null : email.trim();
        String hasSameAddress = this.getServletRequest().getParameter("sameAddress" + i);
        hasSameAddress = (hasSameAddress == null) ? null : hasSameAddress.trim();


        if((title == null || title.isEmpty())
                && (firstName == null || firstName.isEmpty())
                && (surname == null || surname.isEmpty())
                && (dobDay == null || dobDay.isEmpty())
                && (dobMonth == null || dobMonth.isEmpty())
                && (dobYear == null || dobYear.isEmpty())
                && (gender == null || gender.isEmpty())
                && (workA == null || workA.isEmpty())
                && (workB == null || workB.isEmpty())
                && (homeA == null || homeA.isEmpty())
                && (homeB == null || homeB.isEmpty())
                && (mobileA == null || mobileA.isEmpty())
                && (mobileB == null || mobileB.isEmpty())
                && (email == null || email.isEmpty())
                && (hasSameAddress == null || hasSameAddress.isEmpty())
                && i > 1) {
          
          continue;
        }


        if(i > 1 && (hasSameAddress == null || !Boolean.parseBoolean(hasSameAddress)) && this.isSameAddress()) {
          this.setSameAddress(false);
        } 


        if (title == null || title.isEmpty()) {
          this.addFieldError("title" + i, "Please select a title");
        }

        if (firstName == null || firstName.isEmpty()) {
          this.addFieldError("firstName" + i, "Please provide a first name");
        }
        if (surname == null || surname.isEmpty()) {
          this.addFieldError("surname" + i, "Please provide a surname");
        }

        if (dobDay != null && dobMonth != null && dobYear != null) {
          String dob = dobDay + "/" + dobMonth + "/" + dobYear;
          if (dob != null && dob.equals("//")) {
            dob = null;
            this.addFieldError("dob" + i + "Day", "Please provide a date of birth");
          }
          this.validateDob(dob, 16, 110, "dob" + i + "Day", "date of birth");
        } else {
          this.addFieldError("dob" + i + "Day", "Please provide a date of birth");
        } 
        
        if (gender == null || gender.isEmpty()) {
          this.addFieldError("gender" + i, "Please provide a gender");
        }
        if (this.getPersistedAgentUser() == null || this.getPersistedAgentUser().isEmpty()) {
	        if (email == null || email.isEmpty()) {
	          this.addFieldError("email" + i, "Please provide an email address");
	        } else if (!REGEXP_EMAIL.matcher(email).matches()) {
	          this.addFieldError("email" + i, "Please provide a valid email address");
	        }
        }

        if (workA != null && !workA.isEmpty()) {
          if (workB == null || workB.length() != 8) {
            this.addFieldError("phone" + i, "Work phone number must be 8 digits");
          } else {
            phone = true;
          }
        }

        if (homeA != null && !homeA.isEmpty()) {
          if (homeB == null || homeB.length() != 8) {
            this.addFieldError("phone" + i, "Home phone number must be 8 digits");
          } else {
            phone = true;
          }
        }

        if (mobileA != null && !mobileA.isEmpty()) {
          if (mobileA.length() != 4 || mobileB == null || mobileB.length() != 6) {
            this.addFieldError("phone" + i, "Mobile number must be 10 digits");
          } else {
            phone = true;
          }
        }

        if (!phone) {
          this.addFieldError("phone" + i, "Please provide at least one phone number");
        }
      }
    }
  }

  /**
   * @return the servletRequest
   */
  public HttpServletRequest getServletRequest() {
    return servletRequest;
  }

  /**
   * @param servletRequest the servletRequest to set
   */
  @Override
  public void setServletRequest(HttpServletRequest servletRequest) {
    this.servletRequest = servletRequest;
  }

  /**
   * @return the count
   */
  public int getCount() {
    return count;
  }

  /**
   * @param count the count to set
   */
  public void setCount(int count) {
    this.count = count;
  }


  /**
   * @return the selectedClient
   */
  public String getSelectedClient() {
      if(null == selectedClient) {
          selectedClient = getSelectedPolicyHolder();
      }
      return selectedClient;
  }
  
  /**
   * @param selectedClient the selectedClient to set
   */
  @RequiredFieldValidator(message = "Please answer this question", type = ValidatorType.SIMPLE)
  public void setSelectedClient(String existingClient) {
      this.selectedClient = existingClient;
  }

  /**
   * @return the sameAddress
   */
  public boolean isSameAddress() {
    return sameAddress;
  }

  /**
   * @param sameAddress the sameAddress to set
   */
  public void setSameAddress(boolean sameAddress) {
    this.sameAddress = sameAddress;
  }

  

}
