package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.*;

import com.ract.web.insurance.*;
import futuremedium.client.ract.secure.Config;

/**
 *
 * @author xmfu
 */
public class QuotePageSave7 extends QuotePageSave {
  private String fullDriverLicenseYear;
  private String accidentNumber;
  private String bothAccidents;

  @Override
  public String execute() throws Exception {
    // If user has reached page 9, access from page 1 to page 8 is not allowed.
    if (isReachedPage9()) {
      this.setNextAction();
      return SUCCESS;
    }

    this.setUp();
    int quoteNo = this.getQuoteNo();

    // Go back to previous page.
    if (this.getSubmit().equals(BUTTON_BACK)) {
      this.setNextAction();
      return SUCCESS;
    }
   
    WebInsClient ratingDriver = this.insuranceMgr.getRatingDriver(quoteNo);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());

    // Validation.
    if (!this.validate(this.getFullDriverLicenseYear(), VALIDATE_INTEGER_STRING)) {
      this.loadPage7(this.getFullDriverLicenseYear(), this.getAccidentNumber(), bothAccidents, ratingDriver.getGivenNames(), ratingDriver.getSurname());
      return this.validationFailed("Driver License Commencement Year", VALIDATE_INTEGER, false);
    } else if (!this.validate(this.getAccidentNumber(), VALIDATE_INTEGER_STRING)) {
      this.loadPage7(this.getFullDriverLicenseYear(), this.getAccidentNumber(), bothAccidents, ratingDriver.getGivenNames(), ratingDriver.getSurname());
      return this.validationFailed("Accident Number", VALIDATE_INTEGER, false);
    } else {

      // year obtained full driver licence must be greater 16 years.
      if ((Integer.valueOf(this.getFullDriverLicenseYear()) - ratingDriver.getBirthDate().getDateYear()) < 16 || Integer.valueOf(this.getFullDriverLicenseYear()) > calendar.get(Calendar.YEAR)) {
        this.loadPage7(this.getFullDriverLicenseYear(), this.getAccidentNumber(), bothAccidents, ratingDriver.getGivenNames(), ratingDriver.getSurname());
        return this.validationFailed("", VALIDATE_COMMENCED_DRIVING, false);
      }

      // Save.
      WebInsClient tempRatingDriver = insuranceMgr.getRatingDriver(quoteNo);
      tempRatingDriver.setYearCommencedDriving(Integer.valueOf(this.getFullDriverLicenseYear()));

      tempRatingDriver.setNumberOfAccidents(Integer.valueOf(this.getAccidentNumber()));
      this.insuranceMgr.setClient(tempRatingDriver);
   
      this.insuranceMgr.setQuoteDetail(quoteNo, "rating driver", String.valueOf(tempRatingDriver.getWebClientNo()));

      // Calculate NCB.
      int ncb = this.insuranceMgr.calculateNCD(tempRatingDriver);
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.NO_CLAIM_DISCOUNT, String.valueOf(ncb));

      this.getInsuranceMgr().setQuoteStatus(this.getQuoteNo(), WebInsQuote.QUOTE);
      this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), QuotePageSave.FIELD_VIEWED_PREMIUM, "true");

      // If has more than 2 recent accidents, stop quote.
      if (tempRatingDriver.getNumberOfAccidents() > 2) {
        return this.messagePage(MESSAGE_ACCIDENTS_MORETHANTWO);
      }

      // If both accidents true, stop quote.
      if (this.getBothAccidents() != null && this.getBothAccidents().equals("yes")) {
        return this.messagePage(MESSAGE_TWOACCIDENTS);
      }
      
      this.setNextAction(1);
      return SUCCESS;
    }
  }
  
  @Override
    public void validate() {
        
        if (this.getSubmit().equals(BUTTON_BACK)) {
            return;
        }

        try {
          
            WebInsClient ratingDriver = this.getInsuranceMgr().getRatingDriver(this.getQuoteNo());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());

            // Validation.
            if (!this.validate(this.getAccidentNumber(), VALIDATE_INTEGER_STRING)) {
              this.addFieldError("accidentNumber", "Please enter a valid accident number");
            } 

            if (!this.validate(this.getFullDriverLicenseYear(), VALIDATE_INTEGER_STRING)) {
              this.addFieldError("fullDriverLicenseYear", "Please enter a valid driver license year");
            } else if ((Integer.valueOf(this.getFullDriverLicenseYear()) - ratingDriver.getBirthDate().getDateYear()) < 16 || Integer.valueOf(this.getFullDriverLicenseYear()) > calendar.get(Calendar.YEAR)) {
              this.addFieldError("fullDriverLicenseYear", "Please enter a valid driver license year");
            }
            
            
            // Check Captcha before proceed.
            if (!this.verifyCaptcha()) {
              if (this.getCaptchaTriedTimes() == null) {
                this.setCaptchaTriedTimes(1l);
              } else {
                this.setCaptchaTriedTimes(this.getCaptchaTriedTimes() + 1);
              }

              if (this.getCaptchaTriedTimes() > 3) {
                this.setPageId(0l);
                this.setQuoteNo(null);
                this.setCaptchaTriedTimes(null);
              } else {
                this.setUp();
                this.addFieldError("captchaResponse", "Please enter the correct security phrase");
                this.setPageId(7l);
              }
            } 
            
            if (!this.getFieldErrors().isEmpty()) {
                this.loadPage7(this.getFullDriverLicenseYear(), this.getAccidentNumber(), bothAccidents, ratingDriver.getGivenNames(), ratingDriver.getSurname());
            }


        } catch (Exception e) {
            Config.logger.debug(Config.key + ": loadpage7 failed on validation: ", e);
        }
  
    }

  /**
   * @return the fullDriverLicenseYear
   */
  public String getFullDriverLicenseYear() {
    return fullDriverLicenseYear;
  }

  /**
   * @param fullDriverLicenseYear the fullDriverLicenseYear to set
   */
  public void setFullDriverLicenseYear(String fullDriverLicenseYear) {
    this.fullDriverLicenseYear = fullDriverLicenseYear;
  }

  /**
   * @return the accidentNumber
   */
  public String getAccidentNumber() {
    return accidentNumber;
  }

  /**
   * @param accidentNumber the accidentNumber to set
   */
  public void setAccidentNumber(String accidentNumber) {
    this.accidentNumber = accidentNumber;
  }

  /**
   * @return the vehicle
   */
  //public VehicleInfo getVehicleInfo() {
  //  return vehicleInfo;
  //}

  /**
   * @param vehicle the vehicle to set
   */
  //public void setVehicleInfo(VehicleInfo vehicleInfo) {
  //  this.vehicleInfo = vehicleInfo;
  //}

  /**
   * @return the startDate
   */
  //public String getStartDate() {
  //  return startDate;
  //}

  /**
   * @param startDate the startDate to set
   */
  //public void setStartDate(String startDate) {
  //  this.startDate = startDate;
  //}

  /**
   * @return the driver1
   */
  //public WebInsClient getDriver1() {
  //  return driver1;
  //}

  /**
   * @param driver1 the driver1 to set
   */
  //public void setDriver1(WebInsClient driver1) {
  //  this.driver1 = driver1;
  //}

  /**
   * @return the driver2
   */
  //public WebInsClient getDriver2() {
  //  return driver2;
  //}

  /**
   * @param driver2 the driver2 to set
   */
  //public void setDriver2(WebInsClient driver2) {
  //  this.driver2 = driver2;
  //}

  /**
   * @return the bothAccidents
   */
  public String getBothAccidents() {
    return bothAccidents;
  }

  /**
   * @param bothAccidents the bothAccidents to set
   */
  public void setBothAccidents(String bothAccidents) {
    this.bothAccidents = bothAccidents;
  }

  /**
   * @return the ratingDriver
   */
  //public WebInsClient getRatingDriver() {
  //  return ratingDriver;
  //}

  /**
   * @param ratingDriver the ratingDriver to set
   */
  //public void setRatingDriver(WebInsClient ratingDriver) {
  //  this.ratingDriver = ratingDriver;
  //}

  /**
   * @return the noClaimDiscount
   */
  //public String getNoClaimDiscount() {
  //  return noClaimDiscount;
  //}

  /**
   * @param noClaimDiscount the noClaimDiscount to set
   */
  //public void setNoClaimDiscount(String noClaimDiscount) {
  //  this.noClaimDiscount = noClaimDiscount;
  //}
}