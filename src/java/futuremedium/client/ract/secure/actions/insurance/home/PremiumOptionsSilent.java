package futuremedium.client.ract.secure.actions.insurance.home;

import java.math.BigDecimal;

/**
 * Variation on PremiumOptions that silently saves and returns updated premium via JSON.
 * Does not update value of lastAction.
 * @author gnewton
 */
public class PremiumOptionsSilent extends PremiumOptions {

  private boolean showPremiumValues = true;

  @Override
  public String execute() throws Exception {

    // save and do not update lastAction
    boolean ok = savePremiumOptions(false);

    return JSON;
  }

  /**
   * @return the showPremiumValues
   */
  public boolean isShowPremiumValues() {
    return showPremiumValues;
  }

  /**
   * @param showPremiumValues the showPremiumValues to set
   */
  public void setShowPremiumValues(boolean showPremiumValues) {
    this.showPremiumValues = showPremiumValues;
  }

  /**
   * Retrieve the premium from the DB as we have just saved it.
   * @return the totalAnnualPremium
   */
  @Override
  public BigDecimal getTotalAnnualPremium() {
    if (this.totalAnnualPremium == null) {
      this.totalAnnualPremium = this.getHomeService().retrieveTotalAnnualPremium(this);
    }
    return totalAnnualPremium;
  }

  /**
   * @param totalAnnualPremium the totalAnnualPremium to set
   */
  @Override
  public void setTotalAnnualPremium(BigDecimal totalAnnualPremium) {
    this.totalAnnualPremium = totalAnnualPremium;
  }

  /**
   * Retrieve the premium from the DB as we have just saved it.
   * @return the totalMonthlyPremium
   */
  @Override
  public BigDecimal getTotalMonthlyPremium() {
    if (this.totalMonthlyPremium == null) {
      this.totalMonthlyPremium = this.getHomeService().retrieveTotalMonthlyPremium(this);
    }
    return totalMonthlyPremium;
  }

  /**
   * @param totalMonthlyPremium the totalMonthlyPremium to set
   */
  @Override
  public void setTotalMonthlyPremium(BigDecimal totalMonthlyPremium) {
    this.totalMonthlyPremium = totalMonthlyPremium;
  }

}
