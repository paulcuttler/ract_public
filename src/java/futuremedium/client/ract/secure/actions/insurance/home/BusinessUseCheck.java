package futuremedium.client.ract.secure.actions.insurance.home;

import com.ract.web.insurance.InRfDet;

/**
 * Used in AJAX request to check whether to show additional information based on selected Business Use.
 * @author gnewton
 */
public class BusinessUseCheck extends AboutYourHomePolicyBase {

  static private final String FIELD_VALUE_BUSINESS_USE_NONE = "NONE";
  private String businessUse;
  private Boolean showBusinessUseInfo;

  @Override
  public String execute() throws Exception {

    if (this.getBusinessUse() != null && !this.getBusinessUse().isEmpty()) {
      InRfDet selectedBusinessUse = this.getBusinessUseLookup().get(this.getBusinessUse());
      if (selectedBusinessUse.isAcceptable() && !selectedBusinessUse.getrClass().equals(FIELD_VALUE_BUSINESS_USE_NONE)) {
        this.setShowBusinessUseInfo(true);
      } else {
        this.setShowBusinessUseInfo(false);
      }
    }

    return JSON;
  }

  /**
   * @return the businessUse
   */
  public String getBusinessUse() {
    return businessUse;
  }

  /**
   * @param businessUse the businessUse to set
   */
  public void setBusinessUse(String businessUse) {
    this.businessUse = businessUse;
  }

  /**
   * @return the showBusinessUseInfo
   */
  public Boolean isShowBusinessUseInfo() {
    return showBusinessUseInfo;
  }

  /**
   * @param showBusinessUseInfo the showBusinessUseInfo to set
   */
  public void setShowBusinessUseInfo(Boolean showBusinessUseInfo) {
    this.showBusinessUseInfo = showBusinessUseInfo;
  }

}
