package futuremedium.client.ract.secure.actions.insurance;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import futuremedium.client.ract.secure.entities.insurance.AgentService;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author gnewton
 */
public class AgentPortal extends InsuranceBase implements SessionAware {

  private String username;
  private String password;
  private String agentCode;

  private boolean showLinks;

  private AgentService agentService;


  @Override
  public String execute() throws Exception {

    boolean ok = this.getAgentService().authenticateAgent(this);

    if (!ok) { // action message set by authenticateAgent.
      return ERROR;
    } else {
      this.setShowLinks(true);
    }
    
    return SUCCESS;
  }

  /**
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  @RequiredStringValidator(message="Please provide your Username")
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password the password to set
   */
  @RequiredStringValidator(message="Please provide your Password")
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @return the agentId
   */
  public String getAgentCode() {
    return agentCode;
  }

  /**
   * @param agentId the agentId to set
   */
  @RequiredStringValidator(message="Please provide your Agent Code")
  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  @Override
  public void setSession(Map<String, Object> session) {
    this.session = session;
  }

  /**
   * @return the agentService
   */
  public AgentService getAgentService() {
    return agentService;
  }

  /**
   * @param agentService the agentService to set
   */
  public void setAgentService(AgentService agentService) {
    this.agentService = agentService;
  }

  /**
   * @return the showLinks
   */
  public boolean isShowLinks() {
    return showLinks;
  }

  /**
   * @param showLinks the showLinks to set
   */
  public void setShowLinks(boolean showLinks) {
    this.showLinks = showLinks;
  }
}
