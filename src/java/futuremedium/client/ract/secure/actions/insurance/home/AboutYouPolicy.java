package futuremedium.client.ract.secure.actions.insurance.home;

import com.opensymphony.xwork2.validator.annotations.*;
import com.ract.web.insurance.WebInsQuoteDetail;
import futuremedium.client.ract.secure.Config;

/**
 *
 * @author gnewton
 */
public class AboutYouPolicy extends PolicySupport {

  private Boolean criminalConvictions;
  private Boolean insuranceRefused;
  private Integer claims;

  @Override
  public String execute() throws Exception {

    // save details
    boolean ok = this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.CRIMINAL_CONV, this.isCriminalConvictions().toString());
    ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_INSURANCE_REFUSED, this.isInsuranceRefused().toString());
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.CLAIM, this.getClaims().toString());

    // exclusions
    if (this.isCriminalConvictions()) {
      return this.getHomeService().abortQuote(this, "Criminal Convictions", this.isCriminalConvictions().toString(), Config.configManager.getProperty("insurance.home.exclusion.criminalConvictions.message"));
    }
    if (this.isInsuranceRefused()) {
      return this.getHomeService().abortQuote(this, "Insurance Refused", this.isInsuranceRefused().toString(), Config.configManager.getProperty("insurance.home.exclusion.insuranceRefused.message"));
    }
    if (this.getClaims() > Integer.parseInt(Config.configManager.getProperty("insurance.home.maxClaims"))) {
      return this.getHomeService().abortQuote(this, "Claims", this.getClaims().toString(), "Please contact us to discuss your claims history");
    }

    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    return SUCCESS;
  }

  /**
   * @return the criminalConvictions
   */
  public Boolean isCriminalConvictions() {
    return criminalConvictions;
  }

  /**
   * @param criminalConvictions the criminalConvictions to set
   */
  @RequiredFieldValidator(message = "Please answer this question", type = ValidatorType.SIMPLE)
  public void setCriminalConvictions(Boolean criminalConvictions) {
    this.criminalConvictions = criminalConvictions;
  }

  /**
   * @return the insuranceRefused
   */
  public Boolean isInsuranceRefused() {
    return insuranceRefused;
  }

  /**
   * @param insuranceRefused the insuranceRefused to set
   */
  @RequiredFieldValidator(message = "Please answer this question", type = ValidatorType.SIMPLE)
  public void setInsuranceRefused(Boolean insuranceRefused) {
    this.insuranceRefused = insuranceRefused;
  }

  /**
   * @return the claims
   */
  public Integer getClaims() {
    return claims;
  }

  /**
   * @param claims the claims to set
   */
  @RequiredFieldValidator(message = "Please answer this question", type = ValidatorType.SIMPLE)
  public void setClaims(Integer claims) {
    this.claims = claims;
  }

}
