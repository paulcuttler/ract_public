package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.*;

import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuoteDetail;
import futuremedium.client.ract.secure.Config;

/**
 *
 * @author xmfu
 */
public class QuotePageSave15 extends QuotePageSave {

  private String emailAddress;
  private String postcode;
  private String suburb;
  private String streetSuburbId;
  private String streetNumber;
  private String pobox;
  private String residentialPostcode;
  private String residentialSuburb;
  private String residentialStreetSuburbId;
  private String residentialStreetNumber;


  @Override
  public String execute() throws Exception {
    this.setUp();
    int quoteNo = this.getQuoteNo();

    if (this.getSubmit().equals(BUTTON_BACK)) {
      //this.setPageId(this.getPageId() - 1);
      //this.back();

      //this.loadPage14(false);
      if (this.isMember()) {
        if (this.getNewPolicyInSession().equals("true")) {
          //this.loadFirstContactPolicyHolder();
        } else {
          // Load existing policy holders.
          this.setPageId(this.getPageId() - 1);
        }
      }

      this.setNextAction();
      return SUCCESS;
    }

    if ((this.getPersistedAgentUser() == null || this.getPersistedAgentUser().isEmpty()) 
    		&& !this.validate(this.getEmailAddress(), VALIDATE_EMAIL)) {
      this.savePolicyHolderContact(this.getEmailAddress());
      this.loadFirstContactPolicyHolder();
      return this.validationFailed("Email", VALIDATE_EMAIL, false);

    } else if (!this.validate(this.getPostcode(), VALIDATE_STRING)) {
      this.savePolicyHolderContact(this.getEmailAddress());
      this.loadFirstContactPolicyHolder();
      return this.validationFailed("Post Code", VALIDATE_STRING, false);

    } else if (!this.validate(this.getSuburb(), VALIDATE_STRING)) {
      this.savePolicyHolderContact(this.getEmailAddress());
      this.loadFirstContactPolicyHolder();
      return this.validationFailed("Suburb", VALIDATE_STRING, false);

      //} else if (!this.validate(this.getStreetSuburbId(), VALIDATE_STRING)) {
      //this.savePolicyHolderContact(this.getEmailAddress());
      //this.loadFirstContactPolicyHolder();
      //return this.validationFailed("Street", VALIDATE_STRING , false);

    } else if ((this.getPobox() == null || this.getPobox().isEmpty())) {
      if ((this.getStreetNumber() == null || this.getStreetNumber().isEmpty())) {
        this.savePolicyHolderContact(this.getEmailAddress());
        this.loadFirstContactPolicyHolder();
        return this.validationFailed("You must fill in either Street Name and Number or Post Box", VALIDATE_CUSTOMIZED, false);
      }

      if (!this.validate(this.getStreetSuburbId(), VALIDATE_STRING)) {
        this.savePolicyHolderContact(this.getEmailAddress());
        this.loadFirstContactPolicyHolder();
        return this.validationFailed("You must fill in either Street Name and Number or Post Box", VALIDATE_CUSTOMIZED, false);
      }
    }

    this.savePolicyHolderContact(this.getEmailAddress());

    // Additinal check.
    // Check fifty plus.
    String fiftyPlusAnswer = insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS);

    if (fiftyPlusAnswer.equals(WebInsQuoteDetail.TRUE)) {
      if (!this.insuranceMgr.checkFiftyPlus(quoteNo)) {
        return this.messagePage(MESSAGE_FIFTYPLUS);
      }
    }

    // Check whether there is no primary contact point.
    boolean noPrimaryContact = true;
    List<WebInsClient> clients = this.insuranceMgr.getClients(quoteNo);

    for (WebInsClient client : clients) {
      if (client.isPreferredAddress()) {
        noPrimaryContact = false;
        break;
      }
    }

    if (noPrimaryContact) {
      return this.messagePage(MESSAGE_NOPRIMARYCONTACT);
    }

    this.setNextAction();
    return SUCCESS;
  }

  @Override
  public void validate() {

    if (this.getSubmit().equals(BUTTON_BACK)) {
      return;
    }

    try {
      
     
      if (!this.validate(this.getResidentialPostcode(), VALIDATE_STRING)) {
        this.addFieldError("residentialPostcode", "Please enter a postcode");
      }

      if (!this.validate(this.getResidentialSuburb(), VALIDATE_STRING)) {
        this.addFieldError("residentialSuburb", "Please select an option");
      }
      
      if (!this.validate(this.getResidentialStreetSuburbId(), VALIDATE_STRING)) {
        this.addFieldError("residentialStreetSuburbId", "Please select an option");
      }
      
      if (!this.validate(this.getResidentialStreetNumber(), VALIDATE_STRING)) {
        this.addFieldError("residentialStreetNumber", "Please enter a street number");
      }
      
      

      if ((this.getPersistedAgentUser() == null || this.getPersistedAgentUser().isEmpty()) 
      		&& !this.validate(this.getEmailAddress(), VALIDATE_EMAIL)) {
        this.addFieldError("emailAddress", "Please enter a valid email");
      }

      if (!this.validate(this.getPostcode(), VALIDATE_STRING)) {
        this.addFieldError("postcode", "Please enter a postcode");
      }

      if (!this.validate(this.getSuburb(), VALIDATE_STRING)) {
        this.addFieldError("suburb", "Please select an option");
      }

      if ((this.getPobox() == null || this.getPobox().isEmpty())) {
        if ((this.getStreetNumber() == null || this.getStreetNumber().isEmpty())) {
          this.addFieldError("streetNumber", "Please enter either Street Name and Number or Post Box");
        }

        if (!this.validate(this.getStreetSuburbId(), VALIDATE_STRING)) {
          this.addFieldError("streetSuburbId", "Please enter either Street Name and Number or Post Box");
        }
      }
      
      

      if (!this.getFieldErrors().isEmpty()) {
        this.savePolicyHolderContact(this.getEmailAddress());
        this.loadFirstContactPolicyHolder();
      }


    } catch (Exception e) {
      Config.logger.debug(Config.key + ": loadpage15 failed on validation: ", e);
    }

  }

  /**
   * @return the emailAddress
   */
  public String getEmailAddress() {
    return emailAddress;
  }

  /**
   * @param emailAddress the emailAddress to set
   */
  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  /**
   * @return the postcode
   */
  public String getPostcode() {
    return postcode;
  }

  /**
   * @param postcode the postcode to set
   */
  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  /**
   * @return the suburb
   */
  public String getSuburb() {
    return suburb;
  }

  /**
   * @param suburb the suburb to set
   */
  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  /**
   * @return the streetSuburbId
   */
  public String getStreetSuburbId() {
    return streetSuburbId;
  }

  /**
   * @param streetSuburbId the streetSuburbId to set
   */
  public void setStreetSuburbId(String streetSuburbId) {
    this.streetSuburbId = streetSuburbId;
  }

  /**
   * @return the streetNumber
   */
  public String getStreetNumber() {
    return streetNumber;
  }

  /**
   * @param streetNumber the streetNumber to set
   */
  public void setStreetNumber(String streetNumber) {
    this.streetNumber = streetNumber;
  }

  /**
   * @return the pobox
   */
  public String getPobox() {
    return pobox;
  }

  /**
   * @param pobox the pobox to set
   */
  public void setPobox(String pobox) {
    this.pobox = pobox;
  }

  /**
   * @return the residentialPostcode
   */
  public String getResidentialPostcode() {
    return residentialPostcode;
  }

  /**
   * @param residentialPostcode the residentialPostcode to set
   */
  public void setResidentialPostcode(String residentialPostcode) {
    this.residentialPostcode = residentialPostcode;
  }

  /**
   * @return the residentialSuburb
   */
  public String getResidentialSuburb() {
    return residentialSuburb;
  }

  /**
   * @param residentialSuburb the residentialSuburb to set
   */
  public void setResidentialSuburb(String residentialSuburb) {
    this.residentialSuburb = residentialSuburb;
  }

  /**
   * @return the residentialStreetSuburbId
   */
  public String getResidentialStreetSuburbId() {
    return residentialStreetSuburbId;
  }

  /**
   * @param residentialStreetSuburbId the residentialStreetSuburbId to set
   */
  public void setResidentialStreetSuburbId(String residentialStreetSuburbId) {
    this.residentialStreetSuburbId = residentialStreetSuburbId;
  }

  /**
   * @return the residentialStreetNumber
   */
  public String getResidentialStreetNumber() {
    return residentialStreetNumber;
  }

  /**
   * @param residentialStreetNumber the residentialStreetNumber to set
   */
  public void setResidentialStreetNumber(String residentialStreetNumber) {
    this.residentialStreetNumber = residentialStreetNumber;
  }


}