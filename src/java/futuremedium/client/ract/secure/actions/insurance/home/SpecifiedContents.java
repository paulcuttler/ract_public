package futuremedium.client.ract.secure.actions.insurance.home;

import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;

/**
 * Store Specified Contents details.
 * @author gnewton
 */
public class SpecifiedContents extends SpecifiedContentsBase implements PolicyAware, ServletRequestAware {
    private static final long serialVersionUID = 2759073032204033731L;
    private Boolean specify;
    private int count;
    private HttpServletRequest servletRequest;

    @Override
    public String execute() throws Exception {

        boolean ok = this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_SPECIFY, this.isSpecify().toString());
        ok = ok && this.getHomeService().storeSpecifiedContents(this);

        if (ok) {
            // this.setActionMessage(this.getSuccessMessage());
        } else {
            this.setActionMessage(this.getErrorMessage());
            return ERROR;
        }

        int totalSpecifiedContentsValue = calculateTotalSpecifiedContents();
        int totalContentsSumInsured = Integer.parseInt(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.CNTS_SUM_INSURED));
        float percent = (totalSpecifiedContentsValue * 100.0f) / totalContentsSumInsured;
        String abortMessage = Config.configManager.getProperty("insurance.home.contents.exclusion.specifiedContentsSumOver.message");

        if (percent > getMaxSpecifiedContentsPercentage()) {
            return this.getHomeService().abortQuote(this, "SpecifiedContents > " + getMaxSpecifiedContentsPercentage() + "% of ContentsSumInsured", String.valueOf(percent), abortMessage);
        }

        return SUCCESS;
    }

    public void validate() {
        if (this.isSpecify() != null && this.isSpecify()) {
            boolean found = false;
            for (int i = 1; i <= this.getCount(); i++) {
                String type = this.getServletRequest().getParameter("type" + i);
                String description = this.getServletRequest().getParameter("description" + i);
                String sumInsured = this.getServletRequest().getParameter("sumInsured" + i);

                if ((type != null && !type.isEmpty()) || (description != null && !description.isEmpty()) || (sumInsured != null && !sumInsured.isEmpty())) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                this.addFieldError("specify", "Please enter your specified items");
            } else {
                int minValue = getMinSpecifiedContentsValue();
                DecimalFormat df = new DecimalFormat("�#,###");

                for (int i = 1; i <= this.getCount(); i++) {
                    String sumInsured = this.getServletRequest().getParameter("sumInsured" + i);
                    if (sumInsured != null) {
                        sumInsured = sumInsured.replaceAll("\\$", "").replaceAll(",", "");
                        try {
                            int value = Integer.parseInt(sumInsured);
                            if (value < minValue) {
                                this.addFieldError("specify", "Items less than " + df.format(minValue) + " do not have to be listed");
                                break;
                            }
                        } catch (Exception e) {
                            Config.logger.debug(Config.key + ": could not parse sumInsured" + i + ": " + sumInsured + " as an int", e);
                        }
                    } else {
                        Config.logger.debug(Config.key + ": sumInsured" + i + " is null");
                    }
                }
            }
        }
    }

  /**
   * @return the specify
   */
  public Boolean isSpecify() {
    return specify;
  }

  /**
   * @param specify the specify to set
   */
  @RequiredFieldValidator(message = "Please answer this question", type = ValidatorType.SIMPLE)
  public void setSpecify(Boolean specify) {
    this.specify = specify;
  }

  /**
   * @return the servletRequest
   */
  public HttpServletRequest getServletRequest() {
    return servletRequest;
  }

  /**
   * @param servletRequest the servletRequest to set
   */
  @Override
  public void setServletRequest(HttpServletRequest servletRequest) {
    this.servletRequest = servletRequest;
  }

  /**
   * @return the count
   */
  public int getCount() {
    return count;
  }

  /**
   * @param count the count to set
   */
  public void setCount(int count) {
    this.count = count;
  }
  
    private int calculateTotalSpecifiedContents() {
        int totalSpecifiedContentsValue = 0;

        if (this.isSpecify() == null || !this.isSpecify()) {
            return totalSpecifiedContentsValue;
        }

        for (int i = 1; i <= this.getCount(); i++) {
            String sumInsured = this.getServletRequest().getParameter("sumInsured" + i);
            if (sumInsured != null) {
                sumInsured = sumInsured.replaceAll("\\$", "").replaceAll(",", "");
                try {
                    int value = Integer.parseInt(sumInsured);
                    totalSpecifiedContentsValue += value;
                } catch (Exception e) {
                    Config.logger.debug(Config.key + ": could not parse sumInsured" + i + ": " + sumInsured + " as an int", e);
                }
            } else {
                Config.logger.debug(Config.key + ": sumInsured" + i + " is null");
            }
        }
        return totalSpecifiedContentsValue;
    }
}
