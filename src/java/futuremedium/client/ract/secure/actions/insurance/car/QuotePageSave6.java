package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.*;

import com.ract.util.DateTime;
import com.ract.web.insurance.GlVehicle;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuoteDetail;
import futuremedium.client.ract.secure.Config;

/**
 * Page 6 action. Client details.
 * @author xmfu
 */
public class QuotePageSave6 extends QuotePageSave {

    private String firstName1;
    private String firstName2;
    private String lastName1;
    private String lastName2;
    private String sex1;
    private String sex2;
    private String dob1;
    private String dob2;
    private String frequence1;
    private String frequence2;

    @Override
    public String execute() throws Exception {
        this.setUp();
        int quoteNo = this.getQuoteNo();

        // If user has reached page 9, access from page 1 to page 8 is not allowed.
        if (isReachedPage9()) {
            this.setNextAction();
            return SUCCESS;
        }

        // Go back to previous page.
        if (this.getSubmit().equals(BUTTON_BACK)) {
            this.setNextAction();
            return SUCCESS;
        }

        // Create client 1.
        WebInsClient driver1 = new WebInsClient();
        driver1.setDriver(true);
        driver1.setGivenNames(getFirstName1());
        driver1.setSurname(getLastName1());
        driver1.setGender(this.getSex1());

        if (this.validate(this.getDob1(), VALIDATE_DATETIME_STRING)) {
            driver1.setBirthDate(new DateTime(this.getDob1()));
        }

        driver1.setDrivingFrequency(this.getFrequence1());
        List<WebInsClient> drivers = new ArrayList<WebInsClient>();
        // Add client 1 to the drivers list for page loading after validation failed.
        drivers.add(driver1);

        // Create client 2.
        WebInsClient driver2 = new WebInsClient();
        driver2.setDriver(true);
        driver2.setGivenNames(getFirstName2());
        driver2.setSurname(getLastName2());
        driver2.setGender(this.getSex2());

        if (this.validate(this.getDob2(), VALIDATE_DATETIME_STRING)) {
            driver2.setBirthDate(new DateTime(this.getDob2()));
        }

        driver2.setDrivingFrequency(this.getFrequence2());
        // Add client 2 to the drivers list for page loading after validation failed.
        drivers.add(driver2);

        // Validation for client1.
        if (!this.validate(this.getFirstName1(), VALIDATE_NAME)) {
            this.loadPage6(drivers);
            return this.validationFailed("First name of Driver 1", VALIDATE_NAME, true);
        } else if (!this.validate(this.getLastName1(), VALIDATE_NAME)) {
            this.loadPage6(drivers);
            return this.validationFailed("Last name of Driver 1", VALIDATE_NAME, false);
        } else if (!this.validate(this.getDob1(), VALIDATE_DATETIME_STRING)) {
            this.loadPage6(drivers);
            return this.validationFailed("Date of Birth of Driver 1", VALIDATE_DATETIME_STRING, false);
        } else if (!this.validate(this.getDob1(), VALIDATE_DOB)) {
            this.loadPage6(drivers);
            return this.validationFailed("", VALIDATE_DOB, false);
        } else {
            // Validate client2 if any.
            if (this.validate(this.getDob2(), VALIDATE_DATETIME_STRING)
                    || this.validate(this.getFirstName2(), VALIDATE_STRING)
                    || this.validate(this.getLastName2(), VALIDATE_STRING)) {
                WebInsClient driver = new WebInsClient();
                driver.setDriver(true);
                driver.setGivenNames(getFirstName2());
                driver.setSurname(getLastName2());
                driver.setGender(this.getSex2());

                if (this.validate(this.getDob2(), VALIDATE_DATETIME_STRING)) {
                    driver.setBirthDate(new DateTime(this.getDob2()));
                }

                driver.setDrivingFrequency(this.getFrequence2());
                drivers.add(driver);

                if (!this.validate(this.getFirstName2(), VALIDATE_NAME)) {
                    this.loadPage6(drivers);
                    return this.validationFailed("First name of Driver 2", VALIDATE_NAME, false);
                } else if (!this.validate(this.getLastName2(), VALIDATE_NAME)) {
                    this.loadPage6(drivers);
                    return this.validationFailed("Last name of Driver 2", VALIDATE_NAME, false);
                } else if (!this.validate(this.getDob2(), VALIDATE_DATETIME_STRING)) {
                    this.loadPage6(drivers);
                    return this.validationFailed("Date of birth of Driver 2", VALIDATE_DATETIME_STRING, false);
                } else if (!this.validate(this.getDob2(), VALIDATE_DOB)) {
                    this.loadPage6(drivers);
                    return this.validationFailed("", VALIDATE_DOB, false);
                }
            }

            // Save Dirver 1.
            List<WebInsClient> savedDrivers = this.getDrivers();
            driver1 = null;
            driver2 = null;

            if (savedDrivers.size() > 0) {
                driver1 = savedDrivers.get(0);
                if (savedDrivers.size() > 1) {
                    driver2 = savedDrivers.get(1);
                }
            } else {
                driver1 = insuranceMgr.createInsuranceClient(quoteNo);
            }

            driver1.setDriver(true);
            driver1.setWebQuoteNo(quoteNo);
            driver1.setGivenNames(getFirstName1());
            driver1.setSurname(getLastName1());

            if (this.getSex1().equals("Male")) {
                driver1.setGender(WebInsClient.GENDER_MALE);
            } else if (this.getSex1().equals("Female")) {
                driver1.setGender(WebInsClient.GENDER_FEMALE);
            }

            driver1.setBirthDate(new DateTime(this.getDob1()));
            driver1.setDrivingFrequency(getFrequence1());

            insuranceMgr.setClient(driver1);

            // Save Driver 2.
            if (this.getFirstName2() != null && !this.getFirstName2().equals("")) {

                if (driver2 == null) {
                    driver2 = insuranceMgr.createInsuranceClient(quoteNo);
                }

                driver2.setDriver(true);
                driver2.setWebQuoteNo(quoteNo);
                driver2.setGivenNames(getFirstName2());
                driver2.setSurname(getLastName2());

                if (this.getSex2().equals("Male")) {
                    driver2.setGender(WebInsClient.GENDER_MALE);
                } else if (this.getSex2().equals("Female")) {
                    driver2.setGender(WebInsClient.GENDER_FEMALE);
                }

                driver2.setBirthDate(new DateTime(this.getDob2()));
                driver2.setDrivingFrequency(getFrequence2());

                insuranceMgr.setClient(driver2);
            }

            // Get the rating driver.
            WebInsClient ratingDriver = insuranceMgr.getRatingDriver(quoteNo);
            insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.DRIVER_DOB, ratingDriver.getBirthDate().formatShortDate());

            // Check whether the rating driver is over 25 years old.
            boolean below25 = false;

            Calendar cutoff = Calendar.getInstance();
            cutoff.add(Calendar.YEAR, -25);

            Calendar dob = Calendar.getInstance();
            dob.setTime(ratingDriver.getBirthDate());

            if (dob.after(cutoff)) {
                below25 = true;
            }

            // Below 25 years old and driving a high performance vehicle is not allowed to continue the quote.
            if (below25) {
                GlVehicle vehicle = this.getVehicle();
                if (vehicle.getAcceptable().trim().equalsIgnoreCase(VALUE_HIGH_PERFORMANCE_VEHICLE)) {
                    return this.messagePage(MESSAGE_HIGHPERFORMANCE);
                }
            }

            if (insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS).equals(WebInsQuoteDetail.TRUE)) {
                // check both drivers are over 50 if user has answered 'yes' to 50+ question
                if (!isOlderThan(driver1.getBirthDate(), 50)) {
                    return this.messagePage(MESSAGE_FIFTYPLUS);
                }

                if (driver2 != null) {
                    if (!isOlderThan(driver2.getBirthDate(), 50)) {
                        return this.messagePage(MESSAGE_FIFTYPLUS);
                    }
                }
            }

            this.setNextAction();
            return SUCCESS;
        }
    }

    @Override
    public void validate() {

        if (this.getSubmit().equals(BUTTON_BACK)) {
            return;
        }

        try {

            //Construct a temp vehicle bean to hold submitted data. The only purpose is to reload page after validation failed.
            List<WebInsClient> drivers = new ArrayList<WebInsClient>();

            WebInsClient driver1 = new WebInsClient();
            driver1.setDriver(true);
            driver1.setGivenNames(this.getFirstName1());
            driver1.setSurname(this.getLastName1());
            driver1.setGender(this.getSex1());
            driver1.setDrivingFrequency(this.getFrequence1());

            try {
                Date dobDate = dateFormat.parse(this.getDob1());
                DateTime dob = new DateTime(dobDate);
                driver1.setBirthDate(dob);
            } catch (Exception e) {
                this.addFieldError("dob1", "Please enter a valid date of birth");
            }
            
            if (!this.validate(this.getDob1(), VALIDATE_DOB)) {
                this.addFieldError("dob1", getValidationMessage("", VALIDATE_DOB));
            }

            drivers.add(driver1);

            if (!this.validate(this.getFirstName1(), VALIDATE_NAME)) {
                this.addFieldError("firstName1", getValidationMessage("First name", VALIDATE_NAME));
            }
            if (!this.validate(this.getLastName1(), VALIDATE_NAME)) {
                this.addFieldError("lastName1", getValidationMessage("Last name", VALIDATE_NAME));
            }
            
            if (this.getSex1() == null || this.getSex1().isEmpty()) {
                this.addFieldError("sex1", "Please select an option");
            }
            
            if (this.getFrequence1() == null || this.getFrequence1().isEmpty()) {
                this.addFieldError("frequence1", "Please select how often the person drives");
            }
            
            // Validate client2 if any.
            if (this.validate(this.getDob2(), VALIDATE_DATETIME_STRING)
                    || this.validate(this.getFirstName2(), VALIDATE_STRING)
                    || this.validate(this.getLastName2(), VALIDATE_STRING)) {

                WebInsClient driver2 = new WebInsClient();
                driver2.setDriver(true);
                driver2.setGivenNames(this.getFirstName2());
                driver2.setSurname(this.getLastName2());
                driver2.setGender(this.getSex2());
                driver2.setDrivingFrequency(this.getFrequence2());

                try {
                    Date dobDate = dateFormat.parse(this.getDob2());
                    DateTime dob = new DateTime(dobDate);
                    driver2.setBirthDate(dob);
                } catch (Exception e) {
                    this.addFieldError("dob2", "Please enter a valid date of birth");
                }
                
                if (!this.validate(this.getDob2(), VALIDATE_DOB)) {
                    this.addFieldError("dob2", getValidationMessage("", VALIDATE_DOB));
                }

                drivers.add(driver2);

                if (!this.validate(this.getFirstName2(), VALIDATE_NAME)) {
                    this.addFieldError("firstName2", getValidationMessage("First name", VALIDATE_NAME));
                }
                if (!this.validate(this.getLastName2(), VALIDATE_NAME)) {
                    this.addFieldError("lastName2", getValidationMessage("Last name", VALIDATE_NAME));
                }
                
                if (this.getSex2() == null || this.getSex2().isEmpty()) {
                  this.addFieldError("sex2", "Please select an option");
                }

                if (this.getFrequence2() == null || this.getFrequence2().isEmpty()) {
                    this.addFieldError("frequence2", "Please select how often the person drives");
                }
            }

            if (!this.getFieldErrors().isEmpty()) {
                this.loadPage6(drivers);
            }

        } catch (Exception e) {
            Config.logger.debug(Config.key + ": loadpage6 failed on validation: ", e);
        }

    }

    /**
     * @return the firstName1
     */
    public String getFirstName1() {
        return firstName1;
    }

    /**
     * @param firstName1 the firstName1 to set
     */
    public void setFirstName1(String firstName1) {
        this.firstName1 = firstName1;
    }

    /**
     * @return the firstName2
     */
    public String getFirstName2() {
        return firstName2;
    }

    /**
     * @param firstName2 the firstName2 to set
     */
    public void setFirstName2(String firstName2) {
        this.firstName2 = firstName2;
    }

    /**
     * @return the lastName1
     */
    public String getLastName1() {
        return lastName1;
    }

    /**
     * @param lastName1 the lastName1 to set
     */
    public void setLastName1(String lastName1) {
        this.lastName1 = lastName1;
    }

    /**
     * @return the lastName2
     */
    public String getLastName2() {
        return lastName2;
    }

    /**
     * @param lastName2 the lastName2 to set
     */
    public void setLastName2(String lastName2) {
        this.lastName2 = lastName2;
    }

    /**
     * @return the sex1
     */
    public String getSex1() {
        return sex1;
    }

    /**
     * @param sex1 the sex1 to set
     */
    public void setSex1(String sex1) {
        this.sex1 = sex1;
    }

    /**
     * @return the sex2
     */
    public String getSex2() {
        return sex2;
    }

    /**
     * @param sex2 the sex2 to set
     */
    public void setSex2(String sex2) {
        this.sex2 = sex2;
    }

    /**
     * @return the dob1
     */
    public String getDob1() {
        return dob1;
    }

    /**
     * @param dob1 the dob1 to set
     */
    public void setDob1(String dob1) {
        this.dob1 = dob1;
    }

    /**
     * @return the dob2
     */
    public String getDob2() {
        return dob2;
    }

    /**
     * @param dob2 the dob2 to set
     */
    public void setDob2(String dob2) {
        this.dob2 = dob2;
    }

    /**
     * @return the frequence1
     */
    public String getFrequence1() {
        return frequence1;
    }

    /**
     * @param frequence1 the frequence1 to set
     */
    public void setFrequence1(String frequence1) {
        this.frequence1 = frequence1;
    }

    /**
     * @return the frequence2
     */
    public String getFrequence2() {
        return frequence2;
    }

    /**
     * @param frequence2 the frequence2 to set
     */
    public void setFrequence2(String frequence2) {
        this.frequence2 = frequence2;
    }
}