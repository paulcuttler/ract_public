package futuremedium.client.ract.secure.actions.insurance.car;

import java.text.ParseException;
import java.util.Date;

import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.actions.insurance.InsuranceBase;
import futuremedium.client.ract.secure.actions.insurance.home.ActionBase;

/**
 * Payment Page.
 * @author xmfu
 */
public class QuotePageSave17 extends QuotePageSave {

    private static final long serialVersionUID = 4084823016347182896L;
    private String paymentOption;
  private String accountType;
  private String payByDirectDebit;
  private String accountName;
  private String expiryDate;
  private String directDebitDate = "1";
  private String cardType;
  private String creditCardName;
  private String creditCard1;
  private String creditCard2;
  private String creditCard3;
  private String creditCard4;
  private String bankcard1;
  private String bankcard2;
  private String bankcard3;
  private String frequency;
  private String bankingConditions;

  @Override
  public String execute() throws Exception {
    this.setUp();
    int quoteNo = this.getQuoteNo();

    if (this.getSubmit().equals(BUTTON_BACK)) {
      this.setNextAction();
      return SUCCESS;
    }
    
    this.getInsuranceMgr().setQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_METHOD, this.getPaymentOption());

    // Martial the payment details into the appropriate fields for submission 
    String type = "";
    String number = "";
    String name = "";
    if (this.getAccountType().equals(ActionBase.FIELD_DD_BANK_ACCOUNT)) {
        type = WebInsQuoteDetail.ACC_TYPE_DEBIT;
        number = this.getAccountNo();
        name = this.getAccountName();
    } else {
        type = this.getCardType();
        number = this.getCreditCard();
        name = this.getCreditCardName();
    }
    String bsb = null == this.getBsb() ? "" : this.getBsb();
    String expiry = null == this.getExpiryDate() ? "" : this.getExpiryDate();
    String day = null == this.getDirectDebitDate() ? "" : this.getDirectDebitDate();
    String frequency = null == this.getFrequency() ? "" : this.getFrequency();
     
    this.getInsuranceMgr().setQuoteStatus(quoteNo, WebInsQuote.COMPLETE);
    
    try {
        this.insuranceMgr.startCover(quoteNo, type, bsb, number, expiry, name, day, frequency);
    } catch (Exception e) {
        return this.messagePage(MESSAGE_SAVE_FAIL);
    }

    WebInsClient primaryContact = this.getPrimaryContactPolciyHolder();
    String confirmationEmail = primaryContact.getEmailAddress();
    this.insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.CONFIRMATION_EMAIL_ADDRESS, confirmationEmail);
    if (confirmationEmail != null && !confirmationEmail.isEmpty()) {
        this.insuranceMgr.printCoverDoc(quoteNo);
    }

    //this.insuranceMgr.setQuoteStatus(quoteNo, WebInsQuote.COVER_ISSUED);


    
    this.setNextAction();

    return SUCCESS;
  }

    @Override
    public void validate() {

        if (this.getSubmit().equals(BUTTON_BACK)) {
            return;
        }

        if (this.getPaymentOption() != null && !this.getPaymentOption().isEmpty()) {

            if (this.getPaymentOption().equals(WebInsQuoteDetail.PAY_ANNUALLY) || (this.getPaymentOption().equals(WebInsQuoteDetail.PAY_DD) && this.getAccountType() != null && this.getAccountType().equals(InsuranceBase.FIELD_DD_CREDIT_CARD))) {

                if (this.getCardType() == null || this.getCardType().isEmpty()) {
                    this.addFieldError("cardType", "Please select a card type");
                }
                if (this.getCreditCard() == null || !validateCreditCardNumber(this.getCreditCard())) {
                    this.addFieldError("creditCard1", "Please enter a valid credit card number");
                }
                if (this.getCreditCardName() == null || this.getCreditCardName().isEmpty()) {
                    this.addFieldError("creditCardName", "Please enter the name on credit card");
                }
                if (this.getExpiryDate() == null || this.getExpiryDate().isEmpty()) {
                    this.addFieldError("expiryDate", "Please enter an expiry date");
                } else {
                    try {
                        if (!validate(this.getExpiryDate(), VALIDATE_CREDIT_CARD_DATE)) {
                            this.addFieldError("expiryDate", "Please enter a valid expiry date");
                        }
                    } catch (Exception e) {
                        this.addFieldError("expiryDate", "Please enter a valid expiry date");
                    }

                    try {
                        Date date = creditCardDateFormat.parse(this.getExpiryDate());

                        Date currentDate = new Date();
                        if (date.getTime() < currentDate.getTime()) { // expiry date in the past
                            addFieldError("expiryDate", "Please enter a current expiry date.");
                        }
                    } catch (ParseException e) {
                        this.addFieldError("expiryDate", "Please enter a valid expiry date");
                    }
                }
            }

            if (this.getPaymentOption().equals(WebInsQuoteDetail.PAY_DD)) {

                if (this.getFrequency() == null || this.getFrequency().isEmpty()) {
                    this.addFieldError("frequency", "Please select a frequency");
                }

                if (this.getAccountType() == null || this.getAccountType().isEmpty()) {
                    this.addFieldError("accountType", "Please select a payment method");
                } else if (this.getAccountType().equals(InsuranceBase.FIELD_DD_BANK_ACCOUNT)) {
                    if (this.getBsb() == null || this.getBsb().isEmpty()) {
                        this.addFieldError("bankcard", "Please enter your BSB");
                    } else if (this.getBsb().length() != 6) {
                        this.addFieldError("bankcard", "Your BSB must consist of two 3 digit numbers.");
                    }
                    if (this.getAccountNo() == null || this.getAccountNo().isEmpty()) {
                        this.addFieldError("bankcard", "Please enter your Account number");
                    }
                    if (this.getAccountName() == null || this.getAccountName().isEmpty()) {
                        this.addFieldError("accountName", "Please enter your Account name");
                    }
                    if (this.getBankingConditions() == null || !this.getBankingConditions().equals("checked")) {
                        this.addFieldError("bankingConditions", "You must agree to the terms of the Direct Debit Request Service Agreement.");
                    }
                }

                if (this.getDirectDebitDate() == null || this.getDirectDebitDate().isEmpty()) {
                    this.addFieldError("directDebitDate", "Please enter your payment day");
                } else {
                    try {
                        int day = Integer.parseInt(this.getDirectDebitDate());
                        if (day < 1 || day > 31) {
                            throw new Exception("Payment Day out of range: " + day);
                        }
                    } catch (Exception e) {
                        this.addFieldError("directDebitDate", "Please enter a payment day between 1 and 31");
                    }
                }
            }
        } else {
            this.addFieldError("paymentOptions", "Please select a payment option");
        }

        if (!this.getFieldErrors().isEmpty()) {

            // System.out.println(this.getFieldErrors().toString());

            this.loadPaymentPage();
        }
    }
  
  /**
   * Reload submitted data if validation failed.
   */
  public void loadPaymentPage() {
    this.setShowBenefits(true);
    
    this.loadQuestion(this.getQuestionById("paymentOptions"), this.getPaymentOption());
    
    // pay freq
    this.loadQuestion(this.getQuestionById("q26"), this.getFrequency());
    
    // DD type
    this.loadQuestion(this.getSubQuestionById("q27", "sub2"), this.getAccountType());
    		
    //this.getHttpRequest().setAttribute("bankcard1", this.getBankcard1());
    //this.getHttpRequest().setAttribute("bankcard2", this.getBankcard2());
    //this.getHttpRequest().setAttribute("bankcard3", this.getBankcard3());
    
    this.loadQuestion(this.getSubQuestionById("q27", "sub1"), this.getAccountName());
    this.loadQuestion(this.getSubQuestionById("q27", "ccName"), this.getCreditCardName());
    this.loadQuestion(this.getSubQuestionById("q27", "selectCardType"), this.getCardType());
    this.loadQuestion(this.getSubQuestionById("q27", "sub4"), this.getExpiryDate());
    this.loadQuestion(this.getSubQuestionById("q27", "sub6"), this.getDirectDebitDate());
  }

  /**
   * @return the accountType
   */
  public String getAccountType() {
    return accountType;
  }

  /**
   * @param accountType the accountType to set
   */
  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  /**
   * @return the accountName
   */
  public String getAccountName() {
    return accountName;
  }

  /**
   * @param accountName the accountName to set
   */
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  /**
   * @return the expiryDate
   */
  public String getExpiryDate() {
    return expiryDate;
  }

  /**
   * @param expiryDate the expiryDate to set
   */
  public void setExpiryDate(String expiryDate) {
    this.expiryDate = expiryDate;
  }

  /**
   * Get complete credit card number
   */
  public String getCreditCard() {
    if (this.getCreditCard1() != null && this.getCreditCard2() != null && this.getCreditCard3() != null && this.getCreditCard4() != null) {
      return this.getCreditCard1() + this.getCreditCard2() + this.getCreditCard3() + this.getCreditCard4();
    } else {
      return null;
    }
  }
  
  /**
   * @return the creditCard1
   */
  public String getCreditCard1() {
    return creditCard1;
  }

  /**
   * @param creditCard1 the creditCard1 to set
   */
  public void setCreditCard1(String creditCard1) {
    this.creditCard1 = creditCard1;
  }

  /**
   * @return the creditCard2
   */
  public String getCreditCard2() {
    return creditCard2;
  }

  /**
   * @param creditCard2 the creditCard2 to set
   */
  public void setCreditCard2(String creditCard2) {
    this.creditCard2 = creditCard2;
  }

  /**
   * @return the creditCard3
   */
  public String getCreditCard3() {
    return creditCard3;
  }

  /**
   * @param creditCard3 the creditCard3 to set
   */
  public void setCreditCard3(String creditCard3) {
    this.creditCard3 = creditCard3;
  }

  /**
   * @return the creditCard4
   */
  public String getCreditCard4() {
    return creditCard4;
  }

  /**
   * @param creditCard4 the creditCard4 to set
   */
  public void setCreditCard4(String creditCard4) {
    this.creditCard4 = creditCard4;
  }

  /**
   * @return the bankcard1
   */
  public String getBankcard1() {
    return bankcard1;
  }

  /**
   * @param bankcard1 the bankcard1 to set
   */
  public void setBankcard1(String bankcard1) {
    this.bankcard1 = bankcard1;
  }

  /**
   * @return the bankcard2
   */
  public String getBankcard2() {
    return bankcard2;
  }

  /**
   * @param bankcard2 the bankcard2 to set
   */
  public void setBankcard2(String bankcard2) {
    this.bankcard2 = bankcard2;
  }
  
  private String getBsb() {
  	return this.getBankcard1() + this.getBankcard2();
  }
  
  private String getAccountNo() {
  	return this.getBankcard3();
  }

  /**
   * @return the directDebitDate
   */
  public String getDirectDebitDate() {
    return directDebitDate;
  }

  /**
   * @param directDebitDate the directDebitDate to set
   */
  public void setDirectDebitDate(String directDebitDate) {
    this.directDebitDate = directDebitDate;
  }

  /**
   * @return the payByDirectDebit
   */
  public String getPayByDirectDebit() {
    return payByDirectDebit;
  }

  /**
   * @param payByDirectDebit the payByDirectDebit to set
   */
  public void setPayByDirectDebit(String payByDirectDebit) {
    this.payByDirectDebit = payByDirectDebit;
  }

  /**
   * @return the paymentFrequency
   */
  public String getFrequency() {
    return frequency;
  }

  /**
   * @param paymentFrequency the paymentFrequency to set
   */
  public void setFrequency(String paymentFrequency) {
    this.frequency = paymentFrequency;
  }

  /**
   * @return the bankcard3
   */
  public String getBankcard3() {
    return bankcard3;
  }

  /**
   * @param bankcard3 the bankcard3 to set
   */
  public void setBankcard3(String bankcard3) {
    this.bankcard3 = bankcard3;
  }

	public String getCreditCardName() {
		return creditCardName;
	}

	public void setCreditCardName(String creditCardName) {
		this.creditCardName = creditCardName;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(String paymentOptions) {
		this.paymentOption = paymentOptions;
	}
	
	public String getCardType() {
		return cardType;
	}
	
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

    public String getBankingConditions() {
        return bankingConditions;
    }

    public void setBankingConditions(String bankingConditions) {
        this.bankingConditions = bankingConditions;
    }
}
