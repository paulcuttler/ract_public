package futuremedium.client.ract.secure.actions.insurance.home;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

/**
 *
 * @author gnewton
 */
public class Exit extends ExitBase {

  private static final String FIELD_EXIT_REASON = "exitReason";

  private String reason;

  @Override
  public String execute() throws Exception {

    // save, don't update last action
    boolean ok = this.getHomeService().storeQuoteDetail(this, FIELD_EXIT_REASON, this.getReason(), null, false);

    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    return SUCCESS;
  }

  @Override
  public String getNextAction() {
    return "HomeReminderForm";
  }

  /**
   * @return the reason
   */
  public String getReason() {
    return reason;
  }

  /**
   * @param reason the reason to set
   */
  @RequiredStringValidator(message="Please select a reason")
  public void setReason(String reason) {
    this.reason = reason;
  }

}
