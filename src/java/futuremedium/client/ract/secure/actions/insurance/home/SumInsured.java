package futuremedium.client.ract.secure.actions.insurance.home;

import futuremedium.client.ract.secure.Config;

import com.ract.web.insurance.WebInsQuoteDetail;

/**
 * @author gnewton
 */
public class SumInsured extends QuoteSubTypeSupport implements QuoteAware {

  /*
   * building and investor sums insured are stored in session as they may be
   * pre-filled on Calculator page.
   */
  private Integer contentsSumInsured;
  private boolean upgrade;

  @Override
  public String execute() throws Exception {

    boolean ok = saveSumInsured();

    String result = testExclusions();

    // clear building and investor sum insured from session
    this.setBuildingSumInsured(null);
    this.setInvestorSumInsured(null);

   

    // got an exclusion so exit
    if (result != null) {
      return result;
    }

    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }
    
    return SUCCESS;
  }

  /**
   * Save values to DB.
   * @return
   */
  private boolean saveSumInsured() {

    boolean ok = true;

    if (this.isBuildingOptionsRequired()) {
      ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.BLDG_SUM_INSURED, this.getBuildingSumInsured().toString());
    }
    if (this.isContentsOptionsRequired()) {
      ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.CNTS_SUM_INSURED, this.getContentsSumInsured().toString());
    }
    if (this.isInvestorOptionsRequired()) {
      ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.INV_SUM_INSURED, this.getInvestorSumInsured().toString());
    }

    return ok;
  }

  /**
   * Test for exclusion conditions.
   * @return @NOTICE if exclusion found, null otherwise.
   */
  private String testExclusions() {

    String underAbortMessage = Config.configManager.getProperty("insurance.home.exclusion.sumInsuredUnder.message");
    String overAbortMessage = Config.configManager.getProperty("insurance.home.exclusion.sumInsuredOver.message");

    // check building sum insured value
    if (this.isBuildingOptionsRequired()) {
      // below min
      if (this.getBuildingSumInsured() < this.getHomeService().getMinBuildingSumInsuredAllowed(this)) {
        return this.getHomeService().abortQuote(this,
                "building sum insured below min",
                this.getBuildingSumInsured().toString(),
                underAbortMessage);
      }

      // above max
      if (this.getBuildingSumInsured() > this.getHomeService().getMaxBuildingSumInsuredAllowed(this)) {
        return this.getHomeService().abortQuote(this,
                "building sum insured above max",
                this.getBuildingSumInsured().toString(),
                overAbortMessage);
      }
    }

    // check contents sum insured value
    if (this.isContentsOptionsRequired()) {
      // below min
      if (this.getContentsSumInsured() < this.getHomeService().getMinContentsSumInsuredAllowed(this)) {
        return this.getHomeService().abortQuote(this,
                "contents sum insured below min",
                this.getContentsSumInsured().toString(),
                underAbortMessage);
      }

      // above max
      if (this.getContentsSumInsured() > this.getHomeService().getMaxContentsSumInsuredAllowed(this)) {
        return this.getHomeService().abortQuote(this,
                "contents sum insured above max",
                this.getContentsSumInsured().toString(),
                overAbortMessage);
      }
    }

    // check investor sum insured value
    if (this.isInvestorOptionsRequired()) {
      // below min
      if (this.getInvestorSumInsured() < this.getHomeService().getMinInvestorSumInsuredAllowed(this)) {
        return this.getHomeService().abortQuote(this,
                "investor sum insured below min",
                this.getInvestorSumInsured().toString(),
                underAbortMessage);
      }

      // above max
      if (this.getInvestorSumInsured() > this.getHomeService().getMaxInvestorSumInsuredAllowed(this)) {
        return this.getHomeService().abortQuote(this,
                "investor sum insured above max",
                this.getInvestorSumInsured().toString(),
                overAbortMessage);
      }
    }

    // no exceptions
    return null;
  }

  @Override
  public void validate() {

    // if we have an upgradable quote type
    if (this.isBuildingOnlyQuote() || this.isContentsOnlyQuote()) {
      if (this.isUpgrade()) { // if the user wants to upgrade
        // upgrade to Building & Contents and allow validation to handle the rest
        this.getHomeService().upgradeCover(this);

      }
    }

    if (this.isBuildingOptionsRequired()) {
      if (this.getBuildingSumInsured() == null) {
        this.addFieldError("buildingSumInsured", "Please enter a Building Sum Insured");
      }
    }
    if (this.isContentsOptionsRequired()) {
      if (this.getContentsSumInsured() == null) {
        this.addFieldError("contentsSumInsured", "Please enter a Contents Sum Insured");
      }
    }
    if (this.isInvestorOptionsRequired()) {
      if (this.getInvestorSumInsured() == null) {
        this.addFieldError("investorSumInsured", "Please enter a Building Sum Insured");
      }
    }

    if (this.getCaptchaResponse() == null || this.getCaptchaResponse().isEmpty()) {
      this.addFieldError("captchaResponse", "Please enter the security phrase");
    } else if (!this.verifyCaptcha()) {
      this.addFieldError("captchaResponse", "Please enter the correct security phrase");
    }
  }

  /**
   * @return the contentsSumInsured
   */
  public Integer getContentsSumInsured() {
    return contentsSumInsured;
  }

  /**
   * @param contentsSumInsured the contentsSumInsured to set
   */
  public void setContentsSumInsured(Integer contentsSumInsured) {
    this.contentsSumInsured = contentsSumInsured;
  }

  /**
   * @return the upgrade
   */
  public boolean isUpgrade() {
    return upgrade;
  }

  /**
   * @param upgrade the upgrade to set
   */
  public void setUpgrade(boolean upgrade) {
    this.upgrade = upgrade;
  }
}
