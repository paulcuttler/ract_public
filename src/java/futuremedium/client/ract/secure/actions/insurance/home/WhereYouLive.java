package futuremedium.client.ract.secure.actions.insurance.home;

import com.ract.web.insurance.WebInsQuoteDetail;
import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.insurance.QasResidentialAware;

/**
 * Save Where You Live form.
 * @author gnewton
 */
public class WhereYouLive extends WhereYouLiveBase implements QuoteAware, QasResidentialAware {

  private String moniker;
	private String residentialAddressQuery;
  private String residentialPostcode;
  private String residentialSuburb;
  private String residentialStreet;
  private String residentialStreetNumber;
  private String residentialLatitude;
  private String residentialLongitude;
  private String residentialGnaf;

  @Override
  public String execute() throws Exception {

  	if (this.getMoniker() != null && !this.getMoniker().isEmpty()) {
  		// populate address fields via QAS
  		this.getQasService().populateAddressFields(this);
  	}
  	
    boolean ok = this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SITUATION_NO, this.getResidentialStreetNumber());
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SITUATION_STREET, this.getResidentialStreet());
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SITUATION_POSTCODE, this.getResidentialPostcode());
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SITUATION_SUBURB, this.getResidentialSuburb());
    
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SITUATION_LATITUDE, this.getResidentialLatitude());
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SITUATION_LONGITUDE, this.getResidentialLongitude());
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SITUATION_GNAF, this.getResidentialGnaf());
    
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SITUATION_ID, this.getHomeService().lookupStreetSuburbId(this));
    ok = ok && this.getHomeService().storeQuoteEffectiveDate(this);

    // check suburb is acceptable
    if (this.getOptionService().isEmbargoSuburb(this)) {
        return this.getHomeService().abortQuote(this,
                "unacceptable suburb",
                this.getResidentialSuburb(),
                Config.configManager.getProperty("insurance.home.exclusion.suburb.message"));
    }
    
    if (this.getOptionService().isUnknownSuburb(this)) {
        return this.getHomeService().abortQuote(this,
                "unacceptable suburb",
                this.getResidentialSuburb(),
                Config.configManager.getProperty("insurance.home.exclusion.suburb.unknown.message"));
    }

    // check that we did get a valid address
    if (this.getResidentialGnaf() == null || this.getResidentialGnaf().isEmpty()) {
    	this.setActionMessage("Unable to verify your address");
    	return ERROR;
    }

    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    return SUCCESS;
  }

  @Override
  public void validate() {  	
  	
  	boolean valid = true;
  	
  	if (this.getMoniker() == null || this.getMoniker().isEmpty()) {
  		
  		if (this.getResidentialStreetNumber() == null || this.getResidentialStreetNumber().isEmpty()) {
  			valid = false;
  		}
  		if (this.getResidentialStreet() == null || this.getResidentialStreet().isEmpty()) {
  			valid = false;
  		}
  		if (this.getResidentialSuburb() == null || this.getResidentialSuburb().isEmpty()) {
  			valid = false;
  		}
  		if (this.getResidentialPostcode() == null || this.getResidentialPostcode().isEmpty()) {
  			valid = false;
  		}
// not all addresses coming from QaS are returning LAT / LONG ... they should be optional in this validation
// Also, jspx form address validation looks for GNaf only, not lat and long, when checking if an address is valid
//  		if (this.getResidentialLatitude() == null || this.getResidentialLatitude().isEmpty()) {
//  			valid = false;
//  		}
//  		if (this.getResidentialLongitude() == null || this.getResidentialLongitude().isEmpty()) {
//  			valid = false;
//  		}
  		if (this.getResidentialGnaf() == null || this.getResidentialGnaf().isEmpty()) {
  			valid = false;
  		}
  		  		
  	}
  	
  	if (!valid) {
  		this.addFieldError("residentialAddressQuery", "Unable to verify your address");
  	}
  }

  /**
   * @return the postcode
   */
  public String getResidentialPostcode() {
    return residentialPostcode;
  }

  /**
   * @param residentialPostcode the postcode to set
   */
  public void setResidentialPostcode(String residentialPostcode) {
    this.residentialPostcode = residentialPostcode;
  }

  /**
   * @return the suburb
   */
  public String getResidentialSuburb() {
    return residentialSuburb;
  }

  /**
   * @param residentialSuburb the suburb to set
   */
  public void setResidentialSuburb(String residentialSuburb) {
    this.residentialSuburb = residentialSuburb;
  }

  /**
   * @return the streetSuburbId
   */
  public String getResidentialStreet() {
    return residentialStreet;
  }

  /**
   * @param streetSuburbId the streetSuburbId to set
   */
  public void setResidentialStreet(String residentialStreet) {
    this.residentialStreet = residentialStreet;
  }

  /**
   * @return the streetNumber
   */
  public String getResidentialStreetNumber() {
    return residentialStreetNumber;
  }

  /**
   * @param streetNumber the streetNumber to set
   */
  public void setResidentialStreetNumber(String streetNumber) {
    this.residentialStreetNumber = streetNumber;
  }

	/**
	 * @return the moniker
	 */
	public String getMoniker() {
		return moniker;
	}

	/**
	 * @param moniker the moniker to set
	 */
	public void setMoniker(String moniker) {
		this.moniker = moniker;
	}

	/**
	 * @return the residentialLatitude
	 */
	public String getResidentialLatitude() {
		return residentialLatitude;
	}

	/**
	 * @param residentialLatitude the residentialLatitude to set
	 */
	public void setResidentialLatitude(String residentialLatitude) {
		this.residentialLatitude = residentialLatitude;
	}

	/**
	 * @return the residentialLongitude
	 */
	public String getResidentialLongitude() {
		return residentialLongitude;
	}

	/**
	 * @param residentialLongitude the residentialLongitude to set
	 */
	public void setResidentialLongitude(String residentialLongitude) {
		this.residentialLongitude = residentialLongitude;
	}

	/**
	 * @return the residentialGnaf
	 */
	public String getResidentialGnaf() {
		return residentialGnaf;
	}

	/**
	 * @param residentialGnaf the residentialGnaf to set
	 */
	public void setResidentialGnaf(String residentialGnaf) {
		this.residentialGnaf = residentialGnaf;
	}

	public String getResidentialAddressQuery() {
		return residentialAddressQuery;
	}

	public void setResidentialAddressQuery(String addressQuery) {
		this.residentialAddressQuery = addressQuery;		
	}
}
