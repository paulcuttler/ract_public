package futuremedium.client.ract.secure.actions.insurance.home;

import futuremedium.client.ract.secure.entities.insurance.home.Page;

/**
 * Re-display this Save Quote form with flag set to alter.
 * @author gnewton
 */
public class SaveQuoteComplete extends SaveQuoteForm {

  @Override
  public String getPageTitle() {
    return "Email sent!";
  }

  public boolean isComplete() {
    return true;
  }

  @Override
  public Page getPage() {
    return this.getHomeService().getPage("HomeSaveQuoteForm");
  }
}
