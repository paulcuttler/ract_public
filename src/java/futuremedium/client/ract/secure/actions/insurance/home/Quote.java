package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * Transition from Quote to Fulfillment (Policy).
 * @author gnewton
 *
 * Implement QuoteFinalisedAware to prevent shadow shopping
 */
public class Quote extends ActionBase implements QuoteFinalisedAware {

  @Override
  public String execute() throws Exception {

    // finalise the quote part of the process
    boolean ok = this.getHomeService().finaliseQuote(this);

    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    return SUCCESS;
  }
}
