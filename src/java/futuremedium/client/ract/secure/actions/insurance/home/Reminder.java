package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.Date;
import com.ract.util.DateTime;

import com.opensymphony.xwork2.validator.annotations.*;

/**
 *
 * @author gnewton
 */
public class Reminder extends ActionBase implements PersistanceAware {

  private static final String FIELD_REMINDER_EMAIL = "reminderEmail";
  private static final String FIELD_REMINDER_DATE = "reminderDate";

  private String email;
  private String reminderDay;
  private String reminderMonth;
  private String reminderYear;

  @Override
  public String execute() throws Exception {

    // save, don't update last action
    boolean ok = this.getHomeService().storeQuoteDetail(this, FIELD_REMINDER_EMAIL, this.getEmail(), null, false);
    ok = ok && this.getHomeService().storeQuoteDetail(this, FIELD_REMINDER_DATE, this.getReminder(), null, false);

    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    return SUCCESS;
  }

  @Override
  public void validate() {
    if (getReminder() != null) {
      try {
        DateTime date = new DateTime(this.getReminder());
        Date now = new Date();

        if (date.getTime() < now.getTime()) {
          throw new Exception("Reminder date must be in the future");
        }
      } catch (Exception e) {
        this.addFieldError("reminderDay", "Please enter a valid reminder date");
      }
    } else {
      this.addFieldError("reminderDay", "Please enter a reminder date");
    }
  }

  @Override
  public String getPageTitle() {
    return "Your reminder information";
  }

  @Override
  public String getNextAction() {
    return "HomeReminderComplete";
  }
  
  private String getReminder() {
    String reminder = null;
    if (this.getReminderDay() != null && this.getReminderMonth() != null && this.getReminderYear() != null) {
      reminder = this.getReminderDay() + "/" + this.getReminderMonth() + "/" + this.getReminderYear();
    }
    if (reminder != null && reminder.equals("//")) {
      reminder = null;
    }

    return reminder;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  @RequiredStringValidator(message="Please supply an email address", shortCircuit=true)
  @EmailValidator(message="Please supply a valid email address")
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the reminderDay
   */
  public String getReminderDay() {
    return reminderDay;
  }

  /**
   * @param reminderDay the reminderDay to set
   */
  public void setReminderDay(String reminderDay) {
    this.reminderDay = reminderDay;
  }

  /**
   * @return the reminderMonth
   */
  public String getReminderMonth() {
    return reminderMonth;
  }

  /**
   * @param reminderMonth the reminderMonth to set
   */
  public void setReminderMonth(String reminderMonth) {
    this.reminderMonth = reminderMonth;
  }

  /**
   * @return the reminderYear
   */
  public String getReminderYear() {
    return reminderYear;
  }

  /**
   * @param reminderYear the reminderYear to set
   */
  public void setReminderYear(String reminderYear) {
    this.reminderYear = reminderYear;
  }

}
