package futuremedium.client.ract.secure.actions.insurance;

import java.awt.Color;

import com.octo.captcha.CaptchaFactory;
import com.octo.captcha.component.image.backgroundgenerator.*;
import com.octo.captcha.component.image.fontgenerator.*;
import com.octo.captcha.component.image.textpaster.*;
import com.octo.captcha.component.image.wordtoimage.*;
import com.octo.captcha.component.word.wordgenerator.*;
import com.octo.captcha.engine.*;
import com.octo.captcha.image.gimpy.GimpyFactory;
import com.octo.captcha.service.AbstractCaptchaService;
import com.octo.captcha.service.multitype.GenericManageableCaptchaService;

/**
 * Create Captcha.
 * @author xmfu
 */
public class CaptchaServiceSingleton {

  private static AbstractCaptchaService instance;
  private static BackgroundGenerator backgroundGenerator;
  private static TextPaster nonLinearTextPaster;
  private static WordGenerator randomWordGenerator;
  private static WordToImage composedWordToImage;
  private static CaptchaFactory[] captchaFactories;
  private static CaptchaEngine genericCaptchaEngine;

  public static void setCaptchaConfiguration(String text, int minFontSize, int maxFontSize, int imageWidth, int imageHeight, int textLength, Color textColor) {
    // Min font size and max font size.
    FontGenerator randomFontGenerator = new RandomFontGenerator(minFontSize, maxFontSize);
    // Image width, height, background Color and  font color.
    backgroundGenerator = new UniColorBackgroundGenerator(imageWidth, imageHeight); //, new Color(229, 229, 229));
    nonLinearTextPaster = new NonLinearTextPaster(textLength, textLength, textColor);

    randomWordGenerator = new RandomWordGenerator(text);
    composedWordToImage = new ComposedWordToImage(randomFontGenerator, backgroundGenerator, nonLinearTextPaster);
    captchaFactories = new CaptchaFactory[]{new GimpyFactory(randomWordGenerator, composedWordToImage)};
    genericCaptchaEngine = new GenericCaptchaEngine(captchaFactories);
  }

  public static AbstractCaptchaService getInstance() {
    if (instance == null) {
      if (genericCaptchaEngine == null) {
        return null;
      } else {
        instance = new GenericManageableCaptchaService(genericCaptchaEngine, 180, 100000, 75000);
        return instance;
      }
    } else {
      return instance;
    }
  }
}