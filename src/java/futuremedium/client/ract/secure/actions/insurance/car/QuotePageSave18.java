package futuremedium.client.ract.secure.actions.insurance.car;

/**
 *
 * @author xmfu
 */
public class QuotePageSave18 extends QuotePageSave {

  @Override
  public String execute() throws Exception {
    if (this.getSubmit().equals(BUTTON_BACK)) {
      //this.setPageId(this.getPageId() - 1);
      //this.back();
      this.setNextAction();
      return SUCCESS;
    }

    this.setNextAction();
    // Return to the first page.
    return SUCCESS;
  }
}