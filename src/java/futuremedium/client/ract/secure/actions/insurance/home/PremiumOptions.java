package futuremedium.client.ract.secure.actions.insurance.home;

import java.math.BigDecimal;

import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.Premium;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;

/**
 *
 * @author gnewton
 */
public class PremiumOptions extends PremiumOptionsBase implements QuoteFinalisedAware {
    private static final long serialVersionUID = -5702769223149974926L;
  private String buildingExcess;
  private String contentsExcess;
  private String investorExcess;
  private boolean buildingAccidentalDamage;
  private boolean buildingStormDamage;
  private boolean investorStormDamage;
  private boolean buildingMotorDamage;
  private boolean contentsMotorDamage;
  private boolean investorMotorDamage;
  private boolean investorContentsMotorDamage;
  private boolean unspecifiedPersonalEffects;
  private boolean contentsAccidentalDamage;
  private boolean workersCompensation;
  private boolean investorCover30;
  private boolean investorCover50;

  @Override
  public String execute() throws Exception {
    // save and update lastAction
    boolean ok = savePremiumOptions(true);

    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    BigDecimal tmpAnnualPremium = this.getHomeService().retrieveTotalAnnualPremium(this);
    BigDecimal tmpMonthlyPremium = this.getHomeService().retrieveTotalMonthlyPremium(this);
        
    if (tmpAnnualPremium.compareTo(BigDecimal.ZERO) != 1 || tmpMonthlyPremium.compareTo(BigDecimal.ZERO) != 1) { // not > 0
    	return this.getHomeService().abortQuote(this,
          "zero premium",
          tmpAnnualPremium.toString(),
          Config.configManager.getProperty("insurance.home.retrieve.aborted.message"));    	
    }
    
    return SUCCESS;
  }

  /**
   * Save the appropriate premium options via API.
   * @param updateLastAction whether or not to update lastAction field in DB.
   * @return true if all fields saved ok, false otherwise.
   */
    protected boolean savePremiumOptions(boolean updateLastAction) throws Exception {
        boolean ok = true;

        // If investor contents cover for $50K selected, $30K must be deselected.
        if (this.isInvestorCover50()) {
            this.setInvestorCover30(false);
        }
        // If investor contents cover for $30K selected, $50K must be deselected.
        if (this.isInvestorCover30()) {
            this.setInvestorCover50(false);
        }

        if (this.isBuildingOptionsRequired()) {
            // retrieve data objects from submitted value
            InRfDet selectedBuildingExcess = this.getLookup(this.getBuildingExcessOptions()).get(this.getBuildingExcess());
            InRfDet selectedBuildingAccidentalDamage = this.getLookup(this.getBuildingInsuranceOptions()).get(PremiumOptionsBase.VALUE_BLDG_ACCIDENTAL_DAMAGE);
            InRfDet selectedBuildingStormDamage = this.getLookup(this.getBuildingInsuranceOptions()).get(PremiumOptionsBase.VALUE_BLDG_STORM_DAMAGE);
            InRfDet selectedBuildingMotorDamage = this.getLookup(this.getBuildingInsuranceOptions()).get(PremiumOptionsBase.VALUE_BLDG_ELECTRIC_MOTORS);

            ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.BLDG_EXCESS, this.getBuildingExcess(), selectedBuildingExcess, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_BLDG_ACCIDENTAL_DAMAGE, Boolean.toString(this.isBuildingAccidentalDamage()), selectedBuildingAccidentalDamage, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_BLDG_STORM_DAMAGE, Boolean.toString(this.isBuildingStormDamage()), selectedBuildingStormDamage, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_BLDG_ELECTRIC_MOTORS, Boolean.toString(this.isBuildingMotorDamage()), selectedBuildingMotorDamage, updateLastAction);
        }

        if (this.isContentsOptionsRequired()) {
            // retrieve data objects from submitted value
            InRfDet selectedContentsExcess = this.getLookup(this.getContentsExcessOptions()).get(this.getContentsExcess());
            InRfDet selectedContentsAccidentalDamage = this.getLookup(this.getContentsInsuranceOptions()).get(PremiumOptionsBase.VALUE_CNTS_ACCIDENTAL_DAMAGE);
            InRfDet selectedPersonalEffects = this.getLookup(this.getContentsInsuranceOptions()).get(PremiumOptionsBase.VALUE_CNTS_PERSONAL_EFFECTS);
            InRfDet selectedContentsMotorDamage = this.getLookup(this.getContentsInsuranceOptions()).get(PremiumOptionsBase.VALUE_CNTS_ELECTRIC_MOTORS);
            InRfDet selectedDwc = this.getLookup(this.getContentsInsuranceOptions()).get(PremiumOptionsBase.VALUE_CNTS_DWC);

            ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.CNTS_EXCESS, this.getContentsExcess(), selectedContentsExcess, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_CNTS_PERSONAL_EFFECTS, Boolean.toString(this.isUnspecifiedPersonalEffects()), selectedPersonalEffects, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_CNTS_ACCIDENTAL_DAMAGE, Boolean.toString(this.isContentsAccidentalDamage()), selectedContentsAccidentalDamage, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_CNTS_ELECTRIC_MOTORS, Boolean.toString(this.isContentsMotorDamage()), selectedContentsMotorDamage, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_CNTS_DWC, Boolean.toString(this.isWorkersCompensation()), selectedDwc, updateLastAction);
        }

        if (this.isInvestorOptionsRequired()) {
            // retrieve data objects from submitted value
            InRfDet selectedInvestorExcess = this.getLookup(this.getInvestorExcessOptions()).get(this.getInvestorExcess());
            InRfDet selectedInvestorStormDamage = this.getLookup(this.getInvestorInsuranceOptions()).get(PremiumOptionsBase.VALUE_INV_STORM_DAMAGE);
            InRfDet selectedInvestorMotorDamage = this.getLookup(this.getInvestorInsuranceOptions()).get(PremiumOptionsBase.VALUE_INV_ELECTRIC_MOTORS);
            InRfDet selectedInvestorContentsMotorDamage = this.getLookup(this.getInvestorContentsInsuranceOptions()).get(PremiumOptionsBase.VALUE_INV_CONTENTS_ELECTRIC_MOTORS);
            InRfDet selectedInvestorCover30 = this.getLookup(this.getInvestorInsuranceOptions()).get(PremiumOptionsBase.VALUE_INV_CONTENTS_COVER_30);
            InRfDet selectedInvestorCover50 = this.getLookup(this.getInvestorInsuranceOptions()).get(PremiumOptionsBase.VALUE_INV_CONTENTS_COVER_50);

            ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.INV_EXCESS, this.getInvestorExcess(), selectedInvestorExcess, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_INV_STORM_DAMAGE, Boolean.toString(this.isInvestorStormDamage()), selectedInvestorStormDamage, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_INV_ELECTRIC_MOTORS, Boolean.toString(this.isInvestorMotorDamage()), selectedInvestorMotorDamage, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_INV_CONTENTS_ELECTRIC_MOTORS, Boolean.toString(this.isInvestorContentsMotorDamage()), selectedInvestorContentsMotorDamage, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_INV_CONTENTS_COVER_30, Boolean.toString(this.isInvestorCover30()), selectedInvestorCover30, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, PremiumOptionsBase.VALUE_INV_CONTENTS_COVER_50, Boolean.toString(this.isInvestorCover50()), selectedInvestorCover50, updateLastAction);
        }

        Premium annualPremium = this.getHomeService().retrievePremium(this, WebInsQuoteDetail.PAY_ANNUALLY, WebInsQuote.COVER_BLDG);
        Premium monthlyPremium = this.getHomeService().retrievePremium(this, WebInsQuoteDetail.PAY_MONTHLY, WebInsQuote.COVER_BLDG);

        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.ANNUAL_PAYMENT_BASE_ANNUAL_PERMIUM, annualPremium.getBaseAnnualPremium().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.ANNUAL_PAYMENT_GST, annualPremium.getGst().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.ANNUAL_PAYMENT_STAMP_DUTY, annualPremium.getStampDuty().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.ANNUAL_PAYMENT_INSTALMENT_AMOUNT, annualPremium.getPayment().toString(), null, false);

        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.MONTHLY_PAYMENT_BASE_ANNUAL_PERMIUM, monthlyPremium.getBaseAnnualPremium().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.MONTHLY_PAYMENT_GST, monthlyPremium.getGst().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.MONTHLY_PAYMENT_STAMP_DUTY, monthlyPremium.getStampDuty().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.MONTHLY_PAYMENT_TOTAL_ANNUAL_PREMIUM, monthlyPremium.getAnnualPremium().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT, monthlyPremium.getPayment().toString(), null, false);

        // save legacy Premium data points to DB
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.PREMIUM, annualPremium.getAnnualPremium().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.PAYMENT, monthlyPremium.getPayment().toString(), null, false);
        if (this.isBuildingOptionsRequired()) {
            ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_BUILDING_MONTHLY_PREMIUM, monthlyPremium.getPayment().toString(), null, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_BUILDING_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString(), null, updateLastAction);
        }
        if (this.isContentsOptionsRequired()) {
            ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_CONTENTS_MONTHLY_PREMIUM, monthlyPremium.getPayment().toString(), null, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_CONTENTS_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString(), null, updateLastAction);
        }
        if (this.isInvestorOptionsRequired()) {
            ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_INVESTOR_MONTHLY_PREMIUM, monthlyPremium.getPayment().toString(), null, updateLastAction);
            ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_INVESTOR_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString(), null, updateLastAction);
        }
        return ok;
    }

  /**
   * @return the buildingAccidentalDamage
   */
  public boolean isBuildingAccidentalDamage() {
    return buildingAccidentalDamage;
  }

  /**
   * @param buildingAccidentalDamage the buildingAccidentalDamage to set
   */
  public void setBuildingAccidentalDamage(boolean buildingAccidentalDamage) {
    this.buildingAccidentalDamage = buildingAccidentalDamage;
  }

  /**
   * @return the buildingStormDamage
   */
  public boolean isBuildingStormDamage() {
    return buildingStormDamage;
  }

  /**
   * @param stormDamage the stormDamage to set
   */
  public void setBuildingStormDamage(boolean buildingStormDamage) {
    this.buildingStormDamage = buildingStormDamage;
  }

  /**
   * @return the buildingMotorDamage
   */
  public boolean isBuildingMotorDamage() {
    return buildingMotorDamage;
  }

  /**
   * @param buildingMotorDamage the buildingMotorDamage to set
   */
  public void setBuildingMotorDamage(boolean buildingMotorDamage) {
    this.buildingMotorDamage = buildingMotorDamage;
  }

  /**
   * @return the unspecifiedPersonalEffects
   */
  public boolean isUnspecifiedPersonalEffects() {
    return unspecifiedPersonalEffects;
  }

  /**
   * @param unspecifiedPersonalEffects the unspecifiedPersonalEffects to set
   */
  public void setUnspecifiedPersonalEffects(boolean unspecifiedPersonalEffects) {
    this.unspecifiedPersonalEffects = unspecifiedPersonalEffects;
  }

  /**
   * @return the contentsAccidentalDamage
   */
  public boolean isContentsAccidentalDamage() {
    return contentsAccidentalDamage;
  }

  /**
   * @param contentsAccidentalDamage the contentsAccidentalDamage to set
   */
  public void setContentsAccidentalDamage(boolean contentsAccidentalDamage) {
    this.contentsAccidentalDamage = contentsAccidentalDamage;
  }

  /**
   * @return the workersCompensation
   */
  public boolean isWorkersCompensation() {
    return workersCompensation;
  }

  /**
   * @param workersCompensation the workersCompensation to set
   */
  public void setWorkersCompensation(boolean workersCompensation) {
    this.workersCompensation = workersCompensation;
  }

  /**
   * @return the contentsMotorDamage
   */
  public boolean isContentsMotorDamage() {
    return contentsMotorDamage;
  }

  /**
   * @param contentsMotorDamage the contentsMotorDamage to set
   */
  public void setContentsMotorDamage(boolean contentsMotorDamage) {
    this.contentsMotorDamage = contentsMotorDamage;
  }

  /**
   * @return the buildingExcess
   */
  public String getBuildingExcess() {
    return buildingExcess;
  }

  /**
   * @param buildingExcess the buildingExcess to set
   */
  public void setBuildingExcess(String buildingExcess) {
    this.buildingExcess = buildingExcess;
  }

  /**
   * @return the contentsExcess
   */
  public String getContentsExcess() {
    return contentsExcess;
  }

  /**
   * @param contentsExcess the contentsExcess to set
   */
  public void setContentsExcess(String contentsExcess) {
    this.contentsExcess = contentsExcess;
  }

  /**
   * @return the investorExcess
   */
  public String getInvestorExcess() {
    return investorExcess;
  }

  /**
   * @param investorExcess the investorExcess to set
   */
  public void setInvestorExcess(String investorExcess) {
    this.investorExcess = investorExcess;
  }

  /**
   * @return the investorStormDamage
   */
  public Boolean isInvestorStormDamage() {
    return investorStormDamage;
  }

  /**
   * @param investorStormDamage the investorStormDamage to set
   */
  public void setInvestorStormDamage(Boolean investorStormDamage) {
    this.investorStormDamage = investorStormDamage;
  }

  /**
   * @return the investorMotorDamage
   */
  public boolean isInvestorMotorDamage() {
    return investorMotorDamage;
  }

  /**
   * @param investorMotorDamage the investorMotorDamage to set
   */
  public void setInvestorMotorDamage(boolean investorMotorDamage) {
    this.investorMotorDamage = investorMotorDamage;
  }
  
  /**
   * @return the investorContentsMotorDamage
   */
  public boolean isInvestorContentsMotorDamage() {
      return investorContentsMotorDamage;
  }
  
  /**
   * @param investorContentsMotorDamage the investorContentsMotorDamage to set
   */
  public void setInvestorContentsMotorDamage(boolean investorContentsMotorDamage) {
      this.investorContentsMotorDamage = investorContentsMotorDamage;
  }

  /**
   * Return investor contents cover for $30K.
   * @return the investorCover30
   */
  public boolean isInvestorCover30() {
    return investorCover30;
  }

  /**
   * @param investorCover30 the investorCover30 to set
   */
  public void setInvestorCover30(boolean investorCover30) {
    this.investorCover30 = investorCover30;
  }

  /**
   * Return investor contents cover for $50K.
   * @return the investorCover50
   */
  public boolean isInvestorCover50() {
    return investorCover50;
  }

  /**
   * @param investorCover50 the investorCover50 to set
   */
  public void setInvestorCover50(boolean investorCover50) {
    this.investorCover50 = investorCover50;
  }
}
