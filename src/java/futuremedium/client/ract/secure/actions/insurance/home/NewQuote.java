package futuremedium.client.ract.secure.actions.insurance.home;

import com.opensymphony.xwork2.validator.annotations.*;

import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.actions.insurance.InsuranceBase;

/**
 * Handle Quote Type form submission.
 * @author gnewton
 */
public class NewQuote extends ActionBase {
    private static final long serialVersionUID = -6642163131132013174L;
  private boolean agree;

  @Override
  public String execute() throws Exception {
    
    boolean ok = this.getHomeService().startQuote(this);

    if (ok) {
    	// record user's session
      String sessionId = recordSession(null, RactActionSupport.CONTEXT_HOME_QUOTE);
      this.getHomeService().storeSessionId(this, sessionId);
      
      this.getHomeService().storeQuoteDetail(this, InsuranceBase.FIELD_USER_AGENT, this.getHttpRequest().getHeader("User-Agent"));
      String originatingIp = this.extractOriginatingIp();
      this.getHomeService().storeQuoteDetail(this, InsuranceBase.FIELD_USER_IP, originatingIp);
      this.getHomeService().storeQuoteDetail(this, InsuranceBase.FIELD_USER_COUNTRY, this.extractOriginatingCountry(originatingIp));
      this.getHomeService().storeQuoteDetail(this, InsuranceBase.FIELD_USER_TRAFFIC_SRC, this.getTrafficSource());
      this.getHomeService().storeQuoteDetail(this, InsuranceBase.FIELD_USER_SITE_SRC, this.getSiteSource()); // set via cookie
      this.getHomeService().storeQuoteDetail(this, InsuranceBase.FIELD_USER_SCREEN_WIDTH, this.getScreenWidth()); // set via cookie
      this.getHomeService().storeQuoteDetail(this, InsuranceBase.FIELD_USER_COOKIES, Boolean.toString((this.getScreenWidth() != null)));
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    return SUCCESS;
  }



  /**
   * @return the quoteType
   */
  @Override
  public String getQuoteType() {
    return super.getQuoteType();
  }

  /**
   * @param quoteType the quoteType to set
   */
  @RequiredStringValidator(message = "Please select a quote type")
  @Override
  public void setQuoteType(String quoteType) {
    super.setQuoteType(quoteType);
  }

  /**
   * @return the agree
   */
  public boolean isAgree() {
    return agree;
  }

  /**
   * @param agree the agree to set
   */
  @FieldExpressionValidator(message="You must agree to the terms and conditions", expression="agree == true")
  public void setAgree(boolean agree) {
    this.agree = agree;
  }

}
