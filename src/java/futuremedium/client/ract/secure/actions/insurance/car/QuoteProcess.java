package futuremedium.client.ract.secure.actions.insurance.car;

import futuremedium.client.ract.secure.actions.*;

import futuremedium.client.ract.secure.entities.insurance.car.*;

/**
 * I don't think this is actually used??
 * @author xmfu
 */
public class QuoteProcess extends RactActionSupport {
  private QuotePage quotePage;
  private long pageId = 1;
  private QuoteQuestionManager quoteQuestionManager = new QuoteQuestionManager();
  private int pageNumbers = 0;

  @Override
  public String execute() throws Exception {
    next();
    return SUCCESS;
  }

  public String next() throws Exception {
    // Get total page numbers.
    this.pageNumbers = this.quoteQuestionManager.getPageNumbers();

    // Get questions from Xml.
    if ((this.getPageId() > this.pageNumbers )) {
       //@todo user has completed the vehicle quote process, an end page should display.
    } else {
      this.setQuotePage(this.quoteQuestionManager.getPage(this.getPageId()));
    }
    
    return SUCCESS;
  }

  public String back() throws Exception {
    // Get questions from Xml.
    this.setPageId(this.getPageId() - 1);

    if (this.getPageId() <= 1) {
      this.setPageId(1);
    }

    this.setQuotePage(this.quoteQuestionManager.getPage(this.getPageId()));

    return SUCCESS;
  }

  /**
   * @return the quotePage
   */
  public QuotePage getQuotePage() {
    return quotePage;
  }

  /**
   * @param quotePage the quotePage to set
   */
  public void setQuotePage(QuotePage quotePage) {
    this.quotePage = quotePage;
  }

  /**
   * @return the pageId
   */
  public long getPageId() {
    return pageId;
  }

  /**
   * @param pageId the pageId to set
   */
  public void setPageId(long pageId) {
    this.pageId = pageId;
  }
}