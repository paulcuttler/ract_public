package futuremedium.client.ract.secure.actions.insurance.car;

import java.util.*;

import com.ract.util.DateTime;
import com.ract.web.insurance.*;
import futuremedium.client.ract.secure.Config;

/**
 * Load page 11. Contains cover start date, vehicle registration number, no existing damage, extra modifications, extra accessories and financier.
 * @author xmfu
 */
public class QuotePageSave11 extends QuotePageSave {

  private String coverStart;
  private String registration;
  private String noExistingDamage;
  private String extras;
  private List<String> extraModifications;

  @Override
  public String execute() throws Exception {
    this.setUp();

    // Go back to previous page.
    if (this.getSubmit().equals(BUTTON_BACK)) {
      this.setNextAction();
      return SUCCESS;
    }

    // Validation.
    if (!this.validate(this.getCoverStart(), VALIDATE_DATETIME_STRING)) {
      this.loadPage11(this.getCoverStart(), this.getRegistration(), this.getNoExistingDamage(), this.getExtraModifications(), this.getExtras());
      return this.validationFailed("Cover start date", VALIDATE_DATETIME_STRING, false);
    } else if (!this.validate(this.getRegistration(), VALIDATE_STRING)) {
      this.loadPage11(this.getCoverStart(), this.getRegistration(), this.getNoExistingDamage(), this.getExtraModifications(), this.getExtras());
      return this.validationFailed("Registration", VALIDATE_STRING, false);
    } else if (!this.validate(this.getNoExistingDamage(), VALIDATE_STRING)) {
      this.loadPage11(this.getCoverStart(), this.getRegistration(), this.getNoExistingDamage(), this.getExtraModifications(), this.getExtras());
      return this.validationFailed("Existing Damage.", VALIDATE_RADIO, false);
    } else if (!this.validate(this.getExtras(), VALIDATE_STRING)) {
      this.loadPage11(this.getCoverStart(), this.getRegistration(), this.getNoExistingDamage(), this.getExtraModifications(), this.getExtras());
      return this.validationFailed("Extra modifications.", VALIDATE_RADIO, false);
    }

    // Save form data.
    int quoteNo = this.getQuoteNo();
    this.setUp();

    Date sd = dateFormat.parse(this.getCoverStart());
    DateTime sdTime = new DateTime(sd);

    this.insuranceMgr.setStartCoverDate(quoteNo, sdTime);
    this.insuranceMgr.getQuote(quoteNo).setStartCover(sdTime);

    this.insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.COVER_START, this.getCoverStart());
    
    // if rego was already supplied for NVIC lookup, don't save it again (input field won't be shown)
    if (!this.getInsuranceMgr().getQuoteDetail(this.getQuoteNo(), FIELD_LOOKUP_OPTION).equals(LOOKUP_OPTION_REGO)) {
    	this.insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.REG_NO, this.getRegistration());
    }

    // Extras and existing damage.
    this.insuranceMgr.setQuoteDetail(quoteNo, "nonExistingDamage", noExistingDamage);
    
    this.insuranceMgr.setQuoteDetail(quoteNo, "extras", extras);
    if (!extras.equals("no")) {
      for (String extraModification : this.getExtraModifications()) {
        this.insuranceMgr.setQuoteDetail(quoteNo, extraModification, WebInsQuoteDetail.TRUE);
      }
    }

    // Check whether client is a RACT member and load client groups.
    if (this.isMember()) {
      this.setMember(true);
    } else { // Not a RACT member, need to create new policy holders.
      this.setMember(false);
      this.setPageId(this.getPageId() + 1);
      this.setNewPolicyInSession("true");
    }

    if (!noExistingDamage.equals("yes")) {
      return this.messagePage(MESSAGE_EXTRADAMAGE);
    }
    if (!extras.equals("no")) {
      return this.messagePage(MESSAGE_NONSTANDARDACCRESSORIES);
    }
    
    this.setNextAction();
    return SUCCESS;
  }

  @Override
  public void validate() {

    if (this.getSubmit().equals(BUTTON_BACK)) {
      return;
    }

    try {


       // Validation.
      if (!this.validate(this.getCoverStart(), VALIDATE_DATETIME_STRING)) {
        this.addFieldError("coverStart", "Please select an option");
      } 
      if (!this.validate(this.getRegistration(), VALIDATE_STRING)) {
        this.addFieldError("registration", "Please a  enter valid registration number");
      } 
      if (!this.validate(this.getNoExistingDamage(), VALIDATE_STRING)) {
        this.addFieldError("noExistingDamage", "Please select an option");
      } 
      if (!this.validate(this.getExtras(), VALIDATE_STRING)) {
        this.addFieldError("extras", "Please select an option");
      }
      
      
      if (this.extras != null && !this.extras.equals("no")) {
        if(this.getExtraModifications() == null || this.getExtraModifications().isEmpty()) {
          this.addFieldError("extras", "Please select at least 1 extras modification");
        }
      }
      

      if (!this.getFieldErrors().isEmpty()) {
        this.loadPage11(this.getCoverStart(), this.getRegistration(), this.getNoExistingDamage(), this.getExtraModifications(), this.getExtras());
      }


    } catch (Exception e) {
      Config.logger.debug(Config.key + ": loadpage11 failed on validation: ", e);
    }

  }

  /**
   * @return the coverStart
   */
  public String getCoverStart() {
    return coverStart;
  }

  /**
   * @param coverStart the coverStart to set
   */
  public void setCoverStart(String coverStart) {
    this.coverStart = coverStart;
  }

  /**
   * @return the registration
   */
  public String getRegistration() {
    return registration;
  }

  /**
   * @param registration the registration to set
   */
  public void setRegistration(String registration) {
    this.registration = registration;
  }

  /**
   * @return the noExistingDamage
   */
  public String getNoExistingDamage() {
    return noExistingDamage;
  }

  /**
   * @param noExistingDamage the noExistingDamage to set
   */
  public void setNoExistingDamage(String noExistingDamage) {
    this.noExistingDamage = noExistingDamage;
  }

  /**
   * @return the extras
   */
  public String getExtras() {
    return extras;
  }

  /**
   * @param extras the extras to set
   */
  public void setExtras(String extras) {
    this.extras = extras;
  }

  /**
   * @return the extraModifications
   */
  public List<String> getExtraModifications() {
    return extraModifications;
  }

  /**
   * @param extraModifications the extraModifications to set
   */
  public void setExtraModifications(List<String> extraModifications) {
    this.extraModifications = extraModifications;
  }
}
