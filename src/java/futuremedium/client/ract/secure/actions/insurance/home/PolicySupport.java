package futuremedium.client.ract.secure.actions.insurance.home;

import java.math.BigDecimal;

/**
 * Common functionality required by Policy stage of Quote.
 * @author gnewton
 */
public class PolicySupport extends QuoteSubTypeSupport {

  private BigDecimal totalAnnualPremium;
  private BigDecimal totalMonthlyPremium;
  private String paymentMethod;

  /**
   * @return the totalAnnualPremium
   */
  public BigDecimal getTotalAnnualPremium() {
    if (this.totalAnnualPremium == null) {
      this.totalAnnualPremium = this.getHomeService().retrieveTotalAnnualPremium(this);
    }
    return totalAnnualPremium;
  }

  /**
   * @param totalAnnualPremium the totalAnnualPremium to set
   */
  public void setTotalAnnualPremium(BigDecimal totalAnnualPremium) {
    this.totalAnnualPremium = totalAnnualPremium;
  }

  /**
   * @return the totalMonthlyPremium
   */
  public BigDecimal getTotalMonthlyPremium() {
    if (this.totalMonthlyPremium == null) {
      this.totalMonthlyPremium = this.getHomeService().retrieveTotalMonthlyPremium(this);
    }
    return totalMonthlyPremium;
  }

  /**
   * @param totalMonthlyPremium the totalMonthlyPremium to set
   */
  public void setTotalMonthlyPremium(BigDecimal totalMonthlyPremium) {
    this.totalMonthlyPremium = totalMonthlyPremium;
  }

  /**
   * @return the paymentMethod
   */
  public String getPaymentMethod() {
    if (this.paymentMethod == null) {
      this.paymentMethod = this.getHomeService().retrievePaymentMethod(this);
    }
    return paymentMethod;
  }

  /**
   * @param paymentMethod the paymentMethod to set
   */
  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }
}
