package futuremedium.client.ract.secure.actions.insurance.home;

import futuremedium.client.ract.secure.entities.insurance.home.Page;

/**
 *
 * @author gnewton
 */
public class SurveyComplete extends SurveyForm {

  @Override
  public String getPageTitle() {
    return "Survey sent!";
  }

  public boolean isComplete() {
    return true;
  }

  @Override
  public Page getPage() {
    return this.getHomeService().getPage("HomeSurveyForm");
  }
}
