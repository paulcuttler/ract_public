package futuremedium.client.ract.secure.actions.insurance;

public interface QasPostalAware {

	public String getPostalPostcode();
  public String getPostalSuburb();  
  public String getPostalStreet();  
  public String getPostalStreetNumber();  
  public String getPostalDpid();
  public String getPostalAusbar();
  public String getSearch();
	
	public void setPostalPostcode(String postalPostcode);
  public void setPostalSuburb(String postalSuburb);
  public void setPostalStreet(String postalStreet);
  public void setPostalStreetNumber(String postalStreetNumber);
  public void setPostalDpid(String dpid);
  public void setPostalAusbar(String postalAusbar);
  public void setSearch(String search);
}
