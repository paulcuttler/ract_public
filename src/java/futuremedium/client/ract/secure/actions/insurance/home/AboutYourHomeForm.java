package futuremedium.client.ract.secure.actions.insurance.home;

import com.ract.web.insurance.WebInsQuoteDetail;

/**
 *
 * @author gnewton
 */
public class AboutYourHomeForm extends AboutYourHomeBase implements QuoteAware {
  
    private static final long serialVersionUID = -910042560549640710L;
  private String buildingType;
  private Integer buildingTypeConjoined;
  private String yearConstructed;
  private String wallConstruction;
  private String roofConstruction;
  private String occupancy;
  private Boolean hardwiredSmokeAlarm;
  private String securityAlarm;
  private String securityLock;
  private String securityOther;
  private String manager;
  private String managerOther;

  /**
   * @return the buildingType
   */
  public String getBuildingType() {
    if (this.buildingType == null) {
      this.buildingType = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.BUILDING_TYPE);
    }
    return buildingType;
  }

  /**
   * @param buildingType the buildingType to set
   */
  public void setBuildingType(String buildingType) {
    this.buildingType = buildingType;
  }

  /**
   * @return the yearConstructed
   */
  public String getYearConstructed() {
    if (this.yearConstructed == null) {
      this.yearConstructed = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.YEAR_CONST);
    }
    return yearConstructed;
  }

  /**
   * @param yearConstructed the yearConstructed to set
   */
  public void setYearConstructed(String yearConstructed) {
    this.yearConstructed = yearConstructed;
  }

  /**
   * @return the wallConstruction
   */
  public String getWallConstruction() {
    if (this.wallConstruction == null) {
      this.wallConstruction = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.CONSTRUCTION);
    }
    return wallConstruction;
  }

  /**
   * @param wallConstruction the wallConstruction to set
   */
  public void setWallConstruction(String wallConstruction) {
    this.wallConstruction = wallConstruction;
  }

  /**
   * @return the roofConstruction
   */
  public String getRoofConstruction() {
    if (this.roofConstruction == null) {
      this.roofConstruction = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.ROOF);
    }
    return roofConstruction;
  }

  /**
   * @param roofConstruction the roofConstruction to set
   */
  public void setRoofConstruction(String roofConstruction) {
    this.roofConstruction = roofConstruction;
  }

  /**
   * @return the occupancy
   */
  public String getOccupancy() {
    if (this.occupancy == null) {
      this.occupancy = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.OCCUPANCY);
    }
    return occupancy;
  }

  /**
   * @param occupancy the occupancy to set
   */
  public void setOccupancy(String occupancy) {
    this.occupancy = occupancy;
  }

  /**
   * @return the hardwiredSmokeAlarm
   */
  public Boolean isHardwiredSmokeAlarm() {
    if (this.hardwiredSmokeAlarm == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SMOKE_ALARM);
      if (value == null) {
        this.hardwiredSmokeAlarm = null; // neither option selected
      } else {
        this.hardwiredSmokeAlarm = Boolean.parseBoolean(value);
      }
    }
    return hardwiredSmokeAlarm;
  }

  /**
   * @param hardwiredSmokeAlarm the hardwiredSmokeAlarm to set
   */
  public void setHardwiredSmokeAlarm(Boolean hardwiredSmokeAlarm) {
    this.hardwiredSmokeAlarm = hardwiredSmokeAlarm;
  }

  public String getSecurityAlarm() {
      if (this.securityAlarm == null) {
          this.securityAlarm = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SECURITY_ALARM);
      }
      return securityAlarm;
  }
  
  public void setSecurityAlarm(String securityAlarm) {
      this.securityAlarm = securityAlarm;
  }
  
  public String getSecurityLock() {
      if (this.securityLock == null) {
          this.securityLock = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.SECURITY_LOCK);
      }
      return securityLock;
  }
  
  public void setSecurityLock(String securityLock) {
      this.securityLock = securityLock;
  }

  /**
   * @return the securityOther
   */
  public String getSecurityOther() {
    if (this.securityOther == null) {
      this.securityOther = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_SECURITY_OTHER);
    }
    return securityOther;
  }

  /**
   * @param securityOther the securityOther to set
   */
  public void setSecurityOther(String securityOther) {
    this.securityOther = securityOther;
  }


  /**
   * @return the security
   */
  public String getManager() {
    if (this.manager == null) {
      this.manager = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.RENTAL_MANAGER);
    }
    return manager;
  }

  /**
   * @param security the security to set
   */
  public void setManager(String manager) {
    this.manager = manager;
  }

  /**
   * @return the securityOther
   */
  public String getManagerOther() {
    if (this.managerOther == null) {
      this.managerOther = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_MANAGER_OTHER);
    }
    return managerOther;
  }

  /**
   * @param securityOther the securityOther to set
   */
  public void setManagerOther(String managerOther) {
    this.managerOther = managerOther;
  }


  /**
   * @return the buildingTypeConjoined
   */
  public Integer getBuildingTypeConjoined() {
    if (this.buildingTypeConjoined == null) {
      try {
        this.buildingTypeConjoined = Integer.parseInt(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_BUILDING_TYPE_CONJOINED));
      } catch (Exception e) {}
    }
    return buildingTypeConjoined;
  }

  /**
   * @param buildingTypeConjoined the buildingTypeConjoined to set
   */
  public void setBuildingTypeConjoined(Integer buildingTypeConjoined) {
    this.buildingTypeConjoined = buildingTypeConjoined;
  }



}
