package futuremedium.client.ract.secure.actions.insurance.home;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

/**
 * @todo Home: Saving the comments field is probably going to throw a data-truncation error - need to work out where to save it.
 * @author gnewton
 */
public class Survey extends SurveyBase {

  private static final String FIELD_SURVEY_RATING = "surveyRating";
  private static final String FIELD_SURVEY_COMMENTS = "surveyComments";

  private String rate;
  private String comments;

  @Override
  public String execute() throws Exception {

    // save, don't update last action
    boolean ok = this.getHomeService().storeQuoteDetail(this, FIELD_SURVEY_RATING, this.getRate(), null, false);
    ok = ok && this.getHomeService().storeQuoteDetail(this, FIELD_SURVEY_COMMENTS, this.getComments(), null, false);
    
    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    return SUCCESS;
  }

  @Override
  public String getPageTitle() {
    return "Rate the process";
  }

  @Override
  public String getNextAction() {
    return "HomeSurveyComplete";
  }

  /**
   * @return the rate
   */
  public String getRate() {
    return rate;
  }

  /**
   * @param rate the rate to set
   */
  @RequiredStringValidator(message="Please rate the process")
  public void setRate(String rate) {
    this.rate = rate;
  }

  /**
   * @return the comments
   */
  public String getComments() {
    return comments;
  }

  /**
   * @param comments the comments to set
   */
  public void setComments(String comments) {
    this.comments = comments;
  }

}
