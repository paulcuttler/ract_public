package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.*;

import com.ract.web.insurance.InRfDet;
import com.ract.util.DateTime;

import futuremedium.client.ract.secure.Config;

/**
 * Common elements for About Your Home selections within Policy area.
 * @author gnewton
 */
public class AboutYourHomePolicyBase extends PolicySupport {

  static private final String TOKEN_BUSINESS_USE_OPTIONS = "BUSINESS_USE_OPTIONS";
  static private final String TOKEN_FINANCIER_OPTIONS = "FINANCIER_OPTIONS";

  private List<InRfDet> businessUseOptions;
  private List<InRfDet> financierOptions;
  private List<DateTime> startDates;  

  /**
   * @return the startDates
   */
  public List<DateTime> getStartDates() {
    if (this.startDates == null) {
      this.startDates = this.getOptionService().getStartDates(0, Integer.parseInt(Config.configManager.getProperty("insurance.home.maxStartDateDays", "14")));
    }
    return startDates;
  }

  /**
   * @param dates the dates to set
   */
  public void setStartDates(List<DateTime> dates) {
    this.startDates = dates;
  }

  /**
   * Retrieve or populate businessUseOptions from session.
   * @return the businessUseOptions
   */
  @SuppressWarnings("unchecked")
  public List<InRfDet> getBusinessUseOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_BUSINESS_USE_OPTIONS)) { // retrieve from session
        this.businessUseOptions = (List<InRfDet>) this.session.get(TOKEN_BUSINESS_USE_OPTIONS);
      } else { // initialise in session
        this.businessUseOptions = this.getOptionService().getBusinessUseOptions(this);
        this.session.put(TOKEN_BUSINESS_USE_OPTIONS, this.businessUseOptions);
      }
    }
    return businessUseOptions;
  }

  /**
   * Store businessUseOptions in session.
   * @param businessUseOptions the businessUseOptions to set
   */
  public void setBusinessUseOptions(List<InRfDet> businessUseOptions) {
    this.businessUseOptions = businessUseOptions;
    if (this.session != null) {
      this.session.put(TOKEN_BUSINESS_USE_OPTIONS, this.businessUseOptions);
    }
  }

  /**
   * Return a lookup of financier objects by key.
   * @return
   */
  public Map<String, InRfDet> getBusinessUseLookup() throws Exception {
    return getLookup(this.getBusinessUseOptions());
  }

  @SuppressWarnings("unchecked")
  public List<InRfDet> getFinancierOptions() throws Exception {
    if (this.session != null) {
      if (this.session.containsKey(TOKEN_FINANCIER_OPTIONS)) { // retrieve from session
        this.financierOptions = (List<InRfDet>) this.session.get(TOKEN_FINANCIER_OPTIONS);
      } else { // initialise in session
        this.financierOptions = this.getOptionService().getFinancierOptions(this);
        this.session.put(TOKEN_FINANCIER_OPTIONS, this.financierOptions);
      }
    }
    return financierOptions;
  }

  /**
   * Store financierOptions in session.
   * @param financierOptions the financierOptions to set
   */
  public void setFinancierOptions(List<InRfDet> financierOptions) {
    this.financierOptions = financierOptions;
    if (this.session != null) {
      this.session.put(TOKEN_FINANCIER_OPTIONS, this.financierOptions);
    }
  }

  /**
   * Return a lookup of financier objects by key.
   * @return
   */
  public Map<String, InRfDet> getFinancierLookup() throws Exception {
    return getLookup(this.getFinancierOptions());
  }
  
}
