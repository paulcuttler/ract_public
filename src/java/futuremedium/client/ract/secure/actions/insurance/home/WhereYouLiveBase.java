package futuremedium.client.ract.secure.actions.insurance.home;

import java.util.*;

import com.ract.util.DateTime;

/**
 * Common components for Where You Live form and processing.
 * @author gnewton
 */
public class WhereYouLiveBase extends ActionBase {

  private List<DateTime> startDates;

  /**
   * @return the dates
   */
  public List<DateTime> getStartDates() {
    if (this.startDates == null) {
      this.startDates = this.getOptionService().getStartDates(0, 14);
    }
    return startDates;
  }

  /**
   * @param dates the dates to set
   */
  public void setStartDates(List<DateTime> dates) {
    this.startDates = dates;
  }

}
