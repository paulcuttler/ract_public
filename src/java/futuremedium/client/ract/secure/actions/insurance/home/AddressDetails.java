package futuremedium.client.ract.secure.actions.insurance.home;

/**
 *
 * @author gnewton
 */
public class AddressDetails extends AddressDetailsBase implements PolicyAware {

  private String residentialPostcode;
  private String residentialSuburb;
  private String residentialStreet;
  private String residentialStreetNumber;

  private String poBoxPostcode;
  private String poBoxSuburb;
  private String poBoxNumber;

  private String email;

  private Boolean differntPostalAddress = false;

  @Override
  public String execute() throws Exception {

    boolean ok = this.getHomeService().storeQuoteDetail(this, ActionBase.DIFFERENT_POSTAL_ADDRESS, this.differntPostalAddress.toString());
    ok = ok && this.getHomeService().storeClientAddressDetails(this);
    ok = ok && this.getHomeService().storeLastActionName(this);
    
    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    return SUCCESS;
  }

  @Override
  public void validate() {
    // nothing entered
    if ((this.getResidentialPostcode() == null || this.getResidentialPostcode().isEmpty())
            && (this.getPoBoxPostcode() == null || this.getPoBoxPostcode().isEmpty())) {
      this.addFieldError("postalPostcode", "Please enter a postcode");
    }
    if ((this.getResidentialSuburb() == null || this.getResidentialSuburb().isEmpty())
            && (this.getPoBoxSuburb() == null || this.getPoBoxSuburb().isEmpty())) {
      this.addFieldError("postalSuburb", "Please enter a suburb");
    }
    if ((this.getResidentialStreetNumber() == null || this.getResidentialStreetNumber().isEmpty())
            && (this.getPoBoxNumber() == null || this.getPoBoxNumber().isEmpty())) {
      this.addFieldError("postalStreetNumber", "Please enter a street number");
    }

    // user is filling in postal address
    if ((this.getResidentialPostcode() != null && !this.getResidentialPostcode().isEmpty())
            || (this.getResidentialSuburb() != null && !this.getResidentialSuburb().isEmpty())
            || (this.getResidentialStreet() != null && !this.getResidentialStreet().isEmpty())
            || (this.getResidentialStreetNumber() != null && !this.getResidentialStreetNumber().isEmpty())) {

      if (this.getResidentialPostcode() == null || this.getResidentialPostcode().isEmpty()) {
        this.addFieldError("postalPostcode", "Please enter a postcode");
      }
      if (this.getResidentialSuburb() == null || this.getResidentialSuburb().isEmpty()) {
        this.addFieldError("postalSuburb", "Please enter a suburb");
      }
      if (this.getResidentialStreet() == null || this.getResidentialStreet().isEmpty()) {
        this.addFieldError("postalStreet", "Please enter a street");
      }
      if (this.getResidentialStreetNumber() == null || this.getResidentialStreetNumber().isEmpty()) {
        this.addFieldError("postalStreetNumber", "Please enter a street number");
      }
    }

    // user is filling in PO Box address
    if (this.isDifferntPostalAddress()) {

      if (this.getPoBoxPostcode() == null || this.getPoBoxPostcode().isEmpty()) {
        this.addFieldError("poBoxPostcode", "Please enter a postcode");
      }
      if (this.getPoBoxSuburb() == null || this.getPoBoxSuburb().isEmpty()) {
        this.addFieldError("poBoxSuburb", "Please enter a suburb");
      }
      if (this.getPoBoxNumber() == null || this.getPoBoxNumber().isEmpty()) {
        this.addFieldError("poBoxNumber", "Please enter a GPO/PO Box number");
      }
    }

    if ((this.getPersistedAgentUser() == null || this.getPersistedAgentUser().isEmpty()) 
    		&& (this.getEmail() == null || this.getEmail().isEmpty())) {
      this.addFieldError("email", "Please provide your email address.");
    }

  }

  /**
   * @return the residentialPostcode
   */
  public String getResidentialPostcode() {
    return residentialPostcode;
  }

  /**
   * @param residentialPostcode the residentialPostcode to set
   */
  public void setResidentialPostcode(String residentialPostcode) {
    this.residentialPostcode = residentialPostcode;
  }

  /**
   * @return the residentialSuburb
   */
  public String getResidentialSuburb() {
    return residentialSuburb;
  }

  /**
   * @param residentialSuburb the residentialSuburb to set
   */
  public void setResidentialSuburb(String residentialSuburb) {
    this.residentialSuburb = residentialSuburb;
  }

  /**
   * @return the residentialStreet
   */
  public String getResidentialStreet() {
    return residentialStreet;
  }

  /**
   * @param residentialStreet the residentialStreet to set
   */
  public void setResidentialStreet(String residentialStreet) {
    this.residentialStreet = residentialStreet;
  }

  /**
   * @return the residentialStreetNumber
   */
  public String getResidentialStreetNumber() {
    return residentialStreetNumber;
  }

  /**
   * @param residentialStreetNumber the residentialStreetNumber to set
   */
  public void setResidentialStreetNumber(String residentialStreetNumber) {
    this.residentialStreetNumber = residentialStreetNumber;
  }

  /**
   * @return the poBoxPostcode
   */
  public String getPoBoxPostcode() {
    return poBoxPostcode;
  }

  /**
   * @param poBoxPostcode the poBoxPostcode to set
   */
  public void setPoBoxPostcode(String poBoxPostcode) {
    this.poBoxPostcode = poBoxPostcode;
  }

  /**
   * @return the poBoxSuburb
   */
  public String getPoBoxSuburb() {
    return poBoxSuburb;
  }

  /**
   * @param poBoxSuburb the poBoxSuburb to set
   */
  public void setPoBoxSuburb(String poBoxSuburb) {
    this.poBoxSuburb = poBoxSuburb;
  }

  /**
   * @return the poBoxNumber
   */
  public String getPoBoxNumber() {
    return poBoxNumber;
  }

  /**
   * @param poBoxNumber the poBoxNumber to set
   */
  public void setPoBoxNumber(String poBoxNumber) {
    this.poBoxNumber = poBoxNumber;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the differntPostalAddress
   */
  public boolean isDifferntPostalAddress() {
    return differntPostalAddress;
  }

  /**
   * @param differntPostalAddress the differntPostalAddress to set
   */
  public void setDifferntPostalAddress(boolean differntPostalAddress) {
    this.differntPostalAddress = differntPostalAddress;
  }
  
}
