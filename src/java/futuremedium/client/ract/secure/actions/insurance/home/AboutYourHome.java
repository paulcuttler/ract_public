package futuremedium.client.ract.secure.actions.insurance.home;

import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.WebInsQuoteDetail;
import futuremedium.client.ract.secure.Config;

/**
 * Process About Your Home form.
 * @author gnewton
 */
public class AboutYourHome extends AboutYourHomeBase implements QuoteAware {

  private String buildingType;
  private Integer buildingTypeConjoined;
  private String yearConstructed;
  private String wallConstruction;
  private String roofConstruction;
  private String occupancy;
  private Boolean hardwiredSmokeAlarm;
  private String securityLock;
  private String securityAlarm;
  private String securityOther;
  private String manager;
  private String managerOther;

  static final private String FIELD_VALUE_OCCUPANCY_TENANTED = "INVSOT";

  @Override
  public String execute() throws Exception {

    // retrieve data objects from submitted value
    InRfDet selectedBuldingType = this.getBuildingTypeLookup().get(this.getBuildingType());
    InRfDet selectedWallConstruction = this.getWallConstructionLookup().get(this.getWallConstruction());
    InRfDet selectedRoofConstruction = this.getRoofConstructionLookup().get(this.getRoofConstruction());
    InRfDet selectedOccupancy = this.getOccupancyLookup().get(this.getOccupancy());
    InRfDet selectedSecurityAlarm = this.getSecurityAlarmLookup().get(this.getSecurityAlarm());
    InRfDet selectedSecurityLock = this.getSecurityLockLookup().get(this.getSecurityLock());
    
    InRfDet selectedManager = this.getManagerLookup().get(this.getManager());

    // save details
    boolean ok = this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.YEAR_CONST, this.getYearConstructed());
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.BUILDING_TYPE, this.getBuildingType(), selectedBuldingType);

    if (this.getBuildingType().equals(ActionBase.VALUE_BUILDING_TYPE_CONJOINED)) {
      ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_BUILDING_TYPE_CONJOINED, this.getBuildingTypeConjoined().toString());
    } else {
      ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_BUILDING_TYPE_CONJOINED, ""); // clear value
    }

    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.CONSTRUCTION, this.getWallConstruction(), selectedWallConstruction);
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.ROOF, this.getRoofConstruction(), selectedRoofConstruction);
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.OCCUPANCY, this.getOccupancy(), selectedOccupancy);
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SMOKE_ALARM, this.getHardwiredSmokeAlarm().toString());
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SECURITY_ALARM, this.getSecurityAlarm(), selectedSecurityAlarm);
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.SECURITY_LOCK, this.getSecurityLock(), selectedSecurityLock);

    if (this.getSecurityLock().equals(ActionBase.VALUE_SECURITY_OTHER)) {
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_SECURITY_OTHER, this.getSecurityOther());
    } else {
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_SECURITY_OTHER, ""); // clear value
    }

    if(this.isInvestorOptionsRequired()){
      
      ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.RENTAL_MANAGER, this.getManager(), selectedManager);

      if (this.getManager().equals(ActionBase.VALUE_MANAGER_OTHER)) {
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_MANAGER_OTHER, this.getManagerOther());
      } else {
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_MANAGER_OTHER, ""); // clear value
      }

    }

    


    if (ok) {
      //this.setActionMessage(this.getSuccessMessage());
    } else {
      this.setActionMessage(this.getErrorMessage());
      return ERROR;
    }

    String abortMessage = Config.configManager.getProperty("insurance.home.exclusion.options.message");
    if (this.isBuildingOptionsRequired() || this.isInvestorOptionsRequired()) {
      abortMessage += " building";
    }
    if (this.isBuildingOptionsRequired() && this.isContentsOptionsRequired()) {
      abortMessage += " and";
    }
    if (this.isContentsOptionsRequired()) {
      abortMessage += " contents";
    }

    // exit on exclusions
    if (!selectedBuldingType.isAcceptable()) {
      return this.getHomeService().abortQuote(this, "unacceptable Building Type", this.getBuildingType(), abortMessage);
    }
    if (!selectedWallConstruction.isAcceptable()) {
      return this.getHomeService().abortQuote(this, "unacceptable Wall Construction", this.getWallConstruction(), abortMessage);
    }    
    if (!selectedRoofConstruction.isAcceptable()) {
      return this.getHomeService().abortQuote(this, "unacceptable Roof Construction", this.getRoofConstruction(), abortMessage);
    }
    // selected occupancy may be null if an investor quote
    if (selectedOccupancy != null && !selectedOccupancy.isAcceptable() && !this.isInvestorOptionsRequired()) {
      return this.getHomeService().abortQuote(this, "unacceptable Occupancy", this.getOccupancy(), abortMessage);
    }
    if (this.getBuildingTypeConjoined() != null && this.getBuildingTypeConjoined() > Integer.parseInt(Config.configManager.getProperty("insurance.home.maxConjoinedBuldings"))) {
      return this.getHomeService().abortQuote(this, "exceeds max conjoined buildings", this.getBuildingTypeConjoined().toString(), Config.configManager.getProperty("insurance.home.exclusion.conjoinedBuildings.message"));
    }

    return SUCCESS;
  }

  @Override
  public void validate() {  
    if (!this.isInvestorOptionsRequired()) {
      if (this.getOccupancy() == null || this.getOccupancy().isEmpty()) {
        this.addFieldError("occupancy", "Please nominate the Occupancy");
      }
    }

    if (ActionBase.VALUE_SECURITY_OTHER.equals(this.getSecurityLock())) {
        if (this.getSecurityOther() == null || this.getSecurityOther().isEmpty()) {
            this.addFieldError(ActionBase.FIELD_SECURITY_OTHER, "Please provide details of your security measures");
        }
    }

    if (this.isInvestorOptionsRequired()) {
      if (this.getManager() == null || this.getManager().isEmpty()) {
        this.addFieldError("manager", "Please nominate who manages the property");
      }

      if (this.getManager().equals(ActionBase.VALUE_MANAGER_OTHER)) {
        if (this.getManagerOther() == null || this.getManagerOther().isEmpty()) {
          this.addFieldError("managerOther", "Please provide details of who manages the property");
        }
      }
    }

    if (this.getBuildingType().equals(ActionBase.VALUE_BUILDING_TYPE_CONJOINED)) {
      if (this.getBuildingTypeConjoined() == null) {
        this.addFieldError("buildingTypeConjoined", "Please enter how many buildings/units are conjoined on the site");
      } else if (this.getBuildingTypeConjoined() == 1) {
        this.addFieldError("buildingTypeConjoined", "Conjoined buildings/units must be greater than 1");
      }
    }
  }

  /**
   * @return the buildingType
   */
  public String getBuildingType() {
    return buildingType;
  }

  /**
   * @param buildingType the buildingType to set
   */
  @RequiredStringValidator(message="Please nominate a Building Type")
  public void setBuildingType(String buildingType) {
    this.buildingType = buildingType;
  }

  /**
   * @return the yearConstructed
   */
  public String getYearConstructed() {
    return yearConstructed;
  }

  /**
   * @param yearConstructed the yearConstructed to set
   */
  @RequiredStringValidator(message="Please nominate the Year Constructed")
  public void setYearConstructed(String yearConstructed) {
    this.yearConstructed = yearConstructed;
  }

  /**
   * @return the wallConstruction
   */
  public String getWallConstruction() {
    return wallConstruction;
  }

  /**
   * @param wallConstruction the wallConstruction to set
   */
  @RequiredStringValidator(message="Please nominate the Wall Construction")
  public void setWallConstruction(String wallConstruction) {
    this.wallConstruction = wallConstruction;
  }

  /**
   * @return the roofConstruction
   */
  public String getRoofConstruction() {
    return roofConstruction;
  }

  /**
   * @param roofConstruction the roofConstruction to set
   */
  @RequiredStringValidator(message="Please nominate the Roof Construction")
  public void setRoofConstruction(String roofConstruction) {
    this.roofConstruction = roofConstruction;
  }

  /**
   * @return the occupancy
   */
  public String getOccupancy() {
    // for Investor, always return Tenanted
    if (this.isInvestorOptionsRequired()) {
      this.occupancy = AboutYourHome.FIELD_VALUE_OCCUPANCY_TENANTED;
    }
    
    return occupancy;
  }

  /**
   * @param occupancy the occupancy to set
   */
  public void setOccupancy(String occupancy) {
    this.occupancy = occupancy;
  }

  /**
   * @return the hardwiredSmokeAlarm
   */
  public Boolean getHardwiredSmokeAlarm() {
    return hardwiredSmokeAlarm;
  }

  /**
   * @param hardwiredSmokeAlarm the hardwiredSmokeAlarm to set
   */
  @RequiredFieldValidator(message="Please nominate whether a hardwired smoke alarm is fitted")
  public void setHardwiredSmokeAlarm(Boolean hardwiredSmokeAlarm) {
    this.hardwiredSmokeAlarm = hardwiredSmokeAlarm;
  }

  public String getSecurityLock() {
      return securityLock;
  }
  
  @RequiredStringValidator(message="Please nominate the maximum security locks fitted to your building")
  public void setSecurityLock(String securityLock) {
      this.securityLock = securityLock;
  }
  
  public String getSecurityAlarm() {
      return securityAlarm;
  }
  
  @RequiredStringValidator(message="Please nominate the maximum security alarm fitted to your building")
  public void setSecurityAlarm(String securityAlarm) {
      this.securityAlarm = securityAlarm;
  }

  /**
   * @return the securityOther
   */
  public String getSecurityOther() {
    return securityOther;
  }

  /**
   * @param securityOther the securityOther to set
   */
  public void setSecurityOther(String securityOther) {
    this.securityOther = securityOther;
  }

  /**
   * @return the buildingTypeConjoined
   */
  public Integer getBuildingTypeConjoined() {
    return buildingTypeConjoined;
  }

  /**
   * @param buildingTypeConjoined the buildingTypeConjoined to set
   */
  public void setBuildingTypeConjoined(Integer buildingTypeConjoined) {
    this.buildingTypeConjoined = buildingTypeConjoined;
  }

  /**
   * @return the manager
   */
  public String getManager() {
    return manager;
  }

  /**
   * @param manager the manager to set
   */
  
  public void setManager(String manager) {
    this.manager = manager;
  }

  /**
   * @return the managerOther
   */
  public String getManagerOther() {
    return managerOther;
  }

  /**
   * @param managerOther the managerOther to set
   */
  public void setManagerOther(String managerOther) {
    this.managerOther = managerOther;
  }

}
