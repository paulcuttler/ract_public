package futuremedium.client.ract.secure.actions.insurance.car;

import futuremedium.client.ract.secure.entities.insurance.car.*;

/**
 * Load final summary page action.
 * @author xmfu
 */
public class QuotePageSave10 extends QuotePageSave {
  private String compAnnualPremium;
  private String compMonthlyPremium;
  private String tpAnnualPremium;
  private String tpMonthlyPremium;
  private QuoteTempMemoryObject tempMemoryObject;

  @Override
  public String execute() throws Exception {
    this.setNextAction();
    return SUCCESS;
  }

  /**
   * @return the compAnnualPremium
   */
  public String getCompAnnualPremium() {
    return compAnnualPremium;
  }

  /**
   * @param compAnnualPremium the compAnnualPremium to set
   */
  public void setCompAnnualPremium(String compAnnualPremium) {
    this.compAnnualPremium = compAnnualPremium;
  }

  /**
   * @return the compMonthlyPremium
   */
  public String getCompMonthlyPremium() {
    return compMonthlyPremium;
  }

  /**
   * @param compMonthlyPremium the compMonthlyPremium to set
   */
  public void setCompMonthlyPremium(String compMonthlyPremium) {
    this.compMonthlyPremium = compMonthlyPremium;
  }

  /**
   * @return the tpAnnualPremium
   */
  public String getTpAnnualPremium() {
    return tpAnnualPremium;
  }

  /**
   * @param tpAnnualPremium the tpAnnualPremium to set
   */
  public void setTpAnnualPremium(String tpAnnualPremium) {
    this.tpAnnualPremium = tpAnnualPremium;
  }

  /**
   * @return the tpMonthlyPremium
   */
  public String getTpMonthlyPremium() {
    return tpMonthlyPremium;
  }

  /**
   * @param tpMonthlyPremium the tpMonthlyPremium to set
   */
  public void setTpMonthlyPremium(String tpMonthlyPremium) {
    this.tpMonthlyPremium = tpMonthlyPremium;
  }

  /**
   * @return the tempMemoryObject
   */
  public QuoteTempMemoryObject getTempMemoryObject() {
    return tempMemoryObject;
  }

  /**
   * @param tempMemoryObject the tempMemoryObject to set
   */
  public void setTempMemoryObject(QuoteTempMemoryObject tempMemoryObject) {
    this.tempMemoryObject = tempMemoryObject;
  }
}