package futuremedium.client.ract.secure.actions.insurance;

public interface QasResidentialAware {

	public String getResidentialAddressQuery();
  public String getResidentialPostcode();
  public String getResidentialSuburb();  
  public String getResidentialStreet();  
  public String getResidentialStreetNumber();  
	public String getMoniker();	
	public String getResidentialLatitude();	
	public String getResidentialLongitude();	
	public String getResidentialGnaf();
	
	public void setResidentialAddressQuery(String addressQuery);
	public void setResidentialPostcode(String residentialPostcode);
  public void setResidentialSuburb(String residentialSuburb);
  public void setResidentialStreet(String residentialStreet);
  public void setResidentialStreetNumber(String residentialStreetNumber);
	public void setMoniker(String moniker);
	public void setResidentialLatitude(String residentialLatitude);
	public void setResidentialLongitude(String residentialLongitude);
	public void setResidentialGnaf(String residentialGnaf);
	
}
