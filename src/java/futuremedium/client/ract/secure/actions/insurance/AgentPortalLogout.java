package futuremedium.client.ract.secure.actions.insurance;

import futuremedium.client.ract.secure.entities.insurance.AgentService;

public class AgentPortalLogout extends InsuranceBase {

  private AgentService agentService;

  @Override
  public String execute() throws Exception {

    this.getAgentService().logout(this);
    
    return SUCCESS;
  }
  
	public AgentService getAgentService() {
		return agentService;
	}

	public void setAgentService(AgentService agentService) {
		this.agentService = agentService;
	}
  
	@Override
  public String getPersistedAgentCode() {
  	return null;
  }
  
	@Override
  public String getPersistedAgentUser() {
  	return null;
  }
}
