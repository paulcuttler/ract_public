package futuremedium.client.ract.secure.actions.insurance.car;

import com.opensymphony.xwork2.ActionContext;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.insurance.QasResidentialAware;
import futuremedium.client.ract.secure.entities.insurance.car.GaragedAddress;

/**
 *
 * @author xmfu
 */
public class QuotePageSave4 extends QuotePageSave implements QasResidentialAware {
    private static final long serialVersionUID = 3378027056025386456L;
    private String moniker;
	private String residentialAddressQuery;
  private String residentialPostcode;
  private String residentialSuburb;
  private String residentialStreet;
  private String residentialStreetSuburbId;
  private String residentialStreetNumber;
  private String residentialLatitude;
  private String residentialLongitude;
  private String residentialGnaf;
  
  GaragedAddress garagedAddress = new GaragedAddress();

  @Override
  public String execute() throws Exception {
    this.setUp();
    int quoteNo = this.getQuoteNo();

    // If user has reached page 9, access from page 1 to page 8 is not allowed.
    if (isReachedPage9()) {
      this.setNextAction();
      return SUCCESS;
    }

    // go back to previous page.
    if (this.getSubmit().equals(BUTTON_BACK)) {
      this.setNextAction();
      return SUCCESS;
    }

    
    if (this.getMoniker() != null && !this.getMoniker().isEmpty()) {
  		// populate up via QAS
  		this.getQasService().populateAddressFields(this);
  	}

    // Stored address to session for later use in summary page and for page reloading after validation is failed.
    garagedAddress.setPostCode(this.getResidentialPostcode());
    garagedAddress.setStreetSuburbId(this.getResidentialStreetSuburbId());
    garagedAddress.setStreetNumber(this.getResidentialStreetNumber());
    garagedAddress.setSuburb(this.getResidentialSuburb());
    garagedAddress.setStreet(this.getResidentialStreet());
    garagedAddress.setLatitude(this.getResidentialLatitude());
    garagedAddress.setLongitude(this.getResidentialLongitude());
    garagedAddress.setGnaf(this.getResidentialGnaf());
        
    // Store address.
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_NO, this.getResidentialStreetNumber());
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_STREET, this.getResidentialStreet());
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_POSTCODE, this.getResidentialPostcode());
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_SUBURB, this.getResidentialSuburb());

    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_LATITUDE, this.getResidentialLatitude());
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_LONGITUDE, this.getResidentialLongitude());
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_GNAF, this.getResidentialGnaf());
    
    this.setResidentialStreetSuburbId(this.getAddressService().lookupStreetSuburbId(this.getResidentialSuburb(), this.getResidentialStreet(), this.getResidentialPostcode(), true).toString());
    insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_ID, this.getResidentialStreetSuburbId());
    
    // Store garaged address.
    String garagedAddressString = this.getResidentialStreetNumber() + " " + this.getResidentialStreet() + " " + this.getResidentialSuburb() + " " + this.getResidentialPostcode();
    insuranceMgr.setQuoteDetail(quoteNo, "garagedAddress", garagedAddressString);

    String suburbValidationOutput = this.getInsuranceMgr().getInsSetting(
            WebInsQuoteDetail.SUBURB_UNACCEPTABLE, 
            this.getResidentialSuburb(), 
            this.getInsuranceMgr().getQuote(this.getQuoteNo()).getQuoteDate());
    
    if ("EB".equals(suburbValidationOutput)) {
        return this.messagePage(MESSAGE_UNACCEPTABLE_SUBURB);
    }
    if ("UK".equals(suburbValidationOutput)) {
        return this.messagePage(MESSAGE_UNKNOWN_SUBURB);
    }
    
    this.setNextAction();
    return SUCCESS;

  }

  @Override
  public void validate() {
    if (this.getSubmit().equals(BUTTON_BACK)) {
      return;
    }

    boolean valid = true;
  	
  	if (this.getMoniker() == null || this.getMoniker().isEmpty()) {
  		
  		if (this.getResidentialStreetNumber() == null || this.getResidentialStreetNumber().isEmpty()) {
  			valid = false;
  		}
  		if (this.getResidentialStreet() == null || this.getResidentialStreet().isEmpty()) {
  			valid = false;
  		}
  		if (this.getResidentialSuburb() == null || this.getResidentialSuburb().isEmpty()) {
  			valid = false;
  		}
  		if (this.getResidentialPostcode() == null || this.getResidentialPostcode().isEmpty()) {
  			valid = false;
  		}
// not all addresses coming from QaS are returning LAT / LONG ... they should be optional in this validation
// Also, jspx form address validation looks for GNaf only, not lat and long, when checking if an address is valid
//  		if (this.getResidentialLatitude() == null || this.getResidentialLatitude().isEmpty()) {
//  			valid = false;
//  		}
//  		if (this.getResidentialLongitude() == null || this.getResidentialLongitude().isEmpty()) {
//  			valid = false;
//  		}
  		if (this.getResidentialGnaf() == null || this.getResidentialGnaf().isEmpty()) {
  			valid = false;
  		}
  		  		
  	}
  	
  	if (!valid) {
  		this.addFieldError("residentialAddressQuery", "Unable to verify your address");
  	}

    if (!this.getFieldErrors().isEmpty()) {
    	try {
        garagedAddress.setPostCode(this.getResidentialPostcode());
        garagedAddress.setStreetSuburbId(this.getResidentialStreetSuburbId());
        garagedAddress.setStreetNumber(this.getResidentialStreetNumber());
        garagedAddress.setStreet(this.getResidentialStreet());
        garagedAddress.setSuburb(this.getResidentialSuburb());
        garagedAddress.setLatitude(this.getResidentialLatitude());
        garagedAddress.setLongitude(this.getResidentialLongitude());
        garagedAddress.setGnaf(this.getResidentialGnaf());

        ActionContext.getContext().getValueStack().setValue("residentialAddressQuery", this.getResidentialAddressQuery());
        
        this.loadPage4(garagedAddress);
      
	    } catch (Exception e) {
	      Config.logger.debug(Config.key + ": loadpage4 failed on validation: ", e);
	    }
    }
  }

  /**
   * @return the postcode
   */
  public String getResidentialPostcode() {
    return residentialPostcode;
  }

  /**
   * @param postcode the postcode to set
   */
  public void setResidentialPostcode(String postcode) {
    this.residentialPostcode = postcode;
  }

  /**
   * @return the suburb
   */
  public String getResidentialSuburb() {
    return residentialSuburb;
  }

  /**
   * @param suburb the suburb to set
   */
  public void setResidentialSuburb(String suburb) {
    this.residentialSuburb = suburb;
  }

  /**
   * @return the streetName
   */
  public String getResidentialStreetSuburbId() {
    return residentialStreetSuburbId;
  }

  /**
   * @param streetName the streetName to set
   */
  public void setResidentialStreetSuburbId(String streetSuburbId) {
    this.residentialStreetSuburbId = streetSuburbId;
  }

  /**
   * @return the streetNumber
   */
  public String getResidentialStreetNumber() {
    return residentialStreetNumber;
  }

  /**
   * @param streetNumber the streetNumber to set
   */
  public void setResidentialStreetNumber(String streetNumber) {
    this.residentialStreetNumber = streetNumber;
  }

	/**
	 * @return the moniker
	 */
	public String getMoniker() {
		return moniker;
	}

	/**
	 * @param moniker the moniker to set
	 */
	public void setMoniker(String moniker) {
		this.moniker = moniker;
	}

	/**
	 * @return the latitude
	 */
	public String getResidentialLatitude() {
		return residentialLatitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setResidentialLatitude(String latitude) {
		this.residentialLatitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getResidentialLongitude() {
		return residentialLongitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setResidentialLongitude(String longitude) {
		this.residentialLongitude = longitude;
	}

	/**
	 * @return the gnaf
	 */
	public String getResidentialGnaf() {
		return residentialGnaf;
	}

	/**
	 * @param gnaf the gnaf to set
	 */
	public void setResidentialGnaf(String gnaf) {
		this.residentialGnaf = gnaf;
	}

	/**
	 * @return the street
	 */
	public String getResidentialStreet() {
		return residentialStreet;
	}

	/**
	 * @param street the street to set
	 */
	public void setResidentialStreet(String street) {
		this.residentialStreet = street;
	}

	public String getResidentialAddressQuery() {
		return residentialAddressQuery;
	}

	public void setResidentialAddressQuery(String addressQuery) {
		this.residentialAddressQuery = addressQuery;		
	}
}
