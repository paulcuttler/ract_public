package futuremedium.client.ract.secure.actions.insurance.home;

/**
 *
 * @author gnewton
 */
public class PolicyholderDetailsForm extends PolicyholderDetailsBase implements PolicyAware {
    private static final long serialVersionUID = 8320800095986383168L;
    private String selectedClient;
    
    /**
     * @return the selectedClient
     */
    public String getSelectedClient() {
        if(null == selectedClient) {
            selectedClient = getSelectedPolicyHolder();
        }
        return selectedClient;
    }

    /**
     * @param selectedClient the selectedClient to set
     */
    public void setSelectedClient(String selectedClient) {
        this.selectedClient = selectedClient;
    }
}
