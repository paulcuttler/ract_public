package futuremedium.client.ract.secure.actions.insurance.home;

import java.math.BigDecimal;

import com.ract.web.insurance.Premium;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;

/**
 * Display the Premium / Options form.
 * Save the initial premium values for reporting.
 * @author gnewton
 */
public class PremiumOptionsForm extends PremiumOptionsBase implements QuoteAware {
    private static final long serialVersionUID = 3823773820758470186L;
  private String buildingExcess;
  private String contentsExcess;
  private String investorExcess;
  private Boolean buildingAccidentalDamage;
  private Boolean contentsAccidentalDamage;
  private Boolean buildingMotorDamage;
  private Boolean contentsMotorDamage;
  private Boolean investorMotorDamage;
  private Boolean investorContentsMotorDamage;
  private Boolean buildingStormDamage;
  private Boolean investorStormDamage;
  private Boolean unspecifiedPersonalEffects;
  private Boolean workersCompensation;
  private Boolean investorCover30;
  private Boolean investorCover50;

  @Override
  public String execute() throws Exception {
    boolean ok = true;
    
    // store Building initial premium and default excess, don't update lastAction
    if (this.isBuildingOptionsRequired()) {
      String currentExcess = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.BLDG_EXCESS);
      if (currentExcess == null || currentExcess.isEmpty()) {
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.BLDG_EXCESS, this.getBuildingDefaultExcess(), null, false);
      }
    }

    // store Contents initial premium and default excess, don't update lastAction
    if (this.isContentsOptionsRequired()) {
      String currentExcess = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.CNTS_EXCESS);
      if (currentExcess == null || currentExcess.isEmpty()) {
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.CNTS_EXCESS, this.getContentsDefaultExcess(), null, false);
      }
    }

    // store Investor initial premium and default excess, don't update lastAction
    if (this.isInvestorOptionsRequired()) {
      String currentExcess = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.INV_EXCESS);
      if (currentExcess == null || currentExcess.isEmpty()) {
        ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.INV_EXCESS, this.getInvestorDefaultExcess(), null, false);
      }
    }

    // record that premium has been viewed
    ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_VIEWED_PREMIUM, Boolean.TRUE.toString(), null, false);

    Premium annualPremium = this.getHomeService().retrievePremium(this, WebInsQuoteDetail.PAY_ANNUALLY, WebInsQuote.COVER_BLDG);
    Premium monthlyPremium = this.getHomeService().retrievePremium(this, WebInsQuoteDetail.PAY_MONTHLY, WebInsQuote.COVER_BLDG);
    
    if (this.isBuildingOptionsRequired()) {
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_BUILDING_MONTHLY_PREMIUM, monthlyPremium.getPayment().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_BUILDING_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString(), null, false);
    }
    if (this.isContentsOptionsRequired()) {
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_CONTENTS_MONTHLY_PREMIUM, monthlyPremium.getPayment().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_CONTENTS_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString(), null, false);
    }
    if (this.isInvestorOptionsRequired()) {
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_INVESTOR_MONTHLY_PREMIUM, monthlyPremium.getPayment().toString(), null, false);
        ok = ok && this.getHomeService().storeQuoteDetail(this, ActionBase.FIELD_INVESTOR_ANNUAL_PREMIUM, annualPremium.getAnnualPremium().toString(), null, false);
    }
    
    BigDecimal tmpAnnualPremium = annualPremium.getAnnualPremium();
    BigDecimal tmpMonthlyPremium = monthlyPremium.getPayment();
    
    // save total premium to DB
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.PREMIUM, tmpAnnualPremium.toString(), null, false);
    ok = ok && this.getHomeService().storeQuoteDetail(this, WebInsQuoteDetail.PAYMENT, tmpMonthlyPremium.toString(), null, false);
    
    // store total premiums in PremiumOptionsBase
    this.setTotalAnnualPremium(tmpAnnualPremium);
    this.setTotalMonthlyPremium(tmpMonthlyPremium);
    
    if (!ok) {
      Config.logger.error(Config.key + ": unable to store initial premium for Home Quote #" + this.getQuoteNo());
    }
    
    if (tmpAnnualPremium.compareTo(BigDecimal.ZERO) != 1 || tmpMonthlyPremium.compareTo(BigDecimal.ZERO) != 1) { // not > 0
    	return this.getHomeService().abortQuote(this,
          "zero premium",
          tmpAnnualPremium.toString(),
          Config.configManager.getProperty("insurance.home.retrieve.aborted.message"));    	
    }

    return SUCCESS;
  }

  /**
   * @return the buildingAccidentalDamage
   */
  public Boolean isBuildingAccidentalDamage() {
    if (this.buildingAccidentalDamage == null) {
      this.buildingAccidentalDamage = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_BLDG_ACCIDENTAL_DAMAGE));
    }
    return buildingAccidentalDamage;
  }

  /**
   * @param buildingAccidentalDamage the buildingAccidentalDamage to set
   */
  public void setBuildingAccidentalDamage(Boolean buildingAccidentalDamage) {
    this.buildingAccidentalDamage = buildingAccidentalDamage;
  }

  /**
   * @return the buildingStormDamage
   */
  public Boolean isBuildingStormDamage() {
    if (this.buildingStormDamage == null) {
      this.buildingStormDamage = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_BLDG_STORM_DAMAGE));
    }
    return buildingStormDamage;
  }

  /**
   * @param stormDamage the stormDamage to set
   */
  public void setBuildingStormDamage(Boolean buildingStormDamage) {
    this.buildingStormDamage = buildingStormDamage;
  }

  /**
   * @return the buildingMotorDamage
   */
  public Boolean isBuildingMotorDamage() {
    if (this.buildingMotorDamage == null) {
      this.buildingMotorDamage = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_BLDG_ELECTRIC_MOTORS));
    }
    return buildingMotorDamage;
  }

  /**
   * @param buildingMotorDamage the buildingMotorDamage to set
   */
  public void setBuildingMotorDamage(Boolean buildingMotorDamage) {
    this.buildingMotorDamage = buildingMotorDamage;
  }

  /**
   * @return the unspecifiedPersonalEffects
   */
  public Boolean isUnspecifiedPersonalEffects() {
    if (this.unspecifiedPersonalEffects == null) {
      this.unspecifiedPersonalEffects = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_CNTS_PERSONAL_EFFECTS));
    }
    return unspecifiedPersonalEffects;
  }

  /**
   * @param unspecifiedPersonalEffects the unspecifiedPersonalEffects to set
   */
  public void setUnspecifiedPersonalEffects(Boolean unspecifiedPersonalEffects) {
    this.unspecifiedPersonalEffects = unspecifiedPersonalEffects;
  }

  /**
   * @return the contentsAccidentalDamage
   */
  public Boolean isContentsAccidentalDamage() {
    if (this.contentsAccidentalDamage == null) {
      this.contentsAccidentalDamage = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_CNTS_ACCIDENTAL_DAMAGE));
    }
    return contentsAccidentalDamage;
  }

  /**
   * @param contentsAccidentalDamage the contentsAccidentalDamage to set
   */
  public void setContentsAccidentalDamage(Boolean contentsAccidentalDamage) {
    this.contentsAccidentalDamage = contentsAccidentalDamage;
  }

  /**
   * @return the investorStormDamage
   */
  public Boolean isInvestorStormDamage() {
    if (this.investorStormDamage == null) {
      this.investorStormDamage = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_INV_STORM_DAMAGE));
    }
    return investorStormDamage;
  }

  /**
   * @param investorStormDamage the investorStormDamage to set
   */
  public void setInvestorStormDamage(Boolean investorStormDamage) {
    this.investorStormDamage = investorStormDamage;
  }

  /**
   * @return the workersCompensation
   */
  public Boolean isWorkersCompensation() {
    if (this.workersCompensation == null) {
      this.workersCompensation = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_CNTS_DWC));
    }
    return workersCompensation;
  }

  /**
   * @param workersCompensation the workersCompensation to set
   */
  public void setWorkersCompensation(Boolean workersCompensation) {
    this.workersCompensation = workersCompensation;
  }

  /**
   * @return the contentsMotorDamage
   */
  public Boolean isContentsMotorDamage() {
    if (this.contentsMotorDamage == null) {
      this.contentsMotorDamage = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_CNTS_ELECTRIC_MOTORS));
    }
    return contentsMotorDamage;
  }

  /**
   * @param contentsMotorDamage the contentsMotorDamage to set
   */
  public void setContentsMotorDamage(Boolean contentsMotorDamage) {
    this.contentsMotorDamage = contentsMotorDamage;
  }

  /**
   * @return the investorMotorDamage
   */
  public Boolean isInvestorMotorDamage() {
    if (this.investorMotorDamage == null) {
      this.investorMotorDamage = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_INV_ELECTRIC_MOTORS));
    }
    return investorMotorDamage;
  }

  /**
   * @param investorMotorDamage the investorMotorDamage to set
   */
  public void setInvestorMotorDamage(Boolean investorMotorDamage) {
    this.investorMotorDamage = investorMotorDamage;
  }
  
  /**
   * @return the investorContentsMotorDamage
   */
  public Boolean isInvestorContentsMotorDamage() {
      if (this.investorContentsMotorDamage == null) {
          this.investorContentsMotorDamage = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_INV_CONTENTS_ELECTRIC_MOTORS));
      }
      return investorContentsMotorDamage;
  }
  
  /**
   * @param investorContentsMotorDamage the investorContentsMotorDamage to set
   */
  public void setInvestorContentsMotorDamage(Boolean investorContentsMotorDamage) {
      this.investorContentsMotorDamage = investorContentsMotorDamage;
  }

  /**
   * @return the buildingExcess
   */
  public String getBuildingExcess() throws Exception {
    if (this.buildingExcess == null) {
      this.buildingExcess = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.BLDG_EXCESS);
      if (this.buildingExcess == null) {
        this.buildingExcess = this.getBuildingDefaultExcess();
      }
    }
    return buildingExcess;
  }

  /**
   * @param buildingExcess the buildingExcess to set
   */
  public void setBuildingExcess(String buildingExcess) {
    this.buildingExcess = buildingExcess;
  }

  /**
   * @return the contentsExcess
   */
  public String getContentsExcess() throws Exception {
    if (this.contentsExcess == null) {
      this.contentsExcess = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.CNTS_EXCESS);
      if (this.contentsExcess == null) {
        this.contentsExcess = this.getContentsDefaultExcess();
      }
    }
    return contentsExcess;
  }

  /**
   * @param contentsExcess the contentsExcess to set
   */
  public void setContentsExcess(String contentsExcess) {
    this.contentsExcess = contentsExcess;
  }

  /**
   * @return the investorExcess
   */
  public String getInvestorExcess() throws Exception {
    if (this.investorExcess == null) {
      this.investorExcess = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.INV_EXCESS);
      if (this.investorExcess == null) {
        this.investorExcess = this.getInvestorDefaultExcess();
      }
    }
    return investorExcess;
  }

  /**
   * @param investorExcess the investorExcess to set
   */
  public void setInvestorExcess(String investorExcess) {
    this.investorExcess = investorExcess;
  }

  /**
   * @return the investorCover30
   */
  public Boolean isInvestorCover30() {
    if (this.investorCover30 == null) {
      this.investorCover30 = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_INV_CONTENTS_COVER_30));
    }
    return investorCover30;
  }

  /**
   * @param investorCover30 the investorCover30 to set
   */
  public void setInvestorCover30(Boolean investorCover30) {
    this.investorCover30 = investorCover30;
  }

  /**
   * @return the investorCover50
   */
  public Boolean isInvestorCover50() {
    if (this.investorCover50 == null) {
      this.investorCover50 = Boolean.parseBoolean(this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), PremiumOptionsBase.VALUE_INV_CONTENTS_COVER_50));
    }
    return investorCover50;
  }

  /**
   * @param investorCover50 the investorCover50 to set
   */
  public void setInvestorCover50(Boolean investorCover50) {
    this.investorCover50 = investorCover50;
  }
}
