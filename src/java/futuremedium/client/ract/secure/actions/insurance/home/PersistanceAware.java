package futuremedium.client.ract.secure.actions.insurance.home;

/**
 * Marker interface to say that this action needs to be able to persist via the API.
 * Used in conjunction with @HomeInsuranceQuoteCheck interceptor so that we don't
 * try to write to DB if we have no quoteNo.
 * @author gnewton
 */
public interface PersistanceAware {

}
