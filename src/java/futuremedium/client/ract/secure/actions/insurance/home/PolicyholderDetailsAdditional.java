package futuremedium.client.ract.secure.actions.insurance.home;

import futuremedium.client.ract.secure.entities.insurance.home.Page;

/**
 * Save additional policy holder details and return to Policyholder Details Form.
 * @author gnewton
 */
public class PolicyholderDetailsAdditional extends PolicyholderDetails implements PolicyAware {

  @Override
  public String getPageTitle() {
    return "Policyholder Details";
  }

  @Override
  public Page getPage() {
    return this.getHomeService().getPage("HomePolicyholderDetailsForm");
  }

  

}
