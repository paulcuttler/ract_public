package futuremedium.client.ract.secure.actions.insurance.home;

import com.ract.web.insurance.WebInsQuoteDetail;

/**
 *
 * @author gnewton
 */
public class AboutYourHomePolicyForm extends AboutYourHomePolicyBase implements PolicyAware {

    private static final long serialVersionUID = -2071187402473617512L;
  private String businessUse;
  private Boolean goodOrder;
  private Boolean currentlyUnoccupied;
  private Boolean within90Days;
  private Boolean workUndertaken;
  private Boolean exceed50K;
  private Boolean unrelatedPersons;
  private Boolean unrelatedExceed2;
  private Boolean underFinance;
  private String financier;

  /**
   * @return the underFinance
   */
  public Boolean isUnderFinance() {
    if (this.underFinance == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_UNDER_FINANCE);
      if (value == null) {
        this.underFinance = null; // neither option selected
      } else {
        this.underFinance = Boolean.parseBoolean(value);
      }
    }
    return underFinance;
  }

  /**
   * @param underFinance the underFinance to set
   */
  public void setUnderFinance(Boolean underFinance) {
    this.underFinance = underFinance;
  }

  /**
   * @return the unrelatedExceed2
   */
  public Boolean isUnrelatedExceed2() {
    if (this.unrelatedExceed2 == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_UNRELATED_EXCEED_2);
      if (value == null) {
        this.unrelatedExceed2 = null; // neither option selected
      } else {
        this.unrelatedExceed2 = Boolean.parseBoolean(value);
      }
    }
    return unrelatedExceed2;
  }

  /**
   * @param unrelatedExceed2 the unrelatedExceed2 to set
   */
  public void setUnrelatedExceed2(Boolean totalUnrelated) {
    this.unrelatedExceed2 = totalUnrelated;
  }

  /**
   * @return the unrelatedPersons
   */
  public Boolean isUnrelatedPersons() {
    if (this.unrelatedPersons == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_UNRELATED_PERSONS);
      if (value == null) {
        this.unrelatedPersons = null; // neither option selected
      } else {
        this.unrelatedPersons = Boolean.parseBoolean(value);
      }
    }
    return unrelatedPersons;
  }

  /**
   * @param unrelatedPersons the unrelatedPersons to set
   */
  public void setUnrelatedPersons(Boolean unrelatedPersons) {
    this.unrelatedPersons = unrelatedPersons;
  }

  /**
   * @return the exceed50K
   */
  public Boolean isExceed50K() {
    if (this.exceed50K == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.EXCEED_50K);
      if (value == null) {
        this.exceed50K = null; // neither option selected
      } else {
        this.exceed50K = Boolean.parseBoolean(value);
      }
    }
    return exceed50K;
  }

  /**
   * @param exceed50K the exceed50K to set
   */
  public void setExceed50K(Boolean exceed50K) {
    this.exceed50K = exceed50K;
  }

  /**
   * @return the workUndertaken
   */
  public Boolean isWorkUndertaken() {
    if (this.workUndertaken == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.WORK_UNDERTAKEN);
      if (value == null) {
        this.workUndertaken = null; // neither option selected
      } else {
        this.workUndertaken = Boolean.parseBoolean(value);
      }
    }
    return workUndertaken;
  }

  /**
   * @param workUndertaken the workUndertaken to set
   */
  public void setWorkUndertaken(Boolean workUndertaken) {
    this.workUndertaken = workUndertaken;
  }

  /**
   * @return the within90Days
   */
  public Boolean isWithin90Days() {
    if (this.within90Days == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_WITHIN_90_DAYS);
      if (value == null) {
        this.within90Days = null; // neither option selected
      } else {
        this.within90Days = Boolean.parseBoolean(value);
      }
    }
    return within90Days;
  }

  /**
   * @param within90Days the within90Days to set
   */
  public void setWithin90Days(Boolean within90Days) {
    this.within90Days = within90Days;
  }

  /**
   * @return the currentlyUnoccupied
   */
  public Boolean isCurrentlyUnoccupied() {
    if (this.currentlyUnoccupied == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_CURRENTLY_UNOCCUPIED);
      if (value == null) {
        this.currentlyUnoccupied = null; // neither option selected
      } else {
        this.currentlyUnoccupied = Boolean.parseBoolean(value);
      }
    }
    return currentlyUnoccupied;
  }

  /**
   * @param currentlyUnoccupied the currentlyUnoccupied to set
   */
  public void setCurrentlyUnoccupied(Boolean currentlyUnoccupied) {
    this.currentlyUnoccupied = currentlyUnoccupied;
  }

  /**
   * @return the goodOrder
   */
  public Boolean isGoodOrder() {
    if (this.goodOrder == null) {
      String value = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), ActionBase.FIELD_GOOD_ORDER);
      if (value == null) {
        this.goodOrder = null; // neither option selected
      } else {
        this.goodOrder = Boolean.parseBoolean(value);
      }
    }
    return goodOrder;
  }

  /**
   * @param goodOrder the goodOrder to set
   */
  public void setGoodOrder(Boolean goodOrder) {
    this.goodOrder = goodOrder;
  }

  /**
   * @return the businessUse
   */
  public String getBusinessUse() {
    if (this.businessUse == null) {
      this.businessUse = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.BUSINESS_USE);
    }
    return businessUse;
  }

  /**
   * @param businessUse the businessUse to set
   */
  public void setBusinessUse(String businessUse) {
    this.businessUse = businessUse;
  }

  /**
   * @return the businessUse
   */
  public String getFinancier() {
    if (this.financier == null) {
      this.financier = this.getHomeService().retrieveQuoteDetail(this.getQuoteNo(), WebInsQuoteDetail.FINANCIER);
    }
    return financier;
  }

  /**
   * @param financier the financier to set
   */
  public void setFinancier(String financier) {
    this.financier = financier;
  }
}
