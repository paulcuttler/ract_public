package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 *
 * @author gnewton
 */
public class AddTippingForm extends RactActionSupport {

  @Override
  public String execute() throws Exception {

    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }
}
