package futuremedium.client.ract.secure.actions.user;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * View User account details.
 * @author gnewton
 */
public class ViewAccountDetailsForm extends RactActionSupport {

  @Override
  public String execute() throws Exception {

    return SUCCESS;
  }
}
