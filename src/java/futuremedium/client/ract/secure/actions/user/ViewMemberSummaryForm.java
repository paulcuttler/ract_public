package futuremedium.client.ract.secure.actions.user;

import java.util.*;

import com.opensymphony.xwork2.ActionContext;

import com.ract.insurance.*;
import com.ract.web.security.*;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * View Member details such as Roadside and Insurance specifics.
 * @author gnewton
 */
public class ViewMemberSummaryForm extends RactActionSupport {

  private Collection<Policy> policies;

  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    User user = (User) getSession().get(USER_TOKEN);
    if (user instanceof MemberUser) {
      String clientNumber = ((MemberUser) user).getClientNumber();

      // load risks, excluded cancelled and renewal
      Collection<Policy> tmpPolicies = insuranceMgr.getClientPolicies(Integer.parseInt(clientNumber), true, false, false);
      Collection<Policy> finalPolicies = new ArrayList<Policy>();

      if (tmpPolicies != null) {
        for (Policy p : tmpPolicies) {
          if (!p.getPolicyStatus().equals(Policy.STATUS_CANCELLED)) {
            Collection<Risk> riskList = p.getRiskList();
            Collections.sort((List) riskList); //ascending order

            finalPolicies.add(p);
          }
        }
      }

      Collections.sort((List) finalPolicies);
      Collections.reverse((List) finalPolicies); //descending order

      setPolicies(finalPolicies);
    }

    return SUCCESS;
  }

  /**
   * @return the policies
   */
  public Collection<Policy> getPolicies() {
    return policies;
  }

  /**
   * @param policies the policies to set
   */
  public void setPolicies(Collection<Policy> policies) {
    this.policies = policies;
  }
}
