package futuremedium.client.ract.secure.actions.user;

import java.util.regex.Pattern;

import com.opensymphony.xwork2.ActionContext;

import com.ract.common.GenericException;
import com.ract.web.client.ActivationRequest;
import com.ract.web.security.*;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.CreateUpdateActionBase;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Promote a Tipping User to a Member after verifying entered card number.
 * @author gnewton
 */
public class Promote extends CreateUpdateActionBase {

  @Override
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    User user = (User) getSession().get(USER_TOKEN);

    try {

      User tmpUser = userMgr.getUserByCardNumber(getCardNumber(), false);
      if (tmpUser != null) {
        if (tmpUser.getActivationDate() == null) {
          userMgr.disableUserAccount(tmpUser.getUserId()); // disable non-activated account
        } else {
          throw new GenericException("A member account with that card number already exists.");
        }
      }

      // retrieve
      user = userMgr.getUser(user.getUserId());

      // convert to member type
      user.setUserType(User.TYPE_MEMBER);
      user = userMgr.updateUserAccount(user);

      // request activation
      ActivationRequest activationRequest = userMgr.requestMemberAccountActivation(
              user.getUserId(),
              getCardNumber(),
              getLastName(),
              getResidentialPostcode(),
              RactActionSupport.stripNonDigits(getHomePhoneAreaCode() + getHomePhone()),
              RactActionSupport.stripNonDigits(getMobilePhone()),
              getDobDate(),
              null,
              Config.configManager.getProperty("ract.secure.activation.url"));

      if (activationRequest.getMessage() != null) {
        throw new GenericException(activationRequest.getMessage());
      }

      String msg = Config.configManager.getProperty("member.signup.success");
      if (msg.contains("%EMAIL%")) {
        msg = Pattern.compile("%EMAIL%").matcher(msg).replaceAll(user.getEmailAddress());
      }
      if (msg.contains("\n\n")) {
        msg = Pattern.compile("\n\n").matcher(msg).replaceAll("</p><p>");
      }

      this.setActionMessage(msg);
      this.setPageTitle("Member Only Access Registration Successful.");

      return SUCCESS;

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage() + Config.configManager.getProperty("member.error.suffix"));
      this.setPageTitle("Member Only Access Registration Failed.");
      Config.logger.warn(Config.key + ": Unable to request member activation, converting MemberUser back to BasicUser. ", e);

      try {
        user.setUserType(User.TYPE_BASIC);
        userMgr.updateUserAccount(user);
      } catch (GenericException ex) {
        Config.logger.error(Config.key + ": Unable to convert failed MemberUser back to BasicUser: ", ex);
      }

      return ERROR;
    }
  }
}
