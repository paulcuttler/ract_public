package futuremedium.client.ract.secure.actions.user;

import com.ract.web.client.ClientTransaction;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Take a request from Tipping and redirect based on user type.
 * @author gnewton
 */
public class RedirectEmail extends RactActionSupport {

  private String redirectUrl;

  @Override
  public String execute() throws Exception {

    ClientTransaction customer = (ClientTransaction) getSession().get(RactActionSupport.CUSTOMER_TOKEN);

    if (customer == null) { // Tipper
      redirectUrl = getBaseUrl() + "UserChangeDetailForm.action";
    } else { // Member + Tipper
      redirectUrl = getBaseUrl() + "UserChangeEmailForm.action";
    }

    return SUCCESS;
  }

  /**
   * @return the redirectUrl
   */
  public String getRedirectUrl() {
    return redirectUrl;
  }

  /**
   * @param redirectUrl the redirectUrl to set
   */
  public void setRedirectUrl(String redirectUrl) {
    this.redirectUrl = redirectUrl;
  }
}
