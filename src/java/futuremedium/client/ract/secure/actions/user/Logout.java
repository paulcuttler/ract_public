package futuremedium.client.ract.secure.actions.user;

import javax.servlet.http.Cookie;

import com.opensymphony.xwork2.ActionContext;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;

import futuremedium.client.ract.secure.actions.*;
import futuremedium.client.ract.secure.Config;

/**
 *
 * @author gnewton
 */
public class Logout extends RactActionSupport {

  @SuppressWarnings("unchecked")
  @Override
  public String execute() throws Exception {

    this.setActionName(ActionContext.getContext().getName());

    // clear cookie
    Cookie userCookie = new Cookie("secureLoggedIn", "false");
    userCookie.setMaxAge(0);
    if (!useStub) {
      userCookie.setDomain(".ract.com.au");
    }
    userCookie.setPath(Config.configManager.getProperty("ract.secure.urlRewriteFrom"));
    ServletActionContext.getResponse().addCookie(userCookie);

    // invalidate session
    ((SessionMap<String, Object>) this.getSession()).invalidate();

    return SUCCESS;
  }
}
