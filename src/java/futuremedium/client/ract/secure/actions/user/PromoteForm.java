package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;
import com.ract.web.security.User;
import futuremedium.client.ract.secure.Config;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 *
 * @author gnewton
 */
public class PromoteForm extends RactActionSupport {

  private String cardNumber1;
  private String cardNumber2;
  private String lastName;
  private String residentialPostcode;
  private String dob;
  private String mobilePhone;
  private String homePhoneAreaCode;
  private String homePhone;
  private String verifyOption;

  @Override
  public String execute() throws Exception {
    setUp();

    User user = (User) getSession().get(USER_TOKEN);

    this.setCardNumber1("3084");
    this.setCardNumber2("07");
    this.setVerifyOption("dob");

    this.setLastName(user.getSurname());
    this.setResidentialPostcode(user.getResiPostcode());
    this.setMobilePhone(user.getMobilePhone());

    try {
      this.setDob(RactActionSupport.expandedDf.format(user.getBirthDate()));
    } catch (Exception e) {
      Config.logger.warn(Config.key + ": could not parse DOB : ", e);
    }

    String rawHomePhone = user.getHomePhone();
    if (rawHomePhone != null && !rawHomePhone.isEmpty() && rawHomePhone.length() >= 2) {
      this.setHomePhoneAreaCode(rawHomePhone.substring(0, 2));
      if (rawHomePhone.length() == 10) {
        this.setHomePhone(rawHomePhone.substring(2, 10));
      } else {
        this.setHomePhone(rawHomePhone.substring(2));
      }
    }

    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }

  /**
   * @return the cardNumber1
   */
  public String getCardNumber1() {
    return cardNumber1;
  }

  /**
   * @param cardNumber1 the cardNumber1 to set
   */
  public void setCardNumber1(String cardNumber1) {
    this.cardNumber1 = cardNumber1;
  }

  /**
   * @return the cardNumber2
   */
  public String getCardNumber2() {
    return cardNumber2;
  }

  /**
   * @param cardNumber2 the cardNumber2 to set
   */
  public void setCardNumber2(String cardNumber2) {
    this.cardNumber2 = cardNumber2;
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * @return the residentialPostcode
   */
  public String getResidentialPostcode() {
    return residentialPostcode;
  }

  /**
   * @param residentialPostcode the residentialPostcode to set
   */
  public void setResidentialPostcode(String residentialPostcode) {
    this.residentialPostcode = residentialPostcode;
  }

  /**
   * @return the dob
   */
  public String getDob() {
    return dob;
  }

  /**
   * @param dob the dob to set
   */
  public void setDob(String dob) {
    this.dob = dob;
  }

  /**
   * @return the mobilePhone
   */
  public String getMobilePhone() {
    return mobilePhone;
  }

  /**
   * @param mobilePhone the mobilePhone to set
   */
  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }

  /**
   * @return the homePhoneAreaCode
   */
  public String getHomePhoneAreaCode() {
    return homePhoneAreaCode;
  }

  /**
   * @param homePhoneAreaCode the homePhoneAreaCode to set
   */
  public void setHomePhoneAreaCode(String homePhoneAreaCode) {
    this.homePhoneAreaCode = homePhoneAreaCode;
  }

  /**
   * @return the homePhone
   */
  public String getHomePhone() {
    return homePhone;
  }

  /**
   * @param homePhone the homePhone to set
   */
  public void setHomePhone(String homePhone) {
    this.homePhone = homePhone;
  }

  /**
   * @return the verifyOption
   */
  public String getVerifyOption() {
    return verifyOption;
  }

  /**
   * @param verifyOption the verifyOption to set
   */
  public void setVerifyOption(String verifyOption) {
    this.verifyOption = verifyOption;
  }
}
