package futuremedium.client.ract.secure.actions.user;

import futuremedium.client.ract.secure.actions.*;
import com.opensymphony.xwork2.ActionContext;

/**
 * Display confirm update customer form
 * @author gnewton
 */
public class ConfirmUpdateCustomerForm extends RactActionSupport {

  @Override
  public String execute() throws Exception {

    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }

  public boolean isEnterCampaign() {
    return (Boolean) getSession().get("enterCampaign");
  }
}
