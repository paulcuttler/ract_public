package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;

import futuremedium.client.ract.secure.actions.*;

/**
 *
 * @author gnewton
 */
public class Welcome extends RactActionSupport {

  @Override
  public String execute() throws Exception {
    
    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }

}
