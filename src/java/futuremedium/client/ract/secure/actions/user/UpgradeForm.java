package futuremedium.client.ract.secure.actions.user;

import java.util.Collection;

import com.opensymphony.xwork2.ActionContext;

import com.ract.common.GenericException;
import com.ract.membership.ProductVO;
import com.ract.util.DateTime;
import com.ract.web.membership.*;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Upgrade Roadside Membership.
 * @todo not full implemented
 * @author gnewton
 */
public class UpgradeForm extends RactActionSupport {
  @SuppressWarnings("unchecked")
  @Override
  public String execute() throws Exception {
    setUp();

    getSession().put("cardTypeList", getCardTypeList());
    getSession().put("product", ProductVO.PRODUCT_ULTIMATE);

    WebMembershipTransactionContainer ultimate = roadsideMgr.createWebMembershipJoinTransaction(
                new DateTime(),
                new WebMembershipClient(),
                ProductVO.PRODUCT_ULTIMATE,
                "1:0:0:0:0:0:0",
                null,
                true);
    
    getSession().put("container", ultimate);

    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }

  /**
   * @return the cardTypeList
   */
  private Collection<String> getCardTypeList() {
    try {
      return roadsideMgr.getMembershipPaymentTypes();
    } catch (GenericException ex) {
      Config.logger.error(Config.key + ": could not retrieve card type list: ", ex);
      return null;
    }
  }

}
