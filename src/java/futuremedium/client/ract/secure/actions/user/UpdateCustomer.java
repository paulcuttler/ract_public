package futuremedium.client.ract.secure.actions.user;

import java.util.*;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.annotations.*;
import com.ract.client.ClientVO;

import com.ract.common.*;
import com.ract.web.campaign.CampaignEntry;
import com.ract.web.campaign.CampaignMgrRemote;
import com.ract.web.client.ClientTransaction;
import com.ract.web.security.*;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.*;

/**
 *
 * @author gnewton
 */
public class UpdateCustomer extends CreateUpdateActionBase {

  @SuppressWarnings("unchecked")
  @Override
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    try {
      User user = (User) getSession().get(USER_TOKEN);

      if (user instanceof MemberUser) {

        String clientNumber = ((MemberUser) user).getClientNumber();

        ClientTransaction customer = getCustomer(Integer.parseInt(clientNumber));

        customer.setEmailAddress(getEmail());
        customer.setHomePhone(getHomePhoneAreaCode() + getHomePhone());
        customer.setMobilePhone(getMobilePhone());
        customer.setWorkPhone(getWorkPhoneAreaCode() + getWorkPhone());

        customer.setResiPostcode(getResidentialPostcode());
        customer.setResiStreet(getResidentialStreet().toUpperCase());
        customer.setResiSuburb(getResidentialSuburb().toUpperCase());
        customer.setResiProperty(getResidentialPropertyName().toUpperCase());
        customer.setResiStreetChar(getResidentialPropertyQualifier().toUpperCase());
        customer.setResiState(getResidentialState());

        // clear residential streetSuburbId
        customer.setResiStsubid(null);

        StreetSuburbVO ssvo = addressMgr.getUniqueStreetSuburb(getResidentialStreet(), getResidentialSuburb(), getResidentialPostcode());
        if (ssvo != null) {
        	customer.setResiStsubid(ssvo.getStreetSuburbID());
        }

        // populate postal address details from residential address if the same
        if (!isShowPostal()) {
          setPostalPostcode(getResidentialPostcode());
          setPostalStreet(getResidentialStreet());
          setPostalSuburb(getResidentialSuburb());
          setPostalPropertyName(getResidentialPropertyName());
          setPostalPropertyQualifier(getResidentialPropertyQualifier());
          setPostalState(getResidentialState());
        }
        customer.setPostPostcode(getPostalPostcode());
        customer.setPostStreet(getPostalStreet().toUpperCase());
        customer.setPostSuburb(getPostalSuburb().toUpperCase());
        customer.setPostProperty(getPostalPropertyName().toUpperCase());
        customer.setPostStreetChar(getPostalPropertyQualifier().toUpperCase());
        customer.setPostState(getPostalState());

        // clear postal streetSuburbId
        customer.setPostStsubid(null);

        // lookup streetSuburbID and store
        ssvo = addressMgr.getUniqueStreetSuburb(getPostalStreet(), getPostalSuburb(), getPostalPostcode());
        if (ssvo != null) {
        	customer.setPostStsubid(ssvo.getStreetSuburbID());
        }

        // store temp updated customer in session
        getSession().put(CUSTOMER_TMP_TOKEN, customer);

        getSession().put("showPostal", isShowPostal());

        getSession().put("enterCampaign", isUpdateDetails());

        
      }

      this.setPageTitle("Confirm Member Details");

      return SUCCESS;

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred changing client details: ", e);
      return ERROR;

    }
  }
}
