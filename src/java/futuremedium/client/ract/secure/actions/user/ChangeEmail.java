package futuremedium.client.ract.secure.actions.user;

import java.util.*;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.validator.annotations.*;
import com.ract.web.client.ClientTransaction;
import com.ract.client.ClientVO;

import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.web.client.ClientTransactionPK;
import com.ract.web.client.WebClient;
import com.ract.web.security.MemberUser;
import com.ract.web.security.User;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.*;

/**
 * 
 * @author gnewton
 */
public class ChangeEmail extends RactActionSupport {
  
  private String email;
  private String email2;
  private boolean updateDetails;

  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();
    this.setActionName(ActionContext.getContext().getName());

    try {

      User user = (User) getSession().get(USER_TOKEN);

      List<User> usersByEmail = (List<User>) userMgr.getUsersByEmailAddress(getEmail());
      if (usersByEmail != null && !usersByEmail.isEmpty()) {
        throw new GenericException("Unable to update your email address, that email address is not available.");
      }

      // update email address in tipping platform
      if (user.getFootyTipping()) {

        // abort if email is not available at Tipping end
        if (!tippingEmailAvailable(user.getUserName(), getEmail())) {
          throw new GenericException("Unable to update your email address, that email address is not available on the Tipping Platform.");
        }

        // change email on remote server
        String responseString = retrieveHtml(Config.configManager.getProperty("futuremedium.sso.targetUrl"),
                "entity=user&command=changeDetails"
                + "&username=" + user.getUserName()
                + "&email=" + getEmail()
                );

        if (responseString != null && responseString.contains(";")) {
          String msgs[] = responseString.split(";");
          if (!msgs[0].contains("true")) {
            throw new GenericException("Unable update your email address: " + (msgs.length == 2 ? msgs[1] : "Unable to communicate with server."));
          }
        } else {
          throw new GenericException("Unable update your email address: Unable to communicate with server");
        }
      }

      user.setEmailAddress(getEmail());
      user = userMgr.updateUserAccount(user);

      if (isUpdateDetails() && user instanceof MemberUser) {
        // User wants to update his client record with this email address
        String clientNumber = ((MemberUser) user).getClientNumber();

        // retrieve/create client transaction
        ClientTransaction trans = this.getCustomer(Integer.parseInt(clientNumber));

        // set email address
        trans.setEmailAddress(getEmail());

        // create new primary key for transaction
        ClientTransactionPK pk = trans.getClientTransactionPK();
        pk.setTransactionDate(new DateTime());
        pk.setTransactionType(ClientTransaction.TRANSACTION_TYPE_UPDATE);
        trans.setClientTransactionPK(pk);

        // persist transaction
        this.getCustomerMgr().createClientTransaction(trans);
      }

      
      getSession().put(USER_TOKEN, user);

      this.setActionMessage("Your email address has been updated");
      this.setPageTitle("Change Account Email");

      return SUCCESS;
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred changing Member email address: ", e);
      return ERROR;
    }
  }

  @Override
  public void validate() {
    if (!getEmail().equals(getEmail2())) {
      addFieldError("email2", "Email addresses must match");
    }
  }

  /**
   * @return the email
   */
  @RequiredStringValidator(message="Please supply a new email address")
  @EmailValidator(message="Please supply a valid email address")
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the email2
   */
  @RequiredStringValidator(message="Please supply an email address confirmation")
  @EmailValidator(message="Please supply a valid new email confirmation")
  public String getEmail2() {
    return email2;
  }

  /**
   * @param email2 the email2 to set
   */
  public void setEmail2(String email2) {
    this.email2 = email2;
  }

  /**
   * @return the updateDetails
   */
  public boolean isUpdateDetails() {
    return updateDetails;
  }

  /**
   * @param updateDetails the updateDetails to set
   */
  public void setUpdateDetails(boolean updateDetails) {
    this.updateDetails = updateDetails;
  }

}
