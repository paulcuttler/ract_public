package futuremedium.client.ract.secure.actions.user;

import java.util.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.client.ClientVO;
import com.ract.common.*;
import com.ract.membership.MembershipVO;
import com.ract.util.DateTime;
import com.ract.web.client.ClientTransaction;
import com.ract.web.security.*;

import futuremedium.common2.*;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.*;
 
/**
 * Authenticate login request.
 * @author gnewton
 */
public class Auth extends RactActionSupport {

  private String cardNumber1;
  private String cardNumber2;
  private String cardNumber3;
  private String cardNumber4;
  private String username;
  private String password;
  private String initialContext; // string representing user logging in as Member or Tipster
  private String redirectUrl = Config.configManager.getProperty("ract.secure.welcome.redirectAbsUrl");

  final private String MESSAGE_TIPPING_USERNAME_NOT_FOUND = "A tipping account registered with the entered username does not exist.";
  final private String MESSAGE_CARD_NUMBER_NOT_FOUND = "A member access account registered with the entered card number does not exist.";
  final private String MESSAGE_ACCOUNT_NOT_ACTIVATED = "Your account has not yet been activated.";
  final private String MESSAGE_ACCOUNT_DEACTIVATED = "Your account has been deactivated.";
  final private String MESSAGE_TIPPING_PASSWORD_INCORRECT = "Your username or password was incorrect.";
  final private String MESSAGE_MEMBER_PASSWORD_INCORRECT = "Your card number or password was incorrect.";

  @Override
  @SuppressWarnings({"unchecked"})
  public String execute() throws Exception {
    setUp();
    
    this.setActionName(ActionContext.getContext().getName());
    
    return ERROR;
    /*
    User user;
    ClientTransaction customer = null;
    MembershipVO personal = null;

    if (getInitialContext() != null && getInitialContext().equals(RactActionSupport.CONTEXT_TIPPING)) {
      user = userMgr.getUserAccount(getUsername());
      
      if (user == null) {
        this.setActionMessage(MESSAGE_TIPPING_USERNAME_NOT_FOUND);
        this.setRedirectUrl(Config.configManager.getProperty("ract.secure.tipping.redirectAbsUrl"));
        recordLoginAudit(null, getUsername(), null, true, MESSAGE_TIPPING_USERNAME_NOT_FOUND);
        return SUCCESS; // return success and redirect
      }
    } else { // member
      user = userMgr.getUserByCardNumber(getCardNumber(), true);

      if (user == null) {
        this.setActionMessage(MESSAGE_CARD_NUMBER_NOT_FOUND);
        recordLoginAudit(null, null, getCardNumber(), true, MESSAGE_CARD_NUMBER_NOT_FOUND);
        return ERROR;

      } else { // found user
        if (user.getActivationDate() == null) {
          this.setActionMessage(MESSAGE_ACCOUNT_NOT_ACTIVATED);
          recordLoginAudit(user.getUserId(), null, getCardNumber(), true, MESSAGE_ACCOUNT_NOT_ACTIVATED);
          return ERROR;

        } else if (!user.isActive()) {
          this.setActionMessage(MESSAGE_ACCOUNT_DEACTIVATED);
          recordLoginAudit(user.getUserId(), null, getCardNumber(), true, MESSAGE_ACCOUNT_DEACTIVATED);
          return ERROR;
        }
      }
    }

    if (userMgr.passwordsMatch(getPassword(), user.getUserId())) {

      // invalidate and recreate session
      ((SessionMap<String,Object>) this.getSession()).invalidate();
      this.setSession(ActionContext.getContext().getSession());

      // save previous last login date in session
      getSession().put("lastLoggedIn", user.getLastLoggedIn());

      // update last login
      user.setLastLoggedIn(new DateTime());
      user = userMgr.updateUserAccount(user);

      getSession().put(USER_TOKEN, user);

      recordSession(user.getUserId(), getInitialContext());

      try {
        if (user instanceof MemberUser) {

          getSession().put(USER_TOKEN, (MemberUser) user);
          String clientNumber = ((MemberUser) user).getClientNumber();
          int clientNo = -1;

          try {
            clientNo = Integer.parseInt(clientNumber);
          } catch (Exception e) {
            Config.logger.error(Config.key + ": could not parse clientNumber = '" + clientNumber + "', for userId = " + user.getUserId(), e);
          }

          if (clientNo != -1 && user.getActivationDate() != null) {

            // get customer info
            customer = getCustomer(clientNo);
            // get membership info
            personal = roadsideMgr.getPersonalMembership(clientNo);

            checkMembershipExpiry(personal);

            getSession().put(CUSTOMER_TOKEN, customer);
            getSession().put(MEMBER_TOKEN, personal);
          }
        }

        if (isTippingEnabled() && user.getFootyTipping()) { // fire SSO

          String extraParams = "";
          if (user instanceof MemberUser) {
            extraParams = "clientNumber=" + ((MemberUser) user).getClientNumber();
          }

          SsoRequest ssoRequest = SsoSupport.ssoSetup(Config.configManager, user.getUserName());

          if (customer != null) { // members should end up on Member Welcome
            setRedirectUrl(SsoSupport.ssoLoginUrl(Config.configManager, ssoRequest, Config.configManager.getProperty("ract.secure.welcome.redirectUrl"), extraParams));
          } else { // tipsters-only end up on tipping home
            setRedirectUrl(SsoSupport.ssoLoginUrl(Config.configManager, ssoRequest, Config.configManager.getProperty("ract.secure.tipping.redirectUrl"), extraParams));
          }
        }

        // set cookie for public site
        String theUsername;
        if (customer != null) {
          theUsername = customer.getDisplayName() + ":" + ((MemberUser) user).getClientNumber();
        } else {
          theUsername = user.getUserName() + ":";
        }

        Cookie userCookie = new Cookie("secureLoggedIn", theUsername);
        userCookie.setPath(Config.configManager.getProperty("ract.secure.urlRewriteFrom"));
        userCookie.setDomain(".ract.com.au");
        ServletActionContext.getResponse().addCookie(userCookie);

      } catch (GenericException e) {
        this.setActionMessage("An Error occurred : " + e.getMessage());
        recordLoginAudit(user.getUserId(), getUsername(), getCardNumber(), true, "An Error occurred : " + e.getMessage());
        return ERROR;
      }

    } else {
      if (getInitialContext() != null && getInitialContext().equals(RactActionSupport.CONTEXT_TIPPING)) {
        this.setActionMessage(MESSAGE_TIPPING_PASSWORD_INCORRECT);
        this.setRedirectUrl(Config.configManager.getProperty("ract.secure.tipping.redirectAbsUrl"));
        recordLoginAudit(null, getUsername(), null, true, MESSAGE_TIPPING_PASSWORD_INCORRECT + ". Password = " + getPassword());
        return SUCCESS; // return success and redirect

      } else {
        this.setActionMessage(MESSAGE_MEMBER_PASSWORD_INCORRECT);
        recordLoginAudit(null, null, getCardNumber(), true, MESSAGE_MEMBER_PASSWORD_INCORRECT + ". Password = " + getPassword());
      }
      return ERROR;
    }


    this.setActionMessage("Login successful");
    recordLoginAudit(user.getUserId(), getUsername(), getCardNumber(), false, null);
    return SUCCESS;*/
  }

  @Override
  public void validate() {
    if (getInitialContext() != null && getInitialContext().equals(RactActionSupport.CONTEXT_TIPPING)) {
      if (getUsername() == null || getUsername().isEmpty()) {
        this.addFieldError("username", "Please provide a username");
      }
    } else { // member
      if (getCardNumber().length() != 16) {
        addFieldError("cardNumber1", "Please enter a valid card number.");
      } else if (!RactActionSupport.REGEXP_DIGITS.matcher(getCardNumber()).matches()) {
        addFieldError("cardNumber1", "Please enter a valid card number.");
      }
    }
  }

  /**
   * Checks to see if a membership is due to expire soon.
   * @param personal MembershipVO to interrogate.
   */
  @SuppressWarnings("unchecked")
  private void checkMembershipExpiry(MembershipVO personal) {
    if (personal != null) {
      Calendar now = Calendar.getInstance();
      int expiryDays = 14; // default
      try {
        expiryDays = Integer.parseInt(Config.configManager.getProperty("ract.secure.membership.expiryDays", "14"));
      } catch (Exception e) {
        Config.logger.warn(Config.key + ": Could not determine expiryDays from .properties file, defaulting to 14");
      }
      now.add(Calendar.DATE, expiryDays);

      if (personal.getExpiryDate().beforeDay(now.getTime())) {
        getSession().put("membershipExpiring", "true");
      }
    }
  }

  /**
   * Lookup list of appropriate banners for this client number.
   * Store as array in the session.
   * @param clientNumber
   * @deprecated
   */
  @SuppressWarnings("unchecked")
  private void retrieveBannerAds(String clientNumber) {
  	return;
  			/*
    String bannerHtml = RactActionSupport.retrieveHtml(Config.configManager.getProperty("ract.public.banner.service.url"), "cn=" + clientNumber);

    getSession().put(BANNER_TOKEN, bannerHtml);*/
  }

  /**
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * @return the password
   */
  @RequiredStringValidator(message = "Please supply a password")
  public String getPassword() {
    return password;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @return the initialContext
   */
  public String getInitialContext() {
    return initialContext;
  }

  /**
   * @param initialContext the initialContext to set
   */
  public void setInitialContext(String initialContext) {
    this.initialContext = initialContext;
  }

  /**
   * @return the redirectUrl
   */
  public String getRedirectUrl() {
    return redirectUrl;
  }

  /**
   * @param redirectUrl the redirectUrl to set
   */
  public void setRedirectUrl(String redirectUrl) {
    this.redirectUrl = redirectUrl;
  }

  /**
   * @return the cardNumber1
   */
  public String getCardNumber1() {
    return cardNumber1;
  }

  /**
   * @param cardNumber1 the cardNumber1 to set
   */
  public void setCardNumber1(String cardNumber1) {
    this.cardNumber1 = cardNumber1;
  }

  /**
   * @return the cardNumber2
   */
  public String getCardNumber2() {
    return cardNumber2;
  }

  /**
   * @param cardNumber2 the cardNumber2 to set
   */
  public void setCardNumber2(String cardNumber2) {
    this.cardNumber2 = cardNumber2;
  }

  /**
   * @return the cardNumber3
   */
  public String getCardNumber3() {
    return cardNumber3;
  }

  /**
   * @param cardNumber3 the cardNumber3 to set
   */
  public void setCardNumber3(String cardNumber3) {
    this.cardNumber3 = cardNumber3;
  }

  /**
   * @return the cardNumber4
   */
  public String getCardNumber4() {
    return cardNumber4;
  }

  /**
   * @param cardNumber4 the cardNumber4 to set
   */
  public void setCardNumber4(String cardNumber4) {
    this.cardNumber4 = cardNumber4;
  }

  /**
   * Returns an aggregation of 4 card number parts.
   * @return
   */
  public String getCardNumber() {
    StringBuilder number = new StringBuilder("");

    number.append(getCardNumber1() != null ? getCardNumber1() : "");
    number.append(getCardNumber2() != null ? getCardNumber2() : "");
    number.append(getCardNumber3() != null ? getCardNumber3() : "");
    number.append(getCardNumber4() != null ? getCardNumber4() : "");

    return number.toString();
  }

  private void recordLoginAudit(Integer userId, String username, String cardNumber, boolean loginFailure, String message) {

    LoginAudit audit = new LoginAudit();
    audit.setCreateDate(new DateTime());
    audit.setUserId(userId);
    audit.setUserName(username);
    audit.setMemberCardNumber(cardNumber);
    audit.setFailReason(message);
    audit.setLoginFailure(loginFailure);

    try {
      userMgr.createLoginAudit(audit);
    } catch (GenericException e) {
      Config.logger.error(Config.key + ": unable to record loginAudit, ", e);
    }
  }

  /**
   * Populates a User object with test data.
   * @param user
   * @return
   */
  private User populateFakeMember(User user) throws Exception {
    user = new MemberUser();
    ((MemberUser) user).setUserName("gnewton");
    try {
      ((MemberUser) user).setActivationDate(new DateTime("1/11/2009"));
      ((MemberUser) user).setLastLoggedIn(new DateTime());
      ((MemberUser) user).setLastPasswordChange(new DateTime("2/11/2008"));
    } catch (Exception e) {
    }
    ((MemberUser) user).setEmailAddress("test@email.com");
    ((MemberUser) user).setClientNumber("432002");
    ((MemberUser) user).setMemberCardNumber("5123456789012346");

    user.setBirthDate(new DateTime("31/05/1980"));
    user.setSurname("Person");
    user.setHomePhone("0362243535");
    user.setResiPostcode("7008");

    ((MemberUser) user).setFootyTipping(true);

    return user;
  }

  /**
   * Populates a ClientTransaction with test data.
   * @return
   * @throws Exception
   */
  private ClientTransaction populateFakeClient() throws Exception {
    ClientTransaction customer = new ClientTransaction();

    customer.setBirthDate(new DateTime("31/05/1980"));
    customer.setEmailAddress("Garth.Newton@FutureMedium.com.au");
    customer.setTitle("MR");
    customer.setGivenNames("TEST");
    customer.setSurname("PERSON");
    customer.setHomePhone("0362249010");
    customer.setMobilePhone("0404000444");
    customer.setWorkPhone("          "); // test dirty unix data

    customer.setResiProperty("");
    customer.setResiStreetChar("Unit 1, 239");
    customer.setResiStreet("MACQUARIE STREET");
    customer.setResiSuburb("HOBART");
    customer.setResiPostcode("7000");
    customer.setResiState("TAS");

    customer.setPostProperty("PO Box 1000");
    customer.setPostStreetChar("");
    customer.setPostStreet("");
    customer.setPostSuburb("HOBART");
    customer.setPostPostcode("7001");
    customer.setPostState("TAS");

    customer.setGender(ClientVO.GENDER_MALE);

    return customer;
  }

  /**
   * Populates a MembershipVO with test data.
   * @return
   * @throws Exception
   */
  private MembershipVO populateFakeMembership() throws Exception {
    MembershipVO personal = new MembershipVO();
    personal.setProductCode("Advantage");
    personal.setExpiryDate("7/02/2011");
    personal.setJoinDate("13/3/2005");
    personal.setCommenceDate("13/3/2005");
    personal.setClientNumber("432002");
    personal.setStatus("Active");

    return personal;
  }
}
