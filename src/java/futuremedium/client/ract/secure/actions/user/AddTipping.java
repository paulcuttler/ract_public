package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

import com.ract.common.GenericException;
import com.ract.web.client.ClientTransaction;
import com.ract.web.security.User;
import futuremedium.client.ract.secure.Config;

import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.common2.*;

/**
 * Add Tipping to current User's account.
 * @author gnewton
 */
public class AddTipping extends RactActionSupport {

  private String username;
  private String team;
  private String redirectUrl;

  @Override
  public String execute() throws Exception {

    setUp();

    this.setActionName(ActionContext.getContext().getName());

    try {
      User user = (User) getSession().get(USER_TOKEN);
      ClientTransaction customer = (ClientTransaction) getSession().get(CUSTOMER_TOKEN);

      user.setUserName(getUsername());
      user.setBirthDate(customer.getBirthDate());
      user.setSurname(customer.getSurname());
      user.setAflTeam(getTeam());

      createTippingAccount(user);

      // SSO - log user into Tipping and return
      SsoRequest ssoRequest = SsoSupport.ssoSetup(Config.configManager, user.getUserName());
      setRedirectUrl(SsoSupport.ssoLoginUrl(Config.configManager, ssoRequest, getBaseUrl() + "UserPostAddTipping.action", null));

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred Activating Tipping for an existing Member: ", e);
      return ERROR;
    }

    return SUCCESS;
  }

  @Override
  public void validate() {
    try {
      setUp();
      
      if (!useStub && getUsername() != null && !getUsername().isEmpty()) {
	      if (getUsername().length() < 4 || getUsername().length() > 15) {
	        addFieldError("username", "Your username must be between 4 and 15 characters long");
	      } else if (!RactActionSupport.REGEXP_USERNAME.matcher(getUsername()).matches()) {
	        addFieldError("username", "Your username may contain alphanumeric characters but no spaces");
	      } else if (!userMgr.userNameAvailable(getUsername())) {
	        this.addFieldError("username", "That username is not available, please choose another");
	      }
      }
      
    } catch (GenericException e) { // could not access users
      this.addFieldError("username", "That username is not available, please choose another");
    } catch (Exception e) { // could not setup comms
      this.addFieldError("username", "A communication error has occurred");
    }
  }

  /**
   * @return the username
   */
  @RequiredStringValidator(message = "Please supply a username")
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * @return the team
   */
  public String getTeam() {
    return team;
  }

  /**
   * @param team the team to set
   */
  public void setTeam(String team) {
    this.team = team;
  }

  /**
   * @return the redirectUrl
   */
  public String getRedirectUrl() {
    return redirectUrl;
  }

  /**
   * @param redirectUrl the redirectUrl to set
   */
  public void setRedirectUrl(String redirectUrl) {
    this.redirectUrl = redirectUrl;
  }
}
