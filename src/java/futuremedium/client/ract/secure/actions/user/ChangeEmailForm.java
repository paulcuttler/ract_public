package futuremedium.client.ract.secure.actions.user;

import futuremedium.client.ract.secure.actions.*;
import com.opensymphony.xwork2.ActionContext;

/**
 * Update User email form.
 * @author gnewton
 */
public class ChangeEmailForm extends RactActionSupport {

  @Override
  public String execute() throws Exception {

    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }
}