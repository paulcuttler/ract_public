package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;

import com.ract.common.*;
import com.ract.web.security.*;

import futuremedium.client.ract.secure.actions.*;

/**
 * 
 * @author gnewton
 */
public class ManageSubscriptions extends RactActionSupport {

  private boolean footyTipping;

  @Override
  @SuppressWarnings({"unchecked"})
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());
    String extraMsg = "";

    try {
      User user = (User) getSession().get(USER_TOKEN);

      if (isFootyTipping()) {

        // @todo re-enable tipping account?
        // abort if username or email are not available at Tipping end
        tippingUsernameEmailAvailable(user.getUserName(), user.getEmailAddress());

        // remotely create account in tipping platform
        extraMsg += createTippingAccount(user);

        // update local copy of user
        getSession().put(USER_TOKEN, user);

      } else {
        // disable tipping account in tipping platform
        extraMsg += disableTippingAccount(user);

        // update local copy of user
        getSession().put(USER_TOKEN, user);
      }

      this.setActionMessage("Your subscriptions have been updated." + extraMsg);
      this.setPageTitle("Manage User Account Options");
      
      return SUCCESS;

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      return ERROR;
    }

  }

  /**
   * @return the footyTipping
   */
  public boolean isFootyTipping() {
    return footyTipping;
  }

  /**
   * @param footyTipping the footyTipping to set
   */
  public void setFootyTipping(boolean footyTipping) {
    this.footyTipping = footyTipping;
  }
}
