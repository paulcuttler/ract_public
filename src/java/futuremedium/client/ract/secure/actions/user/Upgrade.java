package futuremedium.client.ract.secure.actions.user;

import java.util.*;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.common.GenericException;
import com.ract.web.membership.*;
import com.ract.web.payment.WebMembershipPayment;
import com.ract.web.payment.WebPayment;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 *
 * @author gnewton
 */
public class Upgrade extends RactActionSupport {

  // payment details
  private String cardName;
  private String cardNumber1;
  private String cardNumber2;
  private String cardNumber3;
  private String cardNumber4;
  private String cardType;
  private String expiryDate1;
  private String expiryDate2;
  private String pdfUrl;

  @Override
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    try {      
      WebMembershipTransactionContainer container = (WebMembershipTransactionContainer) getSession().get("container");

      // attempt to store payment info
//      WebPayment payment = new WebMembershipPayment(
//              container.getWebMembershipTransactionHeader().getTransactionHeaderId().toString(),
//              getCardName(),
//              getCardType(),
//              getCardNumber(),
//              null,
//              getExpiryDate(), "CreditCard");

      // email membership advice
      WebMembershipClient customer = (WebMembershipClient) getSession().get(CUSTOMER_TOKEN);
      roadsideMgr.emailJoinMembershipAdvice(container.getWebMembershipTransactionHeader().getTransactionHeaderId(), customer.getEmailAddress());

      return SUCCESS;

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      return ERROR;

    }
  }

  @Override
  public void validate() {
    if (getCardNumber1().length() != 4 || getCardNumber2().length() != 4 || getCardNumber3().length() != 4 || getCardNumber4().length() != 4) {
      addFieldError("cardNumber1", "Please enter a valid credit card number.");
    } else if (!RactActionSupport.REGEXP_DIGITS.matcher(getCardNumber()).matches()) {
      addFieldError("cardNumber1", "Please enter a valid credit card number.");
    } else if (!validateCreditCardNumber(getCardNumber())) {
      addFieldError("cardNumber1", "Please enter a valid credit card number.");
    }

    if (!RactActionSupport.REGEXP_DIGITS.matcher(getExpiryDate1()).matches()
            || !RactActionSupport.REGEXP_DIGITS.matcher(getExpiryDate2()).matches()) {
      addFieldError("expiryDate1", "Please enter a valid expiry date.");
    } else if (!getExpiryDate1().isEmpty() && !getExpiryDate2().isEmpty()) {
      Calendar expiryDate = Calendar.getInstance();
      expiryDate.set(Calendar.YEAR, 2000 + Integer.parseInt(getExpiryDate2()));
      expiryDate.set(Calendar.MONTH, Integer.parseInt(getExpiryDate1()) - 1); // subtract 1 from month value
      expiryDate.set(Calendar.DATE, expiryDate.getMaximum(Calendar.DAY_OF_MONTH));

      //Config.logger.debug(Config.key + ": Expiry date = " + expiryDate.getTime());

      if (expiryDate.getTime().before(new Date())) { // expiry date in the past
        addFieldError("expiryDate1", "Please enter a current expiry date.");
      }
    }

    // fall through to annotation validation
  }

  /**
   * @return the cardName
   */
  @RequiredStringValidator(message = "Please supply a Card Name")
  public String getCardName() {
    return cardName;
  }

  /**
   * @param cardName the cardName to set
   */
  public void setCardName(String cardName) {
    this.cardName = cardName;
  }

  /**
   * @return the cardNumber1
   */
  public String getCardNumber1() {
    return cardNumber1;
  }

  /**
   * @param cardNumber1 the cardNumber1 to set
   */
  public void setCardNumber1(String cardNumber1) {
    this.cardNumber1 = cardNumber1;
  }

  /**
   * @return the cardNumber2
   */
  public String getCardNumber2() {
    return cardNumber2;
  }

  /**
   * @param cardNumber2 the cardNumber2 to set
   */
  public void setCardNumber2(String cardNumber2) {
    this.cardNumber2 = cardNumber2;
  }

  /**
   * @return the cardNumber3
   */
  public String getCardNumber3() {
    return cardNumber3;
  }

  /**
   * @param cardNumber3 the cardNumber3 to set
   */
  public void setCardNumber3(String cardNumber3) {
    this.cardNumber3 = cardNumber3;
  }

  /**
   * @return the cardNumber4
   */
  public String getCardNumber4() {
    return cardNumber4;
  }

  /**
   * @param cardNumber4 the cardNumber4 to set
   */
  public void setCardNumber4(String cardNumber4) {
    this.cardNumber4 = cardNumber4;
  }

  /**
   * Returns an aggregation of 4 card number parts.
   * @return
   */
  public String getCardNumber() {
    return getCardNumber1() + getCardNumber2() + getCardNumber3() + getCardNumber4();
  }

  /**
   * @return the expiryDate1
   */
  @RequiredStringValidator(message = "Please supply a 2 digit Expiry Date month")
  @StringLengthFieldValidator(message = "Please supply a 2 digit Expiry Date month", minLength = "2", maxLength = "2")
  public String getExpiryDate1() {
    return expiryDate1;
  }

  /**
   * @param expiryDate the expiryDate1 to set
   */
  public void setExpiryDate1(String expiryDate) {
    this.expiryDate1 = expiryDate;
  }

  /**
   * @return the expiryDate2
   */
  @RequiredStringValidator(message = "Please supply a 2 digit Expiry Date year")
  @StringLengthFieldValidator(message = "Please supply a 2 digit Expiry Date year", minLength = "2", maxLength = "2")
  public String getExpiryDate2() {
    return expiryDate2;
  }

  /**
   * @param expiryDate the expiryDate1 to set
   */
  public void setExpiryDate2(String expiryDate) {
    this.expiryDate2 = expiryDate;
  }

  public String getExpiryDate() {
    return getExpiryDate1() + getExpiryDate2();
  }

  /**
   * @return the cardType
   */
  @RequiredStringValidator(message = "Please supply a Card Type")
  public String getCardType() {
    return cardType;
  }

  /**
   * @param cardType the cardType to set
   */
  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  /**
   * @return the pdfUrl
   */
  public String getPdfUrl() {
    return pdfUrl;
  }

  /**
   * @param pdfUrl the pdfUrl to set
   */
  public void setPdfUrl(String pdfUrl) {
    this.pdfUrl = pdfUrl;
  }
}
