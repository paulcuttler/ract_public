package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;

import futuremedium.client.ract.secure.actions.*;

/**
 * Display reset password form.
 * @author gnewton
 */
public class ResetPasswordForm extends RactActionSupport {

  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {

    this.setActionName(ActionContext.getContext().getName());
    
    return SUCCESS;
  }
}
