package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;

import com.ract.web.security.*;

import futuremedium.client.ract.secure.actions.*;

/**
 * Display manage subscriptions form.
 * @author gnewton
 */
public class ManageSubscriptionsForm extends RactActionSupport {

  private boolean eNews = false;

  @Override
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    User user = (User) getSession().get(USER_TOKEN);
    if (user instanceof MemberUser) {
      String clientNumber = ((MemberUser) user).getClientNumber();
      setENews(customerMgr.hasEmailNewsletterSubscription(Integer.parseInt(clientNumber)));
    }

    return SUCCESS;
  }

  /**
   * @return the eNews
   */
  public boolean isENews() {
    return eNews;
  }

  /**
   * @param eNews the eNews to set
   */
  public void setENews(boolean eNews) {
    this.eNews = eNews;
  }
}
