package futuremedium.client.ract.secure.actions.user;

import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.web.campaign.CampaignEntry;
import com.ract.web.campaign.CampaignMgrRemote;
import com.ract.web.client.ClientTransaction;
import com.ract.web.client.ClientTransactionPK;
import com.ract.web.security.*;
import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Step 2/2 of the Update Customer Details process.
 * @author gnewton
 */
public class ConfirmUpdateCustomer extends RactActionSupport {

  @SuppressWarnings("unchecked")
  @Override
  public String execute() throws Exception {
    setUp();

    try {

      // get temp customer details
      ClientTransaction customer = (ClientTransaction) getSession().get(CUSTOMER_TMP_TOKEN);

      if (!useStub) {
        // update primary key with trans type
        ClientTransactionPK pk = customer.getClientTransactionPK();
        pk.setTransactionDate(new DateTime());
        pk.setTransactionType(ClientTransaction.TRANSACTION_TYPE_UPDATE);
        customer.setClientTransactionPK(pk);

        customerMgr.createClientTransaction(customer);
      }

      boolean updateDetails = (Boolean) getSession().get("enterCampaign");
      if (updateDetails) {
        // Enter the user in campaign if he checked the checkbox
        User user = (User) getSession().get(USER_TOKEN);

        String clientNumber = ((MemberUser) user).getClientNumber();
        // Record campaign entry
        CampaignEntry entry = new CampaignEntry(this.getMembershipCompetition(), Integer.parseInt(clientNumber));

        try {
          CampaignMgrRemote campaignMgr = (CampaignMgrRemote) this.getContext().lookup("CampaignMgr/remote");
          campaignMgr.createEntry(entry);
        }
        catch (Exception ex) {
          Config.logger.error(Config.key + ": can not create campaignMgr: ", ex);
        }
      }

      // remove temp customer details
      getSession().put(CUSTOMER_TMP_TOKEN, null);
      getSession().remove(CUSTOMER_TMP_TOKEN);

      // update customer in session
      getSession().put(CUSTOMER_TOKEN, customer);

      return SUCCESS;

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred confirming Client details: ", e);
      return ERROR;

    }
  }
}
