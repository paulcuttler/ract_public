package futuremedium.client.ract.secure.actions.user;

import java.util.*;
import java.util.regex.Pattern;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.common.GenericException;
import com.ract.web.client.ActivationRequest;
import com.ract.web.security.*;
import futuremedium.client.ract.secure.Config;

import futuremedium.client.ract.secure.actions.*;

/**
 * Create web user and tipping user.
 * @author gnewton
 */
public class TippingCreate extends CreateUpdateActionBase {

  @Override
  public String execute() throws Exception {

    setUp();

    this.setActionName(ActionContext.getContext().getName());
    String extraMsg = "";
    User u = null;

    try {
      if (!getCardNumber().isEmpty()) {
        // check user doesn't already have account
        u = userMgr.getUserByCardNumber(getCardNumber(), false);
        if (u != null) {
          if (u.getActivationDate() == null) {
            userMgr.disableUserAccount(u.getUserId()); // disable non-activated account
          } else {
            throw new GenericException("You already have a member account. You can activate tipping after logging in.");
          }
        }
      }

      // check for user by email
      boolean haveMergedUser = false;
      @SuppressWarnings("unchecked")
      List<User> usersByEmail = (List<User>) userMgr.getUsersByEmailAddress(getEmail());
      Collections.reverse(usersByEmail); // most recent first

      Config.logger.debug(Config.key + ": looking for users by email = " + getEmail());

      if (usersByEmail != null && !usersByEmail.isEmpty()) {
        for (User tmpUser : usersByEmail) {
          Config.logger.debug(Config.key + ": found user by email, type = " + (tmpUser instanceof MemberUser ? "Member" : "Basic") + ", activated = " + tmpUser.getActivationDate());
          if (tmpUser instanceof MemberUser && tmpUser.isActive() && tmpUser.getActivationDate() != null && !haveMergedUser) {
            // found member account, insert tipping details and update password
            tmpUser.setUserName(getUsername());
            tmpUser.setPassword(SecurityHelper.encryptPassword(getPassword()));
            tmpUser.setAflTeam(getTeam());
            tmpUser.setFootyTipping(true);
            userMgr.updateUserAccount(tmpUser);
            haveMergedUser = true;
          } else {
            userMgr.disableUserAccount(tmpUser.getUserId());
          }
        }
      } else {
        Config.logger.debug(Config.key + ": found 0 users by email");
      }

      // skip this if we have just merged an existing Member account
      if (!haveMergedUser) {
        if (getCardNumber().isEmpty()) {
          u = new BasicUser();
        } else {
          u = new MemberUser();
          ((MemberUser) u).setMemberCardNumber(getCardNumber());
        }
        u.setAflTeam(getTeam());
        u.setUserName(getUsername());
        u.setBirthDate(getDobDate());
        u.setEmailAddress(getEmail());
        u.setFootyTipping(false); // set tipping to false until we have successfully communicated with server
        u.setGender(getGender());
        u.setGivenNames(getFirstName());
        u.setHomePhone(getHomePhoneAreaCode() + getHomePhone());
        u.setMobilePhone(getMobilePhone());
        u.setPassword(SecurityHelper.encryptPassword(getPassword()));
        u.setResiPostcode(getResidentialPostcode());
        u.setResiProperty(getResidentialPropertyName());
        u.setResiState(getResidentialState());
        u.setResiStreet(getResidentialStreet());
        u.setResiStreetChar(getResidentialPropertyQualifier());
        u.setResiSuburb(getResidentialSuburb());
        u.setSurname(getLastName());
        u.setTitle(getTitle());
        u.setWorkPhone(getWorkPhoneAreaCode() + getWorkPhone());

        u = userMgr.createUserAccount(u);

        // request member activation if card number supplied
        if (!getCardNumber().isEmpty()) {
          try {
            String phoneNumberToSend = "";
            if (!u.getHomePhone().isEmpty()) {
              phoneNumberToSend = u.getHomePhone();
            } else if (!u.getWorkPhone().isEmpty()) {
              phoneNumberToSend = u.getWorkPhone();
            }
            ActivationRequest activationRequest = userMgr.requestMemberAccountActivation(
                    u.getUserId(),
                    getCardNumber(),
                    u.getSurname(),
                    u.getResiPostcode(),
                    RactActionSupport.stripNonDigits(phoneNumberToSend),
                    RactActionSupport.stripNonDigits(getMobilePhone()),
                    u.getBirthDate(),
                    null,
                    Config.configManager.getProperty("ract.secure.activation.url"));

            if (activationRequest.getMessage() != null) {
              throw new GenericException(activationRequest.getMessage());
            }

            String msg = Config.configManager.getProperty("member.signup.success");
            if (msg.contains("%EMAIL%")) {
              msg = Pattern.compile("%EMAIL%").matcher(msg).replaceAll(getEmail());
            }
            if (msg.contains("\n\n")) {
              msg = Pattern.compile("\n\n").matcher(msg).replaceAll("</p><p>");
            }
            extraMsg = "</p><p>" + msg;

          } catch (GenericException e) {
            // catch exception here and add message
            extraMsg = "</p><p class='sys-msg msg-error'>Note: " + e.getMessage() + Config.configManager.getProperty("member.error.suffix");
            Config.logger.warn(Config.key + ": Unable to request member activation, converting MemberUser back to BasicUser: ", e);

            try {
              u.setUserType(User.TYPE_BASIC);
              ((MemberUser) u).setMemberCardNumber(null);
              userMgr.updateUserAccount(u);
            } catch (GenericException ex) {
              Config.logger.error(Config.key + ": Unable to convert failed MemberUser back to BasicUser: ", ex);
            }
          }
        }

        // retrieve user from DB after activation request sent
        u = userMgr.getUser(u.getUserId());
      } else {
        extraMsg = "Note: Your tipping details were merged with an existing Member Account.";
      }

      // remotely create account in tipping platform, then update account
      createTippingAccount(u);

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + extraMsg + e.getMessage() + Config.configManager.getProperty("member.error.suffix"));
      Config.logger.error(Config.key + ": An error occurred creating a Tipping User: ", e);
      return ERROR;
    }

    this.setActionMessage("Your tipping account has been created. You will receive a confirmation email shortly." + extraMsg);
    this.setPageTitle("Signup for Footy Tipping Account");

    return SUCCESS;
  }

  @Override 
  public void validate() {
    super.validate();

    try {
      setUp();
      if (!useStub && !userMgr.userNameAvailable(getUsername())) {
        this.addFieldError("username", "That username is not available, please choose another");
      }
    } catch (GenericException e) { // could not access users
      this.addFieldError("username", "That username is not available, please choose another");
    } catch (Exception e) { // could not setup comms
      this.addFieldError("username", "A communication error has occurred");
      Config.logger.error(Config.key + ": A communication error has occurred checking username: ", e);
    }

    // check email on tipping
    if (!tippingEmailAvailable(null, getEmail())) {
      this.addFieldError("email", "That email address is not available, please choose another");
    }

  }
}
