package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.common.GenericException;
import com.ract.web.security.User;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.*;

/**
 * Update user's password.
 * @author gnewton
 */
public class ChangePassword extends RactActionSupport {

  private String currentPassword;
  private String password;
  private String password2;

  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();
    this.setActionName(ActionContext.getContext().getName());

    try {
      User user = (User) getSession().get(USER_TOKEN);

      if (!userMgr.passwordsMatch(getCurrentPassword(), user.getUserId())) {
        throw new GenericException("Incorrect password supplied");
      }

      user = userMgr.changeUserPassword(user, getPassword());
      getSession().put(USER_TOKEN, user);

      this.setActionMessage("Your password has been updated");
      this.setPageTitle("Change Account Password");

      return SUCCESS;
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred changing User password: ", e);
      return ERROR;
    }
  }

  @Override
  public void validate() {
    if (!getPassword().equals(getPassword2())) {
      addFieldError("password2", "Passwords must match");
    }
  }

  /**
   * @return the password
   */
  @RequiredStringValidator(message="Please supply new password")
  @RegexFieldValidator(message = "Your password may contain alphanumeric characters but no spaces", regexExpression = "^[A-Za-z0-9_]*$")
  @StringLengthFieldValidator(message = "Your password must be at least 6 characters long", minLength = "6")
  public String getPassword() {
    return password;
  }

  /**
   * @return the password2
   */
  @RequiredStringValidator(message="Please supply new password confirmation")
  public String getPassword2() {
    return password2;
  }

   /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @param password2 the password2 to set
   */
  public void setPassword2(String password2) {
    this.password2 = password2;
  }

  /**
   * @return the currentPassword
   */
  @RequiredStringValidator(message="Please supply your current password")
  public String getCurrentPassword() {
    return currentPassword;
  }

  /**
   * @param currentPassword the currentPassword to set
   */
  public void setCurrentPassword(String currentPassword) {
    this.currentPassword = currentPassword;
  }

}
