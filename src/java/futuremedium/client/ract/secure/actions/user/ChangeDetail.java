package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.common.GenericException;
import com.ract.web.security.User;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.*;

/**
 * Update User details.
 * @author gnewton
 */
public class ChangeDetail extends CreateUpdateActionBase {

  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();
    this.setActionName(ActionContext.getContext().getName());

    try {

      User user = (User) getSession().get(USER_TOKEN);

      // update details in tipping platform

      // abort if email is not available at Tipping end
      if (!tippingEmailAvailable(user.getUserName(), getEmail())) {
        throw new GenericException("Unable update your details, that email address is not available on the Tipping Platform.");
      }

      // change email on remote server
      String responseString = retrieveHtml(Config.configManager.getProperty("futuremedium.sso.targetUrl"),
              "entity=user&command=changeDetails"
              + "&username=" + user.getUserName()
              + "&email=" + getEmail()
              + "&dob=" + getDob()
              + "&team=" + getTeam()
              + "&surname=" + getLastName());

      if (responseString != null && responseString.contains(";")) {
        String msgs[] = responseString.split(";");
        if (!msgs[0].contains("true")) {
          throw new GenericException("Unable update your details: " + (msgs.length == 2 ? msgs[1] : "Unable to communicate with server."));
        }
      } else {
        throw new GenericException("Unable update your details: Unable to communicate with server");
      }

      user.setAflTeam(getTeam());
      user.setBirthDate(getDobDate());
      user.setEmailAddress(getEmail());
      user.setGender(getGender());
      user.setGivenNames(getFirstName());
      user.setHomePhone(getHomePhoneAreaCode() + getHomePhone());
      user.setMobilePhone(getMobilePhone());
      user.setResiPostcode(getResidentialPostcode());
      user.setResiProperty(getResidentialPropertyName());
      user.setResiState(getResidentialState());
      user.setResiStreet(getResidentialStreet());
      user.setResiStreetChar(getResidentialPropertyQualifier());
      user.setResiSuburb(getResidentialSuburb());
      user.setSurname(getLastName());
      user.setTitle(getTitle());
      user.setWorkPhone(getWorkPhoneAreaCode() + getWorkPhone());

      user = userMgr.updateUserAccount(user);

      getSession().put(USER_TOKEN, user);

      this.setActionMessage("Your details have been updated");
      this.setPageTitle("Change Account Details");

      return SUCCESS;
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred changing User details: ", e);
      return ERROR;
    }
  }
}
