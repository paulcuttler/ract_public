package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;

import com.ract.common.GenericException;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Display tipping signup form.
 * @author gnewton
 */
public class TippingSignup extends RactActionSupport {

  private boolean showCardNumber = false;

  @Override
  public String execute() throws Exception {
    setUp();

    prepareTitles();

    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }

  @SuppressWarnings("unchecked")
  private void prepareTitles() throws GenericException {
    getSession().put("titles", customerMgr.getClientTitles(true,false));
  }

  /**
   * @return the showCardNumber
   */
  public boolean isShowCardNumber() {
    return showCardNumber;
  }

  /**
   * @param showCardNumber the showCardNumber to set
   */
  public void setShowCardNumber(boolean showCardNumber) {
    this.showCardNumber = showCardNumber;
  }

}