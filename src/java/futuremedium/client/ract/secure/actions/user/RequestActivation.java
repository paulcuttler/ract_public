package futuremedium.client.ract.secure.actions.user;

import java.util.*;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.common.GenericException;
import com.ract.membership.MembershipVO;
import com.ract.util.DateTime;
import com.ract.web.client.ActivationRequest;
import com.ract.web.client.ClientTransaction;
import com.ract.web.security.MemberUser;
import com.ract.web.security.User;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.*;

/**
 * Validates membership activation request.
 * @author gnewton
 */
public class RequestActivation extends RactActionSupport {

  private String cardNumber1;
  private String cardNumber2;
  private String cardNumber3;
  private String cardNumber4;
  private String lastName;
  private String dob;
  private String phone;
  private String mobilePhone;
  private String postcode;
  private String email;
  DateTime newDob;

  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    try {
      User user = (User) getSession().get(USER_TOKEN);

      // activate account
      ActivationRequest activationRequest = userMgr.requestMemberAccountActivation(
              user.getUserId(),
              getCardNumber(),
              getLastName(),
              getPostcode(),
              RactActionSupport.stripNonDigits(getPhone()),
              RactActionSupport.stripNonDigits(getMobilePhone()),
              newDob,
              null,
              Config.configManager.getProperty("ract.secure.activation.url"));

      if (activationRequest.getMessage() != null) {
        throw new GenericException(activationRequest.getMessage());
      }
      
      user = userMgr.getUserAccount(user.getUserName());
      if (user instanceof MemberUser) {
        // update user
        getSession().put(USER_TOKEN, (MemberUser) user);
        String clientNumber = ((MemberUser) user).getClientNumber();

        // get customer info
        ClientTransaction customer = getCustomer(Integer.parseInt(clientNumber));
        getSession().put(CUSTOMER_TOKEN, customer);

        // get membership info
        MembershipVO personal = roadsideMgr.getPersonalMembership(Integer.parseInt(clientNumber));
        getSession().put(MEMBER_TOKEN, personal);
      }

      this.setActionMessage("You will receive an confirmation email at the address provided.");
      this.setPageTitle("Activation Request");

      return SUCCESS;

    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage() + Config.configManager.getProperty("member.error.suffix"));
      return ERROR;
    }
  }


  @Override
  public void validate() {
    if (getCardNumber1().length() != 4 || getCardNumber2().length() != 4 || getCardNumber3().length() != 4 || getCardNumber4().length() != 4) {
      addFieldError("cardNumber1", "Please enter a valid card number.");
    } else if (!RactActionSupport.REGEXP_DIGITS.matcher(getCardNumber()).matches()) {
      addFieldError("cardNumber1", "Please enter a valid card number.");
    }

    try {
      newDob = new DateTime(getDob());
      Calendar testDate = Calendar.getInstance();

      if (newDob.after(testDate.getTime())) { // dob is in the future
        throw new Exception("Date of birth cannot be in the future");
      }

      testDate.add(Calendar.YEAR, -99);
      if (newDob.before(testDate.getTime())) { // dob is more than 99 years ago
        throw new Exception("Date of birth cannot be more than 99 years ago");
      }

    } catch (Exception e) {
      addFieldError("dob", "Please provide a valid date of birth.");
    }

    // fall through to annotation validation
  }

  /**
   * @return the cardNumber1
   */
  public String getCardNumber1() {
    return cardNumber1;
  }

  /**
   * @param cardNumber1 the cardNumber1 to set
   */
  public void setCardNumber1(String cardNumber1) {
    this.cardNumber1 = cardNumber1;
  }

  /**
   * @return the cardNumber2
   */
  public String getCardNumber2() {
    return cardNumber2;
  }

  /**
   * @param cardNumber2 the cardNumber2 to set
   */
  public void setCardNumber2(String cardNumber2) {
    this.cardNumber2 = cardNumber2;
  }

  /**
   * @return the cardNumber3
   */
  public String getCardNumber3() {
    return cardNumber3;
  }

  /**
   * @param cardNumber3 the cardNumber3 to set
   */
  public void setCardNumber3(String cardNumber3) {
    this.cardNumber3 = cardNumber3;
  }

  /**
   * @return the cardNumber4
   */
  public String getCardNumber4() {
    return cardNumber4;
  }

  /**
   * @param cardNumber4 the cardNumber4 to set
   */
  public void setCardNumber4(String cardNumber4) {
    this.cardNumber4 = cardNumber4;
  }

  /**
   * Returns an aggregation of 4 card number parts.
   * @return
   */
  public String getCardNumber() {
    return getCardNumber1() + getCardNumber2() + getCardNumber3() + getCardNumber4();
  }

  /**
   * @return the lastName
   */
  @RequiredStringValidator(message="Please supply a last name")
  public String getLastName() {
    return lastName;
  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * @return the dob
   */
  @RequiredStringValidator(message="Please supply a date of birth")
  public String getDob() {
    return dob;
  }

  /**
   * @param dob the dob to set
   */
  public void setDob(String dob) {
    this.dob = dob;
  }

  /**
   * @return the phone
   */
  @RequiredStringValidator(message="Please supply a phone number")
  @RegexFieldValidator(message = "Phone number must be ten digits long starting with zero", regexExpression="0\\d\\d\\d\\d\\d\\d\\d\\d\\d", fieldName="phone")
  public String getPhone() {
    return phone;
  }

  /**
   * @param phone the phone to set
   */
  public void setPhone(String phone) {
    this.phone = phone;
  }

  /**
   * @return the postcode
   */
  @RequiredStringValidator(message="Please supply a postcode")
  @StringLengthFieldValidator(message = "Postcode must be 4 digits long", minLength="4", maxLength="4")
  @RegexFieldValidator(message = "Postcode must be 4 digits long", regexExpression="\\d\\d\\d\\d", fieldName="postcode")
  public String getPostcode() {
    return postcode;
  }

  /**
   * @param postcode the postcode to set
   */
  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the mobilePhone
   */
  public String getMobilePhone() {
    return mobilePhone;
  }

  /**
   * @param mobilePhone the mobilePhone to set
   */
  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }
}
