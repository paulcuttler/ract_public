package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;

import futuremedium.client.ract.secure.actions.*;

/**
 * Display login form.
 * @author gnewton
 */
public class Login extends RactActionSupport {
  private String cardNumber1;
  private String cardNumber2;

  @Override
  public String execute() throws Exception {

    this.setActionName(ActionContext.getContext().getName());

    this.setCardNumber1("3084");
    this.setCardNumber2("07");

    return SUCCESS;
  }

  /**
   * @return the cardNumber1
   */
  public String getCardNumber1() {
    return cardNumber1;
  }

  /**
   * @param cardNumber1 the cardNumber1 to set
   */
  public void setCardNumber1(String cardNumber1) {
    this.cardNumber1 = cardNumber1;
  }

  /**
   * @return the cardNumber2
   */
  public String getCardNumber2() {
    return cardNumber2;
  }

  /**
   * @param cardNumber2 the cardNumber2 to set
   */
  public void setCardNumber2(String cardNumber2) {
    this.cardNumber2 = cardNumber2;
  }
}
