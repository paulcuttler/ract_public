/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package futuremedium.client.ract.secure.actions.user;

import futuremedium.client.ract.secure.actions.*;
import com.opensymphony.xwork2.ActionContext;

/**
 * Display update password form
 * @author gnewton
 */
public class ChangePasswordForm extends RactActionSupport {

  @Override
  public String execute() throws Exception {

    this.setActionName(ActionContext.getContext().getName());

    return SUCCESS;
  }
}
