package futuremedium.client.ract.secure.actions.user;

import java.util.*;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.common.GenericException;

import com.ract.web.security.User;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.*;

/**
 * Resets the user's password.
 * @author gnewton
 */
public class ResetPassword extends RactActionSupport {

  private String username;
  private String cardNumber1;
  private String cardNumber2;
  private String cardNumber3;
  private String cardNumber4;
  private String email;
  private String gotcha;
  private final int PASSWORD_LENGTH = Integer.parseInt(Config.configManager.getProperty("ract.secure.password.length", "8"));

  @Override
  public String execute() throws Exception {
    
    setUp();
    this.setActionName(ActionContext.getContext().getName());

    try {
      User user = null;
      
      if (user == null && !getCardNumber().isEmpty()) {
        user = userMgr.getUserByCardNumber(getCardNumber());
      }

      if (user == null && !getUsername().isEmpty()) {
        user = userMgr.getUserAccount(getUsername());
      }
      
      if (user == null && !getEmail().isEmpty()) {
        @SuppressWarnings("unchecked")
        ArrayList<User> returnedUsers = (ArrayList<User>) userMgr.getUsersByEmailAddress(getEmail());
        if (returnedUsers.size() > 1) {
          throw new GenericException("This email address has been used for multiple membership accounts. You will need to enter your membership card number or Footy Tipping username to indicate which account you would like to reset the password for");
        } else if (returnedUsers.size() == 1) {
          user = returnedUsers.get(0);
        }
      }

      if (user == null) {
        throw new GenericException("Unable to find a user by card number, email address or Footy Tipping username.");
      }

      if (user.getActivationDate() == null) {
      	throw new GenericException("Your account has not yet been activated. Please click the link in your activation email.");
      }
      
      userMgr.resetUserPassword(user.getUserId(), PASSWORD_LENGTH, true);

      this.setActionMessage("A temporary password has been sent to your email address.");
      this.setPageTitle("Reset Password");

      return SUCCESS;
      
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred resetting password: ", e);
      return ERROR;
    }
  }

  @Override
  public void validate() {
    if (getEmail().isEmpty() && getCardNumber().isEmpty() && getUsername().isEmpty()) {
      this.addFieldError("cardNumber1", "Please provide either a card number, account email address or Footy Tipping username");
    } else if (!getCardNumber().isEmpty()) {
      if (getCardNumber1().length() != 4 || getCardNumber2().length() != 4 || getCardNumber3().length() != 4 || getCardNumber4().length() != 4) {
        addFieldError("cardNumber1", "Please enter a valid card number.");
      } else if (!RactActionSupport.REGEXP_DIGITS.matcher(getCardNumber()).matches()) {
        addFieldError("cardNumber1", "Please enter a valid card number.");
      }
    } else if (!getEmail().isEmpty()) {
      if (!RactActionSupport.REGEXP_EMAIL.matcher(getEmail()).matches()) {
        addFieldError("email", "Please supply a valid email address");
      }
    }
  }

  /**
   * Honey pot - this field should remain blank, invalid otherwise.
   * @return the gotcha
   */
  @StringLengthFieldValidator(message = "An error occurred", minLength="0", maxLength="0")
  public String getGotcha() {
    return gotcha;
  }

  /**
   * @param gotcha the gotcha to set
   */
  public void setGotcha(String gotcha) {
    this.gotcha = gotcha;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the cardNumber1
   */
  public String getCardNumber1() {
    return cardNumber1;
  }

  /**
   * @param cardNumber1 the cardNumber1 to set
   */
  public void setCardNumber1(String cardNumber1) {
    this.cardNumber1 = cardNumber1;
  }

  /**
   * @return the cardNumber2
   */
  public String getCardNumber2() {
    return cardNumber2;
  }

  /**
   * @param cardNumber2 the cardNumber2 to set
   */
  public void setCardNumber2(String cardNumber2) {
    this.cardNumber2 = cardNumber2;
  }

  /**
   * @return the cardNumber3
   */
  public String getCardNumber3() {
    return cardNumber3;
  }

  /**
   * @param cardNumber3 the cardNumber3 to set
   */
  public void setCardNumber3(String cardNumber3) {
    this.cardNumber3 = cardNumber3;
  }

  /**
   * @return the cardNumber4
   */
  public String getCardNumber4() {
    return cardNumber4;
  }

  /**
   * @param cardNumber4 the cardNumber4 to set
   */
  public void setCardNumber4(String cardNumber4) {
    this.cardNumber4 = cardNumber4;
  }

  /**
   * Returns an aggregation of 4 card number parts.
   * @return
   */
  public String getCardNumber() {
    return getCardNumber1() + getCardNumber2() + getCardNumber3() + getCardNumber4();
  }

  /**
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }

}
