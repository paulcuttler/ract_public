package futuremedium.client.ract.secure.actions.user;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Following a Member successfully adding Tipping to their account
 * hit this action folloing SSO to print success message.
 * @author gnewton
 */
public class PostAddTipping extends RactActionSupport {
  
  @Override
  public String execute() throws Exception {

    this.setActionMessage("Your tipping account has been created. You will receive a confirmation email shortly.");
    this.setPageTitle("Tipping Account Activation Successful.");

    return SUCCESS;
  }
}
