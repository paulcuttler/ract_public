package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.*;

import futuremedium.client.ract.secure.actions.*;

/**
 * Display user signup form.
 * @author gnewton
 */
public class Signup extends RactActionSupport {
  private String cardNumber1;
  private String cardNumber2;
  private String verifyOption;
  private boolean showTipping = false;

  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    this.setCardNumber1("3084");
    this.setCardNumber2("07");
    this.setVerifyOption("dob");

    return SUCCESS;
  }

  /**
   * @return the cardNumber1
   */
  public String getCardNumber1() {
    return cardNumber1;
  }

  /**
   * @param cardNumber1 the cardNumber1 to set
   */
  public void setCardNumber1(String cardNumber1) {
    this.cardNumber1 = cardNumber1;
  }

  /**
   * @return the cardNumber2
   */
  public String getCardNumber2() {
    return cardNumber2;
  }

  /**
   * @param cardNumber2 the cardNumber2 to set
   */
  public void setCardNumber2(String cardNumber2) {
    this.cardNumber2 = cardNumber2;
  }

  /**
   * @return the verifyOption
   */
  public String getVerifyOption() {
    return verifyOption;
  }

  /**
   * @param verifyOption the verifyOption to set
   */
  public void setVerifyOption(String verifyOption) {
    this.verifyOption = verifyOption;
  }

  /**
   * @return the showTipping
   */
  public boolean isShowTipping() {
    return showTipping;
  }

  /**
   * @param showTipping the showTipping to set
   */
  public void setShowTipping(boolean showTipping) {
    this.showTipping = showTipping;
  }
}
