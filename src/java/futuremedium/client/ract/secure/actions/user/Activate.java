package futuremedium.client.ract.secure.actions.user;

import com.ract.web.client.ClientTransaction;
import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.web.client.ActivationRequest;
import com.ract.web.client.ClientTransactionPK;
import futuremedium.client.ract.secure.Config;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Process an activation link.
 * @author gnewton
 */
public class Activate extends RactActionSupport {
  private String cardNumber1;
  private String cardNumber2;
  private String suppressLoginText;

  private String activationRequestNo;

  @Override
  public String execute() throws Exception {
    try {
      setUp();

      int activationRequest = Integer.parseInt(getActivationRequestNo());
      userMgr.activateUser(activationRequest);

      Config.logger.info(Config.key + ": ActivationRequest number: " + activationRequest);
      ActivationRequest request = userMgr.getActivationRequest(activationRequest);
      if (request.getUpdateEmailAddress() != null && !request.getUpdateEmailAddress().isEmpty()) {
        // User wants to update his client record with this email address

        ClientVO client = this.getCustomerMgr().getClient(Integer.parseInt(request.getClientNumber()));
        
        // retrieve/create client transaction
        ClientTransaction trans = this.getCustomer(client.getClientNumber());

        // set email address
        trans.setEmailAddress(request.getUpdateEmailAddress());

        // create new primary key for transaction
        ClientTransactionPK pk = trans.getClientTransactionPK();
        pk.setTransactionDate(new DateTime());
        pk.setTransactionType(ClientTransaction.TRANSACTION_TYPE_UPDATE);
        trans.setClientTransactionPK(pk);

        // persist transaction
        this.getCustomerMgr().createClientTransaction(trans);
      }

      this.setActionMessage("You now have access to the member only access section. To continue please enter your login details below.");
      this.setPageTitle("Member Only Access Successfully Activated");

    } catch (GenericException e) {
      this.setActionMessage("An error occurred: " + e.getMessage());
      Config.logger.error(Config.key + ": An error occurred activating member request: ", e);
    } catch (Exception e) { // catch NPE etc.
      this.setActionMessage("An error occurred: Invalid activation request");
      Config.logger.error(Config.key + ": An error occurred activating member request: ", e);
    }

    this.setCardNumber1("3084");
    this.setCardNumber2("07");
    this.setSuppressLoginText("true");

    return SUCCESS;
  }

  /**
   * @return the activationRequestNo
   */
  public String getActivationRequestNo() {
    return activationRequestNo;
  }

  /**
   * @param activationRequestNo the activationRequestNo to set
   */
  public void setActivationRequestNo(String activationRequestNo) {
    this.activationRequestNo = activationRequestNo;
  }

  /**
   * @return the cardNumber1
   */
  public String getCardNumber1() {
    return cardNumber1;
  }

  /**
   * @param cardNumber1 the cardNumber1 to set
   */
  public void setCardNumber1(String cardNumber1) {
    this.cardNumber1 = cardNumber1;
  }

  /**
   * @return the cardNumber2
   */
  public String getCardNumber2() {
    return cardNumber2;
  }

  /**
   * @param cardNumber2 the cardNumber2 to set
   */
  public void setCardNumber2(String cardNumber2) {
    this.cardNumber2 = cardNumber2;
  }

  /**
   * @return the suppressLoginText
   */
  public String getSuppressLoginText() {
    return suppressLoginText;
  }

  /**
   * @param suppressLoginText the suppressLoginText to set
   */
  public void setSuppressLoginText(String suppressLoginText) {
    this.suppressLoginText = suppressLoginText;
  }
}
