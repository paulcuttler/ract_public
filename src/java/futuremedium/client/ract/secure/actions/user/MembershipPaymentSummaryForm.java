package futuremedium.client.ract.secure.actions.user;

import java.util.Iterator;

import com.opensymphony.xwork2.ActionContext;

import com.ract.membership.MembershipVO;
import com.ract.payment.bank.*;
import com.ract.payment.directdebit.*;

import futuremedium.client.ract.secure.actions.*;

/**
 *
 * @author gnewton
 */
public class MembershipPaymentSummaryForm extends RactActionSupport {

  private DirectDebitAuthority authority;
  private boolean creditCard = false;

  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    MembershipVO personal = (MembershipVO) getSession().get(MEMBER_TOKEN);
    if (personal != null) {
      Iterator i = personal.getPayableSchedules().iterator();

      while (i.hasNext()) {
        DirectDebitSchedule dds = (DirectDebitSchedule) i.next();
        setAuthority(dds.getDirectDebitAuthority());
        
        if (authority.getBankAccount() instanceof DebitAccount) {
          setCreditCard(false);
        } else if (authority.getBankAccount() instanceof CreditCardAccount) {
          setCreditCard(true);
        }

        break; // should only be one schedule
      }
    }

    return SUCCESS;
  }

  /**
   * @return the authority
   */
  public DirectDebitAuthority getAuthority() {
    return authority;
  }

  /**
   * @param authority the authority to set
   */
  public void setAuthority(DirectDebitAuthority authority) {
    this.authority = authority;
  }

  /**
   * @return the creditCard
   */
  public boolean isCreditCard() {
    return creditCard;
  }

  /**
   * @param creditCard the creditCard to set
   */
  public void setCreditCard(boolean creditCard) {
    this.creditCard = creditCard;
  }
}
