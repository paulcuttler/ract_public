package futuremedium.client.ract.secure.actions.user;

import com.opensymphony.xwork2.ActionContext;

import com.ract.common.GenericException;
import com.ract.web.client.ClientTransaction;
import com.ract.web.security.*;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Step 1/2 of the Update Customer Details process.
 * @author gnewton
 */
public class UpdateCustomerForm extends RactActionSupport {

  private String homePhoneAreaCode;
  private String homePhone;
  private String workPhoneAreaCode;
  private String workPhone;

  @SuppressWarnings("unchecked")
  @Override
  public String execute() throws Exception {
    setUp();

    this.setActionName(ActionContext.getContext().getName());

    User user = (User) getSession().get(USER_TOKEN);

    // refresh ClientTransaction in session
    if (user != null && user instanceof MemberUser) {
      String clientNumber = ((MemberUser) user).getClientNumber();
      ClientTransaction customer = getCustomer(Integer.parseInt(clientNumber));
      getSession().put(CUSTOMER_TOKEN, customer);
    }

    try {
      prepareTitles();
      preparePhoneNumbers();
      checkAddress();
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      return ERROR;
    }
    
    return SUCCESS;
  }

  @SuppressWarnings("unchecked")
  private void prepareTitles() throws GenericException {
    getSession().put("titles", customerMgr.getClientTitles(true, false));
  }

  /**
   * Utility method to split raw phone number into area code and number.
   * @throws GenericException
   */
  private void preparePhoneNumbers() throws GenericException {
    ClientTransaction customer = (ClientTransaction) getSession().get(CUSTOMER_TOKEN);

    String rawHomePhone = customer.getHomePhone();
    if (rawHomePhone != null && !rawHomePhone.isEmpty() && rawHomePhone.length() >= 2) {
      this.setHomePhoneAreaCode(rawHomePhone.substring(0, 2));
      if (rawHomePhone.length() == 10) {
        this.setHomePhone(rawHomePhone.substring(2, 10));
      } else {
        this.setHomePhone(rawHomePhone.substring(2));
      }
    }

    String rawWorkPhone = customer.getWorkPhone();
    if (rawWorkPhone != null && !rawWorkPhone.isEmpty() && rawWorkPhone.length() >= 2) {
      this.setWorkPhoneAreaCode(rawWorkPhone.substring(0, 2));
      if (rawWorkPhone.length() == 10) {
        this.setWorkPhone(rawWorkPhone.substring(2, 10));
      } else {
        this.setWorkPhone(rawWorkPhone.substring(2));
      }
    }
  }

  /**
   * @return the homePhoneAreaCode
   */
  public String getHomePhoneAreaCode() {
    return homePhoneAreaCode;
  }

  /**
   * @param homePhoneAreaCode the homePhoneAreaCode to set
   */
  public void setHomePhoneAreaCode(String homePhoneAreaCode) {
    this.homePhoneAreaCode = homePhoneAreaCode;
  }

  /**
   * @return the homePhone
   */
  public String getHomePhone() {
    return homePhone;
  }

  /**
   * @param homePhone the homePhone to set
   */
  public void setHomePhone(String homePhone) {
    this.homePhone = homePhone;
  }

  /**
   * @return the workPhoneAreaCode
   */
  public String getWorkPhoneAreaCode() {
    return workPhoneAreaCode;
  }

  /**
   * @param workPhoneAreaCode the workPhoneAreaCode to set
   */
  public void setWorkPhoneAreaCode(String workPhoneAreaCode) {
    this.workPhoneAreaCode = workPhoneAreaCode;
  }

  /**
   * @return the workPhone
   */
  public String getWorkPhone() {
    return workPhone;
  }

  /**
   * @param workPhone the workPhone to set
   */
  public void setWorkPhone(String workPhone) {
    this.workPhone = workPhone;
  }

  @SuppressWarnings("unchecked")
  private void checkAddress() {
    ClientTransaction customer = (ClientTransaction) getSession().get(CUSTOMER_TOKEN);

    // if residential and postal addresses are different, pre-select show postal address checkbox
    if (
            (customer.getResiSuburb() != null && !customer.getResiSuburb().equals(customer.getPostSuburb())) ||
            (customer.getResiStreet() != null && !customer.getResiStreet().equals(customer.getPostStreet())) ||
            (customer.getResiStreetChar() != null && !customer.getResiStreetChar().equals(customer.getPostStreetChar())) ||
            (customer.getResiProperty() != null && !customer.getResiProperty().equals(customer.getPostProperty())) ||
            (customer.getResiPostcode() != null && !customer.getResiPostcode().equals(customer.getPostPostcode())) ||
            (customer.getResiState() != null && !customer.getResiState().equals(customer.getPostState()))
      ) {
      getSession().put("showPostal", true);
    }
  }
}

