package futuremedium.client.ract.secure.actions;

import java.util.*;

import com.opensymphony.xwork2.validator.annotations.*;

import com.ract.client.ClientTitleVO;
import com.ract.util.DateTime;
import com.ract.web.client.WebClient;

import futuremedium.client.ract.secure.actions.join.Personal;
import futuremedium.client.ract.secure.actions.user.ChangeDetail;
import futuremedium.client.ract.secure.actions.user.Create;
import futuremedium.client.ract.secure.actions.user.Promote;
import futuremedium.client.ract.secure.actions.user.TippingCreate;
import futuremedium.client.ract.secure.actions.user.UpdateCustomer;

/**
 * Base class for dealing with the following similar actions:
 * - UserCreate
 * - UserTippingCreate
 * - UserUpdateCustomer
 * - UserChangeDetail
 * - JoinPersonal
 *
 * @author gnewton
 */
public class CreateUpdateActionBase extends RactActionSupport {

  private boolean showCardNumber = false;
  private boolean showTipping = false;
  private boolean showPostal = false;
  private boolean terms = false;
  private String cardNumber1;
  private String cardNumber2;
  private String cardNumber3;
  private String cardNumber4;
  private String username;
  private String password;
  private String password2;
  private String team;
  private String title;
  private String firstName;
  private String lastName;
  private String gender;
  private String email;
  private String confirmEmail;
  private String dob;
  private DateTime dobDate;
  // residential details
  private String residentialPropertyName;
  private String residentialPropertyQualifier; // street number
  private String residentialStreet;
  private String residentialSuburb;
  private String residentialPostcode;
  private String residentialState;
  private String residentialStreetSuburbId;
  // postal details
  private String postalPropertyName;
  private String postalPropertyQualifier; // street number
  private String postalStreet;
  private String postalSuburb;
  private String postalPostcode;
  private String postalState;
  private String postalStreetSuburbId;
  private String homePhone;
  private String mobilePhone;
  private String workPhone;
  private String homePhoneAreaCode;
  private String workPhoneAreaCode;
  private String gotcha;
  private String verifyOption;
  private boolean updateDetails = false;

  @Override
  public void validate() {

    if (isShowPostal()) {
      if (getPostalStreet().isEmpty() && getPostalPropertyName().isEmpty()) {
        addFieldError("postalPropertyName", "Please provide either a Property name, PO Box or Street name");
      }
      if (getPostalSuburb() == null || getPostalSuburb().isEmpty()) {
        addFieldError("postalSuburb", "Please supply a postal suburb");
      }
      if (getPostalState() == null || getPostalState().isEmpty()) {
        addFieldError("postalState", "Please supply a postal state");
      }
      if (getPostalPostcode() == null || getPostalPostcode().isEmpty()) {
        addFieldError("postalPostcode", "Please supply a postal postcode");
      }
    }

    if (this instanceof ChangeDetail || this instanceof Personal || this instanceof TippingCreate) {
      validateDob();

      if (getTitle() == null || getTitle().isEmpty()) {
        addFieldError("title", "Please supply a title");
      }
      if (getFirstName() == null || getFirstName().isEmpty()) {
        addFieldError("firstName", "Please supply given names");
      }
      if (getGender() == null || getGender().isEmpty()) {
        addFieldError("gender", "Please supply a gender");
      }

      if (getTitle() != null && getGender() != null) {
        validateTitle();
      }
    }

    if (this instanceof Create || this instanceof Promote) {
      if ( (getHomePhone() == null || getHomePhone().isEmpty()) 
              && (getMobilePhone() == null || getMobilePhone().isEmpty())
              && (getDob() == null || getDob().isEmpty()) ) {
        addFieldError("mobilePhone", "Please provide either a phone, mobile number or date of birth.");

      } else {
        if (getHomePhone() != null && !getHomePhone().isEmpty()) {
          validateHomePhone("Work/home phone");
        }
        if (getMobilePhone() != null && !getMobilePhone().isEmpty()) {
          validateMobile();
        }
        if (getDob() != null && !getDob().isEmpty()) {
          validateDob();
        }
      }

      if (getResidentialPostcode() == null || getResidentialPostcode().isEmpty()) {
        addFieldError("residentialPostcode", "Please supply a postcode");
      }

      validateCardNumber();

    } else { // Anything other than UserCreate and UserPromote

      if (getHomePhone().isEmpty() && getHomePhoneAreaCode().isEmpty()
          && getWorkPhone().isEmpty() && getWorkPhoneAreaCode().isEmpty() && getMobilePhone().isEmpty()) {
        addFieldError("homePhone", "Please provide at least one telephone number.");
      } else {
        if ((getHomePhone() != null && !getHomePhone().isEmpty()) 
            || (getHomePhoneAreaCode() != null && !getHomePhoneAreaCode().isEmpty())) {
          validateHomePhone("Home phone");
        }
        if ((getWorkPhone() != null && !getWorkPhone().isEmpty()) 
            || (getWorkPhoneAreaCode() != null && !getWorkPhoneAreaCode().isEmpty())) {
          validateWorkPhone();
        }
        if (getMobilePhone() != null && !getMobilePhone().isEmpty()) {
          validateMobile();
        }
      }

      if (!getEmail().equals(getConfirmEmail())) {
        addFieldError("confirmEmail", "Email addresses must match");
      }
      if (getResidentialPropertyQualifier() == null || getResidentialPropertyQualifier().isEmpty()) {
        addFieldError("residentialPropertyQualifier", "Please supply a residential property number");
      }
      if (getResidentialStreet() == null || getResidentialStreet().isEmpty()) {
        addFieldError("residentialStreet", "Please supply a residential street");
      }
      if (getResidentialSuburb() == null || getResidentialSuburb().isEmpty()) {
        addFieldError("residentialSuburb", "Please supply a residential suburb");
      }
      if (getResidentialState() == null || getResidentialState().isEmpty()) {
        addFieldError("residentialState", "Please supply a residential state");
      }
      if (getResidentialPostcode() == null || getResidentialPostcode().isEmpty()) {
        addFieldError("residentialPostcode", "Please supply a residential postcode");
      }
    }

    if (this instanceof Create || this instanceof TippingCreate) {
      if (isShowTipping() || this instanceof TippingCreate) {
        if (getUsername() == null || getUsername().isEmpty()) {
          addFieldError("username", "Please supply a username");
        } else if (getUsername().length() < 4 || getUsername().length() > 15) {
          addFieldError("username", "Your username must be between 4 and 15 characters long");
        } else if (!RactActionSupport.REGEXP_USERNAME.matcher(getUsername()).matches()) {
          addFieldError("username", "Your username may contain alphanumeric characters but no spaces");
        }
      }

      if (getPassword() == null || getPassword().isEmpty()) {
        addFieldError("password", "Please supply a password");
      } else if (getPassword().length() < 6) {
        addFieldError("password", "Your password must be at least 6 characters long");
      } else if (!RactActionSupport.REGEXP_USERNAME.matcher(getPassword()).matches()) {
        addFieldError("password", "Your password may contain alphanumeric characters but no spaces");
      } else if (getPassword2() != null && !getPassword().equals(getPassword2())) {
        addFieldError("password", "Your supplied passwords must match");
      }

      if (getPassword2() == null || getPassword2().isEmpty()) {
        addFieldError("password2", "Please supply a password confirmation");
      }

      if (!isTerms()) {
        addFieldError("terms", "You must agree to the terms and conditions");
      }

      // gotcha should be passed and blank
      if (getGotcha() == null || !getGotcha().isEmpty()) {
        addFieldError("gotcha", "An error has occurred.");
      }
    }

    if (this instanceof TippingCreate) {
      if (this.isShowCardNumber()) {
        validateCardNumber();
      }
    }

    if (!(this instanceof UpdateCustomer)) {
      if (getLastName() == null || getLastName().isEmpty()) {
        addFieldError("lastName", "Please supply a surname");
      }
    }

    if (!(this instanceof Promote)) {
      if (getEmail() == null || getEmail().isEmpty()) {
        addFieldError("email", "Please supply an email address");
      }
    }

  }

  private void validateDob() {
    validateDob(getDob(), 16, 99, "dob", "Date of Birth");
  }

  private void validateHomePhone(String fieldLabel) {
    if (getHomePhone().length() != 8 || !RactActionSupport.REGEXP_DIGITS.matcher(getHomePhone()).matches()) {
      addFieldError("homePhone", fieldLabel + " must be eight digits long.");
    } else if (getHomePhoneAreaCode() == null || getHomePhoneAreaCode().length() != 2 || !getHomePhoneAreaCode().startsWith("0") || !RactActionSupport.REGEXP_DIGITS.matcher(getHomePhoneAreaCode()).matches()) {
      addFieldError("homePhone", fieldLabel + " must contain a two digit area code starting with zero.");
    }
  }

  private void validateWorkPhone() {
    if (getWorkPhone().length() != 8 || !RactActionSupport.REGEXP_DIGITS.matcher(getWorkPhone()).matches()) {
      addFieldError("workPhone", "Work phone must be eight digits long.");
    } else if (getWorkPhoneAreaCode() == null || getWorkPhoneAreaCode().length() != 2 || !getWorkPhoneAreaCode().startsWith("0") || !RactActionSupport.REGEXP_DIGITS.matcher(getWorkPhoneAreaCode()).matches()) {
      addFieldError("workPhone", "Work phone must contain a two digit area code starting with zero.");
    }
  }

  private void validateMobile() {
    if (getMobilePhone().length() != 10 || !getMobilePhone().startsWith("0") || !RactActionSupport.REGEXP_DIGITS.matcher(getMobilePhone()).matches()) {
      addFieldError("mobilePhone", "Mobile phone must be ten digits long starting with zero.");
    }
  }

  private void validateTitle() {
    @SuppressWarnings("unchecked")
    Collection<ClientTitleVO> titleList = (Collection<ClientTitleVO>) getSession().get("titles");

    if (getTitle().equals("MX")) {
    	if(!getGender().equals("unspecified"))
	          addFieldError("title", "Gender selected is not appropriate for the title selected");
    } else {
	    for (ClientTitleVO ttl : titleList) {
	      if (getTitle().equals(ttl.getTitleName()) && ttl.getSexSpecific()) {
	        if ((ttl.getSex() && !getGender().equals(WebClient.GENDER_MALE)) || (!ttl.getSex() && !getGender().equals(WebClient.GENDER_FEMALE))) {
	          addFieldError("title", "Gender selected is not appropriate for the title selected");
	        }
	      }
	    }
    }
  }

  private void validateCardNumber() {
    if (getCardNumber1().length() != 4 || getCardNumber2().length() != 4 || getCardNumber3().length() != 4 || getCardNumber4().length() != 4) {
      addFieldError("cardNumber1", "Please enter a valid card number.");
    } else if (!RactActionSupport.REGEXP_DIGITS.matcher(getCardNumber()).matches()) {
      addFieldError("cardNumber1", "Please enter a valid card number.");
    }
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * @return the gender
   */
  public String getGender() {
    return gender;
  }

  /**
   * @param gender the gender to set
   */
  public void setGender(String gender) {
    this.gender = gender;
  }

  /**
   * @return the email
   */
  @EmailValidator(message = "Please supply a valid email")
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the dob
   */
  public String getDob() {
    return dob;
  }

  /**
   * @param dob the dob to set
   */
  public void setDob(String dob) {
    this.dob = dob;
  }

  /**
   * @return the residentialPropertyName
   */
  public String getResidentialPropertyName() {
    return residentialPropertyName;
  }

  /**
   * @param residentialPropertyName the residentialPropertyName to set
   */
  public void setResidentialPropertyName(String residentialPropertyName) {
    this.residentialPropertyName = residentialPropertyName;
  }

  /**
   * @return the residentialPropertyQualifier
   */
  public String getResidentialPropertyQualifier() {
    return residentialPropertyQualifier;
  }

  /**
   * @param residentialPropertyQualifier the residentialPropertyQualifier to set
   */
  public void setResidentialPropertyQualifier(String residentialPropertyQualifier) {
    this.residentialPropertyQualifier = residentialPropertyQualifier;
  }

  /**
   * @return the residentialStreet
   */
  public String getResidentialStreet() {
    return residentialStreet;
  }

  /**
   * @param residentialStreet the residentialStreet to set
   */
  public void setResidentialStreet(String residentialStreet) {
    this.residentialStreet = residentialStreet;
  }

  /**
   * @return the residentialSuburb
   */
  public String getResidentialSuburb() {
    return residentialSuburb;
  }

  /**
   * @param residentialSuburb the residentialSuburb to set
   */
  public void setResidentialSuburb(String residentialSuburb) {
    this.residentialSuburb = residentialSuburb;
  }

  /**
   * @return the residentialPostcode
   */
  public String getResidentialPostcode() {
    return residentialPostcode;
  }

  /**
   * @param residentialPostcode the residentialPostcode to set
   */
  public void setResidentialPostcode(String residentialPostcode) {
    this.residentialPostcode = residentialPostcode;
  }

  /**
   * @return the residentialState
   */
  public String getResidentialState() {
    return residentialState;
  }

  /**
   * @param residentialState the residentialState to set
   */
  public void setResidentialState(String residentialState) {
    this.residentialState = residentialState;
  }

  /**
   * @return the postalPropertyName
   */
  public String getPostalPropertyName() {
    return postalPropertyName;
  }

  /**
   * @param postalPropertyName the postalPropertyName to set
   */
  public void setPostalPropertyName(String postalPropertyName) {
    this.postalPropertyName = postalPropertyName;
  }

  /**
   * @return the postalPropertyQualifier
   */
  public String getPostalPropertyQualifier() {
    return postalPropertyQualifier;
  }

  /**
   * @param postalPropertyQualifier the postalPropertyQualifier to set
   */
  public void setPostalPropertyQualifier(String postalPropertyQualifier) {
    this.postalPropertyQualifier = postalPropertyQualifier;
  }

  /**
   * @return the postalStreet
   */
  public String getPostalStreet() {
    return postalStreet;
  }

  /**
   * @param postalStreet the postalStreet to set
   */
  public void setPostalStreet(String postalStreet) {
    this.postalStreet = postalStreet;
  }

  /**
   * @return the postalSuburb
   */
  public String getPostalSuburb() {
    return postalSuburb;
  }

  /**
   * @param postalSuburb the postalSuburb to set
   */
  public void setPostalSuburb(String postalSuburb) {
    this.postalSuburb = postalSuburb;
  }

  /**
   * @return the postalPostcode
   */
  public String getPostalPostcode() {
    return postalPostcode;
  }

  /**
   * @param postalPostcode the postalPostcode to set
   */
  public void setPostalPostcode(String postalPostcode) {
    this.postalPostcode = postalPostcode;
  }

  /**
   * @return the postalState
   */
  public String getPostalState() {
    return postalState;
  }

  /**
   * @param postalState the postalState to set
   */
  public void setPostalState(String postalState) {
    this.postalState = postalState;
  }

  /**
   * @return the homePhone
   */
  public String getHomePhone() {
    return homePhone;
  }

  /**
   * @param homePhone the homePhone to set
   */
  public void setHomePhone(String homePhone) {
    this.homePhone = homePhone;
  }

  /**
   * @return the mobilePhone
   */
  public String getMobilePhone() {
    return mobilePhone;
  }

  /**
   * @param mobilePhone the mobilePhone to set
   */
  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }

  /**
   * @return the workPhone
   */
  public String getWorkPhone() {
    return workPhone;
  }

  /**
   * @param workPhone the workPhone to set
   */
  public void setWorkPhone(String workPhone) {
    this.workPhone = workPhone;
  }

  /**
   * @return the residentialStreetSuburbId
   */
  public String getResidentialStreetSuburbId() {
    return residentialStreetSuburbId;
  }

  /**
   * @param residentialStreetSuburbId the residentialStreetSuburbId to set
   */
  public void setResidentialStreetSuburbId(String residentialStreetSuburbId) {
    this.residentialStreetSuburbId = residentialStreetSuburbId;
  }

  /**
   * @return the postalStreetSuburbId
   */
  public String getPostalStreetSuburbId() {
    return postalStreetSuburbId;
  }

  /**
   * @param postalStreetSuburbId the postalStreetSuburbId to set
   */
  public void setPostalStreetSuburbId(String postalStreetSuburbId) {
    this.postalStreetSuburbId = postalStreetSuburbId;
  }

  /**
   * @return the showPostal
   */
  public boolean isShowPostal() {
    return showPostal;
  }

  /**
   * @param showPostal the showPostal to set
   */
  public void setShowPostal(boolean showPostal) {
    this.showPostal = showPostal;
  }

  /**
   * Honey pot - this field should remain blank, invalid otherwise.
   * @return the gotcha
   */
  public String getGotcha() {
    return gotcha;
  }

  /**
   * @param gotcha the gotcha to set
   */
  public void setGotcha(String gotcha) {
    this.gotcha = gotcha;
  }

  /**
   * @return the confirmEmail
   */
  @EmailValidator(message = "Please supply a valid confirmation email")
  public String getConfirmEmail() {
    return confirmEmail;
  }

  /**
   * @param confirmEmail the confirmEmail to set
   */
  public void setConfirmEmail(String confirmEmail) {
    this.confirmEmail = confirmEmail;
  }

  /**
   * @return the homePhoneAreaCode
   */
  public String getHomePhoneAreaCode() {
    return homePhoneAreaCode;
  }

  /**
   * @param homePhoneAreaCode the homePhoneAreaCode to set
   */
  public void setHomePhoneAreaCode(String homePhoneAreaCode) {
    this.homePhoneAreaCode = homePhoneAreaCode;
  }

  /**
   * @return the workPhoneAreaCode
   */
  public String getWorkPhoneAreaCode() {
    return workPhoneAreaCode;
  }

  /**
   * @param workPhoneAreaCode the workPhoneAreaCode to set
   */
  public void setWorkPhoneAreaCode(String workPhoneAreaCode) {
    this.workPhoneAreaCode = workPhoneAreaCode;
  }

  /**
   * @return the cardNumber1
   */
  public String getCardNumber1() {
    return cardNumber1;
  }

  /**
   * @param cardNumber1 the cardNumber1 to set
   */
  public void setCardNumber1(String cardNumber1) {
    this.cardNumber1 = cardNumber1;
  }

  /**
   * @return the cardNumber2
   */
  public String getCardNumber2() {
    return cardNumber2;
  }

  /**
   * @param cardNumber2 the cardNumber2 to set
   */
  public void setCardNumber2(String cardNumber2) {
    this.cardNumber2 = cardNumber2;
  }

  /**
   * @return the cardNumber3
   */
  public String getCardNumber3() {
    return cardNumber3;
  }

  /**
   * @param cardNumber3 the cardNumber3 to set
   */
  public void setCardNumber3(String cardNumber3) {
    this.cardNumber3 = cardNumber3;
  }

  /**
   * @return the cardNumber4
   */
  public String getCardNumber4() {
    return cardNumber4;
  }

  /**
   * @param cardNumber4 the cardNumber4 to set
   */
  public void setCardNumber4(String cardNumber4) {
    this.cardNumber4 = cardNumber4;
  }

  /**
   * Returns an aggregation of 4 card number parts.
   * @return
   */
  public String getCardNumber() {
    return getCardNumber1() + getCardNumber2() + getCardNumber3() + getCardNumber4();
  }

  /**
   * @return the showCardNumber
   */
  public boolean isShowCardNumber() {
    return showCardNumber;
  }

  /**
   * @param showCardNumber the showCardNumber to set
   */
  public void setShowCardNumber(boolean showCardNumber) {
    this.showCardNumber = showCardNumber;
  }

  /**
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @return the password2
   */
  public String getPassword2() {
    return password2;
  }

  /**
   * @param password2 the password2 to set
   */
  public void setPassword2(String password2) {
    this.password2 = password2;
  }

  /**
   * @return the showTipping
   */
  public boolean isShowTipping() {
    return showTipping;
  }

  /**
   * @param showTipping the showTipping to set
   */
  public void setShowTipping(boolean showTipping) {
    this.showTipping = showTipping;
  }

  /**
   * @return the terms
   */
  public boolean isTerms() {
    return terms;
  }

  /**
   * @param terms the terms to set
   */
  public void setTerms(boolean terms) {
    this.terms = terms;
  }

  /**
   * @return the team
   */
  public String getTeam() {
    return team;
  }

  /**
   * @param team the team to set
   */
  public void setTeam(String team) {
    this.team = team;
  }

  /**
   * @return the dobDate
   */
  public DateTime getDobDate() {
  	if (this.dobDate == null) {
	    try { // init dobDate from dob passed from form
	      this.dobDate = new DateTime(this.getDob());
	    } catch (Exception e) {
	      // do nothing, this will be handled by validateDob
	    }
	  }
    return dobDate;
  }

  /**
   * @param dobDate the dobDate to set
   */
  public void setDobDate(DateTime dobDate) {
    this.dobDate = dobDate;
  }

  /**
   * @return the verifyOption
   */
  public String getVerifyOption() {
    return verifyOption;
  }

  /**
   * @param verifyOption the verifyOption to set
   */
  public void setVerifyOption(String verifyOption) {
    this.verifyOption = verifyOption;
  }

  /**
   * @return the updateDetails
   */
  public boolean isUpdateDetails() {
    return updateDetails;
  }

  /**
   * @param updateDetails the updateDetails to set
   */
  public void setUpdateDetails(boolean updateDetails) {
    this.updateDetails = updateDetails;
  }
}

