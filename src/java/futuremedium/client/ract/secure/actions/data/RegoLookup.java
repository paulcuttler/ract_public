package futuremedium.client.ract.secure.actions.data;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.ract.common.SystemException;
import com.ract.util.DateTime;
import com.ract.web.insurance.GlVehicle;

import futuremedium.client.ract.secure.Config;

public class RegoLookup extends DataProtectionBase {
    private static final long serialVersionUID = 4001222407032430705L;
    private String rego;

    private String nvic;
    private String description;
    private String summary;
    private String acceptable;

    public String execute() throws Exception {

        this.setActionName(ActionContext.getContext().getName());

        try {
            this.nvic = this.getGlassesMgr().getNvicByRegistration(rego, STATE_TAS);

            if (this.nvic != null) {
                GlVehicle vehicle = this.getGlassesMgr().getVehicle(this.nvic, new DateTime());
                if (null != vehicle) {
                    this.setDescription(vehicle.getVehYear() + " " + vehicle.getMake() + " " + vehicle.getModel() + " ");
                    this.setSummary(vehicle.getSummary());
                    this.setAcceptable(vehicle.getAcceptable());
                }
            }
        } catch (SystemException e) {
            Config.logger.error(Config.key + ": An Error occurred accessing the registration lookup: rego=" + rego, e);
            return ERROR;
        }

        return SUCCESS;
    }

    public String getRego() {
        return rego;
    }

    @RequiredStringValidator(message = "Please provide a registration number")
    public void setRego(String rego) {
        this.rego = rego;
    }

    public String getNvic() {
        return nvic;
    }

    public void setNvic(String nvic) {
        this.nvic = nvic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getAcceptable() {
        return acceptable;
    }

    public void setAcceptable(String acceptable) {
        this.acceptable = acceptable;
    }
}
