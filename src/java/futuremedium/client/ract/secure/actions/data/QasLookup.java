package futuremedium.client.ract.secure.actions.data;

import java.util.Collection;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.qas.ws.PicklistEntryType;

import futuremedium.client.ract.secure.actions.insurance.InsuranceBase;

public class QasLookup extends DataProtectionBase {

	private String addressQuery;
	private Integer quoteNo;
	private boolean national = false;
	private String addressType;
	
	static public final String TYPE_RESIDENTIAL = "residential";
	static public final String TYPE_POSTAL = "postal";
	static private final Integer QAS_QUERY_CHAR_MIN = 3;
	
	private Collection<PicklistEntryType> qasResultList;
		
	public String execute() throws Exception {
		
    if (this.getAddressQuery() != null && this.getAddressQuery().length() >= QAS_QUERY_CHAR_MIN) {
    	if (this.getQuoteNo() != null) {
    		String addressQry = this.getAddressQuery();
    		if (addressQry.length() > 50) {
    			addressQry = addressQry.substring(0, 50);
    		}
    		this.getInsuranceMgr().setQuoteDetail(this.getQuoteNo(), InsuranceBase.FIELD_RISK_ADDRESS_QUERY, addressQry);
    	}
    	
    	this.setQasResultList(this.getQasService().doAddressSearch(this));
    }
    
		return SUCCESS;
	}

	public Collection<PicklistEntryType> getQasResultList() {
		return qasResultList;
	}

	public void setQasResultList(Collection<PicklistEntryType> resultList) {
		this.qasResultList = resultList;
	}

	public Integer getQuoteNo() {
		return quoteNo;
	}

	public void setQuoteNo(Integer quoteNo) {
		this.quoteNo = quoteNo;
	}

	public String getAddressQuery() {
		return addressQuery;
	}

	@RequiredStringValidator
	public void setAddressQuery(String addressQuery) {
		this.addressQuery = addressQuery;
	}

	public boolean isNational() {
		return national;
	}

	public void setNational(boolean national) {
		this.national = national;
	}

	public String getAddressType() {
		return addressType;
	}
	
	@RequiredStringValidator
	public void setAddressType(String type) {
		this.addressType = type;
	}
}
