package futuremedium.client.ract.secure.actions.data;

import java.util.*;

import com.opensymphony.xwork2.ActionContext;

import com.ract.common.SystemException;
import com.ract.util.DateTime;
import com.ract.web.insurance.GlVehicle;
import com.ract.web.insurance.ListPair;

import futuremedium.client.ract.secure.Config;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Queries Glasses Guide Vehicle data based on a combination of parameters.
 * Returns data as JSON.
 * @author gnewton
 */
public class GlassesGuide extends RactActionSupport {
    private static final long serialVersionUID = 4856591880864909030L;
  static private int MAKE = 1;
  static private int MODEL = 2;
  static private int BODY_TYPE = 3;
  static private int TRANSMISSION = 4;
  static private int CYLINDERS = 5;
  static private int VEHICLE = 6;
  private GlVehicle queryVehicle;
  private int year = 0;
  private String make;
  private String model;
  private String bodyType;
  private String transmission;
  private String cylinders;
  private int lookup;
  private Collection<GlVehicle> vehicleList;
  private Collection<ListPair> makeList;
  private Collection<String> modelList;
  private Collection<ListPair> bodyTypeList;
  private Collection<ListPair> transmissionList;
  private Collection<String> cylinderList;

  @Override
  public String execute() throws Exception {
    Calendar currentCalendar = Calendar.getInstance();
    currentCalendar.setTime(new Date());

    if (this.getYear() > currentCalendar.get(Calendar.YEAR) || this.getYear() < 1900) {
      return SUCCESS;
    }

    this.setUp();

    this.setActionName(ActionContext.getContext().getName());

    try {
      // set whatever attributes are available on our query queryVehicle
      queryVehicle = new GlVehicle();
      queryVehicle.setVehYear(getYear());
      queryVehicle.setMake(getMake());
      queryVehicle.setModel(getModel());
      queryVehicle.setBodyType(getBodyType());
      queryVehicle.setTransmission(getTransmission());
      queryVehicle.setCylinders(getCylinders());

      if (getLookup() == MAKE) {
        setMakeList(glassesMgr.getMakes(queryVehicle));
      } else if (getLookup() == MODEL) {
        setModelList(glassesMgr.getModels(queryVehicle));
      } else if (getLookup() == BODY_TYPE) {
        setBodyTypeList(glassesMgr.getBodyTypes(queryVehicle));
      } else if (getLookup() == TRANSMISSION) {
        setTransmissionList(glassesMgr.getTransmissionTypes(queryVehicle));
      } else if (getLookup() == CYLINDERS) {
        setCylinderList(glassesMgr.getCylinders(queryVehicle));
      } else if (getLookup() == VEHICLE) {
        setVehicleList(glassesMgr.getList(queryVehicle, new DateTime()));
      }

      return SUCCESS;
    } catch (SystemException e) {
      Config.logger.error(Config.key + ": An Error occurred accessing glasses guide data: ", e);
      return ERROR;
    }
  }

  /**
   * @return the queryVehicle
   */
  public GlVehicle getVehicleQuery() {
    return getQueryVehicle();
  }

  /**
   * @param queryVehicle the queryVehicle to set
   */
  public void setVehicleQuery(GlVehicle vehicle) {
    this.setQueryVehicle(vehicle);
  }

  /**
   * @return the year
   */
  public int getYear() {
    return year;
  }

  /**
   * @param year the year to set
   */
  public void setYear(int year) {
    this.year = year;
  }

  /**
   * @return the make
   */
  public String getMake() {
    return make;
  }

  /**
   * @param make the make to set
   */
  public void setMake(String make) {
    this.make = make;
  }

  /**
   * @return the model
   */
  public String getModel() {
    return model;
  }

  /**
   * @param model the model to set
   */
  public void setModel(String model) {
    this.model = model;
  }

  /**
   * @return the bodyType
   */
  public String getBodyType() {
    return bodyType;
  }

  /**
   * @param bodyType the bodyType to set
   */
  public void setBodyType(String bodyType) {
    this.bodyType = bodyType;
  }

  /**
   * @return the transmission
   */
  public String getTransmission() {
    return transmission;
  }

  /**
   * @param transmission the transmission to set
   */
  public void setTransmission(String transmission) {
    this.transmission = transmission;
  }

  /**
   * @return the cylinders
   */
  public String getCylinders() {
    return cylinders;
  }

  /**
   * @param cylinders the cylinders to set
   */
  public void setCylinders(String cylinders) {
    this.cylinders = cylinders;
  }

  /**
   * @return the queryVehicle
   */
  public GlVehicle getQueryVehicle() {
    return queryVehicle;
  }

  /**
   * @param queryVehicle the queryVehicle to set
   */
  public void setQueryVehicle(GlVehicle queryVehicle) {
    this.queryVehicle = queryVehicle;
  }

  /**
   * @return the makeList
   */
  public Collection<ListPair> getMakeList() {
    return makeList;
  }

  /**
   * @param makeList the makeList to set
   */
  public void setMakeList(Collection<ListPair> makeList) {
    this.makeList = makeList;
  }

  /**
   * @return the modelList
   */
  public Collection<String> getModelList() {
    return modelList;
  }

  /**
   * @param modelList the modelList to set
   */
  public void setModelList(Collection<String> modelList) {
    this.modelList = modelList;
  }

  /**
   * @return the bodyTypeList
   */
  public Collection<ListPair> getBodyTypeList() {
    return bodyTypeList;
  }

  /**
   * @param bodyTypeList the bodyTypeList to set
   */
  public void setBodyTypeList(Collection<ListPair> bodyTypeList) {
    this.bodyTypeList = bodyTypeList;
  }

  /**
   * @return the transmissionList
   */
  public Collection<ListPair> getTransmissionList() {
    return transmissionList;
  }

  /**
   * @param transmissionList the transmissionList to set
   */
  public void setTransmissionList(Collection<ListPair> transmissionList) {
    this.transmissionList = transmissionList;
  }

  /**
   * @return the cylinderList
   */
  public Collection<String> getCylinderList() {
    return cylinderList;
  }

  /**
   * @param cylinderList the cylinderList to set
   */
  public void setCylinderList(Collection<String> cylinderList) {
    this.cylinderList = cylinderList;
  }

  /**
   * @return the lookup
   */
  public int getLookup() {
    return lookup;
  }

  /**
   * @param lookup the lookup to set
   */
  public void setLookup(int lookup) {
    this.lookup = lookup;
  }

  /**
   * @return the vehicleList
   */
  public Collection<GlVehicle> getVehicleList() {
    return vehicleList;
  }

  /**
   * @param vehicleList the vehicleList to set
   */
  public void setVehicleList(Collection<GlVehicle> vehicleList) {
    this.vehicleList = vehicleList;
  }
  }
