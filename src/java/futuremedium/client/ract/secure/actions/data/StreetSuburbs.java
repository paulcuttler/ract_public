package futuremedium.client.ract.secure.actions.data;

import java.util.Collection;

import com.opensymphony.xwork2.ActionContext;
import com.ract.common.GenericException;
import com.ract.common.StreetSuburbVO;

import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Queries Street/Subutb data based on a combination of parameters.
 * Returns data as JSON.
 * @author gnewton
 */
public class StreetSuburbs extends RactActionSupport {

  private String postcode;
  private String suburb;
  private String street;
  private Collection<StreetSuburbVO> streetSuburbList;

  @Override
  @SuppressWarnings("unchecked")
  public String execute() throws Exception {
    setUp();
    this.setActionName(ActionContext.getContext().getName());

    try {

      if (getStreet() != null && !getStreet().isEmpty() && getPostcode() != null && !getPostcode().isEmpty()) {
        setStreetSuburbList((Collection<StreetSuburbVO>) addressMgr.getStreetSuburbsByStreet(getStreet(), getPostcode()));
      } else if (getSuburb() != null && !getSuburb().isEmpty() && getPostcode() != null && !getPostcode().isEmpty()) {
        setStreetSuburbList((Collection<StreetSuburbVO>) addressMgr.getStreetSuburbsBySuburb(getSuburb(), getPostcode()));
      } else if (getPostcode() != null && !getPostcode().isEmpty()) {
        setStreetSuburbList((Collection<StreetSuburbVO>) addressMgr.getStreetSuburbsByPostcode(getPostcode()));
      }

      return SUCCESS;
    } catch (GenericException e) {
      this.setActionMessage("An Error occurred: " + e.getMessage());
      return ERROR;
    }
  }

  /**
   * @return the postcode
   */
  public String getPostcode() {
    return postcode;
  }

  /**
   * @param postcode the postcode to set
   */
  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  /**
   * @return the suburb
   */
  public String getSuburb() {
    return suburb;
  }

  /**
   * @param suburb the suburb to set
   */
  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  /**
   * @return the street
   */
  public String getStreet() {
    return street;
  }

  /**
   * @param street the street to set
   */
  public void setStreet(String street) {
    this.street = street;
  }

  /**
   * @return the streetSuburbList
   */
  public Collection<StreetSuburbVO> getStreetSuburbList() {
    return streetSuburbList;
  }

  /**
   * @param streetSuburbList the streetSuburbList to set
   */
  public void setStreetSuburbList(Collection<StreetSuburbVO> streetSuburbList) {
    this.streetSuburbList = streetSuburbList;
  }
}
