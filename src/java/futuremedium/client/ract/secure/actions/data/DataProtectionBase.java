package futuremedium.client.ract.secure.actions.data;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;

/**
 * Base class to prevent Data actions from being accessed without a current session.
 * @author newtong
 *
 */
public class DataProtectionBase extends RactActionSupport {

	private String us;
	
	public void validate() {
		if (this.getUs() == null || this.getUs().isEmpty() 
				|| this.getUserMgr().getUserSession(this.getUs()) == null) {
    	Config.logger.error(Config.key + ": An Error occurred accessing " + this.getClass().getSimpleName() + ": no session found: " + this.getUs());
    	this.addFieldError("us", "");
    	
    } else if (!this.getHttpRequest().getSession().getId().equals(this.getUs())) {
    	Config.logger.error(Config.key + ": An Error occurred accessing " + this.getClass().getSimpleName() + ": user session does not match");
    	this.addFieldError("us", "");    	
    }
	}

	public String getUs() {
		return us;
	}

	@RequiredStringValidator
	public void setUs(String us) {
		this.us = us;
	}	
}
