package futuremedium.client.ract.secure.interceptors;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.StrutsStatics;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * Prevents browser from caching pages.
 * @author newtong
 *
 */
public class DisableCache extends AbstractInterceptor {

	public String intercept(ActionInvocation invocation) throws Exception {

		final ActionContext context =  invocation.getInvocationContext();
		
    // prevent caching
    HttpServletResponse response = (HttpServletResponse) context.get(StrutsStatics.HTTP_RESPONSE);
    if (response != null) {
    	response.setHeader("Cache-control", "no-cache, no-store");
    	response.setHeader("Pragma", "no-cache");
    	response.setHeader("Expires", "-1");
    	
    	// allow framing by RACT Facebook page only
    	response.setHeader("X-FRAME-OPTIONS", "allow-from https://www.facebook.com/RACTOfficial/app_130661127138736");
    }
    
    return invocation.invoke();
	}
}
