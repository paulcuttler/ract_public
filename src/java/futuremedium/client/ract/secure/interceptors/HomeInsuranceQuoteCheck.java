package futuremedium.client.ract.secure.interceptors;

import java.util.Calendar;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import futuremedium.client.ract.secure.Config;

import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.actions.insurance.home.*;

/**
 * Interceptor to ensure user can only view Home Insurance pages in correct order.
 * Checks to ensure that aborted quotes cannot be resumed.
 * @author gnewton
 */
public class HomeInsuranceQuoteCheck extends AbstractInterceptor {

  @Override
  public String intercept(ActionInvocation invocation) throws Exception {

    Object actionObject = invocation.getAction();
    if (!(actionObject instanceof ActionBase)) { // fall through
      Config.logger.debug(Config.key + ": action: " + actionObject.getClass().getSimpleName() + " is not an instance of ActionBase, skipping.");
      return invocation.invoke();
    }

    ActionBase action = (ActionBase) actionObject;

    // trying to access page with no quoteNo
    if (action.getQuoteNo() == 0 && (action instanceof PersistanceAware)) {
      Config.logger.debug(Config.key + ": no quoteNo defined, returning to landing page.");
      invocation.getStack().set("nextAction", action.getHomeService().getFlow().get(0));
      return RactActionSupport.ACTION_REDIRECT;
    }

    // test user is not trying to go beyond current place in system
    String lastActionName = action.getHomeService().retrieveQuoteDetail(action.getQuoteNo(), ActionBase.FIELD_LAST_ACTION);
    if (lastActionName != null && !lastActionName.isEmpty()) {

      String nextActionName = action.getHomeService().calculateNextAction(action);

      if (nextActionName != null && !nextActionName.isEmpty()) {
        Config.logger.debug(Config.key + ": currentAction = " + action.getCurrentActionName() + ", lastAction = " + lastActionName + ", nextAction = " + nextActionName);

        // the currently requested Action's name
        String currentFormActionName = action.getCurrentFormActionName();
        int currentActionIndex = action.getHomeService().getFlow().indexOf(currentFormActionName);
        int nextActionIndex = action.getHomeService().getFlow().indexOf(nextActionName);

        Config.logger.debug(Config.key + ": currentActionIndex: " + currentActionIndex + ", nextActionIndex: " + nextActionIndex);

        if (currentActionIndex > nextActionIndex) {
          Config.logger.info(Config.key + ": currentAction is beyond nextAction, redirecting to: " + nextActionName);
          invocation.getStack().set("nextAction", nextActionName);
          return RactActionSupport.ACTION_REDIRECT;
        }
      }
    }

    // check system hasn't aborted quote if we are trying to access a page other than New/Retrieve quote
    String newQuoteFormActionName = ActionBase.FORM_ACTION_PREFIX + NewQuoteForm.class.getSimpleName();
    String retrieveQuoteFormActionName = ActionBase.FORM_ACTION_PREFIX + RetrieveQuoteForm.class.getSimpleName();
    String saveQuoteFormActionName = ActionBase.FORM_ACTION_PREFIX + SaveQuoteForm.class.getSimpleName();
    if (action.isAborted()
            && !action.getCurrentFormActionName().equals(newQuoteFormActionName)
            && !action.getCurrentFormActionName().equals(retrieveQuoteFormActionName)) {
      action.setActionMessage(Config.configManager.getProperty("insurance.home.retrieve.aborted.message"));
      return RactActionSupport.NOTICE;
    }

    // check that user is not accessing a page prior to PremiumOptionsForm after viewing premium (except New Quote, Retrieve Quote or Save Quote)
    boolean viewedPremium = action.getHomeService().hasViewedPremium(action.getQuoteNo());
    
    if (viewedPremium
          && !action.getCurrentFormActionName().equals(newQuoteFormActionName)
          && !action.getCurrentFormActionName().equals(retrieveQuoteFormActionName)
          && !action.getCurrentFormActionName().equals(saveQuoteFormActionName)) {
      String currentFormActionName = action.getCurrentFormActionName();
      int currentActionIndex = action.getHomeService().getFlow().indexOf(currentFormActionName);

      String premiumOptionsFormActionName = ActionBase.FORM_ACTION_PREFIX + PremiumOptionsForm.class.getSimpleName();
      int premiumOptionsIndex = action.getHomeService().getFlow().indexOf(premiumOptionsFormActionName);

      if (currentActionIndex != -1 && currentActionIndex < premiumOptionsIndex) {
        Config.logger.info(Config.key + ": currentAction (" + currentFormActionName + ") is prior to PremiumOptionsForm (and premium has been viewed), redirecting to: " + premiumOptionsFormActionName);
        invocation.getStack().set("nextAction", premiumOptionsFormActionName);
        return RactActionSupport.ACTION_REDIRECT;
      }
    }

    // check user is allowed to access action (Quote area only)
    boolean finalisedQuote = action.getHomeService().hasFinalisedQuote(action.getQuoteNo());
    if (finalisedQuote) {
      if (action instanceof QuoteAware && !(action instanceof QuoteFinalisedAware)) {
        action.setActionMessage("You are not permitted to access that page.");
        Config.logger.info(Config.key + ": Home Quote #" + action.getQuoteNo() + " hasFinalisedQuote=true and action: " + actionObject.getClass().getSimpleName() + " is QuoteAware, exiting.");

        return RactActionSupport.NOTICE;
      }
    }

    // check user is allowed to access action (Fulfillment area only)
    boolean finalisedPolicy = action.getHomeService().hasFinalisedPolicy(action.getQuoteNo());
    if (finalisedPolicy) {
      if ((action instanceof QuoteAware && !(action instanceof QuoteFinalisedAware)) || (action instanceof PolicyAware && !(action instanceof PolicyFinalisedAware))) {
        action.setActionMessage("You are not permitted to access that page.");
        Config.logger.info(Config.key + ": Home Quote #" + action.getQuoteNo() + " hasFinalisedPolicy=true and action: " + actionObject.getClass().getSimpleName() + " is PolicyAware, exiting.");

        return RactActionSupport.NOTICE;
      }
    }

    // record time taken to complete each page.
    if (action.getCurrentActionName().contains(ActionBase.FORM_ACTION_SUFFIX)) { // View page
    	// start timer
    	action.setStartTime(Calendar.getInstance().getTimeInMillis());
    } else { // Save page
    	// end timer
    	if (action.getStartTime() != null) {
	    	long formTimeMillis = Calendar.getInstance().getTimeInMillis() - action.getStartTime();
	    	String formTimeSecs = Long.toString(formTimeMillis / 1000);
	    	action.getHomeService().storeQuoteDetail(action, "Time_" + action.getCurrentActionName(), formTimeSecs);
    	}
    }
    
    return invocation.invoke();
  }
}
