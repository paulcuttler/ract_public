/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package futuremedium.client.ract.secure.interceptors;

import java.util.*;

import com.opensymphony.xwork2.ActionInvocation;

import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;
import org.json.JSONObject;


/**
 *
 * @author pcock
 */
public class JSONfeed extends AbstractInterceptor {

  public static final String CAMPAIGN_JSON = "campaignJSON";


  @Override
  @SuppressWarnings("unchecked")
  public String intercept(ActionInvocation invocation) throws Exception {

    Map application = invocation.getInvocationContext().getApplication();

    if (invocation.getInvocationContext().getParameters().containsKey("updateTheme") || !application.containsKey(CAMPAIGN_JSON)) {
      application.put(CAMPAIGN_JSON, this.getJSONFromURL(Config.configManager.getProperty("ract.secure.user.campaignJSON.url")));
    }

    return invocation.invoke();
  }


  @SuppressWarnings("unchecked")
  private JSONObject getJSONFromURL(String path) {

    Config.logger.debug(Config.key + ": initialising JSON feed in JSONfeed interceptor");

    String html = RactActionSupport.retrieveHtml(path, "");

    if (html != null) {
      try {
        JSONObject json = new JSONObject(html);
        return json;

      } catch (Exception e) {
        Config.logger.debug(Config.key + ": can not create JSON object in JSONfeed interceptor: " + e);
      }
    }

    return null;
  }

}
