package futuremedium.client.ract.secure.interceptors;

import java.util.*;
import java.util.regex.*;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.interceptor.*;

import com.ract.web.client.ClientTransaction;
import com.ract.web.security.User;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;


/**
 * Setup HTML components used in rendering output.
 * @author gnewton
 */
public class PageSetup extends AbstractInterceptor {

  static final public String SHELL_USER = "User";
  static final public String SHELL_INSURANCE = "Insurance";
  static final public String SHELL_MEMBERSHIP = "Membership";
  static final public String SHELL_TIPPING = "Tipping";

  @Override
  @SuppressWarnings("unchecked")
  public String intercept(ActionInvocation invocation) throws Exception {

    Map application = invocation.getInvocationContext().getApplication();
    Map session = invocation.getInvocationContext().getSession();

    String actionName = ActionContext.getContext().getName();

    String shellKey = getShellKey(actionName, session);
    application.put("shellKey", shellKey);

    if (invocation.getInvocationContext().getParameters().containsKey("resetSkin")
            || (shellKey.equals(SHELL_INSURANCE) && !application.containsKey("insuranceShell"))
            || (shellKey.equals(SHELL_MEMBERSHIP) && !application.containsKey("membershipShell"))
            || (shellKey.equals(SHELL_TIPPING) && !application.containsKey("tippingShell"))
            || (shellKey.equals(SHELL_USER) && !application.containsKey("userShell"))) {

      initHtmlComponents(application, shellKey);
    }

    rewriteRequestComponents(application, session, actionName, shellKey);

    application.put("fakeNs", Config.configManager.getProperty("ract.secure.fakeNamespace"));

    return invocation.invoke();
  }

  /**
   * Determine the shell to lookup based on the actionName provided.
   * @param actionName
   * @return
   */
  private String getShellKey(String action, Map session) {
    // default
    String shellKey = SHELL_USER;

    User user = (User) session.get(RactActionSupport.USER_TOKEN);
    ClientTransaction customer = null;
    if (user != null) {
      customer = (ClientTransaction) session.get(RactActionSupport.CUSTOMER_TOKEN);
    }

    if (customer != null) {
      shellKey = SHELL_USER;
    } else if (user != null) {
      shellKey = SHELL_TIPPING;
    }

    if (action.contains("Quote")) {
      shellKey = SHELL_INSURANCE;
    } else if (action.startsWith("Join")) {// || action.startsWith("UserSignup")) {
      shellKey = SHELL_MEMBERSHIP;
    } else if (action.contains("TippingSignup") || action.contains("TippingCreate")) {
      shellKey = SHELL_TIPPING;
    }

    return shellKey;
  }

  /**
   * Rewrite HTML components based on current request.
   * @param application application-scope Map.
   * @param session session-scope Map.
   * @param actionName name of the executing action.
   * @param shellKey lookup for shell.
   */
  @SuppressWarnings("unchecked")
  private void rewriteRequestComponents(Map application, Map session, String actionName, String shellKey) {
    // rewrite components based on logged-in state
    String midHeader = (String) application.get("midHeader_" + shellKey);

    if (midHeader != null) {
      // test if user is logging out
      if (actionName.contains("Logout")) {
        session.put(RactActionSupport.USER_TOKEN, null);
        session.remove(RactActionSupport.USER_TOKEN);
      }
      // change class based on logged-in status
      if (session.containsKey(RactActionSupport.USER_TOKEN) && session.get(RactActionSupport.USER_TOKEN) != null) {
        // inject username
        User user = (User) session.get(RactActionSupport.USER_TOKEN);
        String theUsername = user.getUserName();
        ClientTransaction customer = null;
        if (user != null) {
          customer = (ClientTransaction) session.get(RactActionSupport.CUSTOMER_TOKEN);
        }
        if (customer != null) {
          theUsername = customer.getDisplayName();
        } else {
          theUsername = user.getUserName();
        }

        midHeader = Pattern.compile("<body class=\"\" id=\"body\">", Pattern.MULTILINE | Pattern.DOTALL).matcher(midHeader).replaceAll("<body class=\"loggedIn\" id=\"body\">");
        midHeader = Pattern.compile("<!-- begin loggedInUser -->(.*?)<!-- end loggedInUser -->", Pattern.MULTILINE | Pattern.DOTALL).matcher(midHeader).replaceAll("<!-- begin loggedInUser -->Logged in as <strong>" + theUsername + "</strong><!-- end loggedInUser -->");

      } else {
        midHeader = Pattern.compile("<body class=\"loggedIn\" id=\"body\">", Pattern.MULTILINE | Pattern.DOTALL).matcher(midHeader).replaceAll("<body class=\"\" id=\"body\">");
        midHeader = Pattern.compile("<!-- begin loggedInUser -->(.*?)<!-- end loggedInUser -->", Pattern.MULTILINE | Pattern.DOTALL).matcher(midHeader).replaceAll("<!-- begin loggedInUser --><!-- end loggedInUser -->");
      }
      application.put("midHeader_" + shellKey, midHeader);
    }

    // rewrite navigation selection based on actionName
    if (application.containsKey("sideNav_" + shellKey)) {
      // strip 'selected' from side nav items as they represent selected state of shell page
      application.put("sideNav_" + shellKey, Pattern.compile("\\bselected\\b", Pattern.MULTILINE | Pattern.DOTALL).matcher((String) application.get("sideNav_" + shellKey)).replaceAll(""));

      // compare with current actionName
      application.put("sideNav_" + shellKey, Pattern.compile("selectLater" + actionName + ".action", Pattern.MULTILINE | Pattern.DOTALL).matcher((String) application.get("sideNav_" + shellKey)).replaceAll("selectLater" + actionName + ".action selected"));
    }

  }

  /**
   * Initialise html chunks and store in application-scope.
   * @param application application-scope Map.
   * @param shellKey lookup for shell.
   */
  @SuppressWarnings("unchecked")
  private void initHtmlComponents(Map application, String shellKey) {
    Config.logger.debug(Config.key + ": initialising HTML components");

    String path = Config.configManager.getProperty("ract.secure.user.shell.url");

    if (shellKey.equals(SHELL_INSURANCE)) {
      application.put("insuranceShell", true);
      path = Config.configManager.getProperty("ract.secure.insurance.shell.url");
    } else if (shellKey.equals(SHELL_MEMBERSHIP)) {
      application.put("membershipShell", true);
      path = Config.configManager.getProperty("ract.secure.membership.shell.url");
    } else if (shellKey.equals(SHELL_TIPPING)) {
      application.put("tippingShell", true);
      path = Config.configManager.getProperty("ract.secure.tipping.shell.url");
    } else if (shellKey.equals(SHELL_USER)) {
      application.put("userShell", true);
    }

    String html = RactActionSupport.retrieveHtml(path, "");

    if (html != null) {

      // rewrite URLs for images, styles, assets etc.
      String urlRewriteFrom = Config.configManager.getProperty("ract.secure.urlRewriteFrom");
      String urlRewriteTo = Config.configManager.getProperty("ract.secure.urlRewriteTo");
      String publicBaseUrl = Config.configManager.getProperty("ract.public.baseUrl");
      if (!urlRewriteFrom.equals(urlRewriteTo)) {
        html = Pattern.compile("\"" + urlRewriteFrom + "assets/", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("\"" + urlRewriteTo + "assets/");
        html = Pattern.compile("\"" + urlRewriteFrom + "images/", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("\"" + urlRewriteTo + "images/");
        html = Pattern.compile("\"" + urlRewriteFrom + "scripts/", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("\"" + urlRewriteTo + "scripts/");
        html = Pattern.compile("\"" + urlRewriteFrom + "styles/", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("\"" + urlRewriteTo + "styles/");
        html = Pattern.compile("\"" + urlRewriteFrom + "FM_common/", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("\"" + urlRewriteTo + "FM_common/");
        html = Pattern.compile("href=\"" + urlRewriteFrom, Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("href=\"" + publicBaseUrl);
        html = Pattern.compile("var _BASE_HREF_ = \"" + urlRewriteFrom, Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("var _BASE_HREF_ = \"" + urlRewriteTo);
        html = Pattern.compile("action=\"" + urlRewriteFrom, Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("action=\"" + urlRewriteTo);
      }

      // replace href and actionName attributes
      //html = Pattern.compile("a href=\"" + urlRewriteTo, Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("a href=\"" + Config.configManager.getProperty("ract.public.prefix"));
      //html = Pattern.compile("action=\"" + urlRewriteTo, Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("action=\"" + Config.configManager.getProperty("ract.public.prefix"));

      // make external references to images (and javascript) https
      if (Config.configManager.getProperty("ract.secure.transformHttp", "false").equals("true")) {
        html = Pattern.compile("src=\"http:", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("src=\"https:");
        // fix reference to flash import
        html = Pattern.compile("data:\"http:", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("data:\"https:");
        // fix CMS panel style references
        html = Pattern.compile("background-image: url\\('http:", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("background-image: url\\('https:");
      }

      // adjust title and metadata
      html = Pattern.compile("<title>(.*?)</title>", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("<title>RACT : " + Config.configManager.getProperty("ract.secure.metadata." + shellKey + ".title") + "</title>");
      html = Pattern.compile("<meta name=\"description\" content=\"(.*?)\" />", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("<meta name=\"description\" content=\"" + Config.configManager.getProperty("ract.secure.metadata." + shellKey + ".description") + "\" />");
      html = Pattern.compile("<meta name=\"keywords\" content=\"(.*?)\" />", Pattern.MULTILINE | Pattern.DOTALL).matcher(html).replaceAll("<meta name=\"keywords\" content=\"" + Config.configManager.getProperty("ract.secure.metadata." + shellKey + ".keywords") + "\" />");

      Matcher topHeader = Pattern.compile("(.*?)<!-- end before head -->", Pattern.MULTILINE | Pattern.DOTALL).matcher(html);
      if (topHeader.find()) {
        String header = topHeader.group(1);
        application.put("topHeader_" + shellKey, header);
      }

      Matcher midHeader = Pattern.compile("<!-- end before head -->(.*?)<!-- begin login -->", Pattern.MULTILINE | Pattern.DOTALL).matcher(html);
      if (midHeader.find()) {
        String header = midHeader.group(1);
        application.put("midHeader_" + shellKey, header);
      }

      Matcher bottomHeader = Pattern.compile("<!-- end login -->(.*?)<!-- end header -->", Pattern.MULTILINE | Pattern.DOTALL).matcher(html);
      if (bottomHeader.find()) {
        application.put("bottomHeader", bottomHeader.group(1));
      }

      Matcher footer = Pattern.compile("<!-- start footer -->(.*)", Pattern.MULTILINE | Pattern.DOTALL).matcher(html);
      if (footer.find()) {
        application.put("footer", footer.group(1));
      }

      Matcher showHeading = Pattern.compile("<!-- start showHeading -->(.*?)<!-- end showHeading -->", Pattern.MULTILINE | Pattern.DOTALL).matcher(html);
      if (showHeading.find()) {
        application.put("showHeading_" + shellKey, showHeading.group(1));
      }

      Matcher quickLinks = Pattern.compile("<!-- start quickLinks -->(.*?)<!-- end quickLinks -->", Pattern.MULTILINE | Pattern.DOTALL).matcher(html);
      if (quickLinks.find()) {
        application.put("quickLinks_" + shellKey, quickLinks.group(1));
      }

      Matcher sidePanels = Pattern.compile("<!-- start sidePanels -->(.*?)<!-- end sidePanels -->", Pattern.MULTILINE | Pattern.DOTALL).matcher(html);
      if (sidePanels.find()) {
        application.put("sidePanels_" + shellKey, sidePanels.group(1));
      }

      Matcher sideNav = Pattern.compile("<!-- start sideNav -->(.*?)<!-- end sideNav -->", Pattern.MULTILINE | Pattern.DOTALL).matcher(html);
      if (sideNav.find()) {
        application.put("sideNav_" + shellKey, sideNav.group(1));
      }
    }

  }

}