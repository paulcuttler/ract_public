package futuremedium.client.ract.secure.interceptors;

import java.util.*;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.interceptor.*;
import com.opensymphony.xwork2.config.entities.*;
import futuremedium.client.ract.secure.Config;

/**
 * Implement a two-step view [Fowler 365].
 *
 * By default, Struts will allow you to designate the page to render, but in a two-step view, the page is usually the same and instead
 * we want to select the content area to render.
 * This takes an 'includePage' parameter defined in the &ltresult&gt; mapping and makes it available in the valueStack where it can be 'included'
 * by the original page pointed to by the original 'location' parameter.
 */
public class TwoStepView extends AbstractInterceptor {

  @SuppressWarnings("unchecked")
  @Override
  public String intercept(ActionInvocation invocation) throws Exception {

    // A method that will be called after action before result
    invocation.addPreResultListener(new PreResultListener() {
      @Override
      public void beforeResult(ActionInvocation actionInvocation, String resultCode) {
        Action action = (Action) actionInvocation.getAction();
        try {
          Map<String, ResultConfig> resultsMap  = actionInvocation.getProxy().getConfig().getResults();
          ResultConfig finalResultConfig = resultsMap.get(resultCode);
          Map resultParamsMap = finalResultConfig.getParams();

          String includeView = (String) resultParamsMap.get("includeView");
          if (includeView != null) {
            actionInvocation.getStack().set("includeView", includeView);
          }

        } catch (Exception e) {
          Config.logger.error(Config.key + ": TwoStepView exception from Action: " + actionInvocation.getProxy().getActionName(), e);
        }
      }
    });

    return invocation.invoke();
  }
}