package futuremedium.client.ract.secure.interceptors;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.json.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.ract.common.SystemException;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.InvalidKeyStore;
import futuremedium.client.ract.secure.SecretsManager;

public class RecaptchaCheck extends AbstractInterceptor {
	
	private static final String EMPTY_STRING = "";
	private String BACK = "back";
	private String RECAPTCHA_PROBLEM = "There was a problem processing the recaptcha, please try again.";
	
	/**
	 * Verify that a user has correctly checked the google recaptcha chekcbox 
	 * and is indeed not a bot
	 * @author Sam Duke
	 */
	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		ActionContext context = invocation.getInvocationContext();
		Map<String, Object> parameters = context.getParameters();
		Map<String, Object> session = invocation.getInvocationContext().getSession(); 
		
		//Clear the error message for the recaptcha, it will be regenerated if there is another error
		session.remove("RECAPTCHA_MESSAGE");
		
		//Get the recaptcha secret 
		String recaptchaSecret = this.loadRecaptchaSecret();
		
		//Get the google+user generated token for confirmation
		String recaptchaResponse = StringUtils.join((String[]) parameters.get("g-recaptcha-response"), "");
		
		//Get the users IP address
		String recaptchaIp = ServletActionContext.getRequest().getRemoteAddr();
		
		/*
		 * Make POST request to google - APPS-275 Replace direct call with Mule API to perfrom second verify step
		 * 
		 * History
		 * =======
		 * As of 25/7/2020, Google changed its HTPPS certiciates, dropping support for the deprecated TLS 1.0
		 * This removed the ability of a Java 1.6 app to request a TLS connection for a recaptcha verify session.
		 * This has forced moving this request upstream to Mule to handle the verify request and return the recaptcha response.
		 * The response is then handled as per the direct request.
		 * 
		 * GS 27/7/2020
		 */
		try {
			String requestBody = String.format("secret=%s&response=%s&remoteIp=%s", 
				     recaptchaSecret, 
				     recaptchaResponse,
				     recaptchaIp);

			String reCaptchaResponse = EMPTY_STRING;

			// Perform the POST - any exception is propogated to the caller
			PostMethod post = new PostMethod("http://mule-worker-internal-google-recaptcha-verify.au-s1.cloudhub.io:8091/verify"); // The same for all environments  - also temp fix awaiting PMP
			try {
				// Create the request
				StringRequestEntity requestEntity = new StringRequestEntity(requestBody);
				
				// Set the body
				post.setRequestEntity(requestEntity);
				
				// Set the encoded form header
				post.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
				
				// Create the client
				HttpClient httpclient = new HttpClient();

				// Excecute the verify method
				int result = httpclient.executeMethod(post);

				// If result not 200 throw an exception
				if (result != 200)
					throw new Exception("Server problem - result code is problematic (not 200): " + result);

				// Get the response body
				reCaptchaResponse = post.getResponseBodyAsString();
			} catch (Exception ex){
				System.out.println(ex.getMessage());
			} finally {
				// Release the HTTP connection
				post.releaseConnection();
			}
			
			if (!reCaptchaResponse.equals(EMPTY_STRING)) {
				JSONObject jResponse = new JSONObject(reCaptchaResponse);
				boolean success = jResponse.getBoolean("success");
				
				if(success) {
					Config.logger.info("This user is not a bot: " + recaptchaIp);
					return invocation.invoke();
				} else {
					session.put("RECAPTCHA_MESSAGE", RECAPTCHA_PROBLEM);
					Config.logger.warn("This user is probably a bot: " + recaptchaIp);
					return BACK;
				}
			} else {
				session.put("RECAPTCHA_MESSAGE", RECAPTCHA_PROBLEM);
				Config.logger.warn("This user is probably a bot: " + recaptchaIp);
				return BACK;
			}
			
		} catch(MalformedURLException e) {
			session.put("RECAPTCHA_MESSAGE", RECAPTCHA_PROBLEM);
			return BACK;
		} catch(IOException e) {
			session.put("RECAPTCHA_MESSAGE", RECAPTCHA_PROBLEM);
			return BACK;
		}
	}
	
	private String loadRecaptchaSecret() throws SystemException {
		String password = null;
		SecretsManager sm;
		
		try {
			sm = SecretsManager.getInstance();
			password = sm.getSecret("recaptcha");
		} catch (InvalidKeyStore e) {
			throw new SystemException("Unable to load the recaptcha secret", e);
		}
				
		return password;
	}

}