package futuremedium.client.ract.secure.interceptors;

import java.util.*;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.actions.insurance.car.*;
import futuremedium.client.ract.secure.entities.insurance.car.*;

/**
 * This interceptor runs before all insurance save actions. It prevents people to call save actions directly.
 * Only first save action is allowed in this case.
 * @author xmfu
 */
public class InsuranceQuoteCheck extends AbstractInterceptor {
    private static final long serialVersionUID = -2308973563063119165L;

@Override
  public String intercept(ActionInvocation invocation) throws Exception {
    Map<String, Object> session = invocation.getInvocationContext().getSession();

    String pageId = invocation.getStack().findString("pageId");
    Object actionObject = invocation.getAction();

    QuotePageSave action = (QuotePageSave) actionObject;
    
    if (pageId != null) {
      if (pageId.equals("1") || pageId.equals("0")) {
        return invocation.invoke();
      }
      
      if (action.getQuoteNo() != null) {
        
        String messagePage = action.getInsuranceMgr().getQuoteDetail(action.getQuoteNo(), QuotePageSave.MESSAGE_PAGE);
        int messageId;
        
        if(messagePage != null ) {
          messageId = Integer.parseInt(messagePage);

          String message = QuotePageSave.lookupMessageTitle(messageId);
          Config.logger.info(Config.key + ": Car quote is aborted: " + message);

          invocation.getStack().set("message", message);
          return QuotePageSave.MESSAGE_PAGE;
        }
      }
      

      if (pageId.equals("2")) {
        if (session.get(QuotePageSave.QUOTE_NUMBER) != null) {
     	  // start timer - first page we know about a quoteNo
          action.setStartTime(Calendar.getInstance().getTimeInMillis());
          
          return invocation.invoke();
        } else if (session.get(QuotePageSave.NEW_QUOTE) != null && (Boolean) session.get(QuotePageSave.NEW_QUOTE)) {
          return invocation.invoke();
        } else {
          return this.goToPage1(invocation);
        }
      } else {

        // ensure that the user cannot skip pages of the quote
        if (action.getQuoteNo() != null) {
          String lastPageId = action.getInsuranceMgr().getQuoteDetail(action.getQuoteNo(), QuotePageSave.FIELD_CURRENT_PAGE_ID);

          int nextPage = Integer.parseInt(lastPageId) + 1;
          int thisPage = Integer.parseInt(pageId);

          // determine the actual pageId being requested based on 'submit' value
          if (invocation.getInvocationContext().getName().equals("QuoteProcess")) {
            String submit = invocation.getStack().findString("submit");
            if (submit != null) {
              if (submit.equals(QuotePageSave.BUTTON_NEXT)) {
                thisPage++;
              } else {
                thisPage--;
              }
            }
          }

          Config.logger.debug(Config.key + ": Car: " + invocation.getInvocationContext().getName() + ", pageId = " + thisPage + ", nextPageId = " + nextPage);

          //this.insuranceMgr.setQuoteDetail(this.getQuoteNo(), MESSAGE_PAGE, Integer.toString(messageId));
          
          
         
           
          
          if (thisPage > nextPage) {
            Config.logger.warn(Config.key + ": Car: current pageId is beyond nextPageId, resetting to: " + nextPage);
            invocation.getStack().set("nextAction", "LoadQuotePage?pageId=" + nextPage);
            return RactActionSupport.ACTION_REDIRECT;
          }
        }
      }
    }

    if (invocation.getStack().findString("submit") == null && !invocation.getInvocationContext().getName().equals("LoadQuotePage")) {
      return this.goToPage1(invocation);
    }

    if (session.get(QuotePageSave.QUOTE_NUMBER) != null) {
    	
    	Config.logger.info(Config.key + ": action=" + action.getCurrentActionName() + ", summary=" + action.getSummary() + ", pageId=" + action.getPageId());
    	
    	// record time taken to complete each page.
		if (action.getCurrentActionName().contains("QuoteProcess")) { // View page
			// start timer
			action.setStartTime(Calendar.getInstance().getTimeInMillis());
		} else if (action.getCurrentActionName().contains("Save")) { // Save page
			// end timer
			if (action.getStartTime() != null) {
		    	long formTimeMillis = Calendar.getInstance().getTimeInMillis() - action.getStartTime();
		    	String formTimeSecs = Long.toString(formTimeMillis / 1000);
		    	action.getInsuranceMgr().setQuoteDetail(action.getQuoteNo(), "Time_" + action.getSummary().replace(" ", "") + "_" + action.getPageId(), formTimeSecs);
			}
		}
    	
      return invocation.invoke();
    } else {
      // If session is expired and quote number is no longer available, return user to the first page.
      return this.goToPage1(invocation);
    }
  }

  public String goToPage1(ActionInvocation invocation) {
    QuoteQuestionManager quoteQuestionManager = new QuoteQuestionManager();
    invocation.getStack().setValue("quotePage", quoteQuestionManager.getPage(1L));
    invocation.getStack().setValue("pageId", 0);
    invocation.getStack().set("submit", "next");

    long totalPage = quoteQuestionManager.getNumberOfPage("1");
    double percentage = 0;

    if (totalPage != 0) {
      percentage = (double) 1 / totalPage;
    }

    if (invocation.getStack().findString("totalPages") == null) {
      invocation.getStack().set("progress", percentage);
      invocation.getStack().set("totalPages", totalPage);
    }

    // Clear the secure quote number.
    Map<String, Object> session = invocation.getInvocationContext().getSession();
    session.put(QuotePageSave.SECURE_QUOTE_NUMBER, null);

    return ActionSupport.SUCCESS;
  }
}