package futuremedium.client.ract.secure.interceptors;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import org.apache.struts2.ServletActionContext;

import futuremedium.client.ract.secure.Config;

/**
 * Verify that remote access is granted.
 * @author gnewton
 */
public class RemoteAccess extends AbstractInterceptor {

  @Override
  public String intercept(ActionInvocation invocation) throws Exception {

    if (Config.configManager.getProperty("ract.public.permit.ips").contains(ServletActionContext.getRequest().getRemoteAddr())) {
      return invocation.invoke();
    } else {
      Config.logger.warn(Config.key + ": Attempt to access remote command by non trusted IP address denied. (" + ServletActionContext.getRequest().getRemoteAddr() + ")");
    }

    return ActionSupport.ERROR;
  }

}