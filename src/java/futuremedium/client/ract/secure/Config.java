package futuremedium.client.ract.secure;

import javax.annotation.*;

import org.apache.log4j.*;

import futuremedium.common2.config.BaseConfigManager;

/**
 * Base Website-specific functionality for loading Configuration data.
 */
public class Config extends BaseConfigManager  {

  /**
   * Convenient reference to this sub-class instance of ConfigManager.
   *
   * This should only be used if there is one instance of this sub-class, which should normally be the case.
   */
  static public Config configManager = null;

  /**
   * Convenient reference to the logger for this sub-class instance of ConfigManager.
   *
   * This should only be used if there is one instance of this sub-class, which should normally be the case.
   */
  static public Logger logger = null;

  /**
   * Convenient reference to the key for this sub-class instance of ConfigManager.
   *
   * This should only be used if there is one instance of this sub-class, which should normally be the case.
   */
  static public String key = null;

  @PostConstruct
  public void initialise() {
    Config.configManager = this;
    Config.logger = this.log;
    Config.key = this.getKey();
  }

  @Override
  @PreDestroy
  public void shutdown() {
    super.shutdown();
    Config.configManager = null;
    Config.logger = null;
  }

}

