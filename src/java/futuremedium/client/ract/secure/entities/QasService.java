package futuremedium.client.ract.secure.entities;

import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.springframework.stereotype.Service;

import com.qas.ws.Address;
import com.qas.ws.AddressLineType;
import com.qas.ws.EngineEnumType;
import com.qas.ws.EngineType;
import com.qas.ws.Fault;
import com.qas.ws.ObjectFactory;
import com.qas.ws.ProWeb;
import com.qas.ws.QAAddressType;
import com.qas.ws.QAGetAddress;
import com.qas.ws.QAPortType;
import com.qas.ws.QASearch;
import com.qas.ws.QASearchResult;
import com.qas.ws.PicklistEntryType;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.data.QasLookup;
import futuremedium.client.ract.secure.actions.insurance.InsuranceBase;
import futuremedium.client.ract.secure.actions.insurance.QasPostalAware;
import futuremedium.client.ract.secure.actions.insurance.QasResidentialAware;
import futuremedium.client.ract.secure.actions.insurance.car.QuotePageSave;
import futuremedium.client.ract.secure.actions.insurance.home.ActionBase;

@Service
public class QasService extends BaseService {
  
  // @todo move to properties file 
  static public final String GNAF_LOT = "G-NAF Allotment (Lot)";
  static public final String GNAF_LOT_NO = "G-NAF Allotment (Number)";
	static public final String GNAF_UNIT = "G-NAF Flat/Unit (Number)";
	static public final String GNAF_STREET_NO = "G-NAF Building number";
	static public final String GNAF_STREET_NAME = "G-NAF Street (Name)";
	static public final String GNAF_STREET_TYPE = "G-NAF Street (Type)";
	static public final String GNAF_SUBURB = "G-NAF Locality";
	static public final String GNAF_STATE = "State code";
	static public final String GNAF_POSTCODE = "G-NAF Postcode";
	static public final String GNAF_LATITUDE = "Address-Level Latitude";
	static public final String GNAF_LONGITUDE = "Address-Level Longitude";
	static public final String GNAF_PID = "G-NAF PID";
	
	static public final String PAF_LOT = "Allotment (Lot)";
  static public final String PAF_LOT_NO = "Allotment (Number)";
	static public final String PAF_UNIT = "Flat/Unit (Number)";
	static public final String PAF_STREET_NO = "Building number";
	static public final String PAF_STREET_NAME = "Street (Name)";
	static public final String PAF_STREET_TYPE = "Street (Type)";
	static public final String PAF_SUBURB = "Locality";
	static public final String PAF_STATE = "State";
	static public final String PAF_POSTCODE = "Postcode";
	static public final String PAF_PID = "DPID/DID";
	static public final String PAF_AUSBAR = "AUSBAR.";
	static public final String PAF_PO_BOX_TYPE = "All postal delivery types (Type)";
	static public final String PAF_PO_BOX_NO = "All postal delivery types (Number)";
  
  static protected final String QAS_LAYOUT_RESIDENTIAL = "RactGnaf";
  static protected final String QAS_LAYOUT_POSTAL = "RactPaf";
	
  static private final String QAS_COUNTRY_RESIDENTIAL = "AUE";
  static private final String QAS_COUNTRY_POSTAL = "AUS";
	
  static private final Integer QAS_RESULT_MAX = 10;	
	
	/**
	 * Populate individual address fields after looking up QAS address by moniker.
	 * @param action
	 * @throws Fault
	 */
	public void populateAddressFields(QasResidentialAware action) throws Fault {

		QAPortType service = (new ProWeb()).getQAPortType();
  	ObjectFactory factory = new ObjectFactory();
  	
  	QAGetAddress search = factory.createQAGetAddress();
  	search.setLayout(QAS_LAYOUT_RESIDENTIAL);
  	search.setMoniker(action.getMoniker());
  	
  	Map<String,String> addressDetails = new HashMap<String,String>();
  	
  	logQasUsage(action);
  	
  	Address address = service.doGetAddress(search);
  	
  	for (AddressLineType line : address.getQAAddress().getAddressLine()) {
  		addressDetails.put(line.getLabel(), line.getLine());
  	}
  	
  	StringBuilder streetNo = new StringBuilder(); 
  	if (!addressDetails.get(GNAF_LOT).isEmpty()) {
  		streetNo.append(addressDetails.get(GNAF_LOT) + " ");
  	}
  	if (!addressDetails.get(GNAF_LOT_NO).isEmpty()) {
  		streetNo.append(addressDetails.get(GNAF_LOT_NO) + " ");
  	}
  	if (!addressDetails.get(GNAF_UNIT).isEmpty()) {
  		streetNo.append("Unit " + addressDetails.get(GNAF_UNIT) + " ");
  	}
  	if (!addressDetails.get(GNAF_STREET_NO).isEmpty()) {
  		streetNo.append(addressDetails.get(GNAF_STREET_NO));
  	}
  	
  	action.setResidentialStreetNumber(streetNo.toString().toUpperCase());
  	action.setResidentialStreet(addressDetails.get(GNAF_STREET_NAME).toUpperCase() + " " + addressDetails.get(GNAF_STREET_TYPE).toUpperCase());
  	action.setResidentialSuburb(addressDetails.get(GNAF_SUBURB).toUpperCase());
  	action.setResidentialPostcode(addressDetails.get(GNAF_POSTCODE));
  	action.setResidentialLatitude(addressDetails.get(GNAF_LATITUDE));
  	action.setResidentialLongitude(addressDetails.get(GNAF_LONGITUDE));
  	action.setResidentialGnaf(addressDetails.get(GNAF_PID));
	}
	
	/**
	 * Populate individual address fields after looking up QAS address by search string.
	 * @param action
	 * @throws Fault
	 */
	public void populateAddressFields(QasPostalAware action) throws Fault {
		QAPortType service = (new ProWeb()).getQAPortType();
  	ObjectFactory factory = new ObjectFactory();
  	
  	EngineType engine = factory.createEngineType();
  	engine.setValue(EngineEnumType.VERIFICATION);
  	    	
  	QASearch search = factory.createQASearch();
  	search.setCountry(QAS_COUNTRY_POSTAL);
  	search.setEngine(engine);
  	search.setLayout(QAS_LAYOUT_POSTAL);
  	search.setSearch(action.getSearch());

  	QASearchResult qaResult = service.doSearch(search);  	
  	
  	QAAddressType address = qaResult.getQAAddress();
  	
  	Map<String,String> addressDetails = new HashMap<String,String>();
  	for (AddressLineType line : address.getAddressLine()) {
  		addressDetails.put(line.getLabel(), line.getLine());
  	}
  	
  	StringBuilder streetNo = new StringBuilder(); 
  	if (!addressDetails.get(PAF_LOT).isEmpty()) {
  		streetNo.append(addressDetails.get(PAF_LOT) + " ");
  	}
  	if (!addressDetails.get(PAF_LOT_NO).isEmpty()) {
  		streetNo.append(addressDetails.get(PAF_LOT_NO) + " ");
  	}
  	if (!addressDetails.get(PAF_UNIT).isEmpty()) {
  		streetNo.append("Unit " + addressDetails.get(PAF_UNIT) + " ");
  	}
  	if (!addressDetails.get(PAF_STREET_NO).isEmpty()) {
  		streetNo.append(addressDetails.get(PAF_STREET_NO));
  	}
  	
  	action.setPostalStreetNumber(streetNo.toString().toUpperCase());
  	action.setPostalStreet(addressDetails.get(PAF_STREET_NAME).toUpperCase() + " " + addressDetails.get(PAF_STREET_TYPE).toUpperCase());
  	action.setPostalSuburb(addressDetails.get(PAF_SUBURB).toUpperCase());
  	action.setPostalPostcode(addressDetails.get(PAF_POSTCODE));
  	action.setPostalDpid(addressDetails.get(PAF_PID));
  	action.setPostalAusbar(addressDetails.get(PAF_AUSBAR));
	}
	
	/**
	 * Log calls to QAS WS to file.
	 * @param action
	 */
	private void logQasUsage(QasResidentialAware action) {
	
  	String quoteNo = "";
  	String src = "";
  	
  	if (action instanceof QuotePageSave) { // Vehicle
  		quoteNo = ((QuotePageSave) action).getQuoteNo().toString();
  		src = "Vehicle Quote";
  	} else if (action instanceof ActionBase) { // Home
  		quoteNo = Integer.toString(((ActionBase) action).getQuoteNo());
  		src = "Home Quote";
  	}
  	
  	//DateTime,Source,RecordIdentifier,Moniker
  	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  	String log = df.format(new Date()) + "," + src + "," + quoteNo + "," + action.getMoniker() + "\n";
  	
  	try {
		  String fileName = FileUtil.getProperty("master", "qas.log.path");
	  	
	  	FileWriter fileWriter = new FileWriter(fileName, true); // append
	  	fileWriter.write(log);
	  	fileWriter.flush();
	    fileWriter.close();
  	} catch (Exception e) {
  		Config.logger.warn(Config.key + ": unable to write to QAS log file! ", e);
  	}
	}
	
	/**
	 * Return a list of address suggestions based on addressQuery.
	 * @param action
	 * @return
	 * @throws Fault
	 */
	public List<PicklistEntryType> doAddressSearch(QasLookup action) throws Fault {
		
		QAPortType service = (new ProWeb()).getQAPortType();
  	ObjectFactory factory = new ObjectFactory();
  	
  	List<PicklistEntryType> resultList = new ArrayList<PicklistEntryType>();
  	    	
  	EngineType engine = factory.createEngineType();
  	engine.setValue(EngineEnumType.INTUITIVE);
  	    	
  	String queryString = action.getAddressQuery();
  	QASearch search = factory.createQASearch();
  	search.setCountry(action.getAddressType().equals(QasLookup.TYPE_POSTAL) ? QAS_COUNTRY_POSTAL : QAS_COUNTRY_RESIDENTIAL);
  	search.setEngine(engine);
  	search.setSearch(queryString);
  	    	
  	QASearchResult qaResult = service.doSearch(search);
  	List<PicklistEntryType> rawResultList = qaResult.getQAPicklist().getPicklistEntry();
  	
  	resultList = refineLocalResults(action, rawResultList);
  	Config.logger.debug(Config.key + ": found " + resultList.size() + "/" + rawResultList.size() + " usable result(s) for: " + queryString);
	  	
  	// if no results, help the user out by appending " TAS"
  	if (resultList.isEmpty()) {
  		queryString += " TAS"; 
  		search.setSearch(queryString);
  		qaResult = service.doSearch(search);
  	  	rawResultList = qaResult.getQAPicklist().getPicklistEntry();
  	  	
  	  	resultList = refineLocalResults(action, rawResultList);
  	  	Config.logger.debug(Config.key + ": found " + resultList.size() + "/" + rawResultList.size() + " usable result(s) for: " + queryString);
  	}
  	
  	if (resultList.size() > QAS_RESULT_MAX) {
  		resultList = resultList.subList(0, QAS_RESULT_MAX);
  	}    	    
    
    return resultList;
	}
	
	/**
	 * Take a list of QAS pick list entries and exclude anything we don't want the user to be able to pick from.
	 * @param action
	 * @param rawResultList
	 * @return
	 */
	private List<PicklistEntryType> refineLocalResults(QasLookup action, List<PicklistEntryType> rawResultList) {
		List<PicklistEntryType> resultList = new ArrayList<PicklistEntryType>();
		
		for (PicklistEntryType p : rawResultList) {
	  		// remove non-TAS addresses      	
	  		if (!action.isNational() && !p.getPostcode().startsWith("7")) {
	  			continue;
	  		}
	  		if (!p.isFullAddress()) {
	  			continue;
	  		}
	  		resultList.add(p);
	  	}
		
		return resultList;
	}
	
}
