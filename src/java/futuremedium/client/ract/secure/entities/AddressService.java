package futuremedium.client.ract.secure.entities;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.ract.common.StreetSuburbVO;

import futuremedium.client.ract.secure.Config;

@Service
public class AddressService extends BaseService {

  /**
   * Lookup streetSuburbId based on directly provided Suburb, Postcode and Street.
   * Optionally create entry if not found.
   * @param suburb
   * @param street
   * @param postcode
   * @param createIfNotExists
   * @return
   */
  public Integer lookupStreetSuburbId(String suburb, String street, String postcode, boolean createIfNotExists) {
    Integer result = null;

    try {
      Collection<StreetSuburbVO> ssList = this.getAddressMgr().getStreetSuburbsBySuburb(suburb, postcode);

      for (StreetSuburbVO streetSuburbVO : ssList) {
        if (String.valueOf(streetSuburbVO.getStreet()).equals(street)) {
          result = streetSuburbVO.getStreetSuburbID();
          break;
        }
      }
      
      if (result == null && createIfNotExists) {      	
  			try {
  				result = this.getAddressMgr().createStreetSuburb(street, suburb, postcode);
  			} catch (Exception e) {
  				Config.logger.error(Config.key + ": unable to create StreetSuburbId from suburb: " + suburb + ", postcode: " + postcode + ", street: " + street, e);
  			}
      }
      
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to retrieve StreetSuburbId from suburb: " + suburb + ", postcode: " + postcode + ", street: " + street, e);
    }

    return result;
  }
}
