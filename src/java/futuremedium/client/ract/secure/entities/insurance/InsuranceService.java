package futuremedium.client.ract.secure.entities.insurance;

import org.springframework.stereotype.Service;

import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.entities.AddressService;

/**
 * Common Insurance services.
 * @author newtong
 *
 */
@Service
public class InsuranceService extends AddressService {
  
  /**
   * Apply GNAF, Latitude and Longitude from risk address to client
   * if risk address is the same as client address.
   * @param quoteNo
   * @param client
   * @return
   */
  public WebInsClient attemptQasRiskAddress(Integer quoteNo, WebInsClient client) {
  	
  	String streetChar = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_NO);
  	
  	if (streetChar == null) {
  		Config.logger.warn(Config.key + ": unable to find Risk StreetChar for quoteNo: " + quoteNo);
  		return client;
  	}
  	
  	Integer streetSuburbId = -1;
  	try {
  		streetSuburbId = Integer.parseInt(this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_ID));
  	} catch (NumberFormatException e) {
  		Config.logger.warn(Config.key + ": unable to find Risk StreetSuburbId for quoteNo: " + quoteNo);
  		return client;  		
  	}
  	
  	String riskLatitude = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_LATITUDE);
  	String riskLongitude = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_LONGITUDE);
  	String riskGnaf = this.getInsuranceMgr().getQuoteDetail(quoteNo, WebInsQuoteDetail.SITUATION_GNAF);
  	  	
  	if (streetChar.equals(client.getResiStreetChar()) && streetSuburbId.equals(client.getResiStsubid())) {
  		client.setResiLatitude(riskLatitude);
  		client.setResiLongitude(riskLongitude);
  		client.setResiGnaf(riskGnaf);
  	} else {
  		client.setResiLatitude(null);
  		client.setResiLongitude(null);
  		client.setResiGnaf(null);
  	}
  	
  	return client;
  }
}
