package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.Comparator;

/**
 * A comparator to sort by suburb name.
 *
 * @author xmfu
 */
public class SuburbComparator implements Comparator<GaragedAddress> {
  public int compare(GaragedAddress o1, GaragedAddress o2) {
    return o1.getSuburb().compareTo(o2.getSuburb());
  }
}