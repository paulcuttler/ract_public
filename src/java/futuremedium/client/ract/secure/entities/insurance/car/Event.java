package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.*;

/**
 * A Content class holds event node from insurance_quote_question.
 * @author xmfu
 */
public class Event {
  private String ONCLICK = "onclick";
  private String ONCHANGE = "onchange";
  private String ONBLUR = "onblur";
  private String ONKEYPRESS = "onkeypress";
  private String type;
  private String function;
  private List<AjaxReturn> ajaxReturnMessages;

  public String getOnKeyPressType() {
    return this.ONKEYPRESS;
  }

  public String getOnBlurType() {
    return this.ONBLUR;
  }

  public String getOnChangeType() {
    return this.ONCHANGE;
  }

  public String getOnclickType() {
    return ONCLICK;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the function
   */
  public String getFunction() {
    return function;
  }

  /**
   * @param function the function to set
   */
  public void setFunction(String function) {
    this.function = function;
  }

  /**
   * @return the ajaxReturnMessages
   */
  public List<AjaxReturn> getAjaxReturnMessage() {
    return ajaxReturnMessages;
  }

  /**
   * @param ajaxReturnMessages the ajaxReturnMessages to set
   */
  public void setAjaxReturnMessage(List<AjaxReturn> ajaxReturnMessages) {
    this.ajaxReturnMessages = ajaxReturnMessages;
  }
}
