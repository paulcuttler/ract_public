package futuremedium.client.ract.secure.entities.insurance.home;

import com.ract.util.DateTime;

import java.util.*;

import org.springframework.stereotype.Service;

import com.ract.web.insurance.*;
import com.ract.client.*;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.insurance.home.*;
import futuremedium.client.ract.secure.entities.BaseService;

/**
 * Services relating to Home Insurance Quoting Options.
 * @author gnewton
 */
@Service
public class OptionService extends BaseService {

  List<Map<String,String>> quoteTypeList;


  /**
   * Retrieve a list of Year Constructed options.
   * @return
   */
  public Collection<ClientTitleVO> getTitleOptions() {

    Collection<ClientTitleVO> options = new ArrayList<ClientTitleVO>();

     try {
      options = this.getCustomerMgr().getClientTitles(true, false);
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to populate title options: ", e);
    }

    return options;
  }

  /**
   * Retrieve a list of Manger options.
   * @return
   */
  public List<InRfDet> getMangerOptions(ActionBase action) throws Exception {

    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.RENTAL_MANAGER, action.getQuoteEffectiveDate());

    return options;
  }

  /**
   * Retrieve a list of Year Constructed options.
   * @return
   */
  public List<String> getYearConstructedOptions() {
    List<String> options = new ArrayList<String>();

    int endYear = Calendar.getInstance().get(Calendar.YEAR);

    for (int i=1800; i<=endYear; i++) {
      options.add(Integer.toString(i));
    }

    Collections.reverse(options);

    return options;
  }

  /**
   * Retrieve a list of Building Type options.
   * @return
   */
  public List<InRfDet> getBuildingTypeOptions(ActionBase action) throws Exception {

    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.BUILDING_TYPE, action.getQuoteEffectiveDate());

    return options;
  }

  /**
   * Retrieve a list of Wall Construction options.
   * @return
   */
  public List<InRfDet> getWallConstructionOptions(ActionBase action) throws Exception {

    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.CONSTRUCTION, action.getQuoteEffectiveDate());

    return options;
  }

  /**
   * Retrieve a list of Roof Construction Options.
   * @return
   * 
   */
  public List<InRfDet> getRoofConstructionOptions(ActionBase action) throws Exception {

    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.ROOF, action.getQuoteEffectiveDate());

    return options;
  }


  /**
   * Retrieve a list of Occupancy Options.
   * @return
   *
   */
  public List<InRfDet> getOccupancyOptions(ActionBase action) throws Exception {

    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.OCCUPANCY, action.getQuoteEffectiveDate());

    return options;
  }
  

  /**
   * Retrieve a list of Business Use options.
   * @return
   */
  public List<InRfDet> getBusinessUseOptions(ActionBase action) throws Exception {

    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.BUSINESS_USE, action.getQuoteEffectiveDate());

    return options;
  }
  
  /**
   * Retrieve a list of Security Lock options.
   * @return
   */
  public List<InRfDet> getSecurityLockOptions(ActionBase action) throws Exception {
      
      List<InRfDet> options = null;
      
      options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.SECURITY_LOCK, action.getQuoteEffectiveDate());
      
      return options;
  }
  
  /**
   * Retrieve a list of Security Alarm options.
   * @return
   */
  public List<InRfDet> getSecurityAlarmOptions(ActionBase action) throws Exception {
      
      List<InRfDet> options = null;
      
      options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.SECURITY_ALARM, action.getQuoteEffectiveDate());
      
      return options;
  }

  /**
   * Retrieve a list of selectable financier options.
   * @return
   */
  public List<InRfDet> getFinancierOptions(AboutYourHomePolicyBase action) throws Exception {
    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.FINANCIER, action.getQuoteEffectiveDate());

    return options;
  }

  /**
   * Retrieve a list of selectable Unrelated Persons options.
   * @return
   */
  public List<InRfDet> getUnrelatedPersonsOptions(AboutYourHomePolicyBase action) throws Exception {
    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.UNRELATED_PERSONS, action.getQuoteEffectiveDate());

    return options;
  }

  /**
   * Retrieve a list of selectable Specified Contents options.
   * @return
   */
  public List<InRfDet> getSpecifiedContentsOptions(SpecifiedContentsBase action) throws Exception {
    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", action.getTranslatedCoverType(), WebInsQuoteDetail.SPEC_CNTS, action.getQuoteEffectiveDate());

    return options;
  }

  /**
   * Return a list of Maps representing Quote Types.
   * @return 
   */
  public List<Map<String,String>> getQuoteTypeList() {

    if (quoteTypeList == null) {
      quoteTypeList = new ArrayList<Map<String,String>>();

      Map<String,String> lookup = new HashMap<String,String>();
      lookup.put("key", ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
      lookup.put("list", "top");
      lookup.put("value", "Building & Contents");
      quoteTypeList.add(lookup);

      lookup = new HashMap<String,String>();
      lookup.put("key", ActionBase.VALUE_QUOTE_TYPE_BLDG_ONLY);
      lookup.put("list", "top");
      lookup.put("value", "Building only");
      quoteTypeList.add(lookup);

      lookup = new HashMap<String,String>();
      lookup.put("key", ActionBase.VALUE_QUOTE_TYPE_CNTS_ONLY);
      lookup.put("list", "top");
      lookup.put("value", "Contents only");
      quoteTypeList.add(lookup);

      lookup = new HashMap<String,String>();
      lookup.put("key", ActionBase.VALUE_QUOTE_TYPE_INV);
      lookup.put("list", "bottom");
      lookup.put("value", "Investor");
      quoteTypeList.add(lookup);
    }
    
    return this.quoteTypeList;
  }

  /**
   * Test if the suburb associated with the action is acceptable.
   * @param action
   * @return
   */
  public boolean isAcceptableSuburb(WhereYouLive action) throws Exception {
    boolean acceptable = false;
    acceptable = this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.SUBURB_UNACCEPTABLE, action.getResidentialSuburb(), action.getQuoteEffectiveDate()).equals("OK");
    return acceptable;
  }
  
  public boolean isEmbargoSuburb(WhereYouLive action) throws Exception {
      return this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.SUBURB_UNACCEPTABLE, action.getResidentialSuburb(), action.getQuoteEffectiveDate()).equals("EB");
  }
  
  public boolean isUnknownSuburb(WhereYouLive action) throws Exception {
      return this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.SUBURB_UNACCEPTABLE, action.getResidentialSuburb(), action.getQuoteEffectiveDate()).equals("UK");
  }

  /**
   * Retrieve a list of Excess options.
   * @param action
   * @param coverType
   * @return
   */
    public List<InRfDet> getExcessOptions(PremiumOptionsBase action, String coverType) throws Exception {
        /**
         * Determine the applicable product type to use - requires either PIP or SS. 
         * PIP is the default case (regardless of whether this is applicable to the client)
         */
        String productModifier = ActionBase.FIELD_PRODUCT_MODIFIER_PIP;
        if (Boolean.parseBoolean(this.getInsuranceMgr().getQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS))) {
            productModifier = ActionBase.FIELD_PRODUCT_MODIFIER_FIFTY_PLUS;
        }
        List<InRfDet> options = this.getInsuranceMgr().getLookupData(productModifier, coverType, WebInsQuoteDetail.EXCESS_OPTIONS, action.getQuoteEffectiveDate());

        return options;
    }
  
  /**
   * Retrieve a list of selectable Building Insurance options.
   * @return
   */
  public List<InRfDet> getBuildingInsuranceOptions(PremiumOptionsBase action) throws Exception {
    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", WebInsQuote.COVER_BLDG, WebInsQuoteDetail.OPTIONS, action.getQuoteEffectiveDate());

    return options;
  }

  /**
   * Retrieve a list of selectable Contents Insurance options.
   * @return
   */
  public List<InRfDet> getContentsInsuranceOptions(PremiumOptionsBase action) throws Exception {
    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", WebInsQuote.COVER_CNTS, WebInsQuoteDetail.OPTIONS, action.getQuoteEffectiveDate());

    return options;
  }

  /**
   * Retrieve a list of selectable Investor Insurance options.
   * @return
   */
  public List<InRfDet> getInvestorInsuranceOptions(PremiumOptionsBase action) throws Exception {
    List<InRfDet> options = null;

    options = this.getInsuranceMgr().getLookupData("", WebInsQuote.COVER_INV, WebInsQuoteDetail.OPTIONS, action.getQuoteEffectiveDate());

    return options;
  }
  
  /**
   * Retrieve a list of selectable Investor Contents Insurance options.
   * @return
   */
  public List<InRfDet> getInvestorContentsInsuranceOptions(PremiumOptionsBase action) throws Exception {
      List<InRfDet> options = null;

      options = this.getInsuranceMgr().getLookupData("", WebInsQuote.COVER_INV, WebInsQuote.COVER_CNTS, action.getQuoteEffectiveDate());
      
      return options;
  }
  
  /**
   * Retrieve a list of selectable Investor Contents Sum Insured options.
   * @return
   */
  public List<InRfDet> getInvestorContentsSumInsuredOptions(PremiumOptionsBase action) throws Exception {
      List<InRfDet> options = null;
      
      options = this.getInsuranceMgr().getLookupData("", WebInsQuote.COVER_INV, WebInsQuoteDetail.CNTS_SUM_INSURED, action.getQuoteEffectiveDate());
      
      return options;
  }

  /**
   * Populate a List of start dates from @initialOffset days in the future to @limit days in the future.
   * @param initialOffset
   * @param limit
   * @return
   */
  public List<DateTime> getStartDates(int initialOffset, int limit) {
    Calendar start = Calendar.getInstance();
    start.add(Calendar.DATE, initialOffset);
    start.set(Calendar.HOUR_OF_DAY, 0);
    start.set(Calendar.MINUTE, 0);
    start.set(Calendar.SECOND, 0);
    start.set(Calendar.MILLISECOND, 0);

    Calendar end = Calendar.getInstance();
    end.add(Calendar.DATE, limit);
    end.set(Calendar.HOUR_OF_DAY, 0);
    end.set(Calendar.MINUTE, 0);
    end.set(Calendar.SECOND, 0);
    end.set(Calendar.MILLISECOND, 0);

    List<DateTime> dates = new ArrayList<DateTime>();
    Calendar date = Calendar.getInstance();
    date.setTime(start.getTime());

    while (date.before(end)) {
      dates.add(new DateTime(date.getTime()));
      date.add(Calendar.DATE, 1);
    }
    
    // sanity check
    if (dates.size() > limit) {
    	dates = dates.subList(0, limit);
    }

    return dates;
  }

  /**
   * Retrieve a list of Gender options.
   * @return
   */
  public Map<String,String> getGenderOptions() {

    Map<String,String> options = new LinkedHashMap<String,String>();
    
    options.put(WebInsClient.GENDER_MALE, "Male");
    options.put(WebInsClient.GENDER_FEMALE, "Female");

    return options;
  }

  /**
   * Retrieve a list of Credit Card options.
   * @return
   */
  public Map<String,String> getCreditCardOptions() {

    Map<String,String> options = new LinkedHashMap<String,String>();

    options.put(WebInsQuoteDetail.ACC_TYPE_VISA, "Visa");
    options.put(WebInsQuoteDetail.ACC_TYPE_MASTERCARD, "Mastercard");

    return options;
  }

  /**
   * Retrieve a list of Phone Area Code options.
   * @return
   */
  public List<String> getAreaCodeOptions() {
    return Arrays.asList(Config.configManager.getProperty("insurance.home.areaCodes").split(","));
  }

  /**
   * Retrieve a list of Payment Options.
   */
  public Map<String,String> getPaymentOptions() {
    Map<String,String> options = new LinkedHashMap<String,String>();
    
    // Business Continuity Processing - means Invoice payment is only option
    boolean bcpMode = Boolean.parseBoolean(Config.configManager.getProperty("insurance.bcpMode", "false"));
    if (!bcpMode) {
    	options.put(WebInsQuoteDetail.PAY_ANNUALLY, Config.configManager.getProperty("insurance.home.payment.annual.message"));
    	options.put(WebInsQuoteDetail.PAY_DD, Config.configManager.getProperty("insurance.home.payment.directDebit.message"));
    }
    options.put(WebInsQuoteDetail.PAY_CASH, Config.configManager.getProperty("insurance.home.payment.account.message"));

    return options;
  }

  /**
   * Retrieve a list of Direct Debit Options.
   */
  public Map<String,String> getDirectDebitOptions() {
    Map<String,String> options = new LinkedHashMap<String,String>();

    options.put(ActionBase.FIELD_DD_CREDIT_CARD, "Credit Card");
    options.put(ActionBase.FIELD_DD_BANK_ACCOUNT, "Bank Account");

    return options;
  }

  /**
   * Retrieve a list of Payment Frequency Options.
   */
  public Map<String,String> getFrequencyOptions() {
    Map<String,String> options = new LinkedHashMap<String,String>();

    options.put(WebInsQuoteDetail.PAY_ANNUALLY, "Annual");
    options.put(WebInsQuoteDetail.PAY_MONTHLY, "Monthly");

    return options;
  }
  
  /**
   * Retrieve a list of Survey Rate Options.
   * @return
   */
  public Map<String, String> getRateOptions() {
    Map<String, String> options = new LinkedHashMap<String, String>();

    List<String> pairs = Arrays.asList(Config.configManager.getProperty("insurance.home.survey.rate.options").split("\\|"));
    for (String pair : pairs) {
      if (pair.contains(":")) {
        String[] parts = pair.split(":");
        if (parts.length == 2) {
          options.put(parts[0], parts[1]);
        }
      }
    }

    return options;
  }

  /**
   * Retrieve a list of Exit Reason Options.
   * @return
   */
  public Map<String, String> getReasonOptions() {
    Map<String, String> options = new LinkedHashMap<String, String>();

    List<String> pairs = Arrays.asList(Config.configManager.getProperty("insurance.home.exit.reason.options").split("\\|"));
    for (String pair : pairs) {
      if (pair.contains(":")) {
        String[] parts = pair.split(":");
        if (parts.length == 2) {
          options.put(parts[0], parts[1]);
        }
      }
    }

    return options;
  }
}