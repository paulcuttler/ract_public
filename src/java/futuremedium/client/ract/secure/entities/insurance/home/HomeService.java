package futuremedium.client.ract.secure.entities.insurance.home;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.InRiskSi;
import com.ract.web.insurance.Premium;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

import futuremedium.client.ract.secure.Config;
import futuremedium.client.ract.secure.actions.insurance.InsuranceBase;
import futuremedium.client.ract.secure.actions.insurance.home.AboutYou;
import futuremedium.client.ract.secure.actions.insurance.home.AboutYourHomePolicy;
import futuremedium.client.ract.secure.actions.insurance.home.AboutYourHomePolicyForm;
import futuremedium.client.ract.secure.actions.insurance.home.ActionBase;
import futuremedium.client.ract.secure.actions.insurance.home.AddressDetails;
import futuremedium.client.ract.secure.actions.insurance.home.AddressDetailsBase;
import futuremedium.client.ract.secure.actions.insurance.home.NewQuote;
import futuremedium.client.ract.secure.actions.insurance.home.PaymentOptions;
import futuremedium.client.ract.secure.actions.insurance.home.PolicySupport;
import futuremedium.client.ract.secure.actions.insurance.home.PolicyholderDetails;
import futuremedium.client.ract.secure.actions.insurance.home.PolicyholderDetailsBase;
import futuremedium.client.ract.secure.actions.insurance.home.PremiumOptionsBase;
import futuremedium.client.ract.secure.actions.insurance.home.Quote;
import futuremedium.client.ract.secure.actions.insurance.home.QuoteSubTypeSupport;
import futuremedium.client.ract.secure.actions.insurance.home.RetrieveQuote;
import futuremedium.client.ract.secure.actions.insurance.home.SpecifiedContents;
import futuremedium.client.ract.secure.actions.insurance.home.SpecifiedContentsBase;
import futuremedium.client.ract.secure.actions.insurance.home.SumInsured;
import futuremedium.client.ract.secure.actions.insurance.home.WhereYouLive;
import futuremedium.client.ract.secure.entities.insurance.InsuranceService;

/**
 * Services relating to Home Insurance Quoting.
 * @author gnewton
 */
@Service
public class HomeService extends InsuranceService {

  private List<String> flow;
  private List<Page> pageFlow;

  public static final String ACTION_CLASS_PREFIX = "futuremedium.client.ract.secure.actions.insurance.home.";

  /**
   * Start a quote and persist quoteNo, securedQuoteNo and effective date to the session.
   * Clear any session flags such as 'aborted'.
   * @return
   */
  public boolean startQuote(NewQuote action) {

    boolean success = false;

    try {

      Map<String, Object> session = action.getSession();

      String quoteType = (String) session.get(ActionBase.TOKEN_HOME_QUOTE_TYPE);
      String promoCode = action.getPromoCode();
      
      session.clear();

      action.setQuoteType(quoteType);
     
      DateTime startDate = new DateTime();
      int quoteNo = this.getInsuranceMgr().startQuote(startDate, WebInsQuote.TYPE_HOME);
      this.getInsuranceMgr().setQuoteStatus(quoteNo, WebInsQuote.NEW);

      action.setQuoteNo(quoteNo);
      action.setSecuredQuoteNo(this.getSecuredQuoteNumber(quoteNo));
      action.setAborted(false);
      action.setQuoteEffectiveDate(startDate);
      action.resetMemberCardAttempts();
      action.setBuildingSumInsured(null);
      action.setInvestorSumInsured(null);
      action.resetOldestDobAttempts();

      this.getInsuranceMgr().setCoverType(quoteNo, action.getQuoteType());

      success = true && storeQuoteDetail(action, ActionBase.FIELD_QUOTE_SUB_TYPE, action.getQuoteType());
      success = success && storeQuoteDetail(action, ActionBase.FIELD_DISPLAY_QUOTE_NUMBER, action.getSecuredQuoteNo());

      // store agent portal fields if present in session
      if (action.getPersistedAgentCode() != null) {
        success = success && storeQuoteDetail(action, WebInsQuoteDetail.AGENT_CODE, action.getPersistedAgentCode());
      }
      if (action.getPersistedAgentUser() != null) {
        success = success && storeQuoteDetail(action, WebInsQuoteDetail.AGENT_USER, action.getPersistedAgentUser());
      }
      
      // store promo code, if any
      if (promoCode != null && !promoCode.isEmpty()) {
        success = success && storeQuoteDetail(action, ActionBase.FIELD_PROMO_CODE, promoCode);      	
      }

      // store quote type data
      success = success && storeQuoteDetail(action, WebInsQuoteDetail.WEB_QUOTE_TYPE, WebInsQuoteDetail.WEB_QUOTE_TYPE_HOME);
      
    } catch (Exception e) {
      Config.logger.fatal(Config.key + ": unable to start Home Quote: ", e);
    }

    return success;
  }

  /**
   * Store the user's sessionId.
   * @param action
   * @param sessionId
   * @return
   */
  public boolean storeSessionId(ActionBase action, String sessionId) {
    boolean success = false;

    try {
      this.getInsuranceMgr().setQuoteDetail(action.getQuoteNo(), InsuranceBase.FIELD_SESSION_ID, sessionId);
      success = true;
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to update Home Quote #" + action.getQuoteNo() + " sessionId: ", e);
    }

    return success;
  }
  
  /**
   * Update the Quote Start Date.
   * @param action
   * @return
   */
  public boolean storeQuoteEffectiveDate(WhereYouLive action) {
    boolean success = false;

    try {
      this.getInsuranceMgr().setQuoteDate(action.getQuoteNo(), action.getQuoteEffectiveDate());
      success = true;
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to update Home Quote #" + action.getQuoteNo() + " quote start date: ", e);
    }

    return success;
  }

  /**
   * Return the Quote Start Date.
   * @param action
   * @return
   */
  public DateTime getQuoteEffectiveDate(AboutYourHomePolicyForm action) {
    DateTime quoteEffectiveDate = null;

    try {
      quoteEffectiveDate = this.getInsuranceMgr().getQuote(action.getQuoteNo()).getQuoteDate();
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to retrieve Home Quote #" + action.getQuoteNo() + " quote start date: ", e);
    }

    return quoteEffectiveDate;
  }
  
  /**
   * Retrieve a quote via provide quoteNo in action.
   * @param action
   */
  public boolean retrieveQuote(RetrieveQuote action) {
    int quoteNumber;

    try {
      quoteNumber = WebInsQuote.validateQuoteNo(action.getSavedQuoteNo());
    } catch (Exception e) {
      action.addFieldError("savedQuoteNo", "We were unable to locate a valid quote.");
      Config.logger.error(Config.key + ": unable to retrieve Home Quote by quoteNo: " + action.getSavedQuoteNo());
      return false;
    }

    String abortedValue = this.retrieveQuoteDetail(quoteNumber, ActionBase.FIELD_ABORTED);
    boolean aborted = (abortedValue != null && !abortedValue.isEmpty());
    if (aborted) {
      action.addFieldError("savedQuoteNo", "We were unable to locate a valid quote. " + Config.configManager.getProperty("insurance.home.retrieve.aborted.message"));
      Config.logger.info(Config.key + ": Home Quote #" + action.getSavedQuoteNo() + " is not in a retrievable state (aborted).");
      return false;
    }

    // check quote status
    List<String> permittedStatuses = new ArrayList<String>();
    permittedStatuses.add(WebInsQuote.NEW);
    permittedStatuses.add(WebInsQuote.QUOTE);
    permittedStatuses.add(WebInsQuote.COMPLETE);
    permittedStatuses.add(WebInsQuote.COVER_ISSUED);

    WebInsQuote currentQuote = this.getInsuranceMgr().getQuote(quoteNumber);

    //System.out.println("current quote is : " + currentQuote);

    if (null == currentQuote || !permittedStatuses.contains(currentQuote.getQuoteStatus()) || !currentQuote.getQuoteType().equals(WebInsQuote.TYPE_HOME)) {
      action.addFieldError("savedQuoteNo", "We were unable to locate a valid quote. " + Config.configManager.getProperty("insurance.home.retrieve.converted.message"));
      Config.logger.info(Config.key + ": Home Quote #" + action.getSavedQuoteNo() + " is not in a retrievable state (" + currentQuote.getQuoteStatus()  + " | " + currentQuote.getQuoteType() + ").");
      return false;
    }

    // test quote date
    Calendar validDate = Calendar.getInstance();
    validDate.add(Calendar.DATE, -Integer.parseInt(Config.configManager.getProperty("insurance.home.maxQuoteAgeDays", "14")));

    Calendar quoteDate = Calendar.getInstance();
    quoteDate.setTime(currentQuote.getQuoteDate());

    if (quoteDate.before(validDate)) {
      action.addFieldError("savedQuoteNo", "We were unable to locate a valid quote. " + Config.configManager.getProperty("insurance.home.retrieve.expired.message"));
      Config.logger.info(Config.key + ": Home Quote #" + action.getSavedQuoteNo() + " is not in a retrievable state (expired).");
      return false;
    }

    // populate quote particulars in session
    action.setQuoteNo(quoteNumber);
    action.setSecuredQuoteNo(this.getSecuredQuoteNumber(quoteNumber));
    action.setQuoteType(this.retrieveQuoteDetail(quoteNumber, ActionBase.FIELD_QUOTE_SUB_TYPE));
    action.setQuoteEffectiveDate(currentQuote.getQuoteDate());
    action.setLastActionName(this.retrieveLastAction(quoteNumber));

    // instantiate last action so we can determine next action
    action.setNextAction(this.calculateNextAction(action));

    Config.logger.debug(Config.key + ": retrieved Home Quote: " + quoteNumber + ", next action = " + action.getNextAction());

    return true;
  }
  
  /**
   * Calculate next action after first instantiating last action object. 
   * @param action
   * @return
   */
  public String calculateNextAction(ActionBase action) {
      if(null == action || null == action.getLastFormActionName()) {
          Config.logger.warn(Config.key + ": calculateNextAction received null action.getLastFormActionName");
          return null;
      }
      
      String className = ACTION_CLASS_PREFIX + action.getLastFormActionName().replaceFirst(ActionBase.FORM_ACTION_PREFIX, "");
      
    try {
      Class actionClass = Class.forName(className); // convert from Action name to Class name
	  ActionBase lastAction = (ActionBase) actionClass.newInstance();
	  lastAction.setHomeService(this); // inject service
	  lastAction.setQuoteType(action.getQuoteType()); // used to determine next action...
	  return lastAction.getNextAction();
	    
    } catch (Exception e) {
      Config.logger.error(Config.key + ": could not instantiate " + className, e);
    } 
    
    return null;
  }

  /**
   * Wrapper method to mark the quote part of the process as finalised.
   * @param action
   * @return
   */
  public boolean finaliseQuote(Quote action) {
    return this.storeQuoteDetail(action, ActionBase.FIELD_FINALISED_QUOTE, Boolean.TRUE.toString());
  }

  /**
   * Wrapper method to mark the policy part of the process as finalised.
   * @param action
   * @return
   */
  public boolean finalisePolicy(PaymentOptions action) { 
      return this.storeQuoteDetail(action, ActionBase.FIELD_FINALISED_POLICY, Boolean.TRUE.toString());
  }

  /**
   * Determine if this quote has been finalised.
   * @return true if viewed, false otherwise.
   */
  public boolean hasFinalisedQuote(int quoteNo) { 
    boolean finalised = false;

    try {
      finalised = Boolean.parseBoolean(retrieveQuoteDetail(quoteNo, ActionBase.FIELD_FINALISED_QUOTE));
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to determine if Home Quote #" + quoteNo + " has been finalised: ", e);
    }

    return finalised;
  }

  /**
   * Determine if this policy has been finalised.
   * @return true if viewed, false otherwise.
   */
  public boolean hasFinalisedPolicy(int quoteNo) {
    boolean finalised = false;

    try {
      finalised = Boolean.parseBoolean(retrieveQuoteDetail(quoteNo, ActionBase.FIELD_FINALISED_POLICY));
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to determine if Home Policy #" + quoteNo + " has been finalised: ", e);
    }

    return finalised;
  }

  /**
   * Determine if this premium has been viewed.
   * @return true if viewed, false otherwise.
   */
  public boolean hasViewedPremium(int quoteNo) {
    boolean finalised = false;

    try {
      finalised = Boolean.parseBoolean(retrieveQuoteDetail(quoteNo, ActionBase.FIELD_VIEWED_PREMIUM));
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to determine if Home Policy #" + quoteNo + " has had premium viewed: ", e);
    }

    return finalised;
  }

  /**
   * Retrieve secured quote number based on internal quote number.
   * @param quoteNo internal quote number.
   * @return quote number for public display.
   */
  public String getSecuredQuoteNumber(int quoteNo) {
    // try to find in DB first
    String securedQuoteNo = this.retrieveQuoteDetail(quoteNo, ActionBase.FIELD_DISPLAY_QUOTE_NUMBER);

    // otherwise derive from quoteNo
    if (quoteNo != 0 && securedQuoteNo == null) {
      try {
        securedQuoteNo = this.getInsuranceMgr().getQuote(quoteNo).getSecuredQuoteNo();
      } catch (Exception e) {
        Config.logger.error(Config.key + ": unable to retrieve secured Home Quote number for Quote #" + quoteNo + ": ", e);
      }
    }

    return securedQuoteNo;
  }

  /**
   * Save quote progress to DB, update last action.
   * @param action action containing quoteNo
   * @param field name of field being saved.
   * @param value value of field being saved.
   * @return true if successful, false otherwise.
   */
  public boolean storeQuoteDetail(ActionBase action, String field, String value) {
    return storeQuoteDetail(action, field, value, null, true);
  }

  /**
   * Save quote progress to DB, update last action.
   * @param action action containing quoteNo
   * @param field name of field being saved.
   * @param value value of field being saved.
   * @param data data object being saved.
   * @return true if successful, false otherwise.
   */
  public boolean storeQuoteDetail(ActionBase action, String field, String value, InRfDet data) {
    return storeQuoteDetail(action, field, value, data, true);
  }

  /**
   * Save quote progress to DB.
   * @param action action containing quoteNo
   * @param field name of field being saved.
   * @param value value of field being saved.
   * @param data data object being saved.
   * @param storeLastAction whether to record last action.
   * @return true if successful, false otherwise.
   */
  public boolean storeQuoteDetail(ActionBase action, String field, String value, InRfDet data, boolean storeLastAction) {

    boolean success = false;

    try {

      if (data != null) {
        WebInsQuoteDetail detail = new WebInsQuoteDetail(action.getQuoteNo(), field, value, data);
        this.getInsuranceMgr().setQuoteDetail(detail);
      } else {
        this.getInsuranceMgr().setQuoteDetail(action.getQuoteNo(), field, value);
      }

      Config.logger.debug(Config.key + ": stored detail against Home Quote: " + action.getQuoteNo() + ", field: " + field + ", value: " + value);

      if (storeLastAction) {
        storeLastActionName(action);
      }

      success = true;

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to save Home Quote Detail(" + action.getQuoteNo() + ", " + field + ", " + value + "): ", e);
    }

    return success;
  }

  /**
   * Retrieve quote detail from DB.
   */
  public String retrieveQuoteDetail(int quoteNo, String field) {
    String result = null;

    try {
      result = this.getInsuranceMgr().getQuoteDetail(quoteNo, field);

      Config.logger.debug(Config.key + ": retrieved detail from Home Quote: " + quoteNo + ", field: " + field + ", value: " + result);

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to retrieve Home Quote Detail(" + quoteNo + ", " + field + "): ", e);
    }

    return result;
  }

  /**
   * Store last action name in DB.
   * @param action
   * @return true if successful, false otherwise.
   */
  public boolean storeLastActionName(ActionBase action) {
    boolean success = false;
    String mostRecentlastActionName = this.getInsuranceMgr().getQuoteDetail(action.getQuoteNo(), ActionBase.FIELD_LAST_ACTION);

    int mostRecent = this.getFlow().indexOf(ActionBase.formify(mostRecentlastActionName));
    int next = this.getFlow().indexOf(action.getCurrentFormActionName());

    // only update last action if it is further in queue than previous
    if (next > mostRecent) {
      try {
        this.getInsuranceMgr().setQuoteDetail(action.getQuoteNo(), ActionBase.FIELD_LAST_ACTION, action.getCurrentActionName());
        Config.logger.debug(Config.key + ": stored lastAction = " + action.getCurrentActionName());
        success = true;

      } catch (Exception e) {
        Config.logger.error(Config.key + ": unable to save Home Quote Detail(" + action.getQuoteNo() + ", " + ActionBase.FIELD_LAST_ACTION + ", " + action.getCurrentActionName() + "): ", e);
      }
    } else {
      Config.logger.debug(Config.key + ": left lastAction = " + mostRecentlastActionName);
      success = true;
    }

    return success;
  }

  /**
   * Associate a member with a quote.
   * @param action
   * @return
   */
  public WebInsClient attachMember(AboutYou action) {
    WebInsClient client = null;

    try {
      client = this.getInsuranceMgr().setFromRACTClient(action.getQuoteNo(), action.getMembershipCard(), action.getMemberDobDateTime());

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to attach member card: " + action.getMembershipCard() + ", dob: " + action.getMemberDobDateTime() + " to Home Quote #" + action.getQuoteNo(), e);
    }

    return client;
  }

  /**
   * Shortcut method to retrieve last recorded action.
   */
  public String retrieveLastAction(int quoteNo) {
    return this.retrieveQuoteDetail(quoteNo, ActionBase.FIELD_LAST_ACTION);
  }

  /**
   * Lookup streetSuburbId based on provided Suburb, Postcode and Street from action.
   * @param action
   * @return
   */
  public String lookupStreetSuburbId(WhereYouLive action) {
    Integer result = lookupStreetSuburbId(action.getResidentialSuburb(), action.getResidentialStreet(), action.getResidentialPostcode(), true);
    try {
      return result.toString();
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Lookup streetSuburbId based on directly provided Suburb, Postcode and Street.
   * @param suburb
   * @param street
   * @param postcode
   * @return
   */
  private Integer lookupStreetSuburbId(String suburb, String street, String postcode) {
  	return lookupStreetSuburbId(suburb, street, postcode, false);
  }
  
  /**
   * Mark a quote as aborted in the DB and Session.
   * @param action action being executed.
   * @param reason reason causing abortion.
   * @param fieldValue value of field causing abortion.
   * @param message message to be displayed to user.
   * @return @NOTICE result.
   */
  public String abortQuote(ActionBase action, String reason, String fieldValue, String message) {

    this.storeQuoteDetail(action, ActionBase.FIELD_ABORTED, reason);

    Config.logger.info(Config.key + ": Home Quote #" + action.getQuoteNo() + " aborted due to " + reason + ": " + fieldValue);

    action.setAborted(true);
    action.setActionMessage(message);

    return ActionBase.NOTICE;
  }

  /**
   * Lazy initialise basic web flow.
   * @return
   */
  public List<String> getFlow() {

    if (flow == null) {
      flow = SiteInfo.getInstance().getFlow();
    }

    return flow;
  }

  /**
   * @param flow the flow to set
   */
  public void setFlow(List<String> flow) {
    this.flow = flow;
  }

  /**
   * Lazy initialise basic web page flow.
   * @return
   */
  public List<Page> getPageFlow() {

    if (pageFlow == null) {
      pageFlow = SiteInfo.getInstance().getPageFlow();
    }

    return pageFlow;
  }

  /**
   * @param flow the page flow to set
   */
  public void setPageFlow(List<Page> pageFlow) {
    this.pageFlow = pageFlow;
  }

  /**
   * Retrieve a Page metadata object associated with this action.
   */
  public Page getPage(ActionBase action) {
    return getPage(action.getCurrentFormActionName());
  }

  /**
   * Retrieve a Page metadata object directly by pageKey.
   */
  public Page getPage(String pageKey) {
    return (Page) SiteInfo.getInstance().getPageLookup().get(pageKey);
  }

  /**
   * Return the minimum Building Sum Insured value allowable.
   */
  public Integer getMinBuildingSumInsuredAllowed(SumInsured action) {
    Integer value = 0;

    try {
      String minValue = this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.MIN_SUM_INSURED, WebInsQuote.COVER_BLDG, action.getQuoteEffectiveDate());

      value = Integer.parseInt(minValue);

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to determine min building sum insured: quoteDate: " + action.getQuoteEffectiveDate(), e);
    }

    return value;
  }

  /**
   * Return the maximum Building Sum Insured value allowable.
   */
  public Integer getMaxBuildingSumInsuredAllowed(SumInsured action) {
    Integer value = 0;

    try {
      String maxValue = this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.MAX_SUM_INSURED, WebInsQuote.COVER_BLDG, action.getQuoteEffectiveDate());

      value = Integer.parseInt(maxValue);

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to determine max building sum insured: quoteDate: " + action.getQuoteEffectiveDate(), e);
    }

    return value;
  }

  /**
   * Return the minimum Contents Sum Insured value allowable.
   */
  public Integer getMinContentsSumInsuredAllowed(SumInsured action) {
    Integer value = 0;

    try {
      String minValue = this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.MIN_SUM_INSURED, WebInsQuote.COVER_CNTS, action.getQuoteEffectiveDate());

      value = Integer.parseInt(minValue);

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to determine min contents sum insured: quoteDate: " + action.getQuoteEffectiveDate(), e);
    }

    return value;
  }

  /**
   * Return the maximum Contents Sum Insured value allowable.
   */
  public Integer getMaxContentsSumInsuredAllowed(SumInsured action) {
    Integer value = 0;

    try {
      String maxValue = this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.MAX_SUM_INSURED, WebInsQuote.COVER_CNTS, action.getQuoteEffectiveDate());

      value = Integer.parseInt(maxValue);

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to determine max contents sum insured: quoteDate: " + action.getQuoteEffectiveDate(), e);
    }

    return value;
  }

  /**
   * Return the minimum Investor Sum Insured value allowable.
   */
  public Integer getMinInvestorSumInsuredAllowed(SumInsured action) {
    Integer value = 0;

    try {
      String minValue = this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.MIN_SUM_INSURED, WebInsQuote.COVER_INV, action.getQuoteEffectiveDate());

      value = Integer.parseInt(minValue);

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to determine min investor sum insured: quoteDate: " + action.getQuoteEffectiveDate(), e);
    }

    return value;
  }

  /**
   * Return the maximum Investor Sum Insured value allowable.
   */
  public Integer getMaxInvestorSumInsuredAllowed(SumInsured action) {
    Integer value = 0;

    try {
      String maxValue = this.getInsuranceMgr().getInsSetting(WebInsQuoteDetail.MAX_SUM_INSURED, WebInsQuote.COVER_INV, action.getQuoteEffectiveDate());

      value = Integer.parseInt(maxValue);

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to determine max investor sum insured: quoteDate: " + action.getQuoteEffectiveDate(), e);
    }

    return value;
  }

  /**
   * Lookup a @Premium based on information in action for given frequency and cover type.
   * @param action
 * @param frequency one of @WebInsQuoteDetail.PAY_ANNUALLY or @WebInsQuoteDetail.PAY_MONTHLY.
 * @param coverType one of @WebInsQuote.COVER_BLDG, @WebInsQuote.COVER_CNTS or @WebInsQuote.COVER_INV.
   * @return
   */
  public Premium retrievePremium(ActionBase action, String frequency, String coverType) {
    Premium premium = null;

    try {
      premium = this.getInsuranceMgr().getPremium(action.getQuoteNo(), frequency, coverType);

      if (premium.getAnnualPremium().compareTo(BigDecimal.ZERO) != 1) { // not > 0
        throw new GenericException("calculated non-positive premium: " + premium.getAnnualPremium());
      }

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to calculate premium for Home Quote #:" + action.getQuoteNo(), e);
    }

    return premium;
  }

  /**
   * Retrieve a list of discount descriptions applicable to this quote.
   * @param action
   * @return
   */
  public List<String> retrieveDiscounts(ActionBase action) {
    List<String> discountList = new ArrayList<String>();
    String quoteType = action.getQuoteType();
    
    // Silver Savers Discount - lookup previously stored value
    if (!action.getQuoteType().equals(WebInsQuote.COVER_INV)) {
      if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS))) {
        discountList.add("Silver Savers Discount");
      }
    }

    // PIP Discount - lookup previously stored value
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.PIP_DISCOUNT))) {
      discountList.add("PIP Discount");
    }

    // Hard wired Smoke Alarm Discount
    if (!quoteType.equals(WebInsQuote.COVER_CNTS)) {
      if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.SMOKE_ALARM))) {
        discountList.add("Hardwired Smoke Alarm Discount");
      }
    }

    if (!quoteType.equals(WebInsQuote.COVER_BLDG) && !action.getQuoteType().equals(WebInsQuote.COVER_INV)) {
      String securityAlarm = this.retrieveQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.SECURITY_ALARM);
      if (securityAlarm.equals(ActionBase.VALUE_SECURITY_MONITORED)) {
          discountList.add("Monitored Security Alarm Discount");
        } else if (securityAlarm.equals(ActionBase.VALUE_SECURITY_LOCAL)) {
          discountList.add("Local Alarm Discount");
        }
    }
    
    return discountList;
  }

  /**
   * Return a list of applicable benefits as Strings.
   * @param action
   * @return
   */
  public List<String> retrieveBenefits(ActionBase action) {
    List<String> benefitList = new ArrayList<String>();

    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_BLDG_ACCIDENTAL_DAMAGE))) {
      benefitList.add("buildingAccidentalDamage");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_CNTS_ACCIDENTAL_DAMAGE))) {
      benefitList.add("contentsAccidentalDamage");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_BLDG_ELECTRIC_MOTORS))) {
      benefitList.add("buildingMotorDamage");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_CNTS_ELECTRIC_MOTORS))) {
      benefitList.add("contentsMotorDamage");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_INV_ELECTRIC_MOTORS))) {
      benefitList.add("investorMotorDamage");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_INV_CONTENTS_ELECTRIC_MOTORS))) {
        benefitList.add("investorContentsMotorDamage");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_BLDG_STORM_DAMAGE))) {
      benefitList.add("buildingStormDamage");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_INV_STORM_DAMAGE))) {
      benefitList.add("investorStormDamage");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_CNTS_DWC))) {
      benefitList.add("workersCompensation");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_CNTS_PERSONAL_EFFECTS))) {
      benefitList.add("unspecifiedPersonalEffects");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_INV_CONTENTS_COVER_30))) {
      benefitList.add("investorCover30");
    }
    if (Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), PremiumOptionsBase.VALUE_INV_CONTENTS_COVER_50))) {
      benefitList.add("investorCover50");
    }

    return benefitList;
  }

  public String retrievePaymentMethod(PolicySupport action) {
    return this.retrieveQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.PAY_METHOD);
  }

  /**
   * Retrieve a list of Specified Contents Items.
   * @param action
   * @return
   */
  public List<InRiskSi> retrieveSpecifiedContents(SpecifiedContentsBase action) {

    List<InRiskSi> items = null;

    try {
      items = this.getInsuranceMgr().getSpecifiedItems(action.getQuoteNo());

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to retrieve Home Quote #" + action.getQuoteNo() + " specified contents: ", e);
    }

    return items;

  }

  /**
   * Store a dynamic list of specified contents.
   * @param action
   * @return
   */
  public boolean storeSpecifiedContents(SpecifiedContents action) {
    boolean success = false;
    ArrayList<InRiskSi> specifiedContents = new ArrayList<InRiskSi>();

    for (int i = 1; i <= action.getCount(); i++) {
      String type = action.getServletRequest().getParameter("type" + i);
      String description = action.getServletRequest().getParameter("description" + i);
      String sumInsured = action.getServletRequest().getParameter("sumInsured" + i);

      if ((type != null && !type.isEmpty())
              || (description != null && !description.isEmpty())
              || (sumInsured != null && !sumInsured.isEmpty())) {

        BigDecimal sumInsuredValue;
        try {
          sumInsuredValue = new BigDecimal(sumInsured.replaceAll("\\$", "").replaceAll(",", ""));
        } catch (Exception e) {
          sumInsuredValue = BigDecimal.ZERO;
        }
        InRiskSi risk = new InRiskSi(action.getQuoteNo(), i, type, description, "", sumInsuredValue);
        specifiedContents.add(risk);
      }
    }

    try {
      // replace specified items
      this.getInsuranceMgr().removeAllSpecifiedItems(action.getQuoteNo());
      this.getInsuranceMgr().setSpecifiedItems(specifiedContents);

      success = true;

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to store Home Quote #" + action.getQuoteNo() + " specified contents: ", e);
    }

    return success;
  }

  /**
   * Store a list of clients (associated policy holders).
   * @param action
   * @return
   */
  public boolean storeClients(PolicyholderDetails action) {
    boolean success = true;
    List<WebInsClient> newClients = action.getNewClients();
    
    for (int i = 1; i <= action.getCount(); i++) {

      WebInsClient existingClient = null;

      String title = action.getServletRequest().getParameter("title" + i);
      title = (title == null) ? null : title.trim();
      String firstName = action.getServletRequest().getParameter("firstName" + i);
      firstName = (firstName == null) ? null : firstName.trim();
      String surname = action.getServletRequest().getParameter("surname" + i);
      surname = (surname == null) ? null : surname.trim();

      String dobDay = action.getServletRequest().getParameter("dob" + i + "Day");
      dobDay = (dobDay == null) ? null : dobDay.trim();
      String dobMonth = action.getServletRequest().getParameter("dob" + i + "Month");
      dobMonth = (dobMonth == null) ? null : dobMonth.trim();
      String dobYear = action.getServletRequest().getParameter("dob" + i + "Year");
      dobYear = (dobYear == null) ? null : dobYear.trim();

      String gender = action.getServletRequest().getParameter("gender" + i);
      gender = (gender == null) ? null : gender.trim();
      String workA = action.getServletRequest().getParameter("workA" + i);
      workA = (workA == null) ? null : workA.trim();
      String workB = action.getServletRequest().getParameter("workB" + i);
      workB = (workB == null) ? null : workB.trim();
      String homeA = action.getServletRequest().getParameter("homeA" + i);
      homeA = (homeA == null) ? null : homeA.trim();
      String homeB = action.getServletRequest().getParameter("homeB" + i);
      homeB = (homeB == null) ? null : homeB.trim();
      String mobileA = action.getServletRequest().getParameter("mobileA" + i);
      mobileA = (mobileA == null) ? null : mobileA.trim();
      String mobileB = action.getServletRequest().getParameter("mobileB" + i);
      mobileB = (mobileB == null) ? null : mobileB.trim();
      String email = action.getServletRequest().getParameter("email" + i);
      email = (email == null) ? null : email.trim();

        if((title == null || title.isEmpty())
                && (firstName == null || firstName.isEmpty())
                && (surname == null || surname.isEmpty())
                && (dobDay == null || dobDay.isEmpty())
                && (dobMonth == null || dobMonth.isEmpty())
                && (dobYear == null || dobYear.isEmpty())
                && (gender == null || gender.isEmpty())
                && (workA == null || workA.isEmpty())
                && (workB == null || workB.isEmpty())
                && (homeA == null || homeA.isEmpty())
                && (homeB == null || homeB.isEmpty())
                && (mobileA == null || mobileA.isEmpty())
                && (mobileB == null || mobileB.isEmpty())
                && (email == null || email.isEmpty())) {
          continue;
        }

      

      for (WebInsClient c : newClients) {
        // try to match existing "new" client
        if (c.getGivenNames().equals(firstName) && c.getSurname().equals(surname) && c.getGender().equals(gender)) {
          existingClient = c;
          break; // exit loop
        }
      }
      
      try {
        WebInsClient client;
        if (existingClient != null) {
          client = existingClient;
        } else {
          client = this.getInsuranceMgr().createInsuranceClient(action.getQuoteNo());
        }

        DateTime dob = new DateTime(dobDay + "/" + dobMonth + "/" + dobYear);
        
        client.setTitle(title);
        client.setGivenNames(firstName);
        client.setSurname(surname);
        client.setGender(gender);
        client.setBirthDate(dob);
        client.setHomePhone(homeA + homeB);
        client.setWorkPhone(workA + workB);
        client.setMobilePhone(mobileA + mobileB);
        client.setEmailAddress(email);
        client.setOwner(true);

        this.getInsuranceMgr().setClient(client);
        success = success && true;

      } catch (Exception e) {
        Config.logger.error(Config.key + ": unable to store Home Quote #" + action.getQuoteNo() + " client: ", e);
        success = false;
      }
    }

    return success;
  }

  public boolean checkPreferredAddress(PolicyholderDetails action) {

    boolean success = false;
    boolean group = false;

    try {
      ArrayList<WebInsClient> clients = this.getInsuranceMgr().getClients(action.getQuoteNo());
      WebInsClient primaryClient = this.retrievePrimaryClient(action);

      if (clients != null) {
        for (WebInsClient client : clients) {
            if(!client.isOwner()) {
                // ignore this client, they are not associated to the home policy
                continue;
            }
            
          client.setPreferredAddress(false);
          if (client.isCltGroup()) {
            client.setPreferredAddress(true);
            client.setOwner(true);
            group = true;
          }
          this.getInsuranceMgr().setClient(client);
        }
      }

      if (primaryClient != null && !group) {
        primaryClient.setPreferredAddress(true);
        primaryClient.setOwner(true);
        this.getInsuranceMgr().setClient(primaryClient);
      }

      success = true;

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to check Preferred Address: ", e);
    }

    return success;

  }

    /**
     * Attach an existing policy holder to this quote.
     */
    public boolean attachClient(int webQuoteNo, WebInsClient existingPolicyHolder) {
        boolean success = false;

        try {
            boolean clientAlreadyAttached = false;
            
            List<WebInsClient> attachedClientsList = this.getInsuranceMgr().getClients(webQuoteNo);
            for(WebInsClient attachedClient : attachedClientsList) {
                if(attachedClient.getSelectionDescription().equals(existingPolicyHolder.getSelectionDescription())) {
                    clientAlreadyAttached = true;
                    break;
                }
            }
            
            WebInsClient client = new WebInsClient();
            if(!clientAlreadyAttached) {
                client = this.getInsuranceMgr().createInsuranceClient(webQuoteNo);
            }

            client = existingPolicyHolder.createShallowCopy(client);

            if(clientAlreadyAttached) {
                client.setWebQuoteNo(webQuoteNo);
                client.setWebClientNo(existingPolicyHolder.getWebClientNo());
            }

            LogUtil.warn(this.getClass(), "Client to attach: " + client.toString());
            
            this.getInsuranceMgr().setClient(client);

            Config.logger.info(Config.key + ": attached existing policy holder: " + existingPolicyHolder.getRactClientNo() + " to Home Quote #" + webQuoteNo);

            success = true;

        } catch (Exception e) {
            Config.logger.error(Config.key + ": unable to attach existing policy holder: " + existingPolicyHolder.getRactClientNo() + " to Home Quote #" + webQuoteNo + ": ", e);
        }

        return success;
    }

  /**
   * Retrieve sum of annual premiums as stored for quote.
   * @param action
   * @return
   */
    public BigDecimal retrieveTotalAnnualPremium(QuoteSubTypeSupport action) {
        try {
            return new BigDecimal(this.retrieveQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM));
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

  /**
   * Retrieve sum of monthly premiums as stored for quote.
   * @param action
   * @return
   */
    public BigDecimal retrieveTotalMonthlyPremium(QuoteSubTypeSupport action) {
        try {
            return new BigDecimal(this.retrieveQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT));
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

  /**
   * Retrieve a list of Clients attached to the quote as well as existing
   * RACTI Policy Holders (based on the Clients associated with Quote, if any)
   * that could be associated with the Quote.
   * @param action
   * @return
   */
    public List<WebInsClient> retrieveClientOptions(PolicyholderDetailsBase action) {
        List<WebInsClient> finalClientList = new ArrayList<WebInsClient>();
        List<WebInsClient> associatedClients = this.retrieveClients(action);
        List<WebInsClient> groupClients = this.retrieveExistingPolicyHolders(action);

        boolean fiftyPlus = Boolean.parseBoolean(this.retrieveQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.FIFTY_PLUS));

        if (associatedClients != null && !associatedClients.isEmpty()) {
            for (WebInsClient client : associatedClients) {
                if (fiftyPlus && client.getBirthDate() != null && !action.isOlderThan(client.getBirthDate(), 50)) {
                    Config.logger.info(Config.key + ": client " + client.getDisplayName() + " is not over 50 but the Home quote type is 50+, skipping");
                    continue;
                }
                if (client.getRactClientNo() == null) {
                    Config.logger.debug(Config.key + ": client " + client.getDisplayName() + " is a new, skipping");
                    continue;
                }
                finalClientList.add(client);
            }
        }

        if (groupClients != null && !groupClients.isEmpty()) {
            groupClients: for (WebInsClient groupClient : groupClients) {

                if (fiftyPlus && groupClient.getBirthDate() != null && !action.isOlderThan(groupClient.getBirthDate(), 50)) {
                    Config.logger.info(Config.key + ": client " + groupClient.getDisplayName() + " is not over 50 but the Home quote type is 50+, skipping");
                    continue groupClients;
                }

                for (WebInsClient existing : finalClientList) {
                    if(isSameClientNumber(existing.getRactClientNo(), groupClient.getRactClientNo()) && isSameGroupListString(existing.getGroupListString(), groupClient.getGroupListString())) {
                        // existing client has already been added
                        continue groupClients;
                    }
                }
                finalClientList.add(groupClient);
            }
        }
        return finalClientList;
    }

    private boolean isSameClientNumber(Integer first, Integer second) {
        return first != null && second != null && first.equals(second);
    }
    
    private boolean isSameGroupListString(String first, String second) {
        return first != null && second != null && first.equals(second);
    }
    
  /**
   * Retrieve a list of Existing policy holders that could be associated with this quote.
   * @param action
   * @return
   */
  public List<WebInsClient> retrieveExistingPolicyHolders(PolicySupport action) {
    List<WebInsClient> clients = null;
    try {
      clients = this.getInsuranceMgr().getClientGroups(action.getQuoteNo());
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to retrieve existing policy holders for Home Quote #" + action.getQuoteNo(), e);
    }
    return clients;
  }

  /**
   * Retrieve a list of Clients associated with this quote.
   * @param action
   * @return
   */
  public List<WebInsClient> retrieveClients(ActionBase action) {
    List<WebInsClient> clients = null;

    try {
      clients = this.getInsuranceMgr().getClients(action.getQuoteNo());
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to retrieve clients for Home Quote #" + action.getQuoteNo(), e);
    }

    return clients;
  }
  
    /**
     * Retrieve a list of Clients associated with this quote, and sets driver, owner, preferred address to 0 for all of them.
     * 
     * @param action
     * @return
     */
    public boolean detachCurrentClients(PolicyholderDetails action, boolean detachExistingOnly) {
        boolean success = true;
        List<WebInsClient> clients = null;

        try {
            clients = this.getInsuranceMgr().getClients(action.getQuoteNo());
        } catch (Exception e) {
            Config.logger.error(Config.key + ": unable to retrieve clients for Home Quote #" + action.getQuoteNo(), e);
            success = false;
        }

        if (null != clients) {
            for (WebInsClient client : clients) {
                if(detachExistingOnly && null == client.getRactClientNo()) {
                    // This is a new client (no RACT ID), so skip it
                    continue;
                }
                client.setDriver(false);
                client.setOwner(false);
                client.setPreferredAddress(false);
                try {
                    this.getInsuranceMgr().setClient(client);
                } catch (Exception e) {
                    Config.logger.error(Config.key + ": unable to update WebInsClient details for Client #" + client.getWebClientNo(), e);
                    success = false;
                }
                
                // since we are removing clients, clear the local list
                action.setNewClients(null);
            }
        }
        return success;
    }

  /**
   * Retrieve a list of New Clients associated with this quote.
   * New Clients are clients the user has just created who have no ractClientNo.
   * @param action
   * @return
   */
  public List<WebInsClient> retrieveNewClients(PolicySupport action) {
    List<WebInsClient> newClients = new ArrayList<WebInsClient>();
    List<WebInsClient> allClients = this.retrieveClients(action);

    if(allClients != null) {
      for (WebInsClient client : allClients) {
        if (client.getRactClientNo() == null) {
          newClients.add(client);
        }
      }
    }

    return newClients;
  }

  /**
   * Retrieve a String representing policyholders names.
   * @param action
   * @return
   */
    public String retrievePolicyholderNames(AddressDetailsBase action) {
        StringBuilder names = new StringBuilder();
        List<WebInsClient> clients = this.retrieveClients(action);

        boolean first = true;
        if (clients != null) {
            for (WebInsClient client : clients) {
                if (!client.isOwner() && !client.isDriver() && !client.isPreferredAddress()) {
                    continue;
                }
                if (!first) {
                    names.append("<br />");
                }
                names.append(client.getSelectionDescription());
                first = false;
            }
        }
        return names.toString();
    }

  /**
   * Store postal address details against primary client.
   * @param action
   * @return
   */
  public boolean storeClientAddressDetails(AddressDetails action) {
    boolean success = false;

    try {
      List<WebInsClient> clients = this.getInsuranceMgr().getClients(action.getQuoteNo());

      if (clients != null) {
        for (WebInsClient client : clients) {

          if (action.getResidentialPostcode() != null && !action.getResidentialPostcode().isEmpty()) {
            client.setResiPostcode(action.getResidentialPostcode());
            client.setResiSuburb(action.getResidentialSuburb());
            client.setResiStreet(action.getResidentialStreet());
            client.setResiStreetChar(action.getResidentialStreetNumber());
            client.setResiProperty("");

            client.setResiStsubid(this.lookupStreetSuburbId(action.getResidentialSuburb(), action.getResidentialStreet(), action.getResidentialPostcode()));

            client = attemptQasRiskAddress(action.getQuoteNo(), client);
            
            if(action.isDifferntPostalAddress()) {
              client.setPostPostcode(action.getPoBoxPostcode());
              client.setPostSuburb(action.getPoBoxSuburb());
              client.setPostStreet(""); // clear value
              client.setPostStreetChar("");

              client.setPostProperty(action.getPoBoxNumber());

              // pass empty string if no Street available
              client.setPostStsubid(this.lookupStreetSuburbId(action.getPoBoxSuburb(), "", action.getPoBoxPostcode()));

            } else { // GPO/PO Box Defaults to Residential address
              client.setPostPostcode(action.getResidentialPostcode());
              client.setPostSuburb(action.getResidentialSuburb());
              client.setPostStreet(action.getResidentialStreet()); // clear value
              client.setPostStreetChar(action.getResidentialStreetNumber());
              client.setPostProperty("");

              // pass empty string if no Street available
              client.setPostStsubid(this.lookupStreetSuburbId(action.getResidentialSuburb(), action.getResidentialStreet(), action.getResidentialPostcode()));
            }

            // update email if passed
            if (action.getEmail() != null && !action.getEmail().isEmpty()) {
              client.setEmailAddress(action.getEmail());
            }

            try {
              this.getInsuranceMgr().setClient(client);
              success = true;
            } catch (Exception e) {
              Config.logger.error(Config.key + ": unable to update postal address for clients for Home Quote #" + action.getQuoteNo(), e);
            }


          }
        }
      }
      
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to retrieve existing policyholders for Home Quote #" + action.getQuoteNo(), e);
    }


    return success;
  }

  /**
   * Retrieve the primary client for this quote.
   * @param action
   * @return
   */
  public WebInsClient retrievePrimaryClient(PolicySupport action) {
    List<WebInsClient> clients = this.retrieveClients(action);
    if (clients != null && !clients.isEmpty()) {
        // return the first owner as the primary
        for(WebInsClient client : clients) {
            if(client.isOwner()) {
                return client;                
            }
        }
    }

    return null;
  }

  /**
   * Commence cover for this quote.
   * Send cover note email.
   * @param action
   * @return
   */
    public boolean commenceCover(PaymentOptions action) {
        boolean success = false;

        // Martial the payment details into the appropriate fields for submission 
        String type = "";
        String number = "";
        String name = "";
        if (action.getDirectDebitMethod().equals(ActionBase.FIELD_DD_BANK_ACCOUNT)) {
            type = WebInsQuoteDetail.ACC_TYPE_DEBIT;
            number = action.getAccountNo();
            name = action.getAccountName();
        } else {
            type = action.getCardType();
            number = action.getCreditCard();
            name = action.getCreditCardName();
        }
        String bsb = null == action.getBsb() ? "" : action.getBsb();
        String expiry = null == action.getExpiryDate() ? "" : action.getExpiryDate();
        String day = "1";
        String frequency = null == action.getFrequency() ? "" : action.getFrequency();

        try {
            this.getInsuranceMgr().startCover(action.getQuoteNo(), type, bsb, number, expiry, name, day, frequency);
            //this.getInsuranceMgr().setQuoteStatus(action.getQuoteNo(), WebInsQuote.COVER_ISSUED);

            String confirmationEmail = this.retrievePrimaryClient(action).getEmailAddress();
            this.getInsuranceMgr().setQuoteDetail(action.getQuoteNo(), WebInsQuoteDetail.CONFIRMATION_EMAIL_ADDRESS, confirmationEmail);

            if (confirmationEmail != null && !confirmationEmail.isEmpty()) {
                this.getInsuranceMgr().printCoverDoc(action.getQuoteNo());
            }
            success = true;
        } catch (Exception e) {
            Config.logger.error(Config.key + ": unable to commence cover for Home Quote #" + action.getQuoteNo(), e);
        }

        return success;
    }


  /*
   * Upgrades cover to home and contents.
   */
  public boolean upgradeCover(SumInsured action) {

    boolean success = false;

    try {
      action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);

      this.getInsuranceMgr().setCoverType(action.getQuoteNo(), action.getQuoteType());
      this.storeQuoteDetail(action, ActionBase.FIELD_QUOTE_SUB_TYPE, action.getQuoteType());

      success = true;

    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to upgrade cover for Home Quote #" + action.getQuoteNo(), e);
    }

    return success;
  }


  /**
   * Stores start cover date.
   *
   */
  public boolean storeCoverStartDate(AboutYourHomePolicy action) {
    boolean success = false;

    try {
      this.getInsuranceMgr().setStartCoverDate(action.getQuoteNo(), action.getCoverStartDate());
      success = true;
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to set cover start date for Home Quote #" + action.getQuoteNo(), e);
    }

    return success;
  }

  /**
   * Retrieve start cover date.
   */
  public DateTime getCoverStartDate(int quoteNo) {
    DateTime startCover = new DateTime();

    try {
      startCover = this.getInsuranceMgr().getQuote(quoteNo).getStartCover();
    } catch (Exception e) {
      Config.logger.error(Config.key + ": unable to get cover start date for Home Quote #" + quoteNo, e);
    }

    return startCover;
  }
  
}
