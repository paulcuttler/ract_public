package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.*;

/**
 * A temp memory storage bean saves data for page reloading.
 * @author xmfu
 */
public class QuoteTempMemoryObject {
  private String memberCard;
  private String premiumType;
  private boolean withWideScreen=false;
  private boolean withCarHire=false;
  private String excess;
  private boolean withFireAndTheft=false;
  private String compPremiumAnual;
  private String compPremiumMonth;
  private String premium;
  private String accountType;
  private Boolean payByDirectDebit;
  private String accountName;
  private String expiryDate;
  private String creditCard;
  private String bankCard;
  private String directDebtDate;
  private String fullLicenseYear;
  private int ncb;
  private String numberOfAccidents;
  private String noClaimBonus;
  private String paymentOption;
  private String coverStart;
  private List<Boolean> isPolicyHolders = new ArrayList<Boolean>();
  private Boolean page17Checked;
  private String policyHolderOption;

  /**
   * @return the memberCard
   */
  public String getMemberCard() {
    return memberCard;
  }

  /**
   * @param memberCard the memberCard to set
   */
  public void setMemberCard(String memberCard) {
    this.memberCard = memberCard;
  }

  /**
   * @return the premiumType
   */
  public String getPremiumType() {
    return premiumType;
  }

  /**
   * @param premiumType the premiumType to set
   */
  public void setPremiumType(String premiumType) {
    this.premiumType = premiumType;
  }

  /**
   * @return the withWideScreen
   */
  public boolean isWithWideScreen() {
    return withWideScreen;
  }

  /**
   * @param withWideScreen the withWideScreen to set
   */
  public void setWithWideScreen(boolean withWideScreen) {
    this.withWideScreen = withWideScreen;
  }

  /**
   * @return the withCarHire
   */
  public boolean isWithCarHire() {
    return withCarHire;
  }

  /**
   * @param withCarHire the withCarHire to set
   */
  public void setWithCarHire(boolean withCarHire) {
    this.withCarHire = withCarHire;
  }

  /**
   * @return the excess
   */
  public String getExcess() {
    return excess;
  }

  /**
   * @param excess the excess to set
   */
  public void setExcess(String excess) {
    this.excess = excess;
  }

  /**
   * @return the withFireAndTheft
   */
  public boolean isWithFireAndTheft() {
    return withFireAndTheft;
  }

  /**
   * @param withFireAndTheft the withFireAndTheft to set
   */
  public void setWithFireAndTheft(boolean withFireAndTheft) {
    this.withFireAndTheft = withFireAndTheft;
  }

  /**
   * @return the premium
   */
  public String getPremium() {
    return premium;
  }

  /**
   * @param premium the premium to set
   */
  public void setPremium(String premium) {
    this.premium = premium;
  }

  /**
   * @return the accountType
   */
  public String getAccountType() {
    return accountType;
  }

  /**
   * @param accountType the accountType to set
   */
  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  /**
   * @return the payByDirectDebit
   */
  public Boolean isPayByDirectDebit() {
    return payByDirectDebit;
  }

  /**
   * @param payByDirectDebit the payByDirectDebit to set
   */
  public void setPayByDirectDebit(Boolean payByDirectDebit) {
    this.payByDirectDebit = payByDirectDebit;
  }

  /**
   * @return the accountName
   */
  public String getAccountName() {
    return accountName;
  }

  /**
   * @param accountName the accountName to set
   */
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  /**
   * @return the expiryDate
   */
  public String getExpiryDate() {
    return expiryDate;
  }

  /**
   * @param expiryDate the expiryDate to set
   */
  public void setExpiryDate(String expiryDate) {
    this.expiryDate = expiryDate;
  }

  /**
   * @return the creditCard
   */
  public String getCreditCard() {
    return creditCard;
  }

  /**
   * @param creditCard the creditCard to set
   */
  public void setCreditCard(String creditCard) {
    this.creditCard = creditCard;
  }

  /**
   * @return the bankCard
   */
  public String getBankCard() {
    return bankCard;
  }

  /**
   * @param bankCard the bankCard to set
   */
  public void setBankCard(String bankCard) {
    this.bankCard = bankCard;
  }

  /**
   * @return the directDebtDate
   */
  public String getDirectDebtDate() {
    return directDebtDate;
  }

  /**
   * @param directDebtDate the directDebtDate to set
   */
  public void setDirectDebtDate(String directDebtDate) {
    this.directDebtDate = directDebtDate;
  }

  /**
   * @return the fullLicenseYear
   */
  public String getFullLicenseYear() {
    return fullLicenseYear;
  }

  /**
   * @param fullLicenseYear the fullLicenseYear to set
   */
  public void setFullLicenseYear(String fullLicenseYear) {
    this.fullLicenseYear = fullLicenseYear;
  }

  /**
   * @return the ncb
   */
  public int getNcb() {
    return ncb;
  }

  /**
   * @param ncb the ncb to set
   */
  public void setNcb(int ncb) {
    this.ncb = ncb;
  }

  /**
   * @return the numberOfAccidents
   */
  public String getNumberOfAccidents() {
    return numberOfAccidents;
  }

  /**
   * @param numberOfAccidents the numberOfAccidents to set
   */
  public void setNumberOfAccidents(String numberOfAccidents) {
    this.numberOfAccidents = numberOfAccidents;
  }

  /**
   * @return the noClaimBonus
   */
  public String getNoClaimBonus() {
    return noClaimBonus;
  }

  /**
   * @param noClaimBonus the noClaimBonus to set
   */
  public void setNoClaimBonus(String noClaimBonus) {
    this.noClaimBonus = noClaimBonus;
  }

  /**
   * @return the compPremiumAnual
   */
  public String getCompPremiumAnual() {
    return compPremiumAnual;
  }

  /**
   * @param compPremiumAnual the compPremiumAnual to set
   */
  public void setCompPremiumAnual(String compPremiumAnual) {
    this.compPremiumAnual = compPremiumAnual;
  }

  /**
   * @return the compPremiumMonth
   */
  public String getCompPremiumMonth() {
    return compPremiumMonth;
  }

  /**
   * @param compPremiumMonth the compPremiumMonth to set
   */
  public void setCompPremiumMonth(String compPremiumMonth) {
    this.compPremiumMonth = compPremiumMonth;
  }

  /**
   * @return the paymentOption
   */
  public String getPaymentOption() {
    return paymentOption;
  }

  /**
   * @param paymentOption the paymentOption to set
   */
  public void setPaymentOption(String paymentOption) {
    this.paymentOption = paymentOption;
  }

  /**
   * @return the coverStart
   */
  public String getCoverStart() {
    return coverStart;
  }

  /**
   * @param coverStart the coverStart to set
   */
  public void setCoverStart(String coverStart) {
    this.coverStart = coverStart;
  }

  /**
   * @return the isPolicyHolders
   */
  public List<Boolean> getIsPolicyHolders() {
    return isPolicyHolders;
  }

  /**
   * @param isPolicyHolders the isPolicyHolders to set
   */
  public void setIsPolicyHolders(List<Boolean> isPolicyHolders) {
    this.isPolicyHolders = isPolicyHolders;
  }

  /**
   * @return the page17Checked
   */
  public Boolean getPage17Checked() {
    return page17Checked;
  }

  /**
   * @param page17Checked the page17Checked to set
   */
  public void setPage17Checked(Boolean page17Checked) {
    this.page17Checked = page17Checked;
  }

  /**
   * @return the policyHolderOption
   */
  public String getPolicyHolderOption() {
    return policyHolderOption;
  }

  /**
   * @param policyHolderOption the policyHolderOption to set
   */
  public void setPolicyHolderOption(String policyHolderOption) {
    this.policyHolderOption = policyHolderOption;
  }
}
