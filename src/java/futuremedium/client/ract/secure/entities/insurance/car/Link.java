package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.List;

/**
 * This bean class holds information for links in quote question.
 * @author xmfu
 */
public class Link {
  private String id;
  private List<Event> events;
  private boolean hasEvents;
  private String text;

  /**
   * @return the events
   */
  public List<Event> getEvents() {
    return events;
  }

  /**
   * @param events the events to set
   */
  public void setEvents(List<Event> events) {
    this.events = events;
  }

  /**
   * @return the hasEvents
   */
  public boolean isHasEvents() {
    return hasEvents;
  }

  /**
   * @param hasEvents the hasEvents to set
   */
  public void setHasEvents(boolean hasEvents) {
    this.hasEvents = hasEvents;
  }

  /**
   * @return the text
   */
  public String getText() {
    return text;
  }

  /**
   * @param text the text to set
   */
  public void setText(String text) {
    this.text = text;
  }

  /**
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }
}
