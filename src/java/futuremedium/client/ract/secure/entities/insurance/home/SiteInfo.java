package futuremedium.client.ract.secure.entities.insurance.home;

import java.util.*;

import futuremedium.common2.website.*;

import futuremedium.client.ract.secure.actions.RactActionSupport;
import futuremedium.client.ract.secure.actions.insurance.home.*;

/**
 * Model for defining application flow and presentation characteristics.
 * @author
 */
public class SiteInfo extends BaseSiteInfo {
    private static final long serialVersionUID = -6363753656301741449L;
    public static SiteInfo siteInfo;
    public static final int AP_SITE_ID = 0;

  @Override
  public Page getIndexPage() { return (Page) this.getAllPages().get(0); }

  @Override
  public Page getErrorPage() { return null; }

  @Override
  public int getSiteId() {
    return AP_SITE_ID;
  }

  @Override
  public String getBaseHref() {
    return "/";
  }

  public SiteInfo() {

    Page page;

    page = this.addPageToSite(getRootPage(), NewQuoteForm.class);
    page.setSuppressNav(true);
    page.setNewOrSave(true);
    page.setAllowSave(false);
    page.setNewQuote(true);
    page.setHomeNav(true);

    page = this.addPageToSite(getRootPage(), RetrieveQuoteForm.class);
    page.setTitle("Retrieve Saved Quote");
    page.setSuppressNav(true);
    page.setNewOrSave(true);
    page.setOptionalStep(true);
    page.setAllowSave(false);
    page.setHomeNav(true);

    page = this.addPageToSite(getRootPage(), ChecklistForm.class);
    page.setIncludeInNav(false);
    page.setSuppressNav(true);
    page.setChecklist(true);
    page.setOptionalStep(true);
    page.setAllowSave(false);

    page = this.addPageToSite(getRootPage(), SaveQuoteForm.class);
    page.setIncludeInNav(false);
    page.setSuppressNav(true);
    page.setSaved(true);
    page.setOptionalStep(true);
    page.setClose(true);
    page.setResume(true);
    page.setAllowSave(false);

    page = this.addPageToSite(getRootPage(), AboutYouForm.class);
    page.setTitle("About you");

    page = this.addPageToSite(getRootPage(), WhereYouLiveForm.class);
    page.setTitle("Where you live");
    page.setDojoFilteringSelect(true);

    page = this.addPageToSite(getRootPage(), AboutYourHomeForm.class);
    page = this.addPageToSite(getRootPage(), SumInsuredForm.class);
    page.setTitle("Sum insured");
    
    page = this.addPageToSite(getRootPage(), CalculatorForm.class);
    page.setIncludeInNav(false);
    page.setSuppressNav(true);
    page.setCalculator(true);
    page.setOptionalStep(true);
    

    page = this.addPageToSite(getRootPage(), PremiumOptionsForm.class);
    page.setTitle("Premium / Options");
    page.setBenefits(true);
    page.setFinaliseQuote(true);
    
    page = this.addPageToSite(getRootPage(), QuoteForm.class);
    page.setBenefits(true);
    page.setPaymentOptions(true);
    page.setPurchasePolicy(true);
    page.setNavCompletedDisabled(true);

    page = this.addPageToSite(getRootPage(), AboutYourHomePolicyForm.class);
    page.setTitle("About your home");
    page.setBenefits(true);

    page = this.addPageToSite(getRootPage(), AboutYouPolicyForm.class);
    page.setTitle("About you");
    page.setBenefits(true);

    page = this.addPageToSite(getRootPage(), SpecifiedContentsForm.class);
    page.setBenefits(true);
    page.setExcludedFromFlow(ActionBase.VALUE_QUOTE_TYPE_BLDG_ONLY + "," + ActionBase.VALUE_QUOTE_TYPE_INV);

    page = this.addPageToSite(getRootPage(), PolicyholderDetailsForm.class);
    page.setBenefits(true);

    page = this.addPageToSite(getRootPage(), AddressDetailsForm.class);
    page.setDojoFilteringSelect(true);
    page.setBenefits(true);

    page = this.addPageToSite(getRootPage(), DeclarationForm.class);
    page.setBenefits(true);

    page = this.addPageToSite(getRootPage(), PaymentOptionsForm.class);
    page.setBenefits(true);

    page = this.addPageToSite(getRootPage(), FinalisePolicyForm.class);
    page.setBenefits(true);
    page.setClose(true);
    page.setAllowSave(false);
    page.setFinished(true);
    page.setNavCompletedDisabled(true);

    page = this.addPageToSite(getRootPage(), SurveyForm.class);
    page.setOptionalStep(true);
    page.setClose(true);
    page.setSuppressNav(true);
    page.setSurvey(true);
    page.setAllowSave(false);
    page.setExit(true);

    page = this.addPageToSite(getRootPage(), ExitForm.class);
    page.setOptionalStep(true);
    page.setClose(true);
    page.setSuppressNav(true);
    page.setSurvey(true);
    page.setResume(true);
    page.setAllowSave(false);
    page.setExit(true);
    

    page = this.addPageToSite(getRootPage(), ReminderForm.class);
    page.setOptionalStep(true);
    page.setClose(true);
    page.setSuppressNav(true);
    page.setReminder(true);
    page.setResume(true);
    page.setAllowSave(false);
    page.setExit(true);

  }

  @Override
  public Page getNewPage(BasePage parent, String title, String englishURI) {
    return new Page(parent, title, englishURI, this);
  }

  public Page addPageToSite(BasePage parent, Class theClass) {
    String name = RactActionSupport.unCamelise(theClass.getSimpleName().replace(ActionBase.FORM_ACTION_SUFFIX, "")); // Strip "Form"
    String lookup = ActionBase.FORM_ACTION_PREFIX + theClass.getSimpleName();

    Page page = (Page) this.addPageToSite(parent, name, lookup);
    page.setURI(lookup);
    page.setAction(lookup);

    try {
      page.setQuoteOnly(theClass.newInstance() instanceof QuoteAware);
      page.setPolicyOnly(theClass.newInstance() instanceof PolicyAware);
    } catch (Exception e) {}

    return page;
  }

  public List<String> getFlow() {
    List<String> flow = new ArrayList<String>();

    for (BasePage p : this.getAllPages()) {
      if (!((Page) p).isOptionalStep()) {
        flow.add(p.getURI());
      }
    }

    return flow;
  }

  public List<Page> getPageFlow() {
    List<Page> flow = new ArrayList<Page>();

    for (BasePage p : this.getAllPages()) {
      if (!((Page) p).isOptionalStep()) {
        flow.add((Page) p);
      }
    }

    return flow;
  }

    /**
   * Singleton accessor method.
   */
  public static SiteInfo getInstance() {
    if (siteInfo == null) {
      siteInfo = new SiteInfo();
    }
    return siteInfo;
  }


}
