package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.*;

/**
 * Policy holder bean class holds properties of a policy holder.
 * @author xmfu
 */
public class PolicyHolder {
  private long id;
  private String firstName;
  private String surName;
  private long yearCommencedDriving;
  private String gender;
  private String licenseSuspended;
  private String insuranceCancelled;
  private String policyHolder;
  private String title;
  private String homePhoneNumber;
  private String workPhoneNumber;
  private String mobilePhoneNumber;
  private String postAddress;
  private String emailAddress;
  private List<QuoteAccident> quoteAccidents;

  /**
   * @return the id
   */
  public long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(long id) {
    this.id = id;
  }

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * @return the surName
   */
  public String getSurName() {
    return surName;
  }

  /**
   * @param surName the surName to set
   */
  public void setSurName(String surName) {
    this.surName = surName;
  }

  /**
   * @return the yearCommencedDriving
   */
  public long getYearCommencedDriving() {
    return yearCommencedDriving;
  }

  /**
   * @param yearCommencedDriving the yearCommencedDriving to set
   */
  public void setYearCommencedDriving(long yearCommencedDriving) {
    this.yearCommencedDriving = yearCommencedDriving;
  }

  /**
   * @return the gender
   */
  public String getGender() {
    return gender;
  }

  /**
   * @param gender the gender to set
   */
  public void setGender(String gender) {
    this.gender = gender;
  }

  /**
   * @return the licenseSuspended
   */
  public String getLicenseSuspended() {
    return licenseSuspended;
  }

  /**
   * @param licenseSuspended the licenseSuspended to set
   */
  public void setLicenseSuspended(String licenseSuspended) {
    this.licenseSuspended = licenseSuspended;
  }

  /**
   * @return the insuranceCancelled
   */
  public String getInsuranceCancelled() {
    return insuranceCancelled;
  }

  /**
   * @param insuranceCancelled the insuranceCancelled to set
   */
  public void setInsuranceCancelled(String insuranceCancelled) {
    this.insuranceCancelled = insuranceCancelled;
  }

  /**
   * @return the policyHolder
   */
  public String getPolicyHolder() {
    return policyHolder;
  }

  /**
   * @param policyHolder the policyHolder to set
   */
  public void setPolicyHolder(String policyHolder) {
    this.policyHolder = policyHolder;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the homePhoneNumber
   */
  public String getHomePhoneNumber() {
    return homePhoneNumber;
  }

  /**
   * @param homePhoneNumber the homePhoneNumber to set
   */
  public void setHomePhoneNumber(String homePhoneNumber) {
    this.homePhoneNumber = homePhoneNumber;
  }

  /**
   * @return the workPhoneNumber
   */
  public String getWorkPhoneNumber() {
    return workPhoneNumber;
  }

  /**
   * @param workPhoneNumber the workPhoneNumber to set
   */
  public void setWorkPhoneNumber(String workPhoneNumber) {
    this.workPhoneNumber = workPhoneNumber;
  }

  /**
   * @return the mobilePhoneNumber
   */
  public String getMobilePhoneNumber() {
    return mobilePhoneNumber;
  }

  /**
   * @param mobilePhoneNumber the mobilePhoneNumber to set
   */
  public void setMobilePhoneNumber(String mobilePhoneNumber) {
    this.mobilePhoneNumber = mobilePhoneNumber;
  }

  /**
   * @return the postAddress
   */
  public String getPostAddress() {
    return postAddress;
  }

  /**
   * @param postAddress the postAddress to set
   */
  public void setPostAddress(String postAddress) {
    this.postAddress = postAddress;
  }

  /**
   * @return the emailAddress
   */
  public String getEmailAddress() {
    return emailAddress;
  }

  /**
   * @param emailAddress the emailAddress to set
   */
  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  /**
   * @return the accidentMonth
   */
  public Map<Integer, String> getAccidentMonth() {
    Map<Integer, String> accidentMonth = new HashMap<Integer, String>();
    accidentMonth.put(0, "Please Select Month");
    for (int i = 1; i <=12; i++) {
      accidentMonth.put(i, String.valueOf(i));
    }
    return accidentMonth;
  }

  /**
   * @return the accidentYear
   */
  public Map<Integer, String> getAccidentYear() {
    Map<Integer, String> accidentYear = new LinkedHashMap<Integer, String>();
    Calendar calendar = Calendar.getInstance();
    Date date = new Date();
    
    calendar.setTime(date);
    int year = calendar.get(Calendar.YEAR);
    accidentYear.put(0, "Please Select Year");

    for (int i = (year - 5); i <= year; i++) {
      accidentYear.put(i, String.valueOf(i));
    }

    return accidentYear;
  }

  /**
   * @return the quoteAccidents
   */
  public List<QuoteAccident> getQuoteAccidents() {
    return quoteAccidents;
  }

  /**
   * @param quoteAccidents the quoteAccidents to set
   */
  public void setQuoteAccidents(List<QuoteAccident> quoteAccidents) {
    this.quoteAccidents = quoteAccidents;
  }

  public int getNumberOfAccident() {
    if (this.quoteAccidents != null) {
      return this.quoteAccidents.size();
    } else {
      return 0;
    }
  }
}
