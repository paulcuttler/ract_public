package futuremedium.client.ract.secure.entities.insurance.car;

import com.thoughtworks.xstream.io.xml.DomReader;
import java.io.File;
import java.util.*;
import java.text.SimpleDateFormat;

import org.dom4j.*;
import org.dom4j.io.SAXReader;

import futuremedium.client.ract.secure.*;
 
/**
 * This class is a xml parser which reads quote questions from insurance_quote_question.xml.
 * @author xmfu
 */
public class QuoteQuestionManager {

  private Document document = null;
  private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  
  private final int TOTAL_DELETED = 1;  //repesents how many question have me deleted from the process

  public QuoteQuestionManager() {
    try {
      parse(new File(Config.configManager.getProperty("insurance.question.xml.path")));
 
    } catch (Exception e) {
      //@todo log xml parsing error.
      e.printStackTrace();
    }
  }

  public int getPageNumbers() {
    
    List list = document.selectNodes("//page");

    if (list != null) {
      return list.size() + TOTAL_DELETED;
    } else {
      return 0;
    }
  }

  public String getAjaxReturnMessage(long pageId, String questionId, String subQuestionId, boolean validation) {
    String xpath = "//page[@id='" + pageId + "']/question[@id='" + questionId + "']/subQuestions/subQuestion[@id='" + subQuestionId + "']";
    Element subQuestionElement = (Element) document.selectSingleNode(xpath);
    QuoteQuestion subQuestion = this.getQuestion(subQuestionElement);

    if (subQuestion.getType() == QuestionType.RADIO || subQuestion.getType() == QuestionType.TEXT) {
      for (Text text : subQuestion.getTexts()) {
        for (Event event : text.getEvents()) {
          for (AjaxReturn returnMessage : event.getAjaxReturnMessage()) {
            if (returnMessage.isValidation() == validation) {
              return returnMessage.getMessage();
            }
          }
        }
      }
    } else if (subQuestion.getType() == QuestionType.SELECT) {
    }

    return "";
  }

  public QuoteQuestion getQuestion(Element questionElement) {
    // Create new QuoteQuestion object.
    QuoteQuestion quoteQuestion = new QuoteQuestion();

    // Get question attributes.
    String questionType = questionElement.attributeValue("type");

    String display = questionElement.attributeValue("display");
    String helpOption = questionElement.elementText("helpOption");
    String save = questionElement.elementText("save");
    String questionId = questionElement.attributeValue("id");
    String title = questionElement.elementText("title");
    String cssClass = questionElement.attributeValue("class");
    String includeHTML = questionElement.elementText("includeHTML");
    String heading = questionElement.elementText("heading");
    String notes = questionElement.elementText("notes");
    String mandatory = questionElement.attributeValue("mandatory");
    
    // add question attributes.
    if (includeHTML != null) {
      quoteQuestion.setIncludeHTML(includeHTML.trim());
    } else {
      quoteQuestion.setIncludeHTML("");
    }
    
    if (cssClass != null) {
      quoteQuestion.setCssClass(cssClass.trim());
    } else {
      quoteQuestion.setCssClass("");
    }

    if (notes != null) {
      quoteQuestion.setNotes(notes.trim());
    } else {
      quoteQuestion.setNotes("");
    }

    if (heading != null) {
      quoteQuestion.setHeading(heading.trim());
    } else {
      quoteQuestion.setHeading("");
    }

    if (display != null) {
      if (display.trim().equals("show")) {
        quoteQuestion.setDisplay(true);
      } else {
        quoteQuestion.setDisplay(false);
      }
    } else {
      quoteQuestion.setDisplay(false);
    }

    if (helpOption != null) {
      quoteQuestion.setHelpOption(helpOption.trim());
    } else {
      quoteQuestion.setHelpOption("");
    }

    if (questionId != null) {
      quoteQuestion.setId((questionId.trim()));
    } else {
      quoteQuestion.setId("");
    }

    if (title != null) {
      quoteQuestion.setTitle(title.trim());
    } else {
      quoteQuestion.setTitle("");
    }

    if (questionType != null) {
      quoteQuestion.setType(QuestionType.getType(questionType.trim()));
      quoteQuestion.setTypeString(quoteQuestion.getType().getValue());
    } else {
      quoteQuestion.setType(null);
      quoteQuestion.setTypeString("");
    }

    if (mandatory != null) {
      if (mandatory.equals("yes")) {
        quoteQuestion.setMandatory(true);
      } else {
        quoteQuestion.setMandatory(false);
      }
    } else {
      quoteQuestion.setMandatory(false);
    }

    if (save != null) {
      if (save.equals("yes")) {
        quoteQuestion.setSave(true);
      } else {
        quoteQuestion.setSave(false);
      }
    } else {
      quoteQuestion.setSave(false);
    }

    if (quoteQuestion.getType() == QuestionType.NONE) {
    } else if (quoteQuestion.getType() == QuestionType.SELECT) {
      // Get options.
      quoteQuestion.setOptions(getOptions(questionElement));
      OptionRange optionRange = getOptionRange(questionElement);

      if (optionRange != null) {
        quoteQuestion.setOptions(getRangedOptions(optionRange));
      }

      if (quoteQuestion.getOptions().size() != 0) {
        quoteQuestion.setName(quoteQuestion.getOptions().get(0).getName());
      } else {
        quoteQuestion.setName(getOptionsName(questionElement));
      }

      // Get events.
      List<Event> events = this.getEvents(questionElement);
      if (events != null && events.size() > 0) {
        quoteQuestion.setEvent(events.get(0));
      } else {
        quoteQuestion.setEvent(null);
      }

      // Get links.
      Link link = this.getLink(questionElement);

      if (link != null) {
        quoteQuestion.setLink(link);
      } else {
        quoteQuestion.setLink(null);
      }

    } else if (quoteQuestion.getType() == QuestionType.RADIO) {
      // Get texts.
      quoteQuestion.setTexts(getTexts(questionElement));
    } else if (quoteQuestion.getType() == QuestionType.TEXT) {
      // get texts.
      quoteQuestion.setTexts(getTexts(questionElement));
    } else if (quoteQuestion.getType() == QuestionType.CHECK_BOX) {
      // Get texts.
      quoteQuestion.setTexts(getTexts(questionElement));
    } else if (quoteQuestion.getType() == QuestionType.TEXT_SHOW) {
      quoteQuestion.setTexts(getTexts(questionElement));
    } else if (quoteQuestion.getType() == QuestionType.TEXT_HEADING) {
      // Only one event in question type TEXT_HEADING.
      quoteQuestion.setEvent(this.getEvents(questionElement).get(0));
    } else if (quoteQuestion.getType() == QuestionType.CAPTCHA) {
    } else if (quoteQuestion.getType() == QuestionType.SHOW_HEADER) {
    } else if (quoteQuestion.getType() == QuestionType.TEXT_DESCRIPTION) {
      Element descriptionElement = questionElement.element("description");

      if (descriptionElement != null) {
        List<String> description = new ArrayList<String>();

        for (Iterator iter = descriptionElement.elementIterator("paragraph"); iter.hasNext();) {
          Element lineElement = (Element) iter.next();
          description.add(lineElement.getTextTrim());
        }

        quoteQuestion.setTextDescription(description);
      }
    }

    return quoteQuestion;
  }

  public Link getLink(Element element) {
    Element linkElement = element.element("link");

    if (linkElement != null) {
      Link link = new Link();
      String text = linkElement.attributeValue("text");
      String id = linkElement.attributeValue("id");

      List<Event> events = this.getEvents(linkElement);

      if (events != null && events.size() > 0) {
        link.setHasEvents(true);
        link.setEvents(events);
      } else {
        link.setEvents(null);
        link.setHasEvents(false);
      }

      link.setId(id);
      link.setText(text);

      return link;
    }

    return null;
  }

  public List<Event> getEvents(Element element) {
    // Get events.
    Element eventsElement = element.element("events");
    List<Event> events = new ArrayList<Event>();

    if (eventsElement != null) {
      for (Iterator iter = eventsElement.elementIterator("event"); iter.hasNext();) {
        Element eventElement = (Element) iter.next();
        String eventType = eventElement.attributeValue("type");
        String function = eventElement.elementText("function");

        // Build event content object.
        Event event = new Event();
        if (function != null) {
          event.setFunction(function.trim());
        } else {
          event.setFunction("");
        }

        // Get Return messages.
        Element returnMessagesElement = eventElement.element("returnMessages");
        List<AjaxReturn> returnMessages = new ArrayList<AjaxReturn>();

        if (returnMessagesElement != null) {
          for (Iterator returnMessageIter = returnMessagesElement.elementIterator("returnMessage"); returnMessageIter.hasNext();) {
            Element returnMessageElement = (Element) returnMessageIter.next();
            String validation = returnMessageElement.attributeValue("validation");

            AjaxReturn returnMessage = new AjaxReturn();
            returnMessage.setMessage(returnMessageElement.getTextTrim());

            if (validation.equals("true")) {
              returnMessage.setValidation(true);
            } else {
              returnMessage.setValidation(false);
            }

            returnMessages.add(returnMessage);
          }
        }
        event.setAjaxReturnMessage(returnMessages);
        event.setType(eventType);
        events.add(event);
      }
    }

    return events;
  }

  public List<Option> getRangedOptions(OptionRange optionRange) {
    List<Option> options = new ArrayList<Option>();

    // Prepare options in a range.
    if (optionRange != null) {
      if (optionRange.getType().equals(OptionRange.DATE_TYPE)) {
        if (optionRange.getStart() != null) {
          GregorianCalendar start = new GregorianCalendar();

          if (optionRange.getStart().equals(OptionRange.TODAY)) {
            start.setTime(new Date());
          } else {
            start.set(GregorianCalendar.YEAR, Integer.valueOf(optionRange.getStart()));
          }

          int index = 1;

          if (optionRange.getEnd() != null && !optionRange.getEnd().equals("")) {
            GregorianCalendar end = new GregorianCalendar();
            if (optionRange.getEnd().equals(OptionRange.THIS_YEAR)) {
              end.setTime(new Date());
            }

            while (start.get(GregorianCalendar.YEAR) <= end.get(GregorianCalendar.YEAR)) {
              Option option = new Option();

              option.setText(String.valueOf(start.get(GregorianCalendar.YEAR)));
              option.setValue(String.valueOf(start.get(GregorianCalendar.YEAR)));
              option.setName(optionRange.getName());
              options.add(option);

              start.add(GregorianCalendar.YEAR, 1);
            }
          } else {
            while (index <= optionRange.getDays()) {
              Option option = new Option();

              option.setText(dateFormat.format(start.getTime()));
              option.setValue(dateFormat.format(start.getTime()));
              option.setName(optionRange.getName());
              options.add(option);

              start.add(GregorianCalendar.HOUR, 24);
              index++;
            }
          }
        }
      }
    }

    return options;
  }

  @SuppressWarnings("unchecked")
  public long getNumberOfPage(String part) {
    String xpath = "//page[@part='" + part + "']";

    List<Node> pageNodes = document.selectNodes(xpath);

    return pageNodes.size();
  }
  
  @SuppressWarnings("unchecked")
  public List<QuotePage> getPagesByPart(String part) {
    String xpath = "//page[@part='" + part + "']";

    List<Node> pageNodes = document.selectNodes(xpath);

    List<QuotePage> quotePageList = new ArrayList<QuotePage>();

    for (Node node : pageNodes) {
      quotePageList.add(convertNodeToQuotePage(node));
    }

    return quotePageList;
  }

  public QuotePage getPage(long id) {
    String xpath = "//page[@id='" + id + "']";

    Config.logger.debug("Value of id: " + id);
    Node pageNode = document.selectSingleNode(xpath);

    return convertNodeToQuotePage(pageNode);
  }

  private QuotePage convertNodeToQuotePage(Node pageNode) {
    
    QuotePage quotePage = new QuotePage();
    quotePage.setQuestions(this.getQuestionsByPage(Long.valueOf(pageNode.valueOf("@id"))));
    quotePage.setId(Long.valueOf(pageNode.valueOf("@id")));
    quotePage.setSummary(pageNode.valueOf("@summary"));
    quotePage.setSaveAction(pageNode.valueOf("@saveAction"));
    quotePage.setHeading(pageNode.valueOf("@heading"));
    String progress = pageNode.valueOf("@progress");

    String requireNextButton = pageNode.valueOf("@requireNextButton");
    String requirePreviousButton = pageNode.valueOf("@requirePreviousButton");
    String nextButtonText = pageNode.valueOf("@nextButtonText");
    String previousButtonText = pageNode.valueOf("@previousButtonText");
    String part = pageNode.valueOf("@part");

    if (progress != null && !progress.isEmpty()) {
      quotePage.setProgress(Long.valueOf(progress));
    }

    if (requireNextButton.equals("yes")) {
      quotePage.setRequireNextButton(true);
    } else {
      quotePage.setRequireNextButton(false);
    }

    if (requirePreviousButton.equals("yes")) {
      quotePage.setRequirePreviousButton(true);
    } else {
      quotePage.setRequirePreviousButton(false);
    }

    if (part != null && !part.isEmpty()) {
      quotePage.setPart(part);
    } else {
      quotePage.setPart("");
    }

    if (previousButtonText != null && !previousButtonText.isEmpty()) {
      quotePage.setPreviousButtonText(previousButtonText);
    }

    if (nextButtonText != null && !nextButtonText.isEmpty()) {
      quotePage.setNextButtonText(nextButtonText);
    }

    Element pageElement = (Element) pageNode;
    Element descriptionElement = pageElement.element("description");

    if (descriptionElement != null) {
      List<String> description = new ArrayList<String>();

      for (Iterator iter = descriptionElement.elementIterator("paragraph"); iter.hasNext();) {
        Element lineElement = (Element) iter.next();
        description.add(lineElement.getTextTrim());
      }

      quotePage.setDescription(description);
    }

    return quotePage;
  }

  public QuoteQuestion getQuestionByPageAndId(long pageId, String questionId) {
    List<QuoteQuestion> quoteQuestions = this.getQuestionsByPage(pageId);

    for (QuoteQuestion quoteQuestion : quoteQuestions) {
      if (quoteQuestion.getId().equals(questionId)) {
        return quoteQuestion;
      }
    }

    return null;
  }

  public List<QuoteQuestion> getQuestionsByPage(long id) {
    String xpath = "//page[@id='" + id + "']/*";
    List list = document.selectNodes(xpath);
    List<QuoteQuestion> quoteQuestions = new ArrayList<QuoteQuestion>();

    for (int i = 0; i < list.size(); i++) {
      Element questionElement = (Element) list.get(i);

      // Get question.
      QuoteQuestion quoteQuestion = this.getQuestion(questionElement);

      // Get sub questions.
      Element subQuestionsElement = questionElement.element("subQuestions");
      List<QuoteQuestion> subQuestions = new ArrayList<QuoteQuestion>();

      if (subQuestionsElement != null) {
        for (Iterator iter = subQuestionsElement.elementIterator("subQuestion"); iter.hasNext();) {
          Element subQuestionElement = (Element) iter.next();
          QuoteQuestion subQuestion = this.getQuestion(subQuestionElement);
          subQuestions.add(subQuestion);
        }
      }

      quoteQuestion.setSubQuestions(subQuestions);
      quoteQuestions.add(quoteQuestion);
    }

    return quoteQuestions;
  }

  public List<Text> getTexts(Element questionElement) {
    List<Text> texts = new ArrayList<Text>();
    Element textsElement = questionElement.element("texts");

    // This index is used in text.id which must be unique. It is only used for radio type.
    int index = 1;

    // Add special case.
    for (Iterator iter = textsElement.elementIterator("text"); iter.hasNext();) {
      Text text = new Text();

      Element textElement = (Element) iter.next();

      // Get Events.
      List<Event> events = this.getEvents(textElement);

      if (events != null && events.size() > 0) {
        text.setEvents(events);
        text.setHasEvents(true);
      } else {
        text.setHasEvents(false);
      }

      text.setText(textElement.attributeValue("text"));
      text.setName(textElement.attributeValue("name"));
      text.setValue(textElement.attributeValue("value"));
      text.setId(text.getName() + text.getValue() + index);

      String maxLength = textElement.attributeValue("maxLength");
      if (maxLength == null || maxLength.isEmpty()) {
        maxLength = "50"; // @todo put in properties file
      }
      text.setMaxLength(maxLength);

      text.setAnswer("");

      String image = textElement.attributeValue("image");

      if (image == null) {
        text.setImage(image);
      } else {
        text.setImage("");
      }

      String type = textElement.attributeValue("type");

      if (type == null) {
        text.setType(Text.NORMAL_TYPE);
      } else {
        if (type.equals(Text.NORMAL_TYPE)) {
          text.setType(Text.NORMAL_TYPE);
        } else {
          text.setType(type);
        }
      }

      String readonly = textElement.attributeValue("readonly");

      if (readonly != null && readonly.equals("true")) {
        text.setReadonly(true);
      } else {
        text.setReadonly(false);
      }

      String numberOnly = textElement.attributeValue("numberOnly");

      if (numberOnly != null && numberOnly.equals("true")) {
        text.setNumberOnly(true);
      } else {
        text.setNumberOnly(false);
      }

      String dateOnly = textElement.attributeValue("dateOnly");

      if (dateOnly != null && dateOnly.equals("true")) {
        text.setDateOnly(true);
      } else {
        text.setDateOnly(false);
      }

      String costOnly = textElement.attributeValue("costOnly");

      if (costOnly != null && costOnly.equals("true")) {
        text.setCostOnly(true);
      } else {
        text.setCostOnly(false);
      }

      String alphaOnly = textElement.attributeValue("alphaOnly");

      if (alphaOnly != null && alphaOnly.equals("true")) {
        text.setAlphaOnly(true);
      } else {
        text.setAlphaOnly(false);
      }

      String dollar = textElement.attributeValue("dollar");

      if (dollar != null && dollar.equals("true")) {
        text.setDollar(true);
      } else {
        text.setDollar(false);
      }

      texts.add(text);

      index++;
    }

    return texts;
  }

  public OptionRange getOptionRange(Element questionElement) {
    OptionRange optionRange = new OptionRange();
    Element optionsElement = questionElement.element("options");
    String name = optionsElement.attributeValue("name");
    String type = optionsElement.elementText("type");

    optionRange.setType(type);
    optionRange.setName(name);

    Element range = optionsElement.element("range");

    if (range != null) {
      String start = range.elementText("start");
      String operator = range.elementText("operator");
      String days = range.elementText("days");
      String end = range.elementText("end");

      if (start != null) {
        optionRange.setStart(start);
      } else {
        optionRange.setStart(null);
      }

      if (end != null) {
        optionRange.setEnd(end);
      } else {
        optionRange.setEnd("");
      }

      if (operator != null) {
        if (operator.equals(OptionRange.OPERATOR_PLUS)) {
          optionRange.setOperator(OptionRange.OPERATOR_PLUS);
        }
      } else {
        optionRange.setOperator("");
      }

      if (days != null) {
        optionRange.setDays(Integer.valueOf(days));
      } else {
        optionRange.setDays(null);
      }

      return optionRange;
    } else {
      return null;
    }
  }

  public String getOptionsName(Element questionElement) {
    Element optionsElement = questionElement.element("options");
    String name = optionsElement.attributeValue("name").trim();

    return name;
  }

  public List<Option> getOptions(Element questionElement) {
    Element optionsElement = questionElement.element("options");
    String type = optionsElement.elementText("type").trim();
    String name = optionsElement.attributeValue("name").trim();

    List<Option> options = new ArrayList<Option>();

    for (Iterator iter = optionsElement.elementIterator("option"); iter.hasNext();) {
      Element optionElement = (Element) iter.next();
      Option option = new Option();

      // Get Events.
      List<Event> events = this.getEvents(optionElement);

      if (events != null && events.size() > 0) {
        option.setEvents(events);
        option.setHasEvents(true);
      } else {
        option.setHasEvents(false);
      }

      option.setType(type);
      option.setSelected(false);
      option.setValue(optionElement.attributeValue("value"));
      option.setText(optionElement.getTextTrim());
      option.setName(name);

      options.add(option);
    }

    return options;
  }

  public void parse(File fileName) throws DocumentException {

    SAXReader reader = new SAXReader();
    document = reader.read(fileName);
  }
}