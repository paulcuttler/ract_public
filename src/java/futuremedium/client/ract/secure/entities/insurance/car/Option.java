package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.*;

/**
 * Bean class holds option nodes from insurance_quote_question.xml.
 * @author xmfu
 */
public class Option {
  private String type;
  private String value;
  private String text;
  private String name;
  private List<Event> events;
  private boolean hasEvents;
  private boolean selected;

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value the value to set
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return the text
   */
  public String getText() {
    return text;
  }

  /**
   * @param text the text to set
   */
  public void setText(String text) {
    this.text = text;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the events
   */
  public List<Event> getEvents() {
    return events;
  }

  /**
   * @param events the events to set
   */
  public void setEvents(List<Event> events) {
    this.events = events;
  }

  /**
   * @return the hasEvents
   */
  public boolean isHasEvents() {
    return hasEvents;
  }

  /**
   * @param hasEvents the hasEvents to set
   */
  public void setHasEvents(boolean hasEvents) {
    this.hasEvents = hasEvents;
  }

  /**
   * @return the selected
   */
  public boolean isSelected() {
    return selected;
  }

  /**
   * @param selected the selected to set
   */
  public void setSelected(boolean selected) {
    this.selected = selected;
  }
}
