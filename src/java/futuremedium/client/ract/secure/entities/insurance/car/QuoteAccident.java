package futuremedium.client.ract.secure.entities.insurance.car;

/**
 * Accident bean class holds properties for an accident.
 * @author xmfu
 */
public class QuoteAccident {
  private long accidentId;
  private int month;
  private int year;
  private String type;

  /**
   * @return the month
   */
  public int getMonth() {
    return month;
  }

  /**
   * @param month the month to set
   */
  public void setMonth(int month) {
    this.month = month;
  }

  /**
   * @return the year
   */
  public int getYear() {
    return year;
  }

  /**
   * @param year the year to set
   */
  public void setYear(int year) {
    this.year = year;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the accidentId
   */
  public long getAccidentId() {
    return accidentId;
  }

  /**
   * @param accidentId the accidentId to set
   */
  public void setAccidentId(long accidentId) {
    this.accidentId = accidentId;
  }
}
