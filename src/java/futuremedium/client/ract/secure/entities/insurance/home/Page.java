/*
 * Page.java
 *
 *
 */

package futuremedium.client.ract.secure.entities.insurance.home;

import futuremedium.common2.website.*;

/**
 * Representation of presentation attributes for a Quote page.
 * @author
 */
public class Page extends BasePage {

  private boolean allowSave = true;
  private boolean quoteOnly;
  private boolean policyOnly;
  private boolean suppressNav;
  private boolean checklist;
  private boolean newOrSave;
  private boolean saved;
  private boolean newQuote;
  private boolean calculator;
  private boolean optionalStep;
  private boolean benefits;
  private boolean finaliseQuote;
  private boolean resume;
  private boolean close;
  private boolean paymentOptions;
  private boolean purchasePolicy;
  private boolean finished;
  private boolean dojoFilteringSelect;
  private boolean survey;
  private boolean exit;
  private boolean reminder;
  private String excludedFromFlow = "";
  private boolean navCompletedDisabled;
  private boolean homeNav;

  /** Creates a new instance of Page */
  public Page(BasePage parent, String title, String englishURI, BaseSiteInfo siteInfo) {
    super(parent, title, englishURI, siteInfo);
  }

  /**
   * @return the quoteOnly
   */
  public boolean isQuoteOnly() {
    return quoteOnly;
  }

  /**
   * @param quoteOnly the quoteOnly to set
   */
  public void setQuoteOnly(boolean quoteOnly) {
    this.quoteOnly = quoteOnly;
  }

  /**
   * @return the policyOnly
   */
  public boolean isPolicyOnly() {
    return policyOnly;
  }

  /**
   * @param policyOnly the policyOnly to set
   */
  public void setPolicyOnly(boolean policyOnly) {
    this.policyOnly = policyOnly;
  }

  /**
   * @return the suppressNav
   */
  public boolean isSuppressNav() {
    return suppressNav;
  }

  /**
   * @param suppressNav the suppressNav to set
   */
  public void setSuppressNav(boolean suppressNav) {
    this.suppressNav = suppressNav;
  }

  /**
   * @return the checklist
   */
  public boolean isChecklist() {
    return checklist;
  }

  /**
   * @param checklist the checklist to set
   */
  public void setChecklist(boolean checklist) {
    this.checklist = checklist;
  }

  /**
   * @return the newOrSave
   */
  public boolean isNewOrSave() {
    return newOrSave;
  }

  /**
   * @param newOrSave the newOrSave to set
   */
  public void setNewOrSave(boolean newOrSave) {
    this.newOrSave = newOrSave;
  }

  /**
   * @return the calculator
   */
  public boolean isCalculator() {
    return calculator;
  }

  /**
   * @param calculator the calculator to set
   */
  public void setCalculator(boolean calculator) {
    this.calculator = calculator;
  }

  /**
   * @return the optionalStep
   */
  public boolean isOptionalStep() {
    return optionalStep;
  }

  /**
   * @param optionalStep the optionalStep to set
   */
  public void setOptionalStep(boolean optionalStep) {
    this.optionalStep = optionalStep;
  }

  /**
   * @return the benefits
   */
  public boolean isBenefits() {
    return benefits;
  }

  /**
   * @param benefits the benefits to set
   */
  public void setBenefits(boolean benefits) {
    this.benefits = benefits;
  }

  /**
   * @return the finaliseQuote
   */
  public boolean isFinaliseQuote() {
    return finaliseQuote;
  }

  /**
   * @param finaliseQuote the finaliseQuote to set
   */
  public void setFinaliseQuote(boolean finaliseQuote) {
    this.finaliseQuote = finaliseQuote;
  }

  /**
   * @return the resume
   */
  public boolean isResume() {
    return resume;
  }

  /**
   * @param resume the resume to set
   */
  public void setResume(boolean resume) {
    this.resume = resume;
  }

  /**
   * @return the close
   */
  public boolean isClose() {
    return close;
  }

  /**
   * @param close the close to set
   */
  public void setClose(boolean close) {
    this.close = close;
  }

  /**
   * @return the paymentOptions
   */
  public boolean isPaymentOptions() {
    return paymentOptions;
  }

  /**
   * @param paymentOptions the paymentOptions to set
   */
  public void setPaymentOptions(boolean paymentOptions) {
    this.paymentOptions = paymentOptions;
  }

  /**
   * @return the purchasePolicy
   */
  public boolean isPurchasePolicy() {
    return purchasePolicy;
  }

  /**
   * @param purchasePolicy the purchasePolicy to set
   */
  public void setPurchasePolicy(boolean purchasePolicy) {
    this.purchasePolicy = purchasePolicy;
  }

  /**
   * @return the finished
   */
  public boolean isFinished() {
    return finished;
  }

  /**
   * @param finished the finished to set
   */
  public void setFinished(boolean finished) {
    this.finished = finished;
  }

  /**
   * @return the dojoFilteringSelect
   */
  public boolean isDojoFilteringSelect() {
    return dojoFilteringSelect;
  }

  /**
   * @param dojoFilteringSelect the dojoFilteringSelect to set
   */
  public void setDojoFilteringSelect(boolean dojoFilteringSelect) {
    this.dojoFilteringSelect = dojoFilteringSelect;
  }

  /**
   * @return the survey
   */
  public boolean isSurvey() {
    return survey;
  }

  /**
   * @param survey the survey to set
   */
  public void setSurvey(boolean survey) {
    this.survey = survey;
  }

  /**
   * @return the allowSave
   */
  public boolean isAllowSave() {
    return allowSave;
  }

  /**
   * @param allowSave the allowSave to set
   */
  public void setAllowSave(boolean allowSave) {
    this.allowSave = allowSave;
  }

  /**
   * @return the exit
   */
  public boolean isExit() {
    return exit;
  }

  /**
   * @param exit the exit to set
   */
  public void setExit(boolean exit) {
    this.exit = exit;
  }

  /**
   * @return the reminder
   */
  public boolean isReminder() {
    return reminder;
  }

  /**
   * @param reminder the reminder to set
   */
  public void setReminder(boolean reminder) {
    this.reminder = reminder;
  }

  /**
   * @return the excludedFromFlow
   */
  public String getExcludedFromFlow() {
    return excludedFromFlow;
  }

  /**
   * @param excludedFromFlow the excludedFromFlow to set
   */
  public void setExcludedFromFlow(String excludedFromFlow) {
    this.excludedFromFlow = excludedFromFlow;
  }

  /**
   * @return the saved
   */
  public boolean isSaved() {
    return saved;
  }

  /**
   * @param saved the saved to set
   */
  public void setSaved(boolean saved) {
    this.saved = saved;
  }

  /**
   * @return the newQuote
   */
  public boolean isNewQuote() {
    return newQuote;
  }

  /**
   * @param newQuote the newQuote to set
   */
  public void setNewQuote(boolean newQuote) {
    this.newQuote = newQuote;
  }

  /**
   * @return the navCompletedDisabled
   */
  public boolean isNavCompletedDisabled() {
    return navCompletedDisabled;
  }

  /**
   * @param navCompletedDisabled the navCompletedDisabled to set
   */
  public void setNavCompletedDisabled(boolean navCompletedDisabled) {
    this.navCompletedDisabled = navCompletedDisabled;
  }

  /**
   * @return the homeNav
   */
  public boolean isHomeNav() {
    return homeNav;
  }

  /**
   * @param homeNav the homeNav to set
   */
  public void setHomeNav(boolean homeNav) {
    this.homeNav = homeNav;
  }


}
