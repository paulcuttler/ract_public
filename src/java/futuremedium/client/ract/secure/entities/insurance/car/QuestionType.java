package futuremedium.client.ract.secure.entities.insurance.car;

/**
 * Enum class of question type.
 * @author xmfu
 */
public enum QuestionType {
  NONE("none"),SELECT("select"),CHECK_BOX("checkbox"),TEXT("text"),RADIO("radio"), TEXT_SHOW("textShow"), TEXT_HEADING("textHeading"), CAPTCHA("captcha"), RADIO_IMAGE("radioImage"), SHOW_HEADER("showHeader"), TEXT_DESCRIPTION("textDescription");

  private String value;

  QuestionType(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }

  public static QuestionType getType(String value) {
    if (value.equals("none")) {
      return NONE;
    } else if (value.equals("select")) {
      return SELECT;
    } else if (value.equals("checkbox")) {
      return CHECK_BOX;
    } else if (value.equals("text")) {
      return TEXT;
    } else if (value.equals("radio")) {
      return RADIO;
    } else if (value.equals("textShow")) {
      return TEXT_SHOW;
    } else if (value.equals("textHeading")) {
      return TEXT_HEADING;
    } else if (value.equals("captcha")) {
      return CAPTCHA;
    } else if (value.equals("radioImage")) {
      return RADIO_IMAGE;
    } else if (value.equals("showHeader")) {
      return SHOW_HEADER;
    } else if (value.equals("textDescription")) {
      return TEXT_DESCRIPTION;
    }

    return null;
  }
}