package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.Comparator;

/**
 * A comparator to sort by street name.
 * @author xmfu
 */
public class StreetComparator implements Comparator<GaragedAddress> {
  public int compare(GaragedAddress o1, GaragedAddress o2) {
    return o1.getStreet().compareTo(o2.getStreet());
  }
}
