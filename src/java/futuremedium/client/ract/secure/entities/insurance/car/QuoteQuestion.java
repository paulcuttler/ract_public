package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.*;

/**
 * A Insurance Question bean class holds properties parsed from insurance_quote_question.xml.
 * @author xmfu
 */
public class QuoteQuestion {
    
    
  private String id;
  private String title;
  private String includeHTML;
  private QuestionType type;
  private List<QuoteQuestion> subQuestions;
  private String helpOption;
  private boolean display;
  private boolean save;
  private List<Option> options;
  private List<Text> texts;
  private String name;
  private Event event;
  private String value;
  private String heading;
  private String cssClass;
  private String notes;
  private String typeString;
  private Link link;
  private boolean mandatory;
  private List<String> textDescription;

  public QuoteQuestion() {}

  public String getNoneType() {
    return QuestionType.NONE.getValue();
  }

  public String getSelectType() {
    return QuestionType.SELECT.getValue();
  }

  public String getCheckBoxType() {
    return QuestionType.CHECK_BOX.getValue();
  }

  public String getTextType() {
    return QuestionType.TEXT.getValue();
  }

  public String getTextHeadingType() {
    return QuestionType.TEXT_HEADING.getValue();
  }

  public String getRadioType() {
    return QuestionType.RADIO.getValue();
  }

  public String getTextShowType() {
    return QuestionType.TEXT_SHOW.getValue();
  }

  public String getCaptchaType() {
    return QuestionType.CAPTCHA.getValue();
  }

  public String getRadioImageType() {
    return QuestionType.RADIO_IMAGE.getValue();
  }

  public String getTextDescriptionType() {
    return QuestionType.TEXT_DESCRIPTION.getValue();
  }

  public String getTypeString() {
    return this.typeString;
  }

  public void setTypeString(String typeString) {
    this.typeString = typeString;
  }

  /**
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the includeHTML
   */
  public String getIncludeHTML() {
    return includeHTML;
  }

  /**
   * @param includeHTML the includeHTML to set
   */
  public void setIncludeHTML(String includeHTML) {
    this.includeHTML = includeHTML;
  }

  /**
   * @return the type
   */
  public QuestionType getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(QuestionType type) {
    this.type = type;
  }

  /**
   * @return the subQuestions
   */
  public List<QuoteQuestion> getSubQuestions() {
    return subQuestions;
  }

  /**
   * @param subQuestions the subQuestions to set
   */
  public void setSubQuestions(List<QuoteQuestion> subQuestions) {
    this.subQuestions = subQuestions;
  }

  /**
   * @return the helpOption
   */
  public String getHelpOption() {
    return helpOption;
  }

  /**
   * @param helpOption the helpOption to set
   */
  public void setHelpOption(String helpOption) {
    this.helpOption = helpOption;
  }

  /**
   * @return the display
   */
  public boolean getDisplay() {
    return display;
  }

  /**
   * @param display the display to set
   */
  public void setDisplay(boolean display) {
    this.display = display;
  }

  /**
   * @return the save
   */
  public boolean isSave() {
    return save;
  }

  /**
   * @param save the save to set
   */
  public void setSave(boolean save) {
    this.save = save;
  }

  /**
   * @return the options
   */
  public List<Option> getOptions() {
    return options;
  }

  public String getSelectedOption() {
    for (Option option : this.options) {
      if (option.isSelected()) {
        return option.getValue();
      }
    }

    return "";
  }

  /**
   * @param options the options to set
   */
  public void setOptions(List<Option> options) {
    this.options = options;
  }

  public Map<String, String> getOptionsMap() {
    Map<String, String> maps = new LinkedHashMap<String, String>();

    for (Option option : this.getOptions()) {
      maps.put(option.getValue(), option.getText());
    }

    return maps;
  }

  /**
   * @return the texts
   */
  public List<Text> getTexts() {
    return texts;
  }

  /**
   * @param texts the texts to set
   */
  public void setTexts(List<Text> texts) {
    this.texts = texts;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the event
   */
  public Event getEvent() {
    return event;
  }

  /**
   * @param event the event to set
   */
  public void setEvent(Event event) {
    this.event = event;
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value the value to set
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return the heading
   */
  public String getHeading() {
    return heading;
  }

  /**
   * @param heading the heading to set
   */
  public void setHeading(String heading) {
    this.heading = heading;
  }

  /**
   * @return the notes
   */
  public String getNotes() {
    return notes;
  }

  /**
   * @param notes the notes to set
   */
  public void setNotes(String notes) {
    this.notes = notes;
  }

  /**
   * @return the link
   */
  public Link getLink() {
    return link;
  }

  /**
   * @param link the link to set
   */
  public void setLink(Link link) {
    this.link = link;
  }

  /**
   * @return the mandatory
   */
  public boolean isMandatory() {
    return mandatory;
  }

  /**
   * @param mandatory the mandatory to set
   */
  public void setMandatory(boolean mandatory) {
    this.mandatory = mandatory;
  }

  /**
   * @return the textDescription
   */
  public List<String> getTextDescription() {
    return textDescription;
  }

  /**
   * @param textDescription the textDescription to set
   */
  public void setTextDescription(List<String> textDescription) {
    this.textDescription = textDescription;
  }

    /**
     * @return the cssClass
     */
    public String getCssClass() {
        return cssClass;
    }

    /**
     * @param cssClass the cssClass to set
     */
    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

}
