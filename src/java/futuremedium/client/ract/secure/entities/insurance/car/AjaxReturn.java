package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.*;

/**
 * A class encapsulates ajax return object.
 * @author xmfu
 */
public class AjaxReturn {
  private boolean validation;
  private String message;
  private List<String> texts;
  private List<String> values;

  /**
   * @return the validation
   */
  public boolean isValidation() {
    return validation;
  }

  /**
   * @param validation the validation to set
   */
  public void setValidation(boolean validation) {
    this.validation = validation;
  }

  /**
   * @return the messages
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param messages the messages to set
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @return the list
   */
  public List<String> getTexts() {
    return texts;
  }

  /**
   * @param list the list to set
   */
  public void setTexts(List<String> texts) {
    this.texts = texts;
  }

    /**
     * @return the keys
     */
    public List<String> getValues() {
        return values;
    }

    /**
     * @param keys the keys to set
     */
    public void setValues(List<String> values) {
        this.values = values;
    }
}
