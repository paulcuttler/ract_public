package futuremedium.client.ract.secure.entities.insurance.car;

/**
 * A bean class holds garage address data.
 * @author xmfu
 */
public class GaragedAddress {

  private String postCode;
  private String suburb;
  private String streetNumber;
  private String streetSuburbId;
  private String street;
  private String poBox;
  private String state;
  private String latitude;
  private String longitude;
  private String gnaf;

  /**
   * @return the postCode
   */
  public String getPostCode() {
    return postCode;
  }

  /**
   * @param postCode the postCode to set
   */
  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  /**
   * @return the suburb
   */
  public String getSuburb() {
    return suburb;
  }

  /**
   * @param suburb the suburb to set
   */
  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  /**
   * @return the streetNumber
   */
  public String getStreetNumber() {
    return streetNumber;
  }

  /**
   * @param streetNumber the streetNumber to set
   */
  public void setStreetNumber(String streetNumber) {
    this.streetNumber = streetNumber;
  }

  /**
   * @return the street
   */
  public String getStreetSuburbId() {
    return streetSuburbId;
  }

  /**
   * @param street the street to set
   */
  public void setStreetSuburbId(String streetSuburbId) {
    this.streetSuburbId = streetSuburbId;
  }

  /**
   * @return the street
   */
  public String getStreet() {
    return street;
  }

  /**
   * @param street the street to set
   */
  public void setStreet(String street) {
    this.street = street;
  }

  public int compareTo(Object o) {
    if(o instanceof GaragedAddress) {
      GaragedAddress garagedAddress = (GaragedAddress) o;
      return this.getStreet().compareTo(garagedAddress.getStreet());
    } else {
      return 0;
    }
  }

  /**
   * @return the poBox
   */
  public String getPoBox() {
    return poBox;
  }

  /**
   * @param poBox the poBox to set
   */
  public void setPoBox(String poBox) {
    this.poBox = poBox;
  }

  /**
   * @return the state
   */
  public String getState() {
    return state;
  }

  /**
   * @param state the state to set
   */
  public void setState(String state) {
    this.state = state;
  }

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the gnaf
	 */
	public String getGnaf() {
		return gnaf;
	}

	/**
	 * @param gnaf the gnaf to set
	 */
	public void setGnaf(String gnaf) {
		this.gnaf = gnaf;
	}
}
