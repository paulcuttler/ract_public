package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.*;

/**
 * A bean class holds a page in insurance_quote_question.xml.
 * @author xmfu
 */
public class QuotePage {
  private long id;
  private List<QuoteQuestion> questions;
  private String summary;
  private String saveAction;
  private List<String> description;
  private String heading;
  private boolean requireNextButton;
  private boolean requirePreviousButton;
  private String nextButtonText;
  private String previousButtonText;
  private long progress;
  private String part;

  /**
   * @return the id
   */
  public long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(long id) {
    this.id = id;
  }

  /**
   * @return the questions
   */
  public List<QuoteQuestion> getQuestions() {
    return questions;
  }

  /**
   * @param questions the questions to set
   */
  public void setQuestions(List<QuoteQuestion> questions) {
    this.questions = questions;
  }

  /**
   * @return the summary
   */
  public String getSummary() {
    return summary;
  }

  /**
   * @param summary the summary to set
   */
  public void setSummary(String summary) {
    this.summary = summary;
  }

  /**
   * @return the saveAction
   */
  public String getSaveAction() {
    return saveAction;
  }

  /**
   * @param saveAction the saveAction to set
   */
  public void setSaveAction(String saveAction) {
    this.saveAction = saveAction;
  }

  /**
   * @return the description
   */
  public List<String> getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(List<String> description) {
    this.description = description;
  }

  /**
   * @return the heading
   */
  public String getHeading() {
    return heading;
  }

  /**
   * @param heading the heading to set
   */
  public void setHeading(String heading) {
    this.heading = heading;
  }

  /**
   * @return the nextButtonText
   */
  public String getNextButtonText() {
    return nextButtonText;
  }

  /**
   * @param nextButtonText the nextButtonText to set
   */
  public void setNextButtonText(String nextButtonText) {
    this.nextButtonText = nextButtonText;
  }

  /**
   * @return the previousButtonText
   */
  public String getPreviousButtonText() {
    return previousButtonText;
  }

  /**
   * @param previousButtonText the previousButtonText to set
   */
  public void setPreviousButtonText(String previousButtonText) {
    this.previousButtonText = previousButtonText;
  }

  /**
   * @return the requireNextButton
   */
  public boolean isRequireNextButton() {
    return requireNextButton;
  }

  /**
   * @param requireNextButton the requireNextButton to set
   */
  public void setRequireNextButton(boolean requireNextButton) {
    this.requireNextButton = requireNextButton;
  }

  /**
   * @return the requirePreviousButton
   */
  public boolean isRequirePreviousButton() {
    return requirePreviousButton;
  }

  /**
   * @param requirePreviousButton the requirePreviousButton to set
   */
  public void setRequirePreviousButton(boolean requirePreviousButton) {
    this.requirePreviousButton = requirePreviousButton;
  }

  /**
   * @return the progress
   */
  public long getProgress() {
    return progress;
  }

  /**
   * @param progress the progress to set
   */
  public void setProgress(long progress) {
    this.progress = progress;
  }

  /**
   * @return the part
   */
  public String getPart() {
    return part;
  }

  /**
   * @param part the part to set
   */
  public void setPart(String part) {
    this.part = part;
  }
}
