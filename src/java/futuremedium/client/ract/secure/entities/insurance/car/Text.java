package futuremedium.client.ract.secure.entities.insurance.car;

import java.util.*;

/**
 * A bean class holds text nodes in insurance_quote_question.xml.
 * @author xmfu
 */
public class Text {
  public static final String NORMAL_TYPE = "normal";
  public static final String FOUR_DIGIT_TYPE = "4digitCardField";
  public static final String BANK_CARD = "bankCard";
  public static final String DATE_TYPE_ALL = "dateALL";

  private String id;
  private String type;
  private String value;
  private String name;
  private String text;
  private List<Event> events;
  private boolean hasEvents;
  private String answer;
  private boolean readonly;
  private boolean dollar;
  private String maxLength;
  private boolean numberOnly;
  private boolean dateOnly;
  private boolean alphaOnly;
  private boolean costOnly;
  
  private String image;

  public String getDateTypeALL() {
    return DATE_TYPE_ALL;
  }

  public String getFoutDigitType() {
    return FOUR_DIGIT_TYPE;
  }

  public String getBankCardType() {
    return BANK_CARD;
  }
  

  public Map<String, String> getTextList() {
    Map<String, String> textList = new HashMap<String, String>();
    textList.put(this.getValue(), this.getText());

    return textList;
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value the value to set
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the text
   */
  public String getText() {
    return text;
  }

  /**
   * @param text the text to set
   */
  public void setText(String text) {
    this.text = text;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the events
   */
  public List<Event> getEvents() {
    return events;
  }

  /**
   * @param events the events to set
   */
  public void setEvents(List<Event> events) {
    this.events = events;
  }

  /**
   * @return the hasEvents
   */
  public boolean isHasEvents() {
    return hasEvents;
  }

  /**
   * @param hasEvents the hasEvents to set
   */
  public void setHasEvents(boolean hasEvents) {
    this.hasEvents = hasEvents;
  }

  /**
   * @return the answer
   */
  public String getAnswer() {
    return answer;
  }

  /**
   * @param answer the answer to set
   */
  public void setAnswer(String answer) {
    this.answer = answer;
  }

  /**
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return the readonly
   */
  public boolean isReadonly() {
    return readonly;
  }

  /**
   * @param readonly the readonly to set
   */
  public void setReadonly(boolean readonly) {
    this.readonly = readonly;
  }

  /**
   * @return the dollar
   */
  public boolean isDollar() {
    return dollar;
  }

  /**
   * @param dollar the dollar to set
   */
  public void setDollar(boolean dollar) {
    this.dollar = dollar;
  }

  public void setMaxLength(String maxLength) {
    this.maxLength = maxLength;
  }

  public String getMaxLength() {
    return this.maxLength;
  }

  /**
   * @return the numberOnly
   */
  public boolean isNumberOnly() {
    return numberOnly;
  }

  /**
   * @param numberOnly the numberOnly to set
   */
  public void setNumberOnly(boolean numberOnly) {
    this.numberOnly = numberOnly;
  }

  /**
   * @return the image
   */
  public String getImage() {
    return image;
  }

  /**
   * @param image the image to set
   */
  public void setImage(String image) {
    this.image = image;
  }

  /**
   * @return the dateOnly
   */
  public boolean isDateOnly() {
    return dateOnly;
  }

  /**
   * @param dateOnly the dateOnly to set
   */
  public void setDateOnly(boolean dateOnly) {
    this.dateOnly = dateOnly;
  }

  /**
   * @return the alphaOnly
   */
  public boolean isAlphaOnly() {
    return alphaOnly;
  }

  /**
   * @param alphaOnly the alphaOnly to set
   */
  public void setAlphaOnly(boolean alphaOnly) {
    this.alphaOnly = alphaOnly;
  }

  /**
   * @return the costOnly
   */
  public boolean isCostOnly() {
    return costOnly;
  }

  /**
   * @param costOnly the costOnly to set
   */
  public void setCostOnly(boolean costOnly) {
    this.costOnly = costOnly;
  }
}
