package futuremedium.client.ract.secure.entities.insurance.car;

/**
 * A temp vehicle bean class holds properties for a vehicle. It is passed among pages and for various purpose such as page reloading.
 * @author xmfu
 */
public class VehicleInfo {
  private String year;
  private String make;
  private String model;
  private String garagedAddress;
  private String financeType;
  private String usage;
  private String bodyType;
  private String transmission;
  private String variant;
  private String summary;
  private String cylinders;
  private String acceptable;

  /**
   * @return the year
   */
  public String getYear() {
    return year;
  }

  /**
   * @param year the year to set
   */
  public void setYear(String year) {
    this.year = year;
  }

  /**
   * @return the make
   */
  public String getMake() {
    return make;
  }

  /**
   * @param make the make to set
   */
  public void setMake(String make) {
    this.make = make;
  }

  /**
   * @return the model
   */
  public String getModel() {
    return model;
  }

  /**
   * @param model the model to set
   */
  public void setModel(String model) {
    this.model = model;
  }

  /**
   * @return the garagedAddress
   */
  public String getGaragedAddress() {
    return garagedAddress;
  }

  /**
   * @param garagedAddress the garagedAddress to set
   */
  public void setGaragedAddress(String garagedAddress) {
    this.garagedAddress = garagedAddress;
  }

  /**
   * @return the financeType
   */
  public String getFinanceType() {
    return financeType;
  }

  /**
   * @param financeType the financeType to set
   */
  public void setFinanceType(String financeType) {
    this.financeType = financeType;
  }

  /**
   * @return the usage
   */
  public String getUsage() {
    return usage;
  }

  /**
   * @param usage the usage to set
   */
  public void setUsage(String usage) {
    this.usage = usage;
  }

  /**
   * @return the bodyType
   */
  public String getBodyType() {
    return bodyType;
  }

  /**
   * @param bodyType the bodyType to set
   */
  public void setBodyType(String bodyType) {
    this.bodyType = bodyType;
  }

  /**
   * @return the transmission
   */
  public String getTransmission() {
    return transmission;
  }

  /**
   * @param transmission the transmission to set
   */
  public void setTransmission(String transmission) {
    this.transmission = transmission;
  }

  /**
   * @return the nvic
   */
  public String getSummary() {
    return summary;
  }

  /**
   * @param nvic the nvic to set
   */
  public void setSummary(String summary) {
    this.summary = summary;
  }

  /**
   * @return the variant
   */
  public String getVariant() {
    return variant;
  }

  /**
   * @param variant the variant to set
   */
  public void setVariant(String variant) {
    this.variant = variant;
  }

	public String getCylinders() {
		return cylinders;
	}

	public void setCylinders(String cylinders) {
		this.cylinders = cylinders;
	}

	public String getAcceptable() {
		return acceptable;
	}

	public void setAcceptable(String acceptable) {
		this.acceptable = acceptable;
	}
}
