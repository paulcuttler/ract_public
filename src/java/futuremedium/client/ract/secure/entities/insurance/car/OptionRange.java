package futuremedium.client.ract.secure.entities.insurance.car;

/**
 * A bean class encapsulates the range element in insurance_quote_question.xml.
 * @author xmfu
 */
public class OptionRange {
  public static final String OPERATOR_PLUS = "+";
  public static final String TODAY = "today";
  public static final String DATE_TYPE = "date";
  public static final String THIS_YEAR = "thisyear";

  private String type;
  private String name;
  private String start;
  private String operator;
  private Integer days;
  private String end;

  public String getOperatorPlus() {
    return OPERATOR_PLUS;
  }

  /**
   * @return the start
   */
  public String getStart() {
    return start;
  }

  /**
   * @param start the start to set
   */
  public void setStart(String start) {
    this.start = start;
  }

  /**
   * @return the operator
   */
  public String getOperator() {
    return operator;
  }

  /**
   * @param operator the operator to set
   */
  public void setOperator(String operator) {
    this.operator = operator;
  }

  /**
   * @return the days
   */
  public Integer getDays() {
    return days;
  }

  /**
   * @param days the days to set
   */
  public void setDays(Integer days) {
    this.days = days;
  }

  /**
   * @return the end
   */
  public String getEnd() {
    return end;
  }

  /**
   * @param end the end to set
   */
  public void setEnd(String end) {
    this.end = end;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }
}
