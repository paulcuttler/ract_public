package futuremedium.client.ract.secure.entities;

import java.util.Properties;
import javax.annotation.*;
import javax.naming.InitialContext;

import org.springframework.stereotype.Service;

import com.ract.web.address.*;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.insurance.*;
import com.ract.web.membership.WebMembershipMgrRemote;
import com.ract.web.security.*;

import futuremedium.client.ract.secure.Config;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author gnewton
 */
@Service
public class BaseService {

  private UserSecurityMgrRemote userMgr;
  private AddressMgrRemote addressMgr;
  private CustomerMgrRemote customerMgr;
  private WebMembershipMgrRemote roadsideMgr;
  private WebInsMgrRemote insuranceMgr;
  private WebInsGlMgrRemote glassesMgr;
  private Config configManager;

  public BaseService() { }

  @PostConstruct
  public void initialise() throws Exception {
    InitialContext ctx = new InitialContext(this.getConfigManager().getConfiguration().getProperties("java.naming"));

    setUserMgr((UserSecurityMgrRemote) ctx.lookup("UserSecurityMgr/remote"));
    setInsuranceMgr((WebInsMgrRemote) ctx.lookup("WebInsMgr/remote"));
    setGlassesMgr((WebInsGlMgrRemote) ctx.lookup("WebInsGlMgr/remote"));
    setAddressMgr((AddressMgrRemote) ctx.lookup("AddressMgr/remote"));
    setCustomerMgr((CustomerMgrRemote) ctx.lookup("CustomerMgr/remote"));
    setRoadsideMgr((WebMembershipMgrRemote) ctx.lookup("WebMembershipMgr/remote"));
  }

  /**
   * @return the userMgr
   */
  public UserSecurityMgrRemote getUserMgr() {
    return userMgr;
  }

  /**
   * @param userMgr the userMgr to set
   */
  public void setUserMgr(UserSecurityMgrRemote userMgr) {
    this.userMgr = userMgr;
  }

  /**
   * @return the addressMgr
   */
  public AddressMgrRemote getAddressMgr() {
    return addressMgr;
  }

  /**
   * @param addressMgr the addressMgr to set
   */
  public void setAddressMgr(AddressMgrRemote addressMgr) {
    this.addressMgr = addressMgr;
  }

  /**
   * @return the customerMgr
   */
  public CustomerMgrRemote getCustomerMgr() {
    return customerMgr;
  }

  /**
   * @param customerMgr the customerMgr to set
   */
  public void setCustomerMgr(CustomerMgrRemote customerMgr) {
    this.customerMgr = customerMgr;
  }

  /**
   * @return the roadsideMgr
   */
  public WebMembershipMgrRemote getRoadsideMgr() {
    return roadsideMgr;
  }

  /**
   * @param roadsideMgr the roadsideMgr to set
   */
  public void setRoadsideMgr(WebMembershipMgrRemote roadsideMgr) {
    this.roadsideMgr = roadsideMgr;
  }

  /**
   * @return the insuranceMgr
   */
  public WebInsMgrRemote getInsuranceMgr() {
    return insuranceMgr;
  }

  /**
   * @param insuranceMgr the insuranceMgr to set
   */
  public void setInsuranceMgr(WebInsMgrRemote insuranceMgr) {
    this.insuranceMgr = insuranceMgr;
  }

  /**
   * @return the glassesMgr
   */
  public WebInsGlMgrRemote getGlassesMgr() {
    return glassesMgr;
  }

  /**
   * @param glassesMgr the glassesMgr to set
   */
  public void setGlassesMgr(WebInsGlMgrRemote glassesMgr) {
    this.glassesMgr = glassesMgr;
  }

  /**
   * @return the configManager
   */
  public Config getConfigManager() {
    return configManager;
  }

  /**
   * @param configManager the configManager to set
   */
  @Autowired
  public void setConfigManager(Config configManager) {
    this.configManager = configManager;
  }

}
