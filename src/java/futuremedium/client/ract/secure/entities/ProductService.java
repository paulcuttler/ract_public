package futuremedium.client.ract.secure.entities;

import java.math.BigDecimal;
import java.util.HashMap;

public class ProductService {
	
	private String productType;
	private String termLength;
	private HashMap<String, String> ultimateTerms;
	private HashMap<String, String> advantageTerms;
	private HashMap<String, String> lifestyleTerms;
	
	public ProductService(String product, String term) {
		this.setProductType(product);
		this.setTermLength(term);
		
		setup();
	}
	
	public ProductService(String combo) {
		String[] pandt = combo.split("-");
		setProductType(pandt[0]);
		setTermLength(pandt[1]);
		
		setup();
	}
	
	private void setup() {
		ultimateTerms = new HashMap<String, String>();
		ultimateTerms.put("monthly", "0:12:0:0:0:0:0");
		ultimateTerms.put("yearly", "1:0:0:0:0:0:0");
		
		advantageTerms = new HashMap<String, String>();
		advantageTerms.put("monthly", "0:12:0:0:0:0:0");
		advantageTerms.put("yearly", "1:0:0:0:0:0:0");
		
		lifestyleTerms = new HashMap<String, String>();
		lifestyleTerms.put("yearly", "1:0:0:0:0:0:0");
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getTermLength() {
		if(termLength == null) {
			return termLength;
		}
		
		if(productType == null) {
			return termLength;
		}
		
		String tl = null;
		if(productType == "Ultimate") {
			tl = ultimateTerms.get(termLength);
		} else if(productType == "Advantage") {
			tl = advantageTerms.get(termLength);
		} else if(productType == "Lifestyle") {
			tl = lifestyleTerms.get(termLength);
		}
		
		if(tl == null) {
			return "1:0:0:0:0:0:0";
		}
		
		return tl;
	}

	public void setTermLength(String termLength) {
		this.termLength = termLength.toLowerCase();
	}
	
	public static BigDecimal calculateAmountPayableTotal() {
		return new BigDecimal("12.00");
	}
	
	public static BigDecimal calculateAmountPaybleToday() {
		return new BigDecimal("12.00");
	}
	
	public String toString() {
		return this.productType + "-" + this.termLength;
	}
}
