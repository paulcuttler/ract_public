package futuremedium.client.ract.secure;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStore.SecretKeyEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.HashMap;

import javax.crypto.SecretKey;

/**
 * To use the secrets manager create a new instance of the class using the password
 * used to lock the key store file. 
 * <p>
 * This loads the key store and saves the password in memory. You can then call getSecret
 * to load a secret key. Use this key to store any encrypted passwords. 
 * <p>
 * Example code to load a secret key
 * <pre>
 * {@code
 *  SecretsManager sm = null;
		
	try {
		sm = new SecretsManager("12345678".toCharArray(), "resources/keystore.ks", true);
	} catch (InvalidKeyStore e) {
		e.printStackTrace();
	}
	
	SecretKey sk = sm.getSecret("jaspyt_password");
	
	if(sk != null) {
		byte[] encoded = sk.getEncoded();
		try {
			String str = new String(encoded, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
	}
 * }
 * 
 * @author dukes
 */
public class SecretsManager {
	private KeyStore keyStore; 
	private char[] password;
	private String resource;
	
	private HashMap<String, String> secretsCache;
	private static volatile SecretsManager instance;
	
	public static SecretsManager getInstance() throws InvalidKeyStore {
		if (instance == null) {
            synchronized (SecretsManager.class) {
                if (instance == null) {
                    instance = new SecretsManager("jbVy7IToUAimY23aEz493ojonI85PM7K".toCharArray(), System.getProperty("jboss.server.data.dir") + "/keystore.ks");
                }
            }
        }
 
        return instance;
	}
	
	/**
	 * Create the Secrets Manager and setup key store
	 * 
	 * @param password encryptor of the stored secrets. Should be external to the project
	 * @param resource File path to load relative from root
	 * @param load Whether or not to load the keystore file. (false if not created yet)
	 * @throws InvalidKeyStore If there is a problem loading the keystore. Could be a number of reasons
	 */
	private SecretsManager(char[] password, String resource) throws InvalidKeyStore {
		this.password = password;
		this.resource = resource;
		this.secretsCache = new HashMap<String, String>();
		
		try {
			this.loadKeyStore();
		} catch (KeyStoreException e) {
			e.printStackTrace();
			throw new InvalidKeyStore("Unable to load key store", e);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new InvalidKeyStore("Unable to load key store, Use an available algorithm", e);
		} catch (CertificateException e) {
			e.printStackTrace();
			throw new InvalidKeyStore("Unable to load key store, Certificate Error", e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new InvalidKeyStore("Unable to load key store file", e);
		}
	}
	
	/**
	 * Get the stored key. returns null if the key was corrupted of in an incorrect formate
	 * 
	 * @param secretName get a secret key for given entry name
	 * @return SecretKey Possibly null IFF there was a problem with the stored key
	 */
	public String getSecret(String secretName) {
		if(this.secretsCache.containsKey(secretName)) {
			return this.secretsCache.get(secretName);
		}
		
		Entry skEntry = null;
		SecretKey secretKey = null;
		String secret = null; 
		
		try {
			skEntry = this.keyStore.getEntry(secretName, new KeyStore.PasswordProtection(this.password));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnrecoverableEntryException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		
		if(skEntry != null) {
			secretKey = ((SecretKeyEntry) skEntry).getSecretKey();
			byte[] encoded = secretKey.getEncoded();
			
			try {
				secret = new String(encoded, "UTF-8");
				this.secretsCache.put(secretName, secret);
			} catch (UnsupportedEncodingException e) {
				
			}
			
		} 
		
		return secret;
	}
	
	/**
	 * Load the key store from the filesystem. Requires a password and file resource location(as string) 
	 * 
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 */
	private void loadKeyStore() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		this.keyStore = KeyStore.getInstance("jceks");

	    FileInputStream fis = null;
	    try {
	        fis = new FileInputStream(this.resource);
	        this.keyStore.load(fis, this.password);
	    } finally {
	        if (fis != null) {
	            fis.close();
	        }
	    }
	}
	
}