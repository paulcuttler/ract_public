
Event.on(window, 'load', function() {

	initExternalLinks();
  new shell();
  
});

function initExternalLinks() {
    ($$('a[rel="external"]')).each(function(elm) {
        elm.onclick = function() {window.open(this.href); return false; };
    });
}


var shell = Class.create({
	initialize: function() {
		
    this.size();  
    Event.on(window, 'resize', this.size.bind(this) );
		
  },
  size: function() {
	 	
    var funcHeight = $('bside').measure('height');
    var asideHeight = $('aside').measure('height');
    var greaterHeight;
    
    if(funcHeight > asideHeight) {
      greaterHeight = funcHeight;
    } else {
      greaterHeight = asideHeight;
    }
    
		var containerWidth = $('container').measure('width') + $('container').measure('padding-right');
    var windowDimensions = document.viewport.getDimensions();
    
    if(windowDimensions['height'] > greaterHeight && windowDimensions['width'] >= containerWidth ) {
      $('bside').setStyle({'position':'fixed'});
      $('aside').setStyle({'position':'fixed','top':'0','left':'50%','marginLeft':'-493px'});
    } else {
      $('bside').setStyle({'position':'absolute'});
      $('aside').setStyle({'position':'relative','top':'auto','left':'auto','marginLeft':'0'});
    }		
  }
});


				