

Event.on(window, 'load', function() {

  externalLinks();

  if($('article')) {
    
    formFields();

    $('memberDobDayPicker').on('change', updateMemberCard);
    $('memberDobMonthPicker').on('change', updateMemberCard);
    $('memberDobYearPicker').on('change', updateMemberCard);

    $('membershipCard1').on('blur', updateMemberCard);
    $('membershipCard2').on('blur', updateMemberCard);
    $('membershipCard3').on('blur', updateMemberCard);
    $('membershipCard4').on('blur', updateMemberCard);

    updateMemberCard();

  }
});

function formFields() {

  new smoothField('campaignForm_membertrue', {
    smoothFields: '.creditCard, .memberDate, .errorMessage.dateofbirthmemberDob, .errorMessage.cardNum',
    extraActionFields: '#campaignForm_memberfalse',
    eventType: 'click',
    fieldCase: function() {
      return $('campaignForm_membertrue').checked && !$('campaignForm_memberfalse').checked
    }
  });

  new smoothField('campaignForm_memberfalse', {
    smoothFields: '#wwgrp_title, #wwgrp_campaignForm_firstname, #wwgrp_campaignForm_lastname,'+
      '#wwgrp_campaignForm_gender, .dobNew, #wwgrp_postcode, #wwgrp_suburb, #wwgrp_streetSuburbId, #wwgrp_campaignForm_street_no, #wwgrp_campaignForm_phone,' +
      '#wwerr_title, #wwerr_campaignForm_firstname, #wwerr_campaignForm_lastname, #wwerr_campaignForm_gender, .dobNewError, #wwerr_postcode, #wwerr_suburb,' +
      '#wwerr_streetSuburbId, #wwerr_campaignForm_street_no',
    extraActionFields: '#campaignForm_membertrue',
    eventType: 'click',
    fieldCase: function() {
      return !$('campaignForm_membertrue').checked && $('campaignForm_memberfalse').checked
    }
  });


}

function externalLinks() {
  ($$('a[rel="external"]')).each(function(elm) {
    elm.onclick = function() {
      window.open(this.href);
      return false;
    };
  });
}

function updateMemberCard() {
  if($('memberDobDayPicker').value != '' && $('memberDobMonthPicker').value != '' && $('memberDobYearPicker').value != ''
    && ('membershipCard1').value != '' && $('membershipCard2').value != '' && $('membershipCard3').value != '' && $('membershipCard4').value != '') {

    var memberElementName = 'suburb'; //legacy
    var targetUrl = 'CampaignAjaxAction_validateMemberCard.action';

    var dob = $('memberDobDayPicker').value + '/' + $('memberDobMonthPicker').value + '/' + $('memberDobYearPicker').value;
    var memberCardNo = $('membershipCard1').value + ',' + $('membershipCard2').value + ',' + $('membershipCard3').value + ',' + $('membershipCard4').value;

    var req = newXMLHttpRequest();

    var paramString = "";

    paramString += "dob=" + dob;
    paramString += "&memberCardNo=" + memberCardNo;

    $$('.creditCard')[0].addClassName('loading');

    new Ajax.Request('CampaignAjax_validateMemberCard.action', {
      method: 'post',
      parameters: paramString,
      onSuccess: function(t) {

        var text = t.responseText;
        var memberJSON = text.evalJSON(true);

        $$('.creditCard')[0].removeClassName('loading');
        if(memberJSON !=null && memberJSON.validation == 'true') {
          $$('.creditCard')[0].removeClassName('show_error');
          $$('.creditCard')[0].addClassName('show_tick');
          $('updateEmailField').setStyle({'display':'block'});
        } else {
          $$('.creditCard')[0].removeClassName('show_tick');
          $$('.creditCard')[0].addClassName('show_error');
          $('updateEmailField').setStyle({'display':'none'});
        }

      }
    });

    
  }
}


/* smoothField */

var smoothField = Class.create({
  actionField: null,
  completeToggle: false,
  initialize: function(actionFieldId, options) {
    if ($(actionFieldId)) {
      this.actionField = $(actionFieldId);

      this.options = Object.extend({
        startState: 'smooth_vis', //elements that are visable before action
        endState: 'smooth_inv', //elements that are visable after action
        eventType: 'change',
        smoothFields: '',
        extraElements: '',
        extraActionFields: '',
        isInitialState: true, // start or end
        afterAction: function() { },
        fieldCase: function() {
          return this.parent.actionField.value != '';
        }
      }, options || {});
      this.options.parent = this;

      this.setupState();
      this.actionField.on(this.options.eventType, this.action.bind(this));
      $$(this.options.extraActionFields).each(function(elm) {
        elm.on(this.options.eventType, this.action.bind(this));
      }.bind(this));
    }
  },
  action: function() { //user action
    if (this.validateCase()) {
      this.toggleExtraElements();
      this.toggleFieldEffects();
      if(this.completeToggle) {
        this.options.isInitialState = !this.options.isInitialState;
      }
      this.options.afterAction();
    }
  },
  setupState: function() { //check state when it starts up because user can refesh page
    if (this.options.fieldCase()) {
      this.options.isInitialState = false;
      this.toggleExtraElements();
      this.toggleFieldEffects();
      this.options.afterAction();
    }
  },
  validateCase: function() { //toogle conditions
    if((this.options.fieldCase() && this.options.isInitialState) || (!this.options.fieldCase() && !this.options.isInitialState)) {
      return true;
    }
    return false;
  },
  toggleExtraElements: function() { //fade
    var start = this.options.startState;
    var end = this.options.endState;

    $$(this.options.extraElements).each(function(elm) {
      if(elm.effect != undefined){
        elm.effect.cancel();
        this.completeToggle = false;
      } else {
        this.completeToggle = true;
      }

      if(elm.hasClassName(end)) {
        elm.effect = new Effect.Fade(elm, {
          duration: 0.4,
          from: 0,
          to: 1,
          beforeStart: function() {
            elm.setStyle({
              'display':'block',
              opacity: 0
            });
          },
          afterFinish: function() {
            elm.addClassName(start);
            elm.removeClassName(end);
            elm.effect = undefined;
          }
        });
      } else {
        elm.effect = new Effect.Fade(elm, {
          duration: 0.4,
          from: 1,
          to: 0,
          afterFinish: function() {
            elm.addClassName(end);
            elm.removeClassName(start);
            elm.effect = undefined;
          }
        });
      }

    }.bind(this));
  },
  toggleFieldEffects: function() { //slide then fade
    var start = this.options.startState;
    var end = this.options.endState;

    $$(this.options.smoothFields).each(function(elm) {

      if(elm.effect != undefined){
        elm.effect2.cancel();
        elm.effect.cancel();
        this.completeToggle = false;
        height = elm.totalHeight;
      } else {
        this.completeToggle = true;
        var height = elm.measure('height');
        elm.totalHeight = height;
      }

      if (elm.hasClassName(end)) {

        elm.effect = new Effect.Morph(elm, {
          style: 'height: ' + height + 'px;',
          duration: 0.3,
          beforeStart: function() {
            elm.setStyle({
              'display':'block',
              'height':'0',
              opacity: 0
            });
          },
          afterFinish: function() {
            elm.effect2 = new Effect.Fade(elm, {
              duration: 0.3,
              from: 0,
              to: 1,
              afterFinish: function() {
                elm.addClassName(start);
                elm.removeClassName(end);
                elm.effect = undefined;
              }
            });
          }
        });
      } else {
        elm.effect = new Effect.Morph(elm, {
          duration: 0.3,
          style: 'opacity: 0;',
          afterFinish: function() {
            elm.effect2 = new Effect.Morph(elm, {
              style: 'height: 0; padding: 0;',
              duration: 0.5,
              afterFinish: function() {
                elm.effect = undefined;
                elm.setStyle({
                  'display':'none',
                  'padding':'',
                  'height': ''
                });
                elm.addClassName(end);
                elm.removeClassName(start);

              }
            });
          }
        });
      }
    }.bind(this));

  }
});