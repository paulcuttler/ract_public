
var streetsList = new Array();
var suburbsList = new Array();
var streetSuburbList = new Array();

var makeList = new Array();
var modelList = new Array();
var bodyTypeList = new Array();
var transmissionList = new Array();
var cylinderList = new Array();

var RESIDENTIAL = 'residential';
var POSTAL = 'postal';
var POBOX = 'poBox';

var SUBURB = 'suburb';
var STREET = 'street';

var WIDGET_COMBO = 'combo';
var WIDGET_FILTER = 'filter';

var MAKE = 1;
var MODEL = 2;
var BODY_TYPE = 3;
var TRANSMISSION = 4;
var CYLINDERS = 5;
var VEHICLE = 6;

var HPV_MSG_TXT;
var HPV_MSG = '<div id="hpvMsg" class="qHolder adjust-block"><div id="wwerr_hpv" class="wwerr"><div class="errorMessage" errorfor="hpv"> </div></div><dl id="wwgrp_hpv" class="wwgrp"><dt id="wwlbl_hpv" class="wwlbl"><label for="hpv" class="label"><span class="required">*</span> Please be aware of the following exclusions with regard to vehicle use</label></dt><dd id="wwctrl_hpv" class="wwctrl"><div class="wwctrl-inner"><ul><li>%HPV_MSG_TXT%</li></ul></div></dd></dl></div>';
//var VALUE_HPV = "high perf";
var VALUE_HPV = "h";

var dataUrl;

/**
 * Refresh suggestions for Streets & Suburbs and selections for StreetSuburbs
 * @param fld calling field
 * @param targetUrl data url
 * @param type address type RESIDENTIAL or POSTAL
 * @param widget widget type WIDGET_COMBO or WIDGET_FILTER
 */

var loadStreet = new Element('img', {'style': 'float: right; display: none;', src: 'secure_images/loading.gif', alt: 'Loading...', width: '32', height: '32', id: 'loadStreet'});
var loadSuburb = new Element('img', {'style': 'float: right; display: none;', src: 'secure_images/loading.gif', alt: 'Loading...', width: '32', height: '32', id: 'loadSuburb'});

function updateStreetSuburbs(fld, targetUrl, type, widget, memType) {
  dataUrl = targetUrl;

  var postcode = $(type + 'Postcode').value;

  if (postcode.length != 4) {
    return;
  }
  
  // prevent any postcode outside 7nnn for Lifestyle
  if (postcode.charAt(0) != '7') {
    postcode = '7000';
    $(type + 'Postcode').value = '7000';
  }

  //var street = $(type + 'Street').value;
  var suburb = $(type + 'Suburb').value;
  var paramString = "";
  var targetStore = SUBURB;

  selectPostcode(postcode, type);

  paramString += "postcode=" + postcode;


  // if caller is street, limit by selected suburb
  if (fld != null && fld.id == type + 'Street' && suburb != null && suburb != '') {
    //paramString += "&suburb=" + suburb;
    targetStore = STREET;
  }

  if(targetStore == STREET) {
    if($(type + 'Street') != null
        && $(type + 'Street').up('dd') != null
        && $(type + 'Street').up('dd').previous().down('#loadStreet') == null ) {
      $(type + 'Street').up('dd').previous().insert({ bottom: loadStreet });
    }
    loadStreet.show();
  } else if(targetStore == SUBURB) {
    if($(type + 'Suburb') != null 
        && $(type + 'Suburb').up('dd') != null 
        && $(type + 'Suburb').up('dd').previous().down('#loadSuburb') == null) {
      $(type + 'Suburb').up('dd').previous().insert({ bottom: loadSuburb });
    }
    loadSuburb.show();
  }

  var req = newXMLHttpRequest();
  var handlerFunction = getReadyStateHandler(req, function(xml) {
    _updateStreetSuburbResults(xml, targetStore, type, widget);
    if(targetStore == STREET) {
      loadStreet.hide();
    } else if(targetStore == SUBURB) {
      loadSuburb.hide();
    }
  });
  req.onreadystatechange = handlerFunction;
  req.open("POST", targetUrl, true);
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  req.send(paramString);

  

}

/**
 * AJAX - populate array via returned XML.
 * @param resultXML returned XML/JSON
 * @param targetStore are we updating the list of streets or suburbs?
 * @param type RESIDENTIAL, POSTAL or POBOX
 * @param widget widget type WIDGET_COMBO or WIDGET_FILTER
 */
function _updateStreetSuburbResults(resultXML, targetStore, type, widget) {
  var resultList = eval('(' + resultXML + ')'); // convert to JSON

  var tmpSuburbs = new Array();
  var tmpStreets = new Array();
  var tst;
  //var tmpStreetSuburbs = new Array();

  for (var i=0; i < resultList.streetSuburbs.length; i++) {
    // add to list if not present
    if (targetStore == SUBURB) {
      try {
        tst = ';' + tmpSuburbs.join(";") + ';';
        if (tst.indexOf(';' + resultList.streetSuburbs[i].suburb + ';') == -1) {
          tmpSuburbs.push(resultList.streetSuburbs[i].suburb);
        }
      } catch (e) {}
    } else {

      var selectedSuburb = $(type + 'Suburb').value;

      try {
        tst = ';' + tmpStreets.join(";") + ';';
        if (tst.indexOf(';' + resultList.streetSuburbs[i].street + ';') == -1) {
          // match selected suburb
          if (resultList.streetSuburbs[i].suburb == selectedSuburb) {
            tmpStreets.push(resultList.streetSuburbs[i].street);
          }
        }
      } catch (e) {}
    }
  }

  if (targetStore == SUBURB) {
    suburbsList = tmpSuburbs.sort();
  } else {
    streetsList = tmpStreets.sort();
  }

  resetStreetSuburbSuggestions(targetStore, type, widget);

  // prefill if only one result if we are not using a DOJO control.
  if (widget == null) {
    if (tmpStreets.length == 1) {
      $(type + 'Street').value = tmpStreets[0];
    }
    if (tmpSuburbs.length == 1) {
      $(type + 'Suburb').value = tmpSuburbs[0];
    }
  }
}

/**
 * Select appropriate State value based on postcode
 * @param postcode
 * @param type address type RESIDENTIAL or POSTAL
 */
function selectPostcode(postcode, type) {
  var fld = $(type + 'State');

  if (fld != null) {
    if (postcode.substr(0,2) == '26') { // ACT
      fld.selectedIndex = 7;
    } else if (postcode.charAt(0) == '3') { // VIC
      fld.selectedIndex = 1;
    } else if (postcode.charAt(0) == '2') { // NSW
      fld.selectedIndex = 2;
    } else if (postcode.charAt(0) == '4') { // QLD
      fld.selectedIndex = 3;
    } else if (postcode.charAt(0) == '5') { // SA
      fld.selectedIndex = 4;
    } else if (postcode.charAt(0) == '6') { // WA
      fld.selectedIndex = 5;
    } else if (postcode.charAt(0) == '7') { // TAS
      fld.selectedIndex = 0;
    } else if (postcode.charAt(0) == '0') { // NT
      fld.selectedIndex = 6;
    }
  }

}

/**
 * Resets the lookups and StreetSuburb selection based on type.
 * @param targetStore are we updating the list of streets or suburbs?
 * @param type address type RESIDENTIAL or POSTAL
 * @param widget widget type WIDGET_COMBO or WIDGET_FILTER (default is WIDGET_FILTER)
 */
function resetStreetSuburbSuggestions(targetStore, type, widget) {

  widget = (widget == null) ? WIDGET_FILTER : widget;

  // prepare suburb combobox
  if (targetStore == SUBURB) {
    var tmpSuburbs = { label: "name", identifier: "name", items:[] };

    for (var i=0; i<suburbsList.length; i++) {
      tmpSuburbs['items'][i] = {name: suburbsList[i]};
    }

    // setup data store
    var suburbStore = new dojo.data.ItemFileReadStore({
      data: tmpSuburbs
    });

    var suburbName = type + "Suburb";
    if (dijit.byId(suburbName)) {
      // update existing widget with store
      $(suburbName).value = '';
      dijit.byId(suburbName).store = suburbStore;
    } else {
      // init widget
      initSuburbSelect(suburbName, suburbStore, dataUrl, type, widget);
    }
  }

  // prepare street combobox
  if (targetStore == STREET) {
    var streetName = type + "Street";
    //if ($(suburbName).value != '') {
    var tmpStreets = { label: "name", identifier: "name", items:[] };
    for (i=0; i<streetsList.length; i++) {
      tmpStreets['items'][i] = {name: streetsList[i]};
    }

    var streetStore = new dojo.data.ItemFileReadStore({
      data: tmpStreets
    });

    if (dijit.byId(streetName)) {
      // update existing widget with store
      $(streetName).value = '';
      dijit.byId(streetName).store = streetStore;
    } else if ($(streetName)) {
      // init widget
      initStreetSelect(streetName, streetStore, dataUrl, type, widget);
    }
  }
}

/**
 * Initialise a Suburb selector widget.
 * @param suburbName name of target field
 * @param suburbStore json object representing data store
 * @param dataUrl data url
 * @param type address type RESIDENTIAL or POSTAL
 * @param widget widget type WIDGET_COMBO or WIDGET_FILTER
 */
function initSuburbSelect(suburbName, suburbStore, dataUrl, type, widget) {
  var suburbSelect = null;

  var streetName = suburbName.replace('Suburb', 'Street');

  var suburbParams = {
    id: suburbName,
    name: suburbName,
    value: $(suburbName).value,
    store: suburbStore,
    searchAttr: "name",
    onChange: function() {
      // clear street selection
      if($(type + 'Street') != null) {
        $(type + 'Street').value = '';
      }
      if ($('streetNumber') != null) {
        $('streetNumber').value = '';
      }
      updateStreetSuburbs($(streetName), dataUrl, type);
    }
  };

  if (widget == WIDGET_COMBO) {
    suburbSelect = new dijit.form.ComboBox(suburbParams, suburbName);
  } else if (widget == WIDGET_FILTER) {
    suburbSelect = new dijit.form.FilteringSelect(suburbParams, suburbName);
  }
}

/**
 * Initialise a Street selector widget.
 * @param streetName name of target field
 * @param streetStore json object representing data store
 * @param dataUrl data url
 * @param type address type RESIDENTIAL or POSTAL
 * @param widget widget type WIDGET_COMBO or WIDGET_FILTER
 */
function initStreetSelect(streetName, streetStore, dataUrl, type, widget) { //alert($(streetName).value);
  var streetSelect = null;
  var streetParams = {
    id: streetName,
    name: streetName,
    value: $(streetName).value,
    store: streetStore,
    searchAttr: "name"
  };

  if (widget == WIDGET_COMBO) {
    streetSelect = new dijit.form.ComboBox(streetParams, streetName);
  } else if (widget == WIDGET_FILTER) {
    streetSelect = new dijit.form.FilteringSelect(streetParams, streetName);
  }
}

/**
 * Refresh as many vehicle detail select boxes as possible based on values of other fields.
 * @param targetUrl
 * @param lookup the vehicle attribute to lookup
 */
function updateGlassesGuide(targetUrl, lookup) { //alert(targetUrl + '?' + paramString);
  
  var params = $('quoteForm').serialize(true);
  params['lookup'] = lookup;
  
  $("ajaxMessage").update(' ');
  
  if ($('year').value.length == 4) {
    pleaseWaitSelect($('make'));
    pleaseWaitSelect($('model'));
    pleaseWaitSelect($('bodyType'));
    pleaseWaitSelect($('transmission'));
    pleaseWaitSelect($('cylinders'));
    pleaseWaitSelect($('nvic'));
  }
  else
  {
      clearSelect($('make'));
      clearSelect($('model'));
      clearSelect($('bodyType'));
      clearSelect($('transmission'));
      clearSelect($('cylinders'));
      clearSelect($('nvic'));
      clearHpv();
  }
  
  new Ajax.Request(targetUrl, {
    method: 'post',
    parameters: params,

    onSuccess: function(transport){
      var response = transport.responseText;
      var responseJson = response.evalJSON(true);
        
      _updateGlassesGuideResults(responseJson);
    },
    onFailure: function() {
      $("ajaxMessage").update('<p>An error has occurred</p>');
    }
  });
  
}

/**
 * AJAX - populate array via returned XML
 */
function _updateGlassesGuideResults(resultList) {
  var messageElement = $("ajaxMessage");

  if (resultList != '') {
    //var resultList = eval('(' + resultXML + ')'); // convert to JSON

    var tmpMakes = new Array();
    var tmpModels = new Array();
    var tmpBodyTypes = new Array();
    var tmpTransmissions = new Array();
    var tmpCylinders = new Array();
    var tmpVehicles = new Array();
    var i;

    // Makes
    if (resultList.makes != null) {
      for (i=0; i < resultList.makes.length; i++) {
        try {
          tmpMakes.push(new Option(resultList.makes[i].description, resultList.makes[i].code));
        } catch (e) {}
      }
      makesList = tmpMakes;
      refreshSelect($('make'), makesList);
      clearSelect($('model'));
      clearSelect($('bodyType'));
      clearSelect($('transmission'));
      clearSelect($('cylinders'));
      clearSelect($('nvic'));
    }

    // Models
    if (resultList.models != null) {
      for (i=0; i < resultList.models.length; i++) {
        try {
          tmpModels.push(new Option(resultList.models[i].description, resultList.models[i].description));
        } catch (e) {}
      }
      modelsList = tmpModels;
      refreshSelect($('model'), modelsList);
      clearSelect($('bodyType'));
      clearSelect($('transmission'));
      clearSelect($('cylinders'));
      clearSelect($('nvic'));
    }

    // BodyTypes
    if (resultList.bodyTypes != null) {
      for (i=0; i < resultList.bodyTypes.length; i++) {
        try {
          tmpBodyTypes.push(new Option(resultList.bodyTypes[i].description, resultList.bodyTypes[i].code));
        } catch (e) {}
      }

      if ($('bodyTypediv') != null)
        $('bodyTypediv').className = "qHolder adjust-block";

      bodyTypeList = tmpBodyTypes;
      refreshSelect($('bodyType'), bodyTypeList);
      clearSelect($('transmission'));
      clearSelect($('cylinders'));
      clearSelect($('nvic'));
    }

    // Transmissions
    if (resultList.transmissions != null) {
      for (i=0; i < resultList.transmissions.length; i++) {
        try {
          tmpTransmissions.push(new Option(resultList.transmissions[i].description, resultList.transmissions[i].code));
        } catch (e) {}
      }

      if ($('transmissiondiv') != null)
        $('transmissiondiv').className = "qHolder adjust-block";

      transmissionList = tmpTransmissions;
      refreshSelect($('transmission'), transmissionList);
      clearSelect($('cylinders'));
      clearSelect($('nvic'));
    }

    // Cylinders
    if (resultList.cylinders != null) {
      for (i=0; i < resultList.cylinders.length; i++) {
        try {
          tmpCylinders.push(new Option(resultList.cylinders[i].description, resultList.cylinders[i].description));
        } catch (e) {}
      }

      if ($('cylindersdiv') != null)
        $('cylindersdiv').className = "qHolder adjust-block";

      cylinderList = tmpCylinders;
      refreshSelect($('cylinders'), cylinderList);
      clearSelect($('nvic'));
    }

    // Vehicle
    if (resultList.vehicles != null) {
      for (i=0; i < resultList.vehicles.length; i++) {
        try {
          tmpVehicles.push(new Option(resultList.vehicles[i].description, resultList.vehicles[i].nvic + ":" + resultList.vehicles[i].acceptable));
        } catch (e) {}
      }

      if ($('nvicdiv') != null)
        $('nvicdiv').className = "qHolder adjust-block";
      
      vehicleList = tmpVehicles;
      refreshSelect($('nvic'), vehicleList);
    }

    pleaseChooseSelect($('make'));
    pleaseChooseSelect($('model'));
    pleaseChooseSelect($('bodyType'));
    pleaseChooseSelect($('transmission'));
    pleaseChooseSelect($('cylinders'));
    pleaseChooseSelect($('nvic'));
    clearHpv();

    // auto-select single options

    if (resultList.vehicles != null && resultList.vehicles.length == 1) {
      $('nvic').options[1].setAttribute('selected', true);
      checkAcceptable($('nvic'));
    } else if (resultList.cylinders != null && resultList.cylinders.length == 1) {
      $('cylinders').options[1].setAttribute('selected', true);
      updateGlassesGuide('DataGlassesGuide.action', VEHICLE);
    } else if (resultList.transmissions != null && resultList.transmissions.length == 1) {
      $('transmission').options[1].setAttribute('selected', true);
      updateGlassesGuide('DataGlassesGuide.action', CYLINDERS);
    } else if (resultList.bodyTypes != null && resultList.bodyTypes.length == 1) {
      $('bodyType').options[1].setAttribute('selected', true);
      updateGlassesGuide('DataGlassesGuide.action', TRANSMISSION);
    } else if (resultList.models != null && resultList.models.length == 1) {
      $('model').options[1].setAttribute('selected', true);
      updateGlassesGuide('DataGlassesGuide.action', BODY_TYPE);
    }

  } else { // Validation of manufacture year has failed.

    var date = new Date();
    var currentYear = date.getFullYear();
     
    messageElement.style.display = 'block';
    messageElement.innerHTML = '<p>' + 'Year of Manufacture must be in the range  between 1900 and ' + currentYear + '.' + '</p>';
  }
}

function checkAcceptable(fld) {
  if (fld.value.toLowerCase().indexOf(VALUE_HPV) != -1) { 
    if (!$('hpvMsg')) {
      $('nvicdiv').insert({'after':HPV_MSG});
    }
  } else {
    clearHpv();
  }
}

function refreshSelect(fld, optionList) {
  clearSelect(fld);
  for (var i=0; i < optionList.length; i++) {
    fld.options[i+1] = new Option(optionList[i].text, optionList[i].value);
  }
}

function clearSelect(fld) {
  if (fld != null) {
    fld.options.length = 1;
  } // reset to initial instruction
}

function clearHpv() {
  if ($('hpvMsg')) {
    $('hpvMsg').remove();
  }
}

function pleaseWaitSelect(fld) {
  if (fld != null) {
    fld.disabled = true;
    fld.options[0].text = 'Loading...';
  }
}

function pleaseChooseSelect(fld) {
  if (fld != null) {
    fld.disabled = false;
    fld.options[0].text = 'Please choose...';
  }
}

function toggleVisible(fld) {
  fld.style.display = (fld.style.display == 'block') ? 'none' : 'block';
}
function toggleCheckbox(self, fld) {
  fld.style.display = (!self.checked) ? 'none' : 'block';
}
function toggleRadio(self, fld) {
  fld.style.display = (self.value == 'false') ? 'none' : 'block';
}

function calcExpiryDate(fld) {
  var parts = fld.options[fld.options.selectedIndex].value.split('/');

  var d = new Date();

  d.setUTCFullYear(parseInt(parts[2])+1,parseInt(parts[1])-1,parts[0]);

  $('endDate').value = d.getDate() + '/' + (parseInt(d.getMonth())+1) + '/' + d.getFullYear();
}

function toggleField(fld) {
  var vOptions = new Array('mobilePhone', 'homePhoneAreaCode', 'homePhone', 'dob');
  $('phoneHolders').className = 'wwctrl enable_' + fld;

  for (i=0; i < vOptions.length; i++) {
    if (vOptions[i].indexOf(fld) == -1) {
      $(vOptions[i]).disabled = true;
    } else {
      $(vOptions[i]).disabled = false;
    }
  }
}

/**
 * Lookup NVIC via rego, prefill vehicle fields
 * @param targetUrl
 */
function updateRego(targetUrl) { //alert(targetUrl + '?' + paramString);
  
  if ($('rego').value == '') return;  
  $('nvicDirect').value = '';
  
  $('loadingOverlay').setStyle({opacity:0.7});
  $('loadingOverlay').setStyle({display:'block'});
  
  var messageElement = $("ajaxMessage");
  messageElement.innerHTML = '&nbsp;';
  
  var params = $('quoteForm').serialize(true);
    
  new Ajax.Request(targetUrl, {
    method: 'post',
    parameters: params,

    onSuccess: function(transport){
      var response = transport.responseText;
      var responseJson = response.evalJSON(true);
  
      _updateRego(responseJson);
    },
    onFailure: function() {
      if (!$('wwerr_rego')) {
        $('wwgrp_rego').insert({'before':'<div id="wwerr_rego" class="wwerr"><div class="errorMessage" errorfor="rego">Your car was not found, enter your car description</div></div>'});
      }
    }
  });
  
}

/**
 * AJAX - populate array via returned JSON
 */
function _updateRego(resultList) {
  var messageElement = $("ajaxMessage");
  var initMsg = false;
  var msgContents;
  
  if (resultList != '') {
    //var responseJson = eval('(' + resultXML + ')'); // convert to JSON
    
    if ($('regoMsg') != null) {
      $('regoMsg').update('');
    }    
    
    var regoMsg = $('regoMsg');
    if (regoMsg == null) {
      initMsg = true;
      regoMsg = new Element('p', {'id': 'regoMsg'}); 
    }
    
    var a = new Element('a', {'href': '#'}).update('Not your car?');
    a.onclick = function() {
      $('q6rego').checked = false;
      $('q6lookup').checked = true;
      regoFields.action();
      vehicleDetailFields.action(); 
      return false;
    }

    // if (initMsg) {
    //   $('regoLookup').setStyle({'height': $('regoLookup').getHeight() + 60 + 'px'});
    // }
    
    $('rego').insert({'after': regoMsg});
        
    if (resultList.vehicle != null && resultList.vehicle[0].nvic != '') {
      $('nvicDirect').value = resultList.vehicle[0].nvic;      
      msgContents = resultList.vehicle[0].description;
      msgContents += '<br />';
      msgContents += resultList.vehicle[0].summary;
      
      if ($('wwerr_rego')) {
        $('wwerr_rego').remove();
      }
      
      if (resultList.vehicle[0].acceptable.toLowerCase().indexOf(VALUE_HPV) != -1) {
        if (!$('wwgrp_hpv')) {
          $('regoLookup').insert({'after':HPV_MSG});
        }
      }
      
    } else { // nvic not found
      $('nvicDirect').value = '';
      a.update('Enter your car description');
      msgContents = '';
      
      clearHpv();
      if (!$('wwerr_rego')) {
        $('wwgrp_rego').insert({'before':'<div id="wwerr_rego" class="wwerr"><div class="errorMessage" errorfor="rego">Your car was not found, enter your car description</div></div>'});
      }
    }

    if (initMsg) {
      $('regoLookup').setStyle({'height': $('regoLookup').getHeight() + 120 + 'px'});
    }
        
    regoMsg.update(msgContents);
    regoMsg.insert(a);
  }
  
  $('loadingOverlay').fade({duration: 0.3, from: 0.7, to: 0});
}
