var PREMIUM_OPTIONS_URL;
var BUSINESS_USE_CHECK_URL;
var STREET_SUBURB_URL;
var QAS_URL;

var VALUE_MANAGER_OTHER;
var VALUE_SECURITY_OTHER;
var VALUE_BUILDING_TYPE_CONJOINED;

var vehicleDetailFields;
var regoFields;

var ADDRESS_RESIDENTIAL = 'residential';
var ADDRESS_POSTAL = 'postal';
var verifiedQas = false;
var qasTimer;

/* window on load event */


Event.on(window, 'load', function() {
  
  loadMethods();
  AjaxShell = new Shell({
    ajax: true
  });
  //new Shell();
});

function loadMethods() {
  if($('vehicle')){
      
    navDisable();
    actionBtns();
    ajaxNav(); 
    vehicleSmoothFields();
    slider();
    vehiclePremiumOptions();
    
  } else {
    
    contentsCrossSell();
    coverStartDate();
    calc();
    //iconChanger();
    keyCapture();
    initPremiumOptions();
    initPostcodeTrigger();
    initBSB();
    setupSmoothFields();
    specifiedContents();
    payNow();
  }
  
  commonSmoothFields('residential');
  initQAS();
  nav();
  externalLinks();
  autoTab();
  new toolTip('toolTip', 'help');
  notes();
}

function vehiclePremiumOptions() {
  if($('step9')) {
        
    $('benefit-carHire').addClassName("smooth_inv");
    $('benefit-windscreen').addClassName("smooth_inv");
    $('benefit-fireTheft').addClassName("smooth_inv");
    
    $('benefit-stolen').addClassName("smooth_inv");
    $('benefit-key').addClassName("smooth_inv");
    $('benefit-emergency').addClassName("smooth_inv");
    
    $('benefit-uninsured').addClassName("smooth_inv");
        
    var compBenfits = '#benefit-stolen, #benefit-key, #benefit-emergency';
        
    if($('benefitsList').hasClassName('web')) {
      $('benefit-web').addClassName("smooth_inv");
      compBenfits += ', #benefit-web';
    }    
    
    new smoothField('sub2comprehensive', {
      smoothFields: compBenfits,
      extraActionFields: '#sub2thirdPartyDamage',
      eventType: 'click',
      fieldCase: function() {
        return $('sub2comprehensive').checked && !$('sub2thirdPartyDamage').checked
      }
    });
    
    new smoothField('sub2thirdPartyDamage', {
      smoothFields: '#benefit-uninsured',
      extraActionFields: '#sub2comprehensive',
      eventType: 'click',
      fieldCase: function() {
        return !$('sub2comprehensive').checked && $('sub2thirdPartyDamage').checked
      }
    });
        
    new smoothField('compOptionwindScreen1', {
      smoothFields: '#benefit-windscreen',
      extraActionFields: '#sub2comprehensive, #sub2thirdPartyDamage',
      eventType: 'click',
      fieldCase: function() {
        return $('sub2comprehensive').checked && !$('sub2thirdPartyDamage').checked && $('compOptionwindScreen1').checked
      }
    });
  
    new smoothField('compOptionhireCar1', {
      smoothFields: '#benefit-carHire',
      extraActionFields: '#sub2comprehensive, #sub2thirdPartyDamage',
      eventType: 'click',
      fieldCase: function() {
        return $('sub2comprehensive').checked && !$('sub2thirdPartyDamage').checked && $('compOptionhireCar1').checked
      }
    });
    
    new smoothField('tpOptionfireAndTheft1', {
      smoothFields: '#benefit-fireTheft',
      extraActionFields: '#sub2comprehensive, #sub2thirdPartyDamage',
      eventType: 'click',
      fieldCase: function() {
        return !$('sub2comprehensive').checked && $('sub2thirdPartyDamage').checked && $('tpOptionfireAndTheft1').checked
      }
    });
        
  }
  
}

function slider() {        
  if($('zoom_slider') && $('sliderTotal')) {
    var zoom_slider = $('zoom_slider');
    var slider_total = $('sliderTotal').select('span')[0];
    
    var temp = new Array();
    temp = $('hiddenValues').value.split(",");

    for (i=0; i<temp.length; i++) {
      temp[i] = temp[i]/1;
    }

    var lower = $('hiddenLower').value/1;
    var upper = $('hiddenUpper').value/1;
         
    new Control.Slider(zoom_slider.down('.handle'), zoom_slider, {
      range: $R(lower, upper),
      values: temp,
      onSlide: function(value) {
        slider_total.update(value);
        $($('hiddenField').value).value = value;
      },
      onChange: function(value) {
        slider_total.update(value);
        $($('hiddenField').value).value = value;
      },
      sliderValue: $('hiddenDefault').value
    });
  }
}

function actionBtns() {  
  if ($('quoteForm')) {
    $('quoteForm').onsubmit = function() {
      return false;
    }
  }
  
  var prev = $('prevStep');
  if(prev ) {
    prev.onclick = function() {
      submitQuoteForm('quoteForm','submit','back'); 
      return false;
    }
  }
  
  var next = $('nextStep');
  if(next) {
    next.onclick = function() {
      
      if ($('residentialAddressQuery')) {
        if (!verifiedQas) {
          queryQas(ADDRESS_RESIDENTIAL);
          return false;
        }      
      }
      
      if ($('postalAddressQuery')) {
        if (!verifiedQas) {
          queryQas(ADDRESS_POSTAL);
          return false;
        }      
      }
      
      submitQuoteForm('quoteForm','submit','next'); 
      return false;
    }
  }
  
}

function vehicleSmoothFields() {
    
  new smoothField('q5yes', {
    smoothFields: '#sub1, #sub2',
    extraActionFields: '#q5no',
    eventType: 'click',
    fieldCase: function() {
      return $('q5yes').checked && !$('q5no').checked
    }
  });
  
  new smoothField('q21yes', {
    smoothFields: '#sub9',
    extraActionFields: '#q21no',
    eventType: 'click',
    fieldCase: function() {
      return $('q21yes').checked && !$('q21no').checked
    }
  });

  new smoothField('underFinance', {
    smoothFields: '#sub9',
    //extraActionFields: '#q21no',
    eventType: 'click',
    fieldCase: function() {
      return ($('underFinance').value != 'N' && $('underFinance').value != '') 
    }
  });
  
  regoFields = new smoothField('q6rego', {
    smoothFields: '#regoLookup',
    extraActionFields: '#q6lookup',
    eventType: 'click',
    fieldCase: function() {
      return $('q6rego').checked && !$('q6lookup').checked
    }
  });
  
  vehicleDetailFields = new smoothField('q6lookup', {
    smoothFields: '#sub1, #sub2, #sub3, #bodyTypediv, #transmissiondiv, #cylindersdiv, #nvicdiv',
    extraActionFields: '#q6rego',
    eventType: 'click',
    fieldCase: function() {      
      if ($('hpvMsg')) {
        $('hpvMsg').remove();
      }
      return $('q6lookup').checked && !$('q6rego').checked
    }
  });
  
  var link = $$('#sub11 a')[0];
  
  var linkOnly = new smoothField(link, {
    smoothFields: '#sub9, #q12, #sub6, #sub7, #sub8, #sub9, #sub10',
    eventType: 'click',
    fieldCase: function() {
      return $('firstName2').value != '' || $('lastName2').value != '' || $('dob2').value != ''
    }
  });
  
  if(link){
    $('sub11').on('click', function() {
      linkOnly.toggleFieldEffects();
    });
  }
  
  AccidentsInthreeYears = new smoothField('q13sub1', {
    smoothFields: '#q13sub4',
    extraActionFields: '#q13sub2',
    eventType: 'keyup',
    fieldCase: function() {
      return $('fullDriverLicenseYear').value.length == 4 && $('accidentNumber').value != '' && $('accidentNumber').value == 2
    }
  });
  
  if($('q13sub4no')) {
    $('q13sub4no').onclick = function() {
      showAccidentsInthreeYearQuestion('QuoteAjaxAction_calculateNCB.action','accidentNumber','sub3','sub4','show','fullDriverLicenseYear','accidentNumber', 'noClaimbonus', 'true');
    }
  }
  
  new smoothField('sub2comprehensive', {
    smoothFields: '#sub6, #sub7, #sub8, #sub3, #sub4, #sub10, #sub11, #sub13',
    extraActionFields: '#sub2thirdPartyDamage',
    extraElements: '.toolTip.smooth_inv, .toolTip.smooth_vis',
    eventType: 'click',
    afterAction: function() { 
      if($('sub2comprehensive').checked) {
        showInsurance('comp'); 
      } else {
        showInsurance('tp'); 
      }
    },
    fieldCase: function() {
      return !$('sub2comprehensive').checked && $('sub2thirdPartyDamage').checked
    }
  });
  
  $$('#step9 #sub3 input, #step9 #sub10 input').each(function(elm) {
    elm.on('click', function() {
      getPremium('', '')
    });
  });
  
  new smoothField('q20yes', {
    smoothFields: '#sub1, #sub2, #sub3, #sub4, #sub5, #sub6, #sub7, #sub8',
    extraActionFields: '#q20no',
    eventType: 'click',
    fieldCase: function() {
      return $('q20yes').checked && !$('q20no').checked
    }
  });
  
  
  for (var i=1; i<10; i++) {
    if ($('sub5_' + i + 'yes')) {
      
      new smoothField('sub5_' + i + 'yes', {
        subYesVar: 'sub5_' + i + 'yes',
        subNoVar: 'sub5_' + i + 'no',
        smoothFields: '#preferContact_' + i + ', #sub6_' + i + ', #sub7_' + i + ', #sub8_' + i + ', #sub9_' + i + ', #residentialAddress_' + i + ', #sameAsGaragedAddressQ_' + i + ', #postalAddress_' + i + ', #sameAsAboveQ_' + i + ', #sub11_' + i + '',
        extraActionFields: '#sub5_' + i + 'no',
        eventType: 'click',
        fieldCase: function() {
          return $(this.subYesVar).checked && !$(this.subNoVar).checked
        }
      });
      
      new smoothField('sameAsGaragedAddressQ_' + i + 'no', {
        subYesVar: 'sub5_' + i + 'yes',
        subNoVar: 'sub5_' + i + 'no',
        sagYesVar: 'sameAsGaragedAddressQ_' + i + 'yes',
        sagNoVar: 'sameAsGaragedAddressQ_' + i + 'no',
        smoothFields: '#residentialSubPostCode_' + i + ', #residentialSubsuburb_' + i + ', #residentialSubStreet_' + i + ', #residentialSubstreetNumber_' + i + '',
        extraActionFields: '#sameAsGaragedAddressQ_' + i + 'yes, #sub5_' + i + 'yes, #sub5_' + i + 'no',
        eventType: 'click',
        fieldCase: function() {
          return $(this.sagNoVar).checked && !$(this.sagYesVar).checked && $(this.subYesVar).checked && !$(this.subNoVar).checked
        }
      });
            
      new smoothField('sameAsAboveQ_' + i + 'no', {
        subYesVar: 'sub5_' + i + 'yes',
        subNoVar: 'sub5_' + i + 'no',
        saaYesVar: 'sameAsAboveQ_' + i + 'yes',
        saaNoVar: 'sameAsAboveQ_' + i + 'no',
        smoothFields: '#subPostCode_' + i + ', #subsuburb_' + i + ', #subStreet_' + i + ', #subpobox_' + i + ', #substreetNumber_' + i + '',
        extraActionFields: '#sameAsAboveQ_' + i + 'yes, #sub5_' + i + 'yes, #sub5_' + i + 'no',
        eventType: 'click',
        fieldCase: function() {
          return $(this.saaNoVar).checked && !$(this.saaYesVar).checked && $(this.subYesVar).checked && !$(this.subNoVar).checked
        }
      });
            
      new smoothField('showAccident_' + i + 'yes', {
        saYesVar: 'showAccident_' + i + 'yes',
        saNoVar: 'showAccident_' + i + 'no',        
        smoothFields: '.toolTip.smooth_inv, #sub13_' + i + ', #accidentT1_' + i + ', #accidentD1_' + i + ', #accidentT2_' + i + ', #accidentD2_' + i + ', #accidentT3_' + i + ', #accidentD3_' + i + ', #accidentT4_' + i + ', #accidentD4_' + i + ', #accidentT5_' + i + ', #accidentD5_' + i + '',
        extraActionFields: '#showAccident_' + i + 'no',
        eventType: 'click',
        fieldCase: function() {
          return $(this.saYesVar).checked && !$(this.saNoVar).checked
        }
      });
    
    }
  }
  
  new smoothField('sub16yes', {
    smoothFields: '#sub17, #sub18, #sub19, #sub20, #sub21, #residentialAddress, #residentialSubPostCode, #residentialSubsuburb, #residentialSubStreet,' +
      ' #residentialSubstreetNumber, #postalAddress, #additionalSubPostCode, #additionalSubsuburb, #additionalSubStreet, #additionalSubpobox, #additionalSubstreetNumber,' +
      ' #sub26, #sub23, #sub24, #sub27',
    extraActionFields: '#sub16no',
    eventType: 'click',
    fieldCase: function() {
      return $('sub16yes').checked && !$('sub16no').checked
    }
  });
    
  new smoothField('paymentOptionsDD', {
    smoothFields: '#sub2, #q26, #sub6',  // Account name, Payment method, Payment option, DD date
    extraActionFields: '#paymentOptionsnonDD, #paymentOptionsannually',
    eventType: 'click',
    fieldCase: function() {
      return $('paymentOptionsDD').checked && !$('paymentOptionsannually').checked && !$('paymentOptionsnonDD').checked
    }
  });
  
  new smoothField('paymentOptionsannually', {
    smoothFields: '#sub3, #selectCardType, #sub4, #ccName',  // Account name, card number, Card type, Expiry date 
    extraActionFields: '#paymentOptionsDD, #paymentOptionsnonDD, #sub2ddCreditcard, #sub2ddBankaccount',
    eventType: 'click',
    fieldCase: function() {
      return (!$('paymentOptionsDD').checked
          && $('paymentOptionsannually').checked
          && !$('paymentOptionsnonDD').checked) ||
          ($('paymentOptionsDD').checked
          && !$('paymentOptionsannually').checked
          && !$('paymentOptionsnonDD').checked
          && $('sub2ddCreditcard').checked
          && !$('sub2ddBankaccount').checked)
      
    }
  });

  new smoothField('sub2ddBankaccount', {
    smoothFields: '#sub5, #sub1, #bankingTerms',
    extraActionFields: '#sub2ddCreditcard, #paymentOptionsDD, #paymentOptionsannually, #paymentOptionsnonDD',
    eventType: 'click',
    fieldCase: function() {
      
      return $('paymentOptionsDD').checked
      && !$('paymentOptionsannually').checked
      && !$('paymentOptionsnonDD').checked
      && !$('sub2ddCreditcard').checked
      && $('sub2ddBankaccount').checked
    }
  });
  
}

function commonSmoothFields(addressType) {  
  if ($(addressType + 'AddressQuery')) {
    qasForm = $(addressType + 'AddressQuery').up('form');
    
    new smoothField(qasForm.id + '_' + addressType + 'ReuseAddressyes', {
      frmVar: qasForm.id,
      typeVar: addressType,
      smoothFields: '#' + addressType + 'QasPicker, #' + addressType + 'ValidAddress',
      extraActionFields: '#' + qasForm.id + '_'+ addressType + 'ReuseAddressno',
      eventType: 'click',
      fieldCase: function() {
        if ($(this.frmVar + '_' + this.typeVar + 'ReuseAddressno').checked) {
          $(this.typeVar + 'AddressQuery').value = '';
          clearAddress();
          verifiedQas = false;
        }
        return $(this.frmVar + '_' + this.typeVar + 'ReuseAddressno').checked 
          && !$(this.frmVar + '_' + this.typeVar + 'ReuseAddressyes').checked
      }
    });
  }
}

function contentsCrossSell() {
  
  var upgradeContents = $('upgradeContents');
  var addContents = getCookie("addContents");

  if (upgradeContents){

    if (addContents != null && addContents != "" && addContents == "yes") {
      upgradeContents.checked = true;
    }

    upgradeContents.on('change', function() {
      if(this.checked) {
        document.cookie = "addContents=yes";
      } else {
        document.cookie = "addContents=no";
      }
    });

  } else if ($('homeCalc') == null && addContents != null && addContents != "") {
    document.cookie = "addContents=no";
  }

}

function getCookie ( cookie_name ) {
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}

function coverStartDate() {
  var startDate = $('aboutYourHome_coverStartDate');
  if(startDate) {
    startDate.on('change', function() {
      $('navCoverStartDate').innerHTML = this.value;
    });
  }
}


function nav() {

  var nav = $('nav');
  if(nav) {
    if(nav.hasClassName('disableCompleted')) {
      nav.select('.completed a').each(function(elm) {
        
        elm.onclick = function() {
          return false;
        }
      });
    }
  }

}

function payNow() {
  var payForm = $('HomePaymentOptions');
  
  if (payForm) {
    $('submitNext').onclick = function() {
      $('loadingOverlay').setStyle({opacity:0.7});
      $('loadingOverlay').setStyle({display:'block'});
      
      $('submitNext').disabled = true;
    }    
  }
}

function specifiedContents() {
  var specifiedContents = $$('table.specifiedContents')[0];

  if(specifiedContents){
    $('submitNext').onclick = function() {
      var valid = true;

      specifiedContents.select('.wwerr').each(function(elm) {
        elm.remove();
      });

      var rowList = specifiedContents.select('tr');
      rowList.shift();
      
      rowList.each(function(elm) {

        if(!((elm.select('select')[0].value != '' && elm.select('input')[0].value != '' && elm.select('input')[1].value != '') || (elm.select('select')[0].value == '' && elm.select('input')[0].value == '' && elm.select('input')[1].value == '')) || elm.select('input')[1].value == '0') {
          
          valid = false;

          var tr = new Element('tr', {
            'class': 'wwerr',
            'style':'background: #f6e5e5'
          });
          var td = new Element('td', {
            'colspan': '4', 
            'width' : 'auto'
          }).update('Please select the type, enter a valid value and a detailed description');
          tr.appendChild(td);

          specifiedContents.setStyle({
            'height':'auto'
          });

          elm.insert({
            before: tr
          });

        }
      });


      return valid;
      

    }
  }
}

function autoTab() {
  
  var inputs = $$('input');
  inputs.each(function(elm, index) {
    if(elm.hasClassName('autoTab')){
      elm.index = index;
      elm.on('keyup', function(){
        if(this.maxLength == this.value.length) {
          inputs[this.index + 1].focus();
        }
      });
    }
  });
}


function setupSmoothFields(){


  new smoothField('HomeAboutYourHome_manager', {
    smoothFields: '#wwgrp_HomeAboutYourHome_managerOther, #wwerr_HomeAboutYourHome_managerOther',
    eventType: 'change',
    fieldCase: function() {
      return $('HomeAboutYourHome_manager').value == VALUE_MANAGER_OTHER
    }
  });


  new smoothField('differntPostalAddress', {
    smoothFields: '#postalAddressFields',
    eventType: 'click',
    fieldCase: function() {
      return $('differntPostalAddress').checked
    }
  });


  new smoothField('upgradeBuilding', {
    smoothFields: '.cta.home, .wide, #wwgrp_HomeSumInsured_buildingSumInsured, .toolTip.smooth_inv',
    eventType: 'click',
    fieldCase: function() {
      return $('upgradeBuilding').checked
    }
  });

  new smoothField('upgradeContents', {
    smoothFields: '.cta.contents, .wide, #wwgrp_HomeSumInsured_contentsSumInsured, .toolTip.smooth_inv',
    eventType: 'click',
    fieldCase: function() {
      return $('upgradeContents').checked
    },
    afterAction: function() { 
      $('sumMessage').innerHTML += " and "; 
      $('sumMessage').insert({
        bottom: $('contentsURL')
      });
      $('contentsURL').show();
    }
  });


  new smoothField('newQuoteForm_quoteTypebldg_cnts', {
    smoothFields: '.tnc, .wide, .actions, .errorMessage',
    extraActionFields: '#newQuoteForm_quoteTypebldg, #newQuoteForm_quoteTypecnts, #newQuoteForm_quoteTypeinv',
    extraElements: '.focusLink',
    eventType: 'click',
    fieldCase: function() {
      return $('newQuoteForm_quoteTypebldg_cnts').checked
        || $('newQuoteForm_quoteTypebldg').checked
        || $('newQuoteForm_quoteTypecnts').checked
        || $('newQuoteForm_quoteTypeinv').checked
    }
  });
  new smoothField('HomeAboutYou_membertrue', {
    smoothFields: '.creditCard, .cardNum',
    extraActionFields: '#HomeAboutYou_memberfalse',
    eventType: 'click',
    fieldCase: function() {
      return $('HomeAboutYou_membertrue').checked && !$('HomeAboutYou_memberfalse').checked
    }
  });
  new smoothField('membershipCard1', {
    smoothFields: '.memberDob',
    eventType: 'keyup',
    extraActionFields: '#membershipCard2, #membershipCard3, #membershipCard4',
    fieldCase: function() {
      return checkCardNumber();
    }
  });
  new smoothField('dobDayPicker', {
    smoothFields: '#wwgrp_HomeAboutYou_fiftyPlus, #wwerr_HomeAboutYou_fiftyPlus',
    extraElements: '.fiftyPlus',
    extraActionFields: '#dobMonthPicker, #dobYearPicker',
    fieldCase: function() {
      return checkAge();
    }
  });
  new smoothField('buildingType', {
    smoothFields: '#wwgrp_HomeAboutYourHome_buildingTypeConjoined, #wwerr_HomeAboutYourHome_buildingTypeConjoined',
    fieldCase: function() {
      return checkBuildingType();
    }
  });
  new smoothField('securityLock', {
    smoothFields: '#wwgrp_HomeAboutYourHome_securityOther, #wwerr_HomeAboutYourHome_securityOther',
    fieldCase: function() {
      return checkSecurityLock();
    }
  });

  /*if($('HomeAddressDetailsForm') == null) {
    new postCodeSmoothField('residentialPostcode', {
      smoothFields: '#wwgrp_residentialSuburb, #wwerr_residentialSuburb, #wwgrp_residentialStreet, #wwerr_residentialStreet, #wwgrp_streetNumber, #wwerr_streetNumber',
      eventType: 'keyup'
    });
  }*/
    
  new smoothField('aboutYourHome_currentlyUnoccupiedtrue', {
    smoothFields: '.within90Days',
    extraActionFields: '#aboutYourHome_currentlyUnoccupiedfalse',
    eventType: 'click',
    fieldCase: function() {
      return $('aboutYourHome_currentlyUnoccupiedtrue').checked && !$('aboutYourHome_currentlyUnoccupiedfalse').checked
    }
  });

  new businessUseSmoothField('businessUse', {
    smoothFields: '#businessUseInfo',
    eventType: 'change'
  });
  
  new smoothField('aboutYourHome_workUndertakentrue', {
    smoothFields: '.exceed50K',
    extraActionFields: '#aboutYourHome_workUndertakenfalse',
    eventType: 'click',
    fieldCase: function() {
      return $('aboutYourHome_workUndertakentrue').checked && !$('aboutYourHome_workUndertakenfalse').checked
    }
  });
  new smoothField('aboutYourHome_unrelatedPersonstrue', {
    smoothFields: '.unrelatedExceed2',
    extraActionFields: '#aboutYourHome_unrelatedPersonsfalse',
    eventType: 'click',
    fieldCase: function() {
      return $('aboutYourHome_unrelatedPersonstrue').checked && !$('aboutYourHome_unrelatedPersonsfalse').checked
    }
  });
  new smoothField('aboutYourHome_underFinancetrue', {
    smoothFields: '#wwerr_aboutYourHome_financier, #wwgrp_aboutYourHome_financier',
    extraActionFields: '#aboutYourHome_underFinancefalse',
    eventType: 'click',
    fieldCase: function() {
      return $('aboutYourHome_underFinancetrue').checked && !$('aboutYourHome_underFinancefalse').checked
    }
  });
  new smoothField('HomeSpecifiedContents_specifytrue', {
    smoothFields: '.specifiedContents',
    extraActionFields: '#HomeSpecifiedContents_specifyfalse',
    eventType: 'click',
    fieldCase: function() {
      return $('HomeSpecifiedContents_specifytrue').checked && !$('HomeSpecifiedContents_specifyfalse').checked
    }
  });
  new smoothField('policyHolder_selectedClient', {
    smoothFields: '.additionalPolicyHolder',
    eventType: 'change',
    fieldCase: function() {
      return $('policyHolder_selectedClient').nodeName == 'SELECT' && $('policyHolder_selectedClient').value == '-1'
    }
  });


  /* Payment Options start */

  new smoothField('HomePaymentOptions_paymentOptionDD', {
    smoothFields: '#wwgrp_HomePaymentOptions_directDebitMethod, #wwgrp_HomePaymentOptions_frequency, #wwerr_HomePaymentOptions_frequency, #paymentDayNote', 
    extraActionFields: '#HomePaymentOptions_paymentOptionnonDD, #HomePaymentOptions_paymentOptionannually',
    eventType: 'click',
    fieldCase: function() {
      return $('HomePaymentOptions_paymentOptionDD').checked && !$('HomePaymentOptions_paymentOptionannually').checked && !$('HomePaymentOptions_paymentOptionnonDD').checked
    }
  });

  new smoothField('HomePaymentOptions_paymentOptionannually', {
    smoothFields: '#wwgrp_HomePaymentOptions_cardType, .ccf, .datePicker, #wwgrp_HomePaymentOptions_creditCardName, #wwerr_HomePaymentOptions_cardType, .cc, .df, #wwerr_HomePaymentOptions_creditCardName',
    extraActionFields: '#HomePaymentOptions_paymentOptionnonDD, #HomePaymentOptions_paymentOptionDD, #HomePaymentOptions_directDebitMethodddCreditcard, #HomePaymentOptions_directDebitMethodddBankaccount',
    eventType: 'click',
    fieldCase: function() {
      return (!$('HomePaymentOptions_paymentOptionDD').checked
        && $('HomePaymentOptions_paymentOptionannually').checked
        && !$('HomePaymentOptions_paymentOptionnonDD').checked) ||
        ($('HomePaymentOptions_paymentOptionDD').checked
        && !$('HomePaymentOptions_paymentOptionannually').checked
        && !$('HomePaymentOptions_paymentOptionnonDD').checked
        && $('HomePaymentOptions_directDebitMethodddCreditcard').checked
        && !$('HomePaymentOptions_directDebitMethodddBankaccount').checked)
    }
  });

  new smoothField('HomePaymentOptions_directDebitMethodddBankaccount', {
    smoothFields: '.bankAccount, #wwerr_HomePaymentOptions_accountNo, #wwerr_HomePaymentOptions_accountName, .bankAccountError, #bankingTerms',
    extraActionFields: '#HomePaymentOptions_paymentOptionnonDD, #HomePaymentOptions_paymentOptionDD, #HomePaymentOptions_directDebitMethodddCreditcard, #HomePaymentOptions_paymentOptionannually, ',
    eventType: 'click',
    fieldCase: function() {
      return $('HomePaymentOptions_paymentOptionDD').checked
        && !$('HomePaymentOptions_paymentOptionannually').checked
        && !$('HomePaymentOptions_paymentOptionnonDD').checked
        && !$('HomePaymentOptions_directDebitMethodddCreditcard').checked
        && $('HomePaymentOptions_directDebitMethodddBankaccount').checked
    }
  });

  /* Payment Options end */


}


/* misc */


function externalLinks() {
  ($$('a[rel="external"]')).each(function(elm) {
    elm.onclick = function() {
      window.open(this.href);
      return false;
    };
  });
}

/**
 * Suppress key strokes based on class.
 */
function keyCapture() {
  ($$('input[type="text"]')).each(function(elm) {
    if (elm.hasClassName('alpha-only')) {
      elm.onkeypress = function(e) {
        return isAlphaKey(e);
      }
    }
    if (elm.hasClassName('number-only')) {
      elm.onkeypress = function(e) {
        return isNumberKey(e);
      }
    }
    if (elm.hasClassName('ignore-submit')) {
      elm.onkeypress = function(e) {
        return !isSubmitKey(e);
      }
    }
  });
}

function iconChanger() {
  if ($('newQuoteForm')) {

    function getNext() {
      var icon = $RF('newQuoteForm', 'quoteType');

      if(icon == null || icon == '') {
        icon = 'bldg_cnts';
      }
      return icon;
    }

    $$('#icons .' + getNext())[0].addClassName('selected');

    [$('newQuoteForm_quoteTypebldg_cnts'), $('newQuoteForm_quoteTypebldg'), $('newQuoteForm_quoteTypecnts'), $('newQuoteForm_quoteTypeinv')].each(function(elm) {
      elm.onclick = function() {
        var next = $$('#icons .' + getNext())[0];
        var current = $$('#icons .selected')[0];
        if (next != current) {
          next.addClassName('next');
          next.setStyle({
            'display':'block'
          });
          new Effect.Fade(current, {
            duration: 0.5,
            from: 1,
            to: 0,
            afterFinish: function() {
              next.addClassName('selected');
              next.removeClassName('next');
              current.removeClassName('selected');
            }
          });
        }
      }
    });
  }
}





/* smoothField */

var smoothField = Class.create({
  actionField: null,
  completeToggle: false,
  initialize: function(actionFieldId, options) {
    if ($(actionFieldId)) {
      this.actionField = $(actionFieldId);

      this.options = Object.extend({
        startState: 'smooth_vis', //elements that are visable before action
        endState: 'smooth_inv', //elements that are visable after action
        eventType: 'change',
        smoothFields: '',
        extraElements: '', 
        extraActionFields: '',
        isInitialState: true, // start or end
        afterAction: function() { },
        fieldCase: function() {
          return this.parent.actionField.value != '';
        }
      }, options || {});
      this.options.parent = this;
     
      this.setupState();
      this.actionField.on(this.options.eventType, this.action.bind(this));
      $$(this.options.extraActionFields).each(function(elm) {
        elm.on(this.options.eventType, this.action.bind(this));
      }.bind(this));
    }
  },
  action: function() { //user action
    if (this.validateCase()) {
      this.toggleExtraElements();
      this.toggleFieldEffects();
      if(this.completeToggle) {
        this.options.isInitialState = !this.options.isInitialState;
      }
      this.options.afterAction();
    }
  },
  setupState: function() { //check state when it starts up because user can refesh page
    if (this.options.fieldCase()) {
      this.options.isInitialState = false;
      this.toggleExtraElements();
      this.toggleFieldEffects();
      this.options.afterAction();
    }
  },
  validateCase: function() { //toogle conditions
    if((this.options.fieldCase() && this.options.isInitialState) || (!this.options.fieldCase() && !this.options.isInitialState)) {
      return true;
    }
    return false;
  },
  toggleExtraElements: function() { //fade
    var start = this.options.startState;
    var end = this.options.endState;

    $$(this.options.extraElements).each(function(elm) {
      if(elm.effect != undefined){
        elm.effect.cancel();
        this.completeToggle = false;
      } else {
        this.completeToggle = true;
      }

      if(elm.hasClassName(end)) {
        elm.effect = new Effect.Fade(elm, {
          duration: 0.4,
          from: 0,
          to: 1,
          beforeStart: function() {
            elm.setStyle({
              'display':'block', 
              opacity: 0
            });
          },
          afterFinish: function() {
            elm.addClassName(start);
            elm.removeClassName(end);
            elm.effect = undefined;
          }
        });
      } else {
        elm.effect = new Effect.Fade(elm, {
          duration: 0.4,
          from: 1,
          to: 0,
          afterFinish: function() {
            elm.addClassName(end);
            elm.removeClassName(start);
            elm.effect = undefined;
          }
        });
      }

    }.bind(this));
  },
  toggleFieldEffects: function() { //slide then fade
    var start = this.options.startState;
    var end = this.options.endState;

    $$(this.options.smoothFields).each(function(elm) {

      if(elm.effect != undefined){
        elm.effect2.cancel();
        elm.effect.cancel();
        this.completeToggle = false;
        height = elm.totalHeight;
      } else {
        this.completeToggle = true;
        var height = elm.measure('height');
        elm.totalHeight = height;
      }

      if (elm.hasClassName(end)) {

        elm.effect = new Effect.Morph(elm, {
          style: 'height: ' + height + 'px;',
          duration: 0.3,
          beforeStart: function() {
            elm.setStyle({
              'display':'block', 
              'height':'0', 
              opacity: 0
            });
          },
          afterFinish: function() {
            elm.effect2 = new Effect.Fade(elm, {
              duration: 0.3,
              from: 0,
              to: 1,
              afterFinish: function() {
                elm.addClassName(start);
                elm.removeClassName(end);
                elm.effect = undefined;
              }
            });
          }
        });
      } else {
        elm.effect = new Effect.Morph(elm, {
          duration: 0.3,
          style: 'opacity: 0;',
          afterFinish: function() {
            elm.effect2 = new Effect.Morph(elm, {
              style: 'height: 0; padding: 0;',
              duration: 0.5,
              afterFinish: function() {
                elm.effect = undefined;
                elm.setStyle({
                  'display':'none', 
                  'padding':'', 
                  'height': ''
                });
                elm.addClassName(end);
                elm.removeClassName(start);
                
              }
            });
          }
        });
      }
    }.bind(this));

  }
});


var postCodeSmoothField = Class.create(smoothField, {
  action: function() {
    var run = function() {
      this.toggleExtraElements();
      this.toggleFieldEffects();
      if(this.completeToggle) {
        this.options.isInitialState = !this.options.isInitialState;
      }
    }.bind(this)

    this.options.afterAction();
    this.ajaxData(run);
  },
  setupState: function() {
    var setup = function() {
      this.options.isInitialState = false;
      this.toggleExtraElements();
      this.toggleFieldEffects();
    }.bind(this)
    this.ajaxData(setup);
  },
  ajaxData: function(func) {

    if (!evt) var evt = window.event;
    var fld = $('residentialPostcode');

    var re_tasPostcode = /^7\d{3}$/;
    if (fld.value.search(re_tasPostcode) == -1 && !this.options.isInitialState) {
      func();
    } else if (fld.value.search(re_tasPostcode) == -1 ) {
      return false; 
    } else {

      var params = "postcode=" + $(RESIDENTIAL + 'Postcode').value;

      var imgElm = new Element('img', {
        'style': 'float: right;', 
        src: 'secure_images/loading.gif', 
        alt: 'Loading...', 
        width: '32', 
        height: '32'
      });
      $('wwlbl_residentialPostcode').insert({
        bottom: imgElm
      });
        
      new Ajax.Request(STREET_SUBURB_URL, {
        method: 'post',
        parameters: params,
        onSuccess: function(transport){
          try {
            var response = transport.responseText;
            var responseJson = response.evalJSON(true);
          } catch (e) {
            throw(e);
          }

          if(responseJson.streetSuburbs.size() != 0 && this.options.isInitialState || responseJson.streetSuburbs.size() == 0 && !this.options.isInitialState) {
            func();
          }


          updateStreetSuburbs($('residentialSuburb'), STREET_SUBURB_URL, RESIDENTIAL, WIDGET_FILTER);
          updateStreetSuburbs($('residentialStreet'), STREET_SUBURB_URL, RESIDENTIAL, WIDGET_FILTER);

          imgElm.hide();

        }.bind(this)
      });

    }
  }
});

var businessUseSmoothField = Class.create(smoothField, {
  oldShowBusinessUseInfo: null,
  action: function() {
    this.checkBusinessUse();
  },
  setupState: function() {
    this.checkBusinessUse();
  },
  checkBusinessUse: function(){
    
    var params = $('aboutYourHome').serialize(true);
    new Ajax.Request(BUSINESS_USE_CHECK_URL, {
      method: 'post',
      parameters: params,
      onSuccess: function(transport){
        try {
          var response = transport.responseText;
          var responseJson = response.evalJSON(true);
          var showBusinessUseInfo = false;
          showBusinessUseInfo = responseJson.showBusinessUseInfo;
        } catch (e) {
          throw(e);
        }
          
        //alert('oldShowBusinessUseInfo: ' + this.oldShowBusinessUseInfo + ';showBusinessUseInfo: ' + showBusinessUseInfo + ';result: ' + ((showBusinessUseInfo != this.oldShowBusinessUseInfo && this.oldShowBusinessUseInfo != null) || (this.oldShowBusinessUseInfo == null && this.showBusinessUseInfo == true)));

        if ((showBusinessUseInfo != this.oldShowBusinessUseInfo && this.oldShowBusinessUseInfo != null) || (this.oldShowBusinessUseInfo == null && showBusinessUseInfo == true)) {
          this.toggleFieldEffects();
        }

        this.oldShowBusinessUseInfo = showBusinessUseInfo;
      }.bind(this)
    });

  }
});

/* ToolTip */


var toolTip = Class.create({
  initialize: function(actionClass, containerId) {
    this.tipMarkup(containerId);
    this.linkTip(actionClass);
  },
  tipMarkup: function(containerId) {
    this.toolTip = new Element('span', {
      id: containerId,
      style: 'display: none'
    });
    $$('body')[0].insert({
      bottom: this.toolTip
    });
  },
  linkTip: function(actionClass){
    $$('.' + actionClass).each(function(elm){
      if (elm.title != '' && elm.title != null) {
        Event.observe(elm, 'mouseenter', this.showToolTip.bind(this));
        Event.observe(elm, 'mouseleave', this.hideToolTip.bind(this));
      }
    }.bind(this));
  },
  showToolTip: function(e) {

    elm = Event.element(e).up('a, span');
    if(elm == undefined) {
      elm = Event.element(e);
    } //IE bug fix
    this.currentElm = elm;
    this.toolTip.update(elm.title);
    elm.title = '';

    Event.observe(elm, 'mousemove', function(e) {

      this.toolTip.setStyle({
        position: 'absolute',
        zindex: '300',
        top: Event.pointerY(e) + 'px',
        left: Event.pointerX(e) + 15 + 'px'
      });

    }.bind(this));

    this.toolTip.show();
  },
  hideToolTip: function() {
    this.currentElm.title = this.toolTip.innerHTML;
    this.toolTip.hide();
  }
});


/**
 * Verify age is between 50 and 110 inclusive.
 */
function checkAge() {

  var testDate = new Date();
  testDate.setHours(0,0,0,0);

  if ($("dobYearPicker").value == '' || $("dobMonthPicker").value == '' || $("dobDayPicker").value == '') {
    return false;
  }
  
  var dob = new Date($("dobYearPicker").value, ($("dobMonthPicker").value - 1), $("dobDayPicker").value, 0,0,0,0);

  if (dob >= testDate) { //date in future
    return false;
  }

  testDate.setFullYear(testDate.getFullYear() - 50);
  testDate.setHours(0,0,0,0);
  if (testDate < dob) { // younger than 50
    return false;
  }

  testDate = new Date();
  testDate.setFullYear(testDate.getFullYear() - 111);
  testDate.setHours(0,0,0,0);
  if (testDate >= dob) { // older than 110
    return false;
  }

  return true;
}



/* shell */


var Shell = Class.create({
  initialize: function(options) {
    
    this.options = Object.extend({
      ajax: false
    }, options || {});
    
    this.size();  
    Event.on(window, 'resize', this.size.bind(this) );
    
    $$('body')[0].insert({bottom: new Element( 'div' ,{"id": "loadingOverlay"}).update("<div><!-- --></div>")});
    
    
  },
  size: function() {
	 	
    var funcHeight = $('bside').measure('height');
    var asideHeight = $('aside').measure('height');
    var greaterHeight;
    
    if(funcHeight > asideHeight) {
      greaterHeight = funcHeight;
    } else {
      greaterHeight = asideHeight;
    }
    
    var containerWidth = $('container').measure('width') + $('container').measure('padding-right');
    var windowDimensions = document.viewport.getDimensions();
    
    if(windowDimensions['height'] > greaterHeight && windowDimensions['width'] >= containerWidth ) {
      $('bside').setStyle({
        'position':'fixed'
      });
      $('aside').setStyle({
        'position':'fixed',
        'top':'0',
        'left':'50%',
        'marginLeft':'-493px'
      });
    } else {
      $('bside').setStyle({
        'position':'absolute'
      });
      $('aside').setStyle({
        'position':'relative',
        'top':'auto',
        'left':'auto',
        'marginLeft':'0'
      });
    }		
  },
  vehicleAjaxWindow: function() {

    
  }
});

function ajaxFormData() {
    
  $('loadingOverlay').setStyle({opacity:0.7});
  $('loadingOverlay').setStyle({display:'block'});
  if ($('nextStep')) {
    $('nextStep').onclick = function() {
      return false;
    }
  }
  if ($('prevStep')) {
    $('prevStep').onclick = function() {
      return false;
    }
  }
  var params = $('quoteForm').serialize();
  
  if ($('submitButton').value == 'next') {
    eventVal = 'step_' + $('quoteForm').pageId.value + '_' + $('quoteForm').summary.value.replace(/ /g,'_');
    dataLayer.push({'event': eventVal}); // Google Tag Manager
    
    ga('send', 'event', 'Motor', 'Step', eventVal); // Event Tracking
  }
    
  new Ajax.Request($('quoteForm').action, {
    method: 'post',
    parameters: params + '&ajaxContext=true',
    onFailure: function() {
      $('quoteForm').submit();
    },
    onSuccess: function(t) {
      $('loadingOverlay').fade({duration: 0.3, from: 0.7, to: 0});
      
      var myDomObject = new Element( 'div' ,{
        "class": "ajaxed"
      }).update(t.responseText); 
      
      
      if(myDomObject.select('#isVehicleQuoteForm')[0] != null) {
         
        if(myDomObject.select('#actionResult')[0] != null && myDomObject.select('#actionResult')[0].value == 'success') {
          slideForm($('submitButton').value, myDomObject);
          if(myDomObject.select('#aside')[0] != null) {
            $('aside').update(myDomObject.select('#aside')[0].innerHTML);
          }

        } else {
          $('content').update(myDomObject.select('#content')[0].innerHTML);
          loadMethods();
        }
      
      } else {
        window.location.assign(url);
      } 
          
    }
  });
    
  return false;

}
  

function ajaxNav() {

  var selectedIndex;
  
  $$('#nav li').each(function(elm, index) {
    
    elm.index=index;
    
    if(elm.hasClassName('selected')) {
      selectedIndex = index;
    }
    
    if(!elm.hasClassName('uncompleted')) {

      elm.select('a')[0].onclick = function() {
       
        
        if(this.up('li').index > selectedIndex) {
          ajaxProcess(this.href, 'next');
        }
        
        if(this.up('li').index < selectedIndex) {
          ajaxProcess(this.href, 'back');
        }
        return false;
      }
      
    }
    
    
    
  });
  
  function ajaxProcess(url, direction) {
    
    $('loadingOverlay').setStyle({opacity:0.7});
    $('loadingOverlay').setStyle({display:'block'});
    
    new Ajax.Request(url, {
      method: 'post',
      parameters: '&ajaxContext=true',
      onSuccess: function(t) {
        $('loadingOverlay').fade({duration: 0.3, from: 0.7, to: 0});

        var myDomObject = new Element( 'div' ,{
          "class": "ajaxed"
        }).update(t.responseText); 

        if(myDomObject.select('#isVehicleQuoteForm')[0] != null) {

          if(myDomObject.select('#actionResult')[0] != null && myDomObject.select('#actionResult')[0].value == 'success') {
            slideForm(direction, myDomObject);
            
            if(myDomObject.select('#aside')[0] != null) {
              $('aside').update(myDomObject.select('#aside')[0].innerHTML);
            }
          } else {
            $('content').update(myDomObject.select('#content')[0].innerHTML);
            loadMethods();
          }
          
        } else {
          window.location.assign(url);
        }  

      }
    });
  }
  
}



function slideForm(direction, myDomObject) {
  
  Effect.ScrollTo('wrapper', { 
    duration:'0.2', 
    afterFinish: function () {
      
  
  
      if(direction == 'next'){
        $('content').setStyle({
          'width':'1088px', 
          'position':'relative'
        });
        $$('#content .step')[0].setStyle({
          'float':'left'
        });
        $('content').insert({
          bottom: myDomObject.select('#content')[0].innerHTML
        });
        $$('#content .step')[1].setStyle({
          'float':'right'
        });
    
              
        new Effect.Move($('content'), {
          x: -544,
          duration: 0.3,
          afterFinish: function() {
            $('func').update(myDomObject.select('#func')[0].innerHTML);
            $$('#content .step')[0].remove();
            $('content').setStyle({
              'width':'544px', 
              'left': ''
            });
            loadMethods();
            ajaxNav();
          }
        });
      } else {
        $$('#content .step')[0].setStyle({
          'float':'right'
        });
        $('content').setStyle({
          'position':'relative', 
          'left':'-544px'
        });
        $('content').setStyle({
          'width':'1088px'
        });
              
        $('content').insert({
          top: myDomObject.select('#content')[0].innerHTML
        });
        $$('#content .step')[0].setStyle({
          'float':'left'
        });
              
        new Effect.Move($('content'), {
          x: 544,
          duration: 0.3,
          afterFinish: function() {
                  
            $('func').update(myDomObject.select('#func')[0].innerHTML);
            $$('#content .step')[1].remove();
            $('content').setStyle({
              'width':'544px'
            });
            loadMethods();
            ajaxNav();
          }
        });
           
      }
  
    }
  });
}

function checkCardNumber() {
  var re_cardNo = /^308407\d{10}$/;

  var value = $('membershipCard1').value + $('membershipCard2').value + $('membershipCard3').value + $('membershipCard4').value;
  if (value.search(re_cardNo) == -1) {
    return false;
  } else {
    return true;
  }
}

function showInputError(fld, msg) {
  if ($(fld.id + '_wwerr') != null) {
    return;
  }

  var div1 = new Element('div', {
    'class': 'wwerr smooth_vis', 
    'id': fld.id + '_wwerr'
  });
  var div2 = new Element('div', {
    'class': 'errorMessage'
  }).update(msg);
  div1.appendChild(div2);

  var trg = $(fld.id).up('dl');
  trg.parentNode.insertBefore(div1, trg);
}

function removeInputError(fld) {
  var errElm = $(fld.id + '_wwerr')
  if (errElm) {
    var trg = $(fld.id).up('dl');
    trg.parentNode.removeChild(errElm);
  }
}

function calc() {

  if($('homeCalc')) {
    var smoothMeterSquared = new smoothField('meterSquared', {
      smoothFields: '.meterSquared, #sizeMeterSquared_wwerr',
      extraActionFields: '#squares, #dontKnowSize',
      eventType: 'click',
      fieldCase: function() {
        return $('meterSquared').checked && !$('squares').checked && !$('dontKnowSize').checked
      }
    });

    var smoothSquares = new smoothField('squares', {
      smoothFields: '.squares, #sizeSquares_wwerr',
      extraActionFields: '#meterSquared, #dontKnowSize',
      eventType: 'click',
      fieldCase: function() {
        return !$('meterSquared').checked && $('squares').checked && !$('dontKnowSize').checked
      }
    });

    var smoothdontKnowSize = new smoothField('dontKnowSize', {
      smoothFields: '.single, .double, #single_wwerr',
      extraActionFields: '#meterSquared, #squares',
      eventType: 'click',
      fieldCase: function() {
        return !$('meterSquared').checked && !$('squares').checked && $('dontKnowSize').checked
      }
    });

    var smoothSingle = new smoothField('single', {
      smoothFields: '.singleRooms, #singleRooms_wwerr',
      extraActionFields: '#double, #meterSquared, #squares, #dontKnowSize',
      eventType: 'click',
      fieldCase: function() {
        return !$('meterSquared').checked && !$('squares').checked && $('dontKnowSize').checked && $('single').checked && !$('double').checked
      }
    });

    var smoothDouble = new smoothField('double', {
      smoothFields: '.doubleRooms, #doubleRooms_wwerr',
      extraActionFields: '#single,  #meterSquared, #squares, #dontKnowSize',
      eventType: 'click',
      fieldCase: function() {
        return !$('meterSquared').checked && !$('squares').checked && $('dontKnowSize').checked && !$('single').checked && $('double').checked
      }
    });

    var smoothImpYes = new smoothField('impYes', {
      smoothFields: '.improvements, #impTotal_wwerr',
      extraActionFields: '#impNo',
      eventType: 'click',
      fieldCase: function() {
        return !$('impNo').checked && $('impYes').checked
      }
    });

    $('calculate').onclick = function() {
      calculateHome();
      return false;
    };

    $$('input[type=reset]')[0].onclick = function() {
      this.form.reset();
      updateTotal(0);
      removeErrors();
      smoothMeterSquared.action();
      smoothSquares.action();
      smoothdontKnowSize.action();
      smoothSingle.action();
      smoothDouble.action();
      smoothImpYes.action();
    };

    String.prototype.trim = function () {
      return this.replace(/^\s*/, "").replace(/\s*$/, "");
    }

    function removeErrors() {
      $$('.wwerr').each(Element.remove);
    }

    function calculateHome() {

      var sizeMeterSquared = $('sizeMeterSquared').value;
      var sizeSquares = $('sizeSquares').value;

      var singleRooms = $('singleRooms').value;
      var doubleRooms = $('doubleRooms').value;

      var construction = $('construction').value;
      var impTotal = $('impTotal').value;

      var size = 0;

      removeErrors();

      if(!$('meterSquared').checked && !$('squares').checked && !$('dontKnowSize').checked){
        showInputError($('meterSquared'), 'Please select the size of your house');
        return false;
      }

      if($('meterSquared').checked && (sizeMeterSquared.trim() == '' || isNaN(sizeMeterSquared))){
        showInputError($('sizeMeterSquared'), 'Please enter a size in m&sup2;');
        return false;
      }

      if($('squares').checked && (sizeSquares.trim() == '' || isNaN(sizeSquares))){
        showInputError($('sizeSquares'), 'Please enter a size in squares');
        return false;
      }

      if($('dontKnowSize').checked && !$('single').checked  && !$('double').checked){
        showInputError($('single'), 'Please select how many levels your building has');
        return false;
      }

      if($('dontKnowSize').checked && $('single').checked && $('singleRooms').value == ''){
        showInputError($('singleRooms'), 'Please select an option that best represents your house');
        return false;
      }

      if($('dontKnowSize').checked && $('double').checked && $('doubleRooms').value == ''){
        showInputError($('doubleRooms'), 'Please select an option that best represents your house');
        return false;
      }

      if($('construction').value == ''){
        showInputError($('construction'), 'Please select type of construction');
        return false;
      }

      if(!$('impNo').checked && !$('impYes').checked){
        showInputError($('impNo'), 'Please select whether or not you have any non-standard improvements');
        return false;
      }

      if($('impYes').checked && (impTotal.trim() == '' || isNaN(impTotal))){
        showInputError($('impTotal'), 'Please enter the total estimated value of improvements');
        return false;
      }

      if($('meterSquared').checked) {
        size = sizeMeterSquared;
      } else if($('squares').checked) {
        size = (sizeSquares * 9.3);
      } else if ($('dontKnowSize').checked && $('single').checked ) {
        size = (singleRooms * 9.3);
      } else if ($('dontKnowSize').checked && $('double').checked ) {
        size = (doubleRooms * 9.3);
      }

      if(impTotal == undefined || impTotal == '' || impTotal == null) {
        impTotal = 0;
      }

      totalValue = (size/1) * (construction/1) + (impTotal/1);

      updateTotal(totalValue.toFixed(0)); // 0 dp
      new Effect.ScrollTo('totalAmount', {
        duration:'0.4', 
        offset:-20
      });

      return false;
    }

    function updateTotal(totalValue) {
      var total = $('total');
      total.update('$' + addCommas(totalValue));
      total.setStyle({
        'color': '#5eb646'
      });
      new Effect.Morph(total, {
        style: 'color: #004990;', 
        duration: 0.6
      });
    }

  }
}

function appendTotal() {
  return '?sumInsured=' + $('total').innerHTML.replace(/[^0-9]/g, '');
}

function initPremiumOptions() {
  var frm = $('premiumOptionsForm');

  if (!frm) {
    return;
  }
  
  frm.select('input[type="checkbox"]').each(function(elm) {
    elm.onclick = function() {
      calcPremium(elm);
    }
  });

  frm.select('select').each(function(elm) {
    elm.onchange = function() {
      calcPremium(elm);
    }
  });
}

function calcPremium(fld) {
  var frm = $('premiumOptionsForm');

  // update benefits list
  if (fld.checked) {
    $$('ul.benefits')[0].addClassName(fld.name);
  } else {
    $$('ul.benefits')[0].removeClassName(fld.name);
  }

  // special case for cover30/cover50, only one may be checked
  if (fld.id == 'investorCover30' && fld.checked) {
    $('investorCover50').checked = false;
    $$('ul.benefits')[0].removeClassName('investorCover50');
  }
  if (fld.id == 'investorCover50' && fld.checked) {
    $('investorCover30').checked = false;
    $$('ul.benefits')[0].removeClassName('investorCover30');
  }

  // prepare params
  var params = frm.serialize(true);
  params['jsonContext'] = 'true';

  // replace premium values with gfx
  $$('body')[0].style.cursor = 'wait';
  $('annualPremium').innerHTML = '<img src="secure_images/loading.gif" alt="Loading..." />';
  $('monthlyPremium').innerHTML = '<img src="secure_images/loading.gif" alt="Loading..." />';

  // disable form fields
  frm.select('input', 'select').each(function(elm) {
    elm.disabled = true;
  });

  new Ajax.Request(PREMIUM_OPTIONS_URL, {
    method: 'post',
    parameters: params,

    onSuccess: function(transport){
      var response = transport.responseText;
      var responseJson = response.evalJSON(true);

      try {
        var annualPremium = responseJson.homePremium[0].annual;
        var monthlyPremium = responseJson.homePremium[0].monthly;
      } catch (e) {}

      // update premium values
      $$('body')[0].style.cursor = 'default';
      $('annualPremium').innerHTML = '$' + annualPremium;
      $('monthlyPremium').innerHTML = '$' + monthlyPremium;

      // re-enable form fields
      frm.select('input', 'select').each(function(elm) {
        elm.disabled = false;
      });

    },
    onFailure: function() {
      // re-enable form fields
      frm.select('input', 'select').each(function(elm) {
        elm.disabled = false;
      });
    }
  });
}


function checkSecurityLock() {
  return ($('securityLock').value == VALUE_SECURITY_OTHER);
}

function checkBuildingType() {
  return ($('buildingType').value == VALUE_BUILDING_TYPE_CONJOINED);
}

/**
 * Dynamically add a Specified Contents row.
 */
function addSCRow(elm) {
  var clicked = $(elm);
  /*
  if (elm.value == '') return;
  
  // check there no empty value fields before adding another
  var empty = 0;
  $$('.value input').each(function(elm) {
    if (elm.value == '') {empty++;}
  });
  if (empty > 0) return;*/

  var table = $$('table.specifiedContents')[0];
  // unfix height prior to adding additional row
  table.style.height = 'auto';

  var trHeight = 30;

  var tr = new Element("tr");
  tr.setStyle({
    opacity: 0, 
    height: '0px'
  });
  var td1 = new Element('td', {
    className: ""
  });
  var td2 = new Element('td', {
    className: ""
  });
  var td3 = new Element('td', {
    className: "value"
  });
  var td4 = new Element('td', {
    className: ""
  });

  var count = $('scCount').value; 


  var in1 = new Element('select', {
    name: "type" + count
  });
  // copy option list
  for (var i=0; i<$('masterSCOptions').options.length; i++) {
    in1.insert(new Element('option', {
      value: $('masterSCOptions').options[i].value
    }).update($('masterSCOptions').options[i].text));
  }

  var in2 = new Element('input', {
    name: "description" + count, 
    type: 'text'
  });
  var in3 = new Element('input', {
    name: "sumInsured" + count, 
    id: "sumInsured" + count, 
    type: 'text'
  });
  var in4 = new Element('span', {
    className: "btn btn_add"
  } ).update('<img width="38" height="76" src="./secure_images/insurance/home/btn_add.png" alt="Add" title="Add">');

  

  in4.onclick = function() {
    addSCRow(this);
  }
  in3.onkeypress = function(e) {
    return isNumberKey(e);
  }

  td1.appendChild(in1);
  td2.appendChild(in2);
  td3.appendChild(in3);
  td4.appendChild(in4);

  tr.appendChild(td1);
  tr.appendChild(td2);
  tr.appendChild(td3);
  tr.appendChild(td4);

  elm.up('tr').insert({
    after: tr
  });

  count++;

  $('scCount').value = count;


  elm.effect = new Effect.Morph(tr, {
    style: 'height: ' + trHeight + 'px;',
    duration: 0.3,
    afterFinish: function() {

      new Effect.Fade(tr, {
        from: 0,
        to: 1,
        duration: 0.4
            
      })

      new Effect.Fade(clicked, {
        from: 1,
        to: 0,
        duration: 0.2
      })
    }
  })

  
}

/**
 * Returns the value of the selected radio button in the radio group, null if
 * none are selected, and false if the button group doesn't exist
 *
 * @param {radio Object} or {radio id} el
 * OR
 * @param {form Object} or {form id} el
 * @param {radio group name} radioGroup
 */
function $RF(el, radioGroup) {
  if($(el).type && $(el).type.toLowerCase() == 'radio') {
    var radioGroup = $(el).name;
    var el = $(el).form;
  } else if ($(el).tagName.toLowerCase() != 'form') {
    return false;
  }

  var checked = $(el).getInputs('radio', radioGroup).find(
  function(re) {
    return re.checked;
  }
);
  return (checked) ? $F(checked) : null;
}

function initPostcodeTrigger() {
  if (!$('residentialAddressForm')) return;
  
  $('residentialPostcode').onkeyup = function() {
    // reset street/suburb selectors
    $('residentialSuburb').value = '';
    $('residentialStreet').value = '';
    if ($('residentialStreetNumber')) {
      $('residentialStreetNumber').value = '';
    }
    if ($('streetNumber')) {
      $('streetNumber').value = '';
    }

    updateStreetSuburbs($('residentialSuburb'), STREET_SUBURB_URL, 'residential', WIDGET_FILTER);
  }

  if ($('poBoxPostcode')) {
    $('poBoxPostcode').onkeyup = function() {
      // reset street/suburb selectors
      $('poBoxSuburb').value = '';
      $('poBoxNumber').value = '';
	
      updateStreetSuburbs($('poBoxSuburb'), STREET_SUBURB_URL, 'poBox', WIDGET_FILTER);
    }
  }
}


function initQAS() {
  if ($('residentialAddressQuery')) {
    $('residentialAddressHolder').hide();
    $('residential-ldg').hide(); // loading
    $('residentialAddressQuery').onkeyup = function(e) {       
      clearTimeout(qasTimer);
      if (!isDirectionKey(e)) {
        qasTimer = setTimeout(function() { queryQas(ADDRESS_RESIDENTIAL); }, 500);              
      }
    } // search
  }
  if ($('postalAddressQuery')) {
    $('postalAddressHolder').hide();
    $('postal-ldg').hide(); // loading
    $('postalAddressQuery').onkeyup = function(e) { 
      clearTimeout(qasTimer);
      if (!isDirectionKey(e)) {
        qasTimer = setTimeout(function() { queryQas(ADDRESS_POSTAL); }, 500);
      }
    } // search
  }
}

function queryQas(addressType) {
  // clear stored values
  clearAddress();

  var queryFld = $(addressType + 'AddressQuery');
  var frm = queryFld.up('form');
  var list = $(addressType + 'AddressList');
  var listHolder = $(addressType + 'AddressHolder');
    
  list.descendants().each(Element.remove);
  
  list.onclick = function() {
    queryFld.value = list.options[list.selectedIndex].innerHTML; // update query field
  }

  if ($(addressType + 'AddressQuery').value.length <= 3) return;

  var params = frm.serialize(true);
  params['jsonContext'] = 'true';
  params['addressType'] = addressType;
  params['addressQuery'] = $(addressType + 'AddressQuery').value;
  
  $(addressType + '-ldg').show();
  
  new Ajax.Request(QAS_URL, {
    method: 'post',
    parameters: params,

    onSuccess: function(transport){
      var response = transport.responseText;
      var responseJson = response.evalJSON(true);

      list.descendants().each(Element.remove);
      
      try {
        for (var i = 0; i<responseJson.qaAddress.length; i++) {
          var addressText = responseJson.qaAddress[i].partial;
          var moniker = responseJson.qaAddress[i].moniker;
          
          if (addressType == 'residential') {
            list.insert(new Element('option', {value: moniker}).update(addressText));
          } else if (addressType = 'postal') {
            list.insert(new Element('option', {value: addressText}).update(addressText));
          }
        }
        
        if (list.options.length == 1) {
          list.options.selectedIndex = 0; // pre-select
          //queryFld.value = list.options[list.selectedIndex].innerHTML; // update query field
        }
                  
      } catch (e) {}
            
      if (list.options.length == 0) {
        //listHolder.hide();
        
        /*
        if ($('wwerr_' + addressType + 'AddressQuery')) { 
          $('wwerr_' + addressType + 'AddressQuery').remove();
        }
        $('wwgrp_' + addressType + 'AddressQuery').insert({
          before: '<div class="wwerr" id="wwerr_' + addressType + 'AddressQuery"><div class="errorMessage" errorfor="' + addressType + 'AddressQuery">No address found, please contact us on 13 27 22. RACT Insurance only offers cover for risks based in Tasmania.</div></div>' 
        });
        */

      } else {
        if ($('wwerr_' + addressType + 'AddressQuery')) { 
          $('wwerr_' + addressType + 'AddressQuery').remove();
        }
        listHolder.show();
        
        // recalculate container height 
        if (listHolder.up().getHeight() < listHolder.getHeight()) {
          listHolder.up().style.height = listHolder.up().getHeight() + listHolder.getHeight() + 'px';
        }
        
      }
      
      $(addressType + '-ldg').hide();
    },
    onFailure: function() {
      list.descendants().each(Element.remove);
      listHolder.hide();
      
      if ($('wwerr_' + addressType + 'AddressQuery')) { 
        $('wwerr_' + addressType + 'AddressQuery').remove();
      }
      $('wwgrp_' + addressType + 'AddressQuery').insert({
        before: '<div class="wwerr" id="wwerr_' + addressType + 'AddressQuery"><div class="errorMessage" errorfor="' + addressType + 'AddressQuery">Please try again.</div></div>' 
      });
      
      $(addressType + '-ldg').hide();
    }
  });
  
  verifiedQas = true;  
}

function clearAddress() {
  try {
    $('residentialStreetNumber').value = '';
    $('residentialStreet').value = '';
    $('residentialSuburb').value = '';
    $('residentialPostcode').value = '';
    $('residentialLongitude').value = '';
    $('residentialLatitude').value = '';
    $('residentialGnaf').value = '';  
  } catch (e) {}
  
  try {
    $('postalStreetNumber').value = '';
    $('postalStreet').value = '';
    $('postalSuburb').value = '';
    $('postalPostcode').value = '';
    $('postalDpid').value = '';
    $('postalAusbar').value = '';
  } catch (e) {}
}

function initBSB() {
  if (!$('HomePaymentOptions_bsb1')) return;

  $('HomePaymentOptions_bsb1').onkeyup = function() {
    if ($('HomePaymentOptions_bsb1').value.length == 3) {
      $('HomePaymentOptions_bsb2').focus();
    }
  }
}
  
function notes() {
  $$('.notes').each(function(elm) {
    elm.on('mouseenter', function() {
      elm.previous().addClassName('hover');
    })
        
    elm.on('mouseout', function() {
      elm.previous().removeClassName('hover');
    })
  });

} 

function navDisable() {
  $$('#nav .uncompleted').each(function(elm) {
    elm.onclick = function() {
      return false;
    }
  });
}