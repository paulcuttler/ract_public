var re_int = /\d/;
var re_cost = /\d|\$|,|\./;
var re_date = /\d|\//;
var re_abc = /[A-Za-z \-\']/;
var re_email = /^[A-Za-z0-9_\-\.\&\']+@[A-Za-z0-9_-]+\.[A-Za-z0-9_-]+.*$/;
var re_bankAccount = /\d|-|\s/;
var re_ws = /\s/;

/*
 * whether or not to allow Enter to be accepted within form fields
 * - this is disabled for Vehicle Quoting
 */
var ALLOW_ENTER_AS_CONTROL_KEY = false;

function refreshCapchar(targetUrl) {
  var img = document.getElementById("captchaImage");
  img.src = targetUrl + "?sid="+Math.random();
}

function isControlKey(charCode) {
  // backspace, tab, end, home, left arrow, right arrow, delete
  if (charCode == 8 || charCode == 9 || charCode == 35 || charCode == 36 || charCode == 37 || charCode == 39 || charCode == 46) {
    return true;
  } else if (ALLOW_ENTER_AS_CONTROL_KEY && charCode == 13) {
    return true;
  }

  return false;
}

function isDirectionKey(evt) {
  if (!evt) var evt = window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  // end, home, left arrow, right arrow
  if (charCode == 35 || charCode == 36 || charCode == 37 || charCode == 39) {
    return true;
  }

  return false;
}

function isSubmitKey(evt) {
  if (!evt) var evt = window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode == 13) {
    return true;
  }

  return false;
}

function isBankAccountKey(evt) {
  if (!evt) var evt = window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  var c = String.fromCharCode(charCode);

  if (c.search(re_bankAccount) != -1 || isControlKey(charCode)) {
    return true;
  }

  return false;
}

function isNumberKey(evt) {
  if (!evt) var evt = window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  var c = String.fromCharCode(charCode);

  if(c == '.') {
    return false;
  }

  if (c.search(re_int) != -1 || isControlKey(charCode)) {
    return true;
  }

  return false;
}

function isDateKey(evt) {
  if (!evt) var evt = window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  var c = String.fromCharCode(charCode);

  if (c.search(re_date) != -1 || isControlKey(charCode)) {
    return true;
  }

  return false;
}

function isAlphaKey(evt) {
  if (!evt) var evt = window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  var c = String.fromCharCode(charCode);

  if (c.search(re_abc) != -1 || isControlKey(charCode)) {
    return true;
  }

  return false;
}

function isCostKey(evt) {
  if (!evt) var evt = window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  var c = String.fromCharCode(charCode);

  if (c.search(re_cost) != -1 || isControlKey(charCode)) {
    return true;
  }

  return false;
}

function addCommas(nStg) {
	nStg += '';
	i = nStg.split('.');
	j = i[0];
	k = i.length > 1 ? '.' + i[1] : '';
	var regExp = /(\d+)(\d{3})/;
	while (regExp.test(j)) {
		j = j.replace(regExp, '$1' + ',' + '$2');
	}
	return j + k;
}

function printWindow(href) {
  var w = window.open(href, 'myWin', '', 'location=no,menubar=no,toolbar=no,status=no');
  return w;
}

function printVehicle() {
  //var w = printWindow('LoadQuotePage.action?pageId=10&print=true');
  //w.print();
  window.print();
  return false;
}

function printHome() {
  //var w = printWindow(location.href + '?print=true');
  //w.print();
  window.print();
  return false;
}