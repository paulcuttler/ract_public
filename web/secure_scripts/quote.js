/* Constant Definition */
var COMPREHENSIVE_PREMIUM = 1;
var THIRD_PARTY_PREMIUM = 2;
var SELECT = 1;
var CHECKBOX = 2;
var MONTHLY_PREMIUM;
var ANNUAL_PREMIUM;
var AJAX_IN_PROGRESS = 0;


function validEmail(fld, reqd) {
  if (reqd || fld.value != '') {
    if ((fld.value.search(re_email) == -1) || (fld.value.search(re_ws) != -1)) {
      if (fld.className.indexOf('invalid') == -1) {
        fld.className += ' invalid';
      }
      return;
    }
  }

  fld.className = fld.className.replace(/ invalid/, '');
}

function sendEmail(targetUrl, emailAddressElementId) {
  var emailAddress = document.getElementById(emailAddressElementId)

  var paramString = "";
  paramString += "emailAddress=" + emailAddress;

  var handlerFunction = getReadyStateHandler(req, function(xml) {
    _updateEmail(xml);
  });
  req.onreadystatechange = handlerFunction;
  req.open("POST", targetUrl, true);
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  req.send(paramString);
}

function _updateEmail(xml) {
  var messageElement = document.getElementById("ajaxMessage");
  var resultList = eval('(' + resultXml + ')'); // convert to JSON

  if (resultList.validation == 'true') {
    messageElement.innerHTML = '<p> Email has been successfuly sent.</p>';
  } else {
    messageElement.innerHTML = '<p> Sending Email failed.</p>';
  }
}

function submitQuoteForm(formId, submitId, button) {
  if (!AJAX_IN_PROGRESS) {
    if (button == 'back') {
      document.getElementById('submitButton').value = 'back';
    } else {
      document.getElementById('submitButton').value = 'next';
    }
    //document.getElementById('quoteForm').submit();
    ajaxFormData();
    return false;
  } else {
    alert('Calculation in progress, please wait.');
  }
}

function showQuestions(elementIds, show) {
  var elementIdArray = elementIds.split(",");
 
  for (i=0; i < elementIdArray.length; i++) {
    var element = document.getElementById(elementIdArray[i]);
    if (show == 'show') {
      element.className = "qHolder adjust-block";
    } else {
      element.className = "qHolder adjust-none";
    }
  }
}

function showMember(elementIds, show) {
  var messageElement = document.getElementById("ajaxMessage");

  messageElement.innerHTML = "";
  showQuestions(elementIds, show);
}

function getStreetList(targetUrl, streetElementId, pageId, questionId, subQuestionId, suburbElementId, postCodeElementId) {
  var postcode = document.getElementById(postCodeElementId).value;
  var suburbElement = document.getElementById(suburbElementId);
  var suburb = suburbElement.options[suburbElement.selectedIndex].value;
  
  var req = newXMLHttpRequest();

  var paramString = "";

  paramString += "postCode=" + postcode;
  paramString += "&pageId=" + pageId;
  paramString += "&questionId=" + questionId;
  paramString += "&subQuestionId=" + subQuestionId;
  paramString += "&suburb=" + suburb;

  pleaseWaitSelect(document.getElementById(streetElementId));

  var handlerFunction = getReadyStateHandler(req, function(xml) {
    _updateStreet(xml, streetElementId);
  });
  req.onreadystatechange = handlerFunction;
  req.open("POST", targetUrl, true);
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  req.send(paramString);

//showQuestions(elementIds, show);
}

function _updateStreet(resultXml, streetElementId) {
  var messageElement = document.getElementById("ajaxMessage");
  var streetElement = document.getElementById(streetElementId);
  var message = '';
  var resultList = eval('(' + resultXml + ')'); // convert to JSON

  if (resultList.validation == 'true') {
    var length = streetElement.options.length

    for (i = length - 1; i > 0; i--) {
      streetElement.remove(i);
    }

    for (i = 0; i < resultList.items.length; i++) {
      newOption = new Option(resultList.items[i]);
      newOption.value = resultList.values[i];
      streetElement.options[streetElement.options.length] = newOption;
    }

    pleaseChooseSelect(document.getElementById(streetElementId));

  } else {
    message = resultList.message;
    if (message != null) {
      messageElement.innerHTML = '<p>' + message + '</p>';
    }
  }

}

function getSuburbList(targetUrl, postCodeElementName, pageId, questionId, subQuestionId, suburbElementId, allowTasmaniaAddress) {
  var postCodeElement = document.getElementById(postCodeElementName);
  var postcode = postCodeElement.value;

  if (postcode.length != 4) {
    return;  
  }

  var req = newXMLHttpRequest();

  var paramString = "";

  paramString += "postCode=" + postcode;
  paramString += "&pageId=" + pageId;
  paramString += "&questionId=" + questionId;
  paramString += "&subQuestionId=" + subQuestionId;
  
  if (allowTasmaniaAddress != null) {
    paramString += "&allowTasmaniaAddress=" + allowTasmaniaAddress;
  }

  pleaseWaitSelect(document.getElementById(suburbElementId));

  var handlerFunction = getReadyStateHandler(req, function(xml) {
    _updateSuburb(xml, suburbElementId);
  });
  req.onreadystatechange = handlerFunction;
  req.open("POST", targetUrl, true);
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  req.send(paramString);
}

function _updateSuburb(resultXml, suburbElementId) {
  var messageElement = document.getElementById("ajaxMessage");
  var suburbElement = document.getElementById(suburbElementId);
  var message = '';
 
  var resultList = eval('(' + resultXml + ')'); // convert to JSON
  if (resultList.validation == 'true') {
    var length = suburbElement.options.length

    for (i = length - 1; i > 0; i--) {
      suburbElement.remove(i);
    }

    for (i = 0; i < resultList.items.length; i++) {
      newOption = new Option(resultList.items[i]);
      newOption.value = resultList.values[i];
      suburbElement.options[suburbElement.options.length] = newOption;
    }

    pleaseChooseSelect(document.getElementById(suburbElementId));
    messageElement.innerHTML = '';
  } else {
    message = resultList.message;
    if (message != null) {
      messageElement.innerHTML = '<p>' + message + '</p>';
    }
  }
}

function verifyMemberCard(targetUrl, elementIds, pageId, questionId, subQuestionId, memberElementName) {
  var elementIdArray = elementIds.split(",");
  var dobElement = document.getElementById(elementIdArray[1]);
  var dob = dobElement.value ;
  var startDateElement = document.getElementById(elementIdArray[2]);
  var startDate = '';

  for (i=0; i<startDateElement.length; i++) {
    if (startDateElement.options[i].selected) {
      startDate = startDateElement.options[i].value;
    }
  }

  var memberCardNo = '';
  var memberCardNo1 = document.getElementById(elementIdArray[0] + '1').value;
  var memberCardNo2 = document.getElementById(elementIdArray[0] + '2').value;
  var memberCardNo3 = document.getElementById(elementIdArray[0] + '3').value;
  var memberCardNo4 = document.getElementById(elementIdArray[0] + '4').value;

  memberCardNo = memberCardNo1 + ',' + memberCardNo2 + ',' + memberCardNo3 + ',' + memberCardNo4;

  var req = newXMLHttpRequest();

  var paramString = "";

  paramString += "dob=" + dob;
  paramString += "&memberCardNo=" + memberCardNo;
  paramString += "&startDate=" + startDate;
  paramString += "&pageId=" + pageId;
  paramString += "&questionId=" + questionId;
  paramString += "&subQuestionId=" + subQuestionId;

  var handlerFunction = getReadyStateHandler(req, function(xml) {
    _updateMemberCardStatus(xml, memberElementName);
  });
  req.onreadystatechange = handlerFunction;
  req.open("POST", targetUrl, true);
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  req.send(paramString);
}

function _updateMemberCardStatus(resultXml, memberElementName) {


  var messageElement = document.getElementById("ajaxMessage");
  var memberElements = document.getElementsByName(memberElementName);

  var message = '';
  var resultList = eval('(' + resultXml + ')'); // convert to JSON
  message = resultList.message;
  messageElement.innerHTML = '<p>' + message + '</p>';

  if (resultList.validation == 'true') {
    showQuestions('sub1,sub2','show');
    
    for (i = 0; i < memberElements.length; i++) {
      if (memberElements[i].value == "yes") {
        memberElements[i].checked = true;
      } else {
        memberElements[i].checked = false;
      }
    }
  } else {
    showQuestions('sub1,sub2','hidden');

    for (i = 0; i < memberElements.length; i++) {
      if (memberElements[i].value == "no") {
        memberElements[i].checked = true;
      } else {
        memberElements[i].checked = false;
      }
    }
  } 
}

function getPremiumType() {
  var premiumElements = document.getElementsByName("coverType");

  for (i = 0; i < premiumElements.length; i++) {
    if(premiumElements[i].checked == true) {
      if (premiumElements[i].value == 'comprehensive') {
        return 'comp';
      } else if (premiumElements[i].value == 'thirdPartyDamage') {
        return 'tp';
      }
    }
  }
  
  return 0;
}

function showInsurance(type) {
  var premiumElements = document.getElementsByName("premiumType");

  if(type == 'comp') {

    for (i = 0; i < premiumElements.length; i++) {
      if (premiumElements[i].value == 'comp') {
        premiumElements[i].checked = true;
      }
    }

  } else {

    for (i = 0; i < premiumElements.length; i++) {
      if (premiumElements[i].value == 'tp') {
        premiumElements[i].checked = true;
      }
    }
  }

  getPremium('', '');
}

function getPremium(element, htmlType) {
  var premiumType = getPremiumType();
  var excess = '';
  var compOption = '';
  var hireCar = '';
  var windScreen = '';
  var tpOption = '';

  AJAX_IN_PROGRESS = 1;

  document.getElementById("compPremium").innerHTML = '<img src="secure_images/loading.gif" alt="Loading..." />';
  document.getElementById("tpPremium").innerHTML = '<img src="secure_images/loading.gif" alt="Loading..." />';

  // disable form fields
  var formElms = $('quoteForm').select('input');
  for (i=0; i<formElms.length; i++) {
    formElms[i].disabled = true;
  }
  formElms = $('quoteForm').select('select');
  for (i=0; i<formElms.length; i++) {
    formElms[i].disabled = true;
  }
  $('quoteForm').select('div.actions')[0].style.display = 'none';

  //if (htmlType == SELECT) {
  var selectElement = document.getElementById("excess");

  for (i=0; i<selectElement.length; i++) {
    if (selectElement.options[i].selected) {
      excess = selectElement.options[i].value;
    }
  }
  //} else if (htmlType == CHECKBOX) {
  var checkboxElements = document.getElementsByName("compOption");

  for (i=0; i<checkboxElements.length; i++) {
    if (checkboxElements[i].checked) {
      compOption = checkboxElements[i].value;

      if(compOption == 'hireCar') {
        hireCar = 'yes';
      }

      if(compOption == 'windScreen') {
        windScreen = 'yes';
      }
    }
  }

  checkboxElements = document.getElementsByName("tpOption");
  for (i=0; i<checkboxElements.length; i++) {
    if (checkboxElements[i].checked) {
      tpOption = checkboxElements[i].value;
    }
  }

  //}

  var req = newXMLHttpRequest();

  var paramString = "";

  paramString += "hireCar=" + hireCar;
  paramString += "&windScreen=" + windScreen;
  paramString += "&excess=" + excess;
  paramString += "&tpOption=" + tpOption;
  paramString += "&premiumType=" + premiumType;
  paramString += "&pageId=" + 9;
  paramString += "&questionId=" + 15;
  paramString += "&subQuestionId=" + "";

  var handlerFunction = getReadyStateHandler(req, function(xml) {
    _updatePremium(xml, premiumType);
  });
  req.onreadystatechange = handlerFunction;
  req.open("POST", "QuoteAjaxAction_premium.action", true);
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  req.send(paramString);
}

function _updatePremium(resultXml, premiumType) {
  var resultList = eval('(' + resultXml + ')'); // convert to JSON

  if (resultList.message != '0') {
    var messageArray = resultList.message.split("|");
    if (premiumType.indexOf('comp') != -1) {
      //document.getElementById("compPremium").innerHTML = '<span class="premiumAmount">$' + messageArray[0] + '</span> Per Year OR <span class="premiumAmount">$' + messageArray[1] + '</span> Per Month';
      document.getElementById("compPremium").innerHTML = '<dd class="amounts"><div class="year"><span class="amount" id="annualPremium">$' + messageArray[0] + '</span> <span class="inc">Per Year</span></div> <div class="month"><span class="amount" id="monthlyPremium">$' + messageArray[1] + '</span> <span class="inc">Per Month</span></div><span class="or">or</span></dd>';
    } else if (premiumType.indexOf('tp') != -1) {
      //document.getElementById("tpPremium").innerHTML = '<span class="premiumAmount">$' + messageArray[0] + '</span> Per Year OR <span class="premiumAmount">$' + messageArray[1] + '</span> Per Month';
      document.getElementById("tpPremium").innerHTML = '<dd class="amounts"><div class="year"><span class="amount" id="annualPremium">$' + messageArray[0] + '</span> <span class="inc">Per Year</span></div> <div class="month"><span class="amount" id="monthlyPremium">$' + messageArray[1] + '</span> <span class="inc">Per Month</span></div><span class="or">or</span></dd>';
    }
  }

  // re-enable form fields
  var formElms = $('quoteForm').select('input');
  for (i=0; i<formElms.length; i++) {
    formElms[i].disabled = false;
  }
  formElms = $('quoteForm').select('select');
  for (i=0; i<formElms.length; i++) {
    formElms[i].disabled = false;
  }

  $('quoteForm').select('div.actions')[0].style.display = 'block';

  AJAX_IN_PROGRESS = 0;
}

function checkCreditOrBankCard(elementId, creditCardElementIds, bankAccountElementIds, alwaysShowElementIds) {
  var element = document.getElementById(elementId);
  var show;

  for (i = 0; i < element.options.length; i++) {
    if (element.options[i].selected && element.options[i].value == 'debit') {
      show = 'debit';
    } else {
      show = 'creditCard';
    }
  }

  if (show == 'creditCard') {
    showQuestions(creditCardElementIds, 'show');
    showQuestions(bankAccountElementIds, 'hidden');
  } else {
    showQuestions(bankAccountElementIds, 'show');
    showQuestions(creditCardElementIds, 'hidden');
  }

  showQuestions(alwaysShowElementIds, 'show');
}

function addNewDriver(newPolicy) {
  var newPolicyHolderDiv = document.getElementById("newPolicyHolderDiv");
  var newDriverSize = document.getElementById("driverSize").value + 1;
  var newDriverHtml = "";

  newDriverHtml = "<div>";
  newDriverHtml = newDriverHtml + '<div class="qHolder"><div class="wwgrp"><div class="wwlbl"><label class="label">First Name:</label></div><br /><div class="wwctrl"><input type="text" name="firstName'+newDriverSize+'" value="" class="text" /></div></div><div>';
  newDriverHtml = newDriverHtml + '<div class="qHolder"><div class="wwgrp"><div class="wwlbl"><label class="label">Surname:</label></div><br /><div class="wwctrl"><input type="text" name="surname'+newDriverSize+'" value="" class="text"></div></div></div>';
  newDriverHtml = newDriverHtml + '<div class="qHolder"><div class="wwgrp"><div class="wwlbl"><label class="label">Year Commenced Driving:</label></div><br /><div class="wwctrl"><input type="text" name=yearCommencedDriving"'+newDriverSize+'" value="" class="text" /></div></div></div>';
  newDriverHtml = newDriverHtml + '<div class="qHolder"><div class="wwgrp"><div class="wwlbl"><label class="label">Gender:</label></div><br /><div class="wwctrl"><select name=gender"'+newDriverSize+'"><option value="male">Male</option><option value="female">Femail</option></select></div></div></div>';

  newDriverHtml = newDriverHtml + '<div class="qHolder"><label class="radioLabel">This person will be a policy holder?</label>';
  newDriverHtml = newDriverHtml + '<div class="radioMulti"><div class="wwgrp"><div class="wwctrl"><input type="radio" name="policyHolder'+newDriverSize+'" value="yes" class="radio" onclick="showQuestions(\'polciyHolderDetails'+newDriverSize+'\',\'show\')><label>Yes</label></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwctrl"><input type="radio" name="policyHolder'+newDriverSize+'" value="no" class="radio"><label>Yes</label></div></div></div>';
  newDriverHtml = newDriverHtml + '<div id="polciyHolderDetails'+newDriverSize+'">';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Title:</label></div><br /><div class="wwctrl"><input type="text" name="title'+newDriverSize+'" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Home Phone Number:</label></div><br /><div class="wwctrl"><input type="text" name="homePhoneNumber'+newDriverSize+'" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Work Phone Number:</label></div><br /><div class="wwctrl"><input type="text" name="workPhoneNumber'+newDriverSize+'" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Mobile Phone Number:</label></div><br /><div class="wwctrl"><input type="text" name="mobilePhoneNumber'+newDriverSize+'" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Post Address:</label></div><br /><div class="wwctrl"><input type="text" name="postAddress'+newDriverSize+'" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Email Address:</label></div><br /><div class="wwctrl"><input type="text" name="emailAddress'+newDriverSize+'" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '</div>';

  newDriverHtml = newDriverHtml + '<label class="radioLabel">In the last 5 years has this person had their license suspended, cancelled or restricted</label>';
  newDriverHtml = newDriverHtml + '<div class="radioMulti"><div class="wwgrp"><div class="wwctrl"><input type="radio" name="suspended'+newDriverSize+'" value="yes" class="radio" /><label>Yes</label></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwctrl"><input type="radio" name="suspended'+newDriverSize+'" value="no" class="radio" /><label>No</label></div></div></div>';
  newDriverHtml = newDriverHtml + 'Please list all accidents and thefts in the last 5 years:';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Month/Year:</label></div><br /><div class="wwctrl"><input type="text" name="monthYear'+newDriverSize+'1" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Accident Type:</label></div> <br /><div class="wwctrl"><select name="accidentType"'+ newDriverSize + '1>';
  newDriverHtml = newDriverHtml + '<option value="af">Accident Type</option>';
  newDriverHtml = newDriverHtml + '<option value="af">At fault accident or claim</option>';
  newDriverHtml = newDriverHtml + '<option value="naf">NAF accident or claim</option>';
  newDriverHtml = newDriverHtml + '</select></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Month/Year:</label></div><br /><div class="wwctrl"><input type="text" name="monthYear'+newDriverSize+'1" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Accident Type:</label></div> <br /><div class="wwctrl"><select name="accidentType"'+ newDriverSize + '1>';
  newDriverHtml = newDriverHtml + '<option value="af">Accident Type</option>';
  newDriverHtml = newDriverHtml + '<option value="af">At fault accident or claim</option>';
  newDriverHtml = newDriverHtml + '<option value="naf">NAF accident or claim</option>';
  newDriverHtml = newDriverHtml + '</select></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Month/Year:</label></div><br /><div class="wwctrl"><input type="text" name="monthYear'+newDriverSize+'1" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Accident Type:</label></div> <br /><div class="wwctrl"><select name="accidentType"'+ newDriverSize + '1>';
  newDriverHtml = newDriverHtml + '<option value="af">Accident Type</option>';
  newDriverHtml = newDriverHtml + '<option value="af">At fault accident or claim</option>';
  newDriverHtml = newDriverHtml + '<option value="naf">NAF accident or claim</option>';
  newDriverHtml = newDriverHtml + '</select></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Month/Year:</label></div><br /><div class="wwctrl"><input type="text" name="monthYear'+newDriverSize+'1" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Accident Type:</label></div> <br /><div class="wwctrl"><select name="accidentType"'+ newDriverSize + '1>';
  newDriverHtml = newDriverHtml + '<option value="af">Accident Type</option>';
  newDriverHtml = newDriverHtml + '<option value="af">At fault accident or claim</option>';
  newDriverHtml = newDriverHtml + '<option value="naf">NAF accident or claim</option>';
  newDriverHtml = newDriverHtml + '</select></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Month/Year:</label></div><br /><div class="wwctrl"><input type="text" name="monthYear'+newDriverSize+'1" value="" class="text" /></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwlbl"><label class="label">Accident Type:</label></div> <br /><div class="wwctrl"><select name="accidentType"'+ newDriverSize + '1>';
  newDriverHtml = newDriverHtml + '<option value="af">Accident Type</option>';
  newDriverHtml = newDriverHtml + '<option value="af">At fault accident or claim</option>';
  newDriverHtml = newDriverHtml + '<option value="naf">NAF accident or claim</option>';
  newDriverHtml = newDriverHtml + '</select></div></div>';
  newDriverHtml = newDriverHtml + '<label class="radioLabel">Has this person previously been refused insurance or had insurance cancelled?</label>';
  newDriverHtml = newDriverHtml + '<div class="radioMulti"><div class="wwgrp"><div class="wwctrl"><input type="radio" name="insuranceCancelled'+newDriverSize+'" value="yes" /><label>Yes</label></div></div>';
  newDriverHtml = newDriverHtml + '<div class="wwgrp"><div class="wwctrl"><input type="radio" name="insuranceCancelled'+newDriverSize+'" value="no" /><label>No</label></div></div></div>';
  newDriverHtml = newDriverHtml + "</div>";

  newPolicyHolderDiv.innerHTML = newPolicyHolderDiv.innerHTML + newDriverHtml;
  document.getElementById("driverSize").value = newDriverSize + 1;
}

function showAccidentsInthreeYearQuestion(action, id, ncbElement, elementIds, show, yearObtainedLicenseElementId, numberOfAccidentElementId, noClaimBonusId, allow) {
  var idElements = document.getElementById(id);
  var yearObtainedLicense = document.getElementById(yearObtainedLicenseElementId).value;
  var numberOfAccident = document.getElementById(numberOfAccidentElementId).value;

  var messageElement = document.getElementById("ajaxMessage").innerHTML = "&nbsp;";
  var run = true;

  if (numberOfAccident != '') {
    if (yearObtainedLicense.length == 4) {
      if (idElements.value == '2') {
        //if($('q13sub4').hasClassName('smooth_inv')) {
        //  AccidentsInthreeYears.toggleFieldEffects();
        //}
        if (allow != 'true') {
          run = false;
        }
      } else {
        //if($('q13sub4').hasClassName('smooth_vis')) {
        //  AccidentsInthreeYears.toggleFieldEffects();
        //}
      }

      if (run) {
        
        var req = newXMLHttpRequest();

        var paramString = "";

        paramString += "yearObtainedLicense=" + yearObtainedLicense;
        paramString += "&numberOfAccident=" + numberOfAccident;

        var handlerFunction = getReadyStateHandler(req, function(xml) {
          _showNCBElement(xml, ncbElement, show, noClaimBonusId);
        });
        req.onreadystatechange = handlerFunction;
        req.open("POST", action, true);
        req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        req.send(paramString);
      }
    }
  }
}

function _showNCBElement(resultXml, elementIds, show, noClaimBonusId) {
  var messageElement;
  var resultList = eval('(' + resultXml + ')');
  messageElement = document.getElementById("ajaxMessage");
  messageElement.style.display = 'block';
  if (resultList.validation == 'false') {
    
    var ncb = 0;

    if ( resultList.message == "" || resultList.message.length <= "2") {

      if (resultList.message != "") {
        ncb = resultList.message;
      }

      messageElement.innerHTML = '<p>' + 'You are eligible for a ' + ncb + '% No Claim Bonus.'  + '</p>';
    } else {
      messageElement.innerHTML = '<p>' + resultList.message + '</p>';
    }
  } else {
    messageElement.innerHTML = '<p>' + 'You are eligible for a ' + resultList.message + '% No Claim Bonus. This will be automatically applied to your quote.' + '</p>';
  }
}

function showDirectDebit(yearlyElementIds, monthlyElementIds, paymentType) {
  if (paymentType == 'yearly') {
    showQuestions(yearlyElementIds, 'show');
    showQuestions(monthlyElementIds, 'hidden');

    document.getElementById("payByDirectDebityes1yes").checked = false;
    document.getElementById("payByDirectDebitno2no").checked = false;

  } else {
    showQuestions(yearlyElementIds, 'hidden');
    showQuestions(monthlyElementIds, 'show');
  }

  document.getElementById("accountType").selectedIndex = 0;

}

function landingSwitch(cls) {
  document.getElementById('quoteLanding').className = cls;
  return false;
}

function backButtonOverride() {
  setTimeout("backButtonOverrideBody()", 1);
}

function backButtonOverrideBody() {  
  try {
    history.forward();
  } catch (e) {}
  setTimeout("backButtonOverrideBody()", 250);
}