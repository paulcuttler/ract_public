<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : Confirm
    Created on : 3/08/2009, 11:47:28
    Author     : gnewton
-->
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
          xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
          xmlns:site="urn:jsptagdir:/WEB-INF/tags"
          xmlns:s="urn:jsptld:/struts-tags" version="2.0">

  <jsp:directive.page contentType="text/html" pageEncoding="UTF-8"/>

  <s:url id="personal" action="JoinPersonalForm" includeContext="true" namespace="%{namespace}" />
  
  <s:form theme="css_xhtml" action="JoinConfirm" method="post" namespace="%{namespace}">
    <s:token />

    <h3 class="red">Join ${session['MEMBERSHIP_TYPE']}</h3>

    <div class="applicationForm">
      <site:progressBar progress="${50}" suppressHelpNote="true" />

      <p class="sys-msg msg-${actionResult}"><s:property value="actionMessage" /></p>

      <div class="joinForm">
        <div class="headingPrintContainer">
          <h5>Confirm Details</h5>
          <!-- <a href="#" onclick="window.print();" class="membershipPrintPage"><span>Print Page</span></a> -->
        </div>

        <div class="membershipSummary">

<!--           <div class="summary-title1">${session['MEMBERSHIP_TYPE']} Details</div>-->
          <div class="summary-title1">&#160;</div>
          <div class="membershipConfirm">
            <div class='row'>
            	<div class='col-sm-4 col-xs-12'>Product Type: </div>
            	<div class='col-sm-8 col-xs-12'>
            		<span class="highlight">
            			<s:select onchange="reloadForm()" name="productName" requiredLabel="true" list="%{#session['products']}" value="%{#session['productName']}" id="productNameField"/>
            		</span>
            	</div>
            </div>
            <div class='row'>
            	<div class='col-sm-4 col-xs-12'>Payment Frequency: </div>
            	<div class='col-sm-8 col-xs-12'>
            		<span class="highlight">
            			<s:select onchange="reloadForm()" name="termLength" requiredLabel="true" list="%{#session['frequencies']}" value="%{#session['termLength']}" id="termLengthField" />
            		</span>
            	</div>
            </div>
            <div class='row'>
            	<div class='col-sm-4 col-xs-12'>Start Date: </div>
            	<div class='col-sm-8 col-xs-12'><span class="highlight"><s:property value="#session['startDate']" /></span></div>
            </div>
            <div class='row'>
            	<div class='col-sm-4 col-xs-12'>End Date: </div>
            	<div class='col-sm-8 col-xs-12'><span class="highlight"><s:property value="#session['endDate']" /></span></div>
            </div>
            <div class='row'>
            	<div class='col-sm-4 col-xs-12'>Total Amount Payable: </div>
            	<div class='col-sm-8 col-xs-12'><span class="highlight highlight-inline"><fmt:formatNumber type="currency" pattern="$0.00" value="${container.webMembershipTransactionHeader.amountPayable}" /></span>${container.webMembershipTransactionHeader.amountPayable == 0 ? '':' (Min. annual cost)'}</div>
            </div>
            
            <s:if test="%{#session.termLength != '1:0:0:0:0:0:0'}">
            <div class='row'>
            	<div class='col-sm-4 col-xs-12'>First Monthly Payment: </div>
            	<div class='col-sm-8 col-xs-12'><span class="highlight"><fmt:formatNumber type="currency" pattern="$0.00" value="${session['selectedFirstInstallment']}" />${session['MEMBERSHIP_TYPE'] == 'Lifestyle' ? '':'*'}</span></div>
            </div>
            <div class='row'>
            	<div class='col-sm-4 col-xs-12'>Subsequent Monthly Payments: </div>
            	<div class='col-sm-8 col-xs-12'><span class="highlight"><fmt:formatNumber type="currency" pattern="$0.00" value="${session['selectedSubsequentInstallment']}" />${session['MEMBERSHIP_TYPE'] == 'Lifestyle' ? '':'*'}</span></div>
            </div>
            </s:if>
          </div>
	
          <div class="summary-title2">&#160;</div>
          
          <site:customerDetailsTable customerData="${CUSTOMER_TOKEN}" />

        </div>

        <h5 class="terms">Terms &amp; Conditions</h5>

        <div class="infobox">
          <p>Please read your ${session['MEMBERSHIP_TYPE'] == 'Lifestyle'?'lifestyle':'roadside'} <a href="${session['MEMBERSHIP_TYPE'] == 'Lifestyle' ? 'https://www.ract.com.au/-/media/project/ract-group/ract/ract-website/membership/become-a-member/lifestyle-terms-and-conditions.pdf':'https://www.ract.com.au/-/media/project/ract-group/ract/ract-website/cars-and-driving/roadside-assistance/documents/roadside_tc_dl_brochure_0818_web.pdf'}" target="_new" rel="external">terms &amp;amp; conditions</a> before confirming your purchase.</p>
          
          <div class="checkbox" style='margin-top: 15px;'>
<!--             <s:checkbox label="I have read &amp; accept these conditions." name="terms" requiredLabel="true" />-->
			<div id="terms_error" class="terms_check_error">You must agree to the terms and conditions</div>
			<dl id="wwgrp_terms" class="wwgrp">
				<dd id="wwctrl_terms" class="wwctrl">
					<div class="wwctrl-inner">
						<label class='label-checkbox' for='terms'>
							<input type="checkbox" name="terms" value="true" id="terms" onclick="doToggle(this)"/>
							<input type="hidden" id="__checkbox_terms" name="__checkbox_terms" value="true" />
							<span class='check-indicator'>&#160;</span><span class='check-description'>I have read &amp; accept my ${session['MEMBERSHIP_TYPE'] == 'Lifestyle'?'lifestyle':'roadside'} membership conditions.</span>
						</label>
					</div>
				</dd>
			</dl>


            <script type="text/javascript">
              jQuery('#wwctrl_JoinConfirm_terms').find('span.required').remove();
              jQuery('#wwlbl_JoinConfirm_terms').find('label').append(' <span class="required">*</span>');
            </script>
          </div>
          
          <s:if test="%{#session.MEMBERSHIP_TYPE != 'Lifestyle'}">
          <p>* You are requesting to pay your 12 month roadside cover with <s:if test="%{#session.termLength != '1:0:0:0:0:0:0'}">monthly</s:if> direct debit. We'll send you a payment schedule shortly. This is an annual agreement and cannot be cancelled during this time. Please refer to our <a href='https://www.ract.com.au/-/media/project/ract-group/ract/ract-website/cars-and-driving/roadside-assistance/documents/Roadsideclientserviceagreement2016.pdf' target="_blank"> roadside client service agreement for more information.</a></p>
          </s:if>
        </div>

        <!-- Ensure that T & C is checked -->
        <script type="text/javascript">
        	function check_toggle_state() {
        	  if (!document.getElementById("terms").checked) {
        	    let termsCSS = document.getElementById("terms_error");
        	    termsCSS.className = "terms_check_visible";
        	    return false;
        	  } else return true;
        	}
        	
        	function doToggle(self) {
        	  if (self.checked){
	      	      document.getElementById('terms_error').className = 'terms_check_error';
        	  } else {
      	      	document.getElementById('terms_error').className = 'terms_check_visible';
        	  }
        	}
        </script>

        <div class="actions">
          <div class="row">
            <div class="col-xs-6 col-sm-4">
              <s:a href="%{personal}" id="prevStep" cssClass="actionLink prev">Previous <span class="glyphicon glyphicon-chevron-left"><!-- --></span></s:a>
            </div>
            <div class="col-xs-6 col-sm-4">
              <s:a href="#" id="nextStep" onclick="if (check_toggle_state()) document.getElementById('JoinConfirm').submit(); return false;" cssClass="actionLink next payOnline">${container.webMembershipTransactionHeader.amountPayable == 0 ? 'Get':'Buy'} now <span class="glyphicon glyphicon-chevron-right"><!-- --></span></s:a>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <script type='text/javascript'>
	    var getPathFromUrl = function(url) {
	      return url.split(/[?#]/)[0];
	    }
    
    	var reloadForm = function() {
    	  var productName = jQuery("#productNameField").val();
    	  var termLength = jQuery("#termLengthField").val(); 
    	  var url = getPathFromUrl(window.location.href) + "?productName=" + productName + "&amp;termLength=" + termLength;
    	  window.location = url;
    	};
    </script>
    
  </s:form>
</jsp:root>
