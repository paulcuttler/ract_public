<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : customerDetailsFields
    Created on : 4/12/2009, 13:26:41
    Author     : gnewton
-->
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
          xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
          xmlns:fn="http://java.sun.com/jsp/jstl/functions"
          xmlns:s="urn:jsptld:/struts-tags"
          xmlns:site="urn:jsptagdir:/WEB-INF/tags" version="2.0">

  <jsp:directive.tag pageEncoding="UTF-8"/>

  <jsp:directive.attribute name="title" />
  <jsp:directive.attribute name="readonlyPersonal" type="java.lang.Boolean" />
  <jsp:directive.attribute name="suppressPostalAddress" type="java.lang.Boolean" />
  <!--
    allow customerData to be passed as Object either a ClientVO or ClientTransaction
  -->
  <jsp:directive.attribute name="customerData" type="java.lang.Object" required="true" />

  <s:if test="%{#attr.title != null}">
    <h3>${title}</h3>
  </s:if>

  <fieldset>
    <legend><!-- --></legend>
    <div class="qHolder tight formPersonal">
      <s:if test="%{#attr.readonlyPersonal}">
        <site:readonlyField fieldLabel="Title" fieldValue="${customerData.title}" />
      </s:if>
      <s:else>
        <s:select name="title" label="Title" requiredLabel="true" listKey="titleName" listValue="titleName" list="%{#session['titles']}" value="%{title ? title : #attr.customerData.title.trim()}" />
      </s:else>
    </div>
    <div class="qHolder tight formPersonal">
      <s:if test="%{#attr.readonlyPersonal}">
        <site:readonlyField fieldLabel="Given Names a" fieldValue="${customerData.givenNames}" />
      </s:if>
      <s:else>
        <s:textfield name="firstName" onkeypress="return isAlphaKey(event);" value="%{firstName ? firstName : #attr.customerData.givenNames.trim()}" label="Given Names" size="40" requiredLabel="true" cssClass="text" />
      </s:else>
    </div>
    <div class="qHolder tight formPersonal">
      <s:if test="%{#attr.readonlyPersonal}">
        <site:readonlyField fieldLabel="Surname" fieldValue="${customerData.surname}" />
      </s:if>
      <s:else>
        <s:textfield name="lastName" onkeypress="return isAlphaKey(event);" value="%{lastName ? lastName : #attr.customerData.surname.trim()}" label="Surname" size="40" requiredLabel="true" cssClass="text" />
      </s:else>
    </div>
    <div class="qHolder tight formPersonal">
      <s:if test="%{#attr.readonlyPersonal}">
        <site:readonlyField fieldLabel="Gender" fieldValue="${customerData.gender}" />
      </s:if>
      <s:else>
        <s:select name="gender" disabled="%{#attr.readonlyPersonal}" list="{'','Male','Female','unspecified'}" value="%{gender ? gender : #attr.customerData.gender.trim()}" label="Gender" requiredLabel="true" cssClass="gender" />
      </s:else>
    </div>
    <div class="qHolder formPersonal">
      <s:if test="%{#attr.customerData.birthDate}">
        <fmt:formatDate var="dobString" value="${customerData.birthDate}" pattern="dd/MM/yyyy" scope="request" />
        <s:if test="%{#attr.readonlyPersonal}">
          <site:readonlyField fieldLabel="Date of Birth" fieldValue="${dobString}" />
        </s:if>
        <s:else>
          <s:textfield name="dob" onkeypress="return isDateKey(event);" maxLength="10" label="Date of Birth" cssClass="dob" requiredLabel="true" value="%{#request.dobString}" />
          <!-- <span class="notes">eg dd/mm/yyyy</span> -->
        </s:else>
      </s:if>
      <s:else>
        <s:textfield name="dob" onkeypress="return isDateKey(event);" maxLength="10" label="Date of Birth" cssClass="dob" requiredLabel="true" />
        <!-- <span class="notes">eg dd/mm/yyyy</span> -->
      </s:else>
    </div>
    <div class="qHolder tight formPersonal">
      <s:textfield name="email" onblur="validEmail(this, true);" value="%{email ? email : #attr.customerData.emailAddress.trim()}" label="Email Address" size="40" requiredLabel="true" cssClass="email" />
    </div>
    <div class="qHolder formPersonal">
      <s:textfield name="confirmEmail" onblur="validEmail(this, true);" value="%{confirmEmail ? confirmEmail : #attr.customerData.emailAddress.trim()}" label="Confirm Email" size="40" requiredLabel="true" cssClass="email" />
    </div>

    <div class="qHolder formPersonalNumber formPersonal">
      <div class="wwgrp">
        <div class="wwlbl">
          <label><span class="required">*</span> Please provide at least one phone number</label>
        </div>
      </div>
    </div>
    <div class="qHolder formPersonal formNumbers">
      <div class="wwgrp">
        <div class="phoneHolders">
          <div class="wwerr">
            <s:fielderror>
              <s:param>homePhone</s:param>
            </s:fielderror>
          </div>
          <div class="wwlbl">
            <label class="label" for="homePhone2"> Home: </label>
          </div>

          <div class="wwctrl">
            <input type="text" name="homePhoneAreaCode" onkeypress="return isNumberKey(event);" onkeyup="if (this.value.length==2) { document.getElementById('homePhone2').focus(); }" value="${fn:trim(homePhoneAreaCode)}" size="2" class="areaCode" maxlength="2" />
            <input type="text" name="homePhone" id="homePhone2" onkeypress="return isNumberKey(event);" value="${fn:trim(homePhone)}" size="8" class="shortPhone" maxlength="8" />
          </div>
		</div>
	  </div>
	</div>
	<div class="qHolder formPersonal formNumbers">
      <div class="wwgrp">
        <div class="phoneHolders">
          <div class="wwerr">
            <s:fielderror>
              <s:param>workPhone</s:param>
            </s:fielderror>
          </div>
          
          <div class="wwlbl">
            <label class="label" for="workPhone"> Work: </label>
          </div>

          <div class="wwctrl">
            <input type="text" name="workPhoneAreaCode" onkeypress="return isNumberKey(event);" onkeyup="if (this.value.length==2) { document.getElementById('workPhone').focus(); }" value="${fn:escapeXml(fn:trim(workPhoneAreaCode))}" size="2" class="areaCode" maxlength="2" />
            <input type="text" name="workPhone" id="workPhone" onkeypress="return isNumberKey(event);" value="${fn:trim(workPhone)}" size="8" class="shortPhone" maxlength="8" />
          </div>

        </div>
      </div>
    </div>
    
    <div class="qHolder tighht formPersonal">
      <div class="phoneHolders">
     	 <s:textfield name="mobilePhone" label="Mobile" onkeypress="return isNumberKey(event);" value="%{mobilePhone ? mobilePhone : #attr.customerData.mobilePhone.trim()}" size="10" cssClass="phone" maxLength="10" />
      </div>
    </div>

    <div class="sub-title"><h5>Residential Address (Tasmania only)</h5></div>

    <div class="qHolder tight formPersonal">
      <s:textfield name="residentialPostcode" onkeypress="return isNumberKey(event);" onchange="updateStreetSuburbs(this, '%{streetSuburbQuery}', RESIDENTIAL,'','%{#session['MEMBERSHIP_TYPE']}');" value="%{residentialPostcode ? residentialPostcode : #attr.customerData.resiPostcode.trim()}" cssClass="postcode" label="Postcode" id="residentialPostcode" maxlength="4" requiredLabel="true" />
    </div>
    <div class="qHolder tight formPersonal">
      <s:select name="residentialState" value="%{residentialState ? residentialState : #attr.customerData.resiState.trim()}" id="residentialState" list="#{'TAS':'Tasmania'}" label="State" requiredLabel="true" />
    </div>
    <div class="qHolder tight claro formPersonal">
      <s:textfield name="residentialSuburb" onkeypress="return isAlphaKey(event);" value="%{residentialSuburb ? residentialSuburb : #attr.customerData.resiSuburb.trim()}" label="Suburb" id="residentialSuburb" size="40" cssClass="text" onfocus="updateStreetSuburbs(this, '%{streetSuburbQuery}', RESIDENTIAL);" requiredLabel="true" />
    </div>
    <div class="qHolder tight claro formPersonal">
      <s:textfield name="residentialStreet" onkeypress="return isAlphaKey(event);" value="%{residentialStreet ? residentialStreet : #attr.customerData.resiStreet.trim()}" label="Street" id="residentialStreet" size="40" cssClass="text" onfocus="updateStreetSuburbs(this, '%{streetSuburbQuery}', RESIDENTIAL);" requiredLabel="true" />
    </div>
    <div class="qHolder tight formPersonal">
      <s:textfield name="residentialPropertyQualifier" value="%{residentialPropertyQualifier ? residentialPropertyQualifier : #attr.customerData.resiStreetChar.trim()}" label="Street Number" requiredLabel="true" cssClass="text" />
    </div>
    <div class="qHolder formPersonal">
      <s:textfield name="residentialPropertyName" value="%{residentialPropertyName ? residentialPropertyName : #attr.customerData.resiProperty.trim()}" label="Property Name" size="40" cssClass="text" />
    </div>
        
    <s:if test="%{#attr.suppressPostalAddress == null or !#attr.suppressPostalAddress}">
	  <div class="sub-title"><h5>Postal Address (Tasmania only)</h5></div>

      <!-- Re-skin remove showPostal checkbox -->
      <div class="checkbox postal-address">
        <!-- <s:checkbox name="showPostal" id="showPostal" label="Please check the box if postal address is different to the above details" onclick="toggleCheckbox(this, document.getElementById('postalContainer'));" value="%{#session['showPostal'] != null ? #session['showPostal'] : showPostal}" />-->
		<dl id="wwgrp_showPostal" class="wwgrp">
			<dd id="wwctrl_showPostal" class="wwctrl">
				<div class="wwctrl-inner">
					<label class='label-checkbox' for='showPostal'>
						<jsp:scriptlet>
						  <![CDATA[
								if (session.getAttribute("showPostal") != null) {
								    boolean doShow = (Boolean) session.getAttribute("showPostal");
								    if (doShow) {
										out.print("<input type=\"checkbox\" name=\"showPostal\" value=\"true\" checked=\"checked\" id=\"showPostal\" onclick=\"toggleCheckbox(this, document.getElementById('postalContainer'));\"/>");
										out.print("<script type=\"text/javascript\">");
										out.print("document.getElementById('postalContainer').style.display = '';");
									    out.print("</script>");
								    } else {
										out.print("<input type=\"checkbox\" name=\"showPostal\" value=\"true\" id=\"showPostal\" onclick=\"toggleCheckbox(this, document.getElementById('postalContainer'));\"/>");
										out.print("<script type=\"text/javascript\">");
										out.print("document.getElementById('postalContainer').style.display = 'none';");
									    out.print("</script>");
								    }
								} else {
									out.print("<input type=\"checkbox\" name=\"showPostal\" value=\"true\" id=\"showPostal\" onclick=\"toggleCheckbox(this, document.getElementById('postalContainer'));\"/>");
								}
						  ]]>
						</jsp:scriptlet>					
<!-- 						<s:if test="%{#isPostalChecked}">
							<input type="checkbox" name="showPostal" value="true" checked="checked" id="showPostal" onclick="toggleCheckbox(this, document.getElementById('postalContainer'));"/>
							<script type="text/javascript">
  						    	alert('show')    
								document.getElementById('postalContainer').style.display = '';
						    </script>
    					</s:if>
						<s:else>
							<input type="checkbox" name="showPostal" value="true" id="showPostal" onclick="toggleCheckbox(this, document.getElementById('postalContainer'));"/>
							<script type="text/javascript">
						    	alert('dont show')    
						        document.getElementById('postalContainer').style.display = 'none';
						    </script>
						</s:else>-->
						<input type="hidden" id="__checkbox_showPostal" name="__checkbox_showPostal" value="true" />
						<span class='check-indicator'>&#160;</span><span class='check-description'>Please check the box if postal address is different to the details above.</span>
					</label>
				</div>
			</dd>
		</dl>
      </div>
      <div id="postalContainer">
        <div class="qHolder tight formPersonal">
          <s:textfield name="postalPostcode" onkeypress="return isNumberKey(event);" onchange="updateStreetSuburbs(this, '%{streetSuburbQuery}', POSTAL,'','%{#session['MEMBERSHIP_TYPE']}');" value="%{postalPostcode ? postalPostcode : #attr.customerData.postPostcode.trim()}" cssClass="postcode" label="Postcode" id="postalPostcode" maxlength="4" requiredLabel="true" />
        </div>
        <div class="qHolder tight formPersonal">
          <s:select name="postalState" value="%{postState ? postState : #attr.customerData.postalState.trim()}" id="postalState" list="#{'TAS':'Tasmania'}" label="State" requiredLabel="true" />
        </div>
        <div class="qHolder tight claro formPersonal">
          <s:textfield name="postalSuburb" onkeypress="return isAlphaKey(event);" value="%{postalSuburb ? postalSuburb : #attr.customerData.postSuburb.trim()}" label="Suburb" id="postalSuburb" size="40" cssClass="text" onfocus="updateStreetSuburbs(this, '%{streetSuburbQuery}', POSTAL);" requiredLabel="true" />
        </div>
        <div class="qHolder tight claro formPersonal">
          <s:textfield name="postalStreet" onkeypress="return isAlphaKey(event);" value="%{postalStreet ? postalStreet : #attr.customerData.postStreet.trim()}" label="Street" id="postalStreet" size="40" cssClass="text" onfocus="updateStreetSuburbs(this, '%{streetSuburbQuery}', POSTAL);" />
          <span class="notes"><i>Leave blank if using PO Box</i></span>
        </div>
        <div class="qHolder tight formPersonal">
          <s:textfield name="postalPropertyQualifier" value="%{postalPropertyQualifier ? postalPropertyQualifier : #attr.customerData.postStreetChar.trim()}" label="Street Number" cssClass="text" />
          <span class="notes"><i>Leave blank if using PO Box</i></span>
        </div>
        <div class="qHolder formPersonal">
          <s:textfield name="postalPropertyName" value="%{postalPropertyName ? postalPropertyName : #attr.customerData.postProperty.trim()}" label="Property Name or PO Box" size="40" cssClass="text" />
        </div>

      </div>
    </s:if>
  </fieldset>

  <s:if test="%{#attr.suppressPostalAddress == null or !#attr.suppressPostalAddress}">
    <script type="text/javascript">
      toggleCheckbox(document.getElementById('showPostal'), document.getElementById('postalContainer'));
    </script>
  </s:if>

  <s:if test="#attr.customerData.resiPostcode.trim() != ''">
    <script type="text/javascript">
      updateStreetSuburbs($('residentialSuburb'), '${streetSuburbQuery}', RESIDENTIAL);
      updateStreetSuburbs($('residentialStreet'), '${streetSuburbQuery}', RESIDENTIAL);
    </script>
  </s:if>

  <s:if test="#attr.customerData.postPostcode.trim() != ''">
    <script type="text/javascript">
      updateStreetSuburbs($('postalSuburb'), '${streetSuburbQuery}', POSTAL);
      updateStreetSuburbs($('postalStreet'), '${streetSuburbQuery}', POSTAL);
    </script>
  </s:if>

</jsp:root>