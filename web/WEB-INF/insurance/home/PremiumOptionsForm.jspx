<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : YourPremiumForm
    Created on : 25/05/2011, 4:40:23 PM
    Author     : gnewton
-->
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
          xmlns:s="urn:jsptld:/struts-tags"
          xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
          xmlns:site="urn:jsptagdir:/WEB-INF/tags" version="2.0">

  <jsp:directive.page contentType="text/html" pageEncoding="UTF-8"/>

  <site:insuranceHomeFormIntro>
    <p>This quote is based on the information you have provided to us.  If you choose to proceed with this quote you will be asked more questions, which may result in a different premium, altered conditions or refusal of cover</p>
  </site:insuranceHomeFormIntro>

  <dl class="premium">
    <dt>Your premium is:</dt>
    <dd class="amounts">
      <div class="year">
        <span class="amount" id="annualPremium"><fmt:formatNumber currencySymbol="$" value="${totalAnnualPremium}" type="currency" /></span>
        <span class="inc">Per Year</span>
      </div>
      <div class="month">
        <span class="amount" id="monthlyPremium"><fmt:formatNumber currencySymbol="$" value="${totalMonthlyPremium}" type="currency" /></span>
        <span class="inc">Per Month</span>
      </div>
      <span class="or">or</span>
    </dd>
  </dl>

  <s:form action="HomePremiumOptions" namespace="%{namespace}" method="post" theme="fm_xhtml" id="premiumOptionsForm">
    <fieldset>
      <s:if test="buildingOptionsRequired">
        <div class="premiumOptions">
          <h3>Building Options</h3>
          <site:help message="An example of cover includes: Falling through the ceiling. Please refer to the Home PDS for further details. The cost of this option is an additional ${buildingAccidentalDamagePercentage}" />
          <s:checkbox name="buildingAccidentalDamage" label="Accidental Damage" labelposition="question" />

          <fmt:formatNumber var="bldgStrmDmgCost" currencySymbol="$" value="${buildingStormDamageCost}" type="currency" maxFractionDigits="0" />
          <site:help message="Storm damage will be covered for gates and fences but retaining walls and freestanding walls will not be covered for storm damage.  Please refer to the Home PDS for further details.  The cost of this option is ${bldgStrmDmgCost}" />
          <s:checkbox name="buildingStormDamage" label="Storm Damage (Gates &amp; Fences)" labelposition="question" />

          <fmt:formatNumber var="bldgMtrDmgCost" currencySymbol="$" value="${buildingMotorDamageCost}" type="currency" maxFractionDigits="0" />
          <site:help message="This option covers burning out or fusing of a hardwired household electric motor which is under 10 years old. The cost of this option is ${bldgMtrDmgCost}" />
          <s:checkbox name="buildingMotorDamage" label="Damage to Electric Motors" labelposition="question" />
        </div>
      </s:if>

      <s:if test="contentsOptionsRequired">
        <div class="premiumOptions">
          <h3>Contents Options</h3>

          <s:if test="allowUnspecifiedPersonalEffects">
            <site:help message="$500 limit per item/$1,500 per claim with a $50 excess. Please refer to RACT Insurance Home Insurance PDS for further details." />
            <s:checkbox name="unspecifiedPersonalEffects" label="Unspecified Personal Effects" labelposition="question" />
          </s:if>

          <site:help message="Some examples of cover includes: Spilling of red wine, Spilling paint. Please refer to the Home PDS for further details The cost of this option is ${contentsAccidentalDamagePercentage}"/>
          <s:checkbox name="contentsAccidentalDamage" label="Accidental Damage" labelposition="question" />

          <fmt:formatNumber var="wkrsCmpCost" currencySymbol="$" value="${workersCompensationCost}" type="currency" maxFractionDigits="0" />
          <site:help message="The following people employed for household domestic duties could be covered under Domestic Workers Compensation:
                     regular cleaner paid by the hour who uses the insured's equipment and receives directions, is not an employee of a cleaning business or contractor, house keeper or servant, regular maintenance person.
                     Contact Workplace Standards Authority or a solicitor for advice on Domestic Workers Compensation. The cost of this option is ${wkrsCmpCost}" />
          <s:checkbox name="workersCompensation" label="Domestic Workers Compensation" labelposition="question" />

          <fmt:formatNumber var="cntsMtrDmgCost" currencySymbol="$" value="${contentsMotorDamageCost}" type="currency" maxFractionDigits="0" />
          <site:help message="This option covers burning out or fusing of a household electric motor which is under 10 years old.  The cost of this option is ${cntsMtrDmgCost}" />
          <s:checkbox name="contentsMotorDamage" label="Damage to Electric Motors" labelposition="question" />
        </div>
      </s:if>

      <s:if test="investorOptionsRequired">
        <div class="premiumOptions">
          <h3>Investor Home Options</h3>

          <fmt:formatNumber var="invStrmDmgCost" currencySymbol="$" value="${investorStormDamageCost}" type="currency" maxFractionDigits="0" />
          <site:help message="Storm damage will be covered for gates and fences but retaining walls and freestanding walls will not be covered for storm damage.  Please refer to the Home PDS for further details.  The cost of this option is ${invStrmDmgCost}" />
          <s:checkbox name="investorStormDamage" label="Storm Damage (Gates &amp; Fences)" labelposition="question" />

          <fmt:formatNumber var="invMtrDmgCost" currencySymbol="$" value="${investorMotorDamageCost}" type="currency" maxFractionDigits="0" />
          <site:help message="This option covers burning out or fusing of a hardwired household electric motor which is under 10 years old.  The cost of this option is ${invMtrDmgCost}" />
          <s:checkbox name="investorMotorDamage" label="Damage to Electric Motors" labelposition="question" />
        </div>

        <div class="premiumOptions">
          <h3>Investor Contents Options</h3>

          <fmt:formatNumber var="invCntsMtrDmgCost" currencySymbol="$" value="${investorContentsMotorDamageCost}" type="currency" maxFractionDigits="0" />
          <site:help message="This option covers burning out or fusing of a household electric motor which is under 10 years old.  The cost of this option is ${invCntsMtrDmgCost}" />
          <s:checkbox name="investorContentsMotorDamage" label="Damage to Electric Motors" labelposition="question" />

          <fmt:formatNumber var="inv30Cost" currencySymbol="$" value="${investorCover30Cost}" type="currency" maxFractionDigits="0" />
          <site:help message="$10,000 worth of Landlord's fittings is automatically covered under the Investor PDS.  By purchasing this option you can increase the contents cover to $30,000. The cost of this option is an additional ${inv30Cost}" />
          <s:checkbox name="investorCover30" id="investorCover30" label="Increase Contents to $30,000" labelposition="question" />

          <fmt:formatNumber var="inv50Cost" currencySymbol="$" value="${investorCover50Cost}" type="currency" maxFractionDigits="0" />
          <site:help message="$10,000 worth of Landlord's fittings is automatically covered under the Investor PDS.  By purchasing this option you can increase the contents cover to $50,000. The cost of this option is an additional ${inv50Cost}" />
          <s:checkbox name="investorCover50" id="investorCover50" label="Increase Contents to $50,000" labelposition="question" />
        </div>
      </s:if>

      <h3>Excess</h3>
      <p>Choose your excess (a higher excess reduces your premium)</p>

      <s:if test="buildingOptionsRequired">
        <site:help message="Choosing a higher excess (payable in the event of a claim) will reduce your premium" />
        <s:select name="buildingExcess" label="Building" list="buildingExcessOptions" listKey="getrClass()" listValue="%{'$' + getrDescription().trim()}" requiredLabel="true" />
      </s:if>
      <s:if test="contentsOptionsRequired">
        <site:help message="Choosing a higher excess (payable in the event of a claim) will reduce your premium" />
        <s:select name="contentsExcess" label="Contents" list="contentsExcessOptions" listKey="getrClass()" listValue="%{'$' + getrDescription().trim()}" requiredLabel="true" />
      </s:if>
      <s:if test="investorOptionsRequired">
        <site:help message="Choosing a higher excess (payable in the event of a claim) will reduce your premium" />
        <s:select name="investorExcess" label="Investor" list="investorExcessOptions" listKey="getrClass()" listValue="%{'$' + getrDescription().trim()}" requiredLabel="true" />
      </s:if>

      <site:nextPrevNav nextCssClass="finaliseQuote" suppressPrev="${true}" cssClass="finalise" formActionNameIs="premiumOptionsForm" customButtonName="Finalise Quote"  />
    </fieldset>
  </s:form>

  <!--
    Start of DoubleClick Floodlight Tag: Please do not remove
    Activity name of this tag: 2. Home Insurance - quote
    URL of the webpage where the tag is expected to be placed: https://secure.ract.com.au/HomePremiumOptionsForm?ajaxContext=false
    This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
    Creation Date: 03/26/2015
  -->
  <script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="https://4084924.fls.doubleclick.net/activityi;src=4084924;type=racth000;cat=2home0;ord=1;num=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
  </script>
  <noscript>
    <iframe src="https://4084924.fls.doubleclick.net/activityi;src=4084924;type=racth000;cat=2home0;ord=1;num=1?" width="1" height="1" frameborder="0" style="display:none"><!-- --></iframe>
  </noscript>
  <!-- End of DoubleClick Floodlight Tag: Please do not remove -->

</jsp:root>