package futuremedium.client.ract.secure.test;

import futuremedium.client.ract.secure.actions.insurance.AgentPortal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gnewton
 */
public class AgentPortalTest extends TestBase {

  public AgentPortalTest() {
    super();
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Test
  public void testAgentPortal() {
    boolean success;
    AgentPortal action = new AgentPortal();
    action.setAgentService(this.getAgentService());

    action.setAgentCode("AAA");
    action.setUsername("AGENT 86");
    action.setPassword("apassword");

    success = this.getAgentService().authenticateAgent(action);
    // cancelled
    assertFalse(success);

    action.setAgentCode("BBB");
    action.setUsername("AGENT 99");
    action.setPassword("sfsd");

    success = this.getAgentService().authenticateAgent(action);
    // active
    assertTrue(success);

    action.setAgentCode("CCC");
    action.setUsername("CHIEF");
    action.setPassword("3password");

    success = this.getAgentService().authenticateAgent(action);
    // inactive (password not effective until 8/8/2011)
    assertFalse(success);
  }
}
