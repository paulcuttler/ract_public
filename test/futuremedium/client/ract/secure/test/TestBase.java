package futuremedium.client.ract.secure.test;

import futuremedium.client.ract.secure.entities.insurance.AgentService;
import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import futuremedium.client.ract.secure.entities.insurance.home.*;

/**
 *
 * @author gnewton
 */
public class TestBase extends AbstractDependencyInjectionSpringContextTests {

  protected static final int QUOTE_NO = 438;
  private OptionService optionService;
  private HomeService homeService;
  private AgentService agentService;

  public TestBase() {
    TestSupport.setupTestEnvironment();

    System.setProperty("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
    System.setProperty("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
    System.setProperty("java.naming.provider.url", "jnp://localhost:1099");

  }
    
  @Override
  protected String[] getConfigLocations() {
    return new String[]{"applicationContext.xml"};
  }

  /**
   * @return the optionService
   */
  public OptionService getOptionService() {
    return optionService;
  }

  /**
   * @param optionService the optionService to set
   */
  public void setOptionService(OptionService optionService) {
    this.optionService = optionService;
  }

  /**
   * @return the homeService
   */
  public HomeService getHomeService() {
    return homeService;
  }

  /**
   * @param homeService the homeService to set
   */
  public void setHomeService(HomeService homeService) {
    this.homeService = homeService;
  }

  /**
   * @return the agentService
   */
  public AgentService getAgentService() {
    return agentService;
  }

  /**
   * @param agentService the agentService to set
   */
  public void setAgentService(AgentService agentService) {
    this.agentService = agentService;
  }

}
