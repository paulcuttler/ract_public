package futuremedium.client.ract.secure.test;

import com.ract.common.GenericException;
import com.ract.web.security.*;
import java.util.*;
import java.util.logging.*;
import javax.naming.*;
import org.junit.*;

import static org.junit.Assert.*;

/**
 * Test Authentication.
 * NB: This test will not work without a VPN connection.
 * @author gnewton
 */
public class AuthTest {

  UserSecurityMgrRemote userMgr;
  InitialContext ctx;

  public AuthTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() {
    Properties properties = new Properties();
    properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
    properties.put("java.naming.factory.url.pkgs", "=org.jboss.naming:org.jnp.interfaces");
    properties.put("java.naming.provider.url", "jnp://localhost:1099");

    try {
      ctx = new InitialContext(properties);
    } catch (NamingException ex) {
      Logger.getLogger(AuthTest.class.getName()).log(Level.SEVERE, null, ex);
    }

  }

  @After
  public void tearDown() {
  }

  @Test
  public void testAuth() {
    try {
      userMgr = (UserSecurityMgrRemote) ctx.lookup("UserSecurityMgr/remote");

      assertNotNull(userMgr);

      User user = userMgr.getUserAccount("testuser");

      assertNotNull(user);

      //assertTrue(userMgr.passwordsMatch("testuser", "test"));

    } catch (Exception ex) {
      Logger.getLogger(AuthTest.class.getName()).log(Level.SEVERE, null, ex);
    }


  }
}