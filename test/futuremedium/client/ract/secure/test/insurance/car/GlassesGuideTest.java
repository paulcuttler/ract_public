/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package futuremedium.client.ract.secure.test.insurance.car;

import com.ract.common.SystemException;
import com.ract.web.insurance.test.WebInsGlMgrStub;
import com.ract.web.insurance.GlVehicle;
import com.ract.web.insurance.ListPair;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gnewton
 */
public class GlassesGuideTest {

    public GlassesGuideTest() {
    }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void lookupVehicle() {
      WebInsGlMgrStub glassesMgr = new WebInsGlMgrStub();

      GlVehicle queryVehicle = new GlVehicle();
      queryVehicle.setVehYear(2000);
      queryVehicle.setMake("FOR");
      queryVehicle.setModel("LASER");
      queryVehicle.setBodyType("05");
      queryVehicle.setTransmission("4F");
      queryVehicle.setCylinders("4");

      try {
        for (GlVehicle vehicle : glassesMgr.getList(queryVehicle)) {
          System.out.println("NVIC = " + vehicle.getNvic() + ": " + vehicle.getSummary());
        }
      } catch (SystemException ex) {
        Logger.getLogger(GlassesGuideTest.class.getName()).log(Level.SEVERE, null, ex);
      }

    }

    @Test
    public void lookupMakes() {
      WebInsGlMgrStub glassesMgr = new WebInsGlMgrStub();

      GlVehicle queryVehicle = new GlVehicle();
      queryVehicle.setVehYear(2000);
      queryVehicle.setMake(" ");
      queryVehicle.setModel(" ");
      queryVehicle.setBodyType(" ");
      queryVehicle.setTransmission(" ");
      queryVehicle.setCylinders(" ");

      try {
        for (ListPair pair : glassesMgr.getMakes(queryVehicle)) {
          System.out.println("Col1 = " + pair.getCol1() + " ; Col2 = " + pair.getCol2());
        }
      } catch (SystemException ex) {
        Logger.getLogger(GlassesGuideTest.class.getName()).log(Level.SEVERE, null, ex);
      }
    }

}