package futuremedium.client.ract.secure.test.insurance.car;

import com.ract.common.StreetSuburbVO;
import com.ract.util.DateTime;

import com.ract.web.insurance.*;
import com.ract.web.insurance.test.*;
import com.ract.web.address.*;
import com.ract.web.client.WebClient;

import java.util.*;
import java.util.logging.*;
import org.junit.*;

import futuremedium.client.ract.secure.entities.insurance.car.GaragedAddress;
import futuremedium.client.ract.secure.test.TestBase;

/**
 *
 * @author gnewton
 */
public class VehicleQuoteTest extends TestBase {

  public VehicleQuoteTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Test
  public void startNewQuote() {
    try {
      // create user session
      WebInsMgrStub insuranceMgr = new WebInsMgrStub();

      AddressMgrRemote addressMgr = this.getOptionService().getAddressMgr();

      int quoteNo = insuranceMgr.startQuote(new DateTime(), WebInsQuote.TYPE_MOTOR);

      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS, WebInsQuoteDetail.FALSE);
      System.out.println("FIFTY_PLUS = " + insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS));

      DateTime dob = new DateTime("21/02/1976");
      String cardNo = "3084073104307418";
      WebInsClient client = insuranceMgr.setFromRACTClient(quoteNo, cardNo, dob);
      System.out.println("client = " + client.getGivenNames());

      // called glassesMgr.getList(vehicle, startDate)
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.NVIC, "2VK");
      System.out.println("NVIC = " + insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.NVIC));

      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.IMOBILISER, WebInsQuoteDetail.TRUE);
      System.out.println("IMOBILISER = " + insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.IMOBILISER));

      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.USEAGE, "Private");
      System.out.println("USEAGE = " + insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.USEAGE));

      Iterator iterator = addressMgr.getStreetSuburbsBySuburb("LENAH VALLEY", "7008").iterator();

      GaragedAddress garagedAddress = new GaragedAddress();
      while (iterator.hasNext()) {
        StreetSuburbVO streetSuburbVO = (StreetSuburbVO) iterator.next();

        if (String.valueOf(streetSuburbVO.getStreetSuburbID()).equals(this.getStreetSuburbId())) {
          garagedAddress.setStreet(streetSuburbVO.getStreet());
        }
      }
      garagedAddress.setPostCode("7000");
      garagedAddress.setStreetSuburbId(this.getStreetSuburbId());
      garagedAddress.setStreetNumber("27");
      garagedAddress.setSuburb("LENAH VALLEY");

      int agreedValue = insuranceMgr.getGlVehicleValue(quoteNo);
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.AGREED_VALUE, Integer.toString(agreedValue));
      System.out.println("AGREED_VALUE = " + insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.AGREED_VALUE));

      WebInsClient driver = insuranceMgr.createInsuranceClient(quoteNo);
      driver.setDriver(true);
      driver.setWebQuoteNo(quoteNo);
      driver.setGivenNames("Test");
      driver.setSurname("Person");
      driver.setTitle("Mr");
      driver.setGender(WebClient.GENDER_MALE);
      driver.setBirthDate(new DateTime("31/05/1980"));
      driver.setDrivingFrequency(WebInsClient.DRIVE_DAILY);
      driver.setYearCommencedDriving(2000);
      driver.setNumberOfAccidents(0);
      insuranceMgr.setClient(driver);
      //

      WebInsClient aDriver = insuranceMgr.getRatingDriver(quoteNo);


      // use highest of calc or suppled (max 60)
      if (insuranceMgr.calculateNCD(aDriver) < 60) { // %
        aDriver.setCurrentNCD(40);
      }



      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.NO_CLAIM_DISCOUNT, Integer.toString(insuranceMgr.calculateNCD(client)));
      System.out.println("NO_CLAIM_DISCOUNT = " + insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.NO_CLAIM_DISCOUNT));

      // Summary
      WebInsQuote quote = insuranceMgr.getQuote(quoteNo);

      List<WebInsQuoteDetail> quoteDetails = insuranceMgr.getAllQuoteDetails(quoteNo);
      List<WebInsClient> driverList = insuranceMgr.getClients(quoteNo);
      for (WebInsClient d : driverList) {
        List<WebInsClientDetail> driverDetails = insuranceMgr.getClientDetails(d.getWebClientNo());
      }

      // Quote and Options
      Premium premium = insuranceMgr.getPremium(quoteNo, WebInsQuoteDetail.PAY_FREQ, WebInsQuote.COVER_COMP); // or WebInsQuote.COVER_TP
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.HIRE_CAR, WebInsQuoteDetail.TRUE);
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.WINDSCREEN, WebInsQuoteDetail.TRUE);
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.FIRE_THEFT, WebInsQuoteDetail.FALSE);
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.EXCESS, "$0");

      // reset premium after options selected
      premium = insuranceMgr.getPremium(quoteNo, WebInsQuoteDetail.PAY_FREQ, WebInsQuote.COVER_COMP);
      //insuranceMgr.setCoverType(quoteNo, WebInsQuote.COVER_COMP);
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_FREQ, "paymentFrequency?");
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.PREMIUM, premium.getAnnualPremium().toString());
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.PAYMENT, "payment?");

      // Fulfillment
      insuranceMgr.setQuoteDetail(quote.getWebQuoteNo(), WebInsQuoteDetail.COVER_START, insuranceMgr.getQuoteDetail(quoteNo, "stateDate"));
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.REG_NO, "rego");
      insuranceMgr.setQuoteDetail(quoteNo, "goodOrder", WebInsQuoteDetail.TRUE);

      // Extras
      //insuranceMgr.setQuoteDetail(quoteNo, "Description of extra...", WebInsQuoteDetail.TRUE);

      // Add to existing policy holder group
      for (WebInsClient c : insuranceMgr.getClientGroups(quoteNo)) {
        insuranceMgr.setClient(c); // assuming selected
      }

      // Listed Drivers (existing policy holding)
      for (WebInsClient c : insuranceMgr.getClients(quote.getWebQuoteNo())) {

        WebInsClientDetail detail = new WebInsClientDetail();
        detail.setWebClientNo(c.getWebClientNo());
        detail.setDetailType(WebInsClient.CLIENT_DETAIL_ACC_THEFT);
        detail.setDetMonth(5);
        detail.setDetYear(2000);
        detail.setDetail("it was a nice sunny day...");
        insuranceMgr.setClientDetail(detail);

        // e.g. for suspended
        detail.setDetailType(WebInsClient.CLIENT_DETAIL_SUSPENSION);
        detail.setWebClientNo(c.getWebClientNo());

        insuranceMgr.setClientDetail(detail);

        //c.setPreferredAddress(true);
/*
        for (WebInsClientDetail detail : insuranceMgr.getClientDetails(c.getWebClientNo())) {
        // set licence suspensions
        detail.setDetail(WebInsClient.CLIENT_DETAIL_SUSPENSION);
        // set accidents and thefts          
        detail.setDetail(WebInsClient.CLIENT_DETAIL_ACC_THEFT);
        // set previously refused insurance
        detail.setDetail(WebInsClient.CLIENT_DETAIL_REFUSED);
        }
         */
        // set first name
        //??
        // set last name
        c.setGivenNames("Full Name");
        // set year commenced
        c.setYearCommencedDriving(1982);
        // set gender
        c.setGender("Male");
      }

      // Listed Drivers and Policy Holders

      // Additional Policy Holders

      // Criminal convictions

      // Contact details

      // Additional checks

      // Direct Debit Details
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_FREQ, "paymentFrequency?");
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.DD_TYPE, "Bank Account");
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.DD_BSB, "067000");
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.DD_NO, "12345678");
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.DD_NAME, "Test Account Name");
      // OR
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.DD_TYPE, "VISA");
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.CARD_NO, "5353123412341234");
      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.DD_EXP, "05/09");

      insuranceMgr.setQuoteDetail(quoteNo, WebInsQuoteDetail.DD_DAY, "Monday");

      // prepare discount message
      String coverType = insuranceMgr.getQuote(quoteNo).getCoverType();
      String freq = insuranceMgr.getQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_FREQ);
      Premium testPremium = insuranceMgr.getPremium(quoteNo, freq, coverType);

      PremiumDiscount pipDiscount = testPremium.getPremiumDiscount(PremiumDiscount.TYPE_PIP);
      PremiumDiscount ssDiscount = testPremium.getPremiumDiscount(PremiumDiscount.TYPE_SILVER_SAVER);

      // print cover note
      insuranceMgr.printCoverDoc(quoteNo);


    } catch (Exception ex) {
      Logger.getLogger(VehicleQuoteTest.class.getName()).log(Level.SEVERE, "Could not create quote", ex);
    }

  }

  private String getStreetSuburbId() {
    throw new UnsupportedOperationException("Not yet implemented");
  }
}