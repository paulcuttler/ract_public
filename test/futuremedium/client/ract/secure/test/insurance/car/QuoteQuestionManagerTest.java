package futuremedium.client.ract.secure.test.insurance.car;

import org.junit.*;
import java.util.List;

import futuremedium.client.ract.secure.entities.insurance.car.*;

import futuremedium.client.ract.secure.test.TestBase;

/**
 *
 * @author gnewton
 */
public class QuoteQuestionManagerTest extends TestBase {

  public QuoteQuestionManagerTest() {
    super();
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Test
  public void testQuestionManager() {
    QuoteQuestionManager mgr = new QuoteQuestionManager();
    List<QuotePage> quotePages = mgr.getPagesByPart("1");
    List<QuotePage> policyPages = mgr.getPagesByPart("2");

    System.out.println("Quote Pages:");
    for (QuotePage p : quotePages) {
      System.out.println(p.getId() + ": " + p.getSummary());
    }

    System.out.println("Policy Pages:");
    for (QuotePage p : policyPages) {
      System.out.println(p.getId() + ": " + p.getSummary());
    }
  }
}
