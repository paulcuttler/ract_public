package futuremedium.client.ract.secure.test.insurance.home;

import java.util.*;
import java.math.BigDecimal;

import org.junit.*;

import com.ract.web.insurance.*;

import futuremedium.client.ract.secure.actions.insurance.home.*;
import futuremedium.client.ract.secure.test.TestBase;

/**
 *
 * @author gnewton
 */
public class BuildingContentsTest extends TestBase {


  public BuildingContentsTest() {
    super();
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }
  
  @Test
  public void testAboutYou() {

    // age > 50
    System.out.println("Testing age > 50");
    AboutYou action = new AboutYou();
    action.setHomeService(this.getHomeService());
    action.setMember(false);
    action.setDobDay("1");
    action.setDobMonth("1");
    action.setDobYear("1950");

    action.validate();

    Map<String,List<String>> fieldErrors = action.getFieldErrors();

    assertTrue(!fieldErrors.isEmpty());

    System.out.println(fieldErrors);

    // age > 110
    System.out.println("Testing age > 110");
    action = new AboutYou();
    action.setHomeService(this.getHomeService());
    action.setMember(false);
    action.setDobDay("1");
    action.setDobMonth("1");
    action.setDobYear("1900");

    action.validate();

    fieldErrors = action.getFieldErrors();

    assertTrue(!fieldErrors.isEmpty());

    System.out.println(fieldErrors);

    // age < 16
    System.out.println("Testing age < 16");
    action = new AboutYou();
    action.setHomeService(this.getHomeService());
    action.setMember(false);
    action.setDobDay("1");
    action.setDobMonth("1");
    action.setDobYear("1996");

    action.validate();

    fieldErrors = action.getFieldErrors();

    assertTrue(!fieldErrors.isEmpty());

    System.out.println(fieldErrors);

    // member incorrect
    System.out.println("Testing member (incorrect details)");
    action = new AboutYou();
    action.setHomeService(this.getHomeService());
    action.setMember(true);
    action.setMembershipCard1("3084");
    action.setMembershipCard2("0701");
    action.setMembershipCard3("0432");
    action.setMembershipCard4("0029");
    action.setMemberDobDay("31");
    action.setMemberDobMonth("5");
    action.setMemberDobYear("1981");
    action.setDobDay("31");
    action.setDobMonth("5");
    action.setDobYear("1980");

    action.validate();

    fieldErrors = action.getFieldErrors();

    assertTrue(!fieldErrors.isEmpty());

    System.out.println(fieldErrors);

    // member correct
    System.out.println("Testing member (correct details)");
    action = new AboutYou();
    action.setHomeService(this.getHomeService());
    action.setMember(true);
    action.setMembershipCard1("3084");
    action.setMembershipCard2("0701");
    action.setMembershipCard3("0432");
    action.setMembershipCard4("0029");
    action.setMemberDobDay("30");
    action.setMemberDobMonth("3");
    action.setMemberDobYear("1952");
    action.setDobDay("31");
    action.setDobMonth("5");
    action.setDobYear("1980");

    action.validate();

    fieldErrors = action.getFieldErrors();

    assertTrue(fieldErrors.isEmpty());

    System.out.println(fieldErrors);
  }

  @Test
  public void testWhereYouLive() throws Exception {

    // bad postcode
    System.out.println("Testing bad postcode");
    WhereYouLive action = new WhereYouLive();
    action.setHomeService(this.getHomeService());

    action.setResidentialPostcode("3008");
    action.setResidentialSuburb("NEW TOWN");
    action.setResidentialStreet("ATHLEEN AVE");
    action.setResidentialStreetNumber("22");

    action.validate();

    Map<String,List<String>> fieldErrors = action.getFieldErrors();

    assertTrue(!fieldErrors.isEmpty());

    System.out.println(fieldErrors);

    // street / suburb combo bad
    System.out.println("Testing bad street/suburb combo");
    action = new WhereYouLive();
    action.setHomeService(this.getHomeService());

    action.setResidentialPostcode("7008");
    action.setResidentialSuburb("NEW TOWN");
    action.setResidentialStreet("ATHLEEN AVE");
    action.setResidentialStreetNumber("22");

    action.validate();

    fieldErrors = action.getFieldErrors();

    assertTrue(!fieldErrors.isEmpty());

    System.out.println(fieldErrors);

    // address ok
    System.out.println("Testing OK Address");
    action = new WhereYouLive();
    action.setHomeService(this.getHomeService());

    action.setResidentialPostcode("7008");
    action.setResidentialSuburb("LENAH VALLEY");
    action.setResidentialStreet("ATHLEEN AVE");
    action.setResidentialStreetNumber("22");

    action.validate();

    fieldErrors = action.getFieldErrors();

    assertTrue(fieldErrors.isEmpty());

    System.out.println(fieldErrors);

    // test unacceptable suburb
    System.out.println("Testing Unacceptable Suburb");
    action = new WhereYouLive();
    action.setHomeService(this.getHomeService());
    action.setOptionService(this.getOptionService());

    action.setResidentialSuburb("Rossarden");

    assertFalse(action.getOptionService().isAcceptableSuburb(action));

    action.setResidentialSuburb("Hobart");

    assertTrue(action.getOptionService().isAcceptableSuburb(action));

  }

  @Test
  public void testPremium() throws Exception {

    PremiumOptionsForm action = new PremiumOptionsForm();

    action.setHomeService(this.getHomeService());
    action.setOptionService(this.getOptionService());
    action.setQuoteNo(QUOTE_NO);
    action.setQuoteType(action.getHomeService().retrieveQuoteDetail(action.getQuoteNo(), ActionBase.FIELD_QUOTE_SUB_TYPE));

    BigDecimal annualPremium = action.getTotalAnnualPremium();
    assertNotNull(annualPremium);
    System.out.println("annualPremium: " + annualPremium);
    assertTrue(annualPremium.compareTo(BigDecimal.ZERO) == 1); // > 0

    BigDecimal monthlyPremium = action.getTotalMonthlyPremium();
    assertNotNull(monthlyPremium);
    System.out.println("monthlyPremium: " + monthlyPremium);
    assertTrue(monthlyPremium.compareTo(BigDecimal.ZERO) == 1); // > 0
    
    String percent = action.getBuildingAccidentalDamagePercentage();
    assertNotNull(percent);
    System.out.println("Building Accidental Damage Percentage: " + percent);

    BigDecimal cost = action.getBuildingMotorDamageCost();
    assertNotNull(cost);
    System.out.println("Building Motor Damage Cost: " + cost);

    percent = action.getContentsAccidentalDamagePercentage();
    assertNotNull(percent);
    System.out.println("Contents Accidental Damage Percentage: " + percent);

    cost = action.getContentsMotorDamageCost();
    assertNotNull(cost);
    System.out.println("Contents Motor Damage Cost: " + cost);

    cost = action.getWorkersCompensationCost();
    assertNotNull(cost);
    System.out.println("DWC Cost: " + cost);

    cost = action.getBuildingStormDamageCost();
    assertNotNull(cost);
    System.out.println("Building Storm Damage Cost: " + cost);
  }

  @Test
  public void testSpecifiedContents() {
    SpecifiedContentsForm action = new SpecifiedContentsForm();

    action.setHomeService(this.getHomeService());
    action.setOptionService(this.getOptionService());
    action.setQuoteNo(496);
    action.setQuoteType(action.getHomeService().retrieveQuoteDetail(action.getQuoteNo(), ActionBase.FIELD_QUOTE_SUB_TYPE));

    BigDecimal annualPremium = action.getTotalAnnualPremium();
    assertTrue(annualPremium.compareTo(BigDecimal.ZERO) != 0);

    System.out.println("Annual Premium (DB): " + annualPremium);

    Premium premium = action.getBasePremium();
    annualPremium = premium.getAnnualPremium();
    assertTrue(annualPremium.compareTo(BigDecimal.ZERO) != 0);

    System.out.println("Annual Premium (Calc): " + annualPremium);


    List<InRiskSi> risks = action.getSpecifiedContents();
    assertNotNull(risks);

    System.out.println("Specified Contents for Home Quote #" + action.getQuoteNo());
    
    for (InRiskSi r : risks) {
      for (InRiskSiLineItem i : r.getItemList()) {
        System.out.println("Risk: " + r.getSiClass() + ", " + i.getDescription() + ", " + i.getSumIns());
      }
    }
  }

  @Test
  public void testInvestorOccupancy() {

    AboutYourHome action = new AboutYourHome();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_INV);

    assertNotNull(action.getOccupancy());
    System.out.println("Investor Occupancy default: " + action.getOccupancy());
  }
  
  @Test
  public void testUnspecifiedPersonalEffectsAllowed() {

    PremiumOptionsForm action = new PremiumOptionsForm();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setQuoteNo(501); // Owner Occupied

    assertTrue(action.getAllowUnspecifiedPersonalEffects());

    action = new PremiumOptionsForm();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setQuoteNo(508); // Holiday Home

    assertTrue(!action.getAllowUnspecifiedPersonalEffects());
  }

  @Test
  public void testExistingPolicyHolders() {

    PolicyholderDetailsForm action = new PolicyholderDetailsForm();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_INV);
    action.setQuoteNo(507);

    List<WebInsClient> clientList = action.getHomeService().retrieveClients(action);
    assertNotNull(clientList);
    System.out.println("attached clients: " + clientList);

    List<WebInsClient> newClientList = action.getHomeService().retrieveNewClients(action);
    System.out.println("attached new clients: " + newClientList);

    List<WebInsClient> policyholderList = action.getHomeService().retrieveExistingPolicyHolders(action);
    System.out.println("unattached (existing) policy holders: " + policyholderList);

    Map<String,String> options = action.getExistingPolicyHolders();
    System.out.println("selectable options: " + options);
  }

  @Test
  public void testPaymentOptions() {
    Map<String,List<String>> fieldErrors;

    // verify DD + bank account
    PaymentOptions action = new PaymentOptions();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_INV);

    action.setPaymentOption(WebInsQuoteDetail.PAY_DD);
    action.setDirectDebitMethod(ActionBase.FIELD_DD_BANK_ACCOUNT);
    action.setBsb1("067");
    action.setBsb2("000");
    action.setAccountNo("12341234");
    action.setAccountName("Test Account");

    action.validate();

    fieldErrors = action.getFieldErrors();
    System.out.println(fieldErrors);
    
    assertTrue(fieldErrors.isEmpty());

    // verify DD + CC
    action = new PaymentOptions();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_INV);

    action.setPaymentOption(WebInsQuoteDetail.PAY_DD);
    action.setDirectDebitMethod(ActionBase.FIELD_DD_CREDIT_CARD);
    action.setCardType(WebInsQuoteDetail.ACC_TYPE_MASTERCARD);
    action.setCreditCard1("5123");
    action.setCreditCard2("4567");
    action.setCreditCard3("8901");
    action.setCreditCard4("2346");
    action.setCreditCardName("Test Person");
    action.setExpiryDateMonth("02");
    action.setExpiryDateYear("2014");

    action.validate();

    fieldErrors = action.getFieldErrors();
    System.out.println(fieldErrors);

    assertTrue(fieldErrors.isEmpty());

    // verify Annual + CC
    action = new PaymentOptions();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_INV);

    action.setPaymentOption(WebInsQuoteDetail.PAY_ANNUALLY);
    action.setCardType(WebInsQuoteDetail.ACC_TYPE_MASTERCARD);
    action.setCreditCard1("5123");
    action.setCreditCard2("4567");
    action.setCreditCard3("8901");
    action.setCreditCard4("2346");
    action.setCreditCardName("Test Person");
    action.setExpiryDateMonth("02");
    action.setExpiryDateYear("2014");

    action.validate();

    fieldErrors = action.getFieldErrors();
    System.out.println(fieldErrors);
    
    assertTrue(fieldErrors.isEmpty());

    // verify Annual + CC + dodgy input
    action = new PaymentOptions();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_INV);

    action.setPaymentOption(WebInsQuoteDetail.PAY_ANNUALLY);
    action.setCardType(WebInsQuoteDetail.ACC_TYPE_MASTERCARD);
    action.setCreditCard1("1324");
    action.setCreditCard2("1324");
    action.setCreditCard3("1324");
    action.setCreditCard4("1324");
    action.setCreditCardName("Test Person");
    action.setExpiryDateMonth("02");
    action.setExpiryDateYear("2010");

    action.validate();

    fieldErrors = action.getFieldErrors();
    System.out.println("Expecting input errors for valid credit card and expiry date:");
    System.out.println(fieldErrors);

    assertTrue(!fieldErrors.isEmpty());

    // verify DD + bank account + dodgy input
    action = new PaymentOptions();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_INV);

    action.setPaymentOption(WebInsQuoteDetail.PAY_DD);
    action.setDirectDebitMethod(ActionBase.FIELD_DD_BANK_ACCOUNT);
    action.setBsb1("067");
    action.setBsb2("000");
    action.setAccountNo("12345678");
    action.setAccountName("Test Account");
    
    action.validate();

    fieldErrors = action.getFieldErrors();
    System.out.println("Expecting input errors for payment day:");
    System.out.println(fieldErrors);

    assertTrue(!fieldErrors.isEmpty());

  }

  @Test
  public void testQuoteForm() {

    QuoteForm action = new QuoteForm();
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setQuoteNo(511);

    action.getTotalAnnualPremium();
  }
}
