package futuremedium.client.ract.secure.test.insurance.home;

import java.util.*;

import org.junit.*;

import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.WebInsQuote;

import com.ract.web.insurance.WebInsQuoteDetail;
import futuremedium.client.ract.secure.actions.insurance.home.*;
import futuremedium.client.ract.secure.test.TestBase;

/**
 *
 * @author gnewton
 */
public class BuildingContentsOptionsTest extends TestBase {

  public BuildingContentsOptionsTest() {
    super();
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }
/*
  @Test
  public void testYearConstructed() {
    AboutYourHomeForm action = new AboutYourHomeForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<String> options = action.getOptionService().getYearConstructedOptions();
    assertNotNull(options);
  }

  @Test
  public void testBusinessUse() throws Exception {
    AboutYourHomeForm action = new AboutYourHomeForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getBusinessUseOptions(action);
    assertNotNull(options);
    report("Business Use", options);
  }

  @Test
  public void testBuildingType() throws Exception {
    AboutYourHomeForm action = new AboutYourHomeForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getBuildingTypeOptions(action);
    assertNotNull(options);
    report("Building Type", options);
  }

  @Test
  public void testWallConstructionOptions() throws Exception {
    AboutYourHomeForm action = new AboutYourHomeForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getWallConstructionOptions(action);
    assertNotNull(options);
    report("Wall Construction Options", options);
  }

  @Test
  public void testRoofConstructionOptions() throws Exception {
    AboutYourHomeForm action = new AboutYourHomeForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getRoofConstructionOptions(action);
    assertNotNull(options);
    report("Roof Construction Options", options);
  }

  @Test
  public void testOccupancyOptions() throws Exception {
    AboutYourHomeForm action = new AboutYourHomeForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getOccupancyOptions(action);
    assertNotNull(options);
    report("Occupancy Options", options);
  }

  @Test
  public void testInvestorOccupancyOptions() throws Exception {
    AboutYourHomeForm action = new AboutYourHomeForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_INV);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getOccupancyOptions(action);
    assertNotNull(options);
    report("Investor Occupancy Options", options);
  }

  @Test
  public void testManagerOptions() throws Exception {
    AboutYourHomeForm action = new AboutYourHomeForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_INV);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getMangerOptions(action);
    assertNotNull(options);
    report("Manager Options", options);
  }

  @Test
  public void testSecurity() throws Exception {
    AboutYourHomeForm action = new AboutYourHomeForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getSecurityOptions(action);
    assertNotNull(options);
    report("Security", options);
  }

  @Test
  public void testUnrelatedPersons() throws Exception {
    AboutYourHomePolicyForm action = new AboutYourHomePolicyForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getUnrelatedPersonsOptions(action);
    assertNotNull(options);
    report("Unrelated Persons", options);
  }

  @Test
  public void testFinancier() throws Exception {
    AboutYourHomePolicyForm action = new AboutYourHomePolicyForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getFinancierOptions(action);
    assertNotNull(options);
    report("Financier", options);
  }

  @Test
  public void testSpecifiedContents() throws Exception {
    SpecifiedContentsForm action = new SpecifiedContentsForm();
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getSpecifiedContentsOptions(action);
    assertNotNull(options);
    report("Specified Contents", options);
  }

  @Test
  public void testBuildingInsuranceOptions() throws Exception {
    PremiumOptionsForm action = new PremiumOptionsForm();
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getBuildingInsuranceOptions(action);
    assertNotNull(options);
    report("Building Insurance Options", options);
  }

  @Test
  public void testContentsInsuranceOptions() throws Exception {
    PremiumOptionsForm action = new PremiumOptionsForm();
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getContentsInsuranceOptions(action);
    assertNotNull(options);
    report("Contents Insurance Options", options);
  }

  @Test
  public void testInvestorInsuranceOptions() throws Exception {
    PremiumOptionsForm action = new PremiumOptionsForm();
    action.setOptionService(this.getOptionService());
    List<InRfDet> options = action.getOptionService().getInvestorInsuranceOptions(action);
    assertNotNull(options);
    report("Investor Insurance Options", options);
  }

  @Test
  public void testExcessOptions() throws Exception {
    PremiumOptionsForm action = new PremiumOptionsForm();
    action.setOptionService(this.getOptionService());
    action.setHomeService(this.getHomeService());
    action.setQuoteType(ActionBase.VALUE_QUOTE_TYPE_BLDG_CNTS);
    action.setQuoteNo(QUOTE_NO);
    List<InRfDet> options = action.getOptionService().getExcessOptions(action, WebInsQuote.COVER_BLDG);
    assertNotNull(options);
    report("Excess Options", options);

    System.out.println("Building Default Excess: " + action.getBuildingDefaultExcess());
  }*/


  @Test
  public void testVehicleOptionValue() throws Exception {
    PremiumOptionsForm action = new PremiumOptionsForm();
    action.setOptionService(this.getOptionService());
    
    List<InRfDet> options = action.getOptionService().getInsuranceMgr().getLookupData("PIP", WebInsQuote.COVER_COMP, WebInsQuoteDetail.FINANCE_TYPE, action.getQuoteEffectiveDate());
    report("finance type", options);
  }
/*
  @Test
  public void testContentsSumInsured() {
    SumInsured action = new SumInsured();
    action.setHomeService(this.getHomeService());
    int min = action.getHomeService().getMinContentsSumInsuredAllowed(action);
    int max = action.getHomeService().getMaxContentsSumInsuredAllowed(action);
    assertTrue(min > 0 && max > 0 && min < max);
    System.out.println("Contents Sum Insured: min: " + min + ", max: " + max);
  }*/

  private void report(String title, List<InRfDet> options) {
    for (InRfDet r : options) {
      System.out.println(title
              + ": class=" + r.getrClass()
              + ", desc=" + r.getrDescription()
              + ", value=" + r.getrValue()
              + ", key=" + r.getrKey3()
              + ", unit=" + r.getrUnit()
              + ", acceptable=" + r.isAcceptable());
    }
  }

}

