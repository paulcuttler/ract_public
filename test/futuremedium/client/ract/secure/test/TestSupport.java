package futuremedium.client.ract.secure.test;

import java.util.Iterator;

import org.apache.commons.configuration.*;

/**
 *
 * @author gnewton
 */
public class TestSupport {

  static public void setupTestEnvironment()  {
    PropertiesConfiguration configuration = new PropertiesConfiguration();

//    try {
//      configuration.load("testsupport.properties");
//    } catch (ConfiguratonException e) {
//      e.printStackTrace();
//    }

    // Have to merge properties otherwise JVM crashes when running tests.
    @SuppressWarnings("unchecked")
    Iterator<String> itr =  configuration.getKeys();
    while (itr.hasNext()) {
      String key = itr.next();
      System.setProperty(key, configuration.getString(key));
    }
  }
}
