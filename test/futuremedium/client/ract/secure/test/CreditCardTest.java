/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package futuremedium.client.ract.secure.test;

import futuremedium.client.ract.secure.actions.RactActionSupport;

import java.util.regex.*;
import org.junit.*;

import static org.junit.Assert.*;

/**
 *
 * @author gnewton
 */
public class CreditCardTest {

  public CreditCardTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testCard() {

    assertTrue(RactActionSupport.validateCreditCardNumber("5123456789012346"));
    assertFalse(RactActionSupport.validateCreditCardNumber("1234567890123456"));

  }

  @Test
  public void testMemberCard() {
    String card = "1234567890123456";
    Matcher m = Pattern.compile("(.{4})(.{4})(.{4})(.{4})").matcher(card);
    m.matches();
    String card1 = m.group(1);
    String card2 = m.group(2);
    String card3 = m.group(3);
    String card4 = m.group(4);

    System.out.println("Card parts = " + card1 + ", " + card2 + ", " + card3 + ", " + card4);
  }
}