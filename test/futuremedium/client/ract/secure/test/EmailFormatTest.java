/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package futuremedium.client.ract.secure.test;

import futuremedium.client.ract.secure.actions.insurance.car.QuotePageSave;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author gnewton
 */
public class EmailFormatTest {
  private String emailPass = "garth.newton@futuremedium.com.au";
  private String emailFail = "garth.newton";
  private String emailNull;
  private String nameBlank = "";
  private String namePass = "Test O'Person-Smith";
  private String nameFail = "Test O'Person-Smith the 3rd";

    public EmailFormatTest() {
    }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * To run this test, QuotePageSave.validate() needs to be temporarily made static
     * and QuotePageSave.VALIDATE needs to be temporarily made public.
     *
    @Test
    public void testEmail() {
      try {
        assertTrue(QuotePageSave.validate(namePass, QuotePageSave.VALIDATE_NAME));
        assertFalse(QuotePageSave.validate(nameFail, QuotePageSave.VALIDATE_NAME));
        assertFalse(QuotePageSave.validate(nameBlank, QuotePageSave.VALIDATE_NAME));

        assertTrue(QuotePageSave.validate(emailPass, QuotePageSave.VALIDATE_EMAIL));
        assertFalse(QuotePageSave.validate(emailFail, QuotePageSave.VALIDATE_EMAIL));
        assertFalse(QuotePageSave.validate(emailNull, QuotePageSave.VALIDATE_EMAIL));

      } catch (Exception e) {
        System.out.println("Could not initialise QuotePageSave.");
      }
    }*/

}