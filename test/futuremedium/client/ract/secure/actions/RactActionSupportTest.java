package futuremedium.client.ract.secure.actions;

import org.junit.Test;

import futuremedium.client.ract.secure.test.TestBase;

public class RactActionSupportTest extends TestBase {
    public RactActionSupportTest() {
        super();
    }

    @Test
    public void testAustralianIpLookup() {
        RactActionSupport sut = new RactActionSupport();
        String ipLookupResult = sut.extractOriginatingCountry("117.120.18.136");
        assertNotNull(ipLookupResult);
        assertTrue(ipLookupResult.equals("Australia"));
    }
    
    @Test
    public void testSingaporeIpLookup_NoCountryInResponse() {
        RactActionSupport sut = new RactActionSupport();
        String ipLookupResult = sut.extractOriginatingCountry("203.9.78.19");
        assertNotNull(ipLookupResult);
        assertTrue(ipLookupResult.equals("Singapore"));
    }
    
    @Test
    public void testPrivateIpLookup() {
        RactActionSupport sut = new RactActionSupport();
        String ipLookupResult = sut.extractOriginatingCountry("10.10.20.48");
        assertNotNull(ipLookupResult);
        assertTrue(ipLookupResult.equals("IP_ADDRESS_RESERVED"));
    }
}
